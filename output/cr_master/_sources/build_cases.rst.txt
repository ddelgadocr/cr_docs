.. 
.. versive, primary, security engine
.. 

==================================================
Threat Cases
==================================================

.. include:: ../../includes_terms/term_threat_case.rst

.. The |vse| generates threat cases that summarize user and host behavioral anomalies that were discovered on a network. A threat case spans multiple days and may identify multiple users and hosts. The |vse| uses known adversary behaviors and patterns to identify anomalies, and then applies these anomaliles to the stages an adversary must perform before they can XCEAIUHFduihxc Each threat case spans multiple days and may identify more than one suspicious user or host. Each threat case represents a potential security threat. Each threat case is based on modeling data and includes anomaly details that provide explanations for any predictions made about any potential security threat. These calculations identify anomalies based on the strength of each anomaly that is discovered, and then from its individual measurement scores, individual stage scores, and combined stage score.


.. _build-cases-threat-cases:

About Threat Cases
==================================================
The patterns that the |vse| looks for depends (somewhat) on the available data sources (including data source quality) and campaign stages:

* Patterns for the |stage_recon| stage are visible from flow and DNS data sources.
* Patterns for the |stage_collect| stage are visible from flow data sources. 
* Patterns for the |stage_exfil| stage are visible from flow and proxy data sources.

Targets
--------------------------------------------------
The |vse| uses a collection of activities, units of measurement, statistics for each measurement, and behaviors to define specific targets:

* :ref:`Target activities <define-behaviors-activities>`, such as collecting data, communicating data, requesting data and publishing data
* :ref:`Detection methods <define-behaviors-detection-methods>`, such as A, AAAA, AXFR, and PTR record activities, byte movement and direction, FTP, ICMP, SNMP, and UDP protocol activities, port traffic and direction of data, and proxy data
* :ref:`Units of measurement <define-behaviors-units>`, such as byte movement (sent and received), domains contacted, IP addresses, broken down by source and destination, types of ports used for communication, types of protocols used for communication
* A determiniation of strength for each measurement: :ref:`strong or weak <define-behaviors-strength>`

The |vse| applies predictable detection methods to identify potential adversary activity and to discover the right types of anomalies within the data. Because these targets are modeled against a rolling seven-day time windows, the |vse| is able to identify more diverse patterns, more easily detect patterns that are more likely to be an actual threat, and to create more useful threat cases. Regardless of campaign stage and data source, strong targets are highly relevant for monitoring stage behaviors and can often be observed directly. Weak targets can be useful, especially when more then one weak target is associated with the same anomaly.


.. _threat-case-time-windows:

Time Windows
--------------------------------------------------
The |vse| identifies patterns over time by continuously modeling the behavior of source-destination IP address pairs over a rolling seven-day time window. A source-destination pair is any two machines on the network that have communicated with each other. Models are trained over rolling seven-day time windows that begin after the completion of a seven day learning period. Normal behavior is determined with respect to the time window. The |vse| observes actual behavior for each target across all network source-destination pairs, and then compares the actual behavior to normal behavior.

Threat Case List
The threat case list over time represents potentially anomalous activity within your corporate network. In many cases, upon further investigation, this activity will turn out to be benign or to be a false-positive. This is expected and normal. In rare cases, this activity will turn out to be a powerful indicator of adversary activity within your network.

All threat cases that appear in the threat case list require follow-up. This will identify the false-positive results and will identify when benign activity occurred. Once identified, these types of results can help inform the model and improve subsequent results over time and will make genuine adversary activity more easily identified. When activity is discovered that's unexpected, analyze it.

For example, the following threat contains the following list of findings for the |stage_exfil| stage:

   .. image:: ../../images/threat_viewer_05.svg
      :width: 500 px
      :align: left

At the top of this list are two suspicious IP addresses to which large amounts of data were exfiltrated: ``40.97.132.18`` and ``40.97.118.162``. Upon closer examination, more than 50 MB of data was exfiltrated to ``40.97.132.18`` and more than 40 MB of data was exfiltrated to ``40.97.118.162``, both amounts being well beyond the expected volume.

   .. image:: ../../images/threat_viewer_06.svg
      :width: 500 px
      :align: left

The following sections---:ref:`build-cases-review-recon`, :ref:`build-cases-review-collect`, and :ref:`build-cases-review-exfil`---go into greater detail about the findings associated with each of the individual stages, what types of behaviors and measurements were used to analyze the data, and what those findings mean. The :ref:`build-cases-adversary-campaign-indicators` section goes into detail about the types of activities thta lead to the discovery of an active adversary campaign.


.. _build-cases-adversary-campaign-indicators:

Adversary Campaigns
==================================================
.. TODO: pasted in quickly, for now

.. include:: ../../includes_terms/term_campaign.rst

.. image:: ../../images/stages_all.*
   :width: 600 px
   :align: center

The |stage_recon|, |stage_collect|, and |stage_exfil| stages represent the most interesting stages because they contain a large amount of visible evidence that is discoverable with the right data inputs and the right amount of processing and are the stages against which the engine is focused.

The engine looks for confirmation of behaviors across all three stages, identifying the patterns for recurring activities and reducing false positives. An adversary doesn't typically perform a single |stage_recon|, |stage_collect|, and |stage_exfil| campaign, rather, once successful, they will return for additional |stage_recon|, |stage_collect|, and |stage_exfil| as long as the time, cost, and complexity of that effort still falls within the adversary's risk tolerance.

Campaign Stages
--------------------------------------------------
.. include:: ../../includes_terms/term_campaign.rst

The |vse| is primarily focused on identifying patterns and behaviors that correlate to reconnaissance, data collection, and data exfiltration activities.

Recon Stage
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_campaign_stage_recon.rst

The |vse| looks for patterns in the data that can be associated with the |stage_recon|, such as:

* An excessive numbers of internal peers
* An excessive amount of communication across the peer network, with a particular focus on high communication volumes

Collection Stage
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_campaign_stage_collection.rst

The |vse| looks for patterns in the data that can be associated with the |stage_collect|, such as:

* The total volume of bytes sent and bytes received
* The ratio of bytes consumed to bytes produced
* The number of internal resources involved

Exfil Stage
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_campaign_stage_exfiltration.rst

.. image:: ../../images/threat_cases_stage_exfil.svg
   :width: 600 px
   :align: left

The |vse| looks for patterns in the data that can be associated with activites that are associated with the |stage_exfil| stage, such as:

* The ratio of outgoing bytes to incoming bytes
* The ratio of bytes sent to rare domains
* A relationship to other hosts inside the network, i.e. "other suspicious hosts in this threat case"

The list of findings are similar to:

.. image:: ../../images/threat_cases_exfil_activity.svg
   :width: 320 px
   :align: left

Each finding in the list identifies:

* A finding score
* A comparison of observed versus expected behavior
* The location---typicaly an IP address or host name---to which activity was published
* The IP address for the suspicious host

Click a finding in the list to view additional details. For example:

.. image:: ../../images/threat_cases_exfil_activity_detail.svg
   :width: 320 px
   :align: left

These additional details will identify:

* The date on which the activity occurred
* The amount of data that was published
* A chart that shows the activity for the time window that is covererd by this threat case
* A link to more information about the suspicious host: WHOIS, WHAT, XXXXX
* A link to more information about the location to which data was exfiltrated: XXXXX



Campaign Activity Indicators
--------------------------------------------------
The |vse| looks for patterns that correlate with the reconnaissance, data collection, and data exfiltration campaign stages.

The types of patterns that the |vse| looks for include:

* Volume of data transferred between entities
* Duration of sessions
* Number of neighbors
* Roles of clients and servers
* Sessions and protocols
* Breadth versus depth
* Client communication patterns
* Many short sessions 
* Large social circles

Patterns will vary in terms of quality:

* Patterns that are observed in isolation are rarely conclusive
* Patterns that are observed that combine with others to show many targets and are visible from high quality data sources are much stronger, especially when visible across time windows

.. _build-cases-data-transfer-volumes:

Data Transfer Volumns
++++++++++++++++++++++++++++++++++++++++++++++++++
Data that is transmitted transmitted between entities is discoverable as an outlier and depending on the outlier may be an important indicator of anomalous behavior. 

* Data transferred between entities within the same network that is discovered as an outlier is an indicator for |stage_collect| stage activity.
* Data transferred from an entity inside the network to an entity located outside that network that is discovered as an outlier is an indicator for |stage_exfil| stage activity.

When outliers are discovered, compare the entities associated with that outlier to all results generated by all data sources across all stages.

.. _build-cases-session-durations:

Session Durations
++++++++++++++++++++++++++++++++++++++++++++++++++
Session durations are an important indicator, especially when outliers show long session durations occuring on hosts that have previously been identified as interesting.

* Abnormally high session durations between hosts located inside the network may be an indicator of data collection.
* Abnormally high session durations from a host inside the network to a host outside the network may be an indicator of data exfiltration.
* Abnormally high session durations combined with little data being transferred between hosts indicates a host may be acting as a command-and-control channel.
* Low session durations with low data transfers, but regular frequency can indicate beaconing.
* A long session duration can indicate the host is being run as a remote system, proxied into from elsewhere, which can cause longer session durations than normal due to the behavior of systems run in this manner.

Session durations combined with data volume are even stronger indicators.

.. _build-cases-client-and-server-roles:

Client and Server Roles
++++++++++++++++++++++++++++++++++++++++++++++++++
Normal data flow across the internal network is straightforward:

* A client frequently initiates communications to a server
* A client rarely initiates communications to another client
* A server typically responds only to a requests from a client
* A server rarely initiates communications with a client or another server
* A client often initiates communications to locations outside of the network
* A server rarely initiates communications to locations outside of the network

The direction of normal data flow across the network should be:

#. A client requests information from a server.
#. A server responds to that request with information.
#. That client collects that information.

Data movement that does not follow this pattern shows up as an outlier and is generally considered anomalous behavior. The |vse| identifies these outliers based on to which campaign stage they are associated:

* The |stage_recon| stage looks for a high numbers of connections to internal systems, both client and server as an indicator for potential adversary mapping of the network.
* The |stage_collect| stage looks for longer connection times, abnormal data movement between clients and servers, and identfying which entities produce information and which entities consume it.
* The |stage_exfil| stage looks for entities inside the network inititing connections to locations outside of the network, and then identifies the amount of data transferred.

.. _build-cases-number-of-neighbors:

Number of Neighbors
++++++++++++++++++++++++++++++++++++++++++++++++++
Communication between entities inside of a network can be an important indicator of anomalous behavior that is often seen with |stage_recon| or |stage_collect| stage activity.

Most users within a network only communicate with a small number of servers. Any client that communicates with a high number of servers should be discoverable as an outlier.

Brief connections with many neighbors from a particular client may be an indicator of |stage_recon| stage activity. The same entity that later on has longer connections combined with data movement may be an indicator of |stage_collect| stage activity.

Most servers do not initiate connections to other entities inside the network. Some servers---DNS servers, mail servers, backup servers, and so on---are known, and can be whitelisted, leaving all non-whitelisted servers that initiate connections discoverable as an outlier.


.. _build-cases-sessions-and-protocols:

Sessions and Protocols
++++++++++++++++++++++++++++++++++++++++++++++++++
Sessions may be interactive or non-interactive:

* An interactive session will show consistently longer pauses because communications are waiting for human interaction.
* A non-interactive session will show consistently shorter pauses because communications are not waiting for human interaction.

Data sources that keep a record of packet sizes, inter-packet arrival times, and other session flow information are useful for discovering if a session was interactive or non-interactive.

In addition, analysis of protocols can be useful:

* SMTP, POP3, IMAP, DNS, NFS, and CIFS are expected to be non-interactive and should show as an outlier if they behave in an interactive manner.
* Telnet, remote terminal access, and chat servers are expected to be interactive and should show as an outlier if they behave in a non-interactive manner.


.. _build-cases-breadth-vs-depth:

Breadth vs. Depth
++++++++++++++++++++++++++++++++++++++++++++++++++
A client will make a get or put request for file directly, such as automatically through a Web browser or interactively via an FTP client. Most of the time a client is acting against a subset of the information stored on the server to which the request is made and navigates directly to the file to the directory in which the file is located. A normal request for a file already understands the data layout either from prior knowledge or via tools that are in place to help assist the effort of locating that file. An indicator of anomalous behavior is a client that first maps the entire directory structure, and then returns later to make requests for specific files within that directory structure.

.. _build-cases-communication-patterns:

Communication Patterns
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_behavior_client_server_schizophrenia.rst

It is useful to compare the behaviors of all clients as they communicate normally with servers on the network to help identify abnormal activity, such as if clients are communicating with other clients. The number of unique internal systems to which each client communicates with, along with the length of those sessions, are interesting when those sessions are longer-lived. Other useful indicators are the protocols used for communication and the numbers of unique internal systems to which each client commmunicates.

.. TODO: Edit the following commented out sections into the above

.. 
.. Normal data flow is easy to detect and graph because all entities within the internal network are easily identifiable as a client or a server, with rare exceptions. As such, it is important to identify all entities correctly.
.. 
.. Understanding the behavior of client and server hosts in a network requires an understanding of their traffic patterns over different ports and IP addresses. For example, a web server can be identified when traffic is typically sent from ports 80 or 443, whereas a client can be identified when traffic is received from ports 80 or 443. Data movement that does not follow normal patterns may be benign, for various reasons. For example:
.. 
.. * A non-standard but otherwise acceptable protocol was used for communication.
.. * The role of the server. For example, a backup server often has a dual role that is known to the network and will behave as both a client and a server. This type of server is a known entity and should be easily identifiable when shows up as an outlier in the data.
.. 


.. _build-cases-many-short-sessions:

Many Short Sessions
++++++++++++++++++++++++++++++++++++++++++++++++++
Many short-lived sessions, combined with breadth-centric file system indexing are indicators of adversary activity in the |stage_recon| stage. This evidence is typically seen in log files. This behavior is similar to what is seen in the |stage_collect| stage; however, the durations of sessions to that network location will increase significantly.

.. _build-cases-large-social-circles:

Large Social Circles
++++++++++++++++++++++++++++++++++++++++++++++++++
A client normally talks to few systems. For example, an email server, a bug tracking system, and other internal team-specific communication tools. A member of a software engineering team rarely uses the same systems available to customer-facing teams. There are normal patterns of use across the network, with regard to the systems to which a client communicates. And most client systems will fall within a normal range.

Identifying larger-than-normal social circles, especially during the |stage_recon| stage, is abnormal and nodes that exhibit this behavior, and then show abnormal behavior during the |stage_collect| phase are more likely to show adversary activity.





Threat Case Scoring
==================================================
Predictions that identify true anomalies are scored; each finding is assigned a risk score and an anomaly score.

Each campaign stage is assigned a score, which is based on two things:

* All of the findings that are associated with that stage for the entire window
* Any unique source-destination pair with a high risk score; source-destination pairs are put in a ranked list

Finally, individual scores assigned to each stage help determine the case score, which is a score assigned to the threat case itself.


The following scoring methods are used to help determine if adversary behavior is present on the network:

* :ref:`Risk scores <build-cases-risk-scores>`
* :ref:`Surprise scores <build-cases-surprise-scores>`
* :ref:`Stage scores <build-cases-stage-scores>`
* :ref:`Finding scores <build-cases-finding-scores>`


.. _build-cases-risk-scores:

Risk Scores
--------------------------------------------------
.. include:: ../../includes_terms/term_score_risk.rst


.. _build-cases-surprise-scores:

Surprise Scores
--------------------------------------------------
.. include:: ../../includes_terms/term_score_surprise.rst


Case Scores
--------------------------------------------------
.. include:: ../../includes_terms/term_score_threat_case.rst


.. _build-cases-stage-scores:

Stage Scores
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_campaign_scores_stage.rst


.. _build-cases-finding-scores:

Finding Scores
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_score_finding.rst







Review Threat Cases
==================================================
A threat case is the outcome of careful analysis of your network by the |vse|. Every threat case shows unique interactions between hosts located inside and outside of your network. As such, every threat case is different. The process of reviewing a threat case is improved by knowing the network that is being analyzed as well as having a good understanding of the expected usage patterns and behaviors for those machines when they are flagged as a suspicious host by the |vse|.

A threat case is reviewable from the web user interface for the |vse|:

.. image:: ../../images/threat_case_components.*
   :width: 600 px
   :align: center

At a high level, every threat case shows:

#. The campaign stage lifecycle, which describe :ref:`the steps and processes that an adversary campaign must follow <build-cases-adversary-campaign-indicators>` in order for it to be successful. Each stage represents patterns and behaviors that are discoverable by the |vse| from within the data about an organization’s network. A threat case is assigned a score, and then each stage (|stage_recon|, |stage_collect|, and |stage_exfil|) are assigned individual scores.
#. The :ref:`patterns that correlate to the campaign stage lifecycle <build-cases-threat-cases>`. Strong indicators of adversary activity will show many identifiable patterns, associated with many identifiable targets, visible from multiple data sources, and across time windows, that help show the relationships between hosts inside and outside of the network, broken out by host and by campaign stage.
#. Behaviors help the |vse| :ref:`separate normal activity from abnormal activity on the network <threat-case-behaviors>`. The |vse| identifies groups of machines, relationships, and communication patterns, an so on, and then identifies anomalies in that data that can be associated with findings, and then assigned a risk score. Behaviors are shown in the threat cases as individual data points.
#. The most valuable threat cases will :ref:`show activity over multiple time windows <threat-case-time-windows>` that detail how the threat case (and related activity) evolved over time.
#. A threat case is generated when :ref:`high risk scores are present <threat-case-generate-threat-cases>` in at least two of three stages for any source-destination pair. A threat case must have at least one suspicious host, but may have more when two (or more) hosts with high risk scores show that data movement has occurred between them. Each threat case generated by the |vse| is assigned a unique identifier and the list of threat cases is available from a simple menu.

Daily Threat Case List
--------------------------------------------------
A threat case list is generated on a daily basis.

Map vs. Stage Views
--------------------------------------------------

.. image:: ../../images/threat_cases_detail_bubbles.svg
   :width: 500 px
   :align: left

.. image:: ../../images/threat_cases_detail_map.svg
   :width: 500 px
   :align: left


.. image:: ../../images/threat_cases_suspicious_host_1_labeled.svg
   :width: 500 px
   :align: left

.. image:: ../../images/threat_cases_suspicious_host_2_labeled.svg
   :width: 500 px
   :align: left

.. image:: ../../images/threat_cases_suspicious_host_3_labeled.svg
   :width: 500 px
   :align: left


Suspicious Hosts
--------------------------------------------------

.. image:: ../../images/threat_cases_suspicious_host_1.svg
   :width: 500 px
   :align: left

.. image:: ../../images/threat_cases_suspicious_host_2.svg
   :width: 500 px
   :align: left

.. image:: ../../images/threat_cases_suspicious_host_3.svg
   :width: 500 px
   :align: left

Stages
--------------------------------------------------
A threat case breaks down activity by |stage_recon|, |stage_collect|, and |stage_exfil| stages.


.. _build-cases-review-recon:

Recon Activity
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_campaign_stage_recon.rst

Reconnaissance behavior is detected from DNS and flow data:

* .. include:: ../../includes_terms/term_datatype_dns.rst
* .. include:: ../../includes_terms/term_datatype_flow.rst

.. image:: ../../images/threat_cases_stage_recon.svg
   :width: 500 px
   :align: left

.. image:: ../../images/threat_cases_recon_activity.svg
   :width: 500 px
   :align: left

.. image:: ../../images/threat_cases_recon_activity_detail.svg
   :width: 500 px
   :align: left


.. _build-cases-review-recon-targets:

Recon Targets
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. TODO: Sync with define_behaviors.rst

Targets for the |stage_recon| stage are calculated from flow and DNS data sources.

Strong targets are highly relevant for monitoring |stage_recon| stage behaviors. These targets can be observed directly and are often present in a successful campaign:

* A distinct count of destination IP addresses accessed via the ICMP protocol over a 30-day window.
* A distinct count of destination IP addresses accessed via the SNMP protocol over a 30-day window.
* A distinct count of destination IP addresses contacted through high-interest ports over a 30-day window.
* A distinct count of named domains contacted via DNS PTR records over a 30-day window.

Weak targets are useful for monitoring |stage_recon| stage behaviors, especially if more than one weak target is associated with the same anomaly:

* A distinct count of destination IP addresses accessed via the UDP protocol over a 30 day window.
* A distinct count of destination IP addresses contacted through medium-interest ports over a 30-day window.
* A distinct count of destination IP addresses contacted through low-interest ports over a 30-day window.
* A distinct count of named domains contacted via DNS A records over a 30-day window.
* A distinct count of named domains contacted via DNS AAAA records over a 30-day window.


.. _build-cases-review-recon-findings-list:

Recon Findings List
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. include:: ../../includes_terms/term_finding.rst

The list of findings for the |stage_recon| stage are presented in a table format, with each finding listed in its own row and each row having the following columns:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Column
     - Description
   * - **Score**
     - The :ref:`risk score <build-cases-risk-scores>` for this finding.
   * - **Observed/Expected**
     - The observed behavior, as compared to the expected behavior. For example, "170 MB bytes published via netflow" vs. "Expected 180 bytes".
   * - **Suspicious Host**
     - The IP address for the suspicious host around which this threat case is built.



.. _build-cases-review-recon-finding-details:

Recon Finding Details
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Each |stage_recon| stage finding adds the following details:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Column
     - Description
   * - **Date**
     - The date on which the reconnaissance activity occurred.
   * - **Destination IP**
     - The IP address that was contacted by the suspicious host.




.. _build-cases-review-collect:

Collection Activity
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_campaign_stage_collection.rst

.. image:: ../../images/threat_cases_stage_collection.svg
   :width: 500 px
   :align: left

.. image:: ../../images/threat_cases_collection_activity.svg
   :width: 500 px
   :align: left

.. image:: ../../images/threat_cases_collection_activity_detail.svg
   :width: 500 px
   :align: left



.. _build-cases-review-collect-targets:

Collection Targets
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. TODO: Sync with define_behaviors.rst

Targets for the |stage_collect| stage are calculated from flow data sources.

.. TODO: 2nd bullet is a wild guess.

Strong targets are highly relevant for monitoring |stage_collect| stage behaviors. These targets can be observed directly and are often present in a successful campaign:

* The sum of bytes collected by source IP addresses over a 30-day window.
* The number of internal neighbors per source IP address, as compared to other source IP addresses on the network, for a server acting as a client and/or for a client acting as a server.

Weak targets are useful for monitoring |stage_collect| stage behaviors, especially if more than one weak target is associated with the same anomaly:

* The number of internal neighbors per source IP address, as compared to other source IP addresses on the network.


.. _build-cases-review-collect-findings-list:

Collection Findings List
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. include:: ../../includes_terms/term_finding.rst

The list of findings for the |stage_collect| stage are presented in a table format, with each finding listed in its own row and each row having the following columns:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Column
     - Description
   * - **Score**
     - The :ref:`risk score <build-cases-risk-scores>` for this finding.
   * - **Observed/Expected**
     - The observed behavior, as compared to the expected behavior. For example, "170 MB bytes published via netflow" vs. "Expected 180 bytes".
   * - **Suspicious Host**
     - The IP address for the suspicious host around which this threat case is built.
   * - **Collected From**
     - The hostname or IP address for the location from which data was collected.



.. _build-cases-review-collect-finding-details:

Collection Finding Details
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Each |stage_collect| stage finding adds the following details:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Column
     - Description
   * - **Date**
     - The date on which the collection activity occurred.
   * - **Bytes Collected**
     - The amount of data (in bytes) that was collected from the internal source by the suspicious host.
   * - **Bytes Published**
     - The amount of data (in bytes) that was published to the internal source by the suspicious host.
   * - **Consumption Ratio**
     - .. include:: ../../includes_terms/term_ratio_consumption.rst

       For the |stage_collect| stage, this is the ratio of data collected and received. A suspicious host that is also interesting will have a high consumption ratio, which is an indicator of large amounts of data moving toward the suspcious host from one (or more) locations inside of the network.

       When a suspicious host has a high collection ratio, investigate the locations inside the network from which data was collected. Are these machines from which data is normally collected? Are these machines that are behaving correctly by role?

       .. note:: The ability to evaluate how interesting a suspicious host may be depends on knowing the layout of the network, and then what the normal and expected traffic on that network should be. The engine doesn't know anything about a network beyond what it can detect from the data it is asked to analyze. As such, hosts that are flagged as suspicious hosts may in fact be benign, even though they exhibit many of the traits of adversary activity.


.. _build-cases-review-exfil:

Exfil Activity
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_campaign_stage_exfiltration.rst

.. image:: ../../images/threat_cases_stage_exfil.svg
   :width: 500 px
   :align: left

.. image:: ../../images/threat_cases_exfil_activity.svg
   :width: 500 px
   :align: left

.. image:: ../../images/threat_cases_exfil_activity_detail.svg
   :width: 500 px
   :align: left


.. _build-cases-review-exfil-targets:

Exfil Targets
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. TODO: Sync with define_behaviors.rst

Targets for the |stage_exfil| stage are calculated from flow and proxy data sources.

Strong targets are highly relevant for monitoring |stage_exfil| stage behaviors. These targets can be observed directly and are often present in a successful campaign:

* The sum of bytes published to exclusive external domains, and to those which have a high publication ratio, over a 30-day window.

Weak targets are useful for monitoring |stage_exfil| stage behaviors, especially if more than one weak target is associated with the same anomaly:

* The sum of bytes published to external domains over a 30-day window.
* The sum of bytes published to external domains, and to those which have a high publication ratio, over a 30-day window.
* The sum of bytes published to exclusive external domains over a 30-day window.


.. _build-cases-review-exfil-findings-list:

Exfil Findings List
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. include:: ../../includes_terms/term_finding.rst

The list of findings for the |stage_exfil| stage are presented in a table format, with each finding listed in its own row and each row having the following columns:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Column
     - Description
   * - **Score**
     - The :ref:`risk score <build-cases-risk-scores>` for this finding.
   * - **Observed/Expected**
     - The observed behavior, as compared to the expected behavior. For example, "170 MB bytes published via netflow" vs. "Expected 180 bytes".
   * - **Suspicious Host**
     - The IP address for the suspicious host around which this threat case is built.
   * - **Published To**
     - The external hostname or IP address to which the exfiltration activity was published.


.. _build-cases-review-exfil-finding-details:

Exfil Finding Details
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Each |stage_exfil| stage finding adds the following details:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Column
     - Description
   * - **Date**
     - The date on which the exfiltration activity occurred.
   * - **Bytes Collected**
     - The amount of data (in bytes) that was collected from the external location by the suspicious host.
   * - **Bytes Published**
     - The amount of data (in bytes) that was published to the external location by the suspicious host.
   * - **Publication Ratio**
     - .. include:: ../../includes_terms/term_ratio_publication.rst

       For the |stage_exfil| stage, this is the ratio of data published and received. A suspicious host that is also interesting will have a high publication ratio, which is an indicator of large amounts of data moving from the suspcious host to a location outside of the network.

       When a suspicious host has a high publication ratio, investigate the IP addresses to which large amounts of data were sent. Click the highlighted URL or IP address under **Published to** to view more detailed information: WHOIS information for a domain name or more detailed information about an IP address. 

       Click VirusTotal to open a page on https://virustotal.com that contains more information about activity related to the URL or IP address. VirusTotal is a service that analyzes suspicious files and URLs, helps identify the presence of viruses, worms, trojans, or other types of malware. 

       Use Google to perform additional searches to learn more about a suspicious IP addresses, and then attempt to identify the names of those domains, where they are located, what they are related to (both inside and outside of the network), and so on, to help determine if that activity is normal.

       .. note:: The ability to evaluate how interesting a suspicious host may be depends on knowing the layout of the network, and then what the normal and expected traffic on that network should be. The engine doesn't know anything about a network beyond what it can detect from the data it is asked to analyze. As such, hosts that are flagged as suspicious hosts may in fact be benign, even though they exhibit many of the traits of adversary activity.



.. _build-cases-review:

Review Threat Cases
==================================================
.. include:: ../../includes_terms/term_threat_case.rst

The threat viewer is a web UI that is designed to help with the daily review of threat cases by breaking down each threat case into findings that represent the greatest threats to your company's security.

The threat viewer shows all of the top threat cases. The data is processed and modeled daily, from which a list of threat cases is also built daily. As such, the threat case list in the threat viewer should also be reviewed on a daily basis.

**To review threat cases**

#. Log into the threat viewer.
#. Open the threat case list and choose the one with the highest case score.
#. Review that threat case at the high level. The individual stage scores---for the |stage_recon|, |stage_collect|, and |stage_exfil| stages---combine to create the case score.

   .. image:: ../../images/threat_viewer_01.svg
      :width: 500 px
      :align: left

#. All of the findings for all of the stage scores are shown graphically on the left panel. The size and color of the bubble indicates the threat level, with larger circles and red circles (as opposed to grey or yellow) indicating the greatest threats, as shown below (left side).

   .. image:: ../../images/threat_viewer_02_both.svg
      :width: 500 px
      :align: left

   Hover over the suspicious host (identified by an IP address) to see the relationships between that host and all of the findings. Click any of the individual findings (bubbles) to view the details for that finding, as shown above (right side).

#. Review each individual stage. Each finding associated with that stage is assigned a finding score. The individual finding scores are combined to create the stage score. The findings are shown in a table. The columns are sortable. For example, the |stage_exfil| stage with the:

   .. image:: ../../images/threat_viewer_03.svg
      :width: 500 px
      :align: left

#. Review the scores over time to get a sense of how this threat case has evolved. For example, the |stage_exfil| stage:

   .. image:: ../../images/threat_viewer_04.svg
      :width: 500 px
      :align: left

