.. 
.. versive, primary, security engine
.. 

==================================================
Cluster Status Portal
==================================================

Detailed information about the cluster status, nodes, and jobs is available from the cluster status portal. Port 9100 is used by default, but may be modified using the :ref:`cmd-cr-cluster-setprop` command.

Each of the links along the top of the portal open the following pages.


.. _manage-cluster-status-portal-cluster-status:

Cluster Status
==================================================
The status page displays the overall cluster memory and CPU usage, compute skew, and any alerts. Each node in the cluster is listed with its IP address, heartbeat, memory, and status of task executors.

.. list-table::
   :widths: 80 520
   :header-rows: 1

   * - Color
     - Description
   * - **White**
     - The task executor is idle.
   * - **Green**
     - The task is running. Hover over the box to see which task is running.
   * - **Red**
     - The task is taking a long time to complete. There are several reasons that can explain long-running tasks, such as:

       * Imbalanced data sharding. For example, if a particular key has a large volume of data, the shard that the key belongs too will require more processing.
       * The initial read of a single large data file.
       * A large reduction operation in which results are being reduced from other nodes to the node running the task.
   * - **Black**
     - The task executor has failed. A single black box indicates a crash, typically an out-of-memory error. An entire row of black boxes represents a machine problem such as a networking issue, a system crash, or a problem with the node monitor resulting in missing heartbeats. Reset the cluster if this occurs.

Currently running and completed jobs are then listed with their start time and the total CPU time required to complete the job. Multiple compute jobs can, and often do, run for the same line of Python script. Each job contains multiple Resilient Distributed Dataset (RDD) and task components. Expand the job list and click any of the jobs to view more detailed statistics that may be useful in troubleshooting.

Below this are graphs for the cluster as a whole and for each individual node that display CPU usage, memory usage, disk space usage, sent messages, and received messages.


.. _manage-cluster-status-portal-nodes:

Nodes
==================================================
This page displays information for each individual node. On the left is listed CPU and memory usage, IP address, heartbeat, task executors, etc. On the right are graphs that display CPU usage, memory usage, disk space usage, sent messages, and received messages.


.. _manage-cluster-status-portal-jobs:

Jobs
==================================================
This page displays currently running and completed jobs.


.. _manage-cluster-status-portal-models:

Models
==================================================
This page displays the quality scores of measurement models built for the engine. These scores are stored in a database after every run.
