.. 
.. xxxxx
.. 

==================================================
Stats API
==================================================

The Stats API may be used for classical statistics routines and helpers.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Stats API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.stats as stats

and then for each use of a function in this API within the customization package add ``security.`` as the first part of the function name. For example:

.. code-block:: python

   stats.t_test()

or:

.. code-block:: python

   stats.t_test()


The following functions may be used to build customized tables, models, and transaction behaviors:

* :ref:`api-stats-t-test`
* :ref:`api-stats-t-test-result`
* :ref:`api-stats-T-square-test`
* :ref:`api-stats-T-square-test-result`
* :ref:`api-stats-wallenius-uni-cdf`
* :ref:`api-stats-wallenius-uni-pmf`



.. _api-stats-t-test:

t_test()
==================================================
Compute the t-statistic for a sample with given mean, standard deviation, and sample size n. Returns both hypothesis test results and a confidence interval for the null hypothesis. Let H0 denote the null hypothesis.

.. note:: This function does not support t-statistics for testing the equality of two independent samples (non-paired) because the degrees of freedom always equals n - 1.

Usage
--------------------------------------------------

.. code-block:: python

   cr.stats.t_test(mean,
                   stddev,
                   n,
                   alpha=0.1,
                   interval_type=TWO_SIDED,
                   mu=0.0)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**mean** (float, required)
   The sample mean.

**stddev** (float, required)
   The sample standard deviation.

**n** (float, required)
   Sample size.

**alpha** (float, optional)
   The significance level of the hypothesis test and control for the stringency of the confidence interval. Default is 0.1.

**interval_type** (str, optional)
   Possible values: ``TWO_SIDED``, ``ONE_SIDED_LOWER``, or ``ONE_SIDED_UPPER``. For the one-sided types, the named end is the defined part of the interval. For example, a one-sided lower interval might be found as (-1.3, +inf), and a one-sided upper interval might be found as (-inf, 1.8). Defaults value ``TWO_SIDED``.

**mu** (float, optional)
   The assumed mean of the sample, under the H0. Default is 0.


Returns
--------------------------------------------------
A t_test_result; result.answer can have values -1, 0, or 1. When answer=0, the sample mean is not significantly different than mu, and t_value is inside the confidence interval. Otherwise, t_value falls outside the confidence interval. When answer=-1 (or answer=1), the sample mean is statistically significantly smaller (larger) than mu, at significance level alpha. For one-sided intervals, only one of 1 or -1 can be returned.

Example
--------------------------------------------------

Get two-sided, 95% confidence interval:

.. code-block:: python

   >>> info = t_test(mean, stddev, n, alpha=0.05)
   >>> print '({0}, {1})'.format(info.lo, info.hi)

Test if mean significantly different from 0, at 0.1 level:

.. code-block:: python

   >>> info = t_test(mean, stddev, n, alpha=0.1)
   >>> print 'yes' if info.answer != 0 else 'no'

Test if mean <= 5:

.. code-block:: python

   >>> info = t_test(mean, stddev, n, mu=5, interval_type='ONE_SIDED_LOWER')
   >>> print 'yes' if info.answer == -1 else 'no'




.. _api-stats-t-test-result:

t_test_result()
==================================================
Result from t_test().

Usage
--------------------------------------------------

.. code-block:: python

   t_test_result(answer,
                 lo,
                 hi,
                 t_value,
                 p_value)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**answer** (int)
   Result of hypothesis test.

**lo** (float)
   Lower bound of the (1-alpha) confidence interval.

**hi** (float)
   Upper bound of the (1-alpha) confidence interval.

**t_value** (float)
   Observed value of the statistic T.

**p_value** (float)
   Assuming H0, the probability of seeing more extreme t-value.

Returns
--------------------------------------------------
Result from t_test().

Example
--------------------------------------------------
None.


.. _api-stats-T-square-test:

T_square_test()
==================================================
Compute `Hotelling’s T-square statistic <https://en.wikipedia.org/wiki/Hotelling%27s_T-squared_distribution>`__. This can be used to test if a p-dimensional mean (vector of values) is significantly different from a mean of zeros. This is the multivariate version of :ref:`the well-known t-test <api-stats-t-test>`.

Usage
--------------------------------------------------

.. code-block:: python

   cr.stats.T_square_test(mean,
                          covariance,
                          n,
                          alpha=0.05,
                          interval_type=TWO_SIDED)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**mean** (numpy array, required)
   The sample mean. Should be a length p vector.

**covariance** (numpy array, required)
   The sample’s covariance matrix. Should be a p by p array.

**n** (float, required)
   Sample size.

**alpha** (float, optional)
   The significance level of the hypothesis test and control for the stringency of the confidence interval. Default value: ``0.05``.

**interval_type** (str, optional)
   Possible values: ``TWO_SIDED``, ``ONE_SIDED_LOWER``, or ``ONE_SIDED_UPPER``. For the one-sided types, the named end is the defined part of the interval. For example, a one-sided lower interval might be found as (-1.3, +inf), and a one-sided upper interval might be found as (-inf, 1.8). Defaults value ``TWO_SIDED``.

Returns
--------------------------------------------------
A T_square_test_result. When result.answer=0, the sample mean is not significantly different from the zero vector. Otherwise, result.mean is -1 or +1, to show whether sum(mean) is less or greater than 0. For one sided intervals, only one of -1 or +1 can be returned.

Example
--------------------------------------------------

.. code-block:: python

   >>> X = numpy.array(...)
   >>> mu = X.mean(axis=0)
   >>> C = numpy.cov(X, rowvar=0)
   >>> result = cr.stats.T_square_test(mu, C, X.shape[0])
   >>> print result



.. _api-stats-T-square-test-result:

T_square_test_result()
==================================================
Result from T_square_test().

Usage
--------------------------------------------------

.. code-block:: python

   cr.stats.T_square_test_result(answer,
                                 T2_value,
                                 F_value,
                                 p_value)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**answer** (int)
   Result of hypothesis test. Zero means the null hypothesis was accepted, +1 indicates rejected because sum(mean) > 0, and -1 indicates rejected because sum(mean) < 0.

**T2_value** (float)
   Observed value of Hotelling’s T-square statistic.

**F_value** (float)
   Observed value of the F-statistic.

**p_value** (float)
   Assuming H0, the probability of seeing more extreme test statistic.

Returns
--------------------------------------------------
Result from T_square_test().

Example
--------------------------------------------------
None.


.. _api-stats-wallenius-uni-cdf:

wallenius_uni_cdf()
==================================================
Compute the probability of drawing at most x balls of the first color from an urn with two colors of weighted balls. Aka, a univariate Wallenius hypergeometric distribution.

Usage
--------------------------------------------------

.. code-block:: python

   cr.stats.wallenius_uni_cdf(x,
                              n,
                              urn,
                              weights)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**x** (int)
   The number of balls drawn of the first color.

**n** (int)
   Total number of balls drawn from the urn.

**urn** (list)
   The total number of balls of each color in the urn.

**weights** (list)
   The sampling weight for each color.

Returns
--------------------------------------------------
Pr(X <= x).

Example
--------------------------------------------------
None


.. _api-stats-wallenius-uni-pmf:

wallenius_uni_pmf()
==================================================
Compute the probability mass for a given draw from a univariate Wallenius non-central hypergeometric distribution. This function requires that the urn contains exactly two types of balls. An exception is raised if any input has length greater than 2.

Usage
--------------------------------------------------

.. code-block:: python

   cr.stats.wallenius_uni_pmf(x,
                              n,
                              urn,
                              weights)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**x** (int)
   The number of balls drawn of the first color.

**n** (int)
   Total number of balls drawn from the urn.

**urn** (list)
   The total number of balls of each color in the urn.

**weights** (list)
   The sampling weight for each color.

Returns
--------------------------------------------------
Pr(X = x).

Example
--------------------------------------------------
None.
