.. 
.. versive, primary, platform
.. 


==================================================
Model Data
==================================================

.. include:: ../../includes_terms/term_model.rst


.. _model-data-predictive-models:

Predictive Models
==================================================
Designing a predictive model---especially a classification model---can be difficult. Some models are straightforward, with clear inputs and clear goals. Other models may be more difficult and require additional processing, such as when the desired inputs to the model do not exist in the source data itself. Often, data requires aggregation and transformation prior to modeling, which increases the processing effort required prior to modeling the data.

The process for designing a predictive model that makes high quality predictions involves a series of steps:

#. Understanding the business goals for the model
#. Understanding the data to be input to the model
#. Preparing and processing that data, so that it is ready for modeling
#. Modeling the data; re-modeling when improvements to the model can be identified
#. Evaluating the results; re-evaluate steps 1-4 as necessary to identify improvements to the model
#. Deploy the model into production; evaluate these results, and then identify improvements to the model, re-evaluating those improvements, and then re-deploying the improved model

These steps are not meant to be linear or synchronous. Good modeling practices and patterns require constant re-evaluation of a model, its inputs, and its outputs, with the goal of finding ways for continuous improvement of the model so that higher quality predictions can be made.


.. TODO: See the image in the /misc/ folder for a visual of the workflow described below: "Cross-Industry Standard Process for Data Mining (CRISP-DM)". Not sure about this one, at least with direct reference to methodology and terminology.

.. _model-data-understand-goals:

Understand the Goals
--------------------------------------------------
The first step in a data science project is to determine business goals and requirements. Because there may be several competing factors, this task can be more difficult than it sounds. We recommend that each problem be analyzed from many different angles to help ensure that the right questions are being asked. There is often a primary goal, with several related secondary goals. For example, identifying which customers are likely to move to a competitor is a primary goal, and then identifying which customer segments are more sensitive to lower fees is a secondary goal.

These goals must be translated into more specific, technical terms. For example, the primary goal of identifying which customers are likely to move to a competitor could be restated as "predict the likelihood of a given customer closing their accounts in the next six months, given purchases made over the past three years, relevant demographic information, and fees for the accounts." The restated goal is more clear, more measurable, and provides more opportunities for specific measurements that can be described as clear inputs to a modeling table.

.. _model-data-understand-data:

Understand the Data
--------------------------------------------------
The |versive| platform structures data in data tables that are similar to those used in other database systems. Each data table contains rows and columns:

* Columns contain inputs: properties that are expected to influence the value of a model's predictions
* Rows contain records, such as data about individual transactions

Each model takes inputs, and then makes predictions based on those inputs.

.. _model-data-prepare-data:

Prepare the Data
--------------------------------------------------
Data preparation involves cleaning up messy data, joining multiple data tables, and other tasks in preparation for modeling. In traditional data modeling, the data preparation phase involves a lot of manual wrangling and cleanup and is by far the most time-consuming phase in the process. With the |versive| platform, many of these tasks are automated. Data preparation includes:

* Data verification
* Measuring descriptive statistics
* Removing unnecessary columns of data
* Removing data that doesn't meet certain criteria
* Joining additional data
* Aggregating data
* Transforming data to calculate new measurements

.. _model-data-model-data:

Model the Data
--------------------------------------------------
From prepared data, the |versive| platform can learn a predictive model from features that are automatically identified as inputs to the model. A model describes the mathematical relationship between the inputs and outputs of interest. For example, a model might use postal codes, number of bedrooms, and build year as inputs to predict housing prices.

The |versive| platform uses supervised machine learning to build predictive models. That is, you provide data in which both the inputs and target values are known and then machine learning algorithms build models that describe the relationship between the inputs and target.

.. _model-data-automated-exploration:

Automated Exploration
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_automated_exploration.rst

**basic**
   .. include:: ../../includes_terms/term_automated_exploration_basic.rst

**interactions**
   .. include:: ../../includes_terms/term_automated_exploration_interactions.rst

**none**
   All colums in the train table are used as features without applying any machine learning techniques.

.. _model-data-table-sets:

Table Sets
++++++++++++++++++++++++++++++++++++++++++++++++++
Before you can begin learning a model, it must be split into the following table sets:

**train table**
   .. include:: ../../includes_terms/term_table_train.rst

**tune table**
   .. include:: ../../includes_terms/term_table_tune.rst

**test table**
   .. include:: ../../includes_terms/term_table_test.rst

.. _model-data-model-types:

Model Types
++++++++++++++++++++++++++++++++++++++++++++++++++
Once the data is split into these different sets, specify the type of model to build:

**binary classification**
   .. include:: ../../includes_terms/term_model_type_binary_classification.rst

   Binary classification models may not have the ideal target defined in the initial data set. This requires aggregation and/or transformation of input data into the ideal target prior to modeling.
**linear regression**
   .. include:: ../../includes_terms/term_model_type_linear_regression.rst

**multiclass classification**
   .. include:: ../../includes_terms/term_model_type_multiclass_classification.rst

   Multiclass classification models support multiple targets, which, similar to a binary classification model, may require aggregating or transforming the inputs, but also often require additional user-defined function support. For example, a model might idenfify which of four items is most likely to be selected in a given timeframe. A user-defined function can be designed to accept these inputs, and then depending on which item is selected, return values as a new inputs to the model.

After a model is learned, it may be saved and retrieved at a later time for evaluation or further predictions. After a model is satisfactory, use it to obtain predictions from new data in which the target is unknown.
  
.. _model-data-evaluate-measurements:

Evaluate Measurements
--------------------------------------------------
After a model is built it should be evaluated for prediction quality. Depending on the results of this evaulation, adjust the targets or levels of desired accuracy. It's common to discover that certain inputs should be removed, or that some important business goals weren't considered fully during the original project planning.

The first predictive model is seldom the model that will be deployed. Instead, the first predictive model is used to gain insights during evaluation so that a improved, more accurate models are designed. Every model should be optimized to ensure the best possible outcome.

The best way to start evaluating a model is to score it with a test table that contains known values of the prediction target and that was not used during the creation of the model. The model will return predictions based on that data that are comparable to the known values of the target. This approach provides insight into how well the model can be expected to perform after it is deployed.

At least one test table is recommended. Update the model based on the results of the first test. Use additional test sets to evaulate subsequent iterations. This will help identify gaps and prevent overfitting, which may occur when a statistical model describes random errors instead of the underlying relationship between inputs and the target.

.. _model-data-prediction-quality:

Prediction Quality
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_api/function_score_predictions.rst

Predictions are evaulated using one (or more) of the following metrics:

**classification accuracy (ACC)**
   .. include:: ../../includes_terms/term_metric_acc.rst

**area under the curve (AUC)**
   .. include:: ../../includes_terms/term_metric_auc.rst

**F1 score (F1)**
   .. include:: ../../includes_terms/term_metric_f1.rst

**logistic loss (LOGLOSS)**
   .. include:: ../../includes_terms/term_metric_logloss.rst

**mean absolute error (MAE)**
   .. include:: ../../includes_terms/term_metric_mae.rst

**mean absolute percentage error (MAPE)**
   .. include:: ../../includes_terms/term_metric_mape.rst

**precision (PREC)**
   .. include:: ../../includes_terms/term_metric_prec.rst

**recall (REC)**
   .. include:: ../../includes_terms/term_metric_rec.rst

**root mean squared error (RMSE)**
   .. include:: ../../includes_terms/term_metric_rmse.rst

.. note:: Many factors influence the decision about which metric is appropriate to use in a model. Generally, using RMSE or MAPE for linear regression models and AUC for binary classification models is a good starting point.


.. _model-data-different-inputs:

Different Inputs
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_api/function_measure_input_importance.rst

For example, a model predicts sales and there is a column that contains data only after a sale occurs. This column should not be used as an input because this type of situation indicates information leakage:

* The column predicts sales with 100% accuracy
* The presence of any value corresponds with a sale
* The column's value doesn't exist until after a sale occurs

Use the following report types to measure prediction accuracy for a model:

**one-in report**
   .. include:: ../../includes_terms/term_report_one_in.rst

**one-out report**
   .. include:: ../../includes_terms/term_report_one_out.rst

**forward report**
   .. include:: ../../includes_terms/term_report_forward.rst

**backward report**
   .. include:: ../../includes_terms/term_report_backward.rst

.. _model-data-learning-curves:

Learning Curves
++++++++++++++++++++++++++++++++++++++++++++++++++
Use the metrics that are available to the :ref:`api-measure-input-importance` function to identify a model's learning curve. Generally, the more data that is used to build a model, the more accurate its predictions will be. However, neither data nor the computational time necessary to process it is free.

.. include:: ../../includes_terms/term_model_learning_curve.rst


.. _model-data-score-curves:

Score Curves
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_api/function_measure_score_curves.rst


.. _model-data-confusion-matrix:

Confusion Matrix
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_api/function_measure_confusion_matrix.rst


.. _model-data-deploy-model:

Deploy the Model
--------------------------------------------------
.. include:: ../../includes_api/function_predict.rst

After model quality has improved to a sufficient level, obtain predictions from new data sources, and then integrate those into the existing tools and workflows. The predictions produced by the model can be translated into recommended actions to improve the business outcome that is the goal of the model. For example, a model that predicts price can determine when to buy or sell and a model that predicts fraudulent transactions can determine if a credit card account should be frozen.

Predictions should be evaluated on an ongoing basis to ensure they continue to produce the desired results. Update the model as necessary. To determine how to update the model, run comparitive statistics or conduct A/B testing in which the outcomes are compared against the performance of groups that are controlled to those that are uncontrolled.









Build a Simple Model
==================================================

.. TODO: Migrated from old docs, formatted, not verified against current platform/engines.

.. include:: ../../includes_models/tutorial_simple_model.rst

Tutorial Requirements
--------------------------------------------------
.. include:: ../../includes_models/tutorial_simple_model_requirements.rst

.. TODO: Need a tutorial for "Create Named Connections"

Prepare the Raw Data
--------------------------------------------------
.. include:: ../../includes_models/tutorial_simple_model_prepare_data.rst

Run Interactive Mode
--------------------------------------------------
.. include:: ../../includes_models/tutorial_simple_model_interactive_mode.rst

Load Data
--------------------------------------------------
.. include:: ../../includes_models/tutorial_simple_model_load_data.rst

Remove Data
--------------------------------------------------
.. include:: ../../includes_models/tutorial_simple_model_remove_data.rst

Review Statistics
--------------------------------------------------
.. include:: ../../includes_models/tutorial_simple_model_review_statistics.rst

Learn the Initial Model
--------------------------------------------------
.. include:: ../../includes_models/tutorial_simple_model_learn.rst

Evaluate the Initial Model
--------------------------------------------------
.. include:: ../../includes_models/tutorial_simple_model_evaluate.rst

Improve the Model
--------------------------------------------------
.. include:: ../../includes_models/tutorial_simple_model_improve.rst


