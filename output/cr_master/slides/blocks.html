<!DOCTYPE html>


<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title align="left">Blocks Tutorial &mdash; Slide Decks</title>

    

    
<link rel="stylesheet" href="_static/css/reveal.min.css">
<link rel="stylesheet" href="_static/css/theme/default.css" id="theme">
<link rel="stylesheet" href="_static/lib/css/zenburn.css">
<link rel="stylesheet" href="_static/revealjs.css" type="text/css" />

    <!-- If the query includes 'print-pdf', include the PDF print sheet -->
    <script>
        if(window.location.search.match(/print-pdf/gi)) {
            var link = document.createElement('link');
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = '_static/css/print/pdf.css';
            document.getElementsByTagName('head')[0].appendChild(link);
        }
    </script>

    <!--[if lt IE 9]>
    <script src="_static/lib/js/html5shiv.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="reveal">
      <div class="slides" align="left">
        
  <div class="section" id="blocks-tutorial">
<h1>Blocks Tutorial<a class="headerlink" href="#blocks-tutorial" title="Permalink to this headline">¶</a></h1>
<section>
<section class="first" data-transition="none">
<img alt="_images/slides_blocks_splash.svg" class="first last" src="_images/slides_blocks_splash.svg" /></section>
<section data-transition="none">
<h2>About Blocks</h2>
<p class="first">Blocks define how data is processed by the engine, including how it is parsed, standardized, transformed, aggregated, and otherwise prepared for modeling.</p>
<p>Blocks are designed around these concepts:</p>
<ul class="last simple">
<li>Blocks are the primary interface to the engine pipeline</li>
<li>Blocks have consistent input and output</li>
<li>Blocks interact with the engine pipeline primarily via the configuration file</li>
<li>Blocks are defined as a group, but are run one at a time in a specified order</li>
</ul>
</section>
<section data-transition="none">
<h2>Blocks and Columns</h2>
<p class="first">Each block defines its own input and output behaviors. Within this tutorial, the block for parsing data into the engine is unique; the other blocks are the same:</p>
<ul class="simple">
<li>Each block inputs a Spark DataFrame object. Input columns are assumed to be identical to the columns that were output from the previous block.</li>
<li>Each block outputs a Spark DataFrame object. Output columns are assumed to be identical to the columns that were input to this block, unless otherwise declared.</li>
</ul>
<p class="last">The expectation that a block will input and output consistent sets of tabular data is sometimes referred to as a &#8220;column contract&#8221; and is visible in the configuration file.</p>
</section>
<section data-transition="none">
<h2>Block Graphs</h2>
<p class="first">A block graph is a directed acyclic graph that defines how the engine processes data through the pipeline. A typical block graph contains multiple blocks, each configured to process data in a specific order.</p>
<img alt="_images/slides_blocks_order.svg" src="_images/slides_blocks_order.svg" /><p class="last">In this tutorial, data is ingested into the engine (1), is parsed into tabular data (2), is cleaned up for IP address data (3) and for user data (4), is transformed (5), and then built into two tables (6).</p>
</section>
<section data-transition="none">
<h2>Block Graph Configuration</h2>
<p class="first">A block graph, and all of the individual blocks in that block graph, are defined in a dedicated location in the configuration file for the engine grouped under <code class="docutils literal"><span class="pre">block_graphs</span></code>:</p>
<div class="highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">block_graphs</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">tutorial</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">blocks</span><span class="p p-Indicator">:</span>
      <span class="l l-Scalar l-Scalar-Plain">&lt;block_name&gt;</span><span class="p p-Indicator">:</span>
        <span class="l l-Scalar l-Scalar-Plain">class_name</span><span class="p p-Indicator">:</span>
          <span class="l l-Scalar l-Scalar-Plain">symbol</span><span class="p p-Indicator">:</span> <span class="s">&#39;BlockName&#39;</span>
        <span class="l l-Scalar l-Scalar-Plain">columns</span><span class="p p-Indicator">:</span>
          <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">column_name</span>
            <span class="l l-Scalar l-Scalar-Plain">type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">str</span>
          <span class="l l-Scalar l-Scalar-Plain">...</span>
</pre></div>
</div>
<p class="last">The configuration for the individual blocks in the block graph are covered later on in this tutorial.</p>
</section>
<section data-transition="none">
<h2>Steps in this Tutorial</h2>
<p class="first">This tutorial walks through the following steps:</p>
<ol class="last arabic simple">
<li>Verify data source configuration</li>
<li>Parse proxy data into a tabular format</li>
<li>Separate parsed from un-parsed</li>
<li>Clean up IP address data</li>
<li>Clean up user data</li>
<li>Add columns</li>
<li>Group by source IP address data</li>
<li>For each unique source IP address, sum traffic to and from the source IP address</li>
<li>For each unique source IP address, create lists for destination IP addresses and users</li>
<li>Verify block dependencies within the block graph</li>
<li>Verify output for parsed and un-parsed data</li>
</ol>
</section>
<section data-transition="none">
<h2>Step One: Verify Data Source</h2>
<p class="first">The <code class="docutils literal"><span class="pre">sources</span></code> section of the configuration file declares the location from which data will be ingested, and then the name of the block graph responsible for processing that data:</p>
<div class="highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">sources</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">proxy</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">table_type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">ProxyTable</span>
    <span class="l l-Scalar l-Scalar-Plain">path</span><span class="p p-Indicator">:</span> <span class="s">&#39;/path/to/proxy/data/&#39;</span>
    <span class="l l-Scalar l-Scalar-Plain">parse</span><span class="p p-Indicator">:</span>
      <span class="l l-Scalar l-Scalar-Plain">default_validation</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">True</span>
      <span class="l l-Scalar l-Scalar-Plain">block_name</span><span class="p p-Indicator">:</span> <span class="s">&#39;tutorial&#39;</span>
</pre></div>
</div>
<p class="last">where the <code class="docutils literal"><span class="pre">block_name</span></code> value tells the engine to run the <code class="docutils literal"><span class="pre">tutorial</span></code> block graph while processing Proxy data. The engine will look under <code class="docutils literal"><span class="pre">block_graphs</span></code> to discover the <code class="docutils literal"><span class="pre">tutorial</span></code> block graph, identify the blocks that are defined in that block graph, and then run the blocks in the defined order.</p>
</section>
<section data-transition="none">
<h2>Step Two: Parse Proxy Data</h2>
<p class="first">Proxy log file data must be converted to tabular data:</p>
<ul class="simple">
<li>A schema file tells the engine how to parse the ingested log file</li>
<li>A path declares the location to which output is saved</li>
<li>A date format to use when applying timestamps in the parsed data</li>
<li>Output tables that split successfully parsed data from unsuccessfully parsed data</li>
</ul>
<p>The configuration for a <code class="docutils literal"><span class="pre">ParseBlock</span></code> is shown here:</p>
<div class="last highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">block_graphs</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">tutorial</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">blocks</span><span class="p p-Indicator">:</span>
      <span class="l l-Scalar l-Scalar-Plain">parse_proxy</span><span class="p p-Indicator">:</span>
        <span class="l l-Scalar l-Scalar-Plain">class_name</span><span class="p p-Indicator">:</span>
          <span class="l l-Scalar l-Scalar-Plain">symbol</span><span class="p p-Indicator">:</span> <span class="s">&#39;ParseBlock&#39;</span>
        <span class="l l-Scalar l-Scalar-Plain">schema</span><span class="p p-Indicator">:</span> <span class="s">&#39;/path/to/schema.yml&#39;</span>
        <span class="l l-Scalar l-Scalar-Plain">date_format</span><span class="p p-Indicator">:</span> <span class="s">&#39;%Y%m%d&#39;</span>
        <span class="l l-Scalar l-Scalar-Plain">path</span><span class="p p-Indicator">:</span> <span class="s">&#39;/path/to/output/location&#39;</span>
    <span class="l l-Scalar l-Scalar-Plain">...</span>
</pre></div>
</div>
</section>
<section data-transition="none">
<h2>Step Three: Split Parsed, Unparsed</h2>
<p class="first">Not all proxy data is parsable. This can be for many reasons and can be mitigated and improved over time. But for each day&#8217;s processing, parsable data must be separated from un-parsable data. Use the <code class="docutils literal"><span class="pre">outputs</span></code> section to declare the output tables for these two groups of data:</p>
<div class="last highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">block_graphs</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">tutorial</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">blocks</span><span class="p p-Indicator">:</span>
      <span class="l l-Scalar l-Scalar-Plain">parse_proxy</span><span class="p p-Indicator">:</span>
        <span class="l l-Scalar l-Scalar-Plain">...</span>
    <span class="l l-Scalar l-Scalar-Plain">outputs</span><span class="p p-Indicator">:</span>
      <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">block_name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">parse_proxy</span>
        <span class="l l-Scalar l-Scalar-Plain">frame_key</span><span class="p p-Indicator">:</span> <span class="p p-Indicator">{</span><span class="s">&#39;stage&#39;</span><span class="p p-Indicator">:</span> <span class="s">&#39;parsed&#39;</span><span class="p p-Indicator">}</span>
      <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">block_name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">parse_proxy</span>
        <span class="l l-Scalar l-Scalar-Plain">frame_key</span><span class="p p-Indicator">:</span> <span class="p p-Indicator">{</span><span class="s">&#39;stage&#39;</span><span class="p p-Indicator">:</span> <span class="s">&#39;parsed_error&#39;</span><span class="p p-Indicator">}</span>
</pre></div>
</div>
</section>
<section data-transition="none">
<h2>Step Four: Clean Up IP Address Data</h2>
<p class="first">Raw IP address data should be cleaned up so that all of the source IP address and all of the destination IP address are separated into unique columms. This step uses the <code class="docutils literal"><span class="pre">StandardizeIPAddressBlock</span></code> and the configuration is similar to:</p>
<div class="last highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">clean_ips</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">class_name</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">symbol</span><span class="p p-Indicator">:</span> <span class="s">&#39;StandardizeIPAddressBlock&#39;</span>
  <span class="l l-Scalar l-Scalar-Plain">columns</span><span class="p p-Indicator">:</span>
    <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">source_ip</span>
      <span class="l l-Scalar l-Scalar-Plain">type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">str</span>
    <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">dest_ip</span>
      <span class="l l-Scalar l-Scalar-Plain">type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">str</span>
</pre></div>
</div>
</section>
<section data-transition="none">
<h2>Step Five: Clean Up User Data</h2>
<p class="first">Raw user data should be cleaned up so that only user data is in the <code class="docutils literal"><span class="pre">user</span></code> column. If the user data contains a domain, that information is stored in a new column named <code class="docutils literal"><span class="pre">user_domain</span></code>:</p>
<div class="last highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">clean_user</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">class_name</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">symbol</span><span class="p p-Indicator">:</span> <span class="s">&#39;StandardizeUsernameBlock&#39;</span>
  <span class="l l-Scalar l-Scalar-Plain">columns</span><span class="p p-Indicator">:</span>
    <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">user</span>
      <span class="l l-Scalar l-Scalar-Plain">type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">str</span>
  <span class="l l-Scalar l-Scalar-Plain">username_contains_domain</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">True</span>
  <span class="l l-Scalar l-Scalar-Plain">domain_suffix</span><span class="p p-Indicator">:</span> <span class="s">&#39;_domain&#39;</span>
</pre></div>
</div>
</section>
<section data-transition="none">
<h2>Step Six: Add Columns</h2>
<p class="first">Data that is transformed in certain ways is more useful. Add the following columns to the <code class="docutils literal"><span class="pre">columns</span></code> list: <code class="docutils literal"><span class="pre">source_ip</span></code>, <code class="docutils literal"><span class="pre">dest_ip</span></code>, <code class="docutils literal"><span class="pre">bytes_in</span></code>, <code class="docutils literal"><span class="pre">bytes_out</span></code>, and <code class="docutils literal"><span class="pre">user</span></code> using the <code class="docutils literal"><span class="pre">name</span></code>/<code class="docutils literal"><span class="pre">type</span></code> pair for each column:</p>
<div class="highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">aggregate_proxy</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">class_name</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">symbol</span><span class="p p-Indicator">:</span> <span class="s">&#39;AggregateBlock&#39;</span>
  <span class="l l-Scalar l-Scalar-Plain">group_by</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">source_ip</span>
  <span class="l l-Scalar l-Scalar-Plain">columns</span><span class="p p-Indicator">:</span>
    <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">source_ip</span>
      <span class="l l-Scalar l-Scalar-Plain">type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">str</span>
    <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">dest_ip</span>
      <span class="l l-Scalar l-Scalar-Plain">type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">str</span>
    <span class="l l-Scalar l-Scalar-Plain">...</span>
</pre></div>
</div>
<p class="last">and so on. (All of the column types should be <code class="docutils literal"><span class="pre">str</span></code>.)</p>
</section>
<section data-transition="none">
<h2>Step Seven: Group by Source IP</h2>
<p class="first">The <code class="docutils literal"><span class="pre">group_by</span></code> column tells the engine to apply aggregations from the context of this column. Set <code class="docutils literal"><span class="pre">group_by</span></code> to <code class="docutils literal"><span class="pre">source_ip</span></code>:</p>
<div class="highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">aggregate_proxy</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">class_name</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">symbol</span><span class="p p-Indicator">:</span> <span class="s">&#39;AggregateBlock&#39;</span>
  <span class="l l-Scalar l-Scalar-Plain">group_by</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">source_ip</span>
  <span class="l l-Scalar l-Scalar-Plain">columns</span><span class="p p-Indicator">:</span>
    <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">source_ip</span>
      <span class="l l-Scalar l-Scalar-Plain">type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">str</span>
    <span class="l l-Scalar l-Scalar-Plain">...</span>
</pre></div>
</div>
<p class="last">For each unique source IP address, the results of each declared operation will be added to the row output.</p>
</section>
<section data-transition="none">
<h2>Step Eight: Sum for Bytes In / Out</h2>
<p class="first">Next, aggregate the <code class="docutils literal"><span class="pre">bytes_in</span></code> and <code class="docutils literal"><span class="pre">bytes_out</span></code> columns to sum their values by using the <code class="docutils literal"><span class="pre">ops</span></code> grouping to declare the calculation (<code class="docutils literal"><span class="pre">sum</span></code>), and then list the columns for which this calculation is performed:</p>
<div class="last highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">aggregate_proxy</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">class_name</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">symbol</span><span class="p p-Indicator">:</span> <span class="s">&#39;AggregateBlock&#39;</span>
  <span class="l l-Scalar l-Scalar-Plain">group_by</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">source_ip</span>
  <span class="l l-Scalar l-Scalar-Plain">columns</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">...</span>
    <span class="l l-Scalar l-Scalar-Plain">- name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">bytes_in</span>
      <span class="l l-Scalar l-Scalar-Plain">type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">str</span>
    <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">bytes_out</span>
      <span class="l l-Scalar l-Scalar-Plain">type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">str</span>
    <span class="l l-Scalar l-Scalar-Plain">...</span>
  <span class="l l-Scalar l-Scalar-Plain">ops</span><span class="p p-Indicator">:</span>
    <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">sum</span>
      <span class="l l-Scalar l-Scalar-Plain">columns</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">bytes_in, bytes_out</span>
</pre></div>
</div>
</section>
<section data-transition="none">
<h2>Step Nine: Create Lists</h2>
<p class="first">Next, aggregate the <code class="docutils literal"><span class="pre">dest_ip</span></code> and <code class="docutils literal"><span class="pre">user</span></code> columns to create a list that does not exceed 255 items:</p>
<div class="last highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">aggregate_proxy</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">class_name</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">symbol</span><span class="p p-Indicator">:</span> <span class="s">&#39;AggregateBlock&#39;</span>
  <span class="l l-Scalar l-Scalar-Plain">group_by</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">source_ip</span>
  <span class="l l-Scalar l-Scalar-Plain">columns</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">...</span>
    <span class="l l-Scalar l-Scalar-Plain">- name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">dest_ip</span>
      <span class="l l-Scalar l-Scalar-Plain">type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">str</span>
    <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">user</span>
      <span class="l l-Scalar l-Scalar-Plain">type</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">str</span>
    <span class="l l-Scalar l-Scalar-Plain">...</span>
  <span class="l l-Scalar l-Scalar-Plain">ops</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">...</span>
    <span class="l l-Scalar l-Scalar-Plain">- name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">list</span>
      <span class="l l-Scalar l-Scalar-Plain">columns</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">dest_ip, user</span>
      <span class="l l-Scalar l-Scalar-Plain">limit</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">255</span>
</pre></div>
</div>
</section>
<section data-transition="none">
<h2>Step Ten: Verify Dependencies</h2>
<p class="first">The desired order of processing for successfully parsed data is 1) parse_proxy, 2) clean_ips, 3) clean_users, 4) aggregate_proxy, after which this data is added to the <code class="docutils literal"><span class="pre">parsed</span></code> output table. After verifying the correct order, ensure the configuration matches:</p>
<div class="highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">block_graphs</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">tutorial</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">blocks</span><span class="p p-Indicator">:</span>
      <span class="l l-Scalar l-Scalar-Plain">...</span>
    <span class="l l-Scalar l-Scalar-Plain">depends</span><span class="p p-Indicator">:</span>
      <span class="l l-Scalar l-Scalar-Plain">clean_ips</span><span class="p p-Indicator">:</span> <span class="p p-Indicator">[</span><span class="s">&#39;parse_proxy&#39;</span><span class="p p-Indicator">]</span>
      <span class="l l-Scalar l-Scalar-Plain">clean_user</span><span class="p p-Indicator">:</span> <span class="p p-Indicator">[</span><span class="s">&#39;clean_ips&#39;</span><span class="p p-Indicator">]</span>
      <span class="l l-Scalar l-Scalar-Plain">aggregate_proxy</span><span class="p p-Indicator">:</span> <span class="p p-Indicator">[</span><span class="s">&#39;clean_user&#39;</span><span class="p p-Indicator">]</span>
</pre></div>
</div>
<p class="last">where each item under <code class="docutils literal"><span class="pre">depends</span></code> contains a block name followed by the block name for which that block has a dependency. For example: the <code class="docutils literal"><span class="pre">aggregate_proxy</span></code> block depends on the <code class="docutils literal"><span class="pre">clean_user</span></code> block, which depends on the <code class="docutils literal"><span class="pre">clean_ips</span></code> block, which depends on the <code class="docutils literal"><span class="pre">parse_proxy</span></code> block.</p>
</section>
<section data-transition="none">
<h2>Step Eleven: Verify Output</h2>
<p class="first">Update the <code class="docutils literal"><span class="pre">outputs</span></code> configuration to specify the final block in the block graph from which the block graph output is generated:</p>
<div class="highlight-yaml"><div class="highlight"><pre><span></span><span class="l l-Scalar l-Scalar-Plain">block_graphs</span><span class="p p-Indicator">:</span>
  <span class="l l-Scalar l-Scalar-Plain">tutorial</span><span class="p p-Indicator">:</span>
    <span class="l l-Scalar l-Scalar-Plain">blocks</span><span class="p p-Indicator">:</span>
      <span class="l l-Scalar l-Scalar-Plain">parse_proxy</span><span class="p p-Indicator">:</span>
        <span class="l l-Scalar l-Scalar-Plain">...</span>
    <span class="l l-Scalar l-Scalar-Plain">outputs</span><span class="p p-Indicator">:</span>
      <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">block_name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">aggregate_proxy</span>
        <span class="l l-Scalar l-Scalar-Plain">frame_key</span><span class="p p-Indicator">:</span> <span class="p p-Indicator">{</span><span class="s">&#39;stage&#39;</span><span class="p p-Indicator">:</span> <span class="s">&#39;parsed&#39;</span><span class="p p-Indicator">}</span>
      <span class="p p-Indicator">-</span> <span class="l l-Scalar l-Scalar-Plain">block_name</span><span class="p p-Indicator">:</span> <span class="l l-Scalar l-Scalar-Plain">parse_proxy</span>
        <span class="l l-Scalar l-Scalar-Plain">frame_key</span><span class="p p-Indicator">:</span> <span class="p p-Indicator">{</span><span class="s">&#39;stage&#39;</span><span class="p p-Indicator">:</span> <span class="s">&#39;parsed_error&#39;</span><span class="p p-Indicator">}</span>
</pre></div>
</div>
<p>Parsed data takes two paths:</p>
<ul class="last simple">
<li>Unsuccessfully parsed data is sent to <code class="docutils literal"><span class="pre">parsed_error</span></code> output</li>
<li>Successfully parsed data is sent to the <code class="docutils literal"><span class="pre">parsed</span></code> output, from which processing&#8212;additional parse, standardize, transform, and aggregate steps&#8212;can be run</li>
</ul>
</section>
<section data-transition="none">
<h2>More Information</h2>
<p class="first">For more information about this tutorial, blocks, and using blocks to build the engine pipeline, see the following links:</p>
<ul class="last simple">
<li><a class="reference external" href="../start_here.html#full-yaml-example">Full YAML configuration example</a></li>
<li><a class="reference external" href="../blocks.html">Complete Blocks reference</a></li>
</ul>
</section>
<section data-transition="none">
<img alt="_images/slides_blocks_parse.svg" class="first last" src="_images/slides_blocks_parse.svg" /></section>
<section data-transition="none">
<img alt="_images/slides_blocks_ips.svg" class="first last" src="_images/slides_blocks_ips.svg" /></section>
<section data-transition="none">
<img alt="_images/slides_blocks_users.svg" class="first last" src="_images/slides_blocks_users.svg" /></section>
<section data-transition="none">
<img alt="_images/slides_blocks_aggregate.svg" class="first last" src="_images/slides_blocks_aggregate.svg" /></section>
<section data-transition="none">
<img alt="_images/slides_blocks_all.svg" class="first last" src="_images/slides_blocks_all.svg" /></section>
<section class="last" data-transition="none">
<img alt="_images/slides_blocks_order.svg" class="first last" src="_images/slides_blocks_order.svg" /></section>
</section>
</div>


      </div>
    </div>
    
<script src="_static/js/jquery.min.js"></script>
<script src="_static/lib/js/head.min.js"></script>
<script src="_static/js/reveal.min.js"></script>
    <script>

      // change DOM for reveal.js
      $("div.section h1").remove();
      var content = $("div.section").html();
      $("div.section").remove();
      $("div.slides").html(content);

      Reveal.initialize({
        width: 1440,
        height: 900,

        margin: 0.3,

        minScale: 0.2,
        maxScale: 1.0,

        controls: true,
        progress: false,
        history: false,
        center: false,

        keyboard : true,
        overview: false,
        touch: true,
        loop: false,
        rtl: false,
        fragments: true,

        autoSlide: 0,
        mouseWheel: false,
        rollingLinks: true,
        previewLinks: false,

        transitionSpeed: "default",
        backgroundTransition: "default",

        slideNumber: false,
        embedded: false,
        autoSlideStoppable: true,
        hideAddressBar: true,

        parallaxBackgroundImage: "",
        parallaxBackgroundSize: "",

        focusBodyOnPageVisiblityChange: true,

        viewDistance: 3,

        theme: Reveal.getQueryHash().theme || "slides",
        transition: Reveal.getQueryHash().transition || "default",

        

        dependencies: [
           { src: '_static/lib/js/classList.js', condition: function() { return !document.body.classList; } },
           { src: '_static/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
           { src: '_static/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
           { src: '_static/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
           { src: '_static/plugin/zoom-js/zoom.js', async: true, condition: function() { return !!document.body.classList; } },
           { src: '_static/plugin/notes/notes.js', async: true, condition: function() { return !!document.body.classList; } }
        ]
      });
    </script>
    
  </body>
</html>