.. 
.. xxxxx
.. 

==================================================
Named Connections
==================================================

.. include:: ../../includes/named_connections.rst


Add Named Connections
==================================================
Named connections may be added for Amazon S3 storage clusters and Hadoop Distributed File System (HDFS).

Amazon S3
--------------------------------------------------
.. include:: ../../includes/cmd_cr_connect_add_amazon_s3.rst


HDFS
--------------------------------------------------
.. include:: ../../includes/cmd_cr_connect_add_hdfs.rst


Verify Named Connections
==================================================
Named connections should be verified:

* Prior to adding a named connection to check if there is one already defined for that storage cluster
* After removing a named connection, to verify that connection was removed from the list of named connections

**Amazon S3 storage**

When the named connection is configured for Amazon AWS:

.. include:: ../../includes/cmd_cr_connect_list_amazon_s3.rst

**HDFS storage**

When the named connection is configured for HDFS:

.. include:: ../../includes/cmd_cr_connect_list_hdfs.rst


Use Existing Connections
==================================================
Connections to a storage cluster that already have a named connection may omit extra type-specific parameters. For example, an AWS S3 storage cluster with an existing named connection named ``aws`` does not require the ``bucket``, ``aws_access_key_id``, and ``aws_secret_access_key`` options to be specified.

**Use an existing named connection for an AWS S3 storage cluster**

.. code-block:: bash

   $ cr connect add --name='aws' --type='s3'
