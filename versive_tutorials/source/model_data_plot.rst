.. 
.. xxxxx
.. 

==================================================
Plotting Functions
==================================================
.. TODO: Migrated from old docs, formatted, not verified against current platform/engines.

This topic shows how to use a series of plotting functions in the |risp_full| that allow for easy visualization of data and results. There are four plotting functions:

* :ref:`model-data-plot-bar`
* :ref:`model-data-plot-histogram`
* :ref:`model-data-plot-line`
* :ref:`model-data-plot-scatter`
 
Each of these functions take in a particular type of table produced by functions such as ``get_example_rows()``, ``score_predictions()``, and ``measure_learning_curve()``. The ``x_column`` parameter is used by all four functions and provides the column for the x-axis data. All of the functions except plot_histogram take in a ``y_columns`` parameter which can provide one (or more) y columns for the y-axis values. For both the ``x_column`` and ``y_columns``, it is acceptable to pass in a zero-based index for the columns instead of the column name. Lastly, the functions provide optional inputs for setting a ``title``, ``x_label``, and ``y_label`` string to be included in the image. The output to each of these functions is a reference to the plot object, which may be passed to the ``save_plot()`` function in |risp|, which saves a copy of the image in PNG format.


.. _model-data-plot-bar:

Bar Plot
==================================================
A bar plot uses tables produced by other functions, and then plots the y-axis against the x-axis. The following example creates a model, and then does some predictions, after which they are scored and grouped by condition. The bar plot shows multiple y columns:

.. code-block:: python

   train, tune, test = risp.split_table(table=dt)
   model = risp.learn_model(target='SalePrice',
                            model_type='LINEAR_REGRESSION',
                            train_table=train,
                            tune_table=tune)
   preds = risp.predict(model=model, table=test)
   score_by_condition = risp.score_predictions(table=preds,
                                               metrics=["RMSE", "MAE"],
                                               group_by_columns=['Condition'])
   risp.plot_bar(table=score_by_condition,
                 x_column="(Condition)",
                 y_columns=["RMSE", "MAE"],
                 title="Prediction Scores",
                 x_label="House Condition",
                 y_label="Score")

and displays similar to:

.. image:: ../../images/model_data_plot_bar.svg
   :width: 600 px
   :align: center


.. _model-data-plot-histogram:

Histogram Plot
==================================================
A histogram plot represents the distribution of data. The following example creates a model, and then does some predictions, after which they are scored and grouped by condition. Inputs are not provided to the y axis. A column index instead of a name.

.. code-block:: python
 
   rows_to_plot = risp.get_example_rows(table=dt,
                                        count=1000)
   risp.plot_histogram(table=rows_to_plot,
                       column=2,
                       title="Price Histogram",
                       x_label="Price Buckets")

and displays similar to:

.. image:: ../../images/model_data_plot_histogram.svg
   :width: 600 px
   :align: center


.. _model-data-plot-line:

Line Plot
==================================================
A line plot creates a chart with the y-axis columns plotted against the x-axis. The following example measures a learning curve for a model to visualize the decrease in a training error as the size of the training set increases.

.. code-block:: python 

   learning_curve = risp.measure_learning_curve(target="SalePrice",
                                                model_type='LINEAR_REGRESSION',
                                                train_table=train,
                                                tune_table=tune,
                                                test_table=test)
   risp.plot_line(table=learning_curve,
                  x_column="Training set size",
                  y_columns=["Score (RMSE)"],
                  title="RMSE vs Training Set Size",
                  x_label="Training Set Size",
                  y_label="RMSE Score")

and displays similar to:

.. image:: ../../images/model_data_plot_line.svg
   :width: 600 px
   :align: center


.. _model-data-plot-scatter:

Scatter Plot
==================================================
A scatter plot creates a chart that compares y-axis column data against x-axis column data. The following example extracts 10000 rows, and then uses the x and y columns and some labels to define the plot.

.. code-block:: python

   rows_to_plot = risp.get_example_rows(table=dt,
                                        count=10000)
   scatter_plot = risp.plot_scatter(table=rows_to_plot,
                                    x_column='SalePrice',
                                    y_columns=['SqFtTotLiving'],
                                    title="Price vs Sq. Ft.",
                                    x_label="Price ($)",
                                    y_label="Sq. Ft.")

   risp.save_plot(plot=scatter_plot,
                  path='myplots/housing/price_vs_sqft.png',
                  connection='s3')


and displays similar to:

.. image:: ../../images/model_data_plot_scatter.svg
   :width: 600 px
   :align: center
