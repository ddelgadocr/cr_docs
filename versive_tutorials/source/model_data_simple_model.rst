.. 
.. xxxxx
.. 

==================================================
Build a Simple Model
==================================================
.. TODO: Migrated from old docs, formatted, not verified against current platform/engines.

.. include:: ../../includes_models/tutorial_simple_model.rst

Tutorial Requirements
==================================================
.. include:: ../../includes_models/tutorial_simple_model_requirements.rst

.. TODO: Need a tutorial for "Create Named Connections"

Prepare the Raw Data
==================================================
.. include:: ../../includes_models/tutorial_simple_model_prepare_data.rst

Run Interactive Mode
==================================================
.. include:: ../../includes_models/tutorial_simple_model_interactive_mode.rst

Load Data
==================================================
.. include:: ../../includes_models/tutorial_simple_model_load_data.rst

Remove Data
==================================================
.. include:: ../../includes_models/tutorial_simple_model_remove_data.rst

Review Statistics
==================================================
.. include:: ../../includes_models/tutorial_simple_model_review_statistics.rst

Learn the Initial Model
==================================================
.. include:: ../../includes_models/tutorial_simple_model_learn.rst

Evaluate the Initial Model
==================================================
.. include:: ../../includes_models/tutorial_simple_model_evaluate.rst

Improve the Model
==================================================
.. include:: ../../includes_models/tutorial_simple_model_improve.rst



Advanced Models
==================================================
.. TODO: These are only in the tutorials section, at the moment.

This section contains more advanced model-building scripts that illustrate how to learn predictive models for various purposes by using publicly-available datasets:

* Advanced housing models
* Binary models
* Multiclass models



Advanced Housing
--------------------------------------------------

This example script expands upon the build your first model tutorial to illustrate more advanced model-building capabilities of RISP using housing sales data.

This example uses the same housing data used in the previous more detailed example. However, whereas that data was cleaned up and combined into a single table to simplify the introduction to building models with RISP , this example works from two files containing property sales and residential building data obtained from the `King County website <http://info.kingcounty.gov/assessor/DataDownload/default.aspx>`_ (Washington state).

This example will illustrate how to do the following tasks with the RISP API:

* How to join two different tables
* How to aggregate data by multiple measurements
* How to measure the learning curve
* How to do additional data transformations to build a more sophisticated, accurate model

Download the `Real Property Sales <http://your.kingcounty.gov/extranet/assessor/Real%20Property%20Sales.zip>`_ and `Residential Building <http://your.kingcounty.gov/extranet/assessor/Residential%20Building.zip>`_ files.

.. note:: In the following Python script example the ``base_path`` setting will need to be updated for the the absolute path to your storage location. This example assumes that the |versive| platform is being run locally on a single node, so if this data is stored elsewhere, such as a remote repository for multi-node processing, the ``storage`` value used for the default connection will also require updating.

.. code-block:: python

   storage='local'

   # Before initializing the cluster, this script defines several functions
   # that you can then call later in the script.

   import cr.risp_v1 as risp
   import datetime
   import dateutil

   # This function will select only properties that are improved residential
   # (property_class 3), are principally used for residential (principal_use 6),
   # and have land with a previously used building (property_type 3)


   def res_filter(property_class, principal_use, property_type):
       if (property_class == 8 and principal_use == 6 and property_type == 3):
           return 1
       else:
           return 0


   def first_building(*args):
       return_value = True
       for x in args:
           return_value = return_value and x == 1
       return return_value

   # This function returns properties with prices between 100,000 and 2
   # million dollars.


   def price_range(x):
       return x > 100000 and x < 2e6


   # This function contains several functions that return descriptive
   # statistics about the table that are then printed out.


   def print_table_stats(table):
       print "\nNumber of rows in table:\n"
       print str(risp.count_rows(table))
       print "\nList of column names in table:\n"
       print str(risp.get_column_names(table))
       print "\nExamples of rows from the data:\n"
       print str(risp.get_example_rows(table))
       print "\nColumn descriptions (or schema):\n"
       print str(risp.get_column_descriptions(table))
       print "\nSummary statistics for each column (this may take a few minutes):\n"
       print str(risp.summarize_table(table))


   risp.initialize_cluster(default_connection=storage)

   base_path = 'training_data/housing/'

   # Load sales table, checkpoint it, and print out statistics.

   sales = risp.load_tabular(path=base_path+'EXTR_RPSale.csv',
                             delimiter=',', has_headers=True)
   risp.checkpoint(sales, key='sales')
   print "\nStatistics for the sales table:\n"
   print_table_stats(sales)

   # Include only the type of properties specified by the res_filter function.

   sales = risp.select_rows(sales,
                            input_columns=['PropertyClass',
                                           'PrincipalUse',
                                           'PropertyType'],
                            udf=res_filter)

   risp.checkpoint(sales, key='sales')

   # Select only the columns we are interested in and then save a CVS locally.

   sales = risp.select_columns(sales, columns=['Major', 'Minor',
                                               'DocumentDate', 'SalePrice'])
   risp.checkpoint(sales, key='sales')

   risp.save_csv(sales, path=base_path+'housing_sales.csv', delimiter=',')
   print "Filtered sales table saved to %s.\n" % (base_path+'housing_sales.csv')

   # Include rows with SalePrice values over 100k and under 2M.

   sales = risp.select_rows(sales, input_columns='SalePrice',
                            udf=price_range)

   risp.checkpoint(sales, key='sales')
   print "\nStatistics for the filtered sales table:\n"
   print_table_stats(sales)

   # Load the property characteristics table.

   prop = risp.load_tabular(path=base_path+'EXTR_ResBldg.csv',
                            delimiter=',', has_headers=True)
   risp.checkpoint(prop, key='prop')
   print "\nStatistics for the property table:\n"
   print_table_stats(prop)

   # Create a subset of the data that includes only single-family homes for which
   # the values in the BldgNbr and NbrLivingUnits columns are 1.

   prop = risp.select_rows(prop, input_columns=['BldgNbr', 'NbrLivingUnits'],
                           udf=first_building)
   risp.checkpoint(prop, key='prop')
   print "\nStatistics for the property table:\n"
   print_table_stats(prop)

   # Analyze sales and property tables to determine which columns we should
   # join the two tables on. Providing None for left_columns and right_columns
   # analyzes all possible columns.

   print risp.relate(sales, prop, left_columns=None, right_columns=None)

   # Do an inner join on the Major and Minor columns and save this
   # as the master data file.

   master = risp.join(sales, prop,
                      key_columns=[('Major', 'Major'),
                                   ('Minor', 'Minor')],
                      join_type='INNER')
   risp.checkpoint(master, key='master')
   risp.save_csv(master, path=base_path+'housing_master.csv', delimiter=',')
   print "Master housing table saved to %s.\n" % (base_path+'housing_master.csv')
   print "\nMaster table column names:\n"
   print risp.get_column_names(master)

   # Count the frequencies of the Condition column values when grouped by
   # ZipCode and then view the results.

   aggprop = risp.count_frequencies(master, group_by_columns='ZipCode',
                                    value_columns='Condition')
   risp.checkpoint(aggprop, key='aggsales')
   print risp.summarize_table(aggprop)

   # Aggregate the data by DocumentDate and Zipcode and calculate the average
   # and maximum SalePrice.

   aggmaster = risp.aggregate(master,
                              group_by_columns=['DocumentDate', 'ZipCode'],
                              aggregate_ops=[('SalePrice', 'avg'),
                                             ('SalePrice', 'max')])
   risp.checkpoint(aggmaster, key='aggmaster')

   print risp.get_column_names(aggmaster)
   print risp.get_example_rows(aggmaster)

   # Join the new aggregate and frequency data to the master table.

   master = risp.join(master, aggprop, key_columns=['ZipCode'],
                      join_type='LEFT_OUTER')
   risp.checkpoint(master, key='master')
   master = risp.join(master, aggmaster,
                      key_columns=['DocumentDate', 'ZipCode'],
                      join_type='LEFT_OUTER')
   risp.checkpoint(master, key='master')
   print_table_stats(master)

   # Create an operation that will calculate the average sale price over
   # a 30-day window.

   op = risp.create_context_op(new_column='SalePriceByMonth',
                               input_column='SalePrice',
                               aggregate_op='mean',
                               start_offset=-30,
                               end_offset=-1,
                               unit='days',
                               include_start=True,
                               include_end=True)

   # Create a new column defined by the op SalePriceByMonth, which calculates
   # the average of the SalePrice over the last 30 days for a given ZIP code.

   final_table = risp.add_context_from(master, from_table=master,
                                       timestamp_columns=('DocumentDate'),
                                       context_ops=op,
                                       group_by_columns=['ZipCode'])
   risp.checkpoint(final_table, key='final_table')

   # Split the final table into train, tune, and test tables at default
   # proportions 60/20/20.

   train, tune, test = risp.split_table(final_table)

   risp.checkpoint(train, key='train')
   print risp.summarize_table(train)
   risp.checkpoint(tune, key='tune')
   print risp.summarize_table(tune)

   risp.checkpoint(test, key='test')

   # Learn a simple linear regression model.

   model = risp.learn_model(target='SalePrice',
                            model_type='LINEAR_REGRESSION',
                            train_table=train, tune_table=tune,
                            exploration='INTERACTIONS')
   risp.save_model(model, path=base_path+'house_model')

   print "\nModel saved to %s.\n" % (base_path+'house_model')
   # Make predictions using the test set and save the results.

   results = risp.predict(model, table=test, return_reasons=False)
   print "\nQuality of model:\n"
   print risp.score_predictions(results)
   risp.save_crtable(results, path=base_path+'house_results')

   # Score quality of model by ZIP code.

   quality_by_zip = risp.score_predictions(results, group_by_columns='ZipCode')
   quality_by_zip = list(quality_by_zip)
   quality_by_zip.sort(key=lambda x: x[1])
   print "\nTen most predictive ZIP codes:\n"
   print quality_by_zip[:10]

   # Measure the importance of any given input by using the one-in style
   # report.

   input_reports = risp.measure_input_importance(target='SalePrice',
                                                 model_type='LINEAR_REGRESSION',
                                                 train_table=train,
                                                 tune_table=tune,
                                                 test_table=test,
                                                 style='one-in',
                                                 metric='MAPE')
   print "\nInput importance report:\n", input_reports

   learning_curve = risp.measure_learning_curve(target='SalePrice',
                                                model_type='LINEAR_REGRESSION',
                                                train_table=train,
                                                tune_table=tune,
                                                test_table=test)
   print "\nLearning curve results:\n", learning_curve

   line = risp.plot_line(learning_curve, x_column=0, y_columns=1)
   risp.save_plot(line, path=base_path+'learning_curve.png')

   print "Learning curve graph saved to %s.\n" % (base_path+'learning_curve.png')
   


Binary Classification
--------------------------------------------------
This binary classification modeling example script uses anonymized customer relationship management (CRM) data from the KDD Cup 2009, a challenge hosted by SIGKDD, the Association for Computing Machinery’s Special Interest Group on Knowledge Discovery and Data Mining. The example illustrates how to do the following tasks with the RISP API:

* How to define targets for three different binary classification models to predict churn, appetency, and upselling
* How to define a series of commands so they are easily reusable
* How to checkpoint your work in a way that minimizes unnecessary computations

Download the raw data files from the `KDD Cup 2009 website <http://www.sigkdd.org/kdd-cup-2009-customer-relationship-prediction>`_ . Download the `small training <http://www.sigkdd.org/sites/default/files/kddcup/site/2009/files/orange_small_train.data.zip>`_ dataset and save it as orange_small_train.tsv. Then, download the `appentency <http://www.sigkdd.org/sites/default/files/kddcup/site/2009/files/orange_small_train_appetency.labels>`_, `churn <http://www.sigkdd.org/sites/default/files/kddcup/site/2009/files/orange_small_train_churn.labels>`_ , and `upselling <http://www.sigkdd.org/sites/default/files/kddcup/site/2009/files/orange_small_train_upselling.labels>`_ label files for the small datasets and save each of them as text files.

Open the training data file in Microsoft Excel or another spreadsheet program and add columns labeled "appetency_label", "churn_label", and "upselling_label" and then copy the content from each of the label files into those columns and save the file. If you encounter problems loading the file output from Excel, copy and paste the spreadsheet into a simple text editor and save the TSV file from there.

.. note:: In the following Python script example the ``base_path`` setting will need to be updated for the the absolute path to your storage location. This example assumes that the |versive| platform is being run locally on a single node, so if this data is stored elsewhere, such as a remote repository for multi-node processing, the ``storage`` value used for the default connection will also require updating.

.. code-block:: python

   storage='local'

   import cr.risp_v1 as risp

   # Define a function that contains several commands for learning and making
   # predictions with a model so that they can be easily reused for each run.

   def learn_and_predict(target, model_type, train_table, tune_table,
                         test_table):
       model = risp.learn_model(target, model_type, train_table,
                                tune_table)
       print model
       print risp.describe_model(model)
       results = risp.predict(model, test_table)
       print "\nPrediction table from model predicting %s:\n" % (target)

       print results
       scores = risp.score_predictions(results, metrics=['AUC',
                                                         'LOGLOSS',
                                                         'RMSE'])
       print "\nScores for this model:\n"
       print scores
       return scores

   risp.initialize_cluster(default_connection=storage)

   base_path = 'training_data/crm_orange'

   # Load the data and print out summary information so you can verify
   # everything is loaded correctly and looks as you expect it to.

   data = risp.load_tabular(path=base_path+'orange_small_train.tsv',
                            has_headers=True)
   risp.save_table_schema(table=data,
                          path=base_path)

   print risp.get_column_descriptions(data)
   print risp.summarize_table(data)
   print risp.get_column_names(data)
   print risp.count_rows(data)

   # Write a simple UDF to represent the classes as 0 and 1 instead of -1
   # and 1, as in the raw data files.    
                       
   def binary(x):
       if x==-1:
           return 0
       else:
           return 1
        
   data = risp.replace_columns(table=data, input_columns='appetency_label',
                                new_columns='appetency_label', udf=binary)
   data = risp.replace_columns(table=data, input_columns='churn_label',
                                new_columns='churn_label', udf=binary)
   data = risp.replace_columns(table=data, input_columns='upselling_label',
                                new_columns='upselling_label', udf=binary)

   # Split the data in preparation to learn the models.

   train, tune, test = risp.split_table(data, proportions=[0.6, 0.2, 0.2])

   # Learn models and make predictions on three different target columns.

   print "\nLearning the first model with all of the data and predicting \
   appetency with accuracy measured using AUC, LOGLOSS, and RMSE:\n"

   scores1 = learn_and_predict('appetency_label', 'BINARY_CLASSIFICATION',
                               train, tune, test)

   print "\nLearning the second model with all of the data and predicting \
   churn with accuracy measured using AUC, LOGLOSS, and RMSE:\n"

   scores2 = learn_and_predict('churn_label', 'BINARY_CLASSIFICATION',
                               train, tune, test)

   print "\nLearning the third model with all of the data and predicting \
   upselling with accuracy measured using AUC, LOGLOSS, and RMSE:\n"

   scores3 = learn_and_predict('upselling_label', 'BINARY_CLASSIFICATION',
                               train, tune, test)


   # Now work with only 10 inputs. Checkpoint the table because we will delete
   # the unused label columns for each run of the model.

   good_inputs = ['Var222', 'Var126', 'Var220', 'Var199', 'Var198', 'Var216',
                  'Var217', 'Var214', 'Var200', 'Var202', 'appetency_label',
                  'upselling_label', 'churn_label']
   data = risp.select_columns(data, columns=good_inputs)
   risp.checkpoint(data, key='full_data')


   # Learn and predict as before, but with only 10 inputs this time. Also
   # delete the other two label columns for each run.

   data = risp.delete_columns(data,
                              columns=['upselling_label', 'churn_label'])
   train, tune, test = risp.split_table(data, proportions=[0.6, 0.2, 0.2])

   print "\nLearning the fourth model with only 10 inputs and predicting \
   appetency with accuracy measured using AUC, LOGLOSS, and RMSE:\n"

   scores4 = learn_and_predict('appetency_label', 'BINARY_CLASSIFICATION',
                               train, tune, test)

   data = risp.load_checkpoint('full_data')
   data = risp.delete_columns(data,
                              columns=['upselling_label', 'appetency_label'])
   train, tune, test = risp.split_table(data, proportions=[0.6, 0.2, 0.2])

   print "\nLearning the fifth model with only 10 inputs and predicting \
   churn with accuracy measured using AUC, LOGLOSS, and RMSE:\n"

   scores5 = learn_and_predict('churn_label', 'BINARY_CLASSIFICATION',
                               train, tune, test)

   data = risp.load_checkpoint('full_data')
   data = risp.delete_columns(data,
                              columns=['churn_label', 'appetency_label'])
   train, tune, test = risp.split_table(data, proportions=[0.6, 0.2, 0.2])

   print "\nLearning the sixth model with only 10 inputs and predicting \
   upselling with accuracy measured using AUC, LOGLOSS, and RMSE:\n"

   scores6 = learn_and_predict('upselling_label', 'BINARY_CLASSIFICATION',
                               train, tune, test)

   # Show the AUC scores for all six models learned to compare accuracy.

   print "\nScores for predicting appetency based on all of the data:\n"
   print scores1
   print "\nScores for predicting churn based on all of the data:\n"
   print scores2
   print "\nScores for predicting upselling based on all of the data:\n"
   print scores3
   print "\nScores for predicting appetency based on 10 inputs of data:\n"
   print scores4
   print "\nScores for predicting churn based on 10 inputs of data:\n"
   print scores5
   print "\nScores for predicting upselling based on 10 inputs of data:\n"
   print scores6


Multiclass Classification
--------------------------------------------------
This multiclass classification model-building example uses anonymized customer relationship management (CRM) data from the KDD Cup 2009, a challenge hosted by SIGKDD, the Association for Computing Machinery’s Special Interest Group on Knowledge Discovery and Data Mining. The example illustrates how to do the following tasks with the RISP API:

* How to use a user-defined function (UDF) to define a target for a multiclass classification model
* How to learn models using different automated exploration modes and then compare the model quality

Download the raw data files from the `KDD Cup 2009 website <http://www.sigkdd.org/kdd-cup-2009-customer-relationship-prediction>`_ . Download the `small training <http://www.sigkdd.org/sites/default/files/kddcup/site/2009/files/orange_small_train.data.zip>`_ dataset and save it as orange_small_train.tsv. Then, download the `appentency <http://www.sigkdd.org/sites/default/files/kddcup/site/2009/files/orange_small_train_appetency.labels>`_, `churn <http://www.sigkdd.org/sites/default/files/kddcup/site/2009/files/orange_small_train_churn.labels>`_ , and `upselling <http://www.sigkdd.org/sites/default/files/kddcup/site/2009/files/orange_small_train_upselling.labels>`_ label files for the small datasets and save each of them as text files.

Open the training data file in Microsoft Excel or another spreadsheet program and add columns labeled "appetency_label", "churn_label", and "upselling_label" and then copy the content from each of the label files into those columns and save the file. If you encounter problems loading the file output from Excel, copy and paste the spreadsheet into a simple text editor and save the TSV file from there.

.. note:: In the following Python script example the ``base_path`` setting will need to be updated for the the absolute path to your storage location. This example assumes that the |versive| platform is being run locally on a single node, so if this data is stored elsewhere, such as a remote repository for multi-node processing, the ``storage`` value used for the default connection will also require updating.

.. code-block:: python

   storage='local'

   import cr.risp_v1 as risp
   import datetime
   import time
   import itertools

   risp.initialize_cluster(default_connection=storage)

   base_path = 'training_data/crm_orange/'

   # Load the data and print out summary information so you can verify
   # everything is loaded correctly and looks as you expect it to.

   raw_table = risp.load_tabular(path=base_path+'orange_small_train.tsv',
                                 has_headers=True)

   risp.save_table_schema(table=data,
                          path=base_path)

   print risp.get_column_descriptions(raw_table)
   print risp.summarize_table(raw_table)
   print risp.get_column_names(raw_table)
   print risp.count_rows(raw_table)

   # Write a simple UDF to represent the classes as 0 and 1 instead of -1
   # and 1, as in the raw data files.    
                       
   def binary(x):
       if x==-1:
           return 0
       else:
           return 1
        
   raw_table = risp.replace_columns(table=raw_table, input_columns='appetency_label',
                                    new_columns='appetency_label', udf=binary)
   raw_table = risp.replace_columns(table=raw_table, input_columns='churn_label',
                                    new_columns='churn_label', udf=binary)
   raw_table = risp.replace_columns(table=raw_table, input_columns='upselling_label',
                                    new_columns='upselling_label', udf=binary)
   risp.checkpoint(raw_table, key='raw_data')

   # Add new columns to the table based on UDF transformations of the raw data.

   table = risp.add_columns(raw_table, input_columns='churn_label',
                            new_columns='churn_new_label', udf=lambda x: x*2)

   table = risp.add_columns(table, input_columns='upselling_label',
                            new_columns='upselling_new_label', udf=lambda x: x*3)

   table = risp.add_columns(table, input_columns=['appetency_label',
                                                  'churn_new_label',
                                                  'upselling_new_label'],
                            new_columns='new_label', udf=lambda x, y, z: x+y+z)

   # Define a variable that lists the columns to retain in the table and then
   # select only those columns.

   cols_to_keep = ['Var1',
                   'Var7',
                   'Var24',
                   'Var35',
                   'Var44',
                   'Var216',
                   'Var228',
                   'Var229',
                   'Var230',
                   'new_label']

   small_data = risp.select_columns(table, columns=cols_to_keep)

   # Checkpoint the filtered table and split it in preparation to learn a model.

   risp.checkpoint(small_data, key='small_data')
   train, tune, test = risp.split_table(small_data, proportions=[0.6, 0.2, 0.2])

   # Checkpoint each of the split tables so you can easily return to them.

   risp.checkpoint(train, key='train')
   risp.checkpoint(tune, key='tune')
   risp.checkpoint(test, key='test')

   # Learn a model using basic mode for Automated Exploration of features.

   basic_model = risp.learn_model(target='new_label',
                                  model_type='MULTI_CLASSIFICATION',
                                  train_table=train, tune_table=tune,
                                  exploration='BASIC')

   print risp.describe_model(basic_model)

   # Make predictions using the test table and review a few predictions.

   basic_results = risp.predict(basic_model, table=test, return_reasons=True)
   risp.get_example_rows(basic_results)

   # Review the quality of the model's predictions.

   basic_scores = risp.score_predictions(basic_results, metrics=['LOGLOSS'],
                                         group_by_columns=None)
   print basic_scores

   # Complete the same process to learn a model using interactions mode for
   # Automated Exploration of features and compare it to the first model.

   inter_model = risp.learn_model(target='new_label',
                                  model_type='MULTI_CLASSIFICATION',
                                  train_table=train, tune_table=tune,
                                  exploration='INTERACTIONS')

   print risp.describe_model(inter_model)

   inter_results = risp.predict(inter_model, table=test, return_reasons=True)
   risp.get_example_rows(inter_results)

   inter_scores = risp.score_predictions(inter_results, metrics=['LOGLOSS'],
                                         group_by_columns=None)
   print inter_scores

   # Measure the learning curve to see how much data is necessary to learn
   # a good model.

   curve = risp.measure_learning_curve(target='new_label',
                                       model_type='MULTI_CLASSIFICATION',
                                       train_table=train, tune_table=tune,
                                       test_table=test, max_train_size=150000)

   line = risp.plot_line(curve, x_column=0, y_columns=1)
   risp.save_plot(line, path=base_path+'learning_curve.png')

   print "Learning curve graph saved to %s.\n" % (base_path+'learning_curve.png')

