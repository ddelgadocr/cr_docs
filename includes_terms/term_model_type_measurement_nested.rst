.. The contents of this file are included in multiple topics:
.. 
..    model_data
..    glossary
.. 

A measurement model may have a nested structure, which first predicts if a value is present, and then if a value is present predicts the expected measurement. A nested model improves the handling of targets that may have null values. For example, a measurement model that predicts the average entropy of URLs visited in a day will have null values on days where no web browsing occurred.