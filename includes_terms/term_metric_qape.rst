.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Quantile absolute percentage error (QAPE) is a metric that measures the absolute percentage difference between a prediction and the true value. This metric is appropriate when data is far from zero, shares a common scale, and may contain outliers. Use with binary classification or linear regression models.

.. TODO: QAPE and QAE were removed from the VSE because they were slow on large data sets; generally true. Update QAPE and QAE metrics in platform, eventually with some type of note/caveat.
