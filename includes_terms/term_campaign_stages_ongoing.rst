.. 
.. The contents of this file are included in the following topics:
.. 
..    describe_behaviors
..    stages
..    slide_decks/stages
.. 

After any exfiltration activity occurs, an adversary often continues to monitor data sources for updates. This requires continued observation of that data source that follows similar patterns as those in the |stage_recon|, |stage_collect|, and |stage_exfil| stages, but:

* May not involve the same amount of effort (because the network has already been mapped and interesting targets have been identified)
* Often collects the data into a previously-used location
* Likely follows a similar pattern to the first exfiltration activity
