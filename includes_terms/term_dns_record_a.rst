.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

An A record is the address record for a 32-bit IPv4 address. A records are associated with source and destination IP address and are discoverable from DNS data.
