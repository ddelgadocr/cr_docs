.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A count regression model predicts a non-negative integer as its target, such as predicting the number of web pages visited.
