.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A single execution element of a job defined by code to execute, start and end time, and input/output channels. Multiple sprockets are created for a given RDD.
