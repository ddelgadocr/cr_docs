.. The contents of this file are included in multiple topics:
.. 
..    build_cases
..    glossary
.. 

A threat case score is the sum of all stage scores.
