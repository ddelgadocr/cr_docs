.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Information leakage is a problem that occurs when data that is derived from the target or data from the future is used as an input for model learning and results in a model that predicts that target with a very high level of accuracy.
