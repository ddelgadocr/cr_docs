.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A score that is assigned to the baseline model, generally the same as a random guess.

.. TODO: REALLY? AN OLD TUTORIAL HAS THIS PARAGRAPH IN IT: In the output, "baseline" or "base" indicates the score of the baseline model. The baseline model predicts the mean of the target column and the score returned is generally the same you would expect from random guessing.
