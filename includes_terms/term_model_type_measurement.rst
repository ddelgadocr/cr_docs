.. The contents of this file are included in multiple topics:
.. 
..    model_data
..    glossary
.. 

A measurement model is generated from baseline models and predicts a single measurement, such as the number of bytes sent out of network or the number of login attempts. The engine merges the signals for all measurement models into a combo model that outputs a single stage score.
