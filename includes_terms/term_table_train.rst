.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The data table from which the |versive| platform learns thousands of possible models and searches for the one that most accurately predict the training data target values.
