.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Classification accuracy (ACC) is a metric that calculates the fraction of total predictions that were correct. Use with binary or multiclass classification models.
