.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Mean absolute log-quotient (MALQ) is a relative error metric that measures the average order-of-magnitude difference between predicted and actual values. Unlike mean absolute percentage error (MAPE), MALQ errors are symmetric for over- and under-predictions and are more robust to outliers with large residual errors. This results in improved model prediction by ignoring small percentages of potential model inaccuracy for data sets that have a high number of interesting data points. Use with linear regression models.
