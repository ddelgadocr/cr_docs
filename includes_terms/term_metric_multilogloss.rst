.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Logistic loss baseline model (MULTILOGLOSS) is a metric for multiclass classification models that `calculates the negative log-likelihood of the data <https://en.wikipedia.org/wiki/Loss_functions_for_classification#Logistic_loss>`__, given the model. Values closer to 0 indicate higher accuracy. Use with multiclass classification models.
