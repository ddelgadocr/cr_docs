.. 
.. xxxxx
.. 

For a one-in report, the |versive| platform learns a series of models, each of which uses a single input. A one-in report is especially useful for identifying information leakage because any single input that accurately predicts the target is likely to be derived from the target.
