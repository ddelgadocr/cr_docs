.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 


.. 
.. The following paragraph is in a slide deck.
.. 

Most networks change over time. This requires an adversary to repeat steps (and create patterns) to maintain their understanding of the network.

* Are high-value targets in previously-known locations? Do they need to be re-discovered?
* Did the layout of the network stay the same? Or did it change?
* Are items of interest still there? Did new items of interest get put there?

The |vse| detects the patterns that reveal the presence of an adversary, often discovered by analyzing the data sources to which the adversary has gained access and the log files that keep track of those systems.

.. 
.. The previous paragraph is in a slide deck.
.. 

