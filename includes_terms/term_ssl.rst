.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A protocol for transmitting private data online. SSL uses a cryptographic system that uses two keys to encrypt data:

* A public key
* A private key

URLs that require an SSL connection start with ``https:`` instead of ``http:``.
