.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The |versive| platform uses a LocalDataTable to return data directly in the Python interpreter as part of a working session while Python is running in interactive mode. A LocalDataTable is stored in a single shard on the primary node in the compute cluster.
