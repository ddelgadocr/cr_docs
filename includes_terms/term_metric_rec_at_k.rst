.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Recall at K (REC@K) is a metric that calculates the percentage of all class 1 records that were ranked in the top K. Use with binary classification and multiclass classification models.
