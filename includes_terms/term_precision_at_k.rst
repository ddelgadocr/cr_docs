.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Precision at K calculates the percentage of ``top_k`` predictions that are true positives.
