.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A system in which networked computers communicate and coordinate their actions by passing messages among one another. 
