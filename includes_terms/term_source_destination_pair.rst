.. 
.. xxxxx
.. 

A source-destination pair represents a unique set of IP addresses---a source IP address and a destination IP address---on the network that have communicated with each other during the observed time window. The |vse| models every target against every unique source-destination pair that is discovered in the data.
