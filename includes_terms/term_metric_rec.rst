.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Recall (REC) is a metric that `calculates the ratio of true positives <https://en.wikipedia.org/wiki/Precision_and_recall>`__ to all class 1 records (true positives plus false negatives). A model with maximum recall predicts no false negatives. Use with binary classification models.
