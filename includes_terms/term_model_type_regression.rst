.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A regression model predicts one of many discrete classes within a continuous value as its target, such as predicting price where each class represents a range of target values. If row weights are applied, the weights will affect how the discrete classes are defined.
