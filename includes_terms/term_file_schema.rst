.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A schema file is a simple text file that lists the column names and the data types they contain, each on a single line, values separated by a tab. Column names must begin with a letter and may contain only letters, numbers, and underscores (no spaces or special characters).

Data types are ``str`` for string data, ``float`` or ``int`` for numeric data, ``datetime`` or ``Timestamp`` for date and time (both of which default to UTC time zone), and ``TypedList`` for lists that preserve the data types of the listed items.

When data is loaded into the system, the |versive| platform will automatically generate a schema based on the data that is found in each column of data.
