.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A function that is defined by a user of the |versive| platform within the extensions package. For example, a user-defined function:

* Identifies values that fit certain criteria, and then applies custom processing
* Aggregates data in a particular way, and then outputs that aggregated data to a custom table for additional processing
