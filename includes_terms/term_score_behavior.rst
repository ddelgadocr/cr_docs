.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A score that indicates how unusual the combined anomalies are that are associated with the behavior.
