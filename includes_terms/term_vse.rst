.. 
.. This include should be used as a "soft intro" to the VSE, at the top of short PDFs, install topics, etc.
.. 

The |vse| analyzes the flow of data across a network, and then identifies bad actors who have breached or accessed your network by automatically understanding core activities adversaries cannot avoid. These activities are linked into readily actionable maps that describe each unfolding adversary campaign. The |vse| detects the most difficult and dangerous internal and external adversaries, and then helps you stop them regardless of tools, tactics, or exploits they choose to use.
