.. The contents of this file are included in multiple topics:
.. 
..    model_data
..    glossary
.. 

A non-random percentage of a full data set. Models that use sampling are processed faster because they are processed against a smaller data set.
