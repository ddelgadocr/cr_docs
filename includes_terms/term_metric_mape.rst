.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Mean absolute percentage error (MAPE) is a metric that `evaluates a predictive model <https://en.wikipedia.org/wiki/Mean_absolute_percentage_error>`__ by measuring error values as a percentage. This metric is appropriate when data is far from zero and shares a common scale. Model results with this metric may be easier to interpret, but are calculated only for positive data values. Use with binary classification or linear regression models.
