.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Precision at K (PREC@K) is a metric that calculates the percentage of top K predictions that are true positives. Use with binary classification and multiclass classification models.
