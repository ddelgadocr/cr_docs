.. 
.. xxxxx
.. 

A block graph is a directed acyclic graph that defines how the engine processes data through the pipeline. A typical block graph contains multiple blocks, each configured to process data in a specific order.
