.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

DNS data maps hostnames to IP addresses and, when analyzed by the engine, is a very rich source for discovering outliers in data that indicate when activities are anomalous from normal usage patterns, such as direct calls to IP addresses, the use of DNS as a tunnel for data exfiltration, or the presence of command and control (C&C) servers.
