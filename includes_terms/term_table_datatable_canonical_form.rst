.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The canonical form is the set of columns (and data types) that are required to be present in a parsed data tables for each of the predefined source types. The engine uses the canonical form as the starting point for processing data. After data is converted into the canonical form, additional processing---adding columns, standardizing data, transforming data, and so on---is performed.