.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

An AAAA record is the address record for a 64-bit IPv6 address. AAAA records are associated with a source and destination IP address and are discoverable from DNS data sources.
