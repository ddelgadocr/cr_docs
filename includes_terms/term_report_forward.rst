.. 
.. xxxxx
.. 

A generalized version of a one-in report, sometimes referred to as forward stepwise regression. This report builds a series of models that starts with a single input and, with each iteration, add the input with the greatest influence on model quality. This report can help identify how many inputs are needed to maintain the accuracy of the predictions.
