.. 
.. xxxxx
.. 

For a one-out report, the |versive| platform learns a series of models, each of which removes a single input from the full model. A one-out report can help identify inputs that have very little influence on the accuracy of predictions. The default report for measuring prediction accuracy. 
