.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The |versive| platform uses a DataTable to process, analyze, and then learn a model. CSV and other raw data files are encoded as a DataTable when loaded into the platform. A DataTable is distributed across all nodes in the compute cluster.
