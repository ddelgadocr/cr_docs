.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A mathematical function that acts on values within a specified time interval. For example, calculating the average of values within the last hour.
