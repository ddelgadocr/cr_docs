.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The sum of a collection of numbers divided by the number of numbers in the collection.
