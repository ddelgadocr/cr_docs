.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

In machine learning, an individual measurable property used to learn a model. Features are related to, but not necessarily the same, as inputs. For example, a feature may be some transformation of one or more inputs.
