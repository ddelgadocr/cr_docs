.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The data table against which the |versive| platform evaluates intermediate models to select the best tuning parameters to improve the quality of the automated model learning.
