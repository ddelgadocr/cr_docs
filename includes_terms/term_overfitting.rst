.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

When a predictive model describes random error or noise instead of the underlying relationship between the inputs and the target. Overfitting generally occurs when a model is overly complex, such as having too many inputs relative to the number of rows.
