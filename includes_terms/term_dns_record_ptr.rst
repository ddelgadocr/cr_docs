.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A PTR record is a pointer record to a canonical domain name. PTR records are often associated with DNS lookups and resolve an IP address to a fully-qualified domain name (FQDN). PTR records are sometimes referred to as reverse DNS records. PTR record data is discoverable from DNS data sources.
