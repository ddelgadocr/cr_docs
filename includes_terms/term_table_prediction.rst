.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The object type returned by a previously-learned model that contains columns for the predicted target value and, optionally, explanations for the data that supports that prediction.
