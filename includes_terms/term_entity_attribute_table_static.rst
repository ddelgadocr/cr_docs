.. The contents of this file are included in multiple topics:
.. 
..    glossary
.. 

A static entity attribute table contains attribute data that was created outside of the engine, such as user attributes that include the department to which they belong or host attributes that distinguish between database servers, personal computers, etc.
