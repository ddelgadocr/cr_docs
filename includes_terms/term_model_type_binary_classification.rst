.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A binary classification model predicts one of two discrete outcomes, such as answering yes or no questions.
