.. The contents of this file are included in multiple topics:
.. 
..    glossary
.. 


An advanced persistent threat (APT) campaign is broken into five distinct stages:

* |stage_plan|
* |stage_access|
* |stage_recon|
* |stage_collect|
* |stage_exfil|

These stages are all necessary for an adversary campaign to be successful. Each stage represents patterns that are discoverable from within an organization's network during an active campaign.
