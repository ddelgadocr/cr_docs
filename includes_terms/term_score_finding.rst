.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A score that indicates how unusual a finding is. The higher the score, the more unusual that finding is. All finding scores for a particular stage are combined to create the stage score.
