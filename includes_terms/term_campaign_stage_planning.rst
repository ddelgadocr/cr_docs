.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 

During the |stage_plan| stage, an adversary considers many factors, such as:

* The amount of time required to identify the target
* The cost of gaining entry (social engineering, phishing, and so on)
* The complexity of the exploit
* The risk tolerance for discovery of the campaign by the target

Activities in the |stage_plan| stage typically occur outside of the network very early in the campaign and are generally undiscoverable by the |vse|.
