.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 


The |stage_access| stage sees an adversary putting considerable effort and expense into gaining entry to the network. For example, by using:

* A phishing attack that relies on a human user to execute a binary
* A valuable zero-day attack against the network's Internet-facing infrastructure: web servers, proxy servers, firewalls, and routers.

Once inside the network, an adversary will work to ensure that initial access is working correctly and will take steps to remove evidence of entry.
