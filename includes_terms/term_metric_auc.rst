.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Area under the curve (AUC) is a metric that measures the probability that the model `correctly ranks a random positive sample higher than a random negative sample <http://gim.unmc.edu/dxtests/roc3.htm>`__. Values range from 0 to 1, with 1 indicating perfect prediction and 0.5 indicating random guessing. Use with binary classification models.
