.. 
.. xxxxx
.. 

A generalized version of a one-out report, sometimes referred to as backward stepwise regression. This report builds a series of models that starts with the full model and, with each iteration, removes the input with the least influence on model quality. This report can help identify how many inputs are needed to maintain the accuracy of the predictions.
