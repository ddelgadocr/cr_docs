.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A means of working with the Python interpreter in which you run an entire script composed of many separate commands.
