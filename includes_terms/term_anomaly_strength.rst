.. 
.. xxxxx
.. 

A measurement that indicates how unusual the anomaly is, in other words, its individual contribution to a threat case.
