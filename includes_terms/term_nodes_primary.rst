.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The node in a training cluster that owns the cluster configuration and assigns tasks to the compute nodes.
