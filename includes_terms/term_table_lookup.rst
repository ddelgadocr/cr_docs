.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A table that maps IP addresses for a given timeframe to specific users and hostnames.
