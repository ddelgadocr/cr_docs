.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The individual data items, or labeled examples, that are in a dataset. In a data table, each row contains a record. When evaluating classification models, you may compare class 1 records (known as class 1) to class 1 predictions (predicted by the model as class 1).
