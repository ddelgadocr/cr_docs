.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

An exclusive external domain is typically a fully-qualified domain name that is unique and directly visible in the data. It is frequently associated with a single internal host and a unique destination IP address. An exclusive external domain is often an anomaly, and can be a strong indicator of adversary activity when associated with other units of measurement such as bytes out of the network.
