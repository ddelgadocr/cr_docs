.. 
.. xxxxx
.. 

Interactions mode allows automated exploration to analyze interactions between different inputs, by exploring the interactions between multiple columns, and then generating features from those interactions to create a more accurate model for complex systems. For example, in a model that predicts drug interactions, two different medications may raise blood pressure slightly when taken individually, but both medications taken at the same time may raise blood pressure to dangerous levels.
