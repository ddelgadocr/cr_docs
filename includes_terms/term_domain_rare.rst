.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A rare domain is typically a subdomain for a destination IP address that is visited infrequently from a small number of hosts within the network, often from only a single host. A rare external domain will often be seen as benign anomaly, becoming interesting when associated with other, stronger indicators of adversary activity.
