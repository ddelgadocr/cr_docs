.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Precision (PREC) is a metric that `calculates the ratio of true positives <https://en.wikipedia.org/wiki/Precision_and_recall>`__ to all class 1 predictions (true positives plus false positives). A model with maximum precision predicts no false positives. Use with binary classification models.
