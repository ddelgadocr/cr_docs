.. The contents of this file are included in multiple topics:
.. 
..    model_data
..    glossary
.. 

A combo model merges the signals from all measurement models, and then outputs a single stage score. Some entities may display several slightly anomalous behaviors that are suspicious when seen together; others may display a single behavior that is very anomalous. In either case, the combo model will return a stage score in the anomalous range.
