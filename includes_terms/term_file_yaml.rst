.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A human-readable data serialization format designed around the common structures of agile programming languages. YAML has a lean, indented outline appearance and is commonly used for configuration files.
