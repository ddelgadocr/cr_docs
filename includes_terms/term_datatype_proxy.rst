.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Proxy data captures data for all client traffic on the network. Proxy log files provide the detailed records of data flow that, when analyzed by the engine, can help identify outliers in the data that indicate when activities are anomalous from normal usage patterns.
