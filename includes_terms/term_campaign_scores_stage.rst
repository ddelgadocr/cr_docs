.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A stage score is the sum of all finding scores that are in the threat case for that stage.
