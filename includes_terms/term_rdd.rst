.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A resilient distributed dataset (RDD) is a logical set of data held in memory, and then split into subsets called shards that are distributed among worker nodes in the cluster.
