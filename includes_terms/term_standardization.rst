.. 
.. xxxxx
.. 

The process of using a lookup table to populate missing columns in datasets. For example, given an IP address, time, and user, fill in the hostname.
