.. 
.. xxxxx
.. 

A model is a series of tables that are used to analyze data, and then make predictions based on outliers that are discovered in that data. A model:

* Is produced on a daily basis
* Contains all available data sets, transforms, aggregations, and other features that were used to learn the model on the previous day
* Applies any improvements that may have been made to the model
* Generates a set of predictions, scores, and results
