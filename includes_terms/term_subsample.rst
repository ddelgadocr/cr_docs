.. 
.. xxxxx
.. 

A subsample is a subset of data that is processed by the engine. Subsampled data is selected randomly from the larger data set, which may reduce model quality if relevant data is not included in the subsample. However, subsampled data processes more quickly and allows for faster iteration during model development.
