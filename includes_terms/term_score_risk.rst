.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A risk score weights an anomaly by how relevant and/or important it is for the discovery of anomalous behavior. A risk score is a surprise score weighted by relevance with the following levels:

* Anomaly: How much risk is associated with a single measurement target? For example, the number of reverse DNS address lookups made in a 4 day window.
* Finding: An alias for an anomaly risk score.
* Stage: How much risk is associated with all measurement targets for a particular campaign stage (|stage_recon|, |stage_collect|, and |stage_exfil|).
* Threat case: How much risk is associated with all measurement targets for all campaign stages?
