.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The nodes in a training cluster that are owned by the cluster configuration and are assigned tasks by the primary node.
