.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The networked group of computers or virtual machines that run the |versive| platform and engines, to analyze data and learn predictive models. 
