.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A cryptographic protocol and interface for executing network services, shell services, and secure network communication with a remote computer.
