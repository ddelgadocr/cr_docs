.. 
.. xxxxx
.. 

A threat case must, at minimum, contain an anomaly in stage 3, 4, or 5, along with a large surprise score associated with one (or more) specified measurement targets and a strong relevance weight. The number of threat cases that are generated depends on the levels at which noteworthy anomalies appear in the results for the |stage_recon|, |stage_collect|, and |stage_exfil| stages.
