.. The contents of this file are included in multiple topics:
.. 
..    commands
..    configure
..    customization_package
..    glossary
..    phases
.. 

The engine is run using the **drive** command, a command-line tool that runs all phases, applies all configuration settings, and builds results for all specified data sources.
