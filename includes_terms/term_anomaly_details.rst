.. 
.. xxxxx
.. 

The set of detailed explanations that describe the rationale for a given anomaly. For example: "The total bytes sent externally over proxy was 25.9M (expected 125.90K because the past median value is 105.53K [+26.2%])".
