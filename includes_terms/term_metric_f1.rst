.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

F1 score (F1) is a metric that `calculates the harmonic mean of precision and recall <https://en.wikipedia.org/wiki/F1_score>`__. Use with binary classification models.
