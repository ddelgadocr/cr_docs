.. The contents of this file are included in multiple topics:
.. 
..    model_data
..    glossary
.. 

A cumulative distribution evaluates the probabilty of seeing a value as extreme (or more extreme) as an observed target value. Cumulative distribution is used to predict surprise scores and risk scores.
