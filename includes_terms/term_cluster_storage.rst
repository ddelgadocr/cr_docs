.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A distributed file system for data storage, such as Amazon Simple Storage Service (S3) or Hadoop Distributed File System (HDFS).
