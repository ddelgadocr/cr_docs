.. 
.. xxxxx
.. 

It is normal for a client to communicate with a server. It is abnormal for a client to communicate with another client. This abnormal behavior is often referred to as client-server schizophrenia and is sometimes an indicator of adversary activity inside a network.
