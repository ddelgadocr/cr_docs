.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

Blocks define how data is processed by the engine, including how it is parsed, standardized, transformed, aggregated, and otherwise prepared for modeling.

Blocks are designed around these concepts:

* Blocks are the primary interface to the engine pipeline
* Blocks have consistent input and output
* Blocks interact with the engine pipeline primarily via the configuration file
* Blocks are defined as a group, but are run one at a time in a specified order
