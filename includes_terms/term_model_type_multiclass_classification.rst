.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A multiclass classification model predicts one of many discrete outcomes or states, which may be represented by integers, floating-point numbers, or strings. For example, predicting the type of product to offer.
