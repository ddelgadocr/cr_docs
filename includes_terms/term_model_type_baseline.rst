.. The contents of this file are included in multiple topics:
.. 
..    model_data
..    glossary
.. 

Baseline models are models without inputs that predict values for every target column for every row. Baseline models are used by measurement models for predicting single measurements.
