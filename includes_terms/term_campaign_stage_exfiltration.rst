.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 

The |stage_exfil| stage typically shows the unauthorized movement of large amounts of data from inside the corporate network to a location outside of it. The |vse| looks for:

* Direction of data movement
* Volume of data being moved
* The target to which data is moved, as compared to already-identified anomalous behaviors

Even after an adversary has moved a large amount of data, they often return and attempt to find and collect more data to be exfiltrated. The engine detects this type of activity by tracking data volume and direction to and from locations inside and outside of the network, along with the ports and protocols used for that data movement.

.. 
.. The previous paragraph is in a slide deck.
.. 
