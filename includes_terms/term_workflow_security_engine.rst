.. 
.. xxxxx
.. 

The end-to-end processing that is performed by the |vse|:

#. Parsing raw data
#. Processing data and preparing it for modeling
#. Modeling data to discover outliers
#. Building results into a threat case list
