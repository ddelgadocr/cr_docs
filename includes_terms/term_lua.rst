.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A lightweight cross-platform programming language designed as a scripting language with extensible semantics as a primary goal.
