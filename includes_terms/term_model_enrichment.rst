.. The contents of this file are included in multiple topics:
.. 
..    model_data
..    glossary
.. 

Columns to use as inputs for specific models (in addition to the inputs used for all models specified as context). Typically, columns are specified as pass-through enrichments, but past-median enrichments may also be added.
