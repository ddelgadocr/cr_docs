.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A table that is produced daily. It contains all available datasets, transforms, and aggregations, plus any other features that are required for learning the models.
