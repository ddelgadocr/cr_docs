.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A description of the mathematical relationship between inputs and outputs of interest that is used to make predictions for your business.
