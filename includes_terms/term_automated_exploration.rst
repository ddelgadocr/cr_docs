.. 
.. xxxxx
.. 

Automated exploration explores the inputs available from a train table, and then automatically identifies new features that may improve model quality. Features are measurable properties that are used to learn a model. Inputs are encoded into numeric features and stored as column data in the train table. Features are not the same as inputs. Inputs may undergo other transformations. The |versive| platform enables automated exploration to allow for many different features to be evaluated in parallel to quickly identify the best features to use for a model.

Automated exploration has the following modes: basic, interactions, or none.
