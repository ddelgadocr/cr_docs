.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Flow data relates byte movement between internal network sources. Flow data is collected as IP addresses and port information from switches and routers as packets move through the network, such as cumulative packets and byte traffic between a source and a destination IP address. Flow data, when analyzed by the engine, can help identify outliers in data that discover abnormal usage patterns.
