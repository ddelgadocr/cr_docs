.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A dataframe is a concept of statistical software that generally refers to tabular data---a data structure that is represented by rows---where each row contains observations and measurements stored as column data.
