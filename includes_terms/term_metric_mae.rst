.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Mean absolute error (MAE) is a metric that `evaluates a predictive model <https://en.wikipedia.org/wiki/Mean_absolute_error>`__ by measuring values in the same units of measurement as the target. Smaller values indicate higher model quality. This metric is less sensitive to the occasional very large error. Use with binary classification or linear regression models.
