.. 
.. xxxxx
.. 

The list of all threat cases that are built by the engine. Each threat case contains details for a specific host within the organization. Each threat case contains a list of findings that compare expected to observed behaviors, broken down by campaign stage.
