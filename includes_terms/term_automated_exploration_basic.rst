.. 
.. xxxxx
.. 

Basic mode allows automated exploration to generate features by analyzing transformations of individual inputs, and then modeling non-linear relationships. For example, in a model that predicts house sale prices, square footage may be an input, but automated exploration may discover that the square root of the total square footage is a more predictive feature.
