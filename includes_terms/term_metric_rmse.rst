.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

Root mean squared error (RMSE) is a metric that `evaluates a predictive model <https://en.wikipedia.org/wiki/Root-mean-square_deviation>`__ by measuring values in the same units of measurement as the target. Smaller values indicate higher model quality. Use with binary classification, multiclass classification, or linear regression models. The default metric for all model types.
