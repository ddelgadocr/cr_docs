.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

In a collection of numbers, the middle value that separates the higher half from the lower half.
