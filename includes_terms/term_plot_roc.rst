.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A graphical plot that illustrates the performance of a binary classification model as its threshold is varied. The curve plots the true positive rate against the false positive rate.
