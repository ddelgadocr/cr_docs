.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The data table used to evaluate the model's predictions; it must not have been included in the data used to learn the model. There may be more than one test table.
