.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A lightweight, language-independent data-interchange format consisting of key-value pairs.
