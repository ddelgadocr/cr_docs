.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A means of working with the Python interpreter in which you enter and execute each command individually at the command line.
