.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

A surprise score is an observed and measured value for an individual anomaly that is an unusual or an outlier. A surprise score is weighted to calculate a risk score.
