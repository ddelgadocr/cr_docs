.. 
.. This is the top-level description for this term. Use in:
..   glossary pages
..   configuration pages
..   etc.
.. 

The measurable properties that you expect to influence the value of the model's predictions. For example, inputs may include date, price, or demographic information.
