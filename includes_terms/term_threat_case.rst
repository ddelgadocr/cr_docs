.. 
.. xxxxx
.. 

A threat case is a user-friendly summary of anomalies that were discovered by the |vse| as they relate to one (or more) suspicious hosts on the network and the data that was moved to and from that host. A threat case identifies a potential security threat that should be investigated.
