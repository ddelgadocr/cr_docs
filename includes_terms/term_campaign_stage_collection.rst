.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 

After an adversary has gained an understanding of the network and knowledge of the location of their actual target (primarily data stores on specific machines), they often move laterally to a position from which they can access the target machine and begin staging data. Staged data is typically moved to a different machine from the target for a few reasons:

* The staging machine must have enough storage space for the data
* The staging machine must be available within the exfiltration window, ideally 24x7 for the most flexibility
* The staging machine must be hard to discover by network defenders

.. 
.. The previous paragraph is in a slide deck.
.. 
