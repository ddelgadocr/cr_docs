.. The contents of this file are included in multiple topics:
.. 
..    model_data
..    glossary
.. 

The predicted value for the target of a measurement model. If the actual measurement is very different, this may be surface as an anomaly in the results.
