.. 
.. versive, primary, platform
.. 


==================================================
About Python
==================================================

.. include:: ../../includes_platform/platform_overview.rst

The |risp_full| exposes a subset of the |versive| platform API that may be used to load datasets, transform that data, build models, and then use those models to make predictions. Python is the programming language that is used for interacting with the |versive| platform API.

.. _python-import-modules:

Import Modules
==================================================
When importing modules of the |versive| platform API into a Python script, include the version number. For example:

.. code-block:: python

   import cr.risp_v1 as risp

This does two things:

#. It simplies the prefix needed by each function call. For example:

   .. code-block:: python

      risp.get_delimiter()

   versus:

   .. code-block:: python

      cr.risp_v1.get_delimiter()

#. It simplies the process of upgrading to newer versions of the |versive| platform API. For example, if the |risp| is upgraded to ``v2``, then the import statement is updated:

   .. code-block:: python

      import cr.risp_v2 as risp

   which will point all function calls in that Python script to the v2 |risp|, as opposed to v1.


.. _python-modes:

Python Modes
==================================================
The |versive| platform API uses the Python programming language, which is easy to learn for beginners and experienced programmers alike.

If you are not familiar with Python, there are many free tutorials available online including one from the `Python Software Foundation <https://docs.python.org/3/tutorial/index.html>`_ . Python code may be run in the following ways:

* By entering each command individually as part of a working session when :ref:`running Python in "interactive mode" <python-interactive-mode>`
* By writing a Python script that is composed of many commands that are :ref:`executed as a batch process <python-batch-mode>`
* By using the :ref:`IPython interactive shell <python-ipython>`
* By using the :ref:`extensions package <python-extension-package-mode>` that is available for engine-specific customizations

.. _python-interactive-mode:

Interactive Mode
--------------------------------------------------
Individual commands may be entered directly in the Python interpreter as part of a working session while Python is running in interactive mode. Interactive mode runs a series of commands from the command line, with each command being entered individually. The period of time between entering and exiting interactive mode is referred to as a working session. Interactive mode is a great way to become more familiar with the |versive| platform API and for testing code before moving it to production.

.. note:: To help prevent typos and speed up the process of entering commands, the |versive| platform supports tab-completion.

**To start a working session in interactive mode**

#. Open a command shell.
#. Enter the following command:

   .. code-block:: console

      $ crpython

.. warning:: Data tables and models will not persist across working sessions unless they are saved to persistent storage.

.. The :doc:`first_model` exercise guides through working in interactive mode to show how each command acts against data.

.. _python-batch-mode:

Batch Mode
--------------------------------------------------
Python scripts are comprised of many commands and may be run together in batch mode. Batch mode runs a series of commands from the command line, with all commands defined in a single Python script file. Batch processing is useful for long-running processes after code has been tested and finalized. Batch scripts must import the RISP module (and other |versive| platform API modules, if used), as well as initialize the compute cluster.

**To run a Python script in batch mode**

#. Verify that the script imports the RISP module and initializes the compute cluster.
#. Open a command shell.
#. Enter the following command:

   .. code-block:: console

      $ crpython script_name.py

   where ``script_name.py`` is the name of the Python script to be run.

.. _python-ipython:

IPython
--------------------------------------------------
`IPython <http://ipython.org/>`_ is an interactive shell for computing that provides a browser-based notebook with support for data visualization, such as inline plots. IPython and supporting modules are included with the |versive| platform beginning with version 2.8.4. Training demos and data using IPython are distributed as a separate .tgz file. 

.. _python-extension-package-mode:

Extensions Package
--------------------------------------------------
.. include:: ../../includes_python/extensions_package.rst



.. _python-recommendations:

Recommendations
==================================================
The following sections describe recommendations to use with Python when working with the |versive| platform API, especially when using the :ref:`extensions package <python-extension-package-mode>` to customize platform behavior.

.. _python-checkpoint-often:

Checkpoint Often
--------------------------------------------------
.. include:: ../../includes_api/function_checkpoint.rst

.. note:: Saving or checkpointing an item with the same key will overwrite the existing checkpointed item. Memory limitations may affect the performance of checkpoints; if memory fills up, older checkpointed items may take longer to retrieve. Using the same key will use the available memory in the most efficient manner.

.. TODO: Currently in both model_data.rst and python.rst copypasta

The following examples show how to use checkpoints:

**Checkpoint a table or model, and then assign a label**

.. code-block:: python

   risp.checkpoint(table_or_model, 'my_label')

**Get all checkpoints**

.. code-block:: python

   risp.get_checkpoints()

**Retrieve a table or model**

.. code-block:: python

   my_table_or_model = risp.load_checkpoint('my_label')

.. TODO: Currently in both model_data.rst and python.rst copypasta

.. _python-column-hints:

Column Hints
--------------------------------------------------
Python has 18 different datetime formats. A column hint decreases the amount of time that is necessary to parse these formats. The string that indicates the format is in :ref:`strptime syntax <python-strptime-syntax>` and is composed of several identifiers that denote the various fields of the date and any separators that may be included.

For example, date and time formatted as "2014-06-01 15:30:00" should use ``date_format="%Y-%m-%d %H:%M:%S"``.

.. _python-consistent-formatting:

Consistent Formatting
--------------------------------------------------
When making calculations on time intervals, be sure that all time-series data has the same formatting throughout. If necessary, create a new column in which all timestamps are converted to the same format prior to performing calculations on that data.

Use one of the following functions to work with timestamps:

* .. include:: ../../includes_api/function_add_context.rst
* .. include:: ../../includes_api/function_add_context_from.rst

.. _python-dates-as-strings:

Dates as Strings
--------------------------------------------------
Datetime data can be costly to parse. To speed up calculations, define datetime columns as string data in the schema until it is necessary to calculate time specifically.


.. _python-identify-information-leaks:

Identify Information Leaks
--------------------------------------------------
.. TODO: This section is in the model_data.rst file in both platform and engine.

.. include:: ../../includes_terms/term_information_leakage.rst

.. include:: ../../includes_api/function_measure_input_importance.rst

Carefully evaluate learned models to identify if information leakage is occuring. If the initial model makes predictions with very high accuracy, investigate for information leakage. Use a one-in report to help identify the source of information leakage. For example, a model that predicts sales has a column that is filled with data only when a sale occurs. That column should not be used as an input because it could predict if a sale occurred with 100% accuracy (each value corresponds to a sale), but the column isn't filled with values until after a sale occurs.

.. _python-learn-models-quickly:

Learn Models Quickly
--------------------------------------------------
.. TODO: This section is in the model_data.rst file in both platform and engine.

Start out by building a simple predictive model and adding new data and calculations iteratively, relearning new models frequently:

* Change one variable at a time, and then compare the summary statistics to the baseline model.
* Do the results make sense?
* Do they agree with the expectations for the model?
* View data summaries and perform other checks on the data periodically to verify that everything is working as expected.

.. _python-learning-curves:

Learning Curves
--------------------------------------------------
In machine learning, a learning curve indicates the point at which additional data stops improving the performance of the model. For example, there may be 5,000,000 rows of data, but after 100,000 rows the model accuracy levels out. Knowing the learning curve, and then loading a data set that is smaller than that learning curve will increase the speed at which a model can be learned without losing accuracy.

.. include:: ../../includes_api/function_measure_learning_curve.rst


.. _python-load-only-relevant-topics:

Load Relevant Inputs
--------------------------------------------------
Exports from databases often include information that is not useful for a predictive model, such as the date and time at which a row is added or modified. There may also be data that has value, but not for the problem the model is trying to solve. Removing this data will make computations faster, especially prior to initially subsampling data.

For example:

.. code-block:: python

   >>> subsample = risp.load_tabular(path='housing_data.csv',
                                     validate=True,
                                     subsample=True,
                                     has_headers=True)
   >>> risp.get_column_names(subsample)

will return data similar to:

.. code-block:: python

   ['Major', 'Minor', 'SalePrice', 'Address', 'ZipCode', 'Stories', 'BldgGrade',
    'Condition', 'YrBuilt', 'YrRenovated', 'FpSingleStory','FpFreestanding',
    'SqFt1stFloor', 'SqFt2ndFloor', 'SqFtTotLiving','SqFtTotBasement',
    'SqFtGarageAttached', 'Bedrooms', 'BathHalfCount','Bath3qtrCount', 'BathFullCount']

The columns ``Major``, ``Minor``, ``Address``, ``FpSingleStory``, ``FpFreestanding``, ``SqFt1stFloor``, ``SqFt2ndFloor``, ``SqFtTotBasement``, ``SqFtGarageAttached``, ``BathHalfCount``, and ``Bath3qtrCount`` are not needed and can be removed:

.. code-block:: python

   >>> table = risp.load_tabular(path='housing_data.csv',
                                 validate=True,
                                 subsample=False,
                                 has_headers=True,
                                 columns=['SalePrice', 'ZipCode', 'Stories', 'BldgGrade',
                                          'Condition', 'YrBuilt', 'YrRenovated',
                                          'SqFtTotLiving', 'Bedrooms', 'BathFullCount'])


.. _python-print-statements:

Print Statements
--------------------------------------------------
.. TODO: This section is in the model_data.rst file in both platform and engine.

.. include:: ../../includes_python/print_statements.rst

.. _python-save-often:

Save Often
--------------------------------------------------
.. TODO: This section is in the model_data.rst file in both platform and engine.

.. include:: ../../includes_python/save_often.rst

.. _python-strptime-syntax:

strptime Syntax
--------------------------------------------------
.. include:: ../../includes_python/strptime_syntax.rst

.. _python-time-zones:

Time Zones
--------------------------------------------------
All datetime columns should be parsed to the Coordinated Universal Time (UTC) time zone. In situations where datatime data is formatted for another time zone, it is offset from UTC by some number of hours.

.. _python-validate-raw-data:

Validate Raw Data
--------------------------------------------------
When working with a dataset for the first time, it's recommended to triage this data by validating a subsample of the data set. A subsample can help identify issues with the data prior to working with the full data set.

.. include:: ../../includes_api/function_load_tablular.rst

For example:

.. code-block:: python

   >>> triage_table = risp.load_tabular(path='training_data/housing/housing_data.csv',
                                        validate=True,
                                        subsample=True,
                                        has_headers=True)




.. _python-user-defined-functions:

User-defined Functions
==================================================
User-defined functions require that they correctly define which rows to select, how to aggregate data, and make calculations. Missing values must be identified, and then correctly handled. Lambda expressions should be avoided. Logging functions, when present in the user-defined function, can help with debugging.


.. _python-missing-values:

Missing Values
--------------------------------------------------
A user-defined function must handle missing values correctly when using |versive| platform API functions like ``select_rows()``, ``add_columns()``, ``create_context_custom_op``, and so on. Approaches for handling missing values include:

* Use :ref:`a parser hint to provide additional markers <python-missing-values-parser-hints>` that identify missing
* Use :ref:`an if statement to catch <python-missing-values-if-statements>` ``None`` values

.. _python-missing-values-parser-hints:

Parser Hints
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_api/function_create_parser_hint_missing.rst

**Find missing values**

.. include:: ../../includes_api/function_create_parser_hint_missing_example_value.rst

**Find missing labels**

.. include:: ../../includes_api/function_create_parser_hint_missing_example_label.rst


.. _python-missing-values-if-statements:

if Statements
++++++++++++++++++++++++++++++++++++++++++++++++++
An ``if`` statement can be used to catch ``None`` values, and then return ``None`` when they occur. For example:

.. code-block:: python

   >>> def new_column(*args):
         if None in args:
           return None
         else:
           v,z,w,d,e,f,g=args
           cated_id = v + z + w +str(d)+str(e)+str(f)+str(g)
           return cated_id

or:

.. code-block:: python

   >>> def parse_timestr(time_str):
         if None in args:
           return None
         else:
           time_str = time_str.zfill(4)
           hour = int(time_str[0:2])
           minute = int(time_str[2:4])
           return hour, minute



.. _python-lambda-expressions:

Lambda Expressions
--------------------------------------------------
A lambda expression is an anonymous function---a function without a name---that may be used in Python instead of a named function.

.. warning:: Although lambda expressions are convenient, they are discouraged for use when extending the |versive| platform because they are more difficult to troubleshoot than a named function.

For example, using lambda functions similar to:

.. code-block:: python

   >>> for each in continuous_vars:
         data = rename_columns(data, name_changes=(each, 'numeric_' + each))
   >>> print data

or:

.. code-block:: python

   >>> for each in col_names[:-1]:
         new_co2_data = rename_columns(data, name_changes=(each, 'co2_' + each))
   >>> print new_co2_data

are discouraged.

Use a named function instead:

.. code-block:: python

   def function_name(args):
     for x in args:
       data = x
     return return_value

.. TODO: This is probably a terrible example.


.. _python-logging-functions:

Logging Functions
--------------------------------------------------
Logging functions are useful for situations where user-defined functions require debugging:

* .. include:: ../../includes_api/function_get_udf_debug_logs.rst
* .. include:: ../../includes_api/function_udf_debug_log.rst


.. _python-module-help:

Module Help
==================================================
After the modules are imported, use the ``help()`` function to get information about the entire module or about specific functions within that module. The ``help()`` function returns information about usage, required and optional arguments, return values, and other relevant information. To exit help, enter ``q``.

**To get help for all functions in the RISP API**

Start the |versive| platform in interactive mode:

.. code-block:: console

and then run the following commands:

.. code-block:: python

   >>> import cr.risp_v1 as risp
   >>> help(risp)

**To get help for a specific function in the RISP API**

Start the |versive| platform in interactive mode:

.. code-block:: console

   $ crpython

and then run the following commands:

.. code-block:: python

   >>> import cr.risp_v1 as risp
   >>> help(load_crtable)
