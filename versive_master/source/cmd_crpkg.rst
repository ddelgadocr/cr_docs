.. 
.. versive, primary, security engine
.. 

==================================================
crpkg Command
==================================================

Manage the installed platform and application packages on a single node.

.. TODO: What about this sentence: "To manage the |versive| platform and applications on the whole cluster, use CR_INSTALL DOC instead."

Usage
==================================================

.. code-block:: console

   crpkg [--list=value --unpack=FILE --setup_new=SETUP_NEW \
          --snapshot=SNAPSHOT --rollback=SNAPSHOT \
          --platform=PLATFORM_VERSION app=APP deactivate_app=APP]

Options
==================================================
The following options are available to this command:

``--app=APP``
   Activate the provided version of an application.

``--deactivate_app=APP``
   Deactivate an application so that it is no longer in use.

``--list=value``
   List all versions of the |versive| platform or applications that are installed in ``$CR_REPO`` or snapshots saved in ``$CR_INSTALL``. Possible values: ``crepe``, ``platform``, or ``snapshot``.

``--platform=PLATFORM_VERSION``
   Activate the specified version of the platform.

``--rollback=SNAPSHOT``
   Roll back to the specified snapshot.

``--setup_new=SETUP_NEW``
   Set up a new empty ``$CR_INSTALL`` directory using the provided platform.

``--snapshot=SNAPSHOT``
   Create a snapshot of the current active installation with the provided name.

``--unpack=FILE``
   Install a platform or application package file into ``$CR_REPO``.

