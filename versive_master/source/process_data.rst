.. 
.. versive, primary, security engine
.. 

==================================================
Process Data
==================================================

A data lake is a storage repository designed to hold massive amounts of raw data and is configured to collect DNS, flow, and proxy log file data from as many network appliances, host machines, and applications as possible. The engine is configured to ingest this data from the data lake on a daily basis, and then analyze this data for the presence of an active adversary campaign.

.. _process-data-sources:

Data Source Types
==================================================
The engine uses the following well-defined source types:

* :ref:`Domain Name System (DNS) <process-data-source-dns>`
* :ref:`Flow <process-data-source-flow>`
* :ref:`Proxy <process-data-source-proxy>`

Raw data that is not columnar must be parsed using regular expressions (regexes) to output data tables. After raw data is parsed, each of the source types must contain, at minimum, certain columns in the schema. This minimum set of required columns is known as the canonical form.

Validation is run on the tables that are successfully parsesd and errors are raised if there are missing columns. In addition, errors will be raised if there are too many missing values in certain columns. Data source types will then undergo additional processing---standardization, transforms, and aggregation---to ensure that the data required by the modeling engine is present in the final table.



.. _process-data-source-dns:

DNS Data Sources
--------------------------------------------------
.. include:: ../../includes_terms/term_datatype_dns.rst


.. _process-data-source-dns-process:

Process DNS Data
++++++++++++++++++++++++++++++++++++++++++++++++++
A primary goal of processing raw DNS log files is to build the right set of data for modeling by building a data processing pipeline for the engine using a series of block graphs and individual blocks within each block graph:

This requires identification of:

#. All internal hostnames, categorized as source IP addresses
#. All contacted entities, categorized as destination IP addresses
#. The DNS record types that were used for contact: A records, AAAA records, and PTR records
#. All users, associated to source IP addresses
#. Specific dates, associated to each hostname
#. Lists that contains up to 200 entries, with each list associated to a hostname/date combination, along with relevant DNS record types

.. warning:: The actual steps required to parse DNS data may vary. Every environment is different. Data sources, even when from same technology and from the same application, may still differ in terms of data source quality.


.. _process-data-source-dns-canonical-form:

Canonical Form
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_table_datatable_canonical_form.rst

The canonical form varies for each predefined data source. For DNS data, the following table lists alphabetically the columns that comprise the canonical form for DNS data tables:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Column
     - Description
   * - **context** (str)
     - A transaction ID is the identifier for the DNS transaction as it takes place across the entire communication sequence between the source and destination IP addresses. A transaction ID is similar to: ``30A8 PACKET 00000011027A4180``. It is parsed into three individual columns: ``thread_id``, ``context``, and ``internal_packet_id``.

       ``context`` captures the ``PACKET`` section of the transaction ID and should be constant for all of the communications that occurred during the transaction.
   * - **dest_ip** (str)
     - The destination IP address.
   * - **flags_code** (str)
     - One (or more) character codes for if the resource record is for an authoritative answer (``A``), a truncated response (``T``), if recursion is desired (``D``), and/or if recursion is available (``R``).
   * - **flags_hex** (str)
     - A hex code for flags that are associated with the resource record. See ``flags_code``.
   * - **internal_packet_id** (str)
     - A transaction ID is the identifier for the DNS transaction as it takes place across the entire communication sequence between the source and destination IP addresses. A transaction ID is similar to: ``30A8 PACKET 00000011027A4180``. It is parsed into three individual columns: ``thread_id``, ``context``, and ``internal_packet_id``.

       ``internal_packet_id`` captures the ``00000011027A4180`` section of the transaction ID and should be unique for each individual query or response that occurred during the transaction.
   * - **named_domain** (str)
     - The text between "http://" and the next "/" in a URL, such as "www.versive.com".
   * - **op_code** (str)
     - A character that represents if the query was a standard query (``Q``), a notification (``N``), an update (``U``), or of an unknown type (``?``).
   * - **protocol** (str)
     - The protocol used for communication: ``TCP`` or ``UDP``.
   * - **query_response** (str)
     - The type of communication: query (an empty column) or response (``R``).
   * - **request_type** (str)
     - The type of DNS request: ``A``, ``AAAA``, ``AXFR``, ``PTR``, or ``TXT``.
   * - **response_code** (str)
     - The response code: query has no errors (``NOERROR``), a format error exists in the query (``FORMERR``), DNS name server is unable to process the request (``SERVFAIL``), the domain name does not exist (``NXDOMAIN``), unsupported query (``NOTAUTH``), or query was refused (``REFUSED``).
   * - **send_receive_indicator** (str)
     - The direction of communication: send (``Snd``) or receive (``Rcv``).
   * - **source_ip** (str)
     - The IP address of the client computer.
   * - **thread_id** (str)
     - A transaction ID is the identifier for the DNS transaction as it takes place across the entire communication sequence between the source and destination IP addresses. A transaction ID is similar to: ``30A8 PACKET 00000011027A4180``. It is parsed into three individual columns: ``thread_id``, ``context``, and ``internal_packet_id``.

       ``thread_id`` captures the ``30A8`` section of the transaction ID and should be constant for all of the communications that occurred during the transaction.
   * - **timestamp** (str)
     - The timestamp of the communication, as string data, including date and time, but not ``AM`` or ``PM``.
   * - **x_id** (str)
     - A unique four-character string that identifies this DNS record.

.. note:: The order of the columns in the data table itself does not matter, as long as all required columns are present and are associated with the correct data type.


.. _process-data-source-dns-parse-raw:

Parse YAML
++++++++++++++++++++++++++++++++++++++++++++++++++
The schema that is used for regular expression parsing of raw DNS data by the |vse| is defined in a YAML patterns file. Every data source and the environment in which it operates is unique; as such, extending the YAML patterns file may be necessary to ensure that all of the columns that are necessary for processing DNS data within that environment are present in the parsed table.

The parse YAML file for parsing raw DNS data into the canonical form is similar to:

.. code-block:: yaml

   file:
     pattern: >-
       (?P<timestamp>\d{1,2}\/\d{1,2}\/\d{4} \d{1,2}:\d{1,2}:\d{1,2})\s+
       (?P<thread_id>\S+)\s+
       (?P<context>\S+)\s+
       (?P<internal_packet_id>\S+)\s+
       (?P<protocol>\S+)\s+
       (?P<send_receive_indicator>\S+)\s+
       (?P<source_ip>\d{1,3}(.\d{1,3}){3})\s+
       (?P<x_id>\S+)\s{1}(?P<query_response>.{1})?\s+
       (?P<op_code>\S+)\s+\
         [(?P<flags_hex>\S+)\s+(?P<flags_code>\S+)\s+(?P<response_code>\S+)\
         ]\s+
       (?P<request_type>\S+)\s+
       (?P<named_domain>\S+)(\s+
       (?P<dest_ip>.*))?$
     skip_patterns:
       - 'DNS Server log file creation .+'
       - 'Log file wrap at .+'
       - 'Message logging key .+'
       - '  .+'
   fields:
     - name: timestamp
       type: str
     - name: thread_id
       type: str
     - name: context
       type: str
     - name: internal_packet_id
       type: str
     - name: protocol
       type: str
     - name: send_receive_indicator
       type: str
     - name: source_ip
       type: str
     - name: x_id
       type: str
     - name: query_response
       type: str
     - name: op_code
       type: str
     - name: flags_hex
       type: str
     - name: flags_code
       type: str
     - name: response_code
       type: str
     - name: request_type
       type: str
     - name: named_domain
       type: str
     - name: dest_ip
       type: str

This creates the parsed table for DNS data.

Validations
++++++++++++++++++++++++++++++++++++++++++++++++++
Validations ensure that the raw DNS data was parsed correctly. When ``default_validation`` is set to ``True`` in the configuration file, errors are raised if any of the the columns in the canonical form are missing, or if the rates of missing values are too high for the following columns:

* The ``dest_ip`` column may have no more than 10% of the columns with missing data
* The ``source_ip`` column may have a maximum 10% of the columns with missing data
* The ``timestamp`` column may have a maximum 1% of the columns with missing data






.. _process-data-source-flow:

Flow Data Sources
--------------------------------------------------
.. include:: ../../includes_terms/term_datatype_flow.rst

Flow data must be ingested properly. First use blocks in the configuration file to define how the ingested data will match the canonical form. The schema used for regular expression parsing is defined in a :ref:`YAML patterns file <process-data-yaml-patterns>`, an example of which is shown below.

Process Flow Data
++++++++++++++++++++++++++++++++++++++++++++++++++
A primary goal of processing raw flow data files is to build the right set of data for modeling by building a data processing pipeline for the engine using a series of block graphs and individual blocks within each block graph:

This requires identification of:

#. All ports, and then the protocols used on those ports
#. All of the traffic (bytes), including direction (bytes in and bytes out)
#. All of the traffic inside of the network, and then all of the traffic outside

.. warning:: The actual steps required to parse flow data may vary. Every environment is different. Data sources, even when from same technology and from the same application, may still differ in terms of data source quality.


Canonical Form
++++++++++++++++++++++++++++++++++++++++++++++++++
The canonical form for flow tables includes the following columns:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Column
     - Description
   * - **bytes_in** (int)
     - The number of bytes received. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``bytes_in_avg`` (float), ``bytes_in_max`` (int), ``bytes_in_min`` (int), ``bytes_in_stddev`` (float), and ``bytes_in_sum`` (int).
   * - **bytes_out** (int)
     - The number of bytes sent. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``bytes_out_avg`` (float), ``bytes_out_max`` (int), ``bytes_out_min`` (int), ``bytes_out_stddev`` (float), and ``bytes_out_sum`` (int).
   * - **dest_ip** (str)
     - The IP address of the destination server.
   * - **dest_port** (str)
     - The port on the IP address of the destination server.
   * - **duration** (int)
     - The duration, in seconds, of the communication.
   * - **external_bytes_in** (int)
     - The number of bytes the proxy server received from an external host. This column is added to the table by the ``reshape_internal_external_categories`` transform. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``external_bytes_in_avg`` (float), ``external_bytes_in_max`` (int), ``external_bytes_in_min`` (int), ``external_bytes_in_stddev`` (float), and ``external_bytes_in_sum`` (int).
   * - **external_bytes_out** (int)
     - The number of bytes the proxy server received to an external host. This column is added to the table by the ``reshape_internal_external_categories`` transform. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``external_bytes_out_avg`` (float), ``external_bytes_out_max`` (int), ``external_bytes_out_min`` (int), ``external_bytes_out_stddev`` (float), and ``external_bytes_out_sum`` (int).
   * - **internal_bytes_in** (int)
     - The number of bytes the proxy server received from an internal host. This column is added to the table by the ``reshape_internal_external_categories`` transform. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``internal_bytes_in_avg`` (float), ``internal_bytes_in_max`` (int), ``internal_bytes_in_min`` (int), ``internal_bytes_in_stddev`` (float), and ``internal_bytes_in_sum`` (int).
   * - **internal_bytes_out** (int)
     - The number of bytes the proxy server received to an internal host. This column is added to the table by the ``reshape_internal_external_categories`` transform. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``internal_bytes_out_avg`` (float), ``internal_bytes_out_max`` (int), ``internal_bytes_out_min`` (int), ``internal_bytes_out_stddev`` (float), and ``internal_bytes_out_sum`` (int).
   * - **protocol** (str)
     - The protocol used for the communication.
   * - **source_ip** (str)
     - The IP address of the source server.
   * - **source_port** (str)
     - The port on the IP address of the source server.
   * - **timestamp** (datetime)
     - The timestamp of the communication.
   * - **user** (str)
     - The name of the user.


Flow YAML Patterns
++++++++++++++++++++++++++++++++++++++++++++++++++
The following example shows a :ref:`YAML patterns file <process-data-yaml-patterns>` for regular expression parsing of flow data:

.. code-block:: yaml

   file:
     pattern: >-
       (?P<timestamp>\d+),
       (?P<timestamp2>\d+),
       (?P<source_ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}),
       (?P<dest_ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}),
       (?P<source_port>\d+),
       (?P<dest_port>\d+),
       (?P<protocol>[^,]*),
       (?P<bytes_out>[^,]*),
       (?P<bytes_in>[^,]*),
       (?P<pks_out>[^,]*),
       (?P<pks_in>[^,]*).

   fields:
     - name: timestamp
       type: str
     - name: timestamp2
       type: str
     - name: source_ip
       type: str
     - name: dest_ip
       type: str
     - name: source_port
       type: int
     - name: dest_port
       type: int
     - name: protocol
       type: str
     - name: bytes_out
       type: int
     - name: bytes_in
       type: int
     - name: pks_out
       type: int
     - name: pks_in
       type: int



Default Validations
++++++++++++++++++++++++++++++++++++++++++++++++++
Errors will be raised if any of the columns required by the canonical form are missing or if the following columns are missing values higher than the percent indicated:

* ``bytes_in``, maximum 1% missing
* ``bytes_out``, maximum 1% missing
* ``dest_ip``, maximum 1% missing
* ``dest_port``, maximum 1% missing
* ``protocol``, maximum 10% missing
* ``source_ip``, maximum 1% missing
* ``source_port``, maximum 1% missing
* ``timestamp``, maximum 1% missing
* ``user``, maximum 1% missing


Default Transforms
++++++++++++++++++++++++++++++++++++++++++++++++++
For flow data, the default transforms are:

**categorize_hostname**
   Categorizes a host name as internal or external using the ``ip`` grouping in the :ref:`configure-category-patterns` section of the configuration file. The input column is ``url_regdomain``. The output column is ``url_regdomain_category``.

**reshape_internal_external_categories**
   Creates the pivot columns configured in the :ref:`configure-category-patterns` section of the configuration file. The input columns are ``bytes_in``, ``bytes_out``, and ``url_regdomain_category``. The output columns are ``external_bytes_in``, ``external_bytes_out``, ``internal_bytes_in``, and ``internal_bytes_out``.



.. _process-data-source-proxy:

Proxy Data Sources
--------------------------------------------------
.. include:: ../../includes_terms/term_datatype_proxy.rst

Proxy data must be ingested properly. First use blocks in the configuration file to define how the ingested data will match the canonical form. The schema used for regular expression parsing is defined in a :ref:`YAML patterns file <process-data-yaml-patterns>`, an example of which is shown below.

Process Proxy Data
++++++++++++++++++++++++++++++++++++++++++++++++++
A primary goal of processing raw proxy log files is to build the right set of data for modeling by building a data processing pipeline for the engine using a series of block graphs and individual blocks within each block graph:

This requires identification of:

#. All domains, ports, subdomains, IP addresses on the network
#. All of the traffic (bytes), including direction (bytes in and bytes out)
#. All of the traffic inside of the network, and then all of the traffic outside

.. warning:: The actual steps required to parse proxy data may vary. Every environment is different. Data sources, even when from same technology and from the same application, may still differ in terms of data source quality.

Canonical Form
++++++++++++++++++++++++++++++++++++++++++++++++++
The canonical form for proxy data includes the following columns:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Column
     - Description
   * - **action** (str)
     - The action taken by the proxy server, such as ``filter``, ``deny``, or ``observe``.
   * - **bytes_in** (int)
     - The number of bytes the proxy server received with the request. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``bytes_in_avg`` (float), ``bytes_in_max`` (int), ``bytes_in_min`` (int), ``bytes_in_stddev`` (float), and ``bytes_in_sum`` (int).
   * - **bytes_subdomain** (int)
     - The number of bytes the proxy server received from the subdomain with the request. This column is added to the table by the ``url_features`` transform. Aggregations for ``avg``, ``max``, ``min``, and ``stddev`` are applied, after which the following columns are added to the table: ``bytes_subdomain_avg`` (float), ``bytes_subdomain_max`` (int), ``bytes_subdomain_min`` (int), and ``bytes_subdomain_stddev`` (float).
   * - **bytes_out** (int)
     - The number of bytes the proxy server sent with the response. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``bytes_out_avg`` (float), ``bytes_out_max`` (int), ``bytes_out_min`` (int), ``bytes_out_stddev`` (float), and ``bytes_out_sum`` (int)
   * - **dest_ip** (str)
     - The IP address of the proxy server.
   * - **dest_port** (str)
     - The port on the IP address of the proxy server.
   * - **duration** (int)
     - The duration of the communication. This column is added to the table when aggregations are applied. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``duration_avg`` (float), ``duration_max`` (int), ``duration_min`` (int), ``duration_stddev`` (float), and ``duration_sum`` (int).
   * - **external_bytes_in** (int)
     - The number of bytes the proxy server received from an external host. This column is added to the table by the ``reshape_internal_external_categories`` transform. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``external_bytes_in_avg`` (float), ``external_bytes_in_max`` (int), ``external_bytes_in_min`` (int), ``external_bytes_in_stddev`` (float), and ``external_bytes_in_sum`` (int).
   * - **external_bytes_out** (int)
     - The number of bytes the proxy server received to an external host. This column is added to the table by the ``reshape_internal_external_categories`` transform. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``external_bytes_out_avg`` (float), ``external_bytes_out_max`` (int), ``external_bytes_out_min`` (int), ``external_bytes_out_stddev`` (float), and ``external_bytes_out_sum`` (int).
   * - **internal_bytes_in** (int)
     - The number of bytes the proxy server received from an internal host. This column is added to the table by the ``reshape_internal_external_categories`` transform. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``internal_bytes_in_avg`` (float), ``internal_bytes_in_max`` (int), ``internal_bytes_in_min`` (int), ``internal_bytes_in_stddev`` (float), and ``internal_bytes_in_sum`` (int).
   * - **internal_bytes_out** (int)
     - The number of bytes the proxy server received to an internal host. This column is added to the table by the ``reshape_internal_external_categories`` transform. Aggregations for ``avg``, ``max``, ``min``, ``stddev``, and ``sum`` are applied, after which the following columns are added to the table: ``internal_bytes_out_avg`` (float), ``internal_bytes_out_max`` (int), ``internal_bytes_out_min`` (int), ``internal_bytes_out_stddev`` (float), and ``internal_bytes_out_sum`` (int).
   * - **len_host** (int)
     - The length of the host name. This column is added to the table by the ``url_features`` transform. Aggregations for ``avg``, ``max``, ``min``, and ``stddev`` are applied, after which the following columns are added to the table: ``len_host_avg`` (float), ``len_host_max`` (int), ``len_host_min`` (int), and ``len_host_stddev`` (float).
   * - **num_dots** (int)
     - The number of dots (``.``) contained in the full URL. This column is added to the table by the ``url_features`` transform. Aggregations for ``avg``, ``max``, ``min``, and ``stddev`` are applied, after which the following columns are added to the table: ``num_dots_avg`` (float), ``num_dots_max`` (int), ``num_dots_min`` (int), and ``num_dots_stddev`` (float).
   * - **num_slashes** (int)
     - The number of slashes (``/``) contained in the full URL. This column is added to the table by the ``url_features`` transform. Aggregations for ``avg``, ``max``, ``min``, and ``stddev`` are applied, after which the following columns are added to the table: ``num_slashes_avg`` (float), ``num_slashes_max`` (int), ``num_slashes_min`` (int), and ``num_slashes_stddev`` (float).
   * - **source_ip** (str)
     - The IP address of the client computer.
   * - **status_code** (str)
     - The HTTP status code received.
   * - **subdomain** (str)
     - The subdomain associated with the full URL. This column is added to the table by the ``url_features`` transform.
   * - **timestamp** (datetime)
     - The timestamp of the communication.
   * - **url** (str)
     - The full URL, including the scheme, top-level domain, subdomain, and query string. If each of these components is in a separate column in the raw data, use the **function** configuration setting in the **parse** section of the configuration file to specify the name of a custom function that concatenates them.
   * - **url_host** (str)
     - The host name associated with the full URL. This column is added to the table by the ``url_features`` transform.
   * - **url_length** (int)
     - The length of the full URL. This column is added to the table by the ``url_features`` transform.
   * - **url_regdomain_count_distinct** (int)
     - The number of unique URL domains.
   * - **user** (str)
     - The name of the user on the client computer.
   * - **user_agent** (str)
     - The user agent used.
   * - **user_agent_count_distinct** (int)
     - The number of unique user agents.
   * - **user_agent_len** (int)
     - The length of the user agent. This column is added to the table by the ``user_agent_len`` transform. Aggregations for ``avg``, ``max``, ``min``, and ``stddev`` are applied, after which the following columns are added to the table: ``user_agent_len_avg`` (float), ``user_agent_len_max`` (int), ``user_agent_len_min`` (int), and ``user_agent_len_stddev`` (float).


Proxy YAML Patterns
++++++++++++++++++++++++++++++++++++++++++++++++++
The following example shows a :ref:`YAML patterns file <process-data-yaml-patterns>` for regular expression parsing of proxy data:

.. code-block:: yaml

   file:
     pattern: >-
       (?P<timestamp>\d{4}-\d{2}-\d{2}),
       (?P<time>\d{2}:\d{2}:\d{2}),
       (?P<time_taken>\d+),
       (?P<source_ip>\d{1,3}(\.\d{1,3}){3}),
       (?P<user>\S+),
       (?P<auth_group>\S+),
       (?P<supplier_name>\S+),
       (?P<dest_ip>\d{1,3}(\.\d{1,3}){3}),
       (?P<supplier_country>\S+),
       (?P<supplier_failures>\S+),
       (?P<exception_id>\S+),
       (?P<filter_results>\S+) \",
       (?P<x_ss_category>[^\"]*)\",
       (?P<referr_uri>\S+)\s+,
       (?P<status_code>\S+) (?P<action>\S+),
       (?P<method>\S+),
       (?P<cs_Content_Type>\S+),
       (?P<cs_uri_schema>\S+),
       (?P<cs_host>\S+),
       (?P<dest_port>\S+),
       (?P<cs_uri_path>\S+),
       (?P<cs_uri_query>\S+),
       (?P<cs_uri_extension>\S+) \",
       (?P<user_agent>[^\"]*)\",
       (?P<proxy_ip>\d{1,3}(\.\d{1,3}){3}),
       (?P<bytes_out>\d+),
       (?P<bytes_in>\d+),
       (?P<virus_id>\S+) \",
       (?P<app_name>[^\"]*)\" \",
       (?P<app_operation>[^\"]*)\",
       (?P<threat_risk>\S+),
       (?P<transaction_uuid>\S+),
       (?P<icap_req_mode_header>\S+),
       (?P<icap_resp_mode_header>\S+)
     skip_patterns:
       - '#Software: .+'
       - '#Version: .+'
       - '#Start-Date: .+'
       - '#Date: .+'
       - '#Fields: .+'
       - '#Remark: .+'
   fields:
     - name: timestamp
       type: str
     - name: time
       type: str
     - name: time_taken
       type: str
     - name: source_ip
       type: str
     - name: user
       type: str
     - name: auth_group
       type: str
     - name: supplier_name
       type: str
     - name: dest_ip
       type: str
     - name: supplier_country
       type: str
     - name: supplier_failures
       type: str
     - name: exception_id
       type: str
     - name: filter_results
       type: str
     - name: x_ss_category
       type: str
     - name: referr_uri
       type: str
     - name: status_code
       type: str
     - name: action
       type: str
     - name: method
       type: str
     - name: cs_Content_Type
       type: str
     - name: cs_uri_schema
       type: str
     - name: cs_host
       type: str
     - name: dest_port
       type: int
     - name: cs_uri_path
       type: str
     - name: cs_uri_query
       type: str
     - name: cs_uri_extension
       type: str
     - name: user_agent
       type: str
     - name: proxy_ip
       type: str
     - name: bytes_out
       type: int
     - name: bytes_in
       type: int
     - name: virus_id
       type: str
     - name: app_name
       type: str
     - name: app_operation
       type: str
     - name: threat_risk
       type: str
     - name: transaction_uuid
       type: str
     - name: icap_req_mode_header
       type: str
     - name: icap_resp_mode_header
       type: str




Validations
++++++++++++++++++++++++++++++++++++++++++++++++++
Errors will be raised if any of the columns required by the canonical form are missing or if the following columns are missing values higher than the percent indicated:

* ``source_ip``, maximum 10% missing
* ``timestamp``, maximum 1% missing

Transforms
++++++++++++++++++++++++++++++++++++++++++++++++++
For proxy data, the default transforms are:

**categorize_url_regdomain**
   Categorizes a host name as internal or external using the ``hostname`` grouping in the :ref:`configure-category-patterns` section of the configuration file. The input column is ``url_regdomain``. The output column is ``url_regdomain_category``.

**reshape_internal_external_categories**
   Creates the pivot columns configured in the :ref:`configure-category-patterns` section of the configuration file. The input columns are ``bytes_in``, ``bytes_out``, and ``url_regdomain_category``. The output columns are ``external_bytes_in``, ``external_bytes_out``, ``internal_bytes_in``, and ``internal_bytes_out``.

**user_agent_len**
   Calculates the length of the user agent. The input column is ``user_agent``. The output column is ``user_agent_len``.

**url_features**
   Calculates useful features for a URL. The input column is ``url``. The output columns are ``bytes_subdomain``, ``len_host``, ``num_dots``, ``num_slashes``, ``subdomain``, and ``url_length``.

.. 
.. Optional Transforms
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. The following transforms may be run by specifing certain configuration settings, and then running a command.
.. 
.. **domain_levenshtein**
..    Calculates the `Levenshtein distance <https://en.wikipedia.org/wiki/Levenshtein_distance>`_ for the URL. Calculated only if **top_url_list_path** is set in the :ref:`configure-transform` section of the configuration file and if **edit_distance_lookup_path** is specified in the :ref:`configure-flighting` section.
.. 
..    The output of the transform has the following meanings:
.. 
..    **None**
..       The domain in the URL is None.
.. 
..    **0**
..       The value appeared in the top URL set.
.. 
..    **1-3**
..       The value had a Levenshtein distance of n to a value in the top URL set.
.. 
..    **10,000**
..       The value had a Levenshtein distance of at least 4 (probably more) to all values in the top URL set.
.. 
..    The output column is ``edit_distance``. Aggregations for ``avg``, ``max``, ``min``, and ``stddev`` add the following columns to the table:
.. 
..    * ``edit_distance_avg`` (float), ``edit_distance_max`` (int), ``edit_distance_min`` (int), ``edit_distance_stddev`` (float)
.. 
.. **entropy_features**
..    Calculates the Shannon entropy for a URL domain or subdomain. The input columns are ``subdomain`` and ``url_domain``. The output columns are ``shannon_entropy`` and ``url_domain_entropy``.
.. 
..    Aggregations for ``avg``, ``max``, ``min``, and ``stddev`` add the following columns to the table:
.. 
..    * ``shannon_entropy_avg`` (float), ``shannon_entropy_max`` (int), ``shannon_entropy_min`` (int), ``shannon_entropy_stddev`` (float), ``url_domain_entropy_avg`` (float), ``url_domain_entropy_max`` (int), ``url_domain_entropy_min`` (int), ``url_domain_entropy_stddev`` (float)
.. 






















.. _process-data-build-pipeline:

Build a Pipeline
==================================================
The engine pipeline ingests data, processes it, and then models that data. The modeling results are output to CSV tables and may be ingested to the |vse| web user interface. Blocks are the primary (and recommended) way to build the engine pipeline for data processing.

.. note:: The following sections provide an introduction of using blocks to build the data processing pipeline. 


.. _process-data-blocks:

About Blocks
--------------------------------------------------
.. include:: ../../includes_terms/term_blocks.rst

.. _process-data-blocks-and-columns:

Blocks and Columns
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_columns.rst

.. _process-data-block-graphs:

Block Graphs
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_block_graph.rst

.. _process-data-blocks-configuration:

Blocks Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_configuration.rst

For more information about the configuration settings for a block graph, see the :ref:`configure-block-graphs` section of the configuration file.


.. _process-data-custom-blocks:

Custom Blocks
--------------------------------------------------
.. TODO: The following paragraph is repetitive (pasted together) and needs an edit.

A custom block handles functionality that is not handled by an existing block from the blocks library. Custom blocks should be added to the :doc:`extension package <extensions>`.

.. note:: Blocks support Spark DataFrames. Custom blocks should be built using the Spark DataFrame API. A data frame is a concept of statistical software that generally refers to tabular data, which is a data structure that is represented by cases (rows) and where each case contains observations and measurements stored as column data. A Spark DataFrame is a distributed collection of data that is grouped into named columns. Apache Spark has a mature API for interacting with data frames: the Spark DataFrame API. All blocks (both library and custom) use `the Python-based Spark DataFrame API <https://spark.apache.org/docs/1.6.1/api/python/pyspark.sql.html#pyspark.sql.DataFrame/>`__. All of the functions in the Spark DataFrame API are supported for use with blocks. Refer to the Spark DataFrame API documentation for more information about each of the individual functions.

.. _process-data-custom-blocks-types:

Block Types
++++++++++++++++++++++++++++++++++++++++++++++++++
Blocks provide four base classes as interfaces to data frames. Each base class provides specific input and output rules for tabular data:

.. list-table::
   :widths: 150 250 250
   :header-rows: 1

   * - Base Class
     - Input Rules
     - Output Rules
   * - BundleBlock
     - One (or more) data frames; input columns are assumed to be identical to output, unless declared in the configuration file.
     - One (or more) data frames; output columns are assumed to be identical to input, unless declared in the configuration file.
   * - FrameBlock
     - One Spark DataFrame; input columns are assumed to be identical to output, unless declared in the configuration file.
     - One Spark DataFrame; output columns are assumed to be identical to input, unless declared in the configuration file.


.. _process-data-blocks-tutorial:

Example: Blocks
--------------------------------------------------
The following sections are a tutorial that walk through building a simple block graph that ingests proxy data, standardizes some columns in the data, and then aggregates that data around specified data points.

In this tutorial, data is:

#. Ingested into the engine
#. Parsed into tabular format
#. Cleaned up for IP address data
#. Cleaned up for user data
#. Transformed
#. Built into two output tables

.. _process-data-blocks-tutorial-verify-data-source:

Verify Data Source
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_steps_00.rst

.. _process-data-blocks-tutorial-parse-proxy-data:

Parse Proxy Data
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_steps_01.rst

.. _process-data-blocks-tutorial-split-parsed-unparsed:

Split Parsed, Unparsed
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_steps_02.rst

.. _process-data-blocks-tutorial-clean-up-ip-address:

Clean Up IP Addresses
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_steps_03.rst

.. _process-data-blocks-tutorial-clean-up-user-data:

Clean Up User Data
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_steps_04.rst

.. _process-data-blocks-tutorial-add-columns:

Add Columns
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_steps_05.rst

.. _process-data-blocks-tutorial-group-by-source-ip:

Group by Source IP
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_steps_06.rst

.. _process-data-blocks-tutorial-sum-bytes-in-out:

Sum Bytes In / Out
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_steps_07.rst

.. _process-data-blocks-tutorial-create-lists:

Create Lists
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_steps_08.rst

.. _process-data-blocks-tutorial-verify-dependencies:

Verify Dependencies
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_steps_09.rst

.. _process-data-blocks-tutorial-verify-output:

Verify Output
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_steps_10.rst

.. _process-data-blocks-tutorial-full-yaml-example:

Full YAML Example
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_tutorial_yaml.rst










.. _process-data-parse-data:

Parse Data
==================================================
To ingest non-tabular data into the engine it must first be parsed from the raw data logs into the canonical form:

#. Create a YAML patterns file that defines the regular expressions necessary to parse data into the columns and rows of a data table.
#. Add the location of this file to the block configuration for the parse block that will be used to parse the raw data source.

.. note:: `RegexOne <http://regexone.com/>`_ is a good resource for learning more about regular expressions.

.. _process-data-yaml-patterns:

YAML Patterns File
--------------------------------------------------
Regular expression parsing of raw data to a format that is usable by the engine is defined using a YAML patterns file. Regular expressions define how raw data maps to columns in a data table. Regular expressions may be defined in two ways:

* Individual regular expressions, by column
* Consolidated into a named capture group

.. warning:: Regular expressions defined in a YAML patterns file cannot use backreferences, such as ``/1``.

.. warning:: YAML string literals may be in single (``'``) or double (``"``) quotes. However, the escapes pattern within the YAML file must match by using a single escape (``\``) for single quotes and double escapes (``\\``) for double quotes. For example, the following patterns will evaluate to the same parsed string:

   .. code-block:: yaml

      pattern: "^172\\.(1[6789]|2\\d|30|31)\\."

   and:

   .. code-block:: yaml

      pattern: '^172\.(1[6789]|2\d|30|31)\.'

Individual
++++++++++++++++++++++++++++++++++++++++++++++++++
Regular expressions may be listed individually, alongside the related ``name`` and ``type``, and grouped under ``fields``. The list of fields must include, at minimum, a name entry for each column in :ref:`the canonical form for that source type <process-data-sources>`. The individual approach is more readable and makes additions easier, but is a less efficient method of parsing regular expressions.

For example: 

.. code-block:: yaml

   fields:
     - name: epoch
       pattern: (?i) IN (?P<epoch>[^ ]+)
       type: str
     - name: source
       pattern: (?i) IN (?P<source>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})
       type: str

where ``name`` and ``type`` correspond to a column in a data table and ``pattern`` is the regular expression to apply to that column.

.. _process-data-named-capture-group:

Named Capture Group
++++++++++++++++++++++++++++++++++++++++++++++++++
Regular expressions may be consolidated into a named capture group. The ``file`` group consolidates all regular expressions into a single statement using ``(?P<name>regex)`` for each regular expression in the statement, with a comma separating each ``(?P<name>regex)`` in the statement. Each ``<name>`` in the statement must match a name in the ``fields`` group, which must include, at minimum, a name entry for each column in :ref:`the canonical form for that source type <process-data-sources>`. The named capture group approach is less readable, but allows for more efficient processing of regular expressions.

For example:

.. code-block:: yaml

   file:
     pattern: >-
       (?P<epoch>[^ ]+),
       (?P<source>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})

   fields:
     - name: epoch
       type: str
     - name: source
       type: str

where ``name`` and ``type`` correspond to a column in a data table and ``pattern`` is the regular expression to apply to that column.

Skip Patterns
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
When a named capture group is used for regular expressions, a list of patterns to be skipped may also be specified. Each pattern to be skipped is defined as a regular expression. If a row of data matches a skip pattern, that row is skipped.

For example:

.. code-block:: yaml

   file:
     pattern: >-
       (?P<timestamp>\d{4}-\d{2}-\d{2}),
       ...
     skip_patterns:
       - 'DNS Server log file creation .+'
       - 'Log file wrap at .+'
       - 'Message logging key .+'
       - '  .+'

      fields:
        - name: field_name
        - type: type
        ...

where the four patterns defined above skip the header found at the top of the DNS log file.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
For example, review the following raw log lines:

.. code-block:: none

   Jan 02 2015 11:56:50PM,-,-,192.168.245.123,-,gdlbhqhwgb,-,-,-,-,-,-
   Jan 02 2015 11:57:11PM,-,-,192.168.27.244,-,xpiqpttwzo,-,-,-,-,-,-
   Jan 02 2015 11:57:48PM,-,-,192.168.48.8,-,vqcrpeikoj,-,-,-,-,-,-
   Jan 02 2015 11:57:59PM,-,-,192.168.131.106,-,yqlyfuwpul,-,-,-,-,-,-
   Jan 02 2015 11:58:17PM,-,-,192.168.214.117,-,wptcsvnsbf,-,-,-,-,-,-
   Jan 02 2015 11:58:24PM,-,-,192.168.67.51,-,gcgwgzwted,-,-,-,-,-,-
   Jan 02 2015 11:58:29PM,-,-,192.168.175.40,-,joaznuzaxp,-,-,-,-,-,-
   Jan 02 2015 11:59:11PM,-,-,192.168.12.147,-,zsxpznvjss,-,-,-,-,-,-
   Jan 02 2015 11:59:21PM,-,-,192.168.225.197,-,xfzpadvdpc,-,-,-,-,-,-
   Jan 02 2015 11:59:32PM,-,-,192.168.173.241,-,bqmtaxyhan,-,-,-,-,-,-

The following YAML patterns file includes the correct regular expressions to parse the logs into columns for ``timestamp``, ``source_ip``, and ``hostname``:

.. code-block:: yaml

   file:
     pattern: >-
       (?P<timestamp>[A-Z][a-z]{2} \d{2} \d{4} \d{2}:\d{2}:\d{2}[A-Z]{2}),-,-,
       (?P<source_ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}),-,
       (?P<hostname>\S+),-,-,-,-,-,-

   fields:
     - name: timestamp
       type: str
     - name: source_ip
       type: str
     - name: hostname
       type: str


.. _process-data-common-regex-patterns:

Common Regex Patterns
--------------------------------------------------
The following list shows common regular expressions used to match patterns:

.. TODO: Go through the four/five topics that are full of regular expressions -- the .yaml files in the /test section of APT that mock the regular expression file. There's quite a few in there that obviously work. Generalize them a bit and put them here.

Match an empty string
   The following regular expression should be placed before the timestamp:

   ``(?P<missing>[^0-9]?)``

   where ``missing`` will match 0 or 1 characters that are not digits. The first numbers in a timestamp are digits, so it will match 0 characters.

.. _process-data-regex-debugger:

regex_debugger.py
--------------------------------------------------
Writing regular expressions is an iterative process. The engine provides the regular expression debugger tool---``regex_debugger.py``---to make this process easier. This tool returns a report that lists the skipped lines and line breaks for each regular expression breakpoint to help you more quickly debug and write the regular expressions for ingestion.

.. warning:: ``regex_debugger.py`` only works when regular expressions are :ref:`consolidated into a named capture group <process-data-named-capture-group>`.

Get Error Tables
++++++++++++++++++++++++++++++++++++++++++++++++++
Error tables are available from two locations: the HDFS cluster or locally.

**Get error tables from the cluster**

Run the following command to get all successfully parsed records and parsing errors from the HDFS cluster:

.. code-block:: console

   $ hadoop fs –du –s –h <root_path>/parsed*/<source>_<date>"


**Get error tables locally**

Do the following to get all successfully parsed records and parsing errors from the local machine:

#. Start ``crpython``:

   .. code-block:: console

      $ crpython

#. Import the data ingest modules:

   .. code-block:: console

      $ import cr.data_ingest_v1 as ingest

#. Import the RISP modules:

   .. code-block:: console

      $ import cr.risp_v1 as risp

#. Initialize the cluster:

   .. code-block:: console

      $ risp.initialize_cluster(default_connection='hdfs')

#. Load a semi-structured text file, parsing it with regular expressions in the YAML patterns file:

   .. code-block:: console

      $ table = ingest.load_text(path='path_to_log', patterns_file='path_to_yaml')
	  
#. Get the errors:

   .. code-block:: console

      $ errors = ingest.get_table_errors(table)

#. Save the errors to a data table:

   .. code-block:: console

      $ risp.save_crtable(table=errors, path='path_to_save')


Run Debugger
++++++++++++++++++++++++++++++++++++++++++++++++++
Call the debugging tool to see where breaks are occurring:

.. code-block:: console

   $ [SUBSAMPLE=1] crpython regex_debugger.py <path_to_error_table> \
     <path_to_patterns_file> <connection>

Optionally, set the SUBSAMPLE environment variable to use a subsampled dataset. This samples data the same as using the :ref:`configure-sampling` section in the configuration file.

Review Report
++++++++++++++++++++++++++++++++++++++++++++++++++
The ``regex_debugger.py`` tool returns a report that displays skipped lines and line breaks for each regular expression breakpoint. To make breaks easier to locate, regular expressions after line breaks are in bold. The report also lists the character that occurs when the regular expression breaks along with example lines. For example:

.. code-block:: console

   ---- BEGIN REPORT ----
   
   Broken at regex index 25, 1 times
   ^(?P<foo>foo)(?P<bar>bar)$
   Failed on: d
     foobard
   
   Broken at regex index 13, 1 times
   ^(?P<foo>foo)(?P<bar>bar)$
   Failed on:
     foo
   
   Broken at regex index 1, 2 times
   ^(?P<foo>foo)(?P<bar>bar)$
   Failed on: a
     afoobar
   
   0 originally failing lines are now fixed
   
   ---- END REPORT ----

Using the output, update the regular expressions. For example, given an initial regular expression of ``(?P<name>[A-Z a-z])`` you might do the following:

* Add additional valid characters to the group, such as underscores.
* Make certain fields optional.
* Allow a dash to be present as a marker for a field that is present but does not contain data.
* Allow spaces in fields by enclosing in an additional group.

Then repeat the steps above. Subsequent runs of the tool will indicate the number of failing lines that have been fixed by any changes.









.. _process-data-transform-multiday:

Multi-day Values
==================================================
All multi-day values are measured in days to ensure that all users and hosts within the network are analyzed over the same timescale. All multi-day tables are generated the same way regardless of source table or type of multi-day transform.

Multi-day values are enabled and configured via the :ref:`configuration file <process-data-multi-day-configure-transforms>`, with each type of multi-day value configured separately. The configuration specifies the range to use for multi-day calculations, the columns to be input for multi-day calculation, and the type of transform to be applied.

Multi-day values may be calculated with the following transforms:

* :ref:`process-data-multi-day-crosstab`
* :ref:`process-data-multi-day-count-distinct`
* :ref:`process-data-multi-day-sum`

All multi-day values are calculated from a single-day table that contains one distinct entry per primary key (typically related to the host or the user). Composite primary keys are allowed in a single-day table, but only when the measurement (days) is not in the composite primary key because all multi-day aggregations require the same baseline starting measurement (days).


.. _process-data-multi-day-configure-transforms:

Configure Transforms
--------------------------------------------------
Multi-day transforms are configured from two locations:

* Use the :ref:`configure-multi-day-features` section of the configuration file for settings that will apply to all multi-day transforms, such as the duration of the lookback window
* Use the :ref:`configure-entity-attribute-sources` section of the configuration file for each entity attribute table that will be used to define a multi-day transform; override global settings on a per-transform basis, if desired

Global Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
Global settings for multi-day transforms are configured in the :ref:`configure-multi-day-features` section of the configuration file. 

Usage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: yaml

   multi_day_features:
     ...
     multi_day_eat_lookback_windows:
       custom_fill_type:            # fill_custom only
       custom_fill_value:           # fill_custom only
       lookback_duration: -3d
       lookback_step: -1d
       missing_values_policy: fill_default

Descriptions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. include:: ../../includes_config/multiday_lookback.rst

Example
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: yaml

    multi_day_eat_lookback_windows:
      - lookback_step: 0d
        lookback_duration: 30d


Per-transform Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
An entity attribute table that calculates multi-day values starts by using a generated table:

#. Input non-timescale ``key_columns`` from a single-day table
#. Define upstream dependencies
#. Input a series of columns that were previously calculated by those upstream dependencies
#. Specify a multi-day transform
#. Generate output for those input columns against the specified transform

Columns are generated that have the following naming pattern:

.. code-block:: none

   <input_cols>_<transform_name>_<window>

where <window> represents the length of the lookback window.

Usage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
All multi-day transforms are similar to:

.. code-block:: yaml

   multi_day_measures_params:
     measures:
       input_cols: 
         - 'column_name'
         - 'column_name'
         - ...
       lookback_window:
         custom_fill_type:            # fill_custom only
         custom_fill_value:           # fill_custom only
         lookback_duration: -3d
         lookback_step: -1d
         missing_values_policy: fill_default
       normalized: True
       transform_args:                # crosstab only
         A_name: 'column_name'        # crosstab only
         B_name: 'column_name'        # crosstab only
         separator: '_'               # crosstab only
         stats: 'count_distinct_AxB'  # crosstab only
       transform_name: 'multi_day_count_distinct' 
       window_unit: 1d

Settings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. include:: ../../includes_config/multiday_params.rst

Example
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: yaml

   multi_day_count_distinct:
     generate: create_multi_day_measures_table
     udf_source: LIBRARY
     key_columns: ['hostname']      # must not contain timescale values!
     attribute_columns: []          # should be empty
     type: MODELING_TABLE_ATTRIBUTE
     keep_all_entities: True
     path_add_date_suffix: True
     depends_on: [
       {'datasource': 'netflow', 'phase': 'agg', 'groupby_key': 'day_host'},
       {'datasource': 'netflow_agg_multi_day', 'offset': 1, 'phase': 'generated'}
     ]
     multi_day_measures_params:
       measures:
         - input_cols:
           - netflow_internal_internal_ICMP_dest_ip_list
           - netflow_internal_internal_SNMP_dest_ip_list
           - netflow_internal_internal_UDP_dest_ip_list
           - netflow_dest_ip_protocol_TCP_NOT_SNMP_list
           transform_name: multi_day_count_distinct



.. _process-data-multi-day-crosstab:

Crosstab
--------------------------------------------------
A crosstab table is a matrix that compares an input column to two other columns over time. It provides statistics that allow for understanding of the frequency of two events occurring together. The following calculations may be performed against a crosstab table.

All crosstab transforms:

#. Input data from a data table that contains all input colums already defined as single-day values. 
#. Input the primary column, for which crosstab calculations will be performed.
#. Input a two-column, delmited list aggregation that represents columns A and B. For example: a column named ``dest_ip_port_list`` with three underscore-separated IP/port pair values (``1.2.3.4_80,1.2.3.4_443,4.5.6.7_181``).
#. Build a table with input column A (``dest_ip``) as the row and input column B (``port``) as the column
#. Compare the two input columns and marks in the table where they intersect
#. Output one (or more) statistics for the multi-day lookback window: ``count_distinct_A``, ``count_distinct_B``, ``count_distinct_AxB``, ``max_marginal_A``, ``max_marginal_B``, ``median_marginal_A``, and ``median_marginal_B``.

.. 
.. someday, with better explanations, it may be useful to have more complete descriptions, with each crosstab calculation getting its own section
.. 
.. * :ref:`process-data-multi-day-crosstab-count-distinct-a`
.. * :ref:`process-data-multi-day-crosstab-count-distinct-b`
.. * :ref:`process-data-multi-day-crosstab-count-distinct-axb`
.. * :ref:`process-data-multi-day-crosstab-max-marginal-a`
.. * :ref:`process-data-multi-day-crosstab-max-marginal-b`
.. * :ref:`process-data-multi-day-crosstab-median-marginal-a`
.. * :ref:`process-data-multi-day-crosstab-median-marginal-b`
.. 


Calculation Types
++++++++++++++++++++++++++++++++++++++++++++++++++
The following multiday crosstab calculations may be performed:

* Use ``count_distinct_A`` for a distinct count of rows that have a value entry, for any column, weighted for each appearance within the duration of the lookback window.
* Use ``count_distinct_B`` for a distinct count of columns that have a value entry, for any row, weighted for each appearance within the duration of the lookback window.
* Use ``count_distinct_AxB`` for the sum of count_distinct_A and count_distinct_B. Put differently, the distinct count of rows and columns that have a value entry, weighted for each appearance within the duration of the lookback window.
* Use ``max_marginal_A`` for the max count of rows that have a value entry, for each column in the table, weighted for each appearance within the duration of the lookback window.
* Use ``max_marginal_B`` for the max count of columns that have a value entry, for each row in the table, weighted for each appearance within the duration of the lookback window.
* Use ``median_marginal_A`` for the median count of rows that have a value entry, weighted for each appearance within the duration of the lookback window.
* Use ``median_marginal_B`` for the median count of columns that have a value entry, weighted for each appearance within the duration of the lookback window.

All crosstab calculations are used to help identify anomalies and outliers in the data by providing an expected value, an actual value, and values that are used for surprise and risk score generation, in addition to preserving the current state of the multiday lookback window for the next day's update.

Crosstab Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The ``cross_tab`` transform for multi-day lookback windows may be configured globally or for specific data sources.

For specific data sources, use the :ref:`configure-entity-attribute-sources` section of the configuration file:

#. Use the ``input_cols`` list to specify the two-column, delmited list aggregation already defined by a single-day table and for which the ``cross_tab`` transform is applied.
#. Use the ``lookback_window`` group to specify the duration of the window (``lookback_duration``) and the number of days from the current day for which the window will save state (``lookback_step``).
#. Optional. The number of tracked A-B values by default is ``1000``. Use the ``state_limit`` setting to modify this number.
#. Set the ``transform_name`` to ``multi_day_cross_tab``.
#. Specify the ``transform_args`` that define the crosstab table: ``A_name``, ``B_name``, ``stats``, and ``stats``. For example:

   .. code-block:: yaml

      transform_args:
        A_name: Dest IP
        B_name: Port
        stats: 'count_distinct_AxB'
        separator: '_' 

.. TODO: Not really sure here ^^^ how the "source_ip" column gets added, if that's the one that gets calculated for all the destination IP and port things. UNCLEAR.

#. Use the ``lookback_window`` group to specify the duration of the window (``lookback_duration``) and the number of days from the current day for which the window will save state (``lookback_step``).

**Example**

The following example shows an entity attribute table that runs the ``count_distinct_AxB`` crosstab transform against destination IP addresses and ports:

.. TODO: This example seems wrong, in that there's no primary input column (i.e. "the one that's measured against the A and B columns")

.. code-block:: yaml

    multi_day_measures_params:
      measures:
        input_cols:
          - 'dest_ip'
          - 'port'
        lookback_window:
          lookback_duration: -3d
          lookback_step: -1d
          missing_values_policy: fill_default
        state_limit: 200
        transform_args:
          A_name: 'Dest IP'
          B_name: 'Port'
          separator: '_'
          stats: 'count_distinct_AxB'
        transform_name: 'multi_day_cross_tab' 
        window_unit: 1d


The following example shows the configuration for a crosstab multi-day transform against columns generated from flow data sources and for all crosstab transforms:

.. code-block:: yaml

   multi_day_crosstab_multistats:
     generate: create_multi_day_measures_table
     udf_source: LIBRARY
     key_columns: ['hostname']
     attribute_columns: []
     type: MODELING_TABLE_ATTRIBUTE
     keep_all_entities: True
     path_add_date_suffix: True
     depends_on: [
       {'datasource': 'netflow', 'phase': 'agg', 'groupby_key': 'day_host'},
       {'datasource': 'netflow_agg_multi_day', 'offset': 1, 'phase': 'generated'}
     ]
     multi_day_measures_params:
       measures:
         - input_cols:
           - netflow_internal_internal_medium_interest_port_dest_ip_port_list
           - netflow_internal_internal_low_interest_port_dest_ip_port_list
           transform_name: multi_day_cross_tab
           transform_args: {
             'A_name' : 'ip',
             'B_name' : 'port',
             'stats' : 'median_marginal_B,max_marginal_B,count_distinct_A,count_distinct_AxB',
             'separator' : '_' }


.. 
.. .. _process-data-multi-day-crosstab-count-distinct-a:
.. 
.. Distinct Rows
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Use ``count_distinct_A`` to count the . 
.. 
.. The ``count_distinct_A`` statistic counts the number of rows on a daily basis, and then again for each day within the lookback window. Each occurence of a row is divided by the total number of occurences.
.. 
.. The following example shows the configuration for a crosstab count distinct A multi-day transform:
.. 
.. .. code-block:: yaml
.. 
..    multi_day_crosstab_count_distinct_a:
..      generate: create_multi_day_measures_table
..      udf_source: LIBRARY
..      key_columns: ['hostname']
..      ...
..      multi_day_measures_params:
..        measures:
..          - input_cols:
..            - netflow_internal_internal_high_interest_port_dest_ip_port_list
..            transform_name: multi_day_cross_tab
..            transform_args: {
..              'A_name' : 'ip',
..              'B_name' : 'port',
..              'stats' : 'count_distinct_A',
..              'separator' : '_' }
.. 
.. 
.. .. _process-data-multi-day-crosstab-count-distinct-b:
.. 
.. Distinct Columns
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Use ``count_distinct_B`` to count xxxxx. 
.. 
.. The ``count_distinct_B`` statistic counts the number of columns on a daily basis, and then again for each day within the lookback window. Each occurence of a column is divided by the total number of occurences.
.. 
.. 
.. The following example shows the configuration for a crosstab count distinct B multi-day transform:
.. 
.. .. code-block:: yaml
.. 
..    multi_day_crosstab_count_distinct_b:
..      generate: create_multi_day_measures_table
..      udf_source: LIBRARY
..      key_columns: ['hostname']
..      ...
..      multi_day_measures_params:
..        measures:
..          - input_cols:
..            - netflow_internal_internal_high_interest_port_dest_ip_port_list
..            transform_name: multi_day_cross_tab
..            transform_args: {
..              'A_name' : 'ip',
..              'B_name' : 'port',
..              'stats' : 'count_distinct_B',
..              'separator' : '_' }
.. 
.. 
.. .. _process-data-multi-day-crosstab-count-distinct-axb:
.. 
.. Distinct Rows/Columns
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Use ``count_distinct_AxB`` to count xxxxx. 
.. 
.. The ``count_distinct_AxB`` statistic counts the number of rows and columns on a daily basis, and then again for each day within the lookback window. Each occurence in both rows and columns is divided by the total number of occurences for those rows and/or columns, respectively.
.. 
.. The following example shows the configuration for a crosstab count distinct AxB multi-day transform:
.. 
.. .. code-block:: yaml
.. 
..    multi_day_crosstab_count_distinct_axb:
..      generate: create_multi_day_measures_table
..      udf_source: LIBRARY
..      key_columns: ['hostname']
..      ...
..      multi_day_measures_params:
..        measures:
..          - input_cols:
..            - netflow_internal_internal_high_interest_port_dest_ip_port_list
..            transform_name: multi_day_cross_tab
..            transform_args: {
..              'A_name' : 'ip',
..              'B_name' : 'port',
..              'stats' : 'count_distinct_AxB',
..              'separator' : '_' }
.. 
.. 
.. .. _process-data-multi-day-crosstab-max-marginal-a:
.. 
.. Maximum Rows
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Use ``max_marginal_A`` to count xxxxx. 
.. 
.. 
.. .. _process-data-multi-day-crosstab-max-marginal-b:
.. 
.. Maximum Columns
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Use ``max_marginal_B`` to count xxxxx. 
.. 
.. The following example shows the configuration for a crosstab max marginal B multi-day transform:
.. 
.. .. code-block:: yaml
.. 
..    multi_day_crosstab_max_marginal:
..      generate: create_multi_day_measures_table
..      udf_source: LIBRARY
..      key_columns: ['hostname']
..      ...
..      multi_day_measures_params:
..        measures:
..          - input_cols:
..            - netflow_internal_internal_high_interest_port_dest_ip_port_list
..            transform_name: multi_day_cross_tab
..            transform_args: {
..              'A_name' : 'ip',
..              'B_name' : 'port',
..              'stats' : 'max_marginal_B',
..              'separator' : '_' }
.. 
.. 
.. .. _process-data-multi-day-crosstab-median-marginal-a:
.. 
.. Median Rows
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Use ``median_marginal_A`` to count xxxxx. 
.. 
.. 
.. .. _process-data-multi-day-crosstab-max-marginal-b:
.. 
.. Median Columns
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Use ``median_marginal_B`` to count xxxxx. 
.. 
.. The following example shows the configuration for a crosstab median marginal B multi-day transform:
.. 
.. .. code-block:: yaml
.. 
..    multi_day_crosstab_median_marginal:
..      generate: create_multi_day_measures_table
..      udf_source: LIBRARY
..      key_columns: ['hostname']
..      ...
..      multi_day_measures_params:
..        measures:
..          - input_cols:
..            - netflow_internal_internal_high_interest_port_dest_ip_port_list
..            transform_name: multi_day_cross_tab
..            transform_args: {
..              'A_name' : 'ip',
..              'B_name' : 'port',
..              'stats' : 'median_marginal_B',
..              'separator' : '_' }
.. 





.. _process-data-multi-day-count-distinct:

Count Distinct
--------------------------------------------------
Multi-day count distinct is an operation that computes the set of distinct values in the input column over a multi-day window. The input column must specify a comma-separated list of distinct values for that day. The multi-day transform takes the input column, combines it with the previous day’s multi-day state (if present), and then produces an output column with a list of distinct values over the lookback window.

The table is then updated on a daily basis starting with the saved state from the previous day's table. Data for the current day is added to the table, data for the oldest day is dropped from the table, and that updated state is saved for the next day.

The list of columns that are input to multi-day tables with ``count_distinct`` transforms may be updated at any time, with the newly-added columns being added to the table after the processing is complete for that day within the defined lookback window.

Count Distinct Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The ``count_distinct`` transform for multi-day lookback windows may be configured globally or for specific data sources. For specific data sources, use the :ref:`configure-entity-attribute-sources` section of the configuration file:

#. Use the ``input_cols`` list to specify all of the columns already defined by a single-day table and for which the ``count_distinct`` transform is applied.
#. Use the ``lookback_window`` group to specify the duration of the window (``lookback_duration``) and the number of days from the current day for which the window will save state (``lookback_step``).
#. Optional. The number of tracked items by default is ``1000``. Use the ``state_limit`` setting to modify this number.
#. Set the ``transform_name`` to ``multi_day_count_distinct``.
#. Use the ``lookback_window`` group to specify the duration of the window (``lookback_duration``) and the number of days from the current day for which the window will save state (``lookback_step``).

.. note:: For global multi-day lookback windows, use the :ref:`configure-multi-day-features` section of the configuration file.

**Example**

The following example shows an entity attribute table that runs the ``count_distinct`` transform against destination and source IP addresses and ports:

.. code-block:: yaml

   multi_day_measures_params:
     measures:
       input_cols:
         - 'dest_ip'
         - 'port'
         - 'source_ip'
       lookback_window:
         lookback_duration: -3d
         lookback_step: -1d
         missing_values_policy: fill_default
       state_limit: 400
       transform_name: 'multi_day_count_distinct' 
       window_unit: 1d

The following example shows the configuration for a count_distinct multi-day transform against columns generated from flow data sources:

.. code-block:: yaml

   multi_day_count_distinct:
     generate: create_multi_day_measures_table
     udf_source: LIBRARY
     key_columns: ['hostname']      # must not contain timescale values!
     attribute_columns: []          # should be empty
     type: MODELING_TABLE_ATTRIBUTE
     keep_all_entities: True
     path_add_date_suffix: True
     depends_on: [
       {'datasource': 'flow', 'phase': 'agg', 'groupby_key': 'day_host'},
       {'datasource': 'flow_agg_multi_day', 'offset': 1, 'phase': 'generated'}
     ]
     multi_day_measures_params:
       measures:
         - input_cols:
           - flow_internal_internal_ICMP_dest_ip_list
           - flow_internal_internal_SNMP_dest_ip_list
           - flow_internal_internal_UDP_dest_ip_list
           - flow_dest_ip_protocol_TCP_NOT_SNMP_list
           transform_name: multi_day_count_distinct



.. _process-data-multi-day-sum:

Sum
--------------------------------------------------
Multi-day lookback windows can be configured to apply the ``sum`` transform. This process inputs a single column that contain single-day values, applies the transform, and then outputs that column to a table that, is aggregated with multi-day values for start/end date and length of lookback window.

The table is then updated on a daily basis starting with the saved state from the previous day's table. Data for the current day is added to the table, data for the oldest day is dropped from the table, and that updated state is saved for the next day.

Sum Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The ``sum`` transform for multi-day lookback windows may be configured globally or for specific data sources. For specific data sources, use the :ref:`configure-entity-attribute-sources` section of the configuration file:

#. Use the ``input_cols`` list to specify all of the columns already defined by a single-day table and for which the ``sum`` transform is applied.
#. Use the ``lookback_window`` group to specify the duration of the window (``lookback_duration``) and the number of days from the current day for which the window will save state (``lookback_step``).
#. Optional. The number of tracked items by default is ``1000``. Use the ``state_limit`` setting to modify this number.
#. Set the ``transform_name`` to ``multi_day_sum``.
#. Use the ``lookback_window`` group to specify the duration of the window (``lookback_duration``) and the number of days from the current day for which the window will save state (``lookback_step``).

.. note:: For global multi-day lookback windows, use the :ref:`configure-multi-day-features` section of the configuration file.

**Example**

The following example shows an entity attribute table that runs the ``sum`` transform for bytes in and bytes out:

.. code-block:: yaml

    multi_day_measures_params:
      measures:
        input_cols:
          - 'bytes_in'
          - 'bytes_out'
        lookback_window:
          lookback_duration: 10d
          lookback_step: 0d
          missing_values_policy: fill_default
        state_limit: 400
        transform_name: 'multi_day_sum' 
        window_unit: 1d


The following example shows the configuration for a sum multi-day transform against columns generated from flow data sources:

.. code-block:: yaml

   multi_day_sum:
     generate: create_multi_day_measures_table
     udf_source: LIBRARY
     key_columns: ['hostname']
     attribute_columns: []
     type: MODELING_TABLE_ATTRIBUTE
     keep_all_entities: True
     path_add_date_suffix: True
     depends_on: [
       {'datasource': 'netflow', 'phase': 'agg', 'groupby_key': 'day_host'},
       {'datasource': 'netflow_agg_multi_day', 'offset': 1, 'phase': 'generated'}
     ]
     multi_day_measures_params:
       measures:


         - input_cols:
           - netflow_internal_bytes_in_sum
           - netflow_external_bytes_out_sum
           transform_name: multi_day_sum

