.. 
.. versive, primary, security engine
.. 

=====================================================
Runbook
=====================================================


This topic describes how to troubleshoot problems that may be encountered when using the |versive| platform.


.. _runbook-external-links:

External Links
=====================================================


* `Apache Hive <https://cwiki.apache.org/confluence/display/Hive/Home;jsessionid=B22BD887ED14A6C99C5699E7EA1690AC>`_
* `Apache Spark Administration Guide <https://www.cloudera.com/documentation/enterprise/latest/topics/administration.html>`_
* `Apache Spark DataFrame API <https://spark.apache.org/docs/1.6.1/api/python/index.html>`_
* `Apache Spark DataFrame Programming Guide <https://spark.apache.org/docs/1.6.0/sql-programming-guide.html>`_
* `Apache Spark Operations Guide <https://www.cloudera.com/documentation/enterprise/latest/topics/operation.html>`_
* `Apache Spark SQL Programming Guide <https://spark.apache.org/docs/latest/sql-programming-guide.html>`_
* `Apache Kafka <https://kafka.apache.org/documentation/>`_
* `Apache Zookeeper <https://cwiki.apache.org/confluence/display/ZOOKEEPER/Index>`_
* `AWS Direct Connect <https://aws.amazon.com/documentation/direct-connect/>`_
* `Cloudera Ecosystem: Apache Hadoop <https://www.cloudera.com/products/open-source/apache-hadoop.html>`_
* `Cloudera Ecosystem: Apache Spark <https://www.cloudera.com/products/open-source/apache-hadoop/apache-spark.html>`_
* `Cloudera Ecosystem: HDFS, MapReduce, YARN <https://www.cloudera.com/products/open-source/apache-hadoop/hdfs-mapreduce-yarn.html#hdfs>`_
* `Cloudera Ecosystem: Key CDH Components <https://www.cloudera.com/products/open-source/apache-hadoop/key-cdh-components.html>`_
* `Cloudera Express <https://www.cloudera.com/documentation/enterprise/latest.html>`_
* `Cloudera Manager-specific <https://www.cloudera.com/documentation/enterprise/latest/topics/cm_intro_primer.html>`_
* `Hadoop 2.7.3 <https://hadoop.apache.org/docs/r2.7.3/>`_
* `Hortonworks Ambari <https://docs.hortonworks.com/HDPDocuments/Ambari/Ambari-2.2.2.0/index.html>`_
* `Hadoop User Experience (Hue): Generic <http://gethue.com/category/tutorial/>`_
* `Hadoop User Experience (Hue): Cloudera <http://www.cloudera.com/documentation/archive/cdh/4-x/4-2-0/Hue-2-User-Guide/hue2.html>`_
* `Red Hat Enterprise Linux <https://access.redhat.com/documentation/en/red-hat-enterprise-linux/?version=6>`_
* `StreamSets <https://streamsets.com/documentation/datacollector/latest/help/#Origins/KConsumer.html#concept_msz_wnr_5q>`_
* `YARN <https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/YARN.html>`_



.. 
.. 
.. Deploy
.. 
.. Manage
..   list of components
..     Hosted on Cloudera vs Hosted on Amazon AWS
..       Cloudera Manager (available in Cloudera Express)
..       Apache Ambari (available in Hortonworks Data Platform)
..       Hadoop Distributed File System (HDFS)
..       Apache Kafka for pipeline
..       YARN services for resoure management
..       Apache Spark services for analytics
..       Apache Hive services for data summizarization, query, analysis
..       Hadoop User Experience (Hue) for Web UI
..       Apache Zookeeper for Hadoop service management
..       AWS Direct Connect for data pipelining (when AWS is used for data pipelining << is this INSTEAD OF KAFKA or IN ADDITION TO KAFKA?)
..       If Cloudera Expresss StreamSets for data pipelining (same question as for AWS Direct Connect)
..     Hadoop 2.6
.. 	kafka
.. 	RHEL 6.5+ / CentOS 6.5+
..     Apache Spark 1.6.1
..     YARN Scheduler
..       hadoop-yarn-resourcemanager
..       hadoop-yarn-proxyserver
..       hadoop-yarn-nodemanager
..     JVM 1.7.0_67
..     GNU C Library (glibc) 2.11+
..     Python 2.7 w/UCS-4 Unicode Libraries
..   networking
..     static IP address
..     ports
..   data sources
..      placeholdered
.. Troubleshooting
..   list of topics
.. 


.. _runbook-apache-spark:

Apache Spark
=====================================================
This section describes how to troubleshoot problems that may be encountered when running Spark.

.. _runbook-apache-spark-multiple-sparkcontexts:

Multiple SparkContexts
-----------------------------------------------------
.. TODO: Very similar to "Existing SparkContexts" and there should also be a topic for ``CLUSTER_INITIALIZED`` also. Each of these should have the same intro, and then a specific "troublshooting" topic should follow. This helps people find the error in a few ways.

A cluster may have a single SparkContexts initialized in the cluster. In some situations, when the Spark cluster is initialized the ``CLUSTER_INITIALIZED`` environment variable is not set to ``compute_version``, which will cause the Spark cluster to remain in an unitialized state. The cause of this error may be one of the following:

* Bad command-line statement
* Missing exception in Python code file

If this error is triggered, an error message similar to the following is returned when running the ``drive`` command:

.. code-block:: console

   Error 10000: Unknown exception. Cannot run multiple SparkContexts at once;
   existing SparkContext(app=Daily run, master=yarn-client) created by __init__
   at /media/ebs0/home/ec2-user/crepe/apt/cluster_mgmt.py:42 .




.. _runbook-compute-cluster:

Compute Cluster
=====================================================
This section describes how to troubleshoot problems that may be encountered with the compute cluster.

.. TODO: Add the Manage the Cluster tasks.

.. _runbook-compute-cluster-batch-script-failure:

Batch Script Failure
-----------------------------------------------------
If a batch script fails while attempting to run several files, do the following:

* Review the output directories using ``hadoop fs -ls`` to pinpoint where the script failed.
* Check the health of the cluster using ``cr cluster health``.
* Review the log files generated by |vse|. The lines include the return values, arguments from the configuration file, and read and write notifications to help with debugging.

.. _runbook-compute-cluster-rdd-sprockets:

RDD Sprockets
-----------------------------------------------------
In some cases, the sprocket associated with a Resilient Distributed Dataset (RDD) requires troubleshooting or monitoring. A sprocket is a single task that is executed as part of a job. A sprocket is created for each RDD used by the |versive| platform.

When troubleshooting or monitoring is required, start by reviewing the log files to identify the sprocket that is associated with a specific RDD, along with the start and stop times for inputs and outputs to code that was executed by the sprocket.

**To identify the sprocket associated with an RDD:**

#. Run the following command:

   .. code-block:: console

      $ crlog.py | crtrace.py -v 2 | less

   which will return the log files for RDDs.

#. Use ``grep`` to search for the RDD. The contents of the log file are hierarchical and are similar to:

   .. code-block:: console

      2015-06-30T21:45:57.6353Z ip-10-230-12-143 8372 I
        [crmr.progress:threading:810]
      Completed Rdd(a3008da9-2c3b-4ae3-b9ea-ad3c6a0e44ed):
        {'ordinal': 1, 'stats': {'time_start': 'Tue Jun 30 21:45:57 2015',
        'lines_in': 1656, 'sprockets_running': 0, 'time_end': 
        'Tue Jun 30 21:45:57 2015', 'duration_coretime': '2ms',
        'duration_walltime': '2ms', 'lines_out': 1, 'sprockets_total': 1,
        'sprockets_done': 1}, 'parents': ('9dbdd42e-7483-4e11-9a49-ccecc68f7584',),
        'id': 'a3008da9-2c3b-4ae3-b9ea-ad3c6a0e44ed', 'desc':
        'ShardReduceRDD:reduce_rows:A:<l>:_reduce_shard_worker'}




.. _runbook-cpio-utility:

cpio Utility
=====================================================
The cpio tool is a general file archiving utility. This section describes how to troubleshoot problems that may be encountered when running the cpio utility.

.. _runbook-cpio-utility-cannot-run:

Cannot Run
-----------------------------------------------------
If the cpio tool cannot run, the following error message is reported:

* ``rootless_lib_install.py: "Failed extracting <package> to <dir>"``

**To verify the cpio tool is running:**

#. Log on to the affected node.
#. Run the following commands:

   .. code-block:: console

      $ id

   then:

   .. code-block:: console

      $ cpio --help

   and then:

   .. code-block:: console

      $ ls -l $<which cpio>

#. If any of these commands do not run verify the user permissions and verify executable permissions via ``chown``, ``chgrp``, and ``chmod`` commands.


.. _runbook-cpio-utility-cannot-write-output:

Cannot Write Output
-----------------------------------------------------
The cpio tool is a general file archiving utility. If the cpio tool cannot write output, one of the following error messages is reported:

* ``rootless_lib_install.py: "Failed extracting <package> to <dir>"``
* ``rootless_lib_install.py: "Could not remove link <path>"``
* ``rootless_lib_install.py: "Error moving <file> to <file>"``
* ``rootless_lib_install.py: "Could not remove <directory>"``

**To verify the cpio tool can write output:**

#. Log on to the affected node.
#. Run the following commands:

   .. code-block:: console

      $ id

   then generate a long list of directories:

   .. code-block:: console

      $ ls -ald <dir>

   then attempt to touch a file in one of those directories:

   .. code-block:: console

      $ touch <dir>/<testfile>

   then attempt to remove that file:

   .. code-block:: console

      $ rm <dir>/<testfile>

#. If any of these commands do not run verify the user permissions and verify executable permissions via ``chown``, ``chgrp``, and ``chmod`` commands.

#. Re-attempt to ``touch`` a file, and then remove that file.

#. If ``touch`` fails, run the following command to verify if the file system is mounted as read/write:

   .. code-block:: console

      $ mount -l
      
#. If the ``ls`` or ``touch`` commands hang or report a timeout, investigate network connection issues.

.. _runbook-cpio-utility-corrupt-file:

Corrupt File
-----------------------------------------------------
If the cpio tool encounters a corrupt file, the following error message is reported:

* ``rootless_lib_install.py: "Failed extracting <package> to <dir>"``

**To resolve a corrupt file:**

#. On the affected node, run:

   .. code-block:: console

      $ md5sum <package_file>

#. Compare the MD5 checksum with the MD5 checksum on the package that was provided by |versive|.
#. If the MD5 checksums are different, delete the local copy.


.. _runbook-engine:

Engine
=====================================================
This section describes how to troubleshoot problems that may be encountered when running the engine.

.. _runbook-engine-bad-command-line-statement:

Bad Command-line Statement
--------------------------------------------------
Verify the command-line statement and ensure that all parameters are specified correctly. Refer to the reference documentation online or view the ``--help`` from the command shell to verify.

Re-run the command.

.. _runbook-engine-existing-sparkcontexts:

Existing SparkContexts
-----------------------------------------------------
.. TODO: Very similar to "Existing SparkContexts" and there should also be a topic for ``CLUSTER_INITIALIZED`` also. Each of these should have the same intro, and then a specific "troublshooting" topic should follow. This helps people find the error in a few ways.

When running the ``drive`` command an error similar to the following may be generated:

.. code-block:: console

   Error 10000: Unknown exception. Cannot run multiple SparkContexts at once;
   existing SparkContext(app=Daily run, master=yarn-client) created by __init__
   at /media/ebs0/home/ec2-user/crepe/apt/cluster_mgmt.py:42 .

This error is often triggered when, during initialization of the Spark cluster the ``CLUSTER_INITIALIZED`` environment variable is not set to ``compute_version``. This will cause the Spark cluster to remain in an un-initialized state and will cause the Spark cluster to re-attempt the initialization on subsequent runs.

The cause of this error may be one of the following:

* Bad command-line statement
* Missing exception in Python code file



.. _runbook-engine-missing-data:

Missing Data
--------------------------------------------------
To help ensure that data is available for all days within the modeling window, the status of data collection processes should be monitored in two places:

* Ensure that data is being added to the data lake from all sources on a daily basis
* Ensure that data is being moved from the data lake to the engine for data processing on a daily basis

If data is missing for some days, the best approach is to treat this data as missing data from a modeling perspective, and then troubleshoot the data collection processes to ensure that data is being added to the data lake correctly and then made available to the engine for processing.


.. _runbook-installer:

Installer
=====================================================
This section describes how to troubleshoot problems that may be encountered when installing, upgrading, or rolling back to a previous version of the |versive| platform or |vse|.

.. _runbook-installer-cr-json-file-issues:

cr.json File Issues
-----------------------------------------------------
If the ``cr.json`` file is lost, unreadable, or corrupt, one of the following messages are reported:

* ``installer/cluster.py: "CrPlatformNotInstalled exception is raised"``
* ``installer/tasks.py: "Could not fetch cr.json for an unknown reason"``

**To resolve cr.json file issues:**

#. Confirm the location of the ``cr.json`` file and the permissions assigned to it.
#. Use a JSON validator to ensure that the format of the ``cr.json`` file is correct.
#. Review the contents of ``cr.json`` to ensure that all required items are listed with valid values.

.. note:: If the error is associated with ``tasks.py``, additional information should be present in the error text that should help with troubleshooting.

.. _runbook-installer-directory-contents-changed:

Directory Contents Changed
-----------------------------------------------------
If the installer found changes to directory contents, the following error message is reported:

* ``rootless_lib_install.py: "Could not remove <directory>"``

**To resolve possible changes to directory contents:**

#. Look in the directory specified by the error message, and then compare the contents in that directory to the contents of ``/opt/cr/lib/thirdparty`` or the rootless third-party library directory.
#. If any files are missing, copy them into place.
#. If copying files into place fails, address the permissions or file system issues that are preventing this action.
#. Remove the source <directory> once the manual copy is complete.
#. If that fails, address the permissions or file system issue preventing the removal of the source <directory>.
#. Re-run the installer.

.. _runbook-installer-error-executing-task:

Error Executing Task
-----------------------------------------------------
If there is error while executing a task, the following message may be reported:

* ``installer/tasks.py: "Error executing task"``

This is an indicator that Fabric can connect to the remote host, but a software or node configuration error is preventing Fabric from completing the task.

**To resolve a Fabric error:**

#. Verify the syntax of the command that was run.
#. Verify that the expected package is on the host.
#. Verify that permissions are correct.
#. Contact |versive| support for assistance.

.. _runbook-installer-question-subsystem-errors:

Question Subsystem Errors
-----------------------------------------------------
If there is a software error with the installer question subsystem, such as

* What is the IP address of the master node?
* What should the cluster be named?
* What is the path of the local data directory on each node?
* and so on.

When issues arise with the installer question subsystem, the following message may be reported:

* ``installer/interface.py:Exception``

If this error occurs, contact |versive| support for assistance.

.. _runbook-installer-unexpected-file:

Unexpected File
-----------------------------------------------------
If the installer encounters an unexpected file, the following error message is reported:

* ``rootless_lib_install.py: "Target <file> exists but is not a <symlink>"``

The unexpected file may be a result of a manual library installation. Move the unexpected file to a new location outside of the intended installer directory, and then try installing again.

.. _runbook-installer-unreadable-directory:

Unreadable Directory
-----------------------------------------------------
If the working or temporary directories become unreable, one of the following messages are reported:

* ``rootless_lib_install.py: "Failed extracting <package> to <dir>"``
* ``rootless_lib_install.py: "Failed extracting <package> to <dir>"``
* ``rootless_lib_install.py: "Error moving <file> to <file>"``
* ``rootless_lib_install.py: "Could not remove <directory>"``

**To resolve an unreadable directory:**

#. Log on to the affected node.
#. Run the following command:

   .. code-block:: console

      $ id

   and then review the results.

   If one (or more) expected groups are missing from the printed results, log out, and then log back in.

   If the user returned by the command is not the correct user, log out, and then log back in as the correct user.

   If the user and/or groups returned by the command do not have read and write access to the current directory, assign the appropriate permissions or choose another directory from which to troubleshoot.

#. Run the following command:

   .. code-block:: console

      $ ls -l <temporary directory>

   or:

   .. code-block:: console

      $ ls -l .

   If either of these commands hangs or reports a timeout, the network file system on which the directory is hosted may be inaccessible, a network identity service---Active Directory, LDAP, NIS, Kerberos---may have experienced some type of failure, or the network itself may be down.


.. _runbook-modeling:

Modeling
=====================================================
This section describes how to troubleshoot problems that may be encountered when ingesting, transforming, joining, or otherwise working with data in the |versive| engine to learn predictive models.

.. _runbook-modeling-binary-classification-failure:

Binary Classification Failure
-----------------------------------------------------
If the ``learn_model`` phase fails when attempting to learn a binary classification model, the output may have been incorrectly split into train, tune, and test tables. Data must be split so that each of the train, tune, and test tables contain both binary classification labels. For example, the train table cannot contain only rows where the target evaluates to ``1`` and the tune table cannot contain only rows where the target evaluates to ``0``. If this situation occurs, change how data is split for the binary classification models for the train, tune, and test tables so that each table contains a random assortment of classification labels.

.. _runbook-modeling-cannot-join-data-tables:

Cannot Join Data Tables
-----------------------------------------------------
Sometimes joining multiple data tables into a single table is troublesome. In these situations, try the following to work around and/or identify causes:

* Review the data after running :ref:`api-relate` to verify that the correct ``key_columns`` are being used to join the tables. These should be user and host identifiers.
* Call :ref:`api-count-frequencies` and/or :ref:`api-summarize-table` to identify the number of unique IDs present in each column.

  .. note:: Two non-key columns can have the same name because every column name in a table must be unique. Use :ref:`api-rename-columns` or :ref:`api-delete-columns` to handle these situations.

* Before and after joins, print out the row count, column names, and some arbitrary example rows to verify that the tables are being joined as expected. This can help diagnose exactly where problems occur.

.. _runbook-modeling-cannot-locate-data-file:

Cannot Locate Data File
-----------------------------------------------------
If the data file cannot be located, verify that the connection to the storage cluster is correct and that the value for the ``path`` argument is the correct path for the file to be loaded. Use a single processor. Load the data file from a remote repository, such as HDFS.

.. _runbook-modeling-cannot-save-csv-excel:

Cannot Save CSV / Excel
-----------------------------------------------------
Saving a table to comma-separated values (CSV) or Microsoft Excel file formats uses only the memory available to the primary node, and not the memory available to the entire cluster. If a large table is being saved and it is running out of memory, use :ref:`api-split-table` to break it into smaller tables that are saved individually as CSV or Excel files, and then merged later.

.. note:: CSV and Excel files may be saved to local storage even when working in a multi-node storage cluster.

.. _runbook-modeling-column-header-values:

Column Header Values
-----------------------------------------------------
If a schema file is not specified or ``has_headers`` is set to ``True`` when loading a table that has headers, the headers are considered string data in the first row of the data table. This may cause problems when the rest of the column contains data of a non-string data type.

.. _runbook-modeling-empty-columns:

Empty Columns
-----------------------------------------------------
After loading a data file the column names are visible, but contain no data. This may occur when viewing the modeling table itself or, if data is stored on HDFS, when running the following commands:

.. code-block:: console

   $ hadoop fs -cat schema.txt

and:

.. code-block:: console

   $ hadoop fs -cat data/000 | awk -F="\t" '{ print $4 }' | sort | uniq -c

**To resolve empty columns:**

#. Verify at the schema file matches the data. Update the schema as necessary.
#. Review the source data set, working backward through ``aggregate``, ``transform``, and ``parse`` phases to identify the location at which the columns are set to empty.

.. _runbook-modeling-out-of-memory:

Out Of Memory
-----------------------------------------------------
If the compute cluster runs out of memory when running computations, try the following to work around and/or identify causes:

* Reset the cluster
* Add print statements to code, and then use those print statements to help identify where the issue is occurring
* Break the task up into smaller components
* Comment out some of the transforms
* Run the script on a subsample of the data
* Run the engine phase on a day that contains less data

.. _runbook-modeling-slow-computations:

Slow Computations
-----------------------------------------------------
Some computations are memory intensive. Use the following guidelines to identify commands that take a long time to process, conserve memory, and speed up processing.

* Develop a subsample of data, get the model working correcly with that subsample, and then run the model against the full dataset.

* Use :ref:`checkpoint functions <api-checkpoint-functions>` and save frequently. Accessing data from memory is faster than accessing memory from disk; both are much faster than computations. Because checkpointing holds the results of prior computations in-memory, it can help prevent having to run the same computations multiple times.

* If the data is on HDFS, check the size of predecessor files. These should be at least as big as the HDFS block size. Set the ``reshard`` setting to ``True``.

* If the data set is large, running a computationally expensive command---such as :ref:`api-measure-learning-curve`---against a dataset larger than the amount of memory available to the compute cluster, first save the work to disk, reset the cluster to clear the memory, and then re-run the computationally expensive command.

  The phases for |vse| are generally expensive to run. It's important to monitor the process to help ensure each phase is on track to complete successfully.

.. _runbook-modeling-slow-reporting:

Slow Reporting
-----------------------------------------------------
Sometimes, running :ref:`api-measure-input-importance` on a short, wide table that has relatively few rows in relation to the number of columns can take much longer than expected because the data is improperly distributed among the compute nodes in the cluster. Remove columns to speed up computation.


.. _runbook-network-access:

Network Access
=====================================================
This section describes how to troubleshoot problems that may be encountered with the network.

.. _runbook-network-access-interrupted:

Access Interrupted
-----------------------------------------------------
If network access is interrupted, one of the following messages may be reported:

* ``installer/tasks.py: "Problem getting users uid value"``
* ``installer/tasks.py "Got a non-numeric uid"``

**To resolve network interruptions:**

#. On the node in question, run:

   .. code-block:: console

      $ id -u

#. If the ``id`` command is not found or if permission is denied, install the ``id`` tool and/or fix permissions so that it may be run.


.. _runbook-network-access-cannot-connect-to-node:

Cannot Connect to Node
-----------------------------------------------------
If there is a problem connecting to a node, the following message may be reported:

* ``installer/tasks.py: "Problem connecting to node"``

**To resolve node connection issues:**

#. SSH from the primary node to the node that is reporting the error. Use the ``-v`` (verbose) option to output additional debugging information.
#. Resolve any authentication, authorization, or access problems at the root cause of passwordless SSH failures.
#. If these steps do not resolve the issue, Fabric may be encountering an unexpected error when making its connection. Review additional debugging information provided by Fabric if it is available.


.. _runbook-python:

Python
=====================================================
The following error types may be encountered when syntax errors are present in Python code.


.. _runbook-python-function-returns-bad-string:

Function Returns Bad String
-----------------------------------------------------
In Python, functions that do not require an argument to be specified must be followed by parentheses (``()``) to be executed as a command. If the parentheses are not present the Python interpreter returns a definition of the function, which is obfuscated, and as such returns a long string of random characters.

.. _runbook-python-invalid-argument-type:

Invalid Argument Type
-----------------------------------------------------
The |vse| checks that arguments passed to functions are of a valid type. By default, when an argument is of an invalid type the |vse| will return an error. This behavior may be updated to return a warning instead.

**To set invalid argument type errors to warnings:**

#. Run the following command:

   .. code-block:: console

      $ import crmr.knobs crmr.knobs.TYPE_CHECK_LEVEL = 0

.. _runbook-python-lists-and-tuples:

Lists and Tuples
-----------------------------------------------------
Lists contain multiple items where order is not important. Lists are a comma-separated list of items enclosed in small brackets. For example: ``['item1', 'item2', 'item3']``.

Tuples contain multiple items where order is important, such as when indicating the names of columns that correspond to one another in different tables. Tuples are a comma-separated list of items enclosed in parentheses. For example: ``('column1', 'column2', 'column3')``.

Problems may be encountered when:

* The closing bracket (``]``) is missing for a list
* The closing parentheses (``)``) is missing for a tuple
* Commas (``,``) are missing within the list of items
* Quotation marks (``'``) are missing around strings within the list of items

.. _runbook-python-missing-exceptions:

Missing Exceptions
-----------------------------------------------------
In Python code for any |versive| engine where the ``except`` statement is present, it is recommended to not hide the error. For example, code similar to the following will hide the error:

.. code-block:: python

   except cr.user_error.CRError:

However, code similar to the following will not hide the error:

.. code-block:: python

   except cr.user_error.CRError as ex:

which can then be called with ``print ex`` or ``log.error(ex)`` similar to:

.. code-block:: python

   try:
     ret_val = _initialize_cluster(default_connection=config.default_connection,
                                   use_single_processor=single_proc,
                                   compute_version=compute_version,
                                   cluster_args=cluster_args)    
     eexcept cr.user_error.CRError as ex:
     ret_val = _initialize_cluster(default_connection=config.default_connection,
                                   use_single_processor=single_proc,
                                   compute_version=compute_version)
     log.error(ex)
     CLUSTER_INITIALIZED = compute_version

.. _runbook-python-object-not-defined:

Object Not Defined
-----------------------------------------------------
String values should be enclosed in quotation marks (``'``) and column names (in particular) must be spelled correctly. When an error message indicates an object cannot be defined, double-check quotations and spelling within and around strings in the related code and also that the correct prefix is used in any funtcion calls.


.. _runbook-user-access:

User Access
=====================================================
This section describes how to troubleshoot problems that may be encountered with users, user permissions, and user access.

.. _runbook-user-access-authentication-error:

Authentication Error
-----------------------------------------------------
If there is an authentication error, the following error messages may be reported:

* ``installer/tasks.py: "Problem getting users uid value"``
* ``installer/tasks.py "Got a non-numeric uid"``

**To resolve authentication errors:**

#. On the node in question, run:

   .. code-block:: console

      $ id -u

#. If the ``id`` command is not found or if permission is denied, install the ``id`` tool and/or fix permissions so that it may be run.
#. If the command runs, but returns an error, authentication may be misconfigured or a network services problem is preventing the tool from running. Work with the system admistrator to resolve this issue.

.. _runbook-network-access-incorrect-sudo:

Incorrect sudo
-----------------------------------------------------
Access via ``sudo`` to the root account is required. If a user lacks this level of access, one of the following messages may be reported:

* ``installer/tasks.py: "Looks like user does not have sudo access"``
* ``installer/tasks.py: "User has sudo access but not as root user``

**To resolve incorrect sudo access for users:**

#. On the node in question, run:

   .. code-block:: console

      $ sudo whoami

#. If the ``sudo`` command is not found, install ``sudo``.
#. If the command reports that the user lacks appropriate administrator rights, add permissions with ``visudo``.



