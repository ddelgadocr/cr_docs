.. 
.. versive, primary, security engine
.. 

==================================================
Blocks
==================================================

.. include:: ../../includes_terms/term_blocks.rst

.. warning:: Blocks interaction with the pipeline has the following limitations:

   * Blocks do not currently interact with storage; individual blocks and/or block graphs must be inserted into an entity attribute table or a phase to interact with storage
   * Blocks do not currently understand time-based dependencies on prior blocks in a block graph because blocks only process data for the current day; individual blocks must be inserted into an entity attribute table to handle time-based dependencies
   * Blocks are used only to process DNS, flow, and proxy data; blocks are not used to model data


.. _blocks-about:

About Blocks
==================================================
Blocks are the primary interface to the engine pipeline for processing data. A data processing pipeline has the following phases:

#. Parse data
#. Standardize data
#. Transform data
#. Aggregate data

Each phase is designed as a block graph. Each block graph contains individual blocks, that are configured to run in a specific order. Each block has a contract that defines the columns that are input to that block, and then the columns that are output from that block. This input and output is clearly visible as the data moves through each phase in the data processing pipeline, from the original parsing of raw data to the aggregated output that is ready for modeling.

.. note:: Most data is processed by these phases, and typically is processed in the order presented above; however, each data source that is ingested to the pipeline may have unique requirements.




.. _blocks-base-block-types:

Base Block Types
==================================================
.. include:: ../../includes_terms/term_table_dataframe.rst

Blocks provides the following base classes as interfaces to dataframes, each with a specific purpose and use case:

* :ref:`blocks-base-block-type-bundle`
* :ref:`blocks-base-block-type-frame`

.. warning:: The base class types are important because they determine :ref:`the input and output rules for that block <blocks-block-graphs-input-output-rules>`.


.. _blocks-base-block-type-bundle:

Bundle Blocks
--------------------------------------------------
.. TODO: Single-source this with the overview of the base block class.

A bundle block is useful for interacting with multiple sets of tabular data and their associated rows, observations, and measurements, for both input and output. A bundle block:

* Inputs multiple dataframes
* Outputs multiple dataframes
* Output columns are identical to input columns, unless otherwise specified in the configuration for that block

.. _blocks-base-block-type-frame:

Frame Blocks
--------------------------------------------------
.. TODO: Single-source this with the overview of the base block class.

A frame block is useful for interacting with tabular data and its associated rows, observations, and measurements, such as adding columns or replacing the contents of columns that already exist in the tabular data. A frame block:

* Inputs a single dataframe as a Spark DataFrame
* Outputs a single dataframe as a Spark DataFrame
* Output columns are identical to input columns, unless otherwise specified in the configuration for that block


.. _blocks-block-graphs:

Block Graphs
==================================================
A block graph is a directed acyclic graph that defines a set of data processing for the engine pipeline.

* The engine pipeline must define at least one block graph and a block graph must include at least one block
* A typical block graph contains multiple blocks that use dependencies to define the order in which blocks are processed
* A typical pipline uses multiple block graphs. For example, most pipelines define at least three block groups: a block graph for parsing data, a block graph for standardizing data, and a block graph for transforming data, after which the processed data is handed off for modeling
* A block graph, and all of the individual blocks in that block graph are defined in a dedicated location in the the :ref:`configure-block-graphs` section of the configuration file.

.. TODO: Update the configuration file with the blocks_graph + subset of settings.

For example, an illustration of a block graph:

.. image:: ../../images/blocks.svg
   :width: 600 px
   :align: center


.. _blocks-block-graphs-configure:

Configure Block Graphs
--------------------------------------------------
A block graph is defined in the :ref:`configure-block-graphs` section of the configuration file. The ``block_graph`` setting group has two main sections:

* One defines each block in the block graph
* One that defines any dependencies blocks in the block graph have on each other

.. code-block:: yaml

   block_graph:
     <block_graph_name>
       depends:
         <block_name>:
           - block_name: 'block_name'
             frame_key: {}
           - block_name: 'outside_block_graph'
             frame_key: {}
         ...
       blocks:
         <block_name>:
           class_name: <class_name>
           ...
         ...

.. TODO: Below. Verify the table that lists the phase/output names for outside_block_graph from time to time.

where:

* ``block_graphs`` is the top-level configuration grouping for all blocks and all block graphs for the engine pipeline.
* ``<block_graph_name>`` is the name of the block graph; one (or more) block graphs may be listed; blocks must be located under the block graph to which they are associated.
* ``blocks`` is one (or more) blocks that are associated with the named block graph and under which this block's configuration settings are specified.
* ``depends`` is a list of one (or more) blocks with a named dependency declared for each block in the list.

  Blocks may have dependencies on data processing that occurs outside the block graph. Instead of declaring the name of a block for the dependency, use ``outside_block_graph`` as the block name.

  When ``outside_block_graph`` is specified, set ``frame_key`` to declare the output name of an application phase, and then (optionally) the name of a ``dataset``. Possible output names for application phases:

  .. list-table::
     :widths: 250 250
     :header-rows: 1

     * - Phase
       - Output Name
     * - ``aggregate``
       - ``agg``
     * - ``generate_lookup_table``
       - ``lookup``
     * - ``generate_modeling_table``
       - ``modeling_table``
     * - ``generate_transition_table``
       - ``transition_table``
     * - ``learn_models``
       - ``models``
     * - ``parse``
       - ``parsed``
     * - ``postprocess``
       - ``results``
     * - ``predict``
       - ``scored``
     * - ``prepare``
       - ``prepared``
     * - ``process``
       - ``transformed``
     * - ``standardize_keys``
       - ``standardized``
     * - ``transform``
       - ``transformed``

  When a block depends on another block in the same block graph, the ``frame_key`` value should be empty: ``frame_key: {}``.

  The following example creates a dependency on the current day's sharing features entity attribute table, which is generated outside of the block graph:

  .. code-block:: yaml

     depends:
       - clean_ips:
         block_name: 'outside_block_graph'
         frame_key: { 'stage': 'generated', 'dataset': 'sharing_features' } 

  The following example creates a dependency for the ``clean_user`` block on the ``clean_ips`` block that outputs the data used as the input to the ``clean_user`` block:

  .. code-block:: yaml

     depends: 
       clean_user:
         - block_name: clean_ips
           frame_key: {}

  The following example creates a dependency for the ``repartition_user`` block on the application phase that generates the lookup table (``generate_lookup_table``), which allows the data in the lookup table to populate column data:

  .. code-block:: yaml

     depends: 
       repartition_user:
         - block_name: outside_block_graph
           frame_key: { 'stage': 'lookup' }  # 'lookup' is the output name for the
                                             # generate_lookup_table application phase



Example
++++++++++++++++++++++++++++++++++++++++++++++++++
.. TODO: Rewrite the following:

The following example shows:

* A block graph named ``std_graph`` that has three three blocks: ``bkup``, ``ip_reg``, and ``parse_date``.
* The ``bkup`` block depends on data from outside the block graph (``'outside_block_graph'``). It uses the ``frame_key`` to specify the parsed stage for the netflow dataset (``{'stage': 'parsed', 'dataset': 'netflow'}``).
* The ``ip_reg`` block takes as its input the output of the block for which it has the named dependency: ``bkup``.
* The ``parse_date`` block takes as its input the output of the block for which it has the named dependency: ``ip_reg``.

.. code-block:: yaml

   block_graphs:
     std_graph:
       depends:
         bkup:
           - block_name: 'outside_block_graph'
             frame_key: {'stage': 'parsed', 'dataset': 'netflow'}
         ip_reg:
           - block_name: bkup
             frame_key: {}
         parse_date:
           - block_name: ip_reg
             frame_key: {}
       blocks:
         bkup:
           class_name: 'BackupBlock'
           columns:
             - name: source_ip
               type: str
             - name: timestamp
               type: str
         ip_reg:
           class_name: 'StandardizeIPAddressBlock'
           columns:
             - name: source_ip
               type: str
             - name: dest_ip
               type: str
         parse_date:
           class_name: 'StringToTimestampBlock'
           column:
             - name: timestamp
               type: str


.. _blocks-block-graphs-run-in-engine:

Run Blocks in the Engine
--------------------------------------------------
.. TODO: Verify the block_name setting under the sources group of settings in the CONFIGURATION_FILE.

.. TODO: In-between TODOs (below this one!) is some "unedited" content that does belong in this section, but still needs to be properly edited and integrated into this section.

So far we have seen how Blocks interact with each other, but not how they run as part of the application. Blocks currently do not load or save from disk: they purely transform data. However, the application, or ``drive`` in particular, works by loading and saving data from disk.

As an interim measure, block graphs can hijack entity attribute tables or phases in the existing application. When ``drive`` gets to that entity attribute table or phase, it runs the block graph instead of the entity attribute table or phase. Then it saves the output of the block graph to where it would have saved the entity attribute table or phase's output. As long as the contract with dependent entity attribute tables or phases is honored, the rest of the application is none the wiser. Thus, block functionality can be inserted into the application.

To hijack a phase or entity attribute table, simply insert the block name within the config like so:

.. code-block:: yaml

   entity_attribute_sources:
     url_regdomain_percentile_intermediate:
       block_name: 'intermediate_rank_graph'

   sources:
     proxyweb:
       standardize:
         block_name: 'standardize_graph'

The rest of the settings for the entity attribute table or phase still apply, such as 'depends_on' to control what the dependencies are. Blocks do not have a concept of dependencies outside the block graph they're in yet. You still have to use entity attribute tables to specify that a particular artifact depends on previous day versions of itself or other artifacts.

.. TODO: In-between TODOs (above this one!) is some "unedited" content that does belong in this section, but still needs to be properly edited and integrated into this section.

To save the output of a block graph it must be embedded in an engine phase or in an entity attribute table.

w/Entity Attribute Tables
++++++++++++++++++++++++++++++++++++++++++++++++++
To embed a block graph in an entity attribute table, use the :ref:`configure-entity-attribute-sources` section in the configuration and specify the name of the block with the ``block_name`` setting. For example, the block-specific settings for an entity attribute table  named ``new_std``:

.. code-block:: yaml

   entity_attribute_sources:
     new_std:
       block_name: 'std_graph'
         type: PRE_MODELING_TABLE_ATTRIBUTE
       depends_on:
         - datasource: netflow
           phase: parsed

.. note:: Beyond the block-specific settings, a block that is defined within an entity attribute table is configured in the same way as any other entity attribute table.

Use the :ref:`configure-block-graphs` section in the configuration file to define the rest of this block:

.. code-block:: yaml

   block_graphs:
     new_std:
       blocks:
         std_graph:
           class_name: BlockType
           outputs:
             - ...


w/Engine Phases
++++++++++++++++++++++++++++++++++++++++++++++++++
To embed a block graph in an engine phase, use the :ref:`configure-entity-attribute-sources` section in the configuration file and specify name of the block with the ``block_name`` setting. For example:

.. code-block:: yaml

.. code-block:: yaml

   block_graphs:
     count_rows:
       ...
       inputs:
         - datasource: proxyweb
           phase: parsed
         - datasource: proxyweb
           phase: parsed_error
     ...
   ...
   entity_attribute_sources:
     process_rows_count:
       block_name:
         - count_rows

and then run the :ref:`command-drive` command similar to:

.. code-block:: console

   $ apt.py drive --output_phase=generated --dataset=netflow ...



.. _blocks-block-graphs-column-validation:

Column Validation
--------------------------------------------------
All block graphs are validated to ensure that the tabular data contained within individual blocks correct. All input to and output from all blocks in the block graph is validated for the correct columns and data to ensure that:

* All blocks receive the correct input of tabular data from the previous block (or blocks) in the block graph
* All blocks provide the correct output of tabular data to the next block (or blocks) in the block graph

Validation occurs even when data is not running through the engine. If data is run through the engine that does not pass validation, the engine stops immediately and generates an error report.


.. _blocks-block-graphs-input-output-rules:

Input and Output Rules
--------------------------------------------------
Rules for input to and output from blocks in a block graph depend primarily on the individual block type, as described in the following table:

.. list-table::
   :widths: 150 250 250
   :header-rows: 1

   * - Block Type
     - Input Rules
     - Output Rules
   * - Bundle Blocks
     - One (or more) dataframes; input columns are assumed to be identical to output, unless declared in the configuration file.
     - One (or more) dataframes; output columns are assumed to be identical to input, unless declared in the configuration file.
   * - Frame Blocks
     - One Spark DataFrame; input columns are assumed to be identical to output, unless declared in the configuration file.
     - One Spark DataFrame; output columns are assumed to be identical to input, unless declared in the configuration file.


Example Block Graph
--------------------------------------------------
The following example shows a simple block graph with two blocks:

* The ``parse_flow`` block parses the raw log files for flow data
* Parsed data is split into two groups: successfully parsed data and unsuccessfully parsed data
* The ``sample`` block takes as input the successfully parsed data, and then samples that data to 5%, and then outputs that data
* The block graph outputs two sets of data: one for successfully parsed data that is sampled at 5%, and then another for unsuccessfully parsed data

.. code-block:: yaml

   block_graphs:
     prs_graph:
       depends:
         sample:
           - block_name: parse_flow
             frame_key: {'stage': 'parsed'}
       blocks:
         parse_flow:
           class_name: 'ParseBlock'
           path: "/path/to/flow/data/"
           schema: "/path/to/flow.yaml"
           date_format: epoch
           source: flow
         sample:
           class_name: 'SampleBlock'
           column:
             - name: source_ip
               type: str
           percent: 5
           input:
             id:
               name: 'name'
             columns:
               - name: source_ip
                 type: str
           output:
             id:
               name: 'name'
             columns:
               - name: source_ip
                 type: str
         outputs:
           - block_name: parse_flow
             frame_key: {'stage': 'parsed_error'}
           - block_name: sample
             frame_key: {'stage': 'parsed'}


.. _blocks-library:

Blocks Library
==================================================
.. include:: ../../includes/blocks_library_list.rst

.. note:: Use the :doc:`extensions package <extensions>` to define custom blocks.



.. _blocks-library-addcolumnbycategory:

AddColumnByCategoryBlock
--------------------------------------------------
Use an add column-by-category block to copy a value from a column based on values in other columns when matching categories are present. This block automatically creates columns with the copied value or a ``None`` value if the matching condition failed. For example, this block can be used to create a column with "bytes sent", but only when those bytes were sent to external IP addresses.

.. warning:: The order of fields listed under ``category`` for ``categories_to_values`` must match the order of fields listed under ``category`` for ``category_columns``.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'AddColumnByCategoryBlock'
           category_columns:
             - name: 'column_name'
               type: data_type
           categories_to_values:
             - category: 'external' # or 'internal'
               output_name: data_type
               value_column:
                 - name: 'column_name'
                   type: data_type
         ...
       ...

           category_columns:

           categories_to_values:


Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'AddColumnByCategoryBlock'``.

**categories_to_values** (required, str)
   An association of categories and values to be applied.

   **category** (required, str)
      The category to be applied when a value matches the pattern. Possible values: ``external``, ``internal``, or a custom value. If a row does not match any of these patterns it will be assigned a ``None`` value.

      Use ``external`` to categorize an external IP address.

      Use ``internal`` to categorize an internal IP address.

   **output_name** (optional, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

   **value_column** (required, str)
      The column to which the cate.

**category_columns** (required, str)
   A list of columns to be categorized, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following example show how to use the add column-by-category block.

Internal A Records
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. include:: ../../includes_terms/term_dns_record_a.rst

The following configuration will identify named domains that are internal and have an A request record:

.. code-block:: yaml

   identify_internal_a_records:
     class_name:
       symbol: 'AddColumnByCategoryBlock'
     category_columns:
       - name: named_domain_category
         type: str
       - name: request_type
         type: str
     categories_to_values:
       - category: ['internal', 'A']
         value_column:
           name: named_domain
           type: str

A table with the following inputs:

.. code-block:: sql

   ----- -------------- ----------
    ...   named_domain   category
   ----- -------------- ----------
    ...   domain         internal 
    ...   domain         external 
    ...   domain         internal 
   ----- -------------- ----------

will be updated with a new column that identifies named domains that are both internal and are associated with an A request record:

.. code-block:: sql

   ----- -------------- ---------- -------------------------
    ...   named_domain   category   internal_A_named_domain 
   ----- -------------- ---------- -------------------------
    ...   domain         internal   named_domain            
    ...   domain         external                           
    ...   domain         internal   named_domain            
   ----- -------------- ---------- -------------------------



Internal AAAA Records
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. include:: ../../includes_terms/term_dns_record_aaaa.rst

The following configuration will identify named domains that are internal and have an AAAA request record:

.. code-block:: yaml

   identify_internal_a_records:
     class_name:
       symbol: 'AddColumnByCategoryBlock'
     category_columns:
       - name: named_domain_category
         type: str
       - name: request_type
         type: str
     categories_to_values:
       - category: ['internal', 'AAAA']
         value_column:
           name: named_domain
           type: str

A table with the following inputs:

.. code-block:: sql

   ----- -------------- ----------
    ...   named_domain   category
   ----- -------------- ----------
    ...   domain         internal
    ...   domain         external
    ...   domain         internal
   ----- -------------- ----------

will be updated with a new column that identifies named domains that are both internal and are associated with an AAAA request record:

.. code-block:: sql

   ----- -------------- ---------- ----------------------------
    ...   named_domain   category   internal_AAAA_named_domain 
   ----- -------------- ---------- ----------------------------
    ...   domain         internal   named_domain               
    ...   domain         external                              
    ...   domain         internal   named_domain               
   ----- -------------- ---------- ----------------------------



Internal PTR Records
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. include:: ../../includes_terms/term_dns_record_ptr.rst

The following configuration will identify named domains that are internal and have a PTR request record:

.. code-block:: yaml

   identify_internal_a_records:
     class_name:
       symbol: 'AddColumnByCategoryBlock'
     category_columns:
       - name: named_domain_category
         type: str
       - name: request_type
         type: str
     categories_to_values:
       - category: ['internal', 'PTR']
         value_column:
           name: named_domain
           type: str

A table with the following inputs:

.. code-block:: sql

   ----- -------------- ----------
    ...   named_domain   category
   ----- -------------- ----------
    ...   domain         internal
    ...   domain         external
    ...   domain         internal
   ----- -------------- ----------

will be updated with a new column that identifies named domains that are both internal and are associated with a PTR request record:

.. code-block:: sql

   ----- -------------- ---------- ---------------------------
    ...   named_domain   category   internal_PTR_named_domain 
   ----- -------------- ---------- ---------------------------
    ...   domain         internal   dest_ip                  
    ...   domain         external                            
    ...   domain         internal   dest_ip                  
   ----- -------------- ---------- ---------------------------





.. _blocks-library-addconstantcolumn:

AddConstantColumnBlock
--------------------------------------------------
Use an add constant columns block to add a column to a table, and then populate that column with a single, constant value.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'AddConstantColumnBlock'
           column_name: 'column_name'
           column_value: 'column_name'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'AddConstantColumnBlock'``.

**column_name** (optional, str)
   A name of the column for which a single, constant value is to be applied.

**column_value** (optional, str)
   The value to be applied to all rows for the specified column.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will add a column named ``constant`` with a value of ``12345`` to all rows in the table:

.. code-block:: yaml

   add_constant_column:
     class_name:
       symbol: 'AddConstantColumnBlock'
     column_name: 'constant'
     column_value: '12345'

A table with the following inputs:

.. code-block:: sql

   ---------------
    source_ip     
   ---------------
    192.168.1.1   
    192.168.20.14 
    192.168.1.1   
   ---------------

will be updated to:

.. code-block:: sql

   --------------- ----------
    source_ip       constant 
   --------------- ----------
    192.168.1.1     12345  
    192.168.20.14   12345  
    192.168.1.1     12345  
   --------------- ----------






.. _blocks-library-addsubnetlabels:

AddSubnetLabelsBlock
--------------------------------------------------
Use an add subnet labels block to add columns to the modeling table for subnet labels for source and destination IP addresses.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'AddSubnetLabelsBlock'
           subnet_labels:
             path: /path/to/subnet_labels.csv
             has_headers: True
             columns: ['label', 'address', 'mask']
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

.. TODO: The subnet_labels (and sub-setting) are identical to subnet label configuration settings under generate_modeling. Sync those up at some point. What about the delimiter setting?

**class_name** (required, str)
   The name of the block. Must be set to ``'AddSubnetLabelsBlock'``.

**subnet_labels** (required, str)
   Subnet labels can help the |vse| identify subsets of an internal network. Use a CSV file to specify a list of subnets for which labels should be applied. The |vse| will process the ``hostname`` and ``fqdn`` columns against the contents of this CSV file to add source and destination IP address labels to the modeling table.

   The ``subnet_labels`` settings group is the category for adding subnet labels to modeling tables. When subnet labels are added to modeling tables, the ``hostname`` and ``fqdn`` columns are processed against the IP addresses specified in a CSV file, after which ``src_ip_subnet_label`` and ``dest_ip_subnet_label`` columns are added to the modeling table.

   **columns** (str, optional)
      A list of three columns in the following order: ``label``, ``address``, and ``mask``.

   **has_headers** (bool, optional)
      Specifies if the subnet has headers. Default value: ``False``.

   **path** (str, required)
      The path to a CSV file in which subnet labels are specified. The CSV file must have three columns named ``label``, ``address``, and ``mask``, and be specified in that order. 

      For subnets that use a network address and mask: ``label`` is the descriptive name assigned to the subnet, ``address`` is the IP address for that subnet, and ``mask`` is the subnet mask assigned the subnet.

      For subnets that use `Classless Inter-Domain Routing (CIDR) notation <https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing>`__: ``label`` is the descriptive name assigned to the subnet, ``address`` is the CIDR notation for the IP address for that subnet, and ``mask`` is left empty.

      For example, a file located at ``/security/subnet_labels.csv``:

      .. code-block:: none

         label,address,mask
         Management,10.10.2.0,255.255.255.0
         DMZ Guest,10.10.10.0,255.255.255.0
         Services,10.10.24.0,255.255.255.0
         Storage,10.10.18.0,255.255.255.0
         Test,10.10.25.0,255.255.255.0
         Users,10.10.28.0,255.255.252.0
         HDFS Cluster 2A,172.18.71.0/26,
         HDFS Cluster 2B,172.18.71.64/26,
         HDFS Cluster 2C,172.18.71.128/26,
         Engineering VPC,172.18.70.0/24,
         Admin Subnet,172.18.70.0/26,

      where the first six rows are subnets that use a network address and mask and the final five rows are subnets that use CIDR notation.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will add columns that label source and destination IP address that use CIDR notation:

* A source IP address is identified in the ``src_ip_subnet_label`` column by hostname
* A destination IP address is identified in the ``dest_ip_subnet_label`` column by fully-qualified domain name (FQDN)

.. code-block:: yaml

   add_constant_column:
     class_name:
       symbol: 'AddSubnetLabelsBlock'
     subnet_labels:
       path: /path/to/subnet_labels.csv
       has_headers: True
     columns: ['label', 'address', 'mask']

A table with the following inputs:

.. code-block:: sql

   ----------- ----------------- ---------------
    label       address           mask          
   ----------- ----------------- ---------------
    Admin       10.10.2.0         255.255.255.0 
    Cluster A   172.18.71.0/26                  
    Cluster B   172.18.71.64/26                 
   ----------- ----------------- ---------------

will be updated to:

.. code-block:: sql

   ----------- ----------------- --------------- --------------------- ----------------------
    label       address           mask            src_ip_subnet_label   dest_ip_subnet_label 
   ----------- ----------------- --------------- --------------------- ----------------------
    Admin       10.10.2.0         255.255.255.0                                              
    Cluster A   172.18.71.0/26                    'hostname'            'FQDN'               
    Cluster B   172.18.71.64/26                   'hostname'            'FQDN'               
   ----------- ----------------- --------------- --------------------- ----------------------





.. _blocks-library-addurlfeatures:

AddURLFeaturesBlock
--------------------------------------------------
Use a URL features block to calculate the following attributes of URLs and hostnames:

* The length of a hostname
* The length of a subdomain
* The length of a URL
* The number of bytes received from the subdomain with the request
* The number of dots contained in the full URL
* The number of slashes contained in the full URL

This block adds the following columns to the table output:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Column
     - Description
   * - **bytes_subdomain** (int)
     - The number of bytes received from the subdomain with the request.
   * - **len_host** (int)
     - The length of the host name.
   * - **num_dots** (int)
     - The number of dots (``.``) contained in the full URL.
   * - **num_slashes** (int)
     - The number of slashes (``/``) contained in the full URL.
   * - **sub_domain** (str)
     - The subdomain associated with the full URL.
   * - **url_length** (int)
     - The length of the full URL.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'AddURLFeaturesBlock'
           hostname: 'url_host'
           url: 'url'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'AddURLFeaturesBlock'``.

**hostname** (required, optional, global, TYPE)
   The label assigned to the entity that is associated with a source IP address. This value is typically transformed from the source IP address that was parsed originally as part of the canonical form for a data source.

**url** (required, optional, global, TYPE)
   The full URL, including the scheme, top-level domain, subdomain, and query string.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will add columns for URL features:

.. code-block:: yaml

   url_features:
     class_name:
       symbol: 'AddURLFeaturesBlock'
     hostname: 'url_host'
     url: 'url'

A table with the following inputs:

.. code-block:: sql

   --------------------------
    url                      
   --------------------------
    docs.versive.com/index   
    www.versive.com/about_us 
   --------------------------

will be updated to:

.. code-block:: sql

   ----- ----------------- ---------- ---------- ------------- ------------ ------------
    url   bytes_subdomain   len_host   num_dots   num_slashes   sub_domain   url_length  
   ----- ----------------- ---------- ---------- ------------- ------------ ------------
    ...   26346             7          2          1             docs         27          
    ...   13246345          7          2          1             www          27          
   ----- ----------------- ---------- ---------- ------------- ------------ ------------




.. _blocks-library-aggregate:

AggregateBlock
--------------------------------------------------
Use an aggregate block to run one of the following aggregations against the specified columns:

* average
* count
* count distinct
* list
* max
* min
* standard deviation
* sum

An aggregate block expects the columns that will be aggregated to be listed as input to the block, and then with operations assigned to those columns.

An aggregate block produces outputs for each column specified, and then for each aggregation specified.

Aggregated columns may be used later on by the engine for more granular processing, such as performing multiday calculations and for modeling against specific behaviors and activities.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'AggregateBlock'
           groupby: ['group']
           input:
             id:
               - name: 'name'
             columns:
               - name: 'column_name'
                 type: data_type
               ...
           ops:
             - name: aggregation
               columns:
                 - 'column_name'
                 - 'column_name'
                 - ...
               limit: int
           source: 'str'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'AggregateBlock'``.

**groupby** (required, str)
   One (or more) column names from the list of ``input`` columns for which the listed aggregations are performed. For each unique column in the specified group-by list, a row is added to the output.

**input** (required, list of str)
   The list of columns to be aggregated.

   **id** (required, str)
      A name to associate with this input group of columns for this aggregate block.

   **columns** (required, str)
      A list of columns to be aggregated, along with the data type for that column.

      **name** (required, str)
         The name of the column.

      **type** (required, str)
         The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

**ops** (required, list of str)
   The list of operations to perform.

   **columns** (required, str)
      A list of columns to be aggregated, along with the data type for that column.

      **name** (required, str)
         The name of the column.

      **type** (required, str)
         The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

   **limit** (optional, int)
      The upper limit on list size for all input/operation pairs when aggregatng columns. Use this setting to help prevent "out of memory errors" in situations where there are a large number of columns to be aggregated.

   **name** (required, str)
      The aggregation operation to perform: ``avg``, ``count``, ``count_distinct``, ``list``, ``max``, ``min``, ``stddev``, or ``sum``.

      Use ``avg`` to aggregate by the average value for the column group.

      Use ``count`` to count the number of values.

      Use ``count_distinct`` to count the number of distinct values.

      Use ``list`` to aggregate by a list of all unique values, in alphanumeric order, for value in the column group.

      Use ``max`` to aggregate by the maximum value for the column group.

      Use ``min`` to aggregate by the minimum value for the column group.

      Use ``stddev`` to aggregate by the standard deviation with N - 1 degrees of freedom for the values for the column group.

      Use ``sum`` to aggregate by the sum of values for the column group.

**source** (required, str)
   The data source for which the aggregation is performed.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following examples show how to use the aggregate block.

Bytes In, Bytes Out
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configuration will input ``bytes_in`` and ``bytes_out`` data, grouped by ``hostname``, and then perform a ``sum`` aggregation:

.. code-block:: yaml

   blocks:
     sum_bytes_in_and_bytes_out:
       class_name:
         symbol: 'AggregateBlock'
       input:
         id:
           name: aggregate-input
         columns:
           - name: bytes_in
             type: int
           - name: bytes_out
             type: float
       groupby:
         - 'hostname'
       source: 'netflow'
       ops:
         - name: sum
           columns: 
             - bytes_in
             - bytes_out

A table with the following inputs:

.. code-block:: sql

   ---------- ---------- -----------
    hostname   bytes_in   bytes_out
   ---------- ---------- -----------
    str        int        int
    str        int        int
    str        int        int
   ---------- ---------- -----------

will be add the following columns:

.. code-block:: sql

   ---- -------------- ---------------
    ...  bytes_in_sum   bytes_out_sum
   ---- -------------- ---------------
    ...  int            int
    ...  int            int
    ...  int            int
   ---- -------------- ---------------


List DNS Records
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This block uses existing columns---``date``, ``hostname``, ``internal_A_named_domain``, ``internal_AAAA_named_domain``, ``internal_PTR_dest_ip``, and ``user``---along with a list operation to identify a list of distinct values for each of these columns, and then sort each list so that it contains up to 200 items.

Add a block named ``aggregate_dns`` to the block graph that is similar to:

.. code-block:: yaml

   aggregate_dns:
     class_name:
       symbol: 'AggregateBlock'
     groupby: ['date', 'hostname']
       input:
        id:
         name: 'dns_aggregation'
        columns:
         - name: date
           type: str
         - name: hostname
           type: str
         - name: user
           type: str
         - name: internal_A_named_domain
           type: str
         - name: internal_AAAA_named_domain
           type: str
         - name: internal_PTR_dest_ip
           type: str
       ops:
         - name: list
           columns: ['internal_A_named_domain',
                     'internal_AAAA_named_domain',
                     'internal_PTR_dest_ip',
                     'user']
           limit: 200
           source: 'dns'

This block adds columns using the pattern specified under ``ops``. This pattern concatenates the values specified by ``columns``, and then the value specified by ``name``:

* ``internal_A_named_domain_list``
* ``internal_AAAA_named_domain_list``
* ``internal_PTR_dest_ip_list``
* ``user_list``

The value that is assigned to these columns is a delimited list of values that are associated with both of the columns identified by ``groupby``. The ``limit`` value sets the total number of possible items in the list.

The following columns are added to the dataframe:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Column
     - Description
   * - **internal_A_named_domain_list** (str)
     - A delimited list of up to 200 distinct named domains that communicated internally via the DNS A request record.
   * - **internal_AAAA_named_domain_list** (str)
     - A delimited list of up of up to 200 distinct named domains that communicated internally via the DNS AAAA request record.
   * - **internal_PTR_dest_ip_list** (str)
     - A delimited list of up of up to 200 distinct distinct IP addresses that communicated internally via the DNS PTR request record.
   * - **user_list** (str)
     - A delimited list of up of up to 200 distinct users.



.. _blocks-library-backup:

BackupBlock
--------------------------------------------------
Use a backup block to make a copy of each specified column, and then add that column to the output using the specified prefix.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'BackupBlock'
           columns:
             - name: 'column_name'
               type: data_type
           prefix: 'raw_'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'BackupBlock'``.

**columns** (required, str)
   A list of columns to be categorized, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

**prefix** (optional, str)
   The column prefix to apply to all output columns. Default value: ``_raw``.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The column data for destination IP addresses, source IP addresses, and timestamps is still in a raw data format that will need to be cleaned up. The following configuration will backup three columns---``dest_ip``, ``source_ip``, and ``timestamp``---by adding three new columns, one for each existing column, but prefixed with ``raw_``:

.. code-block:: yaml

   backup_data:
     class_name:
       symbol: 'BackupBlock'
         columns:
           - name: dest_ip
             type: str
           - name: source_ip
             type: str
           - name: timestamp
             type: str
         prefix: 'raw_'

A table with the following inputs:

.. code-block:: sql

   --------------- ------------- ------------
    dest_ip         source_ip     timestamp  
   --------------- ------------- ------------
    192.168.1.1     192.168.1.1   2017-12-12 
    192.168.20.14   192.168.1.1   2017-12-13 
    192.168.1.1     192.168.1.1   2017-12-14 
   --------------- ------------- ------------

will be updated to:

.. code-block:: sql

   --------------- ------------- ------------ --------------- --------------- ---------------
    dest_ip         source_ip     timestamp    raw_dest_ip     raw_source_ip   raw_timestamp 
   --------------- ------------- ------------ --------------- --------------- ---------------
    192.168.1.1     192.168.1.1   2017-12-12   192.168.1.1     192.168.1.1     2017-12-12    
    192.168.20.14   192.168.1.1   2017-12-13   192.168.20.14   192.168.1.1     2017-12-13    
    192.168.1.1     192.168.1.1   2017-12-14   192.168.1.1     192.168.1.1     2017-12-14    
   --------------- ------------- ------------ --------------- --------------- ---------------




.. _blocks-library-buildurl:

BuildURLBlock
--------------------------------------------------
The build URL block builds a URL from a individual columns that contain parsed URL components. The output of this block is a single column that contains the rebuilt URL.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'BuildURLBlock'
           columns: [ cs_host,
                      cs_uri_path,
                      cs_uri_query,
                      cs_uri_scheme,
                      dest_port ]
           output_column_name: 'string'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'BuildURLBlock'``.

**columns** (required, list)
   The columns that are required to rebuild the URL are part of the canonical form for proxy data---``cs_host``, ``cs_uri_path``, ``cs_uri_query``, ``cs_uri_scheme``, and ``dest_port``---and must be present in the input table. These values area passed in from the input table.

**output_column_name** (optional, str)
   The name of the column to which the rebuilt URL is saved.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will build individual URL into a single column named ``rebuilt_url`` column:

.. code-block:: yaml

   is_arachnid_empty:
     class_name:
       symbol: 'BuildURLBlock'
     columns: [ cs_host,
                cs_uri_path,
                cs_uri_query,
                cs_uri_scheme,
                dest_port ]
     output_column_name: 'rebuilt_url'

A table with the following inputs:

.. code-block:: sql

   ----------------- -------------- -------------- --------------- -----------
    cs_host           cs_uri_path    cs_uri_query   cs_uri_scheme   dest_port 
   ----------------- -------------- -------------- --------------- -----------
    www.versive.com   /search.html   ?query=block   http            80        
    www.spider.com    /about                        https           443       
   ----------------- -------------- -------------- --------------- -----------

will be updated to:

.. code-block:: sql

   ----------------------------------------------------
    rebuilt_url                                        
   ----------------------------------------------------
    http://www.versive.com/search.html?query=block:80  
    https://www.spider.com/about:443                   
   ----------------------------------------------------







.. _blocks-library-calculatebytesratiosblock:

CalculateBytesRatiosBlock
--------------------------------------------------
Use a calculate bytes ratios block to calculate publication and consumption ratios:

* .. include:: ../../includes_terms/term_ratio_consumption.rst
* .. include:: ../../includes_terms/term_ratio_publication.rst

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: none

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'CalculateBytesRatiosBlock'
           dest: 'str'
           source: 'str'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'CalculateBytesRatiosBlock'``.

**dest** (required, str)
   The destination for network traffic.

**source** (required, str)
   The source of network traffic.

.. TODO: Intentionally left out the optional settings for publication_ratio, consumption_ratio, bytes_in, and bytes_out, all str.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
For example:

.. code-block:: yaml

   calculate_sharing_features:
     class_name:
       symbol: 'CalculateBytesRatiosBlock'
     source: hostname
     dest: url_regdomain

A table with the following inputs:

.. code-block:: sql

   ---------- ---------------
    hostname   url_regdomain
   ---------- ---------------
    string     string         
    string     string         
    string     string         
   ---------- ---------------

will be updated to:

.. code-block:: sql

   ---------- --------------- ------------------- ------------------- 
    hostname   url_regdomain   publication_ratio   consumption_ratio
   ---------- --------------- ------------------- ------------------- 
    value      value           float               float
    value      value           float               float       
    value      value           float               float       
   ---------- --------------- ------------------- ------------------- 




.. _blocks-library-calculatecolumnlength:

CalculateColumnLengthBlock
--------------------------------------------------
Use a calculate column length block to calculate the length of one (or more) named columns.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'CalculateColumnLengthBlock'
           columns:
             - name: 'column_name'
               type: data_type
               ...
           output_column_name: 'string'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'CalculateColumnLengthBlock'``.

**columns** (required, str)
   A list of columns to be aggregated, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

**output_column_name** (required, str)
   The name of the column in which the length of field is added.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will get the field length for the column ``arachnid`` for all rows in the table, add a column to the table named ``count``, and then add the number of characters in the ``arachnid`` column to the ``count`` column:

.. code-block:: yaml

   get_field_length:
     class_name:
       symbol: 'CalculateColumnLengthBlock'
     columns:
       - name: 'arachnid'
         type: str
     output_column_name: 'count'

A table with the following inputs:

.. code-block:: sql

   ----------------
    arachnid       
   ----------------
    scorpion       
    tarantula      
    jumping_spider 
   ----------------

will be updated to:

.. code-block:: sql

   ---------------- -------
    arachnid         count 
   ---------------- -------
    scorpion         8     
    tarantula        9     
    jumping_spider   14    
   ---------------- -------




.. _blocks-library-categorize:

CategorizeBlock
--------------------------------------------------
Use a categorize block to categorize columns according pattern matching. For each column that matches a pattern, a column is added to the table with a suffix appended to the original column name.

.. warning:: This block requires the :ref:`configure-category-patterns` section for details about which values are assigned to categories.

.. note:: To categorize port and protocol column data associated with flow data, see :ref:`blocks-library-categorizeport` and :ref:`blocks-library-categorizeprotocol`.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'CategorizeBlock'
           columns:
             - name: 'column_name'
               type: data_type
           category_type: 'type'
           suffix: '_category'
         ...
       ...


Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'CategorizeBlock'``.

**category_patterns**
   This block must be able to reference settings in the :ref:`configure-category-patterns` section of the configuration file.

**category_type** (required, str)
   The category to be applied when a value matches the pattern.

   For DNS data sources: ``A``, ``AAAA``, ``AXFR_IXFR_SOA``, ``PTR``, or ``TXT``. If a row does not match any of the listed patterns for the DNS request, the category is marked ``None``.

      Use ``A`` for an address mapping record (A).

      Use ``AAAA`` for an IPv6 or IPv4 address mapping record (AAAA).

      Use ``AXFR_IXFR_SOA`` for asynchronous transfer full range (AXFR), incremental zone transfer (IXFR), and start of zone authority (SOA) records.

      Use ``PTR`` for a pointer record.

      Use ``TXT`` for a text record.

   For hostname and/or user data sources: ``external`` or ``internal``. If a row does not match any of the listed patterns, the category is marked ``None``.

      Use ``external`` to categorize as internal.

      Use ``internal`` to categorize as external.

**columns** (required, str)
   A list of columns to be categorized, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

**suffix** (optional, str)
   For each column that is categorized, a new column is added to the output with the suffix appended to the original column name. Default value: ``_category``.


Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following sections show examples for using this block to categorize IP addresses and hostnames.

IP Addresses
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Both source and destination IP addresses need to be marked as internal or external, depending on the types of IP addresses discovered in the columns, as compared to a list of IP addresses that are provided by the categorization. Known IP addresses are marked as internal addresses, and then everything else is marked as external.

For example, the following types of IP addresses on a network are known:

* Loopback addresses ("127.0.0.0/8")
* Special purposes for protocol assignment addresses ("192.0.0.0/24")
* Private IPv4 addresss ("172.16.0.0/12", "192.168.0.0/16")
* Reserved IP addresses for documentation ("192.0.2.0/24", "198.51.100.0/24", and "203.0.113.0/24")
* Subnet addresses ("169.254.0.0/16")
* Martian packets---source and/or destination IP addresses that are reserved for special, known uses---such as multicast addresses ("224.0.0.0/4") or "reserved for future use" addresses ("240.0.0.0/4")

.. note:: The list of IP addresses that should be categorized may vary per organization; however for most organizations the starting list of addresses to categorize as internal is often the same.

Add a block named ``categorize_ip`` to the block graph that is similar to:

.. code-block:: yaml

   categorize_named_domain:
     class_name:
       symbol: 'CategorizeBlock'
     columns:
       - name: source
         type: str
     category_type: 'ip'

The parameters for this categorization are defined in the ``categpory_patterns`` section of the configuration file, similar to:

.. code-block:: yaml

   category_patterns:
     ip:
       - pattern: '10.'
         category: internal
       - pattern: '^172\\.(1[6789]|2\\d|30|31)\\.'
         jvm_pattern: '^172\\.(1[6789]|2\\d|30|31)\\.'
         pattern_type: regex
         category: internal
       - pattern: '192.0.0.'
         category: internal
       - pattern: '192.168.'
         category: internal
       - pattern: '127.'
         category: internal
       - pattern: '169.254.'
         category: internal
       - pattern: '192.0.2.'
         category: internal
       - pattern: '198.51.100.'
         category: internal
       - pattern: '203.0.113.'
         category: internal
       - pattern: '^2(2[456789]|3\\d)\\.'
         jvm_pattern: '^2(2[456789]|3\\d)\\.'
         pattern_type: regex
         category: internal
       - pattern: '^2(4\\d|5[012345])\\.'
         jvm_pattern: '^2(4\\d|5[012345])\\.'
         pattern_type: regex
         category: internal
       - pattern: ''
         category: external

A table with the following inputs:

.. code-block:: sql

   ---------------
    source_ip     
   ---------------
    10.9.8.7      
    8.8.8.8       
    127.0.0.4     
    216.34.181.45 
   ---------------

will be updated to:

.. code-block:: sql

   --------------- -----------------
    source_ip       source_category 
   --------------- -----------------
    10.9.8.7        internal        
    8.8.8.8         external        
    127.0.0.4       internal        
    216.34.181.45   external        
   --------------- -----------------



Hostnames
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Hostnames need to be marked as internal or external, as compared to a list of patterns that are provided by the categorization. Known patterns are marked as internal addresses, and then everything else is marked as external. The following example will mark hostnames with the word ``versive`` as internal, and then all other hostnames as external.

Add a block named ``categorize_named_domain`` to the block graph that is similar to:

.. code-block:: yaml

   categorize_named_domain:
     class_name:
       symbol: 'CategorizeBlock'
     columns:
       - name: host
         type: str
     category_type: 'hostname'

The parameters for this categorization are defined in the ``categpory_patterns`` section of the configuration file, similar to:

.. code-block:: yaml

   category_patterns:
     ip:
       ...
     hostname:
       - pattern: 'versive.com'
         pattern_type: endswith
         category: internal
       - pattern: ''
         category: external

which declares that all hostnames that end with ``versive.com`` will be classified as ``internal`` and that all others will be classified as ``external``.

A table with the following inputs:

.. code-block:: sql

   ------------------
    hostname         
   ------------------
    192.168.7.7      
    docs.versive.com 
    www.cnn.com      
    216.34.181.45    
   ------------------

will be updated to:

.. code-block:: sql

   ------------------ ---------------
    hostname           host_category 
   ------------------ ---------------
    192.168.7.7        external      
    docs.versive.com   internal      
    www.cnn.com        external      
    216.34.181.45      external      
   ------------------ ---------------



.. _blocks-library-categorizeport:

CategorizePortBlock
--------------------------------------------------
Use a categorize port block to identify which ports are TCP and UDP, and then identify the level of interest (high, medium, or low) for that port. This table inputs tabular data that was output by the categorize protocol block, and then looks for columns that are identified as having TCP and UDP ports. For each column with those ports, the output column named ``port_category`` rates it as :ref:`high interest, medium interest, or low interest <define-behaviors-unit-ports>`.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'CategorizePortBlock'
           output_column_name: 'port_category'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'CategorizePortBlock'``.
   
**output_column_name** (optional, str)
   The name of the column in which TCP and/or UDP ports are flagged has having high, medium, or low interest.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will categorize TCP and UDP ports as having high, medium, or low interest:

.. code-block:: yaml

   high_medium_and_low_interest_ports:
   class_name:
     symbol: 'CategorizePortBlock'
   output_column_name: 'port_category'

A table with the following inputs:

.. code-block:: sql

   --------------
    port_numbers 
   --------------
    80           
    135          
    181          
    443          
    1433         
    1754         
   --------------

will be updated to:

.. code-block:: sql

   -------------- ---------------
    port_numbers   port_category 
   -------------- ---------------
    80             high          
    135            high          
    181            medium        
    443            high          
    1433           high          
    1732           low           
   -------------- ---------------




.. _blocks-library-categorizeprotocol:

CategorizeProtocolBlock
--------------------------------------------------
Use a categorize protocol block to list protocols by the following categories:

* ``ICMP``
* ``SNMP`` (TCP protocol over ports 181 or 182)
* ``TCP_NOT_SNMP``
* ``UDP``
* ``other``

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'CategorizeProtocolBlock'
           output_column_name: 'protocol_category'
           port_column:
             - name: 'column_name'
               type: int
             ...
           protocol_column:
             - name: 'column_name'
               type: str
             ...
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'CategorizeProtocolBlock'``.

**port_column** (required, str)
   A list of columns to be categorized for port data, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column. Must be set to ``int``.

**protocol_column** (required, str)
   A list of columns to be categorized for protocol data, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column.  Must be set to ``str``.

**output_column_name** (optional, str)
   The name of the column to which categorized protocols are output. Possible column values: ``TCP``, ``SNMP``, ``TCP_NOT_SNMP``, ``UDP``, ``ICMP``, and ``other``. Default value: ``protocol_category``.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
None.






.. _blocks-library-categorizetcpnotsnmp:

CategorizeTCPNotSNMPBlock
--------------------------------------------------
Use a categorize TCP-not-SNMP block perform additional categorization for destination IP addresses that are associated with ``TCP_NOT_SNMP`` values in the ``protocol_category`` column output by :ref:`blocks-library-categorizeprotocol`.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: none

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'CategorizeTCPNotSNMPBlock'
           dest_ip:
             - name: 'column_name'
               type: int
             ...
           output_column_name: 'dest_ip_protocol_TCP_NOT_SNMP'
           protocol_category:
             - name: 'protocol_category'
               type: str
             ...
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'CategorizeTCPNotSNMPBlock'``.

**dest_ip** (required, str)
   The destination IP address.

**output_column_name** (optional, string)
   The name of the column to which categorization for destination addresses associated with the ``TCP_NOT_SNMP`` protocol category is output. Default value: ``dest_ip_protocol_TCP_NOT_SNMP``.

**protocol_category** (required, list of columns)
   The column that was created as the ``output_colum_name`` setting by the :ref:`blocks-library-categorizeprotocol`.


Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration perform additional categorizations for destination IP addresses that are associated with the ``TCP_NOT_SNMP`` protocol category:

.. code-block:: yaml

   categorize_TCP_NOT_SNMP:
     class_name:
       symbol: 'CategorizeTCPNotSNMPBlock'
     dest_ip:
       - name: 'dest_ip'
         type: str
     output_column_name: 'dest_ip_protocol_TCP_NOT_SNMP'
     protocol_category:
       - name: 'protocol_category'
         type: str

A table with the following inputs:

.. code-block:: sql

   -------------- -------------------
    dest_ip        protocol_category  
   -------------- -------------------
    10.10.10.10    TCP                  
    201.14.71.85   TCP_NOT_SNMP         
    201.14.71.85   SNMP                 
    221.14.70.86   TCP_NOT_SNMP         
   -------------- -------------------

will be updated to:

.. code-block:: sql

   -------------- ------------------- -------------------------------
    dest_ip        protocol_category   dest_ip_protocol_TCP_NOT_SNMP 
   -------------- ------------------- -------------------------------
    10.10.10.10    TCP                 None   
    201.14.71.85   TCP_NOT_SNMP        201.14.71.85   
    201.14.71.85   SNMP                None   
    221.14.70.86   TCP_NOT_SNMP        221.14.70.86   
   -------------- ------------------- -------------------------------





.. _blocks-library-cleanflowdata:

CleanFlowDataBlock
--------------------------------------------------
Use a clean flow data block to clean up tablular data for flow data sources.

* If the flow data input contains ``bytes_in``, then the output removes columns for ``user_real`` and ``raw_user``
* If the flow data input does not contain ``bytes_in``, then the output adds columns for ``bytes_in``, ``hostname``, and ``user``
* All other input columns are passed through to the output

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'CleanFlowDataBlock'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'CleanFlowDataBlock'``.


Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following examples show how to use this block.

bytes_in Present
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configuration will clean flow data when ``bytes_in`` is present in the table:

.. code-block:: yaml

   url_features:
     class_name:
       symbol: 'URLFeaturesBlock'
     hostname: 'url_host'
     url: 'url'

A table with the following inputs:

.. code-block:: sql

   ---------- ----------- ---------- 
    bytes_in   user_real   raw_user 
   ---------- ----------- ----------
    12345      string      string   
    12346      string      string   
   ---------- ----------- ----------

will be updated to:

.. code-block:: sql

   ----------
    bytes_in 
   ----------
    12345    
    12346    
   ----------


bytes_in Missing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configuration will clean flow data when ``bytes_in`` is not present in the table:

.. code-block:: yaml

   url_features:
     class_name:
       symbol: 'URLFeaturesBlock'
     hostname: 'url_host'
     url: 'url'

A table with the following inputs:

.. code-block:: sql

   ----------- ----------
    user_real   raw_user 
   ----------- ----------
    string      string   
    string      string   
   ----------- ----------

will be updated to:

.. code-block:: sql

   ----------- ---------- ---------- ---------- --------
    user_real   raw_user   bytes_in   hostname   user  
   ----------- ---------- ---------- ---------- --------
    string      string     12345      string     string
    string      string     12346      string     string
   ----------- ---------- ---------- ---------- --------






.. _blocks-library-collectflowstats:

CollectFlowStatsBlock
--------------------------------------------------
Use a collect flow statistics block to get descriptive statistics for blocks that define data processing for flow data sources.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'CollectFlowStatsBlock'
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'CollectFlowStatsBlock'``.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
None.


.. _blocks-library-concatenate:

ConcatenateBlock
--------------------------------------------------
Use a concatenate block to concatenate two (or more) columns into a single string column. Use a delimiter to separate the individual column names in the string.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'ConcatenateBlock'
           columns:
             - name: 'column_name'
               type: data_type
             - name: 'column_name'
               type: data_type
             ...
           delimiter: ''
           output_column_name: None # inputs separated by _
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'ConcatenateBlock'``.

**columns** (required, str)
   A list of at least two columns to be concatenated, along with the data type for each column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

**delimiter** (required, optional, global, TYPE)
   A string used to separate data fields. By default, there is no delimiter.

**output_column_name** (optional, str)
   The name of the column to which categorized protocols are output. Default value: the input names separated by an underscore (``_``).

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will concatenate the ``user`` and ``ip`` columns with a hyphen delimiter:

.. code-block:: yaml

   concatenate_user_and_ip:
     class_name:
       symbol: 'ConcatenateBlock'
     columns:
       - name: 'user'
         type: str
       - name: 'ip'
         type: int
     delimiter: ''
     output_column_name: concatenated_

A table with the following inputs:

.. code-block:: sql

   -------- ---------
    user     ip  
   -------- ---------
    string   address  
    string   address  
    string   address  
   -------- ---------

will be updated to:

.. code-block:: sql

   -------- --------- ----------------------
    user     ip        concatenated_user_ip 
   -------- --------- ----------------------
    string   address   string-address       
    string   address   string-address       
    string   address   string-address       
   -------- --------- ----------------------









.. _blocks-library-convertstringtodatetime:

ConvertStringToDatetimeBlock
--------------------------------------------------
Before the engine can perform group-by operations against transformed data, any date values that were defined using a string must first be converted to Python ``datetime`` or to ``risp.Timestamp`` formats. Use a string-to-datetime block to add a column to the data table that contains the converted datetime values.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'ConvertStringToDatetimeBlock'
           column:
             - name: 'column_name'
               type: 'str'             # this value must be str
           date_column_name: 'date'
           date_format: 'epoch'
           use_timestamp_type: False
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'ConvertStringToDatetimeBlock'``.

**column** (required, list of str)
   The column for which a ``str`` data type is converted to a ``datetime`` date time. The column ``name`` must be specfied. The type ``type`` must be set to ``str``. If a column is input that is not a string, an error is returned.

**date_column_name** (optional, str)
   The name of the column added to the output tabular data and into which the updated string date value is added. Default value: ``date``. The value of this column is the original string value from the input column, but converted to ``%Y-%m-%d`` format. This format may not be modified.

**date_format** (optional, str)
   The format used to represent datetime values in the named data source. Possible values are ``epoch`` or a timestamp in in Coordinated Universal Time (UTC) that is defined using :ref:`strptime syntax <python-strptime-syntax>`

**use_timestamp_type** (optional, bool)
   Specifies if the the output column will use ``risp.Timestamp`` instead of ``datetime`` for its data type. Default value: ``False`` (i.e. "use Python ``datetime``").

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will convert timestamp data that is stored as a string to a Python datetime format:

.. code-block:: yaml

   convert_timestamp_to_datetime_format:
     class_name:
       symbol: 'ConvertStringToDatetimeBlock'
     column:
       name: timestamp
       type: str
     date_format: '%m/%d/%Y %H:%M:%S'
     date_column_name: 'date'

A table with the following inputs:

.. code-block:: sql

   ----------------
    timestamp: str
   ----------------
    12-21-2017    
    12-21-2017    
   ----------------

will be updated to:

.. code-block:: sql

   ---------------- ---------------------
    timestamp: str   date: datetime
   ---------------- ---------------------
    12-21-2017       2017-12-21 00:00:00
    12-21-2017       2017-12-21 00:00:00
   ---------------- ---------------------






.. _blocks-library-copycolumn:

CopyColumnBlock
--------------------------------------------------
The copy column block copies the values of the named column, and then creates a new column with those same values.

.. 
.. uncomment and fix link to section when FillColumnBlock is in here
.. 
.. note:: Use the blocks-library-fillcolumnblock when the new column should be assigned a new, constant value that is applied to all rows in the table.
.. 

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'CopyColumnBlock'
           input_column: 'column_name'
           output_column: 'column_name'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'CopyColumnBlock'``.

**input_column** (optional, str)
   The name of the column to be copied.

**output_column** (optional, str)
   The name of the column to be created, and to which the values of ``input_column`` are copied.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will add a column named ``hostname`` that is initially assigned the same value as the input column:

.. code-block:: yaml

   add_hostname:
     class_name:
       symbol: 'CopyColumnBlock'
     input_column: 'source_ip'
     output_column: 'hostname'

A table with the following inputs:

.. code-block:: sql

   ---------------
    source_ip     
   ---------------
    192.168.1.1   
    192.168.20.14 
    192.168.1.1   
   ---------------

will be updated to:

.. code-block:: sql

   --------------- ---------------
    source_ip       hostname      
   --------------- ---------------
    192.168.1.1     192.168.1.1   
    192.168.20.14   192.168.20.14 
    192.168.1.1     192.168.1.1   
   --------------- ---------------


.. _blocks-library-deletecolumns:

DeleteColumnsBlock
--------------------------------------------------
Use a delete columns block to delete one (or more) columns.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'DeleteColumnsBlock'
           columns:
             'column_name'
             'column_name'
             ...
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'DeleteColumnsBlock'``.

**columns** (optional, list of str)
   A list of columns to be deleted from the table.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will delete the ``arachnid`` column from a table:

.. code-block:: yaml

   delete_columns:
     class_name:
       symbol: 'DeleteColumnsBlock'
     columns:
       'arachnid'

A table with the following inputs:

.. code-block:: sql

   -------------- -------------- ----------------
    source_ip      dest_ip        arachnid       
   -------------- -------------- ----------------
    192.168.1.1    10.10.10.10    scorpion       
    192.168.20.14  201.14.71.85   tarantula      
    192.168.1.1    201.14.71.85   jumping_spider 
   -------------- -------------- ----------------

will be updated to:

.. code-block:: sql

   -------------- --------------
    source_ip      dest_ip             
   -------------- --------------
    192.168.1.1    10.10.10.10         
    192.168.20.14  201.14.71.85       
    192.168.1.1    201.14.71.85  
   -------------- --------------


.. _blocks-library-fillcolumn:

FillColumnBlock
--------------------------------------------------
The fill column block adds a column to a dataframe, and then assigns that column the specified value for all rows in the dataframe.

.. note:: Use the :ref:`blocks-library-copycolumn` when the new column should have values that are identical to the values in the copied column for each row in the table. 

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'FillColumnBlock'
           column_name: 'column_name'
           column_value: ''
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'FillColumnBlock'``.

**column_name** (required, str)
   The name of the column to be added to the dataframe.

**column_value** (optional, str)
   The value that is assigned to ``column_name`` for all rows in the dataframe.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following examples show how to use this block:

Add url Column
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configuration will add a column named ``url`` that is initially populated with a constant, pre-defined string value:

.. code-block:: yaml

   add_url_col:
     class_name:
       symbol: 'FillColumnBlock'
     column_name: 'url'
     column_value: '-'

This updates the output for the added column with a hyphen (``-``) for all rows in the ``url`` column:

.. code-block:: sql

   ----- -----
    ...   url
   ----- -----
    ...   -   
    ...   -   
    ...   -  
   ----- -----


Add user Column
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configuration will add a column named ``user`` that is initially populated with a constant, pre-defined string value:

.. code-block:: yaml

   add_user:
     class_name:
       symbol: 'FillColumnBlock'
     column_name: 'user'
     column_value: ''

This updates the output for the added column with an empty string (``  ``) for all rows in the ``user`` column:

.. code-block:: sql

   ----- ------
    ...   user
   ----- ------
    ...        
    ...        
    ...        
   ----- ------




.. _blocks-library-filteremptyrows:

FilterEmptyRowsBlock
--------------------------------------------------
Columns that have a values of ``None``, ``''`` (empty), ``-``, or ``' '`` (a space character) contain no valid information. The filter empty rows block removes all rows that contain no valid information for the columns that are specified by this block.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'FilterEmptyRowsBlock'
           column_to_filter: 'column_name'
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'FilterEmptyRowsBlock'``.

**column_to_filter** (optional, str)
   The column for which the presence of ``None``, ``''`` (empty), ``-``, or ``' '`` (a space character) values will cause the row to be filtered out of the dataframe.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will filter all the rows that contain a value of ``None``, ``''``, and ``-`` for the ``arachnid`` column:

.. code-block:: yaml

   delete_columns:
     class_name:
       symbol: 'FilterEmptyRowsBlock'
     column_to_filter: 'arachnid'

A table with the following inputs:

.. code-block:: sql

   --------------- -------------- ----------------
    source_ip       dest_ip        arachnid       
   --------------- -------------- ----------------
    192.168.1.1     10.10.10.10    scorpion       
    192.168.20.14   201.14.71.85   tarantula      
    192.168.1.1     201.14.71.85   None           
    192.168.1.1     10.11.12.13    scor-pion      
    192.178.22.14   221.14.70.86   'tarantula'    
    192.188.1.2     231.14.71.87   jumping_spider 
   --------------- -------------- ----------------

will be updated to:

.. code-block:: sql

   --------------- -------------- ----------------
    source_ip       dest_ip        arachnid       
   --------------- -------------- ----------------
    192.168.1.1     10.10.10.10    scorpion       
    192.168.20.14   201.14.71.85   tarantula      
    192.188.1.2     231.14.71.87   jumping_spider 
   --------------- -------------- ----------------







.. _blocks-library-join:

JoinBlock
--------------------------------------------------
Use a join block to join two tables based on one (or more) key columns.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'JoinBlock'
           description: 'string'
           join_broadcast_rows_limit: 50000
           join_type: FULL_OUTER
           key_columns:
             - col1_left_name: col1_right_name
             - col2_left_name: col2_right_name
           left_table_frame_key:
             dataset: 'data_source'
             stage: 'stage_name'
           num_tablets: 5
           output_frame_key: joined_table
           partitions:
             per_worker_after_load_partitions: 2
             per_worker_before_save_partitions: 2
           tabletize: True
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'JoinBlock'``.

**description** (optional, str)
   A string to be added to log output.

**join_broadcast_rows_limit** (optional, int)
   The maximum number of rows below which a table is considered small.

**join_type** (optional, str)
   The type of join to perform. Possible values: ``CROSS``, ``FULL_OUTER``, ``INNER``, ``LEFT_OUTER``, or ``RIGHT_OUTER``. Default value: ``LEFT_OUTER``.

   ``CROSS``: The Cartesian product of rows in each of the two tables; there are no ``key_columns``.

   ``FULL_OUTER``: Items in either table for which there is no counterpart in the other table will have ``None``-filled values.

   ``INNER``: Items in either table for which there is no counterpart in the other table will not be included in the resulting table.

   ``LEFT_OUTER``: Items in the left table for which there is no counterpart in the right table will have ``None``-filled values.

   ``RIGHT_OUTER``: Items in the right table for which there is no counterpart in the left table will have ``None``-filled values.

**key_columns** (required, list of tuples of strings)
   The columns on which two tables are joined, specified as a list of tuples of strings.

**left_table_frame_key** (optional, str)
   The frame key for the left table in the join operation. For some join operations, this must be specified to ensure the correct order of input to the join operation. For example:

   .. code-block:: yaml

      left_table_frame_key:
        dataset: 'proxyweb'
        stage: 'parsed'

   May also be specified as ``{ 'name': 'table_name' }``:

   .. code-block:: yaml

      left_table_frame_key: { 'name': 'parsed_proxyweb' }

**num_tablets** (optional, int)
   The maximum number of optimizations when ``tabletize`` is ``True``. Default value: ``5``.

**output_frame_key** (required, str)
   The name of the output table. This name may be used to specify dependencies for blocks in a block graph that are configured to process after this output table is generated.

.. include:: ../../includes_config/partitions.rst

**tabletize** (optional, bool)
   If ``True``, enable optimizations for ``LEFT_OUTER`` and ``INNER`` joins. ``RIGHT_OUTER`` joins can be switched in some cases to effectively perform that join as if it is a ``LEFT_OUTER`` join. The secondary (right) table must be significantly smaller than the primary. Default value: ``False``.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following examples show how to join tables.

FULL_OUTER Joins
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configuration will join the left table into the right table:

.. code-block:: yaml

   full_outer_join:
     class_name:
       symbol: 'JoinBlock'
     join_type: FULL_OUTER
     key_columns:
       - 'A': 'A'
     output_frame_key: joined_table

The following tables will be joined. The input for the left table:

.. code-block:: sql

   --- ---
    A   B
   --- ---
    a   1 
    b   2 
   --- ---

and the input for the right table:

.. code-block:: sql

   --- ---
    A   C 
   --- ---
    c   x 
          
   --- ---

will be joined as:

.. code-block:: sql

   --- ------ ------
    A   B      C    
   --- ------ ------
    a   1      None 
    b   2      None 
    c   None   x    
   --- ------ ------

LEFT_OUTER Joins
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configuration will join the A table into the Q table:

.. code-block:: yaml

   left_outer_join:
     class_name:
       symbol: 'JoinBlock'
     join_type: LEFT_OUTER
     key_columns:
       - 'Q': 'A'
     left_table_frame_key: { 'name': 'Q_table' }
       dataset: 'data_source'
       stage: 'stage_name'
     output_frame_key: joined_table

The following tables will be joined. The input for the A table:

.. code-block:: sql

   --- ---
    A   B
   --- ---
    a   1 
    b   2 
   --- ---

and the input for the Q table:

.. code-block:: sql

   --- ---
    Q   C 
   --- ---
    c   x 
    b   g 
   --- ---

will be joined as:

.. code-block:: sql

   --- --- ------
    Q   C   B 
   --- --- ------
    b   g   2    
    c   x   None 
   --- --- ------





.. _blocks-library-lowercase:

LowercaseBlock
--------------------------------------------------
Use a lowercase block to convert the values in a column that has a ``str`` data type to be lowercase.

.. note:: If the data type for a column is not ``str``, that value of that column is passed through to the output.

.. warning:: The ``LowercaseBlock`` requires that both ``input`` and ``output`` be declared in the configuration. Use the ``extra_columns`` setting to define the policy for how columns not explicitly defined as ``input`` and/or ``output`` are to be handled.


Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'LowercaseBlock'
           columns:
             - name: 'column_name'
               type: data_type
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'LowercaseBlock'``.

**columns** (required, str)
   A list of columns for which values will be converted to lower-case, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will convert two columns to lowercase:

.. code-block:: yaml

   lowercase_columns:
     class_name:
       symbol: 'LowercaseBlock'
     input_column:
       - name: 'arachnid'
         type: str
       - name: 'fish'
         type: str

A table with the following inputs:

.. code-block:: sql

   ---------------- --------- --------
    arachnid         fish      mammal 
   ---------------- --------- --------
    SCorPion         saLMon    Lion   
    tarantULA        Trout     tigeR  
    JUMping_spider   HalibUt   bEAR   
   ---------------- --------- --------

will be updated to:

.. code-block:: sql

   ---------------- --------- --------
    arachnid         fish      mammal 
   ---------------- --------- --------
    scorpion         salmon    Lion   
    tarantula        trout     tigeR  
    jumping_spider   halibut   bEAR   
   ---------------- --------- --------



.. _blocks-library-mergecategories:

MergeCategoriesBlock
--------------------------------------------------
Use a merge categories block to replace the value for column A with the value for column B when the value in column A matches the specified category. One of column A or B is dropped from the table, depending on which value matches the category.

For example, the value of column A is ``vegetable``, the value of column B is ``mineral``, and the category is ``potato``. Column A matches the category (a potato is a vegetable); column B is dropped from the table. 

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'MergeCategoriesBlock'
           target_columm: 'column_name'
           override_column: 'column_name'
           override_category: 'category_name'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'MergeCategoriesBlock'``.

**override_category** (required, str)
   The name of the category that determines which of the override or target column values is used to merge the columns.

**override_column** (required, str)
   The name of the column to be merged of the target column doesn't match the specified category.

**target_column** (required, str)
   The name of the column to keep if the override column doesn't match the specified category.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will merge columns using a category named ``mineral``:

.. code-block:: yaml

   merge_minerals:
     class_name:
       symbol: 'MergeCategoriesBlock'
     target_columm: 'column_A'
     override_column: 'column_B'
     override_category: 'mineral'

A table with the following inputs:

.. code-block:: sql

   ----------- ----------- ---------
    column_A    column_B    mineral 
   ----------- ----------- ---------
    mineral     vegetable   bamboo  
    animal      vegetable   frog    
    vegetable   mineral     potato  
    mineral     None        quartz  
   ----------- ----------- ---------

will be updated to:

.. code-block:: sql

   ----------- ----------
    column_A    column_B 
   ----------- ----------
    vegetable   bamboo   
    animal      frog     
    vegetable   potato   
    mineral     quartz   
   ----------- ----------



.. _blocks-library-mergeentityscores:

MergeEntityScoresBlock
--------------------------------------------------
Use a merge entity scores block to get the stage scores for the |stage_recon|, |stage_collect|, and |stage_exfil| stages, and then output these scores to a single table. Each row in this table represents a single entity (host or user) with the total score, individual stage scores, and any associated individual surprise scores.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'MergeEntityScoresBlock'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'MergeEntityScoresBlock'``.

**delimiter** (str, optional)
   A string used to separate data fields. By default, there is no delimiter.

.. include:: ../../includes_config/partitions.rst

.. 
.. verify this note -- the start setting doesn't exist, but daterange_start/daterange_end do.
.. 
.. note:: This block also relies on the ``base_path``, ``date_path``, ``default_connection``, and ``start`` settings in the :doc:`configuration file <configure>`.
.. 

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration shows how entity scores for all three stages---|stage_recon|, |stage_collect|, and |stage_exfil| are merged:

.. code-block:: yaml

   merge_entity_scores:
     class_name:
       symbol: 'MergeEntityScoresBlock'

The following scores tables are input. The |stage_recon| stage is also referred to as "stage 3":

.. code-block:: sql

   ---------- ------------ --------------
    hostname   date         stage3_score 
   ---------- ------------ --------------
    host1      2017-12-12   .3           
    host2      2017-12-12   .4           
    host3      2017-12-12   .7           
   ---------- ------------ --------------

The |stage_collect| stage is also referred to as "stage 4":

.. code-block:: sql

   ---------- ------------ --------------
    hostname   date         stage4_score 
   ---------- ------------ --------------
    host1      2017-12-12   .09          
    host2      2017-12-12   .2           
    host3      2017-12-12   .43          
   ---------- ------------ --------------

The |stage_exfil| stage is also referred to as "stage 5":

.. code-block:: sql

   ---------- ------------ --------------
    hostname   date         stage5_score 
   ---------- ------------ --------------
    host1      2017-12-12   .19          
    host2      2017-12-12   .3           
    host3      2017-12-12   .26          
   ---------- ------------ --------------

will be merged into the following:

.. code-block:: sql

   ---------- ------------ ------------ --------------- --------------- ---------------
    hostname   date         host_score   scored_stage3   scored_stage4   scored_stage5 
   ---------- ------------ ------------ --------------- --------------- ---------------
    host1      2017-12-12   .53          .3              .03              .2           
    host2      2017-12-12   1.14         .05             .05              .64          
    host3      2017-12-12   .47          .2              .27              None         
   ---------- ------------ ------------ --------------- ---------------- --------------





.. _blocks-library-mergetables:

MergeTablesBlock
--------------------------------------------------
Use a merge tables block to merge two (or more) tables into a single table (appended vertically), which may then be used as a dependency for other blocks in a block graph.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'MergeTablesBlock'
           output_frame_key: 'key_name'
           use_common_subset: True
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'MergeTablesBlock'``.

**output_frame_key** (required, str)
   The name of the output table. This name may be used to specify dependencies for blocks in a block graph that are configured to process after this output table is generated.

**use_common_subset** (optional, str)
   Specifies the columns for the output table based on the columns in the input tables. Default value: ``True``.

   Set to ``True`` to output only the columns that were present in each of the input tables. There must be at least one column that is common to all input tables.

   Set to ``False`` to output all of the columns that were present in any of the input tables. Output columns that were missing from the input are marked as ``None``.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following example shows how to use this block:

Output: Input Columns
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configuration will merge the common subset of three tables together, along with adding ``None`` values to empty columns:

.. code-block:: yaml

   merge_to_common_table:
     class_name:
       symbol: 'MergeTablesBlock'
     output_frame_key: 'output_only_common_columns'
     use_common_subset: True

These tables are defined in the ``depends`` section of the block graph:

.. code-block:: yaml

   ...
     depends:
       table_one:
         ...
       table_two:
         ...
       table_three:
         ...

Three tables are input. First, ``table_one``:

.. code-block:: sql

   --------------- ----------------
    source_ip       arachnid       
   --------------- ----------------
    192.168.1.1     scorpion       
    192.168.20.14   tarantula      
    192.168.1.3     jumping_spider 
   --------------- ----------------

then ``table_two``:

.. code-block:: sql

   -------------- ----------------
    dest_ip        arachnid       
   -------------- ----------------
    10.10.10.10    scorpion       
    201.14.71.85   tarantula      
    201.14.71.86   jumping_spider 
   -------------- ----------------

and finally ``table_three``:

.. code-block:: sql

   ------------- ----------------
    some_column   arachnid       
   ------------- ----------------
    'value1'      scorpion       
    'value2'      tarantula      
    'value3'      jumping_spider 
   ------------- ----------------

will be updated to:

.. code-block:: sql

   ----------------
    arachnid       
   ----------------
    scorpion       
    tarantula      
    jumping_spider 
    scorpion       
    tarantula      
    jumping_spider 
    scorpion       
    tarantula      
    jumping_spider 
   ----------------

Output: All Columns
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configuration will merge the common subset of three tables together:

.. code-block:: yaml

   merge_to_common_table:
     class_name:
       symbol: 'MergeTablesBlock'
     output_frame_key: 'output_everything'
     use_common_subset: False

These tables are defined in the ``depends`` section of the block graph:

.. code-block:: yaml

   ...
     depends:
       table_one:
         ...
       table_two:
         ...
       table_three:
         ...

Three tables are input, as defined in the ``depends`` section for the block graph in which the ``merge_to_common_table`` merge block is defined. First, ``table_one``:

.. code-block:: sql

   ---------------
    source_ip     
   ---------------
    192.168.1.1   
    192.168.20.14 
    192.168.1.3    
   ---------------

then ``table_two``:

.. code-block:: sql

   --------------
    dest_ip      
   --------------
    10.10.10.10  
    201.14.71.85 
    201.14.71.86 
   --------------

and finally ``table_three``:

.. code-block:: sql

   ----------------
    arachnid       
   ----------------
    scorpion       
    tarantula      
    jumping_spider 
   ----------------

will be updated to:

.. code-block:: sql

   --------------- -------------- ----------------
    source_ip       dest_ip        arachnid       
   --------------- -------------- ----------------
    192.168.1.1     None           None           
    192.168.20.14   None           None           
    192.168.1.3     None           None           
    None            10.10.10.10    None           
    None            201.14.71.85   None           
    None            201.14.71.86   None           
    None            None           scorpion       
    None            None           tarantula      
    None            None           jumping_spider 
   --------------- -------------- ----------------

Drop Columns
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configuration will merge three tables together, dropping any columns that are not part of the entire input set:

.. code-block:: yaml

   merge_to_common_table:
     class_name:
       symbol: 'MergeTablesBlock'
     output_frame_key: 'source_ip'
     use_common_subset: False

These tables are defined in th e

.. code-block:: yaml

   ...
     depends:
       table_one:
         ...
       table_two:
         ...
       table_three:
         ...

Three tables are input, as defined in the ``depends`` section for the block graph in which the ``merge_to_common_table`` merge block is defined. First, ``table_one``:

.. code-block:: sql

   ---------------
    source_ip     
   ---------------
    192.168.1.1   
    192.168.20.14 
    192.168.1.3    
   ---------------

then ``table_two``:

.. code-block:: sql

   --------------
    dest_ip      
   --------------
    10.10.10.10  
    201.14.71.85 
    201.14.71.86 
   --------------

and finally ``table_three``:

.. code-block:: sql

   --------------- -------------- ----------------
    source_ip       dest_ip        arachnid       
   --------------- -------------- ----------------
    192.168.1.1     10.10.10.10    scorpion       
    192.168.20.14   201.14.71.85   tarantula      
    192.188.1.2     231.14.71.87   jumping_spider 
   --------------- -------------- ----------------

will be updated to:

.. code-block:: sql

   --------------- --------------
    source_ip       dest_ip      
   --------------- --------------
    192.168.1.1     None         
    192.168.20.14   None         
    192.168.1.3     None         
    None            10.10.10.10  
    None            201.14.71.85 
    None            201.14.71.86 
    192.168.1.1     10.10.10.10    
    192.168.20.14   201.14.71.85 
    192.188.1.2     231.14.71.87 
   --------------- --------------


.. _blocks-library-parse:

ParseBlock
--------------------------------------------------
Use a parse block to read raw data from a log file, and then produce it as tabular data. A parse block ingests data from a data source and is typically the first block in a block graph that processes data prior to modeling.

.. warning:: A ``ParseBlock`` is assumed to be reading raw data from a log file as input. Unlike any other block that gets its input from a block or data source that is not defined within the block graph, it does not have to declare a dependency outside the block graph via ``depends: { block_name: 'outside_block_graph' }``.

A parse block expects a log file as input, with one log per line. Use a :ref:`YAML patterns file <process-data-yaml-patterns>` to break each line in the log file into columns.

A parse block produces two outputs:

#. A parsed set of tabular data, as specified by ``frame_key: {'stage': 'parsed'}`` and an associated block
#. A list of lines in the log file that failed to parse, as specified by ``frame_key: {'stage': 'parsed_error'}`` and an associated block

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++
A parse block is defined in the CONFIGURATION_FILE for a BLOCK_GRAPH by specifying ``ParseBlock`` as the symbol for block:

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'ParseBlock'
           date_format: epoch
           path: '/path/to/data/source'
           schema: '/path/to/yaml_patterns_file.yaml'
           source: 'source_name'
         <block_name>
         ...
       outputs:
         - block_name: <block_name>
           frame_key: {'stage': 'parsed'}
         - block_name: <block_name>
           frame_key: {'stage': 'parsed_error'}
       ...

.. TODO: This warning below is kinda lame, but it gets the point across. You gotta spit this out, because good parsed data gets more processing. Bad parsed data does not. Depends on the graph!

.. warning:: Successfully parsed data requires additional processing by subsequent blocks in the block graph. Unsuccessfully parsed data should not receive additional processing. Split this data by specifying different block names in the ``outputs`` block. The correct block names are determined by the actual graph.


Configuration Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'ParseBlock'``.

**date_format** (required, str)
   The format to use for timestamps in the parsed output.

**delimiter** (optional, str)
   The delimiter used to separate data files in the parsed set of tabular data. Default value: no delimiter.

**outputs** (required)
   The outputs produced for parsed data. This is defined outside of the parser block configuration in the ``outputs`` section as part of the same block graph.

   **block_name** (required, str)
      The name of the block for which the ``frame_key`` is associated for successful and unsuccessful parsing of block data.

      .. note:: Successfully parsed data is typically run through multiple blocks in a sequence for additional data processing, whereas unsuccessfully parsed data only needs to be output as a list. As such the ``frame_key`` settings for ``parsed`` and ``parsed_error`` data are typically associated with different blocks in the configuration file.

   **frame_key** (required, key pair)
      The pattern under which successfully and unsuccessfully parsed log data is grouped in the output.

      Use ``frame_key: {'stage': 'parsed'}`` for successfully parsed output.

      Use ``frame_key: {'stage': 'parsed_error'}`` for unsuccessfully parsed output.

**path** (required, str)
   The path to the location on the storage cluster from which a log file is ingested on a daily basis.

**schema** (required, str)
   The path to the location of the :ref:`YAML patterns file <process-data-yaml-patterns>` used to parse ingested log file data.

**source** (required, str)
   The name of a data source.

.. warning:: The ``base_path``, ``date_path``, and ``default_connection`` settings are global settings and are required by the parse block for accessing the log file data, accessing the cluster, and determining the location to which parsed data is output.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following example shows how to use this block:

Parse DNS Data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configuration will read raw data from the DNS log files, and then produce it as tabular data in the canonical form:

.. code-block:: yaml

   parse_dns:
     class_name:
       symbol: 'ParseBlock'
     schema: '/path/to/schema/dns.yml'
     date_format: '%m/%d/%Y %H:%M:%S'
     path: '/path/to/dns/'
     source: 'dns'

The schema that is used for regular expression parsing of raw flow data is defined in a :ref:`YAML patterns file <process-data-yaml-patterns>`. Every data source and the environment in which it operates is unique; as such, extending the YAML patterns file may be necessary to ensure that all of the columns that are necessary for processing flow data within that environment are present in the parsed table.

The parse YAML file for parsing raw flow data into the canonical form is similar to:

.. code-block:: yaml

   file:
     pattern: >-
       (?P<timestamp>[^,]*),
       (?P<duration>[^,]*),
       (?P<protocol>[^,]*),
       (?P<source_ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}),
       (?P<source_port>[^,]*),
       (?P<dest_ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}),
       (?P<dest_port>[^,]*),
       (?P<pks_out>[^,]*),
       (?P<bytes_out>[^,]*),
       (?P<flows>[^,]*)$

   fields:
     - name: timestamp
       type: float
     - name: duration
       type: float
     - name: protocol
       type: str
     - name: source_ip
       type: str
     - name: source_port
       type: int
     - name: dest_ip
       type: str
     - name: dest_port
       type: int
     - name: pks_out
       type: float
     - name: bytes_out
       type: int
     - name: flows
       type: float

This creates the parsed table for flow data, which outputs as a table with the following columns:

.. code-block:: sql

   ----------- ---------- ---------- ----------- ------------- --------- ----------- --------- ----------- -------
    timestamp   duration   protocol   source_ip   source_port   dest_ip   dest_port   pks_out   bytes_out   flows
   ----------- ---------- ---------- ----------- ------------- --------- ----------- --------- ----------- -------
    float       float      str        str         int           str       int         float     int         float
    float       float      str        str         int           str       int         float     int         float
    float       float      str        str         int           str       int         float     int         float
    ...         ...        ...        ...         ...           ...       ...         ...       ...         ...
   ----------- ---------- ---------- ----------- ------------- --------- ----------- --------- ----------- -------

.. 
.. Not sure if this happens with blocks
.. 
.. **Default Validations**
.. 
.. Validations ensure that the raw DNS data was parsed correctly. When ``default_validation`` is set to ``True`` in the configuration file, errors are raised if any of the the columns in the canonical form are missing, or if the rates of missing values are too high for the following columns:
.. 
.. * The ``dest_ip`` column may have no more than 10% of the columns with missing data
.. * The ``source_ip`` column may have a maximum 10% of the columns with missing data
.. * The ``timestamp`` column may have a maximum 1% of the columns with missing data
.. 





.. _blocks-library-parseurl:

ParseURLBlock
--------------------------------------------------
Use a parse URL block to parse the ``url`` column, extract all of the subfields, and then add them into the following columns:

* ``subdomain``
* ``url_depth``
* ``url_domain``
* ``url_host``
* ``url_mime_enc``
* ``url_mime_type``
* ``url_page_url``
* ``url_port``
* ``url_protocol``
* ``url_query``
* ``url_regdomain``
* ``url_tld``

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'ParseURLBlock'
           url: 'url'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'ParseURLBlock'``.

**url** (optional, str)
   The column to be parsed into individual components.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will extract subfields for URLs into specific columns from the ``url`` column:

.. code-block:: yaml

   parse_urls:
     class_name:
       symbol: 'ParseURLBlock'
     url: 'url'

A table with the following inputs:

.. code-block:: sql

   ------------------------------
    url                          
   ------------------------------
    http://www.versive.com:80    
    https://docs.versive.com     
    http://www.versive.com/about 
   ------------------------------

will be updated to:

.. code-block:: sql

   ----- -------------- ----------- ------------ --------- ----------
    ...   url_protocol   subdomain   url_domain   url_port   url_...
   ----- -------------- ----------- ------------ --------- ----------
    ...   http           www         versive      80        ...
    ...   https          docs        versive                ...
    ...   http           www         versive                ...
   ----- -------------- ----------- ------------ --------- ----------

along with columns for ``url_depth``, ``url_domain``, ``url_mime_enc``, ``url_mime_type``, ``url_page_url``, ``url_port``, ``url_query``, ``url_regdomain``, and ``url_tld``.



.. _blocks-library-partition:

PartitionBlock
--------------------------------------------------
Use a partition block to set the number of partitions to use when running the engine with Apache Spark. Data is re-partitioned to the number of workers prior to a save operation, and then to a total number of workers after a load operation.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'PartitionBlock'
           per_worker: 0
           total: 0
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'PartitionBlock'``.

**per_worker** (optional, int)
   The number of partitions per worker. Default value: ``0``.

**total** (optional, int)
   The total number of partitions. Default value: ``0``.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
None.




.. _blocks-library-preaggregateflow:

PreaggregateFlowBlock
--------------------------------------------------
Use a pre-aggregate flow block to reduce the amount of input to subsequent blocks in a block graph that are processing flow data. This block will aggregate ``pks_out``, ``bytes_out``, and ``timestamp`` column data for a single day, grouped by ``dest_ip``, ``dest_port``, ``protocol``, ``source_ip``, and ``source_port`` columns.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'PreaggregateFlowBlock'
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'PreaggregateFlowBlock'``.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   preaggreate_flow_data:
     class_name:
       symbol: 'PreaggregateFlowBlock'



.. _blocks-library-replacenonedates:

ReplaceNoneDatesBlock
--------------------------------------------------
Use a replace none dates block to replace columns that have ``None`` date values with the date from which data processing starts.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'BlockName'
           date_col: 'date'
           date_format: '%Y%m%d'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'BlockName'``.

**date_col** (optional, str)
   The name of the column for which ``None`` values will be replaced with the date on which the processing started.

**date_format** (optional, str)
   A string that defines the format for the date. Default value: ``%Y%m%d``.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will replace all rows in which ``date_column`` has a ``None`` value with ``2017-12-21`` (the date from which data processing starts):

.. code-block:: yaml

   replace_none_dates_with_start_date:
     class_name:
       symbol: 'ReplaceNoneDatesBlock'
     date_col: 'date_column'

A table with the following inputs:

.. code-block:: sql

   --------- -------------
    column1   date_column 
   --------- -------------
    value     2017-12-21  
    value     None        
    value     None        
    value     2017-12-21  
   --------- -------------

will be updated to:

.. code-block:: sql

   --------- -------------
    column1   date_column 
   --------- -------------
    value     2017-12-21  
    value     2017-12-21  
    value     2017-12-21  
    value     2017-12-21  
   --------- -------------






.. _blocks-library-sample:

SampleBlock
--------------------------------------------------
Running the engine against a full data set can be time-consuming. Use a sample block to reduce the size of a successfully parsed data set into a percentage of its original size. Running the engine against a sampled data set increases data processing time and speed of model development. Sample sizes can be increased as the model quality improves. Data is sampled randomly via hashing to identify the percent to select for the sample.

.. warning:: The ``SampleBlock`` requires that both ``input`` and ``output`` be declared in the configuration. Use the ``extra_columns`` setting to define the policy for how columns not explicitly defined as ``input`` and/or ``output`` are to be handled.

.. warning:: A sample block may be configured once per block graph.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'SampleBlock'
           column:
             - name: column_name
               type: data_type
           percent: int
           input:
             id:
               name: input-name
             columns:
               - name: column_name
                 type: data_type
           output:
             id:
               name: output-name
             columns:
               - name: column_name
                 type: data_type
           skip_values:
             - '-'
             - ':'
             - ...
           sampling_udf: 'function_name'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'SampleBlock'``.

**column** (required, str)
   A list of columns to be sampled, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

**input** (required, list of str)
   The list of columns to be input for sampling.

   **id** (required, str)
      A name to associate with this input group of columns for this sample block.

   **columns** (required, str)
      A list of columns to be sampled, along with the data type for that column.

      **name** (required, str)
         The name of the column.

      **type** (required, str)
         The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

**output** (required, list of str)
   The list of columns to be output after sampling.

   **id** (required, str)
      A name to associate with this output group of columns for this sample block.

   **columns** (required, str)
      A list of columns to be output after sampling, along with the data type for that column.

      **name** (required, str)
         The name of the column.

      **type** (required, str)
         The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

**percent** (required, integer)
   An integer between ``1`` and ``100`` that indicates the amount of data to retain. Default value: ``100``. For example, if set to ``5``, 5% of the data will be retained and 95% of the data will be discarded.

**skip_values** (optional, list)
   A list of values that, if present, indicate a row should not be included in the sampled data. Include quotation marks if the value is a YAML indicator character. For example: ``"-"``, ``":"``, ``"["``, or ``"]"``. ``None`` values are always skipped. Default value: ``None``.

**sampling_udf** (optional, str)
   The name of a user-defined function that is located in the :doc:`extension package </extensions>` for use with sampling. This function will override the sample block. It must be a callable class that takes the sampling percent as an argument at initialization, and when called, it takes the value of the sampling column as argument.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following example shows sampling of successfully parsed data.

* The ``parse_netflow`` block parses log files located at ``/path/to/data/source``
* All successfully parsed data is handed off to the sample block
* The sample block is configured to sample source IP address data at 5%
* Generates output for the parsed data (at the sampled percentage) as tabular data

.. code-block:: yaml

   block_graphs:
     prs_graph:
       depends:
         sample:
           - block_name: parse_netflow
             frame_key: {'stage': 'parsed'}
       blocks:
         parse_netflow:
           class_name: 'ParseBlock'
           path: '/path/to/data/source/'
           ...
         sample:
           class_name: 'SampleBlock'
           column:
             name: source_ip
             type: str
           percent: 5
           input:
             id:
               name: sourceip-input
             columns:
               - name: source_ip
                 type: str
           output:
             id:
               name: sourceip-output
             columns:
               - name: source_ip
                 type: str 
       outputs:
         ...
         - block_name: sample
           frame_key: {'stage': 'parsed'}
     ...








.. _blocks-library-standardizeblock:

StandardizeBlock
--------------------------------------------------
Use a standardize block to ensure that columns exist for users and source IP addresses. This block will verify the input columns and if columns do not already exist, will add columns for ``user_real`` and ``hostname``.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'StandardizeBlock'
         ...
       ...


Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
This block has no block-specific settings.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will ensure that columns exist for users and source IP addresses:

.. code-block:: yaml

   standardize_block:
     class_name:
       symbol: 'StandardizeBlock'

A table with the following inputs:

.. code-block:: sql

   --------- ---------
    column1   column2 
   --------- ---------
    value     value   
    value     value   
    value     value   
   --------- ---------

will be updated to:

.. code-block:: sql

   --------- --------- ----------- ------------
    column1   column2   user_real   hostname  
   --------- --------- ----------- ------------
    value     value     user        IP address 
    value     value     user        IP address 
    value     value     user        IP address 
   --------- --------- ----------- ------------




.. _blocks-library-standardizedates:

StandardizeDatesBlock
--------------------------------------------------
Date is processed on a daily basis. Column values with dates should be standardized so that all columns in the dataframe have the same date value. The standardize dates block examines all the values in the input column, and then standardizes those values to be one of the following:

* A value that is passed from the configuration file
* The most frequent value identified, after examining all the date values in the input column

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'StandardizeDatesBlock'
           input_column: 'column_name'
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'StandardizeDatesBlock'``.

**input_column** (required, str)
   The name of the column to be filtered to a single date value.

**start** (referenced from configuration)
   The start date is referenced from the configuration file; if the start date cannot be referenced from the configuration file, the block will assign most frequently occuring date as this value.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
Every day, DNS log files are ingested from the data lake and are then parsed into the canonical form and saved to a data table, with some additional processing. because DNS data is processed daily, all of the values in the ``date`` column must be identical.

The following configuration will standardize values in the ``date`` column:

.. code-block:: yaml

   clean_cross_day_boundry:
     class_name:
       symbol: 'StandardizeDatesBlock'
     input_column: 'date'

A table with the following inputs:

.. code-block:: sql

   ---------- ------------
    hostname   date       
   ---------- ------------
    host1      2017-12-11 
    host2      12-12-2017 
    host3      2017-12-12 
   ---------- ------------

will be updated to:

.. code-block:: sql

   ---------- ------------
    hostname   date       
   ---------- ------------
    host1      2017-12-11 
    host2      2017-12-11 
    host3      2017-12-11 
   ---------- ------------





.. _blocks-library-standardizedomain:

StandardizeDomainBlock
--------------------------------------------------
Many domain names require some cleanup after they have been parsed into the canonical form. For example:

* Numbers (between parentheses) may have been added in locations where periods should have been
* Leading or trailing periods may be present

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'StandardizeDomainBlock'
           named_domain: 'named_domain'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'StandardizeDomainBlock'``.

**named_domain** (optional, str)
   The column in which domain names are present that require standardization.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will clean up the domain names contained in the ``named_domains`` column:

.. code-block:: yaml

   clean_named_domain:
     class_name:
       symbol: 'StandardizeDomainBlock'
     named_domain: 'named_domain'

A table with the following inputs:

.. code-block:: sql

   -------------------------
    named_domain            
   -------------------------
    (12)versive(11)com      
    versive.com             
    versive.com(1)          
    (12)versive(11)com(11). 
   -------------------------

will be updated to:

.. code-block:: sql

   --------------
    named_domain  
   --------------
    versive.com  
    versive.com  
    versive.com  
    versive.com  
   --------------


.. _blocks-library-standardizehostname:

StandardizeHostnameBlock
--------------------------------------------------
Use a standardize hostname block to convert any hyphens (``-``) to ``None`` values, and then also keep only the segment before the first period (``.``) in any hostname that is split by periods.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'StandardizeHostnameBlock'
           column:
             - name: 'column_name'
               type: data_type
           column:
             ...
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'StandardizeHostnameBlock'``.

**columns** (required, str)
   A list of columns to be standardized, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.


Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will standardize hostnames:

.. code-block:: yaml

   standardize_hostnames:
     class_name:
       symbol: 'StandardizeHostnameBlock'
     column:
       - name: 'hostname'
         type: str

A table with the following inputs:

.. code-block:: sql

   ----------
    hostname 
   ----------
    versive  
    versive  
    ve-rsive 
    ve.rsive 
   ----------

will be updated to:

.. code-block:: sql

   ----------
    hostname 
   ----------
    versive  
    versive  
    None    
    ve       
   ----------






.. _blocks-library-standardizeipaddress:

StandardizeIPAddressBlock
--------------------------------------------------
Use a standardize IP address block to standardize the octets in an IP address. In real data, octets often have leading zeroes. The engine uses IP addresses to correlate data and requires that all IP addresses have the same format. For example:

* 192.168.0.1
* 192.168.000.001

are identical IP addresses, yet the engine cannot correlate them due to the zero-padding differences in the formatting. Outside of standardizing the octets in an IP address for the specified columns, this block passes all input columns to output without modification.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'StandardizeIPAddressBlock'
           column:
             - name: 'column_name'
               type: data_type
           column:
             ...
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'StandardizeIPAddressBlock'``.

**columns** (required, str)
   A list of columns to be standardized, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will standardize IP addresses for the ``source_ip`` and ``dest_ip`` columns:

.. code-block:: yaml

   standardize_IP_addresses:
     class_name:
       symbol: 'StandardizeIPAddressBlock'
     column:
       - name: 'source_ip'
         type: str
       - name: 'dest_ip'
         type: str

A table with the following inputs:

.. code-block:: sql

   --------------- ---------------
    source_ip       dest_ip       
   --------------- ---------------
    192.168.1.100   10.10.10.10   
    192.168.100.1   10.10.10.10   
    192.168.1.1     10.10.100.100 
   --------------- ---------------

will be updated to:

.. code-block:: sql

   ------------- -------------
    source_ip     dest_ip     
   ------------- -------------
    192.168.1.1   10.10.10.10 
    192.168.1.1   10.10.10.10 
    192.168.1.1   10.10.10.10 
   ------------- -------------




.. _blocks-library-standardizereverseip:

StandardizeReverseIPBlock
--------------------------------------------------
Use a standardize reverse IP address block to format all IPv4 and IPv6 addresses using a regular expression, and reverse them (when necessary). For example:

* An IPv4 address of ``12.12.34.172`` is reversed to ``172.34.12.12``
* An IPv6 address of ``4347:0640:f7a8:0000:0000:35b8:6bg0:1122.ip6.arpa`` drops the ``ip6.arpa`` suffix, and then is reversed to ``2211:0gb6:8b53:0000:0000:8a7f:0460:7434``
	
Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'StandardizeReverseIPBlock'
           request_type: 'request_type'
           named_domain: 'named_domain'
           dest_ip: 'dest_ip'
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'StandardizeReverseIPBlock'``.

**dest_ip** (optional, str)
   The destination IP address.

**named_domain** (optional, str)
   The text between "http://" and the next "/" in a URL, such as ``'versive.com'``.

**request_type** (optional, str)
   The type of DNS request: ``A``, ``AAAA``, ``AXFR``.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
IP addresses that are parsed by the engine from raw data are not always in a standardized format:

* IPv6 addresses often have extra characters at the end of the address: ``.ip6.arpa``
* IP addresses may be reversed: ``12.12.34.172`` vs. ``172.34.12.12``

The following configuration will:

#. Update the values in the ``dest_ip`` column when the value of the ``named_domain`` column is an IPv4 or IPv6 address
#. Use a regular expression to identify column values that are IP addresses, and then reverse them

.. code-block:: yaml

   reverse_destination_ip_address:
     class_name:
       symbol: 'StandardizeReverseIPBlock'
     request_type: 'request_type'
     named_domain: 'named_domain'
     dest_ip: 'dest_ip'

A table with the following inputs:

.. code-block:: sql

   ----------------- -------------- --------------
    named_domain      request_type   dest_ip      
   ----------------- -------------- --------------
    www.versive.com   A              172.34.11.11 
    www.versive.com   A              12.12.34.172 
    www.versive.com   A              172.38.10.10 
   ----------------- -------------- --------------

will be updated to:

.. code-block:: sql

   ----------------- -------------- --------------
    named_domain      request_type   dest_ip      
   ----------------- -------------- --------------
    www.versive.com   A              172.34.11.11 
    www.versive.com   A              172.34.12.12 
    www.versive.com   A              172.38.10.10 
   ----------------- -------------- --------------


.. _blocks-library-standardizeusername:

StandardizeUsernameBlock
--------------------------------------------------
Use a standardize username block to remove invalid characters and to identify if the username value contains a domain. If the column contains a domain, a column is added to the tabular output that has the same column name and the domain suffix appended to it.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'StandardizeUsernameBlock'
           columns:
             - name: 'column_name'
               type: data_type
           domain_suffix: '_domain'
           force_lowercase: False
           username_contains_domain: False
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'StandardizeUsernameBlock'``.

**columns** (required, str)
   A list of columns to be standardized, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

**domain_suffix** (optional, str)
   The suffix to append to the column name when ``username_contains_domain`` is set to ``True`` and the column value contains a domain. Default value: ``_domain``.

**force_lowercase** (optional, bool)
   Specifies if the username is converted to lower case. Default value: ``False``.

**username_contains_domain** (optional, bool)
   Specifies if user data contains a domain name. When ``True``, a column is added to the output with the value specified by ``domain_suffix`` appended to it. Default value: ``False``.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following example shows a block named ``clean_user`` that standardizes values in the ``user`` column, and then adds a column named ``user_dom`` for any column value that contains a domain name:

.. code-block:: yaml

   clean_user:
     class_name:
       symbol: 'StandardizeUsernameBlock'
     columns:
       - name: 'user'
         type: 'str'
     force_lowercase: True
     username_contains_domain: True
     domain_suffix: '_domain'











.. _blocks-library-summarizetable:

SummarizeTableBlock
--------------------------------------------------
Use a summarize table block to return summary statistics for each specified column. The column type will determine the type of statistics that are returned. Statistics are converted to string data in the output table, regardless of the input data type.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'SummarizeTableBlock'
           columns:
             - name: 'column_name'
               type: datatype
            ...
          ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'SummarizeTableBlock'``.

**columns** (required, str)
   A list of columns for which a summary is returned, along with the data type for that column.

   **name** (required, str)
      The name of the column.

   **type** (required, str)
      The data type for the column: ``datetime``, ``float``, ``int``, ``ndarray``, ``str``, or ``timestamp``.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
None.









.. _blocks-library-validatecolumn:

ValidateColumnBlock
--------------------------------------------------
Use a validate column block to verify if columns meet the specified criteria. Two types of column validations that may be performed:

* Number of columns with ``None`` values
* Number of column values that match a regular expression

.. warning:: The ``ValidateColumnBlock`` requires that both ``input`` and ``output`` be declared in the configuration. Use the ``extra_columns`` setting to define the policy for how columns not explicitly defined as ``input`` and/or ``output`` are to be handled.

.. TODO: Verify that the validate column block **replaces** the ``validation`` setting group associated with the ``aggregate``, ``parse``, and ``transform`` sections for blocks configuration.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           ...
           class_name: 'ValidateColumnBlock'
           validations:
             - column: 'column_name'
               count: int
               percent: int
               regex: 'string'
               type: validation_type
             - column: 'column_name'
               ...
         ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'ValidateColumnBlock'``.

**validations** (required)
   The settings group for column validations, under which individual column name validations are listed.

   **column** (required, str)
      The name of the column to be validated.

   **count** (optional, int)
      An integer that specifies the maximum number of values that may fail before an error is raised.

   **percent** (optional, int)
      A value between ``0`` and ``100`` that specifies the percentage of values that may fail before an error is raised.

   **regex** (optional, str)
      A regular expression that is used to parse the named column for validation. May only be specified when ``type`` is ``regex``.

   **type** (required, enum)
      The type of validation to perform. Possible values: ``missing`` or ``regex``. The validation to be performed must also specify a ``count``,  ``percent``, or both.

      Use ``missing`` to verify the rate at which ``None`` values may be present for the specified column. If the rate at which ``None`` values exceeds the ``count`` and/or ``percent`` level an error is returned.

      Use ``regex`` to verify column values against a regular expression. If the rate at which column values matching the regular expression exceeds the ``count`` and/or ``percent`` level an error is returned.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
None.







.. _blocks-library-verifyemptycolumns:

VerifyEmptyColumnsBlock
--------------------------------------------------
Use the is-field-empty block to verify if the named column's value is empty. This block outputs a new column that contains a value of ``1`` if the value is not empty and a ``0`` if the value is empty.

Configuration
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: yaml

   block_graphs:
     <block_graph>:
       ...
       blocks:
         <block_name>:
           class_name: 'VerifyEmptyColumnsBlock'
           column: 'column_name'
           output_column_name: 'column_name'
         ...
       ...

Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings are available to this block:

**class_name** (required, str)
   The name of the block. Must be set to ``'VerifyEmptyColumnsBlock'``.

**column** (required, str)
   The name of the column to be verified.

**output_column_name** (optional, str)
   The name of the column in which a ``1`` is added (for non-empty column values) and a ``0`` is added (for empty column values).

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration will verify if the ``arachnid`` column is empty for each row in the table:

.. code-block:: yaml

   is_arachnid_empty:
     class_name:
       symbol: 'VerifyEmptyColumnsBlock'
     columns: 'arachnid'
     output_column_name: 'arachnid_empty'

A table with the following inputs:

.. code-block:: sql

   ----------------
    arachnid       
   ----------------
    scorpion       
    tarantula      
                   
    jumping_spider 
   ----------------

will be updated to:

.. code-block:: sql

   ---------------- ----------------
    arachnid         arachnid_empty 
   ---------------- ----------------
    scorpion         1              
    tarantula        1              
                     0              
    jumping_spider   1              
   ---------------- ----------------



