.. 
.. versive, primary, security engine
.. 

=====================================================
Release Notes
=====================================================

Release notes for the |vse|.


|vse| 1.3
=====================================================
The following sections describe what's new, what's changed, and known issues for the |vse| 1.3 release.

What's New
-----------------------------------------------------
The following items are new to the |vse|:

* **Configuration for data processing phases is moved to blocks_graphs** The ``parse``, ``standardize``, ``transform``, and ``aggregate`` phases are now fully in blocks and must be defined within the ``blocks_graphs`` settings group. This means the previous configuration for these phases must be in blocks. For the 1.3 release, there is a flag in the configuration file that will allow the old configuration settings to work while the configuration file is updated to use blocks for data processing. This change may be disabled for certain configurations for this release only by using the ``enable_deprecated_phases`` setting in the configuration for the entity attribute table.
* **Simplified blocks identification in blocks_graphs** The ``class_name`` setting in the ``blocks_graph`` configuration now uses a string to identify the block name and no longer requires the name of the block to be a symbol. The string may be the name of a block in the blocks library or it may be the name of a user-defined function in the extension package.
* **Simplified blocks identification for entity attribute tables** The configuration for entity attribute tables that use blocks is simplified to require only the name of a block that is defined in the ``block_graph`` section of the configuration file. The dependencies for the entity attribute table are still required, but have been moved into the configuration for the block that is associated with the entity attribute table.

Deprecated Phases
+++++++++++++++++++++++++++++++++++++++++++++++++++++
Starting with |vse| 1.3, blocks are required to define how data processing for parsing, standardizing, transforming, and aggregating data. This is a big change: the entire ``sources`` section---``prepare``, ``parse``, ``standardize_keys``, ``generate_lookup``, ``transform``, ``aggregate``---are no longer required. For this release, a setting named ``enable_deprecated_phases`` may be added to the configuration for an entity attribute table to enable the previous configuration. This is temporary and will be fully deprecated in the 1.4 release.

Block Names as Strings
+++++++++++++++++++++++++++++++++++++++++++++++++++++
The ``class_name`` setting in the ``blocks_graph`` configuration now uses a string to identify the block name and no longer requires the name of the block to be a symbol. The string may be the name of a block in the blocks library or it may be the name of a user-defined function in the extension package.

The configuration that was required prior to this release:

.. code-block:: yaml

   block_graphs:
     count_rows:
       class_name:
         symbol: CategorizeBlock

The configuration that is required starting with this release:

.. code-block:: yaml

   block_graphs:
     count_rows:
       class_name: 'CategorizeBlock'


Simplified EATs for Blocks
+++++++++++++++++++++++++++++++++++++++++++++++++++++
Configuring entity attribute tables for use with blocks is simplified. The entity attribute table now only must specifiy the name of the block. Dependencies are still required, but are moved from the entity attribute table to the ``inputs`` section of blocks configuration. All other settings are no longer required.

The following examples show a block named ``count_rows`` that is configured to do the processing related to an entity attribute table named ``process_rows_count``.

The configuration that was required prior to this release:

.. code-block:: yaml

   block_graphs:
     count_rows:
       ...
     ...
   process_rows_count:
     block_names:
       - row_count_stats_graph
     generate: unneeded
     key_columns: []
     attribute_columns: []
     type: PRE_MODELING_TABLE_ATTRIBUTE
     path_add_date_suffix: True
     depends_on:
       - datasource: proxyweb
         phase: parsed
       - datasource: proxyweb
         phase: parsed_error

The configuration that is required starting with this release:

.. code-block:: yaml

   block_graphs:
     count_rows:
       ...
       inputs:
         - datasource: proxyweb
           phase: parsed
         - datasource: proxyweb
           phase: parsed_error
     ...
   ...
   entity_attribute_sources:
     process_rows_count:
       block_name:
         - count_rows



What's Changed
-----------------------------------------------------
The following features and functionality have changed from previous versions of the |vse|:

* The ``aggregate``, ``parse``, ``standardize``, and ``transform`` sections of the confiugration file are replaced by blocks. Use blocks from the blocks library to build a blocks graph that parses, standardizes, transforms, and then aggregates data.

Known Issues
-----------------------------------------------------
None.











|vse| 1.2
=====================================================
The following sections describe what's new, what's changed, and known issues for the |vse| 1.2 release.

What's New
-----------------------------------------------------
The following items are new to the |vse|:

* **Mean absolute log-quotient (MALQ)** measures the average order-of-magnitude difference between predicted and actual values.
* **Support for subnet labels in modeling tables** The ``generate_modeling`` section of the configuration file supports the ``subnet_labels`` grouping, in which subnet labels may be defined for subnets that use network addresses and masks and for subnets that use CIDR notation.
* **Multiple suspicious hosts in a single threat case** The |vse| builds threat cases that contain multiple suspicious hosts.
* **Allowed, disallowed, and ignored performance warnings** Define a global whitelist of performance warnings that specify performance warning types---allowed, disallowed, and ignored---that, when they are thrown as exceptions by the engine, are allowed, disallowed, or ignored.
* **Blocks replace entity attribute tables for data processing** Use blocks to define and configure data processing for DNS, proxy, and flow data sources. Blocks replace the parse, standardize, transform, and aggregate data processing (everything up to just prior to generating the modeling table). Use blocks in the :doc:`blocks library <blocks>` to define this processing.
* **Command-line help for blocks inputs and outputs** The bloxplain tool may be run from the command-line to display help about a block, including an example inputs and outputs for that block.
* **Custom blocks may be added to extensions.py** Custom blocks may be added to the customization package, similar to other customizations.
* **Threat cases for single host with noteworthy score or noteworthy scores combined across stages** The |vse| now generates threat cases that identify a single host with a noteworthy score in each of the |stage_recon|, |stage_collect|, and |stage_exfil| stages **or** identifies multiple hosts that combine to have noteworthy scores in each of the |stage_recon|, |stage_collect|, and |stage_exfil| stages.
* **URLs converted to strings** URLs are converted to strings when the input URL is in Unicode, UTF-8, or is already ASCII.
* **Generate UUIDs for threat cases randomly** The ``use_random_anomaly_ids`` setting specifies if UUIDs are generated randomly (default) or are hash-based identifiers. When ``True``, UUIDs are generated randomly. When ``False``, identifiers are hash-based.
* **Custom blocks examples** Blocks EXAMPLES are now required for custom blocks in the extensions package.
* **New argument for risp.merge_tables()** Use the ``repartition`` argument with the :ref:`api-merge-tables` function to specify the partitioning behavior for the merged output table based on the partitions that exist in the input tables.


New Metric: MALQ
+++++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_metric_malq.rst

Subnet Labels
+++++++++++++++++++++++++++++++++++++++++++++++++++++
Subnet labels can help the |vse| identify subsets of an internal network. Use a CSV file to specify a list of subnets for to labels should be applied. The |vse| will process the ``hostname`` and ``fqdn`` columns against the contents of this CSV file to add source and destination IP address labels to the modeling table.

Use the ``subnet_labels`` settings group in the :ref:`configure-generate-modeling` section of the configuration file to configure how subnet labels are added to modeling tables.

Multiple Hosts
+++++++++++++++++++++++++++++++++++++++++++++++++++++
The |vse| builds :ref:`threat cases that contain multiple suspicious host machines <model-data-multihost>`. Two entity attribute tables define multihost behavior, along with a set of multihost-specific configuration settings:

#. Configuration settings that are part of the :ref:`results <model-data-multihost-config>` section of the configuration file
#. A table that defines the :ref:`connectivity graph <model-data-multihost-connectivity>`
#. A table that defines the :ref:`filtering levels <model-data-multihost-filtering-levels>` to be applied.

Threat Case Levels
+++++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_threat_case_level.rst

The |vse| now generates threat cases that identify a single host with a noteworthy score in each of the |stage_recon|, |stage_collect|, and |stage_exfil| stages **or** identifies multiple hosts that combine to have noteworthy scores in each of the |stage_recon|, |stage_collect|, and |stage_exfil| stages.

Performance Warnings
+++++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_config/performance_warnings.rst


Configuration Settings
+++++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings have been added to the configuration file:

* **encoding**: The encoding of data may now be specified globally or per-parsing group.
* **load_split_from_workspace**: A fully qualified path to a previously-saved modeling split table may be specified. Use this to speed up processing by reusing modeling splits for training, tuning, and testing.
* **join_broadcast_rows_limit**: Specifies the maximum number of rows (default---50,000 rows) in a table that may be present before the engine will stop using broadcast when joining individual tables into the daily modeling table.
* **performance_warnings**: A global whitelist that defines how to handle performance warning types---allowed, disallowed, and ignored.
* **use_random_anomaly_ids**: Specifies if UUIDs are generated randomly (default) or are hash-based identifiers. When ``True``, UUIDs are generated randomly. When ``False``, identifiers are hash-based.

.. The following settings are supposed to be replaced, soon, any day now, they determine thresholds for some outputs that are visible in the user interface, probably, but not for much longer.
.. TODO: publication_ratio_threshold, float, default = 2.0
.. TODO: consumption_ratio_threshold, default = 3.0
.. TODO: exclusivity_threshold, float, default = 1.0

Blocks
+++++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_blocks.rst

Blocks Library
+++++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/blocks_library_list.rst

bloxplain
+++++++++++++++++++++++++++++++++++++++++++++++++++++
bloxplain is a command-line tool that can be run to view information about a block (including input and output examples) for any block in the :ref:`default blocks library <blocks-library>` or any custom block that is defined in the customization package.

To view information about a block, run a command similar to:

.. code-block:: console

   $ bloxplain config.yml BlockName

For example:

.. code-block:: console

   $ bloxplain config.yml AggregateBlock


Repartition After Merge
+++++++++++++++++++++++++++++++++++++++++++++++++++++
The :ref:`api-merge-tables` function has a new argument: ``repartition``. Use this function to define the partitioning behavior for the output table of a merge operation that is based on the partitions that exist in the input tables.

.. include:: ../../includes_api/function_merge_tables_repartition.rst


What's Changed
-----------------------------------------------------
The following features and functionality have changed from previous versions of the |vse|:

* The following settings were removed from the configuration file: ``add_cumulative``, ``add_historical``, ``add_newness``, ``add_uniqueness``, ``lookback``, ``lookback_cumulative``, ``lookback_historical``, and ``percentiles``.
* The following options were removed from commands that run the |vse|: ``add_cumulative``, ``add_historical``, ``add_newness``, ``add_uniqueness``, ``load_pipeline_state``, ``lookback``, ``lookback_cumulative``, ``lookback_historical``, and ``percentiles``.
* The intermediate and features tables are deprecated. These tables were used to identify communication between clients and servers within the network.
* User-defined functions for the :ref:`configure-generate-modeling` section of the configuration file will no longer work. This functionality has been replaced by blocks. User-defined functions specified by the ``replace_function`` setting may require a migration to blocks. The following settings are removed: ``after_save_functions``, ``on_load_function``, ``on_save_function``, and ``replace_function``.
