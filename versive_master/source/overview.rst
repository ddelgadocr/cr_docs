.. 
.. versive, primary, security engine
.. 
.. old overview; deprecated
.. old contents kept here, commented out for now
.. new contents copied from vse.rst
.. 

.. include:: ../../versive_master/source/vse.rst

.. 
.. ==================================================
.. About the |vse|
.. ==================================================
.. 
.. The |vse| analyzes data across rolling time periods (including the current day) to identify interesting behaviors, links them together, and then presents them as ranked threat cases when evidence suggests a potential threat.
.. 
.. .. image:: ../../images/workflow_overview.*
..    :width: 600 px
..    :align: center
.. 
.. #. Process raw data collected from proxy, flow, and DNS data sources
.. #. Build modeling tables
.. #. Discover interesting behaviors
.. #. Build threat cases
.. 
.. 
.. Process Data
.. ==================================================
.. On a daily basis the engine ingests proxy, netflow, and DNS data for analysis.
.. 
.. * .. include:: ../../includes_terms/term_datatype_dns.rst
.. * .. include:: ../../includes_terms/term_datatype_flow.rst
.. * .. include:: ../../includes_terms/term_datatype_proxy.rst
.. 
.. 
.. Behaviors
.. ==================================================
.. The engine compares the results of the current day's modeling results to N days, typically measured by a 7 day rolling time period grouped by 7, 14, 21. This means that the engine looks at the current day, and then compares it to the previous week, previous two weeks, and previous three weeks when analyzing data. After the results are compared, a threat list is generated.
.. 
.. 
.. Model Data
.. ==================================================
.. The engine uses all available data from all available source types---proxy, netflow, and DNS---to create a graph that details which hosts communicated with each other, complete with directed edges that show the direction of information flow. The most suspicious score per host is identified, and then suspicion scores are summed for stage 3, stage 4, and stage 5. Hosts that show evidence of multi-stage interest are identified.
.. 
.. 
.. Build Cases
.. ==================================================
.. After behaviors are discovered and assigned, they are mapped to stages. Threats are linked, starting with any behaviors discovered for the current day, after which those are compared to previously-discovered behaviors in earlier time periods. And then the threat case list is generated.
.. 
.. Map Behaviors to Stages
.. --------------------------------------------------
.. After the threat list is generated, the engine maps behaviors that indicate potential threats, and then map those threats to stages.
.. 
.. Stage 3 looks for behaviors that establish a foothold in the network, show movement across the internal network, and enable information gathering. For example, tracking the number of internal neighbors for each host, which internal neighbors communicated with each other, the ports used, and the port distribution per IP address. Types of behaviors seen in stage 3 include:
.. 
.. * Beaconing from a host inside the network to a host outside the network
.. * Reconnaissance of intranet resources
.. * Reconnaissance of the network
.. * Traffic to or from malicious domains
.. * Unusual netflows
.. * Unusual user agents
.. 
.. Stage 4 looks for behaviors that indicate data is being gathered and staged into single locations. For example, cumulative bytes to a target with a high consumption ratio and many neighbors. Types of behaviors seen in stage 3 include:
.. 
.. * Collection from intranet or cloud services
.. * Unusual netflows
.. 
.. Stage 5 looks for behaviors that indicate data is being moved outside the network. For example, a high publication ratio from a single host to exclusive regdomains. The most important behavior seen in stage 5 is exfiltration from a host inside the network to a host outside the network.
.. 
.. Behaviors are most interesting when they correlate across stages 3, 4, and 5. Behviors that ocurr across only two stages are interesting. Behaviors that only occur in only stage 3 or 4 are less interesting.
.. 
.. 
.. Generate Threat Cases
.. --------------------------------------------------
.. Internally to the engine, the threat linker loads all the warnings in the date range for stages 3, 4, and 5, and then keeps the behaviors with the 10-percent highest strength per stage. The threat linker merges the daily communication graphs based on aggregated source data, and then traces the sub-graph of the host plus reachable machines. The top K threats become the top K threat cases. Each warning is divided into details. Data source, behavior, and strength are broken into separate columns. The top K threat cases are then shown with results, summaries, and scores.
.. 
.. 

.. TODO: Put here for now, as it should be in the overview, needs to be single sourced with the entry on the describe_behaviors page.

.. 
.. Malicious Domains
.. ==================================================
.. A malicious domain is a website, URL, or an IP address that is known to be associated with malware, malicious activity, or is otherwise potentially harmful. Malicious domains are typically blocked so that users within the organization may not access them.
.. 
.. .. image:: ../../images/malicious_domains.*
..    :width: 600 px
..    :align: center
.. 
.. The engine predicts malicious domains by evaluating all unique domains visited by users in your organization, filtering out all of the known websites, URLs, and IP addresses that are already in the blocked list, and then filtering out all of the websites, URLs, and IP addresses that are defined in the list of acceptable domains.
.. 
.. The remaining domains are considered potentially malicious and are assigned a score. The engine generates a list of the top 100 domains that were assigned scores with the highest likelihood of that domain being malicious.
.. 


.. TODO: Put here for now, as it should be in the overview, needs to be single sourced with the entry on the describe_behaviors page.

.. 
.. Beaconing
.. ==================================================
.. Beaconing occurs when short, regular communications from a host inside the network are sent to some domain that is located outside of the network. Beaconing by itself is generally not an indicator of an adversary campaign and is often common (and benign) network activity.
.. 
.. .. image:: ../../images/beaconing.*
..    :width: 600 px
..    :align: center
.. 
.. That said, an adversary campaign will use beaconing to verify that a host inside the network is alive, is functioning, and is ready to receive instructions. This is sometimes referrred to as "command and control" within the APT kill chain.
.. 
.. For domains that are analyzed for beaconing behaviors, they are run through three stages:
.. 
.. #. Computing raw beaconing statistics for each host/destination pair for multiple days.
.. #. Computing raw beaconing statistics for each host/destination pair for a single day.
.. #. Computing a beaconing score for each host/destination pair for a range of days.
.. 
.. The engine generates beaconing scores based on careful analysis of proxy log data, and then provides a list of hosts on which beaconing behaviors are discovered for the previous 24-48 hours.
.. 

   