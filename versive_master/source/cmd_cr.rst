.. 
.. versive, primary, security engine
.. 

==================================================
Cluster Commands
==================================================

The platform has five groups of commands for managing the compute cluster, along with the :ref:`cmd-cr-version` command. Arguments for these commands may be specified with or without an equals sign (``=``). Use the ``-h`` or ``--help`` argument to print help about that command in the command shell.

There are six primary groups of commands:

* :ref:`cmd-cr-cluster`
* :ref:`cmd-cr-connect`
* :ref:`cmd-cr-install`
* :ref:`cmd-cr-node`
* :ref:`cmd-cr-service`
* :ref:`cmd-cr-version`



.. _cmd-cr-cluster:

cr cluster
==================================================
The ``cr cluster`` group of commands enable the management of nodes in the |versive| platform cluster. Nodes in the cluster may be in one of three states:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - State
     - Description
   * - Compute
     - A compute node is an active member of the compute cluster.
   * - Free
     - A free node is ready to be assigned to the compute cluster, but is otherwise not part of the active compute cluster.
   * - Unconfigured
     - An unconfigured node are not assigned to the compute cluster and the cluster is unaware of them. Unconfigured nodes may be assigned to the cluster first by adding them to the pool of free nodes.

The general workflow for the ``cr cluster`` commands is:

#. Use ``cr cluster`` commands to manage compute, free, and unconfigured nodes in the cluster.
#. A single compute node must be the primary node; all other compute nodes are replicas.
#. Use :ref:`cmd-cr-cluster-add` to assign an unconfigured node to the compute cluster as a free node.
#. Use :ref:`cmd-cr-cluster-activate` to assign a free node to the compute cluster, and then activate it.
#. Use :ref:`cmd-cr-cluster-free` to deactivate a compute node, and then move it to the pool of free nodes.
#. Use :ref:`cmd-cr-cluster-delete` to move a compute node or a free node out of the cluster and to the pool of unconfigured nodes.

The following commands are available for managing nodes in the cluster:

* :ref:`cmd-cr-cluster-activate`
* :ref:`cmd-cr-cluster-add`
* :ref:`cmd-cr-cluster-create`
* :ref:`cmd-cr-cluster-delete`
* :ref:`cmd-cr-cluster-free`
* :ref:`cmd-cr-cluster-getprop`
* :ref:`cmd-cr-cluster-health`
* :ref:`cmd-cr-cluster-join`
* :ref:`cmd-cr-cluster-reset`
* :ref:`cmd-cr-cluster-setprop`
* :ref:`cmd-cr-cluster-status`
* :ref:`cmd-cr-cluster-view`

.. _cmd-cr-cluster-activate:

cr cluster activate
--------------------------------------------------
Activate a node from the list of free nodes. This node may be placed into an existing compute cluster as a compute node, or it may become the primary node for a new cluster that is created at the same time.

.. note:: If the node is in an unconfigured state, the :ref:`cmd-cr-cluster-add` command must be run on the node prior to running the :ref:`cmd-cr-cluster-activate` command.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster activate --node=IP_ADDRESS --cluster=NAME [--create --streaming]

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``--cluster=NAME``
   The name of the cluster to which the node is added.

``--create``
   Create a new cluster with the name specified by the ``--cluster`` option, and then set this node to be the primary node.

``--node=IP_ADDRESS``
   The IP address of the node to activate.

``--streaming``
   Activate the node as a streaming execution server instead of a batch training node.



.. _cmd-cr-cluster-add:

cr cluster add
--------------------------------------------------
Add a node to the cluster's free list. This command must be run from any node that is already in the cluster, after which the :ref:`cmd-cr-cluster-activate` command must be run to configure the node as a compute or primary node.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster add --node=IP_ADDRESS --require_ssl

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``--node=IP_ADDRESS``
   The IP address of the node to add to the list of free nodes for this cluster.

``--require_ssl``
   If specified, require Secure Sockets Layer (SSL) for HDFS connections.




.. _cmd-cr-cluster-create:

cr cluster create
--------------------------------------------------
Create a new cluster, and then set the current node to be the primary node of the new
cluster.

.. warning:: It is strongly recommended to avoid using this command and instead use the installer for the |versive| platform.

.. TODO: integrate these?
.. 
.. To reconfigure an existing compute cluster, SEE "CHANGE_MASTER".
.. 

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster create <CLUSTER_NAME> [--elastic --streaming --properties_file=FILE]

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``--elastic``
   Create an elastic cluster; applies only to Amazon EC2.

``--properties_file=FILE``
   The INI-style file containing cluster properties that will be set on the new cluster.

``--streaming``
   Create a streaming execution server instead of a batch training cluster.




.. _cmd-cr-cluster-delete:

cr cluster delete
--------------------------------------------------

If a node fails or is permanently removed from the compute cluster, it is considered a dead node. A dead node should be removed from the compute cluster completely and reset to an unconfigured state, after which it is added to the list of free nodes and from which it can be re-added to a compute cluster.

.. warning:: A node must be deleted from the compute cluster before it is shut down.

.. TODO: integrate the following two sentences:
.. 
.. If a node is shut down without warning, SEE "TROUBLESHOOTING_DEAD_NODES".
.. 
.. If a primary node is deleted, all of its compute nodes are added to the list of free nodes. To reconfigure an operational cluster, SEE "CHANGE_MASTER".
.. 

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster delete --node=IP_ADDRESS

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``--node=IP_ADDRESS``
   The IP address of the node to delete from the cluster.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. TODO: also a troubleshooting topic

**Remove a single dead node from the compute cluster**

From any node in the compute cluster:

.. code-block:: console

   $ cr cluster delete --node=<ip_address>

and then:

.. code-block:: console

   $ cr cluster reset --node=<ip_address>


.. _cmd-cr-cluster-free:

cr cluster free
--------------------------------------------------
Remove a node from a cluster, and then add it to the list of free nodes. If a primary node is freed, all of its compute nodes are also added to the list.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster free --node=IP_ADDRESS

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``--node=IP_ADDRESS``
   The IP address of the node to free.




.. _cmd-cr-cluster-getprop:

cr cluster getprop
--------------------------------------------------
View the current values for all cluster properties or the named cluster property.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster getprop [NAME=VALUE]

.. note:: Values with spaces must be enclosed in quotes: ``NAME='complex value'``.


Options
++++++++++++++++++++++++++++++++++++++++++++++++++
None.




.. _cmd-cr-cluster-health:

cr cluster health
--------------------------------------------------
Display health information about each node in the cluster.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster health

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ cr cluster health

will print information similar to:

.. code-block:: none

   === Health for node http://10.90.128.188:9002 (Master)
       Infrastructure: OK (Watch=2 Nodemon=1)
       Task Executors: OK (Active=0 Expected=0)
       Overall Health: Healthy
   === Health for node http://10.220.204.48:9002 (Compute)
       Infrastructure: OK (Watch=2 Nodemon=1)
       Task Executors: OK (Active=4 Expected=4)
       Overall Health: Healthy
   === Health for node http://10.213.154.225:9002 (Compute)
       Infrastructure: OK (Watch=2 Nodemon=1)
       Task Executors: OK (Active=4 Expected=4)
       Overall Health: Healthy



.. _cmd-cr-cluster-join:

cr cluster join
--------------------------------------------------
Join a node to the cluster, and then activate it.

.. note:: A node may be joined by running this command on the node itself or from the primary node. That said, the preferred method of joining a node to a cluster is to use the :ref:`cmd-cr-cluster-add` and/or :ref:`cmd-cr-cluster-activate` commands.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster join [--master=MASTER --node=IP_ADDRESS]

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``--master=MASTER``
   The IP address of the primary node to which you want to join the specified node as a compute node. If a primary node isn't specified, the current node is considered the primary node.

``--node=IP_ADDRESS``
   The IP address of the compute node to join to the cluster. If a node isn't specified, the current node is considered the compute node.




.. _cmd-cr-cluster-reset:

cr cluster reset
--------------------------------------------------
Restart all |versive| platform services on all of the nodes in a cluster.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster reset [--name=NAME]

or:

.. code-block:: console

   crreset [--name=NAME]

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``--name=NAME``
   The name of the cluster to reset. If not specified, the cluster containing the current node is reset.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. TODO: same exact example as cr cluster delete

**Remove a single dead node from the compute cluster**

From any node in the compute cluster:

.. code-block:: console

   $ cr cluster delete --node=<ip_address>

and then:

.. code-block:: console

   $ cr cluster reset --node=<ip_address>


.. _cmd-cr-cluster-setprop:

cr cluster setprop
--------------------------------------------------
Add or remove a property from the cluster configuration.

.. warning:: The cluster must be reset using the :ref:`cmd-cr-cluster-reset` command before property changes take effect.

.. TODO: The following phrase in the note appears in many spots. Single source this one for absolute sure.

.. note:: The default installation directory is ``/opt/cr``. If the |versive| platform is installed to a different location, substitute that path for ``/opt/cr``.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster setprop [NAME=VALUE]

.. note:: Values with spaces must be enclosed in quotes:

   .. code-block:: console

      $ cr cluster setprop NAME='complex value'
   
Options
++++++++++++++++++++++++++++++++++++++++++++++++++

.. there's a big list of properties in /repo/cluster/crmr/crmr/knobs.pyx

The following properties are available:

``AUTO_DETECT_TIMESTAMPS``
   A Boolean value that indicates if data times in non-typed RDDs are recognized as Timestamp (``True``) or datetime.datetime (``False``). Default value: ``False``.

``CORES_RESERVED_COMPUTE``
   The number of cores on compute nodes to exclude from use by the |versive| platform task executors. Default value: ``0``.

``CORES_RESERVED_MASTER``
   The number of cores on the primary node to exclude from use by the |versive| platform task executers. Default value: ``4``.

``CRMR_DEFAULT_COMPUTE_VERSION``
   The default compute version on the cluster. Set to ``4`` for Apache Spark. Default value: ``2``.

``CRMR_FILE_COMPRESS_DEFAULT``
    A Boolean value that indicates if DataTables are compressed by default. Default value: ``False``.

``CRMR_EXECUTER_HOST_MEMORY_USAGE``
   The fraction of all memory on the host node that each task executor may use, with all task executors contending for the available memory. Use ``0`` to split memory equally among task executors. For example, if set to 0.6, each task executor can use up to 60 percent of the node's memory. Default value: ``0``.

``MEM_RESERVED_COMPUTE``
   The amount of memory (in gigabytes) on the compute nodes to exclude from use by the |versive| platform task executors. The planner/interactive process runs in the reserved space. Default value: ``1``.

``MEM_RESERVED_MASTER``
   The amount of memory (in gigabytes) on the primary node to exclude from use by the |versive| platform task executors. The planner/interactive process runs in the reserved space. Default value: ``16``.

``PIPELINE_ROW_TRANSFORMS``
    A Boolean value that indicates if row transforms using the compute infrastructure are pipelined. Default value: ``True``.

YARN on Spark
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options add compatibility for YARN on Spark:

``SPARK_JSON_ARGS``
   Any additional arguments desired.

``SPARK_JAR_PATH``
   The path for additional jars to ship out.

``SPARK_KERBEROS_KEY_TAB``
   The path to the keytab file.

``SPARK_KERBEROS_PRINCIPAL``
   The Kerberos principal for YARN to use.

``SPARK_MASTER``
   The 'yarn-client' to work with YARN.




.. _cmd-cr-cluster-status:

cr cluster status
--------------------------------------------------
Display performance metrics about every node in the cluster.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster status [--loop=LOOP --debug]

or:

.. code-block:: console

   crstatus [--loop=LOOP --debug]

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``--block``
   Continue running the command in a loop until all nodes are healthy.

``--debug``
   Display additional information for debugging.

``--loop=LOOP``
   Specify how frequently to display the status, in seconds. If not specified, the current status is displayed and the command exits.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. include:: ../../includes/cmd_cr_cluster_status.rst




.. _cmd-cr-cluster-view:

cr cluster view
--------------------------------------------------
Display all nodes in the cluster, their cluster membership, states, and roles.

* Run this command from any node in the cluster to view information about the entire cluster.
* Run this command from an unconfigured node to view information about only that node.

Use :ref:`cmd-cr-cluster-status` to view the health details for the entire cluster.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr cluster view

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++

**One primary node, two compute nodes**

The following output shows the information about a cluster that contains one primary node and two compute nodes:

.. code-block:: console

   $ cr cluster view

returns:

.. code-block:: none

   test-cluster: (uuid = 119920a46f6f4f808c663904d49018df)
   10.250.57.149: (uuid = 3f7ca8b13176409c85c8a9acb02f6fe2)
     leader_node
     config_master
   10.250.3.50: (uuid = b2837810df854aeda014ceee6c17b9b2)
     compute_node
   10.224.6.225: (uuid = b2f9f11851de4b3c909d698d1ab3f659)
     compute_node


**One primary node, one unassigned node**

The following output shows the information about a cluster that contains one primary node and one unassigned node in a free state:

.. code-block:: console

   $ cr cluster view

returns:

.. code-block:: none

   empty-cluster: (uuid = 27583dfa206c48a0a3c20beb59876c62)
   10.250.57.149: (uuid = 1c5253f148a941029dfd836332867633)
     leader_node
     config_master
   Free nodes:
     10.250.9.95: (uuid = afdf01ae0893436489f5de200286cbf9)

To enable the free node to do work, run the :ref:`cmd-cr-cluster-activate` command on it to assign it to the cluster.



.. _cmd-cr-connect:

cr connect
==================================================
.. include:: ../../includes/named_connections.rst

Use the following commands to manage persistent, named connections to the storage cluster:

* :ref:`cmd-cr-connect-add`
* :ref:`cmd-cr-connect-list`
* :ref:`cmd-cr-connect-remove`
* :ref:`cmd-cr-connect-show`



.. _cmd-cr-connect-add:

cr connect add
--------------------------------------------------
Add a named connection to a storage cluster.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr connect add --name=NAME --type=TYPE [option=value] ...

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command for all storage options:

``--desc=DESC``
   Description of the new connection.

``--name=NAME``
   Name of the new connection.

``--overwrite``
   Overwrite an existing connection, if present.

``--type=TYPE [option=value]``
   Type of the new connection: ``hdfs``, ``local``, or ``s3``). Depending on the connection type, additional arguments are available.

Amazon S3
++++++++++++++++++++++++++++++++++++++++++++++++++
For commands run against Amazon S3 storage, the following option:value pairs may be used:

``aws_access_key_id``
   The Amazon S3 access key ID. If unspecified, use environment.

``aws_secret_access_key``
   The Amazon S3 secret access key. If unspecified, use environment.

``base_path``
   Base path used for all operations.

``bucket``
   The Amazon S3 bucket to use. If unspecified, use environment.

``default_host``
   The AWS region to use.

.. note:: These settings may be specified as environment variables instead of passing them via the command line. Use ``AWS_ACCESS_KEY_ID``, ``AWS_SECRET_ACCESS_KEY``, ``S3_DEFAULT_BUCKET``, and ``S3_DEFAULT_HOST`` to specify environment variables..

HDFS
++++++++++++++++++++++++++++++++++++++++++++++++++
For commands run against HDFS storage, the following option:value pairs may be used:

``base_path``
   Base path used for all operations.

``delegation_token``
   The token to authenticate as user.

``host``
   The HDFS host to which the connection is made.

``port``
   Port to use to connect to HDFS storage cluster.

``kerberos_keytab``
   The absolute path to the user's keytab file.

``kerberos_realm``
   The Kerberos realm that defines what the user has access to.

``kerberos_user``
   The user (principal) to authenticate with when using a keytab file.

``user``
   The user to authenticate with when using a delegation token.

Local
++++++++++++++++++++++++++++++++++++++++++++++++++
For commands run against local storage, the following option:value pairs may be used:

``base_path``
   The base path used for all operations.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++
The following examples show how to add storage clusters.

**Add an Amazon S3 storage cluster**

.. code-block:: console

   $ cr connect add --name='aws' --type='s3' bucket='company_bucket' \
     aws_access_key_id='ASsdthbef9CA20' \
     aws_secret_access_key='xzlk9adkjasdtgffwKFJ9Qkljdfaf'

**Add an HDFS storage cluster using delegation tokens**

.. code-block:: console

   $ cr connect add --name=auth_hdfs --type=HDFS host=10.213.168.179 user=hdfs \
     delegation_token='4xNjasdfgrejgwMjA'

**Add an HDFS storage cluster using keytab files**

.. code-block:: console

   $ cr connect add --name=auth_hdfs --type=HDFS \
     host=10.213.168.179 port=50070 kerberos_user=username \
     kerberos_realm=my_realm kerberos_keytab=/auth/settings/username.keytab

**Connect to a storage cluster with an existing named connection**

Connections to a storage cluster that already have a named connection may omit extra type-specific parameters. For example, an AWS S3 storage cluster with an existing named connection named ``aws`` does not require the ``bucket``, ``aws_access_key_id``, and ``aws_secret_access_key`` options to be specified:

.. code-block:: console

   $ cr connect add --name='aws' --type='s3' 


.. _cmd-cr-connect-list:

cr connect list
--------------------------------------------------
List the persistent, named connections in the system.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr connect list

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

**Amazon S3 storage**

.. include:: ../../includes/cmd_cr_connect_list_amazon_s3.rst

**HDFS storage**

.. include:: ../../includes/cmd_cr_connect_list_hdfs.rst




.. _cmd-cr-connect-remove:

cr connect remove
--------------------------------------------------
Remove a persistent, named connection from the system.

.. warning:: Data that is stored in the storage cluster on a named connection cannot be accessed after it is removed.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr connect remove --name=NAME

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``--name=NAME``
    The named connection to be removed.



.. _cmd-cr-connect-show:

cr connect show
--------------------------------------------------
Show the configuration for the named connection.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr connect show --name=NAME

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``--name=NAME``
    The name of the connection for which details are shown.



.. _cmd-cr-install:

cr install
==================================================
Manage the versions of the |versive| platform and applications that are installed on the cluster:

* :ref:`cmd-cr-install-activate`
* :ref:`cmd-cr-install-deactivate`
* :ref:`cmd-cr-install-list`
* :ref:`cmd-cr-install-rollback`


.. _cmd-cr-install-activate:

cr install activate
--------------------------------------------------
Activate a version of the |versive| platform or an application on all nodes in the cluster.

.. note:: Use the :ref:`cmd-cr-install-list` command to get a list of applications, platforms, and snapshots.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr install activate [app=APP_NAME platform=PLATFORM snapshot=SNAPSHOT]

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``app=APP_NAME``
   The name of the application, including version number, to be activated. If a version of that application is already in use, the version is updated.

``platform=PLATFORM``
   The name of the version of the |versive| platform to activate.

``snapshot=SNAPSHOT``
   The name of the snapshot to activate.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

Switch between different versions of the |versive| platform or an application. For example:

.. code-block:: console

    $ cr install activate app <previous version>

then:

.. code-block:: console

    $ cr install activate app <new version>

and then:

.. code-block:: console

    $ cr install activate app <previous version>

The named application is set to be the active application and the previously-active application is deactivated. There is no need to use the :ref:`cmd-cr-install-deactivate` command.


.. _cmd-cr-install-deactivate:

cr install deactivate
--------------------------------------------------
Deactivate an application on all nodes in the cluster.

.. note:: Use the :ref:`cmd-cr-install-list` command to get a list of applications.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr install deactivate [app=APP_NAME]

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``app=APP_NAME``
   The name of the application, including version number, to be deactivated.



.. _cmd-cr-install-list:

cr install list
--------------------------------------------------
List all versions of the |versive| platform, along with any installed applications or snapshots that
are available on all nodes in the cluster.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr install list app platform snapshot

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``app``
   List all versions of all installed engines.

``platform``
   List all installed versions of the |versive| platform.

``snapshot``
   List all snapshots that are available on all nodes in the cluster under the ``$CR_INSTALL`` directory.



.. _cmd-cr-install-rollback:

cr install rollback
--------------------------------------------------
.. TODO: Which of the following two statements is the correct statement? Also, how "old" is that note? Which version are we on? And should that be in that literal spot or a "release notes" type of page?

.. include:: ../../includes/cmd_cr_install_rollback.rst



Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr install rollback [SNAPSHOT]

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
None.



.. _cmd-cr-node:

cr node
==================================================
Modify a node:

* :ref:`cmd-cr-node-datadirectory`
* :ref:`cmd-cr-node-hardreset`
* :ref:`cmd-cr-node-ports`
* :ref:`cmd-cr-node-pythonlink`
* :ref:`cmd-cr-node-setuser`



.. _cmd-cr-node-datadirectory:

cr node datadirectory
--------------------------------------------------
Set the data directory of the node. This location should end in ``/cr``. If a path is not specified, the command will show the current data directory.

.. warning:: This command requires administrator privileges.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   sudo cr node datadirectory=PATH

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
None.


.. _cmd-cr-node-hardreset:

cr node hardreset
--------------------------------------------------
Reset a node to an unconfigured state, after which it can be assigned to a cluster using the :ref:`cmd-cr-cluster-add` command. This command does not tell other nodes in the cluster that it is being reset. To recreate an entire compute cluster in that cluster, first run this command on each node individually.

.. warning:: This command should only be used as a last resort, as using it can leave the compute cluster in an inconsistent state. Try using the :ref:`cmd-cr-cluster-delete` command before running this command.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr node hardreset [--node=IP_ADDRESS --quiet]

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available to this command:

``--node=IP_ADDRESS``
   The IP address of the node to be reset, and then completely removed from the cluster. If this argument is unspecified, reset the current node.

``--quiet``
   Reset the node without prompting.



.. _cmd-cr-node-ports:

cr node ports
--------------------------------------------------
Display TCP/IP ports that are active for |versive| platform services on a node. This command must be run on every node in the cluster to view all of the ports that are in use across the cluster.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr node ports

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ cr node ports

will print information similar to:

.. code-block:: none

   Port     Service
   8000     execution-server
   9100     query-server
   9101     data-tracker
   9103     node-monitor
   9104     query-server



.. _cmd-cr-node-pythonlink:

cr node pythonlink
--------------------------------------------------
Specify the Python binary link, or create it if none exists by specifying the path to use after the command. 

.. note:: This command may require administrator privileges.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr node pythonlink=PATH

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
None.


.. _cmd-cr-node-setuser:

cr node setuser
--------------------------------------------------
Set the user under which |versive| platform services run. This user must already exist.

.. warning:: This command requires administrator privileges.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   sudo cr node setuser=USER_NAME

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
None.


.. _cmd-cr-service:

cr service
==================================================
.. warning:: Deprecated. The ``cr service list`` command should return zero services.

.. Modify or view |versive| platform services running on the current node:

* :ref:`cmd-cr-service-list`

.. cr service list should return no services to start, stop, etc. removing:
.. 
.. * :ref:`cmd-cr-service-start`
.. * :ref:`cmd-cr-service-stop`
.. 

.. _cmd-cr-service-list:

cr service list
--------------------------------------------------
Display a list of all |versive| platform services that are running on the current node.

.. warning:: The list will contain zero services.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ cr service list

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ cr service list

will return a list of zero services.


.. 
.. .. _cmd-cr-service-start:
.. 
.. cr service start
.. --------------------------------------------------
.. Restart |versive| platform services on the current node. This makes the node available to the platform cluster and available for all tasks.
.. 
.. .. note:: Use the :ref:`cmd-cr-cluster-reset` command to restart services on all nodes in the cluster.
.. 
.. Usage
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. 
.. .. code-block:: console
.. 
..    $ cr service start [--no-stop --only=SERVICE_NAME --hard]
.. 
.. or:
.. 
.. .. code-block:: console
.. 
..    $ crstart [--no-stop --only=SERVICE_NAME --hard]
.. 
.. Options
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. The following options are available to this command:
.. 
.. ``--hard``
..    Reset all |versive| platform services. Use with caution.
.. 
.. ``--no-stop``
..    Ignore running |versive| platform services and start only those |versive| platform services that are not already running. Otherwise, running |versive| platform services are stopped before all |versive| platform services are started.
.. 
.. ``--only=SERVICE_NAME``
..    Start only the named |versive| platform services instead of all |versive| platform services. This option may be specified multiple times in the same command.
.. 
.. 
.. Example
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. 
.. **Restart unresponsive node in the compute cluster**
.. 
.. If a cluster is unresponsive because a node is temporarily unavailable, log into the unresponsive node, and then restart all services:
.. 
.. .. code-block:: console
.. 
..    $ cr service start --hard
.. 
.. After the node has restarted, from another node in the compute cluster check the cluster status:
.. 
.. .. code-block:: console
.. 
..    $ cr cluster status
.. 
.. The previously-unresponsive node should appear in the list of nodes.
.. 
.. 
.. .. _cmd-cr-service-stop:
.. 
.. cr service stop
.. --------------------------------------------------
.. Stop |versive| platform services on the current node. This prevents the node from being available to the platform cluster and for all tasks.
.. 
.. Usage
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. 
.. .. code-block:: console
.. 
..    $ cr service stop [--only=SERVICE_NAME --keep=SERVICE_NAME --hard]
.. 
.. or:
.. 
.. .. code-block:: console
.. 
..    $ crstop [--only=SERVICE_NAME --keep=SERVICE_NAME --hard]
.. 
.. Options
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. The following options are available to this command:
.. 
.. ``--hard``
..    Stop all |versive| platform services. Use with caution.
.. 
.. ``--keep=SERVICE_NAME``
..    Keep the named |versive| platform services running. This option may be specified multiple times in the same command.
.. 
.. ``--only=SERVICE_NAME``
..    Stop only the named |versive| platform services instead of all |versive| platform services. This option may be specified multiple times in the same command.
.. 
.. 

.. _cmd-cr-version:

cr version
==================================================
Returns the version for the |versive| platform, along with any active applications.

Usage
--------------------------------------------------

.. code-block:: console

   $ cr version

Options
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: console

    $ cr version

will print information similar to:

.. code-block:: none

    Name     Long Name                   Version     Tags
   -------- --------------------------- --------- ------
    crplat   Context Relevant Platform   2.10.0
    risp     Context Relevant RISP       1.6
   -------- --------------------------- --------- ------
