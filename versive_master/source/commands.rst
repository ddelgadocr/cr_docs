.. 
.. versive, primary, security engine
.. 

==================================================
Commands
==================================================

The engine includes a number of commands that can be used to run the entire application workflow or to run specific phases within the application workflow.



.. _command-bloxplain:

bloxplain
==================================================
.. include:: ../../includes_terms/term_blocks.rst

.. TODO: Below is copied also into the release_notes.html.

bloxplain is a command-line tool that can be run to view information about a block (including input and output examples) for any block in the :ref:`default blocks library <blocks-library>` or any custom block that is defined in the customization package.

To view information about a block, run a command similar to:

.. code-block:: console

   $ bloxplain BlockName

For example:

.. code-block:: console

   $ bloxplain AggregateBlock


.. 
.. this topic documents the command located at /crepes/apt/apt/main.py
.. global and command-specific options are located at /crepes/apt/apt/main_cli.py
.. 

.. _command-drive:

drive
==================================================
.. include:: ../../includes_terms/term_command_drive.rst

The :ref:`command-drive` command runs the commands for all phases of the application workflow. By default, this command will not attempt to recreate artifacts that already exist, which reduces processing time. This command uses the :doc:`configuration file <configure>` to automatically determine which data source to use at each phase. Use the :ref:`configure-block-graphs` section in the configuration file to specify all of the data sources used to generate artifacts for each phase, with the following qualifications:

.. TODO: Verify each of these!

* A data source is used when it is configured in a blocks graph and **use_for_lookups** is not set to ``False``, it has a ``dest_ip`` column, and it has either (or both) a hostname or user column. 
* A data source is used when it is configured in a blocks graph, **use_for_modeling** is not set to ``False``, and it has at least one target.
* To configure the specific operations to perform at each phase in the **drive** workflow, use the sections in the configuration described in each of the phases that will be run. To configure options specific to running the phases by invoking the **drive** command via the :ref:`configure-drive` section of the configuration file.
* Use the :ref:`configure-workspaces` section to configure the input and/or output locations.

.. 
.. what about these two, post-blocks?
.. 
.. 
.. * To process data for multiple days, set **window_days** in the :ref:`configure-generate-lookup-per-source` section of the configuration file to one more than the number of previous days to process.
.. 
.. * To reshard data, set the **reshard** property to ``True`` in the :ref:`configure-prepare` section for the corresponding data source in the configuration file. Alternately, configure an escape hatch to write a custom function.
.. 


Usage
--------------------------------------------------

.. code-block:: console

   crpython /opt/cr/crepes/apt/bin/apt.py drive <config.yaml> \
   --output_phase=PHASE --daterange_start=DATE \
   --daterange_end=DATE [groupby_key=KEY --datasource=SOURCE--dry_run \
   --shallow --retries=NUMBER --ignore_errors --force --use_spark \
   --yarn_queue=QUEUE]

.. note:: .. include:: ../../includes/cmd_install_directory.rst

.. note:: Running the **drive** command with the ``--shallow`` and ``--force`` arguments is equivalent to running the the individual command specified in ``--output_phase``. For example, ``drive --output_phase=transform --shallow --force`` is equivalent to ``transform``.

Arguments
--------------------------------------------------
The following arguments are available to this command:

.. TODO: Use the --datasource=NAME_OF_EAT to run APT so it generates the named entity attribute table. Say output_stage=generated and then datasource=NAME to create the NAME EAT.

``--datasource``
   The data sources used for each phase of the workflow are controlled by the configuration, as described above. This value may be an entity attribute table. Optionally, specify a comma-delimited list of data sources here to override the configuration. For example, ``datasource=proxyweb,dns``.

   For phases that can use multiple data sources, this causes the **drive** command to use a subset of eligible data sources. You can also exclude data sources from either of those phases using **use_for_lookup** or **use_for_modeling** in the **sources** section of the configuration file.

   For phases that use one data source at a time, providing a list here causes drive to create artifacts for each of the data sources up to the ``output_phase`` specified on the command line.

``--daterange_end=DATE``
   Relative to **base_path**, the path to the directory that contains the data for the end of the date range; end date is *exclusive*.

   The date must use the format specified in **date_path** in the configuration file. For example, if ``date_path: %Y%m%d/data``, ``--daterange_end='20151102/data'`` is a valid setting.

``--daterange_start=DATE``
   Relative to **base_path**, the path to the directory that contains the data for the start of the date range; start date is *inclusive*.

   The date must use the format specified in **date_path** in the configuration file. For example, if ``date_path: %Y%m%d/data``, ``--daterange_start='20151101/data'`` is a valid setting.

``--dry_run``
   A flag that indicates that the **drive** command will print out, but not execute, the commands it will attempt to run.

``--force``
   A flag that indicates that the **drive** command will recreate the final phase artifacts, even if they already exist. It will not recreate predecessor phase artifacts that already exist.

``--groupby_key=KEY``
   The subset of keys from which a modeling table is generated.

   For the V1_1 version for workspace naming (set by **naming** in the **drive** section of the configuration file), the accepted values are ``day_host`` and ``day_user``.

   For the V2 version for workspace naming, the accepted values are ``host`` and ``user``. 

   .. warning:: By default, both keys are used. There is a known issue in which the ``groupby_key`` is not honoured.

``-k``, ``--ignore_errors``
   A flag that indicates that the **drive** command will continue trying to build artifacts even if a phase fails, given that the required predecessors are present. For example, if proxy data fails to be resharded, the **drive** command will not attempt to parse proxy data because it is missing a required predecessor. However, it will attempt to reshard DNS data to complete as many phases in the workflow as possible.

``--output_phase=PHASE``
   The phase in the engine workflow to process date up through. This value may be the phase's output directory, the name of an entity attribute table, or the name of the command. The following values are accepted:

   * prepared or prepare
   * parsed or parse
   * lookup or generate_lookup_table
   * transformed or transform
   * standardized or standardize_keys
   * agg or aggregate
   * modeling_table or generate_modeling_table
   * models or learn_models
   * scored or predict
   * results or postprocess
   * name of entity attribute table

   See the **workspaces** section of the configuration file for more about configuring workspaces.

``--retries=NUMBER``
   An integer that indicates the number of times to retry each command in the workflow if it fails. Default is ``3``.

``--shallow``
   A flag that indicates that the **drive** command will not attempt to make the predecessor phase artifacts, but execute only the step that produces the final artifacts from their immediate predecessors.

``--spark_app_name=TRUE``
   When ``--use_spark`` is ``True``, an option to specify the name of the app when running in Spark.

``--use_spark``
   A flag that indicates that the engine will use Spark as the underlying compute engine. Default value: ``True``. See ``--spark-app-name``.

``--yarn_queue=QUEUE``
   The YARN queue to which jobs are submitted.



.. 
.. this section documents the command located at /crepes/apt/apt/list.py
.. global and command-specific options are located at /crepes/apt/apt/main_cli.py
.. 

.. _command-list:

list
==================================================
The :ref:`command-list` command lists each of the artifacts that already exist for each engine phase, including ``parsed_error`` files that were output for parsing errors.

Usage
--------------------------------------------------

.. code-block:: console

   crpython /opt/cr/crepes/apt/bin/apt.py list <config.yaml>

.. note:: .. include:: ../../includes/cmd_install_directory.rst








