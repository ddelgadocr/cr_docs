.. 
.. versive, primary, security engine
.. 

==================================================
Defined Behaviors
==================================================

The |vse| discovers outliers in proxy, DNS, and flow data that can help identify the presence of an active adversary campaign, unusual configurations that may enable that activity, and unexpected communication patterns within your network. These outliers are discovered over time by comparing them to known behaviors and patterns defined by the APT kill chain---planning, access, reconnaissance, data collection, and data exfiltration---and behaviors and patterns related to beaconing and malicious domains.

The engine compares all of this data against a list of defined behaviors, from which the results are generated.

All of the data that is analyzed to first establish a baseline data set. The baseline data set is then updated with thresholds that are appropriate for the network being analyzed---known volumes, numbers of neighbors, normal flows, known benign entities, whitelists, and so on---which improves the quality of the data set against which outliers are to be identified.

The goal is to discover obvious outliers that lead to the discovery of adversary activity. The first step is to find the most extreme outliers in the data set. Extreme outliers are then compared to broader sets of data using a lower threshold to see if there are secondary outliers that can help confirm the extreme outlier. All related outliers are compared to the expected behaviors associated with the |stage_recon|, |stage_collect|, and |stage_exfil| campaign stages. An adversary campaign may be identifiable within a single stage, but most of the time an adversary campaign will be identifiable across multiple stages with multiple outliers confirming its presence.


.. _define-behaviors-about:

About Behaviors
==================================================
.. TODO: Done for now.

A behavior represents a use case that is performed by all individuals on a network. All behaviors share some combination of the following aspects:

#. Some combination of activity that includes data collection, contacting other machines, publishing data, and requesting data. This activity may include only machines within the network. It may also include a combination of machines inside and outside the network.
#. Can be detected via some type of analysis of proxy logs, router logs, DNS records, and flow data.
#. Can be measured with units that are common to a network, such as bytes moved from machine A to machine B, ports and protocols that were used for that movement, IP addresses for source and destination machines, and so on.
#. Can be counted in a way that helps separate abnormal behaviors from normal behaviors.

Nearly all of the traffic on a network is legitimate and is performed by individuals who are not adversaries and never will be. Network traffic that is performed by an adversary, while often similar to normal network traffic, will also have different characteristics from those typically associated with normal traffic.


.. _define-behaviors-targets:

About Targets
==================================================
.. TODO: Done for now.

A target defines a use case to be analyzed by |vse|. A target:

* Looks for a specific combination of activity, detection method, and units.
* Is counted in a specific way, such as creating a list for each distinct source IP address on the network or counting the maximum number of machines that used a specific port with a byte threshold greater than some number.
* Is assigned a value that is canonical, and therefore may be compared to all other values that were calculated for all targets.
* Is measureable across all targets for all behaviors for which the engine will analyze data looking for potential adversary activity.

Each target is assigned a measurement by the engine. These measurements are then used to identify outliers in the data that show network traffic that is not considered to be normal. Network traffic that is not normal is then analyzed for anomalies that highlight potential adversary activity.


.. _define-behaviors-activities:

Target Activities
--------------------------------------------------
Activities are performed by users on a network. There are four primary types of activities:

* :ref:`Collecting data <define-behaviors-activity-collected>`
* :ref:`Communication data <define-behaviors-activity-communication>`
* :ref:`Publishing data <define-behaviors-activity-published>`
* :ref:`Requesting data <define-behaviors-activity-requested>`

These activities allow for context with regard to how users interact with machines on (and outside of) the network. Activities are associated with additional details, such as detection methods and units of measurement to help identify activities that fall outside normal use patterns.

.. TODO: SINGLE SOURCE WITH build-cases-client-server-roles

Normal data flow across the internal network is straightforward:

* A client frequently initiates communications to a server
* A client rarely initiates communications to a client
* A server typically responds only to a requests from a client
* A server rarely initiates communications with a client or a server

The direction of normal data flow across the network should almost always be:

#. A client requested information from a server.
#. A server responded to that request with information.
#. That client collected that information.

.. TODO: SINGLE SOURCE WITH build-cases-client-server-roles


.. _define-behaviors-activity-collected:

Collected
++++++++++++++++++++++++++++++++++++++++++++++++++
Normal data collection is easy to detect and graph because all entities within the internal network are easily identifiable as a client or a server, with rare exceptions, and clients are typically collecting data that has been requested from servers.

Collection behaviors that can lead to discovering outliers in the data include:

* A client that behaves like a server
* A server that behaves like a client
* A high number of connections to specific internal systems
* A high number of internal neighbors
* Differences in the normal request/response patterns
* Longer-than-normal connection times
* A server making a connection to outside the network
* An already-interesting cient making a connection to outside the network
* Larger-than-normal volumes of data movement in any direction, both inside and outside of the network


.. _define-behaviors-activity-communication:

Communication
++++++++++++++++++++++++++++++++++++++++++++++++++
Normal data flow across the network is straightforward: a client consumes data, a server produces data. Contact behaviors that can lead to discovering outliers in the data include:

* A client that produces data, as if it were a server
* A server that consumes data, as if it were a client
* A high number of connections, generally
* A high number of internal neighbors
* A server or a client acting as a server producing data to a machine outside the network
* Longer-than-normal connection times


.. _define-behaviors-activity-published:

Published
++++++++++++++++++++++++++++++++++++++++++++++++++
Normal publishing behavior is typically from server to client. A client rarely publishes data except for to a few known locations, such as a cloud storage location that acts as a backup server. Publishing behaviors that can lead to discovering outliers in the data include:

* A client that publishes data, as if it were a server
* Differences in the normal publish patterns
* A server that publishes data outside of the network
* A high frequency of publishing to specific client machines, over time

.. _define-behaviors-activity-requested:

Requested
++++++++++++++++++++++++++++++++++++++++++++++++++
Normal request behavior is typically from client to server. A server rarely requests data. Request behaviors that can lead to discovering outliers in the data include:

* A server that requests data, as if it were a client
* A high number of requests made to internal systems that do not normally have a high number of requests from individual systems
* Differences to the normal request patterns
* Requests made to machines that are outside of the network by a server or by a client acting as a server
* An already-interesting client making a connection to outside the network
* Requests that are followed by larger-than-normal data movement in any direction, both inside and outside of the network

.. _define-behaviors-detection-methods:

Detection Methods
--------------------------------------------------
Detection methods for activities to be analyzed as targets are available from a combination of proxy data, DNS records, flow data, and log file data, including:

* :ref:`A record activity <define-behaviors-detection-method-a-record>` discoverable from DNS records
* :ref:`AAAA record activity <define-behaviors-detection-method-aaaa-record>` discoverable from DNS records
* :ref:`AXFR record activity <define-behaviors-detection-method-axfr-record>` discoverable from DNS records
* :ref:`Byte movement and direction <define-behaviors-detection-method-proxy>` discoverable from proxy data
* :ref:`FTP protocol activity <define-behaviors-detection-method-axfr-record>` discoverable from flow data
* :ref:`ICMP protocol activity <define-behaviors-detection-method-icmp>` discoverable from flow data
* :ref:`Port traffic and direction of data <define-behaviors-detection-method-ports>` is discoverable from flow data
* :ref:`Proxy data <define-behaviors-detection-method-ports>` is discoverable from the various log files associated with machines and devices that handle proxies inside the network and at the points the network interacts with entities that are outside of it
* :ref:`PTR record activity <define-behaviors-detection-method-ptr-record>` discoverable from DNS records
* :ref:`SNMP protocol activity <define-behaviors-detection-method-snmp>` discoverable from flow data
* :ref:`UDP protocol activity <define-behaviors-detection-method-udp>` discoverable from flow data

Detection methods should examine the source, the destination, the method of contact, the frequency of contact, the duration of contact, and the amount of data transferred.

.. _define-behaviors-detection-method-a-record:

A Records
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_dns_record_a.rst

A records are interesting because they are the most common way individuals access sites on the Internet. This, however, can create a lot of noise in the results. For example, Google Chrome does five garbled lookups at startup as a security check. If a suspicious host is a client machine operated by a human being and these garbled lookups appear in a threat case, these suspicious A records are probably benign; however, if the suspicious host is a server (or some other entity) that should not be making requests outside of the network, then these suspicious A records should be investigated.

.. _define-behaviors-detection-method-aaaa-record:

AAAA Records
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_dns_record_aaaa.rst


.. _define-behaviors-detection-method-axfr-record:

AXFR Records
++++++++++++++++++++++++++++++++++++++++++++++++++
An AXFR record is the authoritative zone transfer from a primary to a secondary server on the network. AXFR records are discoverable from DNS data sources. The engine should analyze statistics that count the number of distinct domains that were requested via AXFR records.


.. _define-behaviors-detection-method-ftp:

FTP Protocol
++++++++++++++++++++++++++++++++++++++++++++++++++
The File Transfer Protocol (FTP) is a network protocol used to transfer files and data between machines on a network. FTP data is discoverable from flow data sources. The engine should analyze statistics that count the bytes sent and received via FTP for both internal and external IP addresses.



.. _define-behaviors-detection-method-icmp:

ICMP Protocol
++++++++++++++++++++++++++++++++++++++++++++++++++
The Internet Control Message Protocol (ICMP) is an error-reporting protocol for when IP packets cannot be delivered. It's associated with routers and switches and some network services. ICMP messages do not send data, but instead send information to the source IP address about the availability (or unavailability) of the destination IP address. ICMP data is discoverable from flow data sources. The engine should analyze statistics that count the number of distinct internal destination IP addresses that used ICMP.



.. _define-behaviors-detection-method-ports:

Ports (Detection)
++++++++++++++++++++++++++++++++++++++++++++++++++
The Transmission Control Protocol (TCP) is an Internet protocol that provides host-to-host connectivity at the transport layer. TCP is a commonly-used protocol for file transfers, secure shell (SSH), peer-to-peer file sharing, communication between clients and servers, and so on. TCP is sometimes referred to as TCP/IP because, while TCP handles the communication, IP is how the individual packets are transferred.

Communication on the network happens between ports on clients, servers, and other network devices. As such, ports are useful for discovering the direction of traffic on the network and the protocols that were used for that traffic. TCP protocol data is discoverable from flow data sources and can be highly useful for identifying outlier behavior in the flow data.

Not every port is the same with regard to analyzing flow data for indicators of adversary activity. At a high level types of port-related activity to look for include:

* Identifying the port used for communication
* Separating the ports into high-, medium-, and low-value groupings
* Identifying `well-known ports <http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml>`__ as they relate to services, such as TCP port 21 (used to connect FTP to the Internet), TCP and/or UDP ports 53 (used for DNS lookup traffic), and HTTP port 80 (the most commonly-used port for Internet traffic), TCP port 1080 (used for the socket secure---SOCKS---protocol traffic); some ports, such as SNMP ports 161 (the agent port) and 162 (the manager port), require specific monitoring
* Identifying the destination IP addresses contacted, including internal vs. external, and then associating that IP address with the ports used for contact

The engine should analyze statistics that count the distinct number of IP addresses and/or find the ports that were contacted is necessary. For example:

* A distinct count of destination IP addresses and the ports that were used to contact them
* A distinct count of internal destination IP addresses at which high-, medium-, and low-value ports are located
* A distinct count of destination IP addresses at which high-value ports were accessed from internal IP addresses
* The maximum and median numbers of high-, medium-, and low-value ports that are associated with internal destination IP addresses
* A distinct count of destination IP address/port combinations for all ports
* A distinct count of destination IP address/port combinations for high- and medium-value ports


.. _define-behaviors-detection-method-ptr-record:

PTR Records
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_dns_record_ptr.rst

PTR records are interesting because domain names are easier to identify than IP addresses. For example ``www.google.com`` is a domain name that is familiar to most everyone, but few individuals know its IP address: ``216.58.192.46``. As such, PTR records can indicate activities such as an adversary using the DNS system to identify IP addresses. Or performing a "ping walk", which is to enter IP addresses sequentially as an attempt to identify other hosts on the same network.

.. _define-behaviors-detection-method-snmp:

SNMP Protocol
++++++++++++++++++++++++++++++++++++++++++++++++++
The Simple Network Management Protocol (SNMP) is an Internet protocol for collecting and organizing information about managed devices on networks, such as modems, routers, switches, servers, workstations, and so on. SNMP data is discoverable from flow data sources. The engine should analyze statistics that count the number of distinct internal destination IP addresses that used SNMP.


.. _define-behaviors-detection-method-proxy:

Proxy
++++++++++++++++++++++++++++++++++++++++++++++++++
Proxy data is available for machines and devices on the network, such as routers and firewalls. Proxies allow clients and servers to access and move around the network. Proxy data is discoverable from log files for specific machines and devices and is often different for each network. The engine should analyze log files for all proxy data sources on the network.



.. _define-behaviors-detection-method-udp:

UDP Protocol
++++++++++++++++++++++++++++++++++++++++++++++++++
The User Datagram Protocol (UDP) is an Internet transport protocol for sending minimal messages across a network, also known as datagrams. UDP is used with protocols like Domain Name System (DNS), Remote Procedure Call (RPC), service discovery, and others. UDP data is discoverable from flow data sources. The engine should analyze statistics that count the number of distinct internal and external destination IP addresses that used the UDP.




.. _define-behaviors-units:

Units of Measurement
--------------------------------------------------
Every activity---contacted, collected, published, or received---for all detection methods must be able to assign a unit of measurement. For example, the number of bytes transferred from an internal IP address to an external IP address or the number of distinct ports contacted by a single source IP address across the entire network.

The following units of measurements are calculated by the engine based on the data contained in the various data sources that are analyzed on a daily basis:

.. TODO: Update the friendly strings below after each section is complete.

* :ref:`Byte movement <define-behaviors-unit-bytes>`
* :ref:`Domains contacted <define-behaviors-unit-domains>`
* :ref:`IP addresses, broken down by source and destination <define-behaviors-unit-ip-address>`
* :ref:`Types of ports used for communication <define-behaviors-unit-ports>`
* :ref:`Types of protocols used for communication <define-behaviors-unit-protocols>`

.. * :ref:`Services accessed <define-behaviors-unit-services>`
.. * :ref:`Systems accessed <define-behaviors-unit-system>`

These units of measurement are necessary to discover adversary activity on the network. The actual units that are required will vary, depending on how the network is configured and where it's located. But generally, the points at which adversary activity may be detected, and how all of that activity maps to the :doc:`individual stages of an adversary campaign <stages>` has these units as a common starting point. These units work together with detection methods and activities to tie-together seemingly unrelated events into a series events that can reveal the presence of adversary activity on the network.


.. _define-behaviors-unit-bytes:

Bytes In, Bytes Out
++++++++++++++++++++++++++++++++++++++++++++++++++
Byte movement is an important part of identifying adversary activity. Bytes move in all directions across the network:

* A client makes a request to a server for data. That movement will show as bytes out of the server and bytes into the client.
* A client sends a large amount of data to a machine outside of the network. That movement will show as bytes out of the client with a large number of bytes moved.

Nearly all byte movement on the network is benign. To identify outliers in data, start with identifying common patterns:

* The direction of traffic (bytes in and bytes out)
* The bytes moved in all directions (bytes moved)
* If any byte movement patterns are unusual
* Thresholds that allow for spotting interesting directions and volumes

.. _define-behaviors-unit-domains:

Domains
++++++++++++++++++++++++++++++++++++++++++++++++++
A domain is a name registered in DNS that is used to associate various networking contexts with an address. The most common domains are associated with Internet Protocol (IP) resources, including clients, servers, web sites, and any other service or device that communicates over the Internet. Information about domains is discoverable by analyzing DNS data for source and destination IP addresses. For example, a URL:

``https://docs.versive.com``

is the following components:

* A fully-qualified domain name: ``docs.versive.com``
* A registered domain: ``versive.com``
* A subdomain: ``docs``

Each of these components may be rare or exclusive.

* .. include:: ../../includes_terms/term_domain_rare.rst
* .. include:: ../../includes_terms/term_domain_exclusive_external.rst


.. _define-behaviors-unit-ip-address:

IP Addresses
++++++++++++++++++++++++++++++++++++++++++++++++++
All IP addresses should be identified by source and destination, which can then be correlated with ports and bytes movement. IP-port pairs should be calculated :ref:`using crosstab multi-day values <process-data-multi-day-crosstab>` to identify all ports that are associated with individual source and/or destination IP addresses.


.. _define-behaviors-unit-ports:

Ports (Units)
++++++++++++++++++++++++++++++++++++++++++++++++++
All ports should tracked, along with the protocol used for communication via that port. Port data is discoverable from flow data sources. The following ports are considering interesting by the engine, by default:

.. list-table::
   :widths: 160 160 160 220
   :header-rows: 1

   * - Port
     - Protocol
     - Interest
     - Notes
   * - **53**
     - **TCP/UDP**
     - **High**
     - DNS
   * - **80**
     - **HTTP**
     - **High**
     - 
   * - **111**
     - **TCP/UDP**
     - **High**
     - RPC
   * - **135**
     - **TCP/UDP**
     - **High**
     - 
   * - **137**
     - **TCP/UDP**
     - **High**
     - 
   * - **139**
     - **TCP/UDP**
     - **High**
     - DCE
   * - **156**
     - **TCP/UDP**
     - **High**
     - SQL Server
   * - **177**
     - **TCP/UDP**
     - **High**
     - X Display
   * - **181**
     - **TCP/UDP**
     - **Medium**
     - SNMP
   * - **182**
     - **TCP/UDP**
     - **Medium**
     - SNMP
   * - **389**
     - **TCP/UDP**
     - **High**
     - LDAP
   * - **443**
     - **HTTPS**
     - **High**
     - 
   * - **445**
     - **SMB over IP**
     - **High**
     - MSFT DS
   * - **514**
     - **TCP**
     - **High**
     - Syslog
   * - **< 1024**
     - 
     - **Medium**
     - Not a high-interest port or ports 181 or 182.
   * - **> 1024**
     - 
     - **Low**
     - Not a high- or medium-interest port.
   * - **1080**
     - **SOCKS**
     - **High**
     - System service scanning
   * - **1270**
     - **TCP**
     - **Medium**
     - MSFT
   * - **1433**
     - **TCP**
     - **High**
     - Database
   * - **1434**
     - **UDP**
     - **High**
     - Database
   * - **1583**
     - **TCP**
     - **High**
     - 
   * - **1748**
     - **TCP**
     - **High**
     - Oracle
   * - **1754**
     - **TCP**
     - **High**
     - Oracle
   * - **1978**
     - **UDP**
     - **High**
     - Database
   * - **2041**
     - **UDP**
     - **High**
     - Database
   * - **2049**
     - **UDP/TCP**
     - **High**
     - Data service
   * - **2512**
     - **TCP**
     - **Medium**
     - Citrix
   * - **2513**
     - **TCP**
     - **Medium**
     - Citrix
   * - **3050**
     - **TCP**
     - **High**
     -  
   * - **3260**
     - **TCP**
     - **High**
     - SCSI
   * - **3268**
     - **TCP**
     - **Medium**
     - MSFT
   * - **3269**
     - **TCP**
     - **Medium**
     - MSFT
   * - **3306**
     - **TCP**
     - **High**
     - MySQL
   * - **3351**
     - **TCP**
     - **High**
     - 
   * - **4000**
     - **UDP**
     - **Medium**
     - Terabase
   * - **5432**
     - **TCP**
     - **High**
     - 
   * - **6660 - 6669**
     - **TCP**
     - **Medium**
     - Often used for command and control activities.


.. TODO: In the user interface, we'll report X activity discovered via Data Ports for high-interest ports, Service Ports for medium-interest ports, and General Ports for low-interest ports.





.. _define-behaviors-unit-protocols:

Protocols
++++++++++++++++++++++++++++++++++++++++++++++++++
The following protocols should be analyzed from flow data sources:

.. TODO: See about glossary terms and also for syncing this up with the detection methods sections.

* The File Transfer Protocol (FTP) is a network protocol used to transfer files and data between machines on a network. FTP data is discoverable from flow data sources. The engine should analyze statistics that count the bytes sent and received via FTP for both internal and external IP addresses.
* The Internet Control Message Protocol (ICMP) is an error-reporting protocol for when IP packets cannot be delivered. It's associated with routers and switches and some network services. ICMP messages do not send data, but instead send information to the source IP address about the availability (or unavailability) of the destination IP address. ICMP data is discoverable from flow data sources. The engine should analyze statistics that count the number of distinct internal destination IP addresses that used ICMP.
* The Simple Network Management Protocol (SNMP) is an Internet protocol for collecting and organizing information about managed devices on networks, such as modems, routers, switches, servers, workstations, and so on. SNMP data is discoverable from flow data sources. The engine should analyze statistics that count the number of distinct internal destination IP addresses that used SNMP.
* The Transmission Control Protocol (TCP) is an Internet protocol for sending messages between hosts that are communicating on an IP network. TCP complements the Internet Protocol (IP) and is often referred to as TCP/IP. TCP is one of the primary protocols used for Web traffic, email, remote administration, and file transfers. TCP data is discoverable from flow data sources. The engine should analyze statistics that count the number of distinct internal and external IP addresses that used TCP.
* The User Datagram Protocol (UDP) is an Internet transport protocol for sending minimal messages across a network, also known as datagrams. UDP is used with protocols like Domain Name System (DNS), Remote Procedure Call (RPC), service discovery, and others. UDP data is discoverable from flow data sources. The engine should analyze statistics that count the number of distinct internal and external destination IP addresses that used UDP.


.. 
.. .. _define-behaviors-unit-services:
.. 
.. Services
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. xxxxx
.. 
.. 
.. 
.. 

.. 
.. .. _define-behaviors-unit-system:
.. 
.. Systems
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. xxxxx
.. 
.. 
.. 
.. 


.. _define-behaviors-statistics:

Target Statistics
--------------------------------------------------
The engine calculates statistics for each target, such as creating a list of distinct IP addresses or determining the median number of ports contacted by all source IP addresses for a given day.

.. TODO: Finish these **after** the multi-day sections are done. Reuse the stock descriptions or something.

.. 
.. .. _define-behaviors-statistic-count-distinct:
.. 
.. Count Distinct
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. .. warning:: TBD pending multiday crosstab docs in :doc:`model_data`.
.. 
.. 
.. by column:
.. count-distinct-a
.. count-distinct-b
.. count-distinct-axb
.. 
.. 
.. TODO: Finish these **after** the multi-day sections are done. Reuse the stock descriptions or something.
.. 
.. .. _define-behaviors-statistic-maximum:
.. 
.. Maximum
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. warning:: TBD pending multiday crosstab docs in :doc:`model_data`.

.. 
.. max-marginal-a
.. max-marginal-b
.. 
.. TODO: Finish these **after** the multi-day sections are done. Reuse the stock descriptions or something.
.. 
.. 
.. .. _define-behaviors-statistic-median:
.. 
.. Median
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. warning:: TBD pending multiday crosstab docs in :doc:`model_data`.
.. 
.. 
.. median-marginal-a
.. median-marginal-b
.. 
.. TODO: Finish these **after** the multi-day sections are done. Reuse the stock descriptions or something.


.. _define-behaviors-strength:

Target Strengths
--------------------------------------------------
All targets are assigned a strength: strong or weak:

.. TODO: Sync these up with the configuration file descriptions. Glossary term called "Target Strengths"? Then for strong and weak below it?

* .. include:: ../../includes_config/modeling_relevance_weights_strong.rst
* .. include:: ../../includes_config/modeling_relevance_weights_weak.rst

.. note:: A target may also be assigned a circumstantial strength.

   .. include:: ../../includes_config/modeling_relevance_weights_circumstantial.rst

.. note:: Target strengths are configured using the ``relevance_weights`` settings group in the :ref:`configure-modeling` section of the configuration file



.. _define-behaviors-measurements:

About Measurements
==================================================
.. TODO: Good enough for today. Needs to be better.

.. Probably true: "A target is a specific combination of statistic, unit, activity, and method. Every target that is modeled is assigned a measurement."

The measurement is a value that is applied to the target. This may be done for a single day and it may be done for a multi-day range, depending on the target, the amount of data available, and the type of calculations the engine is asked to perform.


.. _define-behaviors-anomalies:

About Anomalies
==================================================
.. TODO: Good enough for today. Needs to be better.

Anomalies are the results of discovering outliers in the data. All anomalies require some amount of follow-up. From an adversary perspective:

* Many anomalies are discovered to be benign. A benign anomaly is still often useful, as they help identify areas of the network that need hardening, domains that can be added to blacklists and/or whitelists, incorrect server and/or client configurations, and so on.
* Some anomalies are more interesting and when correlated across the entire set of campaign stages, can reveal the presence of behavior that fits the patterns and characteristics of actual adversary behavior. These anomalies must be followed up on to determine if the anomaly is a real threat to the network.




.. _define-behaviors-stages:

Campaign Stages
==================================================
.. include:: ../../includes_terms/term_campaign.rst


.. _define-behaviors-stage-planning:

Planning Stage
--------------------------------------------------
.. include:: ../../includes_terms/term_campaign_stage_planning.rst


.. _define-behaviors-stage-access:

Access Stage
--------------------------------------------------
.. include:: ../../includes_terms/term_campaign_stage_access.rst

There are no targets for |stage_access| stage behaviors. It is, however, possible for the engine to spot activities that can be associated with |stage_access| behaviors. In these cases, those anomalies tend to provide additional support to adversary activity that has already been detected for the |stage_recon|, |stage_collect|, and |stage_exfil| campaign stages.


.. _define-behaviors-stage-recon:

Recon Stage
--------------------------------------------------
.. include:: ../../includes_terms/term_campaign_stage_recon.rst

Reconnaissance behavior can be detected from DNS and flow data:

* .. include:: ../../includes_terms/term_datatype_dns.rst
* .. include:: ../../includes_terms/term_datatype_flow.rst

Recon Targets
++++++++++++++++++++++++++++++++++++++++++++++++++
Targets for the |stage_recon| stage are calculated for data ingested from flow and DNS data sources.

Strong targets are highly relevant for monitoring |stage_recon| stage behaviors. These targets can be observed directly and are often present in a successful campaign:

* A distinct count of destination IP addresses accessed via the ICMP protocol over a 30-day window.
* A distinct count of destination IP addresses accessed via the SNMP protocol over a 30-day window.
* A distinct count of destination IP addresses contacted through high-interest ports over a 30-day window.
* A distinct count of named domains contacted via DNS PTR records over a 30-day window.

Weak targets are useful for monitoring |stage_recon| stage behaviors, especially if more than one weak target is associated with the same anomaly:

* A distinct count of destination IP addresses accessed via the UDP protocol over a 30 day window.
* A distinct count of destination IP addresses contacted through medium-interest ports over a 30-day window.
* A distinct count of destination IP addresses contacted through low-interest ports over a 30-day window.
* A distinct count of named domains contacted via DNS A records over a 30-day window.
* A distinct count of named domains contacted via DNS AAAA records over a 30-day window.


.. _define-behaviors-stage-collect:

Collection Stage
--------------------------------------------------
.. include:: ../../includes_terms/term_campaign_stage_collection.rst

Collection Targets
++++++++++++++++++++++++++++++++++++++++++++++++++
Targets for the |stage_collect| stage are calculated for data ingested from flow data sources.

.. TODO: 2nd bullet is a wild guess.

Strong targets are highly relevant for monitoring |stage_collect| stage behaviors. These targets can be observed directly and are often present in a successful campaign:

* The sum of bytes collected by source IP addresses over a 30-day window.
* The number of internal neighbors per source IP address, as compared to other source IP addresses on the network, for a server acting as a client and/or for a client acting as a server.

Weak targets are useful for monitoring |stage_collect| stage behaviors, especially if more than one weak target is associated with the same anomaly:

* The number of internal neighbors per source IP address, as compared to other source IP addresses on the network.


.. _define-behaviors-stage-exfil:

Exfiltration Stage
--------------------------------------------------
.. include:: ../../includes_terms/term_campaign_stage_exfiltration.rst

Exfiltration Targets
++++++++++++++++++++++++++++++++++++++++++++++++++
Targets for the |stage_exfil| stage are calculated for data ingested from flow and proxy data sources.

Strong targets are highly relevant for monitoring |stage_exfil| stage behaviors. These targets can be observed directly and are often present in a successful campaign:

* The sum of bytes published to exclusive external domains, and to those which have a high publication ratio, over a 30-day window.

Weak targets are useful for monitoring |stage_exfil| stage behaviors, especially if more than one weak target is associated with the same anomaly:

* The sum of bytes published to external domains over a 30-day window.
* The sum of bytes published to external domains, and to those which have a high publication ratio, over a 30-day window.
* The sum of bytes published to exclusive external domains over a 30-day window.

.. 
.. Bytes Published (exfil):
.. 
.. * High Publish
.. * Exclusive Domain
.. * Publish + Domain
.. * Unblocked + Publish
.. * Unblocked + Category
.. * Blocked + High Publish
.. 

.. _define-behaviors-stage-ongoing:

Ongoing Activity
--------------------------------------------------
.. include:: ../../includes_terms/term_campaign_stages_ongoing.rst


.. 
.. .. _define-behaviors-beaconing:
.. 
.. Beaconing
.. ==================================================
.. Beaconing occurs when short, regular communications from a host inside the network are sent to some domain that is located outside of the network. Beaconing by itself is generally not an indicator of an adversary campaign and is often common (and benign) network activity.
.. 
.. That said, an adversary campaign will use beaconing to verify that a host inside the network is alive, is functioning, and is ready to receive instructions. This is sometimes referrred to as "command and control" within the APT kill chain.
.. 
.. For domains that are analyzed for beaconing behaviors, they are run through three stages:
.. 
.. #. Computing raw beaconing statistics for each host/destination pair for multiple days.
.. #. Computing raw beaconing statistics for each host/destination pair for a single day.
.. #. Computing a beaconing score for each host/destination pair for a range of days.
.. 
.. The engine generates beaconing scores based on careful analysis of proxy log data, and then provides a list of hosts on which beaconing behaviors are discovered for the previous 24-48 hours.
.. 
.. Use :ref:`a beaconing whitelist <define-behaviors-beaconing-whitelist>` to prevent the engine from including popular, known, and/or benign domains in the analysis.
.. 
.. TODO: Update the previous paragraph to say "based on careful analysis of proxy and DNS log data" in the future. Currently CSE does not detect beaconing via DNS logs.
.. 
.. 
.. 
.. .. _define-behaviors-beaconing-scores:
.. 
.. Beaconing Scores
.. --------------------------------------------------
.. All beaconing scores are calculated from raw beaconing statistics for each host/destination pair in the data, and then against the multi-day table or the single-day table. The table that is used to calculate the beaconing score is defined in the configuration file.
.. 
.. 
.. .. _define-behaviors-beaconing-scores-single-day:
.. 
.. Single-day Scores
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Beaconing scores may be calculated from the single-day table. Under the defined beaconing data source in the :ref:`configure-entity-attribute-sources` section of the configuration file, set ``generate`` to ``validate_beaconing_single_day_config`` and then under ``function_extra_args`` specify the following settings:
.. 
.. .. include:: ../../includes_config/beaconing_group_interval_seconds.rst
.. 
.. .. include:: ../../includes_config/beaconing_proportion_to_trim.rst
.. 
.. .. include:: ../../includes_config/beaconing_trim_distribution_tails.rst
.. 
.. .. include:: ../../includes_config/beaconing_trim_threshold.rst
.. 
.. For example:
.. 
.. .. code-block:: yaml
.. 
..    entity_attribute_sources:
..      ...
..      <beaconing data source>:
..        function_extra_args:
..          - grouping_interval_seconds: 1
..          - proportion_to_trim: 0.05
..          - trim_distribution_tails: True
..          - trim_threshold: 10
..        generate: 'validate_beaconing_single_day_config'
..      ...
.. 
.. 
.. .. _define-behaviors-beaconing-scores-multi-day:
.. 
.. Multi-day Scores
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Beaconing scores may be calculated from the multi-day table. Under the defined beaconing data source in the :ref:`configure-entity-attribute-sources` section of the configuration file, set ``generate`` to ``validate_beaconing_scores_config`` and then under ``function_extra_args`` specify the following settings: 
.. 
.. .. include:: ../../includes_config/beaconing_max_rank_for_beaconing_statistic.rst
.. 
.. .. include:: ../../includes_config/beaconing_score_udf_fn.rst
.. 
.. .. include:: ../../includes_config/beaconing_whitelist_path.rst
.. 
.. For example:
.. 
.. .. code-block:: yaml
.. 
..    entity_attribute_sources:
..      ...
..      <beaconing data source>:
..        function_extra_args:
..          - max_rank_for_beaconing_statistic: 5000
..          - score_udf_fn: 'function_name'
..          - whitelist_path: 'path/to/whitelist/file'
..        generate: 'validate_beaconing_scores_config'
..      ...
.. 
.. 
.. .. _define-behaviors-beaconing-whitelist:
.. 
.. Beaconing Whitelist
.. --------------------------------------------------
.. When :ref:`multi-day beaconing scores are calculated <define-behaviors-beaconing-scores-multi-day>`, a beaconing whitelist may be used to prevent the engine from including popular, known, and/or benign domains in its calculations.
.. 
.. The use of a beaconing whitelist typically reduces the number of false-positives and increases the likelihood that a beaconing score generated by the engine is indicative of adverse beaconing behavior.
.. 
.. A beaconing whitelist is a text file located in the file system on the storage cluster. The location of the beaconing whitelist is defined by the ``whitelist_path`` setting, which is defined as part of the list of ``function_extra_args`` in the :ref:`configure-entity-attribute-sources` section of the configuration file.
.. 
.. For example, the configuration necessary to apply the contents of that beaconing whitelist file to the beaconing data. For example:
.. 
.. .. code-block:: yaml
.. 
..    beaconing_multi_day_scores_table:
..      generate: create_beaconing_multi_day_scores_table
..      udf_source: LIBRARY
..      key_columns: ['hostname', 'url_host']
..      type: PRE_MODELING_TABLE_ATTRIBUTE
..      attribute_columns: []
..      path_add_date_suffix: True
..      depends_on: [{'datasource': 'beaconing_multi_day_raw_features_table',
..                    'phase': 'generated'}]
..      function_extra_args:  {'max_rank_for_beaconing_statistic': 10,
..                             'whitelist_path':'/myfolder/whitelist.csv'}
.. 
.. and then the contents of the beaconing whitelist file is similar to:
.. 
.. .. code-block:: text
.. 
..    *.google.com
..    www.berkeley.???
..    www.somewebsite.*
.. 
.. .. note:: The beaconing whitelist file does not require ``http://`` or ``https://`` to be listed as part of the domain, as these are removed by default.
.. 





.. 
.. .. _define-behaviors-malicious-domains:
.. 
.. Malicious Domains
.. ==================================================
.. A malicious domain is a website, URL, or an IP address that is known to be associated with malware, malicious activity, or is otherwise potentially harmful. Malicious domains are typically blocked so that users within the organization may not access them.
.. 
.. The engine predicts malicious domains by
.. 
.. * Ingesting proxy data that includes the current list of blocked domains
.. * Models the proxy data and, for each unique domain in the data set, generates a score that predicts how malicious a domain might be
.. * This list is then filtered to remove all of the known websites, URLs ,and IP addresses that are already in the blocked list, and then filters any websites, URLs, and IP addresses that are defined in the whitelist and/or top URLs files
.. * For the list of remaining domains, the engine generates assigns each domain a score, and then generates a list of the top 100 domains that have the greatest likelihood of being a malicious domain
.. 
.. The list of malicious domains generated by the engine should be reviewed on a daily basis, with updates made to the organization's list blocked domains (at the proxy), and then updates made to the whitelist and/or top URLs file as necessary. This will prevent newly-identified malicious domains from appearing in subsequent prediction lists.
.. 
.. .. _define-behaviors-malicious-domain-predictions:
.. 
.. Malicious Domain Scores
.. --------------------------------------------------
.. All malicious domain scores are calculated from aggregated proxy log statistics for each external domain that is visible in the data, and then against two lookback windows: the first generates the modeling tables and the second generates the score against a smaller set of modeled data.
.. 
.. Malicious domain predictions relies on three defined configuration groups:
.. 
.. * :ref:`Aggregate proxy data <define-behaviors-malicious-domains-aggregate>`
.. * :ref:`Generate multi-day tables <define-behaviors-malicious-domains-multi-day>`
.. * :ref:`Predict malicious domains <define-behaviors-malicious-domains-predict>`
.. 
.. 
.. .. _define-behaviors-malicious-domains-aggregate:
.. 
.. Aggregate Proxy Data
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Malicious domain scores are aggregated from proxy log statistics for a single day.
.. 
.. Under the defined malicious domains data source in the :ref:`configure-entity-attribute-sources` section of the configuration file, set ``generate`` to ``create_malicious_domains_aggregate_table`` and then under ``function_extra_args`` specify the following settings:
.. 
.. .. include:: ../../includes_config/malicious_domains_top_url_list.rst
.. 
.. 
.. For example:
.. 
.. .. code-block:: yaml
.. 
..    entity_attribute_sources:
..      ...
..      malicious_domains_aggregate:
..        attribute_columns:
..          - url_regdomain
..          - url_category
..          - blocked
..        depends_on: [{'datasource': 'proxyweb', 'phase': 'transformed'}]
..        function_extra_args:
..          top_url_list: /path/to/top/domains/list
..        generate: create_malicious_domains_aggregate_table
..        key_columns: ["hostname", "url_host"]
..        path_add_date_suffix: True
..        type: PRE_MODELING_TABLE_ATTRIBUTE
..        udf_source: LIBRARY
.. 
.. 
.. .. _define-behaviors-malicious-domains-multi-day:
.. 
.. Multi-day Table
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Aggregated malicious domain data is then generated into a modeling table with a defined lookback window.
.. 
.. Under the defined malicious domains data source in the :ref:`configure-entity-attribute-sources` section of the configuration file, specify the length of the lookback window in the model name by replacing ``DAYS`` with the number of days, and then set ``generate`` to ``create_malicious_domains_multi_day_model``:
.. 
.. For example:
.. 
.. .. code-block:: yaml
.. 
..    entity_attribute_sources:
..      ...
..      malicious_domains_DAYS_generate_model:
..        attribute_columns:
..          - url_regdomain
..          - url_regdomain_count_distinct
..          - url_category
..          - blocked
..        depends_on: [{'datasource': 'malicious_domains_aggregate',
..                      'lookback': DAYS,
..                      'phase': 'generated'}]
..        generate: create_malicious_domains_multi_day_model
..        key_columns: ['hostname', 'url_host']
..        path_add_date_suffix: True
..        type: MODELING_TABLE_ATTRIBUTE
..        udf_source: LIBRARY
.. 
.. .. warning:: ``DAYS`` appears twice in the previous example, once for the name of the generated modeling table, and then once for the lookback for the aggregated proxy data.
.. 
.. 
.. 
.. .. _define-behaviors-malicious-domains-predict:
.. 
.. Predict Malicious Domains
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Malicious domain predictions are done against proxy data that has been generated into modeling tables. Predictions are made against the modeled data for the defined number of days in the lookback window and filtering out any whitelisted domains.
.. 
.. Under the defined malicious domains data source in the :ref:`configure-entity-attribute-sources` section of the configuration file, specify the length of the lookback window in the model name by replacing ``DAYS`` with the number of days, set ``generate`` to ``create_malicious_domains_model``, and then under ``function_extra_args`` specify the following settings:
.. 
.. .. include:: ../../includes_config/malicious_domains_number_of_folds.rst
.. 
.. .. include:: ../../includes_config/malicious_domains_output_path.rst
.. 
.. .. include:: ../../includes_config/malicious_domains_whitelist_location.rst
.. 
.. For example:
.. 
.. .. code-block:: yaml
.. 
..    entity_attribute_sources:
..      ...
..      malicious_domains_DAYS_modeling:
..        attribute_columns:
..           - url_regdomain
..           - url_regdomain_count_distinct
..           - url_category
..           - blocked
..        depends_on: [{'datasource': 'malicious_domains_DAYS_generate_model',
..                      'phase': 'generated'}]
..        function_extra_args:
..          number_of_folds: 5
..          output_path: /path/to/results
..          whitelist-location: /path/to/whitelist
..        generate: create_malicious_domains_model
..        key_columns: ["hostname", "url_host"]
..        path_add_date_suffix: True
..        type: MODELING_TABLE_ATTRIBUTE
..        udf_source: LIBRARY
.. 
.. .. warning:: ``DAYS`` appears twice in the previous example, once as part of the name of the modeling table and once for the generated table on which this table depends.
.. 
.. .. _define-behaviors-malicious-domains-whitelists-and-top-urls:
.. 
.. Whitelists and Top URLs
.. --------------------------------------------------
.. When malicious domains predictions are calculated, a whitelist and/or a list of top URLs list may be used to prevent the engine from including popular, known, and/or benign domains in the results.
.. 
.. The use of a malicious domains whitelist and/or top URLs list reduces the number of false-positives and increases the likelighood that a malicious domains prediction generated by the engine is indicative of adverse malicious domains behavior.
.. 
.. A malicious domains whitelist and/or top URLs list is a text file located in the file system on the storage cluster:
.. 
.. * The location of the malicious domains whitelist is defined by the ``whitelist-path`` setting, which is defined as part of the list of ``function_extra_args`` in the :ref:`configure-entity-attribute-sources` section of the configuration file for the data source that :ref:`calculates the malicious domains score <define-behaviors-malicious-domains-predict>`.
.. * The location of the malicious domains top URLs list is defined by the ``top_url_list`` setting, which is defined as part of the list of ``function_extra_args`` in the :ref:`configure-entity-attribute-sources` section of the configuration file for the data source that :ref:`aggregates proxy log data <define-behaviors-malicious-domains-aggregate>`.
.. 
.. .. note:: The malicious domains whitelist and top URLs list may be identical. Set the value for both settings to be the path to the same file.
.. 
.. For example, the configuration necessary to apply the contents of that malicious domains whitelist and/or top URLs list file to the malicious domains data. For example:
.. 
.. .. code-block:: yaml
.. 
..    entity_attribute_sources:
..      ...
..      malicious_domains_aggregate:
..        generate: create_malicious_domains_aggregate_table
..        ...
..        function_extra_args:
..          top_url_list: /path/to/top/domains/list
..      ...
..      malicious_domains_DAYS_generate_model:
..        ...
..      ...
..      malicious_domains_DAYS_modeling:
..        ...
..        function_extra_args:
..          whitelist-location: /path/to/whitelist
.. 
.. There is no support for wildcards. Only the domain and top-level domain extension is required. The contents of the malicious domains whitelist and/or top URLs list file is similar to:
.. 
.. .. code-block:: none
.. 
..    google.co.uk
..    yahoo.com
.. 
.. .. note:: The malicious domains whitelist and/or top URLs list file does not require ``http://`` or ``https://`` to be listed as part of the domain, as these are removed by default.
.. 
