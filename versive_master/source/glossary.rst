.. 
.. versive, primary, security engine
.. 

==================================================
Glossary Terms: Security Engine
==================================================

The following glossary terms apply to the |vse|:

.. glossary::

   **A record**
      .. include:: ../../includes_terms/term_dns_record_a.rst

   **AAAA record**
      .. include:: ../../includes_terms/term_dns_record_aaaa.rst

   **access stage**
      .. include:: ../../includes_terms/term_campaign_stage_access.rst

   **anomaly**
      .. include:: ../../includes_terms/term_anomaly.rst

   **anomaly details**
      .. include:: ../../includes_terms/term_anomaly_details.rst

   **anomaly list**
      .. include:: ../../includes_terms/term_anomaly_list.rst

   **anomaly strength**
      .. include:: ../../includes_terms/term_anomaly_strength.rst

   **application phase**
      Each of the data processing phases that make up the engine workflow that raw data must go through before producing model predictions with anomaly details. The artifacts output by a preceding phase are saved to the storage cluster and serve as the inputs for the following phase in the workflow.

   **application workflow**
      .. include:: ../../includes_terms/term_workflow_security_engine.rst

   **base path**
      The parent directory for all engine artifacts. Set in the configuration file.

   **baseline model**
      .. include:: ../../includes_terms/term_model_type_baseline.rst

   **behavior score**
      .. include:: ../../includes_terms/term_score_behavior.rst

   **block graph**
      .. include:: ../../includes_terms/term_block_graph.rst

   **blocks**
      .. include:: ../../includes_terms/term_blocks.rst

   **campaign stages**
      .. include:: ../../includes_terms/term_campaign.rst

   **canonical form**
      .. include:: ../../includes_terms/term_table_datatable_canonical_form.rst

   **client-server schizophrenia**
      .. include:: ../../includes_terms/term_behavior_client_server_schizophrenia.rst

   **collection**, **collection stage**
      .. include:: ../../includes_terms/term_campaign_stage_collection.rst

   **combo model**
      .. include:: ../../includes_terms/term_model_type_combo.rst

   **compute cluster**
      .. include:: ../../includes_terms/term_cluster_compute.rst

   **consumption ratio**
      .. include:: ../../includes_terms/term_ratio_consumption.rst

   **context**
      A list of columns in the modeling table to use as inputs for * all* models.

   **cumulative distribution**
      .. include:: ../../includes_terms/term_cumulative_distribution.rst

   **dataframe**
      .. include:: ../../includes_terms/term_table_dataframe.rst

   **data source** (or **data source type**)
      A source of raw data to be processed by the |vse|: DNS, flow, and proxy. Ideally, each data source type is able to provide raw data to the engine from multiple sources of raw data. For example, proxy data can be provided to the engine from both BlueCoat and Cisco proxy log files.

   **DataFrame**
      .. include:: ../../includes_terms/term_table_dataframe.rst

   **DataTable**
      .. include:: ../../includes_terms/term_table_datatable.rst

   **data set**
      A specific set of data to be ingested. For example, the table containing Blue Coat proxy data for a particular date range.

   **DNS data**
      .. include:: ../../includes_terms/term_datatype_dns.rst

   **drive command**
      .. include:: ../../includes_terms/term_command_drive.rst

   **enrichments**
      .. include:: ../../includes_terms/term_model_enrichment.rst

   **entity**
      The user or host for which behavior is being modeled.

   **entity attribute table**
      .. include:: ../../includes_terms/term_entity_attribute_table.rst

   **escape hatch**
      Properties at particular points in the configuration file where a user-defined function can be specified to run custom code before, during, or after certain application phases to extend the functionality of the engine.

   **exclusive external domain**
      .. include:: ../../includes_terms/term_domain_exclusive_external.rst

   **exfiltration**, **exfiltration stage**
      .. include:: ../../includes_terms/term_campaign_stage_exfiltration.rst

   **expected measurement**
      .. include:: ../../includes_terms/term_model_expected_measurement.rst

   **explainer**
      The functionality in the engine that creates the threat cases.

   **finding**
      .. include:: ../../includes_terms/term_finding.rst

   **finding score**
      .. include:: ../../includes_terms/term_score_finding.rst

   **flighting**
      .. include:: ../../includes_terms/term_flighting.rst

   **flow data**
      .. include:: ../../includes_terms/term_datatype_flow.rst

   **generated entity attribute table**
      .. include:: ../../includes_terms/term_entity_attribute_table.rst

   **kill chain**
      .. include:: ../../includes_terms/term_campaign_kill_chain.rst

   **LocalDataTable**
      .. include:: ../../includes_terms/term_table_localdata.rst

   **lookup table**
      .. include:: ../../includes_terms/term_table_lookup.rst

   **mean absolute log-quotient (MALQ)**
      .. include:: ../../includes_terms/term_metric_malq.rst

   **mean absolute percentage error (MAPE)**
      .. include:: ../../includes_terms/term_metric_mape.rst

   **measurement model**
      .. include:: ../../includes_terms/term_model_type_measurement.rst

      .. include:: ../../includes_terms/term_model_type_measurement_nested.rst

   **model**
      .. include:: ../../includes_terms/term_model.rst

   **modeling table**
      .. include:: ../../includes_terms/term_table_modeling.rst

   **ongoing campaign activity**
      .. include:: ../../includes_terms/term_campaign_stages_ongoing.rst

   **planning stage**
      .. include:: ../../includes_terms/term_campaign_stage_planning.rst

   **proxy data**
      .. include:: ../../includes_terms/term_datatype_proxy.rst

   **PTR record**
      .. include:: ../../includes_terms/term_dns_record_ptr.rst

   **publication ratio**
      .. include:: ../../includes_terms/term_ratio_publication.rst

   **rare domain**
      .. include:: ../../includes_terms/term_domain_rare.rst

   **reconnaissance**, **recon stage**
      .. include:: ../../includes_terms/term_campaign_stage_recon.rst

   **risk score**
      .. include:: ../../includes_terms/term_score_risk.rst

   **root mean squared error (RMSE)**
      .. include:: ../../includes_terms/term_metric_rmse.rst

   **sampling**
      .. include:: ../../includes_terms/term_model_sampling.rst

   **source-destination pair**
      .. include:: ../../includes_terms/term_source_destination_pair.rst

   **stage score**
      .. include:: ../../includes_terms/term_campaign_scores_stage.rst

   **standardization**
      .. include:: ../../includes_terms/term_standardization.rst

   **static entity attribute table**
      .. include:: ../../includes_terms/term_entity_attribute_table.rst

   **storage cluster**
      .. include:: ../../includes_terms/term_cluster_storage.rst

   **surprise score**
      .. include:: ../../includes_terms/term_score_surprise.rst

   **target**
      The property that the model will predict.

   **threat case**
      .. include:: ../../includes_terms/term_threat_case.rst

   **threat case level**
       .. include:: ../../includes_terms/term_threat_case_level.rst

   **threat case list**
       .. include:: ../../includes_terms/term_threat_case_list.rst     

   **threat case scores**
      .. include:: ../../includes_terms/term_score_threat_case.rst

   **top K threat cases**
      .. include:: ../../includes_terms/term_threat_case_top_k.rst

   **validation**
      By default, tests run after the **parse** phase to ensure that the DataTable matches the canonical form for the source type. Additional custom validation can be added at other phases.

   **Versive Security Engine**
      .. include:: ../../includes_terms/term_vse.rst
