.. 
.. versive, primary, security engine
.. 

==================================================
Engine Workflow
==================================================

The following diagram shows the end-to-end engine workflow, which consists of a series of phases that must be executed in order. This workflow starts with raw data, processes it through each of the phases, using the output of one phase as the input to the next, and then produces the threat case list.

.. image:: ../../images/workflow_overview.svg
   :width: 600px
   :alt: Data flow diagram for Versive Security Engine

The engine is invoked from the command line using the :ref:`workflow-drive`, which relies on a configuration file and a customization package. The engine workflow is broken down into four primary phases:

#. :doc:`Processing data <process_data>`
#. :doc:`Describing behaviors <define_behaviors>`
#. :doc:`Modeling data <model_data>`
#. :doc:`Building threat cases <build_cases>`

.. _workflow-run-security-engine:

Run the Security Engine
==================================================
.. TODO: This section should be about using the DRIVE command only. Integrate the phase-specific sections up there ^^^?

The engine is run via the command line, typically by running the :ref:`workflow-drive` command, which runs all of the individual phases sequentially. The phases that are part of the core engine workflow may also be run individually, primarily for troubleshooting purposes.

.. note:: The following sections provide a short overview of the commands that may be run. Each of the phase-specific sections link out to a topic that discusses :doc:`each of these commands </commands>` in more detail, including arguments, examples, and more.



.. _workflow-drive:

drive
--------------------------------------------------
Generally, the engine is run using the :ref:`command-drive` command, which runs all of the individual phases as if they were a single command. With the **drive** command, an output phase is specified and the engine is run through all of the phases (in order) up to (and including) the named output phase. To improve processing time on subsequent runs the **drive** command does not recreate phase-specific artifacts that already exist.

For example, if the **drive** command is set to run up to the **aggregate** phase (``--output_phase=aggregate``), it will attempt to produce artifacts for each of the engine phases that process prior to the **aggregate** phase--- **prepare**, **parse**, **generate_lookup_table**, **transform**, **standardize_keys**---and then will attempt to produce artifacts for the **aggregate** phase.

Use the :ref:`command-list` command to view all of the artifacts that already exist.


