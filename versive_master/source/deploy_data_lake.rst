.. 
.. versive, primary, security engine
.. 

==================================================
Deploy Data Lake
==================================================

A data lake is a storage repository designed to hold massive amounts of raw data and is configured to collect DNS, flow, and proxy log file data from as many network appliances, host machines, and applications as possible. The engine is configured to ingest this data from the data lake on a daily basis, and then analyze this data for the presence of an active adversary campaign.

.. image:: ../../images/arch_data_lake.svg
   :width: 600 px
   :align: center

This topic describes how to set up a data lake using open source applications and steps that can be completed from a Web interface:

#. Provision the virtual machines or hardware on top of which the data lake will run.
#. Set up Apache Hadoop services with either Cloudera or Hortonworks.
#. Connect network logs to the data pipeline.
#. Configure the engine to pull data from the data lake.


.. _deploy-data-lake-provision-machines:

Provision the Machines
==================================================
A data lake can be provisioned using commodity hardware provisioned as virtual machines, cloud machines, or bare metal, with the following recommendations for the engine:

* RHEL 6.5 (or higher)
* 64-bit Intel x86-compatible computer
* A number of nodes that are sufficient for the storage size of the data lake; this number may be increased or decreased later, as necessary


.. _deploy-data-lake-setup-hadoop-services:

Set up Hadoop Services
==================================================
Apache Hadoop services are required on the provisioned machines using one of the following management applications:

* Cloudera Manager available in Cloudera Express
* Apache Ambari available in Hortonworks Data Platform

These tools have a Web interace that allows a user to quickly add and monitor the services necessary to set up and configure a data lake for use with the engine. From either Web interface, enable the following services:

* Hadoop Distributed File System (HDFS) services for data storage
* Apache Kafka services for data pipelining
* YARN services for resource management
* Apache Spark services for analytics
* Apache Hive services for data summarization, query, and analysis
* Hadoop User Expreience (Hue) services to enable the Web user interface 
* Apache ZooKeeper for Hadoop service management

If using Amazon S3 for data storage, enable:

* AWS Direct Connect for data pipelining

If using Cloudera Express:

* StreamSets for data pipelining; available from Cloudera as an add-on package


.. _deploy-data-lake-connect-logs-to-pipeline:

Connect Logs to the Pipeline
==================================================
After Apache Hadoop services are enabled for the data lake, connect each raw data source to the pipeline. If using Cloudera, `use StreamSets connectors <https://streamsets.com/connectors/>`__ to connect each raw data source to the data pipeline. Netflow collectors can typically write to syslog or log files, so you can pipe the rich data source in StreamSets to the data lake. Many network appliance vendors offer direct support for publishing to Kafka, such as raw Blue Coat proxy logs. StreamSets has connectors for UDP (syslog, netflow, etc.), log file tailing, watching directories, listening to HTTP traffic (client and server), and so on. Netflow and proxy collectors typically write to syslog or log files. Applications write to variety of log formats. 


.. _deploy-data-lake-connect-data-to-application:

Connect Data to Application
==================================================
After the |versive| platform and the engine have been deployed, configure the engine to :doc:`pull data from the data lake </deploy_connect_data_sources>`.

