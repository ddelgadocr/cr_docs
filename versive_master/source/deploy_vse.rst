.. 
.. versive, primary, security engine
.. 

==================================================
Deploy the |vse|
==================================================

.. include:: ../../includes_terms/term_vse.rst

The |vse| comprises the following:

#. A :doc:`data lake <deploy_data_lake>` to store raw data collected from various network data sources and from which the |vse| pulls data on a daily basis.
#. The |vse| itself for data processing, data modeling. Analysis of this data leads to discovery of anomalies and the generation of threat cases. A machine learning API---the |versive| platform---runs underneath the |vse|.
#. The threat viewer is a Web user interface that displays the top-ranked threat cases for human analysis.


.. _deploy-vse-requirements:

Requirements
==================================================
The following sections detail the hardware (both per-node and per-cluster), software, and network requirements for the |versive| platform.

.. 
.. Hardware
.. --------------------------------------------------
.. Physical hardware for the compute cluster is preferred, but virtual machines (VMs) are fine for the initial configuration. Use one of the following hardware setups for each node in the compute cluster:
.. 
.. * 64-bit Intel x86-compatible computer
.. * 64-bit virtual machine running under any hypervisor (such as Windows HyperV or VMWare) that can run the supported operating systems
.. 
.. 
.. Per-cluster
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Across the entire compute cluster, the total resource requirements depend on the total size of the data. 
.. 
.. * Actions that look for patterns over long periods of time and/or actions that retrain a model very quickly (in seconds or milliseconds instead of minutes) often require additional computing resources.
.. * Actions that look for patterns over short periods of time or retraining a model slowly (in minutes instead of seconds or milliseconds) require less computing resources.
.. 
.. For large data sets (around 200 GB):
.. 
.. * An initial cluster of 100-200 compute cores
.. * 8-core CPU, 8-16 GB RAM per compute core
.. * HDFS 50-100 TB, depending on the sie of log data; adjust this number downward depending on the desired data rention policy
.. 
.. Per-node
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Each node in the compute cluster, whether running as physical computers or as virtual machines, must have the following minimum resources per node in the compute cluster:
.. 
.. * A 4-core CPU
.. * 8 GB RAM per core or enough RAM to process a single day of data, whichever is greater
.. * 100 GB of free hard disk space or twice the disk space required for data ingestion and at least enough disk space for intermediate checkpoints (relative to the size of the raw data), whichever is greater
.. 
.. The |versive| platform is a scale-out, in-memory solution. The automation used for model building requires a large number of CPU cycles. Therefore, more cores is preferred and additional RAM is always better, even if the CPU core count isn't increased.
.. 

Software
--------------------------------------------------
The |versive| platform requires one of the following operating systems to be running on all nodes in the compute cluster:

* Red Hat Enterprise Linux (RHEL) 6.9 (or higher)
* CentOS 6.9 (or higher)

The following applications and libraries are required, but are typically included in the operating system distribution or by Amazon EMR for Amazon AWS deployents:

* Apache Spark 1.6.1, 1.6.2, or 1.6.3
* Hadoop 2.6 (or higher)
* YARN scheduler; set up YARN on all compute nodes in the cluster with `the Fair Scheduler <https://hadoop.apache.org/docs/r2.4.1/hadoop-yarn/hadoop-yarn-site/FairScheduler.html>`__.
* JVM version 1.7.0_67

All other third-party packages required by the compute cluster are included with the installer for the |versive| platform.


Data Source
--------------------------------------------------
Data types must be provided to the engine from date-partitioned directories located within an :ref:`HDFS <deploy-connect-data-sources-hdfs-keytab-file>` or :ref:`Amazon S3 <deploy-connect-data-sources-amazon-s3>` storage cluster.

.. list-table::
   :widths: 100 200 300
   :header-rows: 1

   * - Data Type
     - Possible Data Sources
     - Required?
   * - Flow
     - Raw flow data, Gigamon
     - Required

       .. note:: Raw :ref:`flow data sources require nfdump tools <deploy-vse-flow-data-sources>` to be installed. Customized scripts are then used to process this data into the canonical format, after which this data is uploaded to the |vse|. Ask your |versive| representative about using these customized scripts for use with flow data in your environment.
   * - Proxy
     - BlueCoat, ZScalar, Cato, ScanSafe
     - Optional
   * - DNS
     - 
     - Optional


.. _deploy-vse-amazon-vpc:

Amazon VPC
==================================================
The |vse| may be deployed to an `Amazon Virtual Private Cloud (VPC) <https://aws.amazon.com/vpc/>`__ that runs an `Amazon Elastic Map Reduce (EMR) <https://aws.amazon.com/emr/>`__ instance. This approach provides a managed Hadoop framework that runs Apache Spark and is capable of processing massive amounts of data across scalable Amazon EC2 instances. Flow, proxy, and DNS data that is to be analyzed by the |vse| may be staged for processing using HDFS or Amazon S3 storage.

**Setup Amazon AWS services**

#. Set up Amazon VPC. This VPC must `support CIDR, network subnets, and routing <https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Subnets.html>`__ for the VPC gateway.
#. For flow data sources, nfdump tools are used to collect, process, and then upload data to the |vse|. This may be uploaded to the HDFS storage in the Amazon EMR instance or it may be uploaded to an Amazon S3 bucket. An Amazon S3 bucket used for storing flow data requires `Amazon Identity and Access Management (IAM) <https://aws.amazon.com/iam/>`__ with the permissions configured to allow the VPC to access the S3 bucket.
#. The EMR instance will contain `Amazon Elastic Compute Cloud (EC2) <https://aws.amazon.com/ec2/>`__, which require an `Amazon EC2 key pairs for SSH credentials <https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-plan-access-ssh.html>`__. The EC2 instances run the HDFS storage that is used by the |vse|.


Amazon AWS Parameters
--------------------------------------------------
The following paramaters provide all of the information to the |vse| that is necessary for the VPC, the S3 bucket (when used), and all EC2 instances running under the EMR instance:

**create_data_bucket**
   Specifies that the |vse| will use an S3 bucket for storage. Default value: ``True``.

**create_emr_cluster**
   Specifies that the |vse| is run in an Amazon EMR cluster. Default value: ``True``.

**create_key_pair**
   Specifies if an EC2 key pair is created automatically. Default value: ``True``.

**data_bucket_name**
   When ``create_data_bucket`` is ``True``, the name of the S3 data bucket.

**emr_master_type**
   The instance for the primary node in the compute cluster. Default value: ``m3.xlarge``.

**emr_worker_count**
   The number of workers, per replica node in the compute cluster. Default value: ``2``.

**emr_worker_type**
   The instance for all replica nodes in the compute cluster. Default value: ``m3.xlarge``.

**extra_tags**
   The identifier for this instance of the |vse|.

**long name**
   The long name of the VPC. The names of public and private instances use this long name, appended with ``-public`` or ``-private``.

**name**
   The name of the VPC.

**net_index**
   The /25 subdivision of the CIDR block. This setting determines CIDR block behaviors, as well as subnets for public and private CIDR blocks. This must be set to ``172.26.0.0/16``.

**region**
   The Amazon AWS region. This is used by the VPC. Default value: ``us-west-2``.

**repo_bucket_name**
   The name of the storage used by the |vse|. Default value: ``vsecloud-software``.

**working_bucket_name**
   When ``create_data_bucket`` is ``True``, the name of the S3 working bucket.





.. _deploy-vse-preparation:

Preparation
==================================================
Prior to installing the |vse| platform, gather the following information about the nodes in the compute cluster:

#. Identify the node that will be the primary node, and then make note of its IP address.
#. Choose a name for the compute cluster.
#. Identify the user account that will install, administer, and build models with the platform. This account must be able to connect to all nodes using SSH without requiring a password, which may require root access.
#. The installation directory is ``$VSE_DIR/vse``. The account that owns the installation must have write access to this directory for all nodes in the cluster. When the self-installer is finished, all of the required directories for the installation are located under ``$VSE_DIR/vse``.
#. The data directory is ``$VSE_DIR/vse/vse``.
#. Start YARN services:

   On the primary node in the cluster---the resource manager---the following services must be started: ``hadoop-yarn-resourcemanager`` and ``hadoop-yarn-proxyserver``.

   On all replica nodes in the cluster---the node managers---the following services must be started: ``hadoop-yarn-nodemanager``.



.. _deploy-vse-install:

Install the VSE
==================================================
Use the following steps to install the |vse|:

#. Get the self-installer for the |vse| from your |versive| representative. This installer will install the |vse|, the |versive| platform, the Web user interface, and all dependencies.
#. Add the self-installer to a directory on the primary node in the cluster. The installer for the |vse| may be run from any directory on the primary node. This directory is referred to as ``$VSE_DIR/vse``.
#. Run ``$VSE_DIR/vse/install/setup.sh`` to install the |vse|. This will install the platform, the engine, the Web user interface, and will ensure that Hadoop, Spark, and Python are configured correctly.
#. Run ``$VSE_DIR/vse/tools/self-test.sh`` to verify the installation. This will confirm that the installation process was completed and that the |vse| is ready.

When the installation process is complete, the following directory structure is created:

::

   $VSE_DIR/vse
   ├── vse-<version>
   │   ├── conf
   │   │   └── spark
   │   ├── vse
   │   ├── info
   │   ├── install
   │   ├── tools
   │   └── ui


.. 
.. section for "optional" configuration steps
.. should be written to link to other pages, and not be more than a list item on this page
.. 
.. steps like
.. 
.. LDAP
.. Spark tuning
.. 


.. _deploy-vse-connect-to-data-sources:

Connect to Data Sources
==================================================
After the |versive| platform is installed and the engine is installed on top of it, :doc:`connect to data sources </deploy_connect_data_sources>`. Data may be stored in a Hadoop Distributed File System (HDFS), Amazon S3, or any other file system with a unified view.

.. _deploy-vse-flow-data-sources:

Flow Data Sources
--------------------------------------------------
For environments with flow data as a data source, install `nfdump tools <https://github.com/phaag/nfdump>`_, and then work with your |versive| representative to add the following scripts to your environment: :ref:`installer-daily-backup-files`, :ref:`installer-daily-nfcapd`, and :ref:`installer-daily-nfdump-running-list`. Flow data may be :ref:`uploaded to an Amazon S3 bucket <deploy-vse-amazon-vpc>` or to the HDFS store in the Amazon EMR instance.







.. 
.. commented out
.. 
.. Rollback Versions
.. --------------------------------------------------
.. .. include:: ../../includes/cmd_cr_install_rollback.rst
.. 
.. For example:
.. 
.. .. code-block:: console
.. 
..    cr install rollback [SNAPSHOT]
.. 


.. 
.. Upgrade the Platform
.. --------------------------------------------------
.. 
.. To upgrade from an older version of the |versive| platform, simply install the new version. The installer will automatically set the new version to be the active version of the |versive| platform. Use the :ref:`cmd-cr-install` group of commands to manage the active version that is running on the |versive| platform.
.. 


.. 
.. Uninstall the Platform
.. --------------------------------------------------
.. 
.. To uninstall the |versive| platform from a cluster, use the ``--clean`` option when running the installer:
.. 
.. .. code-block:: console
.. 
..    $ sh ./cr-<version>.cc.<revision>.run -- --clean
.. 
.. A configuration file may also be specified:
.. 
.. .. code-block:: console
.. 
..    $ sh ./cr-<version>.cc.<revision>.run -- --clean --config_file=<FILE>
.. 