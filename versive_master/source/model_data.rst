.. 
.. versive, primary, security engine
.. 

==================================================
Model Data
==================================================

.. include:: ../../includes_terms/term_model.rst

The |vse| looks for behaviors that suggest the presence of an active adversary campaign. The diagram below shows how modeling results, stage scores, and combined stage scores work together to build a list of threat cases.

 
.. _model-data-develop-models:

Develop Models
==================================================
.. TODO: This section is in the model_data.rst file in both platform and engine.

When developing a model, the best approach is to work iteratively. The following habits will make it easier to iterate on models during their development:

#. :ref:`Learn the initial model quickly <model-data-learn-models-quickly>`
#. :ref:`Use checkpoints <model-data-checkpoint-often>`, first against the baseline established by the initial model, and then frequently as the model is improved
#. :ref:`Save to persistent storage <model-data-save-often>`
#. :ref:`Use print statements <model-data-use-print-statements>` to verify work
#. :ref:`Carefully evaluate learned models for information leakage <model-data-identify-information-leakage>`

.. _model-data-learn-models-quickly:

Learn Models Quickly
--------------------------------------------------
.. TODO: This section is in the model_data.rst file in both platform and engine.

Start out by building a simple predictive model and adding new data and calculations iteratively, relearning new models frequently:

* Change one variable at a time, and then compare the summary statistics to the baseline model.
* Do the results make sense?
* Do they agree with the expectations for the model?
* View data summaries and perform other checks on the data periodically to verify that everything is working as expected.

.. _model-data-checkpoint-often:

Checkpoint Often
--------------------------------------------------

.. 
.. commenting this out because it needs to be edited again
.. 
.. The architecture of the distributed layer in the |versive| platform makes use of RDD (Resilient Distributed Datasets) chains to transform data in the cluster. As data transformations are applied via the |versive| platform API, a RDD is added to the chain. These RDDs form a chain of transformations as the user continues to do various operations on their data. Once the user needs to work with their data in some way, such as calling the ``summarize_table()`` function, the RDD chain is applied to the data in a distributed fashion and the results are returned to the caller in the form of a DataTable object. This process is repeated nearly every time a user needs to access the table's data to perform some operation. Having to apply a long chain of transformations each time data is accessed can be highly inefficient, especially when the same tables are used repeatedly.
.. 
.. Using checkpoints is an important part of the model building process because it allows the user to achieve better performance while building a large model. Checkpointed items are retained across working sessions, as long as the compute cluster is not reset. Using checkpoints frequently allows a model to be easily returned to a previous state, if necessary. Because the contents of a checkpoint are held in-memory, this speeds up development by preventing the need to re-run expensive computations.
.. 
.. The primary reasons to use checkpoints include:
.. 
.. * Performance. Checkpointing allows the user to have the |versive| platform apply all the outstanding RDD operations immediately, and then update the in-memory copy of the data. This can greatly increase the speed of any further consumption of the data as the RDD chain will not need to be reapplied to the data to utilize it.
.. * Persistence. It allows data and models to be saved in-between sessions as long as as the |versive| platform is not running in single processor mode.
.. * Rolling back. When working in interactive mode, checkpointing can be a useful tool because it allows the user to go back to the previous state of a table.
.. * Debugging. The amount of time spent resolving an issue in a user-defined function (UDF) that is passed to the |risp| is reduced. Calling a checkpoint immediately after making the transformation function call will force the user-defined function to be applied across the whole data set. If there is a problem it will be displayed immediately.

.. include:: ../../includes_api/function_checkpoint.rst

.. TODO: Currently in both model_data.rst and python.rst copypasta

The following examples show how to use checkpoints:

**Checkpoint a table or model, and then assign a label**

.. code-block:: python

   risp.checkpoint(table_or_model, 'my_label')

**Get all checkpoints**

.. code-block:: python

   risp.get_checkpoints()

**Retrieve a table or model**

.. code-block:: python

   my_table_or_model = risp.load_checkpoint('my_label')

.. TODO: Currently in both model_data.rst and python.rst copypasta






.. _model-data-save-often:

Save Often
--------------------------------------------------
.. TODO: This section is in the model_data.rst file in both platform and engine.

Tables and models should be saved often to local storage or to a location on the storage cluster.

.. note:: Checkpointed computations held in-memory are not saved; however, the state of the model will be refreshed, especially for expensive computations associated with model metrics, learning curves, and scoring.

The following functions are available:

* .. include:: ../../includes_api/function_save_crtable.rst
* .. include:: ../../includes_api/function_save_csv.rst
* .. include:: ../../includes_api/function_save_model.rst
* .. include:: ../../includes_api/function_save_xls.rst

.. _model-data-use-print-statements:

Use Print Statements
--------------------------------------------------
.. TODO: This section is in the model_data.rst file in both platform and engine.

When running a script, it can be difficult to identify the location at which a problem is occuring, especially when a user-defined function is used to join tables. The following functions should use ``print`` statements to verify data is being correctly removed, added, and modified:

* .. include:: ../../includes_api/function_count_rows.rst
* .. include:: ../../includes_api/function_get_column_names.rst
* .. include:: ../../includes_api/function_summarize_table.rst

.. _model-data-identify-information-leakage:

Identify Information Leakage
--------------------------------------------------
.. TODO: This section is in the model_data.rst file in both platform and engine, but without the example in the 3rd paragraph.

.. include:: ../../includes_terms/term_information_leakage.rst

.. include:: ../../includes_api/function_measure_input_importance.rst

Carefully evaluate learned models to identify if information leakage is occuring. If the initial model makes predictions with very high accuracy, investigate for information leakage. Use a one-in report to help identify the source of information leakage.




.. _model-types:

Model Types
==================================================

The following types of models are used by the engine to collect, analyze, predict, and score data:

* :ref:`model-types-baseline`
* :ref:`model-types-measurement`
* :ref:`model-types-combo`


.. _model-types-baseline:

Baseline Models
--------------------------------------------------
.. include:: ../../includes_terms/term_model_type_baseline.rst


.. _model-types-measurement:

Measurement Models
--------------------------------------------------
.. include:: ../../includes_terms/term_model_type_measurement.rst

.. include:: ../../includes_terms/term_model_type_measurement_nested.rst


.. _model-types-combo:

Combo Model
--------------------------------------------------
.. include:: ../../includes_terms/term_model_type_combo.rst



.. _model-strategies:

Model Strategies
==================================================
.. TODO: A model strategy does ... o_O.

Use the ``strategy_name`` setting in the :ref:`configure-modeling` section of the configuration file to specify the modeling strategy:

* :ref:`model-strategy-comp-cdf`
* :ref:`model-strategy-top-k-entities`
* :ref:`model-strategy-custom-scoring`

.. 
.. commenting out, as these are deprecated in favor of top K and comp_cdf strategies for the security engine
.. 
.. * :ref:`model-strategy-discrete-likelihood`
.. * :ref:`model-strategy-likelihood`

.. _model-strategy-comp-cdf:

Cumulative Distribution
--------------------------------------------------
.. include:: ../../includes_terms/term_cumulative_distribution.rst

.. include:: ../../includes_config/modeling_strategy_comp_cdf.rst

**To use a cumulative distribution strategy**

Set the **strategy_name** setting in the :ref:`configure-modeling` section of the configuration file to ``comp_cdf``:

.. code-block:: yaml

   modeling:
     ...
     default_strategy:
       - strategy_name: 'comp_cdf'



.. 
.. commenting out, as this is deprecated in favor of top K and comp_cdf strategies for the security engine
.. 
.. .. _model-strategy-discrete-likelihood:
.. 
.. Discrete Likelihood
.. --------------------------------------------------
.. .. include:: ../../includes_config/modeling_strategy_discrete.rst
.. 
.. **To use a discrete likelihood strategy**
.. 
.. Set the **strategy_name** setting in the :ref:`configure-modeling` section of the configuration file to ``discrete_likelihood``, and then configure the related settings:
.. 
.. .. code-block:: yaml
.. 
..    modeling:
..      ...
..      default_strategy:
..        - bins_per_sigma: 1
..        - interesting_side: left_side
..        - last_fencepost: 4
..        - strategy_name: 'discrete_likelihood'
.. 
.. Bins per Sigma
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. .. include:: ../../includes_config/modeling_strategy_discrete_bins_per_sigma.rst
.. 
.. Interesting Sides
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. The engine identifies anomalies in data, but not all anomalous results are indicative of a network attack. Right-side outliers are more interesting and important than left-side outliers. This is because the adversary behavior model expects large data volumns, longer sessions, large numbers of resources contacted, and other large indicators of adversary activity across all stages of the adversary campaign. Left-side outliers are less interesting, but can be useful to help confirm the discovery of adversary activity.
.. 
.. To easily surface results that are unusually high (right-side) or unusually low (left-side) for a particular target, use the **interesting_side** setting in the :ref:`configure-modeling` section of the configuration file:
.. 
.. .. code-block:: yaml
.. 
..    modeling:
..      strategies:
..        <target_column>:
..          - interesting_side: right_side
..          - strategy_name: 'discrete_likelihood'
.. 
.. .. include:: ../../includes_config/modeling_strategy_discrete_interesting_side.rst
.. 
.. For example, suppose there are target columns TargetA, TargetB, and TargetC. Only results for columns TargetA and TargetC should be included, and only when they are above the expected values and only when they include all anomalous results for the TargetB column. The configuration for that scenario is as follows:
.. 
.. .. code-block:: yaml
.. 
..    strategies:
..      TargetA:
..        strategy_name: 'discrete_likelihood'
..        interesting_side: right_side
..      TargetB:
..        strategy_name: 'discrete_likelihood'
..      TargetC:
..        strategy_name: 'discrete_likelihood'
..        interesting_side: right_side
.. 
.. Last Fencepost
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. .. include:: ../../includes_config/modeling_strategy_discrete_last_fencepost.rst
.. 

.. 
.. commenting out, as this is deprecated in favor of top K and comp_cdf strategies for the security engine
.. 
.. .. _model-strategy-likelihood:
.. 
.. Likelihood
.. --------------------------------------------------
.. .. include:: ../../includes_config/modeling_strategy_likelihood.rst
.. 
.. **To use a likelihood strategy**
.. 
.. Set the **strategy_name** setting in the :ref:`configure-modeling` section of the configuration file to ``likelihood``:
.. 
.. 
.. .. code-block:: yaml
.. 
..    modeling:
..      ...
..      default_strategy:
..        - strategy_name: 'likelihood'
.. 

.. _model-strategy-top-k-entities:

Top K Entities
--------------------------------------------------
.. include:: ../../includes_config/modeling_strategy_top_k_entities.rst

**To use a top K entities strategy**

Set the **strategy_name** setting in the :ref:`configure-modeling` section of the configuration file to ``top_k_entities``, and then configure the related settings:

.. code-block:: yaml

   modeling:
     ...
     default_strategy:
       - strategy_name: 'top_k_entities'
       - top_k_threshold: 50

Top K Threshold
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_config/modeling_strategy_top_k_entities_threshold.rst


.. _model-strategy-custom-scoring:

Custom Scoring
--------------------------------------------------
A custom scoring strategy is implemented in three steps:

#. Define the strategy in the configuration file.
#. Add a custom function to the customization package that returns a scoring strategy object.
#. Define a custom scoring class that is a subclass of ``MeasurementModelStrategy``; the sections below use ``CustomScoring`` to represent the name of the custom scoring class.

The following sections discuss each of these steps.

Configure
++++++++++++++++++++++++++++++++++++++++++++++++++
Set the value for ``strategy_name`` in the :ref:`configure-modeling` section of the configuration file to ``custom_score_strategy``. For example, to set the default strategy to the custom strategy, the configuration file would be:

.. code-block:: yaml

   modeling:
     ...
     strategies:
       default_strategy:
         strategy_name: 'custom_score_strategy'
     ...

Alternatively, the customization could be set for specific measurement targets:

.. code-block:: yaml

   modeling:
     ...
     default_strategy:
       strategy_name: 'discrete_likelihood'
     <target_column>:
       strategy_name: 'custom_score_strategy'


custom_score_strategy()
++++++++++++++++++++++++++++++++++++++++++++++++++
In the customization package, define a custom scoring strategy using the ``custom_score_strategy()`` function with the following structure:

.. code-block:: python

   def custom_score_strategy(modeling_cfg,        # crepe.apt.config.ModelConfig
                             target_name,         # name of the target column
                             anomaly_formatter):  # OK to ignore
     return CustomScoring(...)

where

* ``modeling_cfg`` is ``crepe.apt.config.ModelConfig``
* ``target_name`` is the name of the target column
* ``anomaly_formatter`` is optional
* ``CustomScoring`` is a subclass of ``MeasurementModelStrategy``

CustomScoring()
++++++++++++++++++++++++++++++++++++++++++++++++++
In the customization package, define the custom scoring subclass, based on ``MeasurementModelStrategy``, and with the following structure:

.. code-block:: python

   class CustomScoring(MeasurementModelStrategy):
     def __init__(self)
       ...

     def apply_model(self, model, data):
       ...

     def get_consumable_outputs(self, model):
       ...

     return [self.output_column_name(model)]

The ``get_consumable_outputs`` function can be customized, but it's usually sufficient if the scoring strategy adds the score in column ``'surprise_' + model.target``. Normally, the ``model`` parameter is an instance of ``crepe.apt.model.SimpleModel``, and the primary method to call is ``model.predict()``.

.. note:: To see the API documentation for MeasurementModelStrategy and read the full details on the interface contract:
 
   .. code-block:: none
 
      $ crpython
      >>> import crepe.apt.combo as combo
      >>> help(combo.MeasurementModelStrategy)




.. _model-results:

Model Results
==================================================
Model results are :ref:`built around risk scores <model-data-risk-scores>`, including aspects such as:

* :ref:`model-data-surprise-scores`
* :ref:`model-data-relevance-weights`
* :ref:`model-data-measurement-targets`
* :ref:`model-data-noteworthy-scores`


.. _model-data-risk-scores:

Risk Scores
--------------------------------------------------
.. include:: ../../includes_terms/term_score_risk.rst


.. _model-data-surprise-scores:

Surprise Scores
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_terms/term_score_surprise.rst

Individual scores for each discovered anomaly are combined to get behavior-, stage- and overall-level surprise scores. By default, the engine scores targets with equal weighting when generating surprise scores. Surprise scores are configurable and the goal is to ensure that increasingly large outliers result in increasingly high surprise scores.

If multiple anomalies are associated with the same strong measurement target, this should result in an even higher surprise score.

**Example: Surprise Scores**

The following example shows the sections in the configuration file are necessary to configure surprise scoring:

.. code-block:: yaml

   modeling:
     ...
     behaviors:
       - key: collection_via_netflow
         stages: [3]
         targets:
           internal_neighbors_1_2_days_count: strong
       - key: recon_via_dns
         stages: [3]
         targets:
           dns_internal_A_named_domain_count_distinct: strong
           dns_internal_AAAA_named_domain_count_distinct: strong
           dns_internal_PTR_dest_ip_count_distinct: weak
       - key: exfiltration_via_proxy
         stages: [5]
         targets:
         cumulative_exclusive_regdomain_high_publication_ratio: strong
         cumulative_high_publication_ratio_to_bytes_out: weak
         cumulative_exclusive_regdomain_to_bytes_out: weak
     relevance_weights:
       strong: 1.0
       weak: 0.25
       circumstantial: 0.0
   ...
   predict:
     ...
     noteworthy_surprise_rank: 25
     ...
   results:
     ...
     keep_threat_case_function: require_noteworthy_2_out_of_3_stages     
     ...

The output in the ``summary`` table will contain information similar to the following:

.. code-block:: text

   start_date = 2017-03-06,
   end_date = 2017-03-07,
   score = 1.44642860629,
   stage_3_score = 0.0178571436554,
   stage_4_score = 0.5,
   stage_5_score = 0.928571462631,
   case_guid = 64f1-4141-46g1-9h80-b24a,
   top_behaviors = exfiltration_via_proxy (0.500000019486);
                   collection_via_netflow (0.5);
                   exfiltration_via_netflow (0.428571443145),
   top_hosts = 192.168.1.1 (1.44642860629),
   top_users = mnoble,
   num_noteworthy_stage_3 = 1,
   num_noteworthy_stage_4 = 2,
   num_noteworthy_stage_5 = 4

.. note:: The previous example is formatted vertically for readability and combines the column headers and values into the same row, with an equals sign in-between the column name and the value. Normal output for the ``summary`` table is:

   .. code-block:: text

      start_date,end_date,score,stage_3_score,...
      2017-03-06,2017-03-07,1.82142861187,0.0178571436554,...


.. _model-data-relevance-weights:

Relevance Weights
++++++++++++++++++++++++++++++++++++++++++++++++++
To configure surprise scores to discover noteworthy anomalies use the **relevance_weights** setting in the :ref:`configure-modeling` section of the configuration file to define the weighting for strong, weak, and circumstantial scoring, and then use the **behaviors** section to specify to which measurement targets these weights are applied.

**strong**
   .. include:: ../../includes_config/modeling_relevance_weights_strong.rst

**weak**
   .. include:: ../../includes_config/modeling_relevance_weights_weak.rst

**circumstantial**
   .. include:: ../../includes_config/modeling_relevance_weights_circumstantial.rst


.. _model-data-measurement-targets:

Measurement Targets
++++++++++++++++++++++++++++++++++++++++++++++++++
Measurement targets may be specified in two locations in the configuration file:

* Under **behaviors** in the :ref:`configure-modeling` section of the configuration file
* As a custom target defined under **relevance_weights** using the **custom** setting

.. TODO: maybe sync up the key, stages, and targets descriptions between here and configure.rst.

Most measurment targets are defined using the **behaviors** section. Each group of measurement targets is defined as a list of targets associated with one (or more) stages and a key:

**key**
   The label for the behavior. If one (or more) measurement targets associated with this behavior indicate anomalous behavior, and if the scores calculated for this behavior are high enough, this key may appear in the ``top_behaviors`` column of the ``summary`` table along with its calculated score.

**stages**
   The stages to which the list of measurement targets are applied.

**targets**
   A list of measurement targets. Each measurement target defines a column in the modeling tables against which the engine will calculate scoring, and then assigns a relevance weight to that column. For example: ``collection_via_netflow: strong``, ``exfiltration_via_proxy: weak``, or ``recon_via_dns: circumstantial``.

For example:

.. code-block:: yaml

   behaviors:
       ...
     - key: collection_via_netflow
       ...
     - key: exfiltration_via_proxy
       stages: [5]
       targets:
         cumulative_exclusive_regdomain_high_publication_ratio: strong
         cumulative_high_publication_ratio_to_bytes_out: weak
         cumulative_exclusive_regdomain_to_bytes_out: weak
     ...


.. _model-data-noteworthy-scores:

Noteworthy Scores
++++++++++++++++++++++++++++++++++++++++++++++++++
A noteworthy anomaly should be associated with a strong measurement target and have a large enough surprise score to be included in the ranked list of surprise scores.

.. TODO: Is the following paragraph still true? Depending on answer, update also the Example: Surprise Scores section.

When noteworthy surprise scores are discovered they are shown in the ``num_noteworthy_stage_3``, ``num_noteworthy_stage_4``, ``num_noteworthy_stage_5`` columns in the ``summary`` table and the ``is_noteworthy`` column in the details table will contain a ``1``.


Ranked Scores
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Use the **noteworthy_surprise_rank** setting in the :ref:`configure-predict` section of the configuration file to specify the maximun number of items that may appear in a ranked list of noteworthy surprise scores.


.. _model-data-threat-case-levels:

Threat Case Levels
--------------------------------------------------
.. include:: ../../includes_terms/term_threat_case_level.rst

Threat case levels may be one of the following:

* A threat case must have a noteworthy score in any stage
* A threat case must have a noteworthy score in two stages
* A threat case must have a noteworthy score in each of the |stage_recon|, |stage_collect|, and |stage_exfil| stages
* A threat case must have a single host with a noteworthy score in two stages
* A threat case identifies a single host with a noteworthy score in each of the |stage_recon|, |stage_collect|, and |stage_exfil| stages or identifies multiple hosts that combine to have noteworthy scores in each of the |stage_recon|, |stage_collect|, and |stage_exfil| stages


Any Stage
++++++++++++++++++++++++++++++++++++++++++++++++++
Set the **keep_threat_case_function** setting in the :ref:`configure-results` section of the configuration file to ``None`` to require that a noteworthy stage score must appear in any stages.

2 of 3 Stages
++++++++++++++++++++++++++++++++++++++++++++++++++
Set the **keep_threat_case_function** setting in the :ref:`configure-results` section of the configuration file to ``require_noteworthy_2_out_of_3_stages`` to require the number of stages for which a noteworthy stage score must appear be at least two.

All Stages
++++++++++++++++++++++++++++++++++++++++++++++++++
Set the **keep_threat_case_function** setting in the :ref:`configure-results` section of the configuration file to ``require_noteworthy_all_stages`` to require that a noteworthy stage score must appear in all stages. 

Single Host
++++++++++++++++++++++++++++++++++++++++++++++++++
Set the **keep_threat_case_function** setting in the :ref:`configure-results` section of the configuration file to ``require_noteworthy_per_host_2_out_3`` to require that a noteworthy stage score must appear in all stages for a threat case with a single host.

Single or Multiple Hosts
++++++++++++++++++++++++++++++++++++++++++++++++++
Set the **keep_threat_case_function** setting in the :ref:`configure-results` section of the configuration file to ``require_noteworthy_2_maybe_3_stages`` to require the number of stages for which a noteworthy score must appear to be in all three stages. A threat case with a single host must have a noteworthy score in all three stages. A threat case with multiple hosts must combine to show noteworthy scores in all three stages, even if individual hosts in that threat case show noteworthy scores in fewer than three stages.



.. _model-data-multihost:

Multiple Hosts
--------------------------------------------------
The |vse| builds :ref:`threat cases that contain multiple suspicious host machines <model-data-multihost>`. Two entity attribute tables define multihost behavior, along with a set of multihost-specific configuration settings:

#. Configuration settings that are part of the :ref:`results <model-data-multihost-config>` section of the configuration file
#. A table that defines the :ref:`connectivity graph <model-data-multihost-connectivity>`
#. A table that defines the :ref:`filtering levels <model-data-multihost-filtering-levels>` to be applied.


.. _model-data-multihost-config:

Configuration Settings
++++++++++++++++++++++++++++++++++++++++++++++++++
The following configuration settings must be specified in the :ref:`configure-results` section of the configuration file:

**connectivity_graph_function** (str, optional)
   .. include:: ../../includes_config/connectivity_graph_function.rst
  
**connectivity_graph_max_depth** (int, optional)
   .. include:: ../../includes_config/connectivity_graph_max_depth.rst
   
**connectivity_graph_source** (str, optional)
   .. include:: ../../includes_config/connectivity_graph_source.rst
   
**group_connected_host_anomalies** (bool, optional)
   .. include:: ../../includes_config/group_connected_host_anomalies.rst
   
**threat_case_overlap_merge_percent** (float, optional)
   .. include:: ../../includes_config/threat_case_overlap_merge_percent.rst

For example:

.. code-block:: yaml

   results:
     ...
     connectivity_graph_function: load_filtered_connectivity_graph
     connectivity_graph_max_depth: 1
     connectivity_graph_source: global_local_filtered_connectivity_graph
     group_connected_host_anomalies: True
     threat_case_overlap_merge_percent: 100


.. _model-data-multihost-connectivity:

Connectivity Graph
++++++++++++++++++++++++++++++++++++++++++++++++++
The following entity attribute table defines the connectivity graph for flow data:

.. code-block:: yaml

   connectivity_graph:
     generate: default_create_connectivity_graph
     udf_source: LIBRARY
     type: PRE_MODELING_TABLE_ATTRIBUTE
     key_columns: []
     attribute_columns: []
     path_add_date_suffix: True
     depends_on:
       - datasource: "netflow"
         phase: agg
         groupby_key: day_host
         lookback: 30


.. _model-data-multihost-filtering-levels:

Filtering Levels
++++++++++++++++++++++++++++++++++++++++++++++++++
The following entity attribute table defines the filtering levels to be applied to the connectivity graph:

.. code-block:: yaml

   global_local_filtered_connectivity_graph:
     generate: create_global_local_filtered_connectivity_graph
     udf_source: LIBRARY
     type: PRE_MODELING_TABLE_ATTRIBUTE
     key_columns: []
     attribute_columns: []
     path_add_date_suffix: True
     function_extra_args:
       graph_filter_threshold_local: 4
       graph_filter_threshold_global: 3000
     depends_on:
       - datasource: connectivity_graph
         phase: generated
       - datasource: "dns.netflow.proxyweb"
         phase: scored
         groupby_key: day_host_fqdn_internal


   

.. 
.. .. _model-strategies-many-internal-neighbors:
.. 
.. Many Internal Neighbors
.. --------------------------------------------------
.. .. TODO: NOT A RESULT, PROBABLY. DUMPED HERE FROM MODELING STRATEGIES FOR NOW.
.. 
.. The following example shows a user-defined function that, for all hostnames that have been identified as having a high consumption ratio, checks if any of those hostnames have more than 30 internal neighbors
.. 
.. .. code-block:: python
.. 
..    def _many_neighbors_one_col(neighbor_count):
..      is_many = 'many_internal_neighbors' if neighbor_count > 30 else 'few_internal_neighbors'
..      return "_".join([is_many, 'to'])
..    
..    def gmt_on_load(context, table):
..      ad = context.get_only_output_descriptor('modeling_load')
..        if 'host' in ad['groupby_key']:
..          windows = context.config.multi_day_features.lookback_internal_neighbors
..            windows = windows[:1]
..            pivot_cols = ['proxyweb_internal_to_bytes_in_sum',
..                          'proxyweb_internal_high_consumption_ratio_to_bytes_in_sum']
..          for window in windows:
..            category_cols = ('internal_neighbors_1_{0}_days_count'.format(window),)
..            pivot_fn = _many_neighbors_one_col
..            table = risp.add_columns(table,
..                                     list(category_cols),
..                                     Column('_'.join(category_cols) + '_category', str),
..                                     pivot_fn)
..          for pivot in pivot_cols:
..            pivots = (pivot,)
..            category_to_pivot = {'many_internal_neighbors_to': pivots, }
..            table = flexi_categorize(table,
..                                     category_cols,
..                                     category_to_pivot,
..                                     pivot_fn)
..        else:
..          raise NotImplementedError('Only implemented for entity host')
..    
..    return table
.. 
.. The **on_load_function** setting in the :ref:`configure-generate-modeling` section of the configuration file must specify the name of this function:
.. 
.. .. code-block:: yaml
.. 
..    generate_modeling:
..      on_load_function: 'gmt_on_load'
..      ...
.. 


.. 
.. .. _model-data-labels:
.. 
.. Labels
.. --------------------------------------------------
.. .. TODO: This whole section is very drafty and expiremental.
.. 
.. Labels allow for models to contain secondary categorizations.
.. 
.. .. _model-data-label-subnets:
.. 
.. Subnets
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. "Use a label to identify subnets. This can enable discovery of sub-groups within a network topology and to associate hosts within that sub-group."
.. 
.. Subnet labels can help the |vse| identify subsets of an internal network. Use a CSV file to specify a list of subnets for to labels should be applied. The |vse| will process the ``hostname`` and ``fqdn`` columns against the contents of this CSV file to add source and destination IP address labels to the modeling table.
.. 
.. Use the ``subnet_labels`` settings group in the ``configure-generate-modeling`` section of the configuration file to configure how subnet labels are added to modeling tables.
.. 
.. Create a CSV file with three columns: ``label``, ``address``, and ``mask``. Each row in the table specifies the label for a subnet, its address, and its mask. For subnets defined using CIDR notation, the ``mask`` column should be left empty. For example:
.. 
..          For example, a file located at ``/security/subnet_labels.csv``:
.. 
..          .. code-block:: none
.. 
..             label               address               mask
..             Management          10.10.2.0          255.255.255.0
..             DMZ Guest           10.10.10.0         255.255.255.0
..             Services            10.10.24.0         255.255.255.0
..             Storage             10.10.18.0         255.255.255.0
..             Test                10.10.25.0         255.255.255.0
..             Users               10.10.28.0         255.255.252.0
..             HDFS Cluster 2A     172.18.71.0/26
..             HDFS Cluster 2B     172.18.71.64/26
..             HDFS Cluster 2C     172.18.71.128/26
..             Engineering VPC     172.18.70.0/24
..             Admin Subnet        172.18.70.0/26
.. 
.. Update the configuration file, under the configure-generate-modeling section:
.. 
.. .. code-block:: yaml
.. 
..    generate_modeling:
..      additional_features:
..        subnet_labels:
..          columns: ['label', 'address', 'mask']
..          delimiter: 'string'
..          has_headers: False
..          path: '/path/to/CSV/file'
.. 
.. For example:
.. 
.. .. code-block:: yaml
.. 
..    generate_modeling:
..      additional_features:
..        subnet_labels:
..          columns: ['label', 'address', 'mask']
..          delimiter: ','
..          has_headers: False
..          path: '/security/subnet_labels.csv'
.. 




.. _model-data-tables:

Table Types
==================================================
.. TODO: This section may need updating based on the Blocks work.

There are two types of tables that may be used by the engine: static and generated.

All tables, regardless of type, are used to process data for the purpose of building models, identifying behaviors, and building cases.


.. _model-data-static-table:

Static Tables
--------------------------------------------------
Static tables are created outside the engine, using static data. Use the **path** setting in the :ref:`configure-entity-attribute-sources` section in the configuration file to specify the path to the directory in which a static table is located.



.. _model-data-generated-table:

Generated Tables
--------------------------------------------------
Generated tables are created by the :ref:`command-drive` command, which aggregates data to the table from other data sources. Generated tables, once created, must be joined during earlier processing phases.

Generated tables may be :ref:`created using functions that are built-in <model-data-generated-table>` to the engine and then configured in the :ref:`configure-entity-attribute-sources` section in the configuration file and they may be :ref:`created as entirely custom tables <model-data-custom-table>` (requiring both the configuration and a custom function to be added to the customization package).



.. _model-data-custom-table:

Custom Tables
--------------------------------------------------
The configuration of a custom table is defined in the :ref:`configure-entity-attribute-sources` section of the configuration file and is similar to:

.. code-block:: yaml

   custom_table_name:
     generate: 'function_name'
     udf_source: CUSTOM
     key_columns: ['key_column']
     attribute_columns: ['column_name', 'column_name', ...]
     type: TYPE
     depends_on: [{'datasource': 'source_name', 'phase': 'phase_name'}]

where

* ``generate`` must specify the  name of a function in the customization package that tells the engine how to process the data in the table
* ``udf_source`` must be set to ``CUSTOM``
* ``type`` must be set to ``PRE_MODELING_TABLE_ATTRIBUTE`` or ``MODELING_TABLE_ATTRIBUTE``, depending on the phase during which the table is generated
* ``key_columns`` specifies one (or more) columns from which the custom table will use as key columns
* ``attribute_columns`` must specify any generated columns to be added back into the table
* ``depends_on`` specifies a data source and an engine phase. The data provided by that data source is processed up to that application phase, and then those results are used to provide data to the sharing table

and the customization package contains a function with the same name as the one specified by ``generate``.





.. _model-data-table-generators:

Table Generators
==================================================
The following table generators are built-in to the engine:

* :ref:`model-data-hostname-table`
* :ref:`model-data-ranks`
* :ref:`model-data-sessionize-table`
* :ref:`model-data-sharing-table`

.. * :ref:`model-data-beaconing`

See the sections below for more information about each of these built-in table generators.

.. 
.. .. _model-data-beaconing:
.. 
.. Beaconing Tables
.. --------------------------------------------------
.. .. TODO: Does Beaconing and Malicious Domains belong in this topic or only in the currently-named "describe-behaviors" topic?
.. 
.. .. TODO: ADD_INCLUDE_FOR_TOP_LEVEL_PARAGRAPH_OF_BEACONING
.. 
.. Beaconing relies on a series of generated tables to identify beaconing behavior and for generating a beaconing score.
.. 
.. .. _model-data-beaconing-raw-features-table:
.. 
.. Raw Features Table
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. .. include:: ../../includes/entity_attribute_table_cse_beaconing_raw_features.rst
.. 
.. The following columns are added to the table:
.. 
.. * url_regdomain
.. * num_obs_interval
.. * interval_sum
.. * interval_sum_squares
.. * num_obs_bytes_out
.. * bytes_out_sum
.. * bytes_out_sum_squares
.. * interval_coeff_variation
.. * interval_mean_ci_upper
.. * bytes_out_coeff_variation
.. * bytes_out_mean_ci_upper
.. * url_regdomain_count_distinct
.. 
.. The configuration of a beaconing raw features table is defined in the :ref:`configure-entity-attribute-sources` section of the configuration file and is similar to:
.. 
.. .. code-block:: yaml
.. 
..    beaconing_raw_features_table_name:
..      generate: create_beaconing_multi_day_raw_features_table
..      udf_source: LIBRARY
..      key_columns: ['hostname', 'url_host']
..      attribute_columns:
..        - column_name
..        - ...
..      type: TYPE
..      depends_on: [{'datasource': 'source_name', 'phase': 'phase_name'}]
.. 
.. where
.. 
.. * ``generate`` must be set to ``create_beaconing_multi_day_raw_features_table``
.. * ``udf_source`` must be set to ``LIBRARY``
.. * ``type`` must be set to ``PRE_MODELING_TABLE_ATTRIBUTE`` or ``MODELING_TABLE_ATTRIBUTE``, depending on the phase during which the table is generated
.. * ``key_columns`` must specify ``hostname`` and ``url_host``
.. * ``attribute_columns`` may  specify one (or more) of the columns that are added to the table by this generator
.. * ``depends_on`` specifies a data source and an engine application phase. The data provided by that data source is processed up to that application phase, and then those results are used to provide data to the intermediate table
.. 
.. 
.. 
.. .. _model-data-beaconing-single-day-table:
.. 
.. Single-day Table
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. .. include:: ../../includes/entity_attribute_table_cse_beaconing_single_day.rst
.. 
.. The following columns are added to the table:
.. 
.. * ``num_obs_interval``
.. * ``interval_sum``
.. * ``interval_sum_squares``
.. * ``num_obs_bytes_out``
.. * ``bytes_out_sum``
.. * ``bytes_out_sum_squares``
.. * ``interval_mean``
.. * ``interval_stddev``
.. * ``bytes_out_mean``
.. * ``bytes_out_stddev``
.. 
.. 
.. The configuration of a beaconing single-day table is defined in the :ref:`configure-entity-attribute-sources` section of the configuration file and is similar to:
.. 
.. .. code-block:: yaml
.. 
..    beaconing_single_day_table_name:
..      generate: create_beaconing_single_day_table
..      udf_source: LIBRARY
..      key_columns: ['hostname', 'url_host']
..      attribute_columns:
..        - column_name
..        - ...
..      type: TYPE
..      depends_on: [{'datasource': 'source_name', 'phase': 'phase_name'}]
.. 
.. where
.. 
.. * ``generate`` must be set to ``create_beaconing_single_day_table``
.. * ``udf_source`` must be set to ``LIBRARY``
.. * ``type`` must be set to ``PRE_MODELING_TABLE_ATTRIBUTE`` or ``MODELING_TABLE_ATTRIBUTE``, depending on the phase during which the table is generated
.. * ``key_columns`` must specify ``hostname`` and ``url_host``
.. * ``attribute_columns`` may  specify one (or more) of the columns that are added to the table by this generator
.. * ``depends_on`` specifies a data source and an engine application phase. The data provided by that data source is processed up to that application phase, and then those results are used to provide data to the intermediate table
.. 
.. 
.. .. _model-data-beaconing-scores-table:
.. 
.. Scores Table
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. .. include:: ../../includes/entity_attribute_table_cse_beaconing_scores.rst
.. 
.. The following columns are added to the table:
.. 
.. * url_regdomain
.. * interval_coeff_variation
.. * interval_mean_ci_upper
.. * bytes_out_coeff_variation
.. * bytes_out_mean_ci_upper
.. * rank_interval_coeff_variation
.. * rank_interval_mean_ci_upper
.. * rank_bytes_out_coeff_variation
.. * rank_bytes_out_mean_ci_upper
.. * rank_exclusive_domains_count_distinct
.. * url_regdomain_count_distinct
.. * beaconing_score
.. 
.. The configuration of a beaconing scores table is defined in the :ref:`configure-entity-attribute-sources` section of the configuration file and is similar to:
.. 
.. .. code-block:: yaml
.. 
..    beaconing_scores_table_name:
..      generate: create_beaconing_multi_day_scores_table
..      udf_source: LIBRARY
..      key_columns: ['hostname', 'url_host']
..      attribute_columns:
..        - column_name
..        - ...
..      type: TYPE
..      depends_on: [{'datasource': 'source_name', 'phase': 'phase_name'}]
..         depends_on: [{'datasource': 'beaconing_{{window}}_days',
..                         'phase': 'generated'}]
.. 
.. where
.. 
.. * ``generate`` must be set to ``create_beaconing_multi_day_scores_table``
.. * ``udf_source`` must be set to ``LIBRARY``
.. * ``type`` must be set to ``PRE_MODELING_TABLE_ATTRIBUTE`` or ``MODELING_TABLE_ATTRIBUTE``, depending on the phase during which the table is generated
.. * ``key_columns`` must specify ``hostname`` and ``url_host``
.. * ``attribute_columns`` may  specify one (or more) of the columns that are added to the table by this generator
.. * ``depends_on`` specifies a data source and an engine application phase. The data provided by that data source is processed up to that application phase, and then those results are used to provide data to the intermediate table
.. 
.. 
.. .. _model-data-beaconing-example-beaconing:
.. 
.. Example: Beaconing
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Beaconing configuration requires three tables. The first two build the data required to calculate the beaconing score, which is tracked as the ``beaconing_score`` column in the third table.
.. 
.. .. code-block:: yaml
.. 
..    beaconing_single_day:
..      generate: create_beaconing_single_day_table
..      udf_source: LIBRARY
..      key_columns: ["hostname", "url_host"]
..      attribute_columns:
..        - num_obs_interval
..        - interval_sum
..        - interval_sum_squares
..        - num_obs_bytes_out
..        - bytes_out_sum
..        - bytes_out_sum_squares
..        - interval_mean
..        - interval_stddev
..        - bytes_out_mean
..        - bytes_out_stddev
..      type: PRE_MODELING_TABLE_ATTRIBUTE
..      path_add_date_suffix: True
..      depends_on: [{'datasource': 'proxyweb', 'phase': 'transformed'}]
..    
..    beaconing_7_days:
..      generate: create_beaconing_multi_day_raw_features_table
..      udf_source: LIBRARY
..      key_columns: ["hostname", "url_host"]
..      attribute_columns:
..        - url_regdomain
..        - num_obs_interval
..        - interval_sum
..        - interval_sum_squares
..        - num_obs_bytes_out
..        - bytes_out_sum
..        - bytes_out_sum_squares
..        - interval_coeff_variation
..        - interval_mean_ci_upper
..        - bytes_out_coeff_variation
..        - bytes_out_mean_ci_upper
..        - url_regdomain_count_distinct
..      type: PRE_MODELING_TABLE_ATTRIBUTE
..      path_add_date_suffix: True
..      depends_on: [{'datasource': 'beaconing_single_day',
..                    'lookback': 7,
..                    'phase': 'generated'},
..                   {'datasource': 'url_regdomain_7_days_percentile',
..                    'phase': 'generated'}]
..    
..    beaconing_7_days_scores:
..      generate: create_beaconing_multi_day_scores_table
..      udf_source: LIBRARY
..      key_columns: ["hostname", "url_host"]
..      attribute_columns:
..        - url_regdomain
..        - interval_coeff_variation
..        - interval_mean_ci_upper
..        - bytes_out_coeff_variation
..        - bytes_out_mean_ci_upper
..        - url_regdomain_count_distinct
..        - rank_interval_coeff_variation
..        - rank_interval_mean_ci_upper
..        - rank_bytes_out_coeff_variation
..        - rank_bytes_out_mean_ci_upper
..        - rank_exclusive_domains_count_distinct
..        - beaconing_score
..      type: PRE_MODELING_TABLE_ATTRIBUTE
..      path_add_date_suffix: True
..      depends_on: [{'datasource': 'beaconing_7_days',
 ..                        'phase': 'generated'}]
.. 
.. 
.. 
.. .. _model-data-beaconing-example-whitelists:
.. 
.. Example: Whitelists
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. .. TODO: Pasted in, needs editing.
.. 
.. URLs may be whitelisted to prevent specified domains from being included in beaconing data.
.. 
.. The whitelist is a text file on the storage cluster. It's location is specified by the ``whitelist_path`` setting, specified as a value of ``function_extra_args`` in the :ref:`configure-entity-attribute-sources` section of the configuration file. The whitelist file does not need to specify ``http://`` or ``https://``, as these are removed by default.
.. 
.. .. warning:: The whitelist file **must** be located at the path and named storage connection or else the contents of the whitelist cannot be applied.
.. 
.. For example, the configuration necessary to apply the contents of that whitelist file to the beaconing data:
.. 
.. .. code-block:: yaml
.. 
..    beaconing_multi_day_scores_table:
..      generate: create_beaconing_multi_day_scores_table
..      udf_source: LIBRARY
..      key_columns: ["hostname", "url_host"]
..      type: PRE_MODELING_TABLE_ATTRIBUTE
..      attribute_columns: []
..      path_add_date_suffix: True
..      depends_on: [{'datasource': 'beaconing_multi_day_raw_features_table',
..                    'phase': 'generated'}]
..      function_extra_args:  {'max_rank_for_beaconing_statistic': 10,
..                             'whitelist_path':'/myfolder/whitelist.csv'}
.. 
.. 
.. and then the contents of the whitelist file is similar to:
.. 
.. .. code-block:: none
.. 
..    *.google.com
..    www.berkeley.???
..    www.somewebsite.*
.. 




.. _model-data-hostname-table:

Hostnames Table
--------------------------------------------------
.. include:: ../../includes/entity_attribute_table_cse_hostname.rst

Hostname tables are configured using the following steps:

#. Verify dependencies, such as ensuring an appropriate data source is running through the engine up to the correct application phase.
#. Configure a hostname table that contains all of the columns necessary to generate the data.

The configuration of a sharing table is defined in the :ref:`configure-entity-attribute-sources` section of the configuration file and is similar to:

.. code-block:: yaml

   hostname_features_table_name:
     generate: create_hostname_features
     udf_source: LIBRARY
     key_columns: ['hostname']
     attribute_columns: []
     type: TYPE
     depends_on: [{'datasource': 'source_name', 'phase': 'phase_name'}]

where

* ``generate`` must be set to ``create_hostname_features``
* ``udf_source`` must be set to ``LIBRARY``
* ``type`` must be set to ``PRE_MODELING_TABLE_ATTRIBUTE`` or ``MODELING_TABLE_ATTRIBUTE``, depending on the phase during which the table is generated
* ``key_columns`` must be set to ``hostname``
* ``attribute_columns`` may specify one (or more) of the following columns to be added to the merged table:

  ``HOSTNAME_INCLUDES_FQDN``
     ``True`` if all labels up to (and including) the top-level domain name (TLD) are specified for the hostname, including for Unicode TLDs.

  ``HOSTNAME_IS_IP``
     ``True`` if the hostname is a valid IPv4 or IPv6 address.

  ``HOSTNAME_IS_IP_VALUE``
     A string that specifies the actual IP address. For IPv4 and IPv6 addresses, this identical to the hostname; however obfuscated IP addresses, such as an IPv4 address written in hex, will appear as IPv4 addresses.

  ``HOSTNAME_NUM_HYPHENS``
     A integer that represents the of the number of hyphens in the hostname.

* ``depends_on`` specifies a data source and an engine application phase. The data provided by that data source is processed up to that application phase, and then those results are used to provide data to the sharing table



Example: IP Addresses
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a hostname table to collect the data for all hostnames with IP addresses, the FQDN, and hyphens, and then merge only the ``HOSTNAME_IS_IP`` column to the table:

.. code-block:: yaml

   hostname_features:
     generate: create_hostname_features
     udf_source: LIBRARY
     key_columns: ['hostname']
     attribute_columns: ['HOSTNAME_IS_IP']
     type: MODELING_TABLE_ATTRIBUTE
     depends_on: [{'datasource': 'proxyweb', 'phase': 'standardized'}]


Example: Hostnames
++++++++++++++++++++++++++++++++++++++++++++++++++
.. TODO: Pasted in, needs editing.

Given an input table with the ``hostname`` column, use a generated entity attribute table to add columns that indicate if a hostname is an IP address, a fully qualified domain name, and the number of hyphens it contains.

To configure a generated attribute table add hostname columns:

#. Add the **hostname_features** group of settings to the :ref:`configure-entity-attribute-sources` section of the configuration file:

   .. code-block:: yaml

      entity_attribute_sources:
        hostname_features:
          generate: 'create_hostname_features'
          udf_source: 'LIBRARY'
          key_columns: ['hostname']
          attribute_columns:
            - 'HOSTNAME_IS_IP'
            - 'HOSTNAME_IS_IP_VALUE'
            - 'HOSTNAME_IS_FQDN'
            - 'HOSTNAME_NUM_HYPHENS'
          path_add_date_suffix: True
          depends_on:
            - datasource: proxyweb
              phase: standardized

   .. note:: The **type** setting does not need to be specified because it's default value---``MODELING_TABLE_ATTRIBUTE``---already specifies to join these columns during the **generate_modeling_table** phase.

#. Run the :ref:`command-drive` command. This will generate an entity attribute table with the following columns:

   ``HOSTNAME_IS_FQDN``
      A hostname is considered an FQDN when all labels up to and including the top-level domain name (TLD) are specified. Unicode TLDs are also supported. Set to string ``True`` if the hostname is an FQDN; otherwise, ``False``.

   ``HOSTNAME_IS_IP``
      Set to string ``True`` if the hostname is a valid IPv4 or IPv6 address; otherwise, ``False``.

   ``HOSTNAME_IS_IP_VALUE``
      Set to the actual IP address of the string deemed to be an IP. For IPv4 and IPv6 addresses, this will be identical to the hostname but, obfuscated IP addresses such as an IPv4 address written in hex will appear as the IPv4 address.

   ``HOSTNAME_NUM_HYPHENS``
      A count of the number of hyphens in the hostname.

#. Run the :ref:`command-drive` command. This will join the generated entity attribute table during the **generate_modeling_table** phase. The ``hostname`` column is the join key.


.. _model-data-ranks:

Ranking Tables
--------------------------------------------------
Use ranking tables to rank data based a key column, compared to additional column data, and over a defined number of days.

Ranking tables are configured using the following steps:

#. Identify the key column for which a ranking percentile will be generated.
#. Identify the columns necessary to build that ranking percentile.
#. Verify dependencies, such as ensuring an appropriate data source is running through the engine up to the correct application phase.
#. Configure a :ref:`intermediate table <model-data-ranks-intermediate-table>` that contains all of the columns necessary to generate the ranking metrics and percentiles for the key column.
#. Configure a :ref:`ranking table <model-data-ranks-ranking-table>` that uses the data from the intermediate table to calculate metrics and percentiles, and then returns a table that contains the percentile results for the key column.


.. _model-data-ranks-intermediate-table:

Intermediate Rank Table
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/entity_attribute_table_cse_rank_intermediate.rst

The configuration of an intermediate table is defined in the :ref:`configure-entity-attribute-sources` section of the configuration file and is similar to:

.. code-block:: yaml

   intermediate_table_name:
     generate: create_intermediate_rank_features
     udf_source: LIBRARY
     key_columns: ['key_column', 'additional_column_name', ...]
     type: TYPE
     depends_on: [{'datasource': 'source_name', 'phase': 'phase_name'}]

where

* ``generate`` must be set to ``create_intermediate_rank_features``
* ``udf_source`` must be set to ``LIBRARY``
* ``type`` must be set to ``PRE_MODELING_TABLE_ATTRIBUTE`` or ``MODELING_TABLE_ATTRIBUTE``, depending on the phase during which the table is generated
* ``key_columns`` specifies one (or more) columns from which the ranking table will calculate metrics and percentiles for the key column
* ``depends_on`` specifies a data source and an engine application phase. The data provided by that data source is processed up to that application phase, and then those results are used to provide data to the intermediate table


.. _model-data-ranks-ranking-table:

Ranking Table
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes/entity_attribute_table_cse_rank.rst

The columns in a ranking table have the following naming patterns:

* ``$key_count_distinct_$nonkey_$days_days``
* ``$key_column_row_count_$days_days``
* ``$key_percentile_count_distinct_$nonkey_$days_days``
* ``$key_percentile_column_row_count_$days_days``

where

* ``$key`` is the ``key_columm`` in the ranking table
* ``$nonkey`` is the additional column name from the intermediate table
* ``$days`` is the day range for which the value was calculated, typically specified by the ``lookback`` setting in the configuration file as part of the dependency on the intermediate table as defined for the ranking table
* ``_percentile`` is added for all columns that contain percentile data

The configuration of a ranked table is defined in the :ref:`configure-entity-attribute-sources` section of the configuration file and is similar to:

.. code-block:: yaml

   ranking_table_name:
     generate: create_rank_features
     udf_source: LIBRARY
     key_columns: ['key_column']
     attribute_columns: ['column_name']
     type: TYPE
     depends_on: [{'datasource': 'intermediate_table_name',
                   'phase': 'phase_name',
                   'lookback': integer}]

where

* ``generate`` must be set to ``create_rank_features``
* ``udf_source`` must be set to ``LIBRARY``
* ``type`` must be set to ``PRE_MODELING_TABLE_ATTRIBUTE`` or ``MODELING_TABLE_ATTRIBUTE``, depending on the phase during which the table is generated
* ``key_columns`` must specify a single column that was specified in the ``key_columns`` setting for the intermediate table
* ``attribute_columns`` specifies one (or more) additional columns to add to the merged table
* ``depends_on`` specifies the name of the intermediate table as the ``datasource``, along with an engine application phase and an integer that specifies the number of days in the lookback window. The data provided by that data source is processed up to that application phase for the number of days specified, and then those results are used to provide data to the ranking table


Example: Exfiltration
++++++++++++++++++++++++++++++++++++++++++++++++++
It may be useful to track the number of total visits, number of unique user visits, and the number of unique host visits for the purpose of tracking exfiltration of data from inside the network.

Use the intermediate table to collect the data with three columns: ``registered_domain``, ``host``, and ``user`` that depends on transformed proxyweb data:

.. code-block:: yaml

   exfiltration_intermediate_table:
     generate: create_intermediate_rank_features
     udf_source: LIBRARY
     key_columns: ['registered_domain','user', 'host']
     attribute_columns: []
     type: PRE_MODELING_TABLE_ATTRIBUTE 
     path_add_date_suffix: True
     depends_on: [{'datasource': 'proxyweb', 'phase': 'transformed'}]

Use the ranked table to build a table using ``registered_domain`` as the key column using the data collected in the intermediate table for a seven day window:

.. code-block:: yaml

   exfiltration_ranking_table:
     generate: create_rank_features
     udf_source: LIBRARY
     key_columns: ['registered_domain']
     attribute_columns: ['column_name']
     type: PRE_MODELING_TABLE_ATTRIBUTE
     depends_on: [{'datasource': 'exfiltration_intermediate_table',
                   'phase': 'transformed',
                   'lookback': 7}]


Example: Percentiles
++++++++++++++++++++++++++++++++++++++++++++++++++
.. TODO: Pasted in, needs editing.

Use a generated entity attribute table with columns for percentile by row count and number of distinct users for every ``user_agent`` encountered.

To configure a generated attribute table track percentiles for user agents:

#. Add the ``user_agent_percentile_intermediate`` and ``user_agent_percentile`` groups of settings to the :ref:`configure-entity-attribute-sources` section of the configuration file:

   .. code-block:: yaml

      entity_attribute_sources:
        user_agent_percentile_intermediate:
          generate: 'create_intermediate_rank_features'
          udf_source: 'LIBRARY'
          key_columns: ["user_agent"]
          attribute_columns: []
          type: 'PRE_MODELING_TABLE_ATTRIBUTE'
          path_add_date_suffix: True
          depends_on:
            - datasource: proxyweb
              phase: standardized

        user_agent_percentile:
          generate: 'create_rank_features'
          udf_source: 'LIBRARY'
          key_columns: ["user_agent"]
          attribute_columns:
            - 'user_agent_row_count_7_days_percentile'
            - 'user_agent_count_distinct_user_7_days'
          type: 'PRE_MODELING_TABLE_ATTRIBUTE'
          path_add_date_suffix: True
          depends_on:
            - dataset: user_agent_percentile_intermediate
              stage: generated
              lookback: 7

#. Run the :ref:`command-drive` command. This will generate an entity attribute table with a ``user_agent_percentile`` column for each date the **drive** command is run.

#. Run the :ref:`command-drive` command. This will join the generated entity attribute table during the **transform** phase. The ``user_agent`` column is the join key.

.. 
.. removed; previously step 3, but needs updating for blocks
.. 
.. #. Update the ``attribute_source_name`` setting in the :ref:`configure-transform` section of the configuration file:
.. 
..    .. code-block:: yaml
.. 
..       sources:
..         proxyweb:
..           transform:
..             additional_attribute:
..               attribute_source_name: user_agent_percentile
.. 





Example: Generated
++++++++++++++++++++++++++++++++++++++++++++++++++
.. TODO: Pasted in, needs editing.

An entity attribute table may be generated from an entity attribute table.

To generate an entity attribute table from an entity attribute table:

#. Add the following **url_regdomain_percentile_intermediate** group of settings to the :ref:`configure-entity-attribute-sources` section of the configuration file:

   .. code-block:: yaml

      entity_attribute_sources:
        url_regdomain_percentile_intermediate:
          generate: create_intermediate_rank_features
          udf_source: LIBRARY
          key_columns: ['url_regdomain', 'hostname']
          attribute_columns: []
          type: PRE_MODELING_TABLE_ATTRIBUTE
          path_add_date_suffix: True
          depends_on:
            - datasource: 'proxyweb'
              phase: 'transformed'

#. Add a second group of settings that generates an entity attribute table based on the lookback window of the data generated in **url_regdomain_percentile_intermediate**:

   .. code-block:: yaml

      url_regdomain_7_days_percentile:
        generate: create_rank_features
        udf_source: LIBRARY
        key_columns: ['url_regdomain']
        attribute_columns: ['url_regdomain_count_distinct_hostname_7_days_percentile']
        type: PRE_MODELING_TABLE_ATTRIBUTE
        path_add_date_suffix: True
        depends_on:
          - datasource: 'url_regdomain_percentile_intermediate'
            phase: 'generated'
            lookback: 7

   Ensure that ``datasource`` specifies the name of the entity attribute table from which the data for this entity attribute table is generated.


.. _model-data-sessionize-table:

Sessionize Table
--------------------------------------------------
.. include:: ../../includes/entity_attribute_table_cse_sessionize.rst

Sessionize tables are configured using the following steps:

#. Verify dependencies, such as ensuring an appropriate data source is running through the engine up to the correct application phase.
#. Configure a sessionize table that contains all of the columns necessary to generate the data.

The configuration of a sessionize table is defined in the :ref:`configure-entity-attribute-sources` section of the configuration file and is similar to:

.. code-block:: yaml

   sessionize_table_name:
     generate: sessionize
     udf_source: LIBRARY
     key_columns: ['key_column']
     attribute_columns: ['publication_ratio', 'consumption_ratio']
     type: TYPE
     depends_on: [{'datasource': 'source_name', 'phase': 'phase_name'}]


Example: Proxy
++++++++++++++++++++++++++++++++++++++++++++++++++
.. TODO: Pasted in, needs editing.

Given an input table with the columns ``hostname``, ``url_domain``, ``url`` and ``timestamp``, use a generated entity attribute table to compute session statistics per hostname. A session is defined as an interaction with a particular domain, such as www.google.com, over consecutive 30-minute time windows.

The engine computes aggregates for the input data sources listed in the **depends_on** setting in the :ref:`configure-entity-attribute-sources` section of the configuration file.

The additional columns defined by the **attribute_columns** setting are joined back into the entity attribute table. To sessionize proxy data, these columns must be listed in the form:

.. code-block:: yaml

   <data_source>_<session>_<aggregate>

where the value of ``<aggregate>`` depends on the value of ``<session>``, which may be one of:

**time_until_next_session**
   Time from the current session to the start of the next session within the same day. The value for the last session in the day is set to ``None``. Possible <aggregate> values are ``avg``, ``max``, ``sum``, and ``stddev``.

**session_length**
   The number of contiguous 30-minute windows for which this session should last. For example, if a particular host accessed URLs under Google at 12:01 PM, 12:31 PM, and 1:15 PM, that would make it a session length of 3, corresponding to the 30-minute windows of 12 PM to 12:30 PM, 12:30 PM to 1:00 PM and 1:00 PM to 1:30 PM. Possible <aggregate> values are ``avg``, ``max``, ``min``, ``stddev``.

**url_depth_diff**
   Within a particular session, the URL depth difference of the various URLs accessed for that domain. The ``<aggregate>`` value must be ``max``.

To configure a generated attribute table to sessionize proxy data:

#. Add the ``sessionize_proxy`` group of settings to the :ref:`configure-entity-attribute-sources` section of the configuration file:

   .. code-block:: yaml

      entity_attribute_sources:
        sessionize_proxy:
          generate: 'sessionize'
          udf_source: 'LIBRARY'
          key_columns: ['hostname']
          attribute_columns:
            - 'bluecoat_proxy_session_length_sum'
            - 'bluecoat_proxy_time_until_next_session_avg'
            - 'bluecoat_proxy_url_depth_diff_max'
          type: 'PRE_MODELING_TABLE_ATTRIBUTE'
          path_add_date_suffix: True
          depends_on:
            - datasource: proxyweb
              phase: standardized

#. Run the :ref:`command-drive` command. This will generate an entity attribute table with columns for ``bluecoat_proxy_session_length_sum``, ``bluecoat_proxy_time_until_next_session_avg``, and ``bluecoat_proxy_url_depth_diff_max`` for each date the **drive** command is run.

#. Run the :ref:`command-drive` command. This will join the generated entity attribute table during the **transform** phase. The ``hostname`` column is the join key and must contain only a single entry.

.. 
.. was step 3, but needs updating for blocks
.. 
.. #. Update the ``attribute_source_name`` setting in the :ref:`configure-transform` section of the configuration file:
.. 
..    .. code-block:: yaml
.. 
..       sources:
..         proxyweb:
..           transform:
..             additional_attribute:
..               attribute_source_name: sessionize_proxy
.. 




.. _model-data-sharing-table:

Sharing Table
--------------------------------------------------
.. include:: ../../includes/entity_attribute_table_cse_sharing.rst

Sharing tables are configured using the following steps:

#. Identify the key column from which publication and consumption ratios are generated.
#. Verify dependencies, such as ensuring an appropriate data source is running through the engine up to the correct application phase.
#. Configure a sharing table that contains all of the columns necessary to generate publication and consumption ratios.

The configuration of a sharing table is defined in the :ref:`configure-entity-attribute-sources` section of the configuration file and is similar to:

.. code-block:: yaml

   sharing_features_table_name:
     generate: create_sharing_features
     udf_source: LIBRARY
     key_columns: ['key_column']
     attribute_columns: ['publication_ratio', 'consumption_ratio']
     type: TYPE
     depends_on: [{'datasource': 'source_name', 'phase': 'phase_name'}]

where

* ``generate`` must be set to ``create_sharing_features``
* ``udf_source`` must be set to ``LIBRARY``
* ``type`` must be set to ``PRE_MODELING_TABLE_ATTRIBUTE`` or ``MODELING_TABLE_ATTRIBUTE``, depending on the phase during which the table is generated
* ``key_columns`` specifies one (or more) columns from which the sharing table will calculates publication and consumption ratios
* ``attribute_columns`` must specify ``publication_ratio`` and ``consumption_ratio``
* ``depends_on`` specifies a data source and an engine application phase. The data provided by that data source is processed up to that application phase, and then those results are used to provide data to the sharing table


Example: Total Visits
++++++++++++++++++++++++++++++++++++++++++++++++++
It may be useful to track the number of total visits, the number of unique user visits, and the number of unique host visits for the purpose of helping to identify exfiltration of data from inside the network.

Use a sharing table to track publication and consumption ratios for host names and URLs for registered domains:

.. code-block:: yaml

    sharing_features:
        generate: create_sharing_features
        udf_source: LIBRARY
        key_columns: ['hostname', 'url_regdomain']
        attribute_columns:
          - publication_ratio
          - consumption_ratio
        type: PRE_MODELING_TABLE_ATTRIBUTE
        depends_on: [{'datasource': 'proxyweb', 'phase': 'standardized'}]


Example: Sent, Received
++++++++++++++++++++++++++++++++++++++++++++++++++
.. TODO: Pasted in, needs editing.

Use a generated entity attribute table to compute the ratio between bytes sent out to a particular domain and bytes received back. Bytes sent divided by bytes received is called ``publication_ratio``. The reciprocal (bytes received back) is called ``consumption_ratio``. Depending on the ``key_columns`` specified, these ratios are calcuated for either every ``user`` and ``url_regdomain`` pair or for every ``hostname`` and ``url_regdomain`` pair.

To configure a generated attribute table to compute the ratio between bytes sent and bytes received:

#. Determine if the ratio for bytes sent and recieved is calcuated for each user or for each hostname.

#. If bytes sent and recieved is calcuated for each user, add the following **sharing_features** group of settings to the :ref:`configure-entity-attribute-sources` section of the configuration file:

   .. code-block:: yaml

      entity_attribute_sources:
        sharing_features:
          generate: 'create_sharing_features'
          udf_source: 'LIBRARY'
          key_columns: ['user', 'url_regdomain']
          attribute_columns:
            - 'publication_ratio'
            - 'consumption_ratio'
          type: 'PRE_MODELING_TABLE_ATTRIBUTE'
          path_add_date_suffix: True
          depends_on:
            - datasource: proxyweb
              phase: standardized

#. If bytes sent and recieved is calcuated for each hostname, add the following **sharing_features** group of settings to the :ref:`configure-entity-attribute-sources` section of the configuration file:

   .. code-block:: yaml

      entity_attribute_sources:
        sharing_features:
          generate: 'create_sharing_features'
          udf_source: 'LIBRARY'
          key_columns: ['hostname', 'url_regdomain']
          attribute_columns:
            - 'publication_ratio'
            - 'consumption_ratio'
          type: 'PRE_MODELING_TABLE_ATTRIBUTE'
          path_add_date_suffix: True
          depends_on:
            - datasource: proxyweb
              phase: standardized

#. Run the :ref:`command-drive` command. This will generate an entity attribute table with the key and attribute columns specified in the configuration file for each date the **drive** command is run.

#. Run the :ref:`command-drive` command. This will join the generated entity attribute table during the **transform** phase, using either the ``user`` and ``url_regdomain`` columns **or** or the ``hostname`` and ``url_regdomain`` columns as the join key, depending on the key columns specified.

.. 
.. was step 3, update for blocks
.. 
.. #. Update the **attribute_source_name** setting in the :ref:`configure-transform` section of the configuration file:
.. 
..    .. code-block:: yaml
.. 
..       sources:
..         proxyweb:
..           transform:
..             additional_attribute:
..               attribute_source_name: create_sharing_features
.. 


.. 
.. .. _model-data-learn-models:
.. 
.. Learn Models
.. ==================================================
.. .. TODO: Is this the right section?
.. 
.. placeholder
.. 
.. 
.. .. _model-data-predict-results:
.. 
.. Predict Results
.. ==================================================.. 
.. Use a custom function during the **predict** phase of the application workflow to customize how the engine makes predictions. This custom function should take the engine configuration object with the following arguments, and then return a DataTable:
.. 
.. **config**
..    APT configuration object (the Python representation of the configuration file).
.. 
.. **models**
..    A dictionary that maps the model's target column to the model itself. This is the same format returned by the modeling UDF.
.. 
.. **table**
..    A DataTable that contains the data to score.
.. 
.. **entity**
..    The entity being modeled. Accepted values are either "user" or "host".
.. 
.. After the custom function is added to the customization package, the **function** setting in the :ref:`configure-predict` section of the configuration file must specify the name of this function:
.. 
.. .. code-block:: yaml
.. 
..    predict:
..      function: 'function_name'
.. 


.. 
.. .. _model-data-results:
.. 
.. Postprocess (Results)
.. ==================================================
.. .. TODO: Is this the right section?
.. 
.. placeholder
.. 


.. 
.. .. _model-data-custom-phases:
.. 
.. Custom Phases
.. ==================================================
.. .. TODO: Not sure this is a true "modeling" strategy, but here for now ... and badly needs editing.
.. 
.. The **modeling** phase has two options for user-defined functions:
.. 
.. * A user-defined function specified by the **experimentation** setting in the configuration file that completely overrides the **learn_models**, **predict**, and **postprocessing** phases when learning a model, scoring, it, and then generating the results.
.. * A user-defined function specified by the **function** setting in the configuration file is run at the end of the **learn_model** phase to modify the standard modeling approach.
.. 
.. Override Phases
.. --------------------------------------------------
.. A user-defined function can completely override the **learn_models**, **predict**, and **postprocessing** phases when learning a model, scoring, it, and then generating the results. Define a function that takes the configuration object and the following arguments, and returns a DataTable (not a ``PredictionTable``):
.. 
.. **datasets**
..    A list of the data sources to use.
.. 
.. **train**
..    The DataTable to use for learning the model.
.. 
.. **tune**
..    The DataTable to use for learning the model.
.. 
.. **test**
..    The DataTable to use for testing the model.
.. 
.. **config**
..    The configuration object (the Python representation of the configuration file).
.. 
.. **connection**
..    The named connection for the storage cluster where model output will be stored.
.. 
.. **out_path**
..    The path to the directory on the cluster where output will be stored.
.. 
.. The result of running the function should be the appearance of one or more result files in the output directory on the output connection.
.. 
.. The **experimentation** setting in the :ref:`configure-modeling` section of the configuration file must specify the name of this function:
.. 
.. .. code-block:: yaml
.. 
..    modeling:
..      ...
..     experimentation: 'function_name'
..      ...
.. 
.. 
.. After learn_models
.. --------------------------------------------------
.. A user-defined function can be run at the end of the **learn_model** phase to modify the standard modeling approach. Define a function that takes the configuration object and the following arguments, and returns a collection of models in the form of a dictionary that maps the name of the model's target column to the model itself:
.. 
.. **modeling_table**
..    The DataTable to use for learning the model.
.. 
.. **config**
..    The configuration object (the Python representation of the configuration file).
.. 
.. **entity**
..    The entity being modeled. Accepted values are either "user" or "host".
.. 
.. The **function** setting in the :ref:`configure-modeling` section of the configuration file must specify the name of this function:
.. 
.. .. code-block:: yaml
.. 
..    modeling:
..      ...
..      function: 'function_name'
..      ...
.. 







.. _model-data-improve-model-formatting:

Improve Model Formatting
==================================================
Models should be improved as feedback is identified, such as improving anomaly details, formatting, and so on.

.. TODO: Total dodge ^^ needs a more better intro.


.. 
.. see import_results_feedback.py
.. 
.. """
.. Module for importing results feedback.
.. 
.. Note: you cannot specify multiple different tag values per (entity id, %reason_code%_tags)
.. across all days combined. The import_results_feedback will fail if it finds such rows.
.. 
.. The manual workflow for providing feedback is as follows.
.. 
.. 1. Update entity_attribute_sources to have a source to save feedback tags (one source per entity).
..    The key_columns for such an attribute source should be the entity (like user or hostname).
.. 2. Reference the entity_attribute_sources in multi_day_features section of the config.
.. 3. After step #1 and #2 the multi day modeling table will join the appropriate tag EAT if present.
.. 4. Run the app and take the details.csv.
.. 5. Add new columns to details.csv where column name is the name of the reason code (or
..    referred to as indicator or behavior target column) + "_tags"
.. 6. To add a tag for an entity locate a row with that entity id (user or hostname), go to
..    the cell in the %reason_code%_tags column and add the tag.
.. 7. Save the details.csv
.. 8. Run the app to invoke the import_results_feedback command. Provide the location to
..    details.csv, the entity name (user or hostname), and the entity_attribute_sources name to use.
.. 9. Now the feedback will be used for any learn_model step from here on. If it is desired to
..    re-learn model on existing data with new feedback, delete the old models, and run the app
..    from learn_model onwards.
.. """
.. 


.. _model-data-format-anomaly-details:

Format Anomaly Details
--------------------------------------------------
By default, the anomaly details are presented in a detailed, but somewhat unfriendly format:

.. code-block:: none

   <target>=<actual> (expected <predicted> because <reason>; ...) NLL=<score>

and with the following format for each ``<reason>``:

.. code-block:: none

   <input column> is <input value> [influence%]

where

* ``<target>`` is the measurement column name.
* ``<actual>`` is the measured value.
* ``<predicted> is the value predicted by the model in ``predicted_<target>``.
* ``<score>`` is the negative log-likelihood (NLL) value in ``nll_<target>``.
* ``<reason>`` is a semicolon-delimited list of reasons for the predicted value, starting with the strongest reason. Each reason is a column and value.
* ``<influence>`` is the relative strength and direction of the reason. Positive influences pushes the prediction upwards and negative influences pushes the prediction downward.

For example (formatted for readability):

.. code-block:: none

   bytes_out=500M (expected 25M because IS_WEEKEND is False [-50%]; 
                   DEPARTMENT is HR [-25%]) NLL=12345


If **show_anomaly_details** is set to ``False`` in the :ref:`configure-results` section of the configuration file, the format is simpler:

.. code-block:: none

   <target>=<actual> (expected <predicted>) NLL=<score>

For example:

.. code-block:: none

   proxyweb_bytes_out_sum=500M (expected 25M) NLL=12345


Example: Format Details
++++++++++++++++++++++++++++++++++++++++++++++++++
The following code formats anomaly details reasons to apply a scale of 0-100 to the surprise score, and then remove the influence information from the reason list:

.. code:: python

   def get_reason_formatter(cfg, **kwargs):
     return CustomReasonFormatter(**kwargs)
   
     def compute_surprise(self, nll_score):
       nll_score = max(nll_score, 0.0)
       prob = math.exp(-1.0 * nll_score)
       return 100 * (1.0 - prob)
   
     class CustomReasonFormatter(MeasurementModelFormatter):
       VERSION = '1.0.0.0'
       COMPAT_VERSION = '1.0.0.0'
       _SAVED_FIELDS = MeasurementModelFormatter._SAVED_FIELDS
   
     def __init__(self, **kwargs):
       kwargs['show_strength'] = False
       super(CustomReasonFormatter, self).__init__(**kwargs)

The **details_formatter_factory_function** setting in the :ref:`configure-results` section of the configuration file must specify the name of this function:

.. code-block:: yaml

   results:
     ...
     details_formatter_factory_function: get_reason_formatter
     formatter_factory_function: get_top_formatter
     ...


Example: Show Details
++++++++++++++++++++++++++++++++++++++++++++++++++
.. include:: ../../includes_config/results_show_anomaly_details.rst

From the perspective of custom functions, when **show_anomaly_details** is ``True``, the anomaly details are formatted by composing two formatting classes. Either piece, or both together, can be customized.

The top-level structure is generated by ``crepe.apt.reasons.DrillDownFormatter``::

.. code-block:: none

   <target>=<actual> (expected <predicted> because <reason list>) NLL=<score>

To change the top-level structure, sub-class ``DrillDownFormatter`` in a user-defined function, and then specify that function name as the value of the **formatter_factory_function** setting in the configuration file.

Each reason in the ``<reason list>`` is generated by ``crepe.apt.reasons.MeasurementModelFormatter``. To change reason formatting, sub-class ``MeasurementModelFormatter`` in a user-defined function, and then specify that function name as the value of the **details_formatter_factory_function** setting in the configuration file.

When **show_anomaly_details** is ``False``, specify a function name for **formatter_factory_function** only. This function should return a sub-class of ``crepe.apt.reasons.SecurityFormatter``.

When customizations exist for anomaly details, update the :ref:`configure-results` section of the configuration file to specify the correct details:

.. code-block:: yaml

   results:
     ...
     details_formatter_factory_function: get_reason_formatter
     formatter_factory_function: get_top_formatter
     show_anomaly_details: True     # or False
     ...


.. _model-data-format-column-names:

Format Column Names
--------------------------------------------------
By default column names are shown in the output. For example:

.. code-block:: none

   percentile_50_proxyweb_bytes_out_sum_7_day

Many column names are long and may be unintuitive or difficult to read. For longer columns, use an alias---a nickname---as a way to shorten the column name in the output for anomaly details prior to showing the output to analysts.

Use the **nicknames** setting in the :ref:`configure-results` section of the configuration file to assign more readable strings to any column. When assigned, the nickname value is shown in the output instead of the actual column name.

.. include:: ../../includes_config/results_nicknames.rst


.. _model-data-format-lookbacks:

Format Lookbacks
--------------------------------------------------

.. TODO: Need a general overview of lookbacks/lookback windows, and then lead that into specific discussions per setting? Below is literally a copy of the configure.rst topic for multi-day section. What is a lookback? Why does it exist? How do you configure it?


**lookback** (list of int, optional)
   A list of integers that indicate how many days to include in each time window. Default value:  ``[3]``.

**lookback_cumulative** (list of int, optional)
   A list of integers---``[int, int, int, ...]``---that indicate the number of days to include in a time window for a cumulative aggregation, inclusive of the current day. The first window is the target window and subsequent windows are interpreted as offsets from the target window. For example, ``[7, 7, 14]`` means the target window is the last 7 days (including today), the first input window covers days 8-14, and the second window covers days 8-21. Default value: ``[7, 7, 14]``.

**lookback_historical** (list of int, optional)
   A list of integers that indicate how many days to include in each time window for a historical aggregation. Default value:  ``[3]``.

**lookback_internal_neighbors** (list of int, optional)
   A list of integers that indicate the number of days to include in a time window for a "number of internal neighbors" aggregation. Default value:  ``[3]``.

   Each value specifies the number of days in the lookback windows for both recent (target) and historical lookback (model input) data. For example, the default value will produce two columns of lookback data:

   #. 3 days starting from the current day, inclusive (target)
   #. 3 days starting from 3 days ago (model input)

   and a value of ``[3, 7]`` means that four columns for lookback windows will be created:

   #. 3 days starting from the current day, inclusive of the current day (target)
   #. 3 days starting from 3 days ago (model input)
   #. 7 days starting from the current day, inclusive of the current day (target)
   #. 7 days starting from 7 days ago (model input)

.. TODO: The 2nd paragraph and the two lists need to be reviewed by Tom. They are based on the description for ReplaceListsByCountersUDF that is found in transforms_server_client.py. CR-11581

.. TODO: Find out what "number of internal neighbors" aggregation means. Also, see transforms_server_client.py, NeighborFeatures object for lots of potential info here. There is a list of new_base_columns that maybe should be in the docs? "If value = something, then new columns added, whatnots." There's a lot of info in there.



.. _model-data-format-results-output:

Format Results Output
--------------------------------------------------
The formatting templates for anomaly details can be customized to better meet the needs of security analysts. The customization package supports formatting results output and anomaly details. The :ref:`configure-results` section of the configuration file has settings to enable both types of custom functions, as well as for specifying if anomaly details show details for individual measurement models.

The sections below show how to format results output and anomaly details with user-defined functions that are configured to run during the **results** application phase to:

* Change the NLL value to a surprise score
* Apply a scale of 0-100 to that score
* Remove the influence information from the reason list
* Determine if individual measurement models are shown in the results

The following example shows how to format results output. It defines a custom function named ``get_top_formatter``:

.. code:: python

   from crepe.apt.reasons import (
     format_value_target,
     DrillDownFormatter,
     MeasurementModelFormatter)
   
   def get_top_formatter(cfg, **kwargs):
     return CustomTopFormatter(**kwargs)
   
   class CustomTopFormatter(DrillDownFormatter):
     VERSION = '1.0.0.0'
     COMPAT_VERSION = '1.0.0.0'
     _SAVED_FIELDS = DrillDownFormatter._SAVED_FIELDS
   
     def __init__(self, **kwargs):
       super(CustomTopFormatter, self).__init__(**kwargs)
   
     def explain_value_feature(self, info):
       target_col = self.get_measurement_column(info.base_name)
       pred_col = 'predicted_' + target_col
       nll_score = info.feature_value
       actual = self.get_column_value(target_col)
       predicted = self.get_column_value(pred_col)
       submodel_reason_string = self.get_drilldown_reasons(target_col)
       code = ("{target}={actual} (expected {predicted} because {submodel_reason})"
               "surprise={score:0.1f}%"
               .format(target=self.get_alias(target_col),
                 actual=format_value_target(actual, target_col),
                 predicted=format_value_target(predicted, target_col),
                 submodel_reason=submodel_reason_string,
                 score=self.compute_surprise(nll_score)))
     return code

The **formatter_factory_function** setting in the :ref:`configure-results` section of the configuration file must specify the name of this function:

.. code-block:: yaml

   results:
     ...
     formatter_factory_function: get_top_formatter
     ...



.. _model-data-apply-feedback:

Apply Feedback
==================================================
.. TODO: This section might be better somewhere else. It's properly positioned AFTER the modeling is done.

Use the **import-results-feedback** command to apply feedback to model predictions output by adding columns to the anomaly list that are associated with free-form tags. This feedback is used to refine the model during the **learn_models** application phase.

.. warning:: .. include:: ../../includes/cmd_import_results_feedback_tags.rst

To apply feedback to a model, and then re-learn it:

#. Add a data source to the :ref:`configure-entity-attribute-sources` section of the configuration file for a generated entity attribute table. This table will be the location in which feedback tags are saved. Set **key_columns** to the entity (such as ``hostname`` or ``user``) and set **path_add_date_suffix** to ``False``:

   .. code-block:: yaml

      entity_attribute_sources:
        <table_name>:
          ...
          key_columns: ['hostname']
          path_add_date_suffix: False

#. In the :ref:`configure-multi-day-features` section of the configuration file enter the ``<table_name>`` from the previous step as the value of **attribute_source_name** under **additional_attributes**, and then set **join** to ``True``:

   .. code-block:: yaml

      multi_day_features:
        additional_attribute:
          - attribute_source_name: '<table_name>'
            join: True

   This will join the multi-day modeling table with the entity attribute table in which the feedback tags are created.

#. Run the :ref:`command-drive` command through at least the **postprocess** phase.

#. In the anomaly list file, make the following changes:

   Add new columns to the anomaly list, where each column name is the name of the anomaly detail + ``_tags``.

   Within the ``<anomaly_detail>_tags`` columns, locate the row that is associated with a particular entity ID and add a string to serve as a tag.

   Save the anomaly list file. If will be uploaded using the named connection to the storage cluster, as specified by the **default_configuration** setting in the configuration file.

#. Run **import-results-feedback** command. If the anomaly list is on the local filesystem, set the ``USE_SINGLE_PROC`` environment variable to ``True`` to run the engine in single process mode.

#. Set the **use_feedback_columns** in the :ref:`configure-modeling` section of the configuration file to ``True`` to use these feedback tags during the **learn_models** phase for all subsequent runs.

#. Delete old models, and then re-run the :ref:`command-drive` command to re-learn the model on existing data with new feedback.

.. 
.. .. _model-data-improve-model-quality:
.. 
.. Improve Model Quality
.. ==================================================
.. 
.. .. TODO: This should be a discussion of results quality, check-quality (cmd), etc.
.. 
.. .. warning:: The following content is PASTED IN from the commands.rst file, specifically the example for the check-quality command. It's here as a placeholder.
.. 
.. 
.. The following example shows the output of a simulation run via the **check_quality** command:
.. 
.. .. code-block:: console
.. 
..    Assumptions: QualitySimConfig(fraction_abnormal=0.001,
..                                  probability_obvious=0.25,
..                                  obvious_negprob=0.999,
..                                  hidden_negprob=0.6,
..                                  days_in_TC_window=14)
..    ENTITY= host ; num_unique= 3156
..    
..      phi(internal_neighbors_1_7_days_count) = 0.823348
..    
..      phi(cumulative_proxyweb_internal_to_bytes_in_sum_7_day) = 0.682995
..      phi(cumulative_proxyweb_internal_high_consumption_ratio_to_bytes_in_sum_7_day) = 0.863176
..      phi(cumulative_many_internal_neighbors_to_proxyweb_internal_to_bytes_in_sum_7_day) = 0.412913
..      phi(cumulative_many_internal_neighbors_to_proxyweb_internal_high_consumption_ratio_to_bytes_in_sum_7_day) = 0.863176
..    
..      phi(cumulative_proxyweb_external_high_publication_ratio_to_bytes_out_sum_7_day) = 0.420420
..      phi(cumulative_proxyweb_external_exclusive_regdomain_to_bytes_out_sum_7_day) = 0.705518
..      phi(cumulative_proxyweb_external_exclusive_regdomain_high_publication_ratio_to_bytes_out_sum_7_day) = 0.748061
..    
..        ---------------
..        combo_phi(3) =      0.823348
..        combo_phi(4) =      0.745598
..        combo_phi(5) =      0.529885
..        ---------------
..        Pr(a > b) =     0.996164
..        ---------------
..        Pr(0 / 25 worthwhile) =     0.000789    (  0.00% recall)
..        Pr(1 / 25 worthwhile) =     0.015661    ( 25.00% recall)
..        Pr(2 / 25 worthwhile) =     0.116453    ( 50.00% recall)
..        Pr(3 / 25 worthwhile) =     0.385600    ( 75.00% recall)
..        Pr(4 / 25 worthwhile) =     0.481497    (100.00% recall)
..    
..        Expect 3.3 / 25 worthwhile on average   ( 83.28% recall)
.. 
.. 
.. where:
.. 
.. **Assumptions**
..    The listed values are defined in the config.yml file. These are the assumptions that determine the outcome of the simulation. For example, that 1 in 2000 entities are abnormal (``fraction_abnormal``) or that a threat window should be 14 days.
.. 
.. **ENTITY**
..    The name of the entity against which the simulation is run.
.. 
.. **num_unique**
..    The number of unique entities examined during the simulation.
.. 
.. **phi()**
..    The results for a particular measurement for 7 days, inclusive of the current day. This is a rolling time period and always includes the current day and the previous six days. The output above shows two very low scores:
.. 
..    .. code-block:: console
.. 
..       phi(cumulative_many_internal_neighbors_to_proxyweb_internal_to_bytes_in_sum_7_day) = 0.412913
..       phi(cumulative_proxyweb_external_high_publication_ratio_to_bytes_out_sum_7_day) = 0.420420
.. 
..    These results indicate a high amout of traffic **to** one (or more) entities during stage 4, and then a high amount of traffic **from** one (or more) entities during stage 5.
.. 
.. **combo_phi()**
..    The results for all ``phi()`` measurements for a particular stage for the current day. The output above shows one very low score:
.. 
..    .. code-block:: console
.. 
..       combo_phi(5) =      0.529885
.. 
.. **Pr(a > b)**
..    The probability that an abnormal entity is ranked above a benign entity. Stated differently, the probability that any given abnormal entity may be an actual threat.
.. 
..    In the example output, there are 3156 entities. Each of these entities was compared to every other entity, which means nearly ten million comparisons were made. The resulting value is not just a simple percentage of total entities, as that would imply ~12 abnormal entities when there are only 4. The value of ``Pr(a > b)`` indicates a 99.6% chance that all of the identified abnormal entities will be discovered within the top K threat cases.
.. 
..    Given that there are two low scores in stages 3 and 4 (as measured by ``phi()``), and given that the combined score for stage 5 is also low, this suggests that one (or more) abnormal entities is an actual threat. The result for ``Pr(a > b)`` in the example indicates that the odds of discovering the actual threat within the top 25 threat cases is very high.
.. 
.. **Pr(N / 25)**
..    The probability that ``N`` abnormal entities will be discovered within the top K threat cases. The results indicate that all four identified abnormal entities will be discovered within the top K threat cases. In this example, results for ``phi()`` in stages 4 and 5 and results for ``combo_phi(5)`` both indicate an actual threat may be present. The results of ``Pr(N / 25)`` indicate that all four of the identified abnormal threats should be discovered within the top 25 threat cases.
.. 
.. **Expect 3.3 / 25 worthwhile on average   ( 83.28% recall )**
..    The expected probability that ``N`` abnormal entities (from ``Pr(N / 25)``) will be discovered in the top K threat cases. The lower recall value indicates that the actual results of the simulation performed better than expected.
