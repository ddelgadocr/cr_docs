.. 
.. versive, primary, security engine
.. 

==================================================
Site Map
==================================================

.. include:: ../../includes_terms/term_vse.rst

This is the documentation for |vse|, including

* Important concepts, workflow, and patterns
* How to deploy and configure the engine
* How to build the data processing pipeline
* How to run, tune, and understand models
* How to understand the results
* Reference topics for commands, settings, extension APIs, blocks, and more

Introduction
==================================================

:doc:`About the Versive Security Engine </vse>` | :doc:`Glossary </glossary>` | :doc:`Release Notes </release_notes>`

**Campaign Stages**: :doc:`About Campaign Stages </stages>` | :ref:`stages-traditional-vs-versive` | :ref:`stages-planning` | :ref:`stages-access` | :ref:`stages-recon` | :ref:`stages-data-collection` | :ref:`stages-data-exfiltration` | :ref:`stages-recurring`

**Security Engine Workflow**: :doc:`About the Engine Workflow </workflow>` | :ref:`workflow-run-security-engine` | :ref:`workflow-drive` (command)

**Use Python** :doc:`Use Python with the Versive Platform <python>` | :ref:`python-module-help` **Import Modules**  :ref:`python-import-modules` **Python Modes** :ref:`python-modes` | :ref:`python-interactive-mode` | :ref:`python-batch-mode` | :ref:`python-ipython` | :ref:`python-extension-package-mode` **Recommendations** :ref:`python-checkpoint-often` | :ref:`python-column-hints` | :ref:`python-consistent-formatting` | :ref:`python-dates-as-strings` | :ref:`python-identify-information-leaks` | :ref:`python-learn-models-quickly` | :ref:`python-learning-curves` | :ref:`python-load-only-relevant-topics` | :ref:`python-print-statements` | :ref:`python-save-often` | :ref:`python-strptime-syntax` | :ref:`python-time-zones` | :ref:`python-validate-raw-data` **User-defined Functions** :ref:`python-user-defined-functions` | :ref:`python-missing-values` | :ref:`python-missing-values-parser-hints` | :ref:`python-missing-values-if-statements` | :ref:`python-lambda-expressions` | :ref:`python-logging-functions`


Deploy
==================================================

**Data Lake**: :doc:`About Data Lakes </deploy_data_lake>` | :ref:`deploy-data-lake-provision-machines` | :ref:`deploy-data-lake-setup-hadoop-services` | :ref:`deploy-data-lake-connect-logs-to-pipeline` | :ref:`deploy-data-lake-connect-data-to-application`

**Versive Security Engine**: :doc:`About the Versive Security Engine </deploy_vse>` | :ref:`deploy-vse-requirements` | :ref:`deploy-vse-preparation` | :ref:`deploy-vse-install` | :ref:`deploy-vse-connect-to-data-sources` | 

**Connect to Data Sources**: :doc:`About Data Source Connections </deploy_connect_data_sources>` | :ref:`deploy-connect-data-sources-requirements` | :ref:`deploy-connect-data-sources-amazon-s3` | :ref:`deploy-connect-data-sources-hdfs-keytab-file` | :ref:`deploy-connect-data-sources-hdfs-token`

**Threat Viewer Integrations** :doc:`About Integrations </integrations>` | :ref:`integration-kibana` | :ref:`integration-recorded-future` | :ref:`integration-splunk` | :ref:`integration-virustotal` | :ref:`integration-whois`

Process Data
==================================================

:doc:`About Processing Data </process_data>`

**Data Sources**: :ref:`About Data Source Types <process-data-sources>` | :ref:`process-data-source-dns` | :ref:`process-data-source-flow` | :ref:`process-data-source-proxy`

**About Blocks** :ref:`process-data-blocks` | :ref:`process-data-blocks-and-columns` | :ref:`process-data-block-graphs` | :ref:`process-data-blocks-configuration` | :ref:`process-data-custom-blocks`

**Parse Data with Regular Expressions**: :ref:`About Parsing Data <process-data-parse-data>` | :ref:`YAML Patterns File <process-data-yaml-patterns>` | :ref:`Common Regex Patterns <process-data-common-regex-patterns>` | :ref:`Regex Debugger <process-data-regex-debugger>`

**Multiday Values** :ref:`About Multi-day Values <process-data-transform-multiday>` | :ref:`process-data-multi-day-count-distinct` | :ref:`process-data-multi-day-crosstab` | :ref:`process-data-multi-day-sum`


**Blocks** :doc:`About Blocks <blocks>` | **Blocks Example**: :ref:`process-data-blocks-tutorial` | :ref:`process-data-blocks-tutorial-verify-data-source` | :ref:`process-data-blocks-tutorial-parse-proxy-data` | :ref:`process-data-blocks-tutorial-split-parsed-unparsed` | :ref:`process-data-blocks-tutorial-clean-up-ip-address` | :ref:`process-data-blocks-tutorial-clean-up-user-data` | :ref:`process-data-blocks-tutorial-add-columns` | :ref:`process-data-blocks-tutorial-group-by-source-ip` | :ref:`process-data-blocks-tutorial-sum-bytes-in-out` | :ref:`process-data-blocks-tutorial-create-lists` | :ref:`process-data-blocks-tutorial-verify-dependencies` | :ref:`process-data-blocks-tutorial-verify-output` | :ref:`process-data-blocks-tutorial-full-yaml-example` **Blocks Library**: :ref:`List of Blocks <blocks-library>`



.. 
.. **Scenarios** :ref:`process-data-scenario-convert-ms-to-seconds` | :ref:`process-data-scenario-convert-uri-to-string` | :ref:`process-data-scenario-remove-comments` | :ref:`process-data-scenario-bytes-in-bytes-out` | :ref:`process-data-scenario-high-publication-ratios` | :ref:`process-data-scenario-extract-os-from-header`
.. 

Defined Behaviors
==================================================

:doc:`About Defined Behaviors </define_behaviors>`

**Behaviors**: :ref:`About Behaviors <define-behaviors-about>` | :ref:`define-behaviors-statistics` | :ref:`define-behaviors-strength` | :ref:`define-behaviors-measurements` | :ref:`define-behaviors-anomalies`

**Activities**: :ref:`About Activities <define-behaviors-activities>` | :ref:`define-behaviors-activity-collected` | :ref:`define-behaviors-activity-published` | :ref:`define-behaviors-activity-requested`

**Detection Methods**: :ref:`About Detection Methods <define-behaviors-detection-methods>` | :ref:`define-behaviors-detection-method-a-record` | :ref:`define-behaviors-detection-method-aaaa-record` | :ref:`define-behaviors-detection-method-axfr-record` | :ref:`define-behaviors-detection-method-ftp` | :ref:`define-behaviors-detection-method-icmp` | :ref:`define-behaviors-detection-method-ports` | :ref:`define-behaviors-detection-method-ptr-record` | :ref:`define-behaviors-detection-method-snmp` | :ref:`define-behaviors-detection-method-proxy` | :ref:`define-behaviors-detection-method-udp`

**Units**: :ref:`About Units <define-behaviors-units>` | :ref:`define-behaviors-unit-bytes` | :ref:`define-behaviors-unit-domains` | :ref:`define-behaviors-unit-ip-address` | :ref:`define-behaviors-unit-ports` | :ref:`define-behaviors-unit-protocols`

**Stages**: :ref:`About Stages <define-behaviors-stages>` | :ref:`define-behaviors-stage-planning` | :ref:`define-behaviors-stage-access` | :ref:`define-behaviors-stage-recon` | :ref:`define-behaviors-stage-collect` | :ref:`define-behaviors-stage-exfil` | :ref:`define-behaviors-stage-ongoing`

Model Data
==================================================

:doc:`About Modeling Data </model_data>`

**Model Types**: :ref:`About Model Types <model-types>` | :ref:`model-types-baseline` | :ref:`model-types-measurement` | :ref:`model-types-combo`

**Model Strategies**: :ref:`About Model Strategies <model-strategies>` | :ref:`model-strategy-comp-cdf` | :ref:`model-strategy-top-k-entities`  | :ref:`model-strategy-custom-scoring`

**Model Results** :ref:`model-data-risk-scores` | :ref:`model-data-surprise-scores` | :ref:`model-data-noteworthy-scores` | :ref:`model-data-threat-case-levels`

**Table Types**: :ref:`About Table Types <model-data-tables>` | :ref:`model-data-static-table` | :ref:`model-data-generated-table` | :ref:`model-data-custom-table`

**Table Generators**: :ref:`About Table Generators <model-data-table-generators>` | :ref:`model-data-hostname-table` | :ref:`model-data-ranks` | :ref:`model-data-sessionize-table` | :ref:`model-data-sharing-table`

**Improve Model Formatting**: :ref:`About Model Formatting <model-data-improve-model-formatting>` | :ref:`model-data-format-anomaly-details` | :ref:`model-data-format-column-names` | :ref:`model-data-format-lookbacks` | :ref:`model-data-format-results-output` | :ref:`model-data-apply-feedback`

Review Threat Cases
==================================================

:doc:`About Reviewing Threat Cases </build_cases>`

**Threat Cases** :ref:`Review Threat Cases <build-cases-review>` **Recon Stage** :ref:`Recon Stage <build-cases-review-recon>` | :ref:`Recon Targets <build-cases-review-recon-targets>` | :ref:`Recon Findings List <build-cases-review-recon-findings-list>` | :ref:`Recon Finding Details <build-cases-review-recon-finding-details>` **Collection Stage**
:ref:`Collection Stage <build-cases-review-collect>` | :ref:`Collection Targets <build-cases-review-collect-targets>` | :ref:`Collection Findings List <build-cases-review-collect-findings-list>` | :ref:`Collection Finding Details <build-cases-review-collect-finding-details>` **Exfiltration Stage** :ref:`Exfiltration Stage <build-cases-review-exfil>` | :ref:`Exfiltration Targets <build-cases-review-exfil-targets>` | :ref:`Exfiltration Findings List <build-cases-review-exfil-findings-list>` | :ref:`Exfiltration Finding Details <build-cases-review-exfil-finding-details>`


**Adversary Campaign Indicators** :ref:`About adversary campaign indicators <build-cases-adversary-campaign-indicators>` | :ref:`Volume of data transferred between entities <build-cases-data-transfer-volumes>` | :ref:`Duration of sessions <build-cases-session-durations>` | :ref:`Number of neighbors <build-cases-number-of-neighbors>` | :ref:`Roles of clients and servers <build-cases-client-and-server-roles>` | :ref:`Sessions and protocols <build-cases-sessions-and-protocols>` | :ref:`Breadth versus depth <build-cases-breadth-vs-depth>` | :ref:`Client communication patterns <build-cases-communication-patterns>` | :ref:`Many short sessions <build-cases-many-short-sessions>` | :ref:`Large social circles <build-cases-large-social-circles>`

**Scoring Types** :ref:`build-cases-stage-scores` | :ref:`build-cases-surprise-scores` | :ref:`build-cases-risk-scores` | :ref:`build-cases-stage-scores`

**External Links and Queries** :doc:`integrations` | :ref:`integration-kibana` | :ref:`integration-recorded-future` | :ref:`integration-splunk` | :ref:`integration-virustotal` | :ref:`integration-whois`



Manage the Cluster
==================================================

**Multi-node cluster**: :doc:`About Managing Cluster Nodes </manage_cluster_nodes>` | :ref:`Add node to cluster <manage-cluster-nodes-add-node>` | :ref:`Change the primary node <manage-cluster-nodes-change-the-primary-node>` | :ref:`Cluster is unresponsive <manage-cluster-nodes-cluster-is-unresponsive>` | :ref:`Move nodes <manage-cluster-nodes-move-nodes>` | :ref:`Recreate the cluster <manage-cluster-nodes-recreate-cluster>` :ref:`Remove node from cluster <manage-cluster-nodes-remove-node>` | :ref:`Repair cluster <manage-cluster-nodes-repair-cluster>` | :doc:`View the cluster status portal </manage_cluster_status_portal>`

**Single-node cluster**: :doc:`View the cluster status portal </manage_cluster_status_portal>` | :doc:`crpkg </cmd_crpkg>`

**Cluster commands**: :ref:`cmd-cr-cluster-activate` | :ref:`cmd-cr-cluster-add` | :ref:`cmd-cr-cluster-create` | :ref:`cmd-cr-cluster-delete` | :ref:`cmd-cr-cluster-free` | :ref:`cmd-cr-cluster-getprop` | :ref:`cmd-cr-cluster-health` | :ref:`cmd-cr-cluster-join` | :ref:`cmd-cr-cluster-reset` | :ref:`cmd-cr-cluster-setprop` | :ref:`cmd-cr-cluster-status` | :ref:`cmd-cr-cluster-view` | :ref:`cmd-cr-connect-add` | :ref:`cmd-cr-connect-list` | :ref:`cmd-cr-connect-remove` | :ref:`cmd-cr-connect-show` | :ref:`cmd-cr-install-activate` | :ref:`cmd-cr-install-deactivate` | :ref:`cmd-cr-install-list` | :ref:`cmd-cr-install-rollback` | :ref:`cmd-cr-node-datadirectory` | :ref:`cmd-cr-node-hardreset` | :ref:`cmd-cr-node-ports` | :ref:`cmd-cr-node-pythonlink` | :ref:`cmd-cr-node-setuser` | :ref:`cmd-cr-service-list` | :ref:`cmd-cr-version`


Reference
==================================================

**Commands**: :doc:`About Commands </commands>` | :ref:`command-drive` | :ref:`command-list`

**Configuration settings**: :doc:`About config.yml </configure>` :ref:`Global settings <configure-global-settings>` | :ref:`configure-category-patterns` | :ref:`configure-drive` | :ref:`configure-entity-attribute-sources` | :ref:`configure-flighting` | :ref:`configure-generate-lookup-global` | :ref:`configure-generate-modeling` | :ref:`configure-modeling` | :ref:`configure-multi-day-features` | :ref:`configure-predict` | :ref:`configure-results` | :ref:`configure-sampling` | :ref:`configure-workspaces`

**Blocks Reference** :ref:`blocks-library-addcolumnbycategory` | :ref:`blocks-library-addconstantcolumn` | :ref:`blocks-library-addsubnetlabels` | :ref:`blocks-library-addurlfeatures` | :ref:`blocks-library-aggregate` | :ref:`blocks-library-backup` | :ref:`blocks-library-buildurl` | :ref:`blocks-library-calculatebytesratiosblock` | :ref:`blocks-library-calculatecolumnlength` | :ref:`blocks-library-categorize` | :ref:`blocks-library-categorizeport` | :ref:`blocks-library-categorizeprotocol` | :ref:`blocks-library-categorizetcpnotsnmp` | :ref:`blocks-library-cleanflowdata` | :ref:`blocks-library-collectflowstats` | :ref:`blocks-library-concatenate` | :ref:`blocks-library-convertstringtodatetime` | :ref:`blocks-library-copycolumn` | :ref:`blocks-library-deletecolumns` | :ref:`blocks-library-fillcolumn` | :ref:`blocks-library-filteremptyrows` | :ref:`blocks-library-join` | :ref:`blocks-library-lowercase` | :ref:`blocks-library-mergecategories` | :ref:`blocks-library-mergeentityscores` | :ref:`blocks-library-mergetables` | :ref:`blocks-library-parse` | :ref:`blocks-library-parseurl` | :ref:`blocks-library-partition` | :ref:`blocks-library-preaggregateflow` | :ref:`blocks-library-replacenonedates` | :ref:`blocks-library-sample` | :ref:`blocks-library-standardizeblock` | :ref:`blocks-library-standardizedates` | :ref:`blocks-library-standardizedomain` | :ref:`blocks-library-standardizehostname` | :ref:`blocks-library-standardizeipaddress` | :ref:`blocks-library-standardizereverseip` | :ref:`blocks-library-standardizeusername` | :ref:`blocks-library-summarizetable` | :ref:`blocks-library-validatecolumn` | :ref:`blocks-library-verifyemptycolumns`

**Extensions**: :doc:`About extensions.py </extensions>`

**Extension API**: :doc:`About the Extension API </api>` (functions are grouped by tables, models, checkpoints, custom logs, and platform below)

**Python and the Versive Platform** :doc:`About Using Python with the Versive Platform </python>` | :ref:`python-import-modules` | :ref:`python-modes` | :ref:`python-interactive-mode` | :ref:`python-batch-mode` | :ref:`python-ipython` | :ref:`python-extension-package-mode` | :ref:`python-recommendations` | :ref:`python-checkpoint-often` | :ref:`python-column-hints` | :ref:`python-consistent-formatting` | :ref:`python-dates-as-strings` | :ref:`python-identify-information-leaks` | :ref:`python-learn-models-quickly` | :ref:`python-learning-curves` | :ref:`python-load-only-relevant-topics` | :ref:`python-print-statements` | :ref:`python-save-often` | :ref:`python-strptime-syntax` | :ref:`python-time-zones` | :ref:`python-validate-raw-data` | :ref:`python-user-defined-functions` | :ref:`python-missing-values` | :ref:`python-missing-values-parser-hints` | :ref:`python-missing-values-if-statements` | :ref:`python-lambda-expressions` | :ref:`python-logging-functions` | :ref:`python-module-help`

**Table Functions**: :ref:`api-add-columns` | :ref:`api-add-context` | :ref:`api-add-context-from` | :ref:`api-aggregate` | :ref:`api-count-frequencies` | :ref:`api-count-rows` | :ref:`api-create-context-custom-op` | :ref:`api-create-context-op` | :ref:`api-create-custom-op` | :ref:`api-create-parser-hint-date` | :ref:`api-create-parser-hint-maxerr` | :ref:`api-create-parser-hint-missing` | :ref:`api-create-parser-hint-table-maxerr` | :ref:`api-delete-columns` | :ref:`api-flexi-categorize` | :ref:`api-generate-rows` | :ref:`api-get-column-descriptions` | :ref:`api-get-column-names` | :ref:`api-get-delimiter` | :ref:`api-get-example-rows` | :ref:`api-join` | :ref:`api-load-crtable` | :ref:`api-load-csv` | :ref:`api-load-tabular` | :ref:`api-merge-tables` | :ref:`api-plot-bar` | :ref:`api-plot-histogram` | :ref:`api-plot-line` | :ref:`api-plot-scatter` | :ref:`api-relate` | :ref:`api-remove-duplicate-rows` | :ref:`api-rename-columns` | :ref:`api-replace-columns` | :ref:`api-save-crtable` | :ref:`api-save-csv` | :ref:`api-save-plot` | :ref:`api-save-table-schema` | :ref:`api-save-xls` | :ref:`api-select-columns` | :ref:`api-select-rows` | :ref:`api-split-table` | :ref:`api-summarize-table`

**Model Functions**: :ref:`api-classification-summary` | :ref:`api-customize-explanations` | :ref:`api-describe-model` | :ref:`api-learn-model` | :ref:`api-load-model` | :ref:`api-measure-confusion-matrix` | :ref:`api-measure-input-importance` | :ref:`api-measure-learning-curve` | :ref:`api-measure-score-curve` | :ref:`api-predict` | :ref:`api-save-model` | :ref:`api-score-predictions` | :ref:`api-summarize-model` 

**Checkpoint Functions**: :ref:`api-checkpoint` | :ref:`api-get-checkpoints` | :ref:`api-load-checkpoint`

**Custom Logs Functions**: :ref:`api-get-udf-debug-logs` | :ref:`api-udf-debug-log` 

**Platform Functions**: :ref:`api-get-connections` | :ref:`api-get-session-property` | :ref:`api-get-versions` | :ref:`api-initialize-cluster` | :ref:`api-set-session-property` | :ref:`api-shutdown-cluster`

.. 
.. **CSV tables (artifacts)**: :ref:`tables-breakdown-details` | :ref:`tables-details` | :ref:`tables-details-with-feedback` | :ref:`tables-scored` | :ref:`tables-summary` | :ref:`tables-user-details`
.. 

**Error Messages**: :ref:`Generic or system errors <error-messages-generic-or-system>` | :ref:`Argument validation errors <error-messages-argument-validation>` | :ref:`Load errors <error-messages-load>` | :ref:`Save and checkpoint errors <error-messages-save-and-checkpoint>` | :ref:`Table split, join, and merge errors <error-messages-table-split-join-merge>` | :ref:`Transform errors <error-messages-transform>` | :ref:`Model learning errors <error-messages-model-learning>` | :ref:`Prediction and evaluation errors <error-messages-prediction-and-evaluation>`

.. Hide the TOC from this file.

.. toctree::
   :hidden:

   api
   blocks
   build_cases
   cmd_cr
   cmd_crpkg
   commands
   configure
   define_behaviors
   deploy_connect_data_sources
   deploy_data_lake
   deploy_vse
   error_messages
   extensions
   glossary
   install
   installer
   integrations
   manage_cluster_nodes
   manage_cluster_status_portal
   model_data
   overview
   process_data
   python
   release_notes
   runbook
   stages
   start_here
   tables
   threat_cases
   vse
   workflow
