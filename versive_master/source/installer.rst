.. 
.. versive, primary, security engine
.. 

==================================================
Installer
==================================================

This topic is a reference for the |vse| install, including the directory structure for the deployed components and for the various configuration files that may be necessary for maintenance and tuning of your specific environment. In many cases, additional tuning is unnecessary and much of what is covered on this page is optional. 

.. note:: For the steps necessary to install and configure the |vse|, please see the following topics:

   * :doc:`deploy_data_lake`
   * :doc:`deploy_vse`



.. 
.. install settings from parameters.py:
.. 
.. The following parameters may be specified:
.. 
.. cidr -- cidr(net_index) -- 172.26.{128 * (net_index % 2)}.{net_index / 2}/25, where ``net_index`` is an integer between 0 and 512.
.. create_data_bucket  -- True
.. create_emr_cluster  -- True
.. create_key_pair  -- True
.. create_repo_bucket  -- False
.. create_working_bucket  -- True
.. data_bucket_name -- '{}-{}-{}'.format(S3_BUCKET_PREFIX, self.name, S3_DATA_BUCKET_SUFFIX)
.. dry_run -- False
.. emr_master_type -- 'm3.xlarge'
.. emr_worker_count -- 2
.. emr_worker_type -- 'm3.xlarge'
.. extra_tags -- [{'Key': 'Creator', 'Value': CREATOR}] # for billing
.. long name -- '{}-{}'.format(PREFIX, name)
.. name -- name
.. net_index -- net_index
.. private_cidr -- subnets(net_index) -- is the cidr value, split for private cidr? THIS DOESN'T NEED TO BE SPECIFIED?
.. private_name  -- self.long_name + , appends '-private'
.. public_cidr  -- self.long_name + , appends '-public'
.. public_name -- subnets(net_index) -- is the cidr value, split for public cidr? THIS DOESN'T NEED TO BE SPECIFIED?
.. region -- region == us-west-2
.. repo_bucket_name -- 'vsecloud-software'
.. working_bucket_name -- '{}-{}-{}'.format(S3_BUCKET_PREFIX, self.name, S3_WORKING_BUCKET_SUFFIX)
.. 



.. _installer-directory-structure:

Directory Structure
==================================================
The |vse| installer creates the following directory structure:

::

   $VSE_DIR/vse
   ├── vse-<version>
   │   ├── conf
   │   │   └── spark
   │   ├── vse
   │   ├── info
   │   ├── install
   │   ├── tools
   │   └── ui
   ├── vse-<version>
   │   ├── conf
   │   │   └── spark
   │   ├── vse
   │   ├── info
   │   ├── install
   │   ├── tools
   │   └── ui
   ├── ...



.. commented out of the table sections below. /vse/conf: installer-cr-zip, installer-monkey-py; /vse/conf/spark: installer-spark-spark-env-sh; /vse/tools: installer-apt-py, installer-daily-run-sh, installer-pyspark, installer-self-test-py`


The following table describes each of the directories:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Directory
     - Description
   * - ``$VSE_DIR/vse``
     - The root folder into which the |vse| is installed.
   * - ``/vse/conf``
     - The directory in which |vse|-specific files are located. This directory contains the following files:

       * :ref:`installer-connect-json`
       * :doc:`extensions`
       * :doc:`configure`

   * - ``/vse/conf/spark``
     - The directory in which Spark-specific configuration files are located. These configuration files are used to override specific default settings and to add specific environment variables required by the |vse| and the |versive| platform. This directory contains the following files:

       * :ref:`installer-spark-defaults-conf`

   * - ``/vse/vse``
     - The directory in which the installed components for the |vse| and the |versive| platform are located.
   * - ``/vse/info``
     - This directory contains the following files:

       * README.txt
       * VERSION

   * - ``/vse/install``
     - The directory in which scripts that are used to install the |vse| are located.
   * - ``/vse/tools``
     - The directory in which tools and files for testing and verifying the |vse| installation are located. This directory contains the following files:

       * :ref:`installer-self-test-sh`

   * - ``/vse/ui``
     - The directory into which the |vse| web user interface (backend and frontend components) are installed, along with Zeppelin notebooks.

.. 
.. .. _installer-xxxxx:
.. 
.. xxxxx
.. ==================================================
.. xxxxx
.. 


.. 
.. installer variables commented out
.. 
.. 1. PLATFORM_VERSION - `<tag|branch|sha>` [platform product version]
.. 2. VSE_PLATFORM_VERSION - `<tag|branch|sha>` [platform version in vse product]
.. 3. VSE_UI_FRONTEND_VERSION - `<tag|branch|sha>` [ui front end version in vse product]
.. 4. VSE_UI_BACKEND_VERSION - `<tag|branch|sha>` [ui back end version in vse product]
.. 5. VSE_VSEAPP_VERSION - `<tag|branch|sha>` [vse app version in vse product]
.. 6. VSE_VERSION - `<tag|branch|sha>` [vse product version]
.. 


.. 
.. .. _installer-apt-py:
.. 
.. apt.py
.. ==================================================
.. xxxxx
.. 



Configuration Files
==================================================
The following reference sections detail configuration files that may be necessary for your |vse| configuration. The |vse| installer applies safe defaults with minimal requirements. These files primarily allow your organization to apply additional configuration options, such as using LDAP with the |vse| Web user interface, tuning the Apache Spark environment, applying archive policies for netflow data, and so on.

.. note:: Some configuration files described in this reference may require additional settings for your environment that are not documented, such as for LDAP or for Spark. Some configuration files may require customization by |versive| representatives, such as the scripts used by nfdump tools for that help provide flow data to the engine as a canonical data source.


.. _installer-configure-ldap-yml:

configure-ldap.yml
--------------------------------------------------
The installer for the |vse| allows LDAP to be used for authentication. A configuration file named ``configure-ldap.yml`` must be created and should be located at ``/some/path/to/be/determined``. This configuration file is passed to the |vse| installer by using the ``--config`` parameter and must specify the following settings:

.. code-block:: yaml

   USE_LDAP_AUTH: True
   LDAP_URLS: [ "ldaps://URL" ]
   SEARCH_DN: "dc=hq,dc=company_name,dc=com"
   DOMAIN: "hq.company_name.com"
   USER_SEARCH_FORMAT: "sAMAccountName={0}"
   CA_CERT_FILE: ""
   ADMIN_GROUP_DNS: ["CN=Common Name,OU=Groups-NoEmail,OU=Managed Groups,DC=hq,DC=versive,DC=com"]
   ANALYST_GROUP_DNS: ["CN=Common Name,OU=Groups-NoEmail,OU=Managed Groups,DC=hq,DC=versive,DC=com"]
   SEARCH_FIELDS: ['mail','givenName','sn','sAMAccountName','memberOf']
   FIRST_NAME_FIELD: "givenName"
   LAST_NAME_FIELD: "sn"
   EMAIL_FIELD: "mail"
   

.. note:: Be sure to leave a blank line at the bottom of this file.



.. _installer-capacity-scheduler-xml:

capacity_scheduler.xml
--------------------------------------------------
The ``capacity_scheduler.xml`` file stores the configuration for the YARN Capacity Scheduler, which enables cluster resources to be shared, while ensuring multi-tenancy for each shared resource. The Capacity Scheduler sets limits on queues to ensure that each queue does not consume more than its resource allocation.

Customizing this file is necessary in situations where more than one YARN queue is required, such as when running Zeppelin notebooks with the |vse|.

.. note:: This configuration file is used only when the |vse| is deployed on-premises and when using YARN to manage the cluster.

Add Queues
++++++++++++++++++++++++++++++++++++++++++++++++++
The |vse| requires two YARN queues to support the use of Zeppelin notebooks. Add a ``small`` queue to the list of queues. Change:

.. code-block:: text
   :emphasize-lines: 3

   <property>
     <name>yarn.scheduler.capacity.root.queues</name>
     <value>default</value>
     <description>The queues at the this level (root is the root queue).</description>
   </property>

to:

.. code-block:: text
   :emphasize-lines: 3,4

   <property>
     <name>yarn.scheduler.capacity.root.queues</name>
     <value>small,default</value>
     <description>Queues for VSE and Zeppelin notebooks.</description>
   </property>

Refresh the YARN queues with the following command:

.. code-block:: console

   $ yarn rmadmin -refreshQueues

and then verify that both queues are present in the cluster with the following command:

.. code-block:: console

   $ hadoop queue -list


Define Queue Capacity
++++++++++++++++++++++++++++++++++++++++++++++++++
Each YARN queue requires its capacity to be specified, as a % of 100. Change the ``default`` queue from:

.. code-block:: text
   :emphasize-lines: 3,4

   <property>
     <name>yarn.scheduler.capacity.root.default.capacity</name>
     <value>100</value>
     <description>Default queue target capacity.</description>
   </property>

to:

.. code-block:: text
   :emphasize-lines: 3,4

   <property>
     <name>yarn.scheduler.capacity.root.default.capacity</name>
     <value>90</value>
     <description>Target capacity for default queue.</description>
   </property>

and then add the following section to the ``capacity_scheduler.xml`` file for the ``small`` queue:

.. code-block:: text

   <property>
     <name>yarn.scheduler.capacity.root.small.capacity</name>
     <value>10</value>
     <description>Target capacity for small queue.</description>
   </property>


Define ACLs
++++++++++++++++++++++++++++++++++++++++++++++++++
Use an access control list (ACL) to specify which users may submit jobs to a YARN queue. Add the the following sections to the ``capacity_scheduler.xml`` file to specify the ACL:

.. code-block:: text

   <property>
     <name>yarn.scheduler.capacity.root.small.acl_submit_applications</name>
     <value>*</value>
     <description>The ACL that defines the users who may submit jobs to the default queue.</description>
   </property>

   <property>
     <name>yarn.scheduler.capacity.root.small.acl_administer_queue</name>
     <value>*</value>
     <description>The ACL that defines the users who may submit jobs to the small queue.</description>
   </property>


Enable Queues
++++++++++++++++++++++++++++++++++++++++++++++++++
A YARN queue may be in a running state or a stopped state. Add the the following section to the ``capacity_scheduler.xml`` file to set the ``small`` queue to a running state:

.. code-block:: text

   <property>
     <name>yarn.scheduler.capacity.root.small.state</name>
     <value>RUNNING</value>
     <description>The state of the small queue. State must be one of RUNNING or STOPPED.</description>
   </property>


Refresh Queues
++++++++++++++++++++++++++++++++++++++++++++++++++
Run the following command to refresh YARN queues when updates are made to the ``capacity_scheduler.xml`` file:

.. code-block:: console

   $ yarn rmadmin -refreshQueues


Example
++++++++++++++++++++++++++++++++++++++++++++++++++
.. TODO: Update this "example" capacity_scheduler.xml file based on a real one, not just a guess.

.. code-block:: text

   <property>
     <name>yarn.scheduler.capacity.root.queues</name>
     <value>small,default</value>
     <description>Queues for VSE and Zeppelin notebooks.</description>
   </property>

   <property>
     <name>yarn.scheduler.capacity.root.default.capacity</name>
     <value>90</value>
     <description>Target capacity for default queue.</description>
   </property>

   <property>
     <name>yarn.scheduler.capacity.root.small.capacity</name>
     <value>10</value>
     <description>Target capacity for small queue.</description>
   </property>

   <property>
     <name>yarn.scheduler.capacity.root.small.state</name>
     <value>RUNNING</value>
     <description>The state of the small queue. State must be one of RUNNING or STOPPED.</description>
   </property>

   <property>
     <name>yarn.scheduler.capacity.root.default.acl_submit_applications</name>
     <value>*</value>
     <description>The ACL that defines the users who may submit jobs to the default queue.</description>
   </property>

   <property>
     <name>yarn.scheduler.capacity.root.small.acl_administer_queue</name>
     <value>*</value>
     <description>The ACL that defines the users who may submit jobs to the small queue.</description>
   </property>






.. _installer-connect-json:

connect.json
--------------------------------------------------
The ``connect.json`` file specifies the named connections to a storage cluster. For each named connection, the syntax is:

.. TODO: Need to identify the location of this file. It used to be in "/opt/cr/etc", but may be in "/cr/etc" per the new VSE installer

.. code-block:: javascript

   "connection_name": [
     "optional_description",
     false,
     "storage_type",
     {
       "host": "location-of-host"
     }
   ],


A ``connect.json`` file typically contains more than one named connection. For example:

.. code-block:: javascript

   {
     "hdfs_local": [
       "",
       false,
       "hdfs",
       {
         "host": "172.18.75.230"
       }
     ],
     "local": [
       "Storage on local disk.",
       false,
       "local",
       {
         "host": "localhost"
       }
     ],
     "hdfs_shared": [
       "",
       false,
       "hdfs",
       {
         "host": "hdfs-shared.host-name"
       }
     ],
   }


.. 
.. .. _installer-cr-zip:
.. 
.. cr.zip
.. --------------------------------------------------
.. The ``cr.zip`` file is xxxxx.
.. 



.. _installer-daily-backup-files:

daily_backup_files.sh
--------------------------------------------------
The following script will back up data, and then clean up the local storage. This script is configured to run once per day, archive output from the netflow capture daemon (``nfcapd``), upload that archive to HDFS, and then delete it from local storage.

.. note:: This script is used to enable flow data to be provided to the |vse| as a data source. This script is customized to your environment by a |versive| representative.

.. 
.. commented out, but this script file is similar to
.. 
.. .. code-block:: bash
.. 
..    current_date=`date +%Y%m%d`
.. 
..    nfcapd_data_directory="/path/to/nfcapd_data/"
..    nfdump_data_directory="/path/to/nfdump_output_csv/"
..    directories_to_possibly_upload_nfcapd=`ls -al $nfcapd_data_directory | grep ^d | tr -s ' ' | cut -d ' ' -f 9 | grep 2018 `
..    directories_to_possibly_upload_nfdump=`ls -al $nfdump_data_directory | grep ^d | tr -s ' ' | cut -d ' ' -f 9 | grep 2018 `
.. 
..    echo "possible directories to upload are $directories_to_possibly_upload_nfcapd"
.. 
..    for dir in ${directories_to_possibly_upload_nfcapd[@]}
..    do
..        echo
..        echo " checking that directory is NOT current day "
..     
..        if [ "$current_date" == "$dir" ] 
..        then
..            echo "dir is current date, skipping"
..            continue
..        else
..            echo " dir is not current date, dir =  $dir"
..        fi 
.. 
..        # zip it
..        tar_file_name="${dir}.tar.gz"
..        echo "running command tar -zcf ${nfcapd_data_directory}$tar_file_name ${nfcapd_data_directory}${dir}/"
..        tar -zcf ${nfcapd_data_directory}$tar_file_name ${nfcapd_data_directory}${dir}/
..        echo "running hadoop fs -copyFromLocal ${nfcapd_data_directory}$tar_file_name /path/to/nfcapd_compressed_backup/"
.. 
..        if hadoop fs -copyFromLocal ${nfcapd_data_directory}$tar_file_name /path/to/nfcapd_compressed_backup/
..        then
..            echo "copied data from $tar_file_name to /path/to/nfcapd_compressed_backup/ on hdfs" 
..            echo "removing the local tar file ${nfcapd_data_directory}$tar_file_name"
..            rm -rf ${nfcapd_data_directory}$tar_file_name
..        else
..            echo "**** copy command failed - exiting *****"
..            break 
..        fi 
..        # removing the original directory
..        echo "running this rm -rf ${nfcapd_data_directory}${dir}/"
..        rm -rf ${nfcapd_data_directory}${dir}/
..    done
.. 
..    # loop through existing directories 
..    for dir in ${directories_to_possibly_upload_nfdump[@]}
..    do
..        echo
..        echo " checking that directory is NOT current day "
..     
..        if [ "$current_date" == "$dir" ] 
..        then
..            echo "dir is current date, skipping"
..            continue
..        else
..            echo " dir is not current date, dir =  $dir"
..        fi 
.. 
..        # remove the original directory
..        echo "running this rm -rf ${nfdump_data_directory}${dir}/"
..        rm -rf ${nfdump_data_directory}${dir}/
..    done
..    


.. _installer-daily-nfcapd:

daily_nfcapd.sh
--------------------------------------------------
The following script will use the netflow capture daemon (``nfcapd``) to listen on a port for netflow packets, interpret them, and then store them on disk at a specified interval. This script will stop the previous running process (typically this same process from the previous day), and then start a new process that stores data in a directory for the current date.

.. note:: This script is used to enable flow data to be provided to the |vse| as a data source. This script is customized to your environment by a |versive| representative.

.. 
.. commented out, but this script file is similar to
.. 
.. .. code-block:: bash
.. 
..    current_day=`date +%Y%m%d`
..    echo "today is $current_day"
.. 
..    current_processes=`ps -ef --sort=start_time | grep "sh" | grep "/path/to/daily_nfcapd.sh" | grep -v "grep" | grep -v "vim" | tail | tr -s ' ' | cut -d ' ' -f 2` 
..    echo "and current processes are $current_processes"
..    echo
..    ps -ef --sort=start_time | grep "sh" | grep "/path/to/daily_nfcapd.sh" | grep -v "grep" | grep -v "vim"
.. 
..    # check to make sure there is more than one process or else it will kill itself
..    echo 
..    echo " length of current_processes is "
..    echo "$current_processes" | wc -l 
..    echo
..    if [ -z "$current_processes" ]
..    then
..        echo "current_process was completely empty, skipping"
..    else
..        if [ `echo "$current_processes" | wc -l` -le 2 ]
..        then
..            echo "no previous processes found to kill"
..        else
..            echo "killing all previous processes"
..            echo
..            ps -ef --sort=start_time | grep "sh" | grep "/path/to/daily_nfcapd.sh" | grep -v "grep" | grep -v "vim" | tail 
..            previous_processes=`echo "$current_processes" | head -n -2`
..            kill $previous_processes
..            echo "sleeping for 10 seconds to relinquish ports"
..            sleep 10
..        fi   
..    fi
..    
..    # kill any nfdump processes on port 9995 
..    # kill previous process of nfdump because remember it is a "while true" in the script :)
..    previous_process=`ps aux | grep "nfcapd -E -p 9995" | grep -v "grep" | grep -v "vim" | tr -s ' ' | cut -d ' ' -f 2`
..    if [ -z "$previous_process" ]
..    then
..        echo "no previous process found to kill" 
..    else
..        echo "killing previous process for nfcapd $previous_process"
..        kill $previous_process
..        echo "sleeping for 10 seconds so that the port gets released for us to launch another command to listen on it"
..        sleep 10   
..    fi
..    
..    mkdir -p /path/to/nfcapd_data/${current_day}
..    # kick off nfcapd command with current date as output directory
..    /path/to/nfdump-master/bin/nfcapd -E -p 9995 -l /path/to/nfcapd_data/${current_day}/
.. 



.. _installer-daily-nfdump-running-list:

daily_nfdump_running_list.sh
--------------------------------------------------
The following script will use the ``nfcapd`` tool to parse files that were saved to disk by the netflow capture daemon (``nfcapd``), extract certain fields from the parsed packets, and then save them in CSV format. This script will monitor a directory for the current day. Every time a new file is saved to this directory it will be parsed and uploaded as a CSV file to HDFS for use by the |vse|.

.. note:: This script is used to enable flow data to be provided to the |vse| as a data source. This script is customized to your environment by a |versive| representative.

.. 
.. commented out, but this script file is similar to
.. 
.. .. code-block:: bash
.. 
..    process_new_files () {
..        new_files=$1
..        echo "new files is $new_files"
..        for new_file in ${new_files[@]}
..        do
..            echo "new_file is $new_file"
..            output_file="${new_file}_csv"
..            echo "output_file is, $output_file"
..            /path/to/nfdump-master/bin/nfdump -r /path/to/nfcapd_data/${current_day}/${new_file} | tail -n +2 | awk '{sub(":", ", ", $7); sub(":", ", ", $5); print $9 ", " $7 ", " $5 ", " $1,  $2", " $4}' > /path/to/nfdump_output_csv/${current_day}/${output_file}
..            echo "nfdump csv output written to /path/to/nfdump_output_csv/${current_day}/${output_file}" 
..            echo " date/time is " 
..            date -u 
..            echo "Archiving file : $output_file"
..            gzip -c /path/to/nfdump_output_csv/${current_day}/${output_file} > /path/to/nfdump_output_csv/${current_day}/${output_file}.gz
..            echo "uploading to hdfs at /path/to/netflow_input/${current_day}/${output_file}.gz" 
..            hadoop fs -copyFromLocal /path/to/nfdump_output_csv/${current_day}/${output_file}.gz /path/to/netflow_input/${current_day}/${output_file}.gz
..            echo "removing the local gzipped file"
..            rm /path/to/nfdump_output_csv/${current_day}/${output_file}.gz 
..        done
..    }
.. 
..    current_day=`date +%Y%m%d`
..    echo "today is $current_day"
..    # kill all previous processes of nfdump 
..    current_processes=`ps -ef --sort=start_time | grep "sh" | grep "/path/to/daily_nfdump_running_list.sh" | grep -v "grep" | grep -v "vim" | tail | tr -s ' ' | cut -d ' ' -f 2` 
..    echo "and current processes are $current_processes"
..    echo
..    ps -ef --sort=start_time | grep "sh" | grep "/path/to/daily_nfdump_running_list.sh" | grep -v "grep"| grep -v "vim" 
..    # check to make sure there is more than one process or else it will kill itself
..    echo 
..    echo " length of current_processes is "
..    echo "$current_processes" | wc -l 
..    echo
..    if [ -z "$current_processes" ]
..    then
..        echo "current_process was completely empty, skipping"
..    else
..        if [ `echo "$current_processes" | wc -l` -le 2 ]
..        then
..            echo "no previous processes found to kill"
..        else
..            echo "killing all previous processes"
..            echo
..            ps -ef --sort=start_time | grep "sh" | grep "/path/to/daily_nfdump_running_list.sh" | grep -v "grep" | grep -v "vim" | tail 
..            previous_processes=`echo "$current_processes" | head -n -2`
..            echo "killing previous process "
..            kill $previous_processes
..            echo "sleeping for 10 seconds to relinquish ports"
..            sleep 10
..        fi
..    fi
.. 
..    # create directory if it does not already exist: 
..    mkdir -p /path/to/nfdump_output_csv/${current_day} 
..    chmod 777 -R /path/to/nfdump_output_csv/${current_day} 
..    hadoop fs -mkdir /path/to/netflow_input/${current_day}/
..    hadoop fs -chmod -R 755 /path/to/netflow_input/${current_day}/
.. 
..    #### This part is running all the time
..    previous_file_count=`ls /path/to/nfcapd_data/${current_day} | wc -l`
..    while true
..    do
..        # check if there are any new files in directory /path/to/nfcapd_data/${current_day}/
..        current_file_count=`ls /path/to/nfcapd_data/${current_day} | wc -l`
..        if [ $current_file_count -gt $previous_file_count ]
..        then
..            echo "previous_file_count is $previous_file_count, and current_file_count is $current_file_count"
..            previous_file_count=$current_file_count
..            # check if the new files are NOT in the list of files already processed 
..            # chop off the "_csv" at then end 
..            current_csv_files=`ls /path/to/nfdump_output_csv/${current_day} | sed s/_csv//g`
..            current_nfcapd_files=`ls /path/to/nfcapd_data/${current_day}`
..            set_diff_files=`echo "$(echo "$current_nfcapd_files"; echo "$current_csv_files")" | sort | uniq -u`
..            # elements in set1 but NOT in set2
..            #set_diff_files=`comm -23 <(sort $current_nfcapd_files) <(sort $current_csv_files)`
..            # the above only finds the files that are in only one of the two sets 
.. 
..            echo "set_diff_files is $set_diff_files"
..            # set_diff_files should be a list of new files to process inside directory nfcapd_data/date
..            num_set_diff_files=`echo "$set_diff_files" | wc -l`
..            set_of_new_files=`echo $set_diff_files`
..            echo "set_of_new_files is $set_of_new_files"
..            if [ $num_set_diff_files -gt 0 ]
..            then
..                echo "processing new files in process_new_files method "
..                echo "num_set_diff_files is $num_set_diff_files"
..                process_new_files "$set_of_new_files"
..            else
..                continue    
..            fi 
..        else
..            continue 
..        fi
..    done
.. 
   


.. 
.. .. _installer-daily-run-sh:
.. 
.. daily_run.sh
.. --------------------------------------------------
.. xxxxx
.. 

.. 
.. .. _installer-monkey-py:
.. 
.. monkey.py
.. --------------------------------------------------
.. The ``monkey.py`` file is used for monkey-patching the |vse| pipeline for on-site troubleshooting.


.. 
.. .. _installer-pyspark:
.. 
.. pyspark.sh
.. --------------------------------------------------
.. xxxxx
.. 



.. _installer-renew_kerberos:

renew_kerberos.sh
--------------------------------------------------
The following script will renew Kerberos tickets to ensure access to HDFS with proper authentication. Use this script if Kerberos tickets are configured to expire on a regular basis, and then specify the frequency at which this script will run (i.e. ``7d`` for "every seven days").

.. code-block:: bash

   cho "password"| kinit -l 7d user@kerberos_domain


.. 
.. .. _installer-self-test-py:
.. 
.. self_test.py
.. --------------------------------------------------
.. xxxxx
.. 

.. _installer-self-test-sh:

self_test.sh
--------------------------------------------------
The ``self_test.sh`` file verifies the installation of the |vse|.



.. _installer-setup-sh:

setup.sh
--------------------------------------------------
The ``setup.sh`` file installs the |versive| platform and the |vse|. The following variables exist and are tracked here for now:

.. code-block:: bash

   # ================================================================
   # Variables
   # ================================================================

   # The product and version information.
   readonly VERSIVE_PRODUCT="vse-app"
   readonly VERSIVE_VERSION="2.15.23-4"
   readonly VERSIVE_RELEASE_DATE="2017-12-15"

   # The following two varibles define the keys for AWS access.
   readonly VERSIVE_BOTO_ACCESS_KEY="ABC"
   readonly VERSIVE_BOTO_SECRET_KEY="XYZ"
   
   # The following two variables define the USER and HOME values.
   readonly VERSIVE_USER=${USER}
   readonly VERSIVE_HOME=${HOME}
   
   # Define the YARN master.
   readonly VERSIVE_YARN_MASTER_HOST="namenode"
   readonly VERSIVE_YARN_MASTER_IP="10.45.67.89"
   
   # Define the installation directory.
   # It can be a shared mount or a local directory.
   # Shared mounts do not need to have the software installed for worker.
   # Local directories need the software installed iff the software is not already there.
   #readonly VERSIVE_INSTALL_DIR="/mnt/share/vse"
   readonly VERSIVE_INSTALL_DIR="${HOME}/vse"
   
   # Where the ZIP files are located.
   readonly VERSIVE_ZIP_DIR="$(pwd)/foo/bar/zip"


.. 
.. .. _installer-spark-spark-env-sh:
.. 
.. spark-env.sh
.. --------------------------------------------------
.. xxxxx
.. 

.. _installer-spark-defaults-conf:

spark-defaults.conf
--------------------------------------------------
The ``spark-defaults.conf`` file stores the configuration for Spark settings, such as environment variables that are specific to the |versive| paltform or the |vse| or to override specific default settings. For Spark configuration setting that are not specified in the ``spark-defaults.conf`` file used by the |versive| platform and the |vse|, `the default configuration settings are applied <https://spark.apache.org/docs/latest/configuration.html>`__.

An example ``spark-defaults.conf`` file:

.. code-block:: text

   # System properties that are specific to the Versive platform
   # and the Versive Security Engine
   
   spark.blacklist.decommissioning.timeout 1h
   spark.driver.extraJavaOptions -Duser.timezone=UTC
   spark.driver.memory 20g
   spark.driver.extraClassPath=${INSTALL_ROOT_DIR}/lib/*:
     /usr/lib/hadoop-lzo/lib/*:
     /usr/lib/hadoop/hadoop-aws.jar:
     /usr/share/aws/aws-java-sdk/*:
     /usr/share/aws/emr/emrfs/conf:
     /usr/share/aws/emr/emrfs/lib/*:
     /usr/share/aws/emr/emrfs/auxlib/*:
     /usr/share/aws/emr/security/conf:
     /usr/share/aws/emr/security/lib/*
   spark.driverEnv.CR_ROOT=${INSTALL_ROOT_DIR}
   spark.driverEnv.CRPYTHON=${INSTALL_ROOT_DIR}/bin/python
   spark.dynamicAllocation.enabled true
   spark.dynamicAllocation.executorIdleTimeout 300s
   spark.dynamicAllocation.initialExecutors 20
   spark.dynamicAllocation.maxExecutors 20
   spark.dynamicAllocation.minExecutors 20
   spark.eventLog.dir hdfs:///var/log/spark/apps
   spark.executor.cores 2
   spark.executor.extraClassPath=${INSTALL_ROOT_DIR}/lib/*:
     /usr/lib/hadoop-lzo/lib/*:
     /usr/lib/hadoop/hadoop-aws.jar:
     /usr/share/aws/aws-java-sdk/*:
     /usr/share/aws/emr/emrfs/conf:
     /usr/share/aws/emr/emrfs/lib/*:
     /usr/share/aws/emr/emrfs/auxlib/*:
     /usr/share/aws/emr/security/conf:
     /usr/share/aws/emr/security/lib/*
   spark.executor.extraJavaOptions -Duser.timezone=UTC
   spark.executor.memory 100g
   spark.executorEnv.CR_ROOT=${INSTALL_ROOT_DIR}
   spark.executorEnv.CRPYTHON=${INSTALL_ROOT_DIR}/bin/python
   spark.hadoop.fs.s3a.access.key=${BOTO_ACCESS_KEY}
   spark.hadoop.fs.s3a.secret.key=${BOTO_SECRET_KEY}
   spark.hadoop.yarn.timeline-service.enabled false
   spark.history.fs.logDirectory hdfs:///var/log/spark/apps
   spark.history.ui.port 18080
   spark.io.compression.codec snappy
   spark.kryoserializer.buffer.max=512mb
   spark.memory.fraction 0.4
   spark.resourceManager.cleanupExpiredHost true
   spark.rpc.askTimeout 600
   spark.shuffle.compress true
   spark.shuffle.service.enabled true
   spark.sql.hive.metastore.sharedPrefixes com.amazonaws.services.dynamodbv2
   spark.sql.shuffle.partitions 1600
   spark.sql.tungsten.enabled true
   spark.sql.warehouse.dir hdfs:///user/spark/warehouse
   spark.stage.attempt.ignoreOnDecommissionFetchFailure true
   spark.submit.pyFiles=${INSTALL_ROOT_DIR}/pkg/crepe-apt.zip
   spark.yarn.appMasterEnv.SPARK_PUBLIC_DNS $(hostname -f)
   spark.yarn.dist.files /etc/spark/conf/hive-site.xml
   spark.yarn.dist.archives=${INSTALL_ROOT_DIR}/pkg/cr.zip
   spark.yarn.executor.memoryOverhead 12288
   spark.yarn.historyServer.address ip-111-22-3-444.us-west-2.compute.internal:18080


The following list describes the important non-default Spark configuration settings that are required by the |versive| platform and the |vse|:

.. spark.blacklist.decommissioning.enabled is AWS only?

**spark.blacklist.decommissioning.enabled**
   Specifies if nodes in a decommissioning state in YARN are blacklisted by Spark. When ``true``, Spark will not schedule new tasks on worker nodes that are in a decommissioning state; tasks that have already started will be allowed to complete. Default value: ``true``. 

.. spark.blacklist.decommissioning.timeout is AWS only?

**spark.blacklist.decommissioning.timeout**
   The amount of time (in hours) that must pass before a node in a decommissioning state is blacklisted. To ensure a node is blacklisted for its entire decommissioning period, set the amount of time the YARN resource manager waits for decommissioning timeouts to be equal to (or greater than) this property value. Default value: ``1h``.

**spark.driver.extraJavaOptions**
   A string of extra JVM options to be passed to the driver. Default value: ``-Duser.timezone=UTC``.

**spark.driver.memory**
   The amount of memory to be available to the driver. Default value: ``20g``.

**spark.driver.extraClassPath**
   A colon-separated list of classpath entries that are prepended to the classpath for the driver. This property helps ensure backwards-compatibility with older versions of Spark. Default value: ``=${INSTALL_ROOT_DIR}`` with the following directories as a colon-separated list:

   .. code-block:: none

      /lib/*:
      /usr/lib/hadoop-lzo/lib/*:
      /usr/lib/hadoop/hadoop-aws.jar:
      /usr/share/aws/aws-java-sdk/*:
      /usr/share/aws/emr/emrfs/conf:
      /usr/share/aws/emr/emrfs/lib/*:
      /usr/share/aws/emr/emrfs/auxlib/*:
      /usr/share/aws/emr/security/conf:
      /usr/share/aws/emr/security/lib/*

**spark.driverEnv.CR_ROOT**
   The root directory for the |versive| application. Default value: ``=${INSTALL_ROOT_DIR}``.

**spark.driverEnv.CRPYTHON**
   The root directory for the instance of Python that is used by the |versive| application. Default value: ``=${INSTALL_ROOT_DIR}/bin/python``.

**spark.dynamicAllocation.enabled**
   Specifies if dynamic resource allocation is enabled. Dynamic resource allocation scales the number of worker nodes that are registered for the |vse| up and down based on workload. When this property is set to ``true``, the following properties may be used to tune the starting, minimum, and maximum numbers of worker nodes: ``spark.dynamicAllocation.initialExecutors``, ``spark.dynamicAllocation.maxExecutors``, and ``spark.dynamicAllocation.minExecutors``. Default value: ``true``.

**spark.dynamicAllocation.executorIdleTimeout**
   When ``spark.dynamicAllocation.enabled`` is set to ``true``, specifies the amount of time (in seconds) that an executor may be idle before it is removed. Default value: ``300s``.

**spark.dynamicAllocation.initialExecutors**
   When ``spark.dynamicAllocation.enabled`` is set to ``true``, specifies the number of worker nodes to run. This value should be equal to or higher than the value specified by the ``spark.dynamicAllocation.minExecutors`` property. The default number of worker nodes specified by the ``spark.executor.instances`` property is ``2``; if the actual number of worker nodes exceeds the value specified by this property, that number is used as the value for this property. Default value: ``20``.

**spark.dynamicAllocation.maxExecutors**
   When ``spark.dynamicAllocation.enabled`` is set to ``true``, specifies the maximum number of worker nodes. Default value: ``20``.

**spark.dynamicAllocation.minExecutors**
   When ``spark.dynamicAllocation.enabled`` is set to ``true``, specifies the minimum number of worker nodes. Default value: ``20``.

**spark.eventLog.dir**
   The directory in which Spark events are logged. Default value: ``hdfs:///var/log/spark/apps``.

**spark.executor.cores**
   The number of cores to be used by each worker node. If the |vse| is running on a single worker node, this allows multiple workers to be run on a single node, up to the number of cores specified by this parameter. Default value: ``2``.

**spark.executor.extraClassPath**
   A colon-separated list of classpath entries that are prepended to the classpath for worker nodes. This property helps ensure backwards-compatibility with older versions of Spark. Default value: ``=${INSTALL_ROOT_DIR}`` with the following directories as a colon-separated list:

   .. code-block:: none

      /lib/*:
      /usr/lib/hadoop-lzo/lib/*:
      /usr/lib/hadoop/hadoop-aws.jar:
      /usr/share/aws/aws-java-sdk/*:
      /usr/share/aws/emr/emrfs/conf:
      /usr/share/aws/emr/emrfs/lib/*:
      /usr/share/aws/emr/emrfs/auxlib/*:
      /usr/share/aws/emr/security/conf:
      /usr/share/aws/emr/security/lib/*

**spark.executor.extraJavaOptions**
   A string of extra JVM options to be passed to worker nodes. Default value: ``-Duser.timezone=UTC``.

**spark.executor.memory**
   The amount of memory to be available to each worker node. Default value: ``100g``.

**spark.executorEnv.CR_ROOT**
   The root location for environment variables that are passed to worker nodes. Default value: ``=${INSTALL_ROOT_DIR}``.

**spark.executorEnv.CRPYTHON**
   One (or more) environment variables that are passed to worker nodes. Default value: ``=${INSTALL_ROOT_DIR}/bin/python``.

.. spark.hadoop.fs.s3a.access.key is AWS only?

**spark.hadoop.fs.s3a.access.key**
   The access key used with Amazon AWS. Default value: ``=${BOTO_ACCESS_KEY}``.

.. spark.hadoop.fs.s3a.secret.key is AWS only?

**spark.hadoop.fs.s3a.secret.key**
   The secret key used with Amazon AWS. Default value: ``=${BOTO_SECRET_KEY}``.

**spark.hadoop.yarn.timeline-service.enabled**
   Specifies if `the YARN timeline service <https://hadoop.apache.org/docs/r2.4.1/hadoop-yarn/hadoop-yarn-site/TimelineServer.html>`__ is enabled. The YARN timeline service stores and retries generic information about completed applications, such as queue names, user information, application attempts, and containers that are run by applications as well as application-specific information, such as map tasks, reduce tasks, counters, and so on. Default value: ``false``.

**spark.history.fs.logDirectory**
   The URL for the directory from which application event logs are loaded. Default value: ``hdfs:///var/log/spark/apps``.

**spark.history.ui.port**
   The port to which the web interface for the history server binds. Default value: ``18080``.

**spark.io.compression.codec**
   The codec used to compress files, such as RDD partitions, event logs, and compressed output files. Possible values: ``lz4`` (the Spark default), ``lzf``, and ``snappy`` (the |vse| default). Snappy is a compression/decompression library that prefers high speeds and reasonable compression quality over lower memory usage (lzf) or maximum decoding speeds (lz4).

**spark.kryoserializer.buffer.max**
   The maximum allowable size of the Kyro serialization buffer. Kryo is `a fast and efficient object graph serialization framework <https://github.com/EsotericSoftware/kryo>`__. Default value: ``=512mb``.

**spark.memory.fraction**
   The fraction of heap space that is used for execution and storage. A lower value will allow spills and cached data eviction to occur more frequently. Default value: ``0.4``.

.. spark.resourceManager.cleanupExpiredHost is AWS only?

**spark.resourceManager.cleanupExpiredHost**
   Specifies if Spark should unregister cached data and/or unregister shuffle blocks that are stored on worker nodes that are in a decommissioned state. Default value: ``true``.

**spark.rpc.askTimeout**
   The amount of time (in seconds) to wait for a response from an RPC client before an Ask operation times out. Default value: ``600``.

**spark.shuffle.compress**
   Specifies if output files are compressed using the codec specified by the ``spark.io.compression.codec`` property. Default value: ``true``.

**spark.shuffle.service.enabled**
   Specifies if the external shuffle service is enabled, which preserves files that are written by worker nodes so those nodes may be removed. This property must be set to ``true`` when the ``spark.dynamicAllocation.enabled`` property is set to a value greater than zero. Default value: ``true``.

**spark.sql.hive.metastore.sharedPrefixes**
   A comma-separated list of class prefixes that are shared between Spark and Hive. Default value: ``com.amazonaws.services.dynamodbv2``.

**spark.sql.shuffle.partitions**
   The number of partitions to use when shuffling data for joins or aggregations. Default value: ``1600``.

**spark.sql.tungsten.enabled**
   Specifies if managed memory is enabled by default. Managed memory is a feature of Spark that improves memory and CPU efficiency with more explicit memory management, cache-aware computations for better memory utilization, and more efficient operations on binary data. Default value: ``true``.

**spark.sql.warehouse.dir**
   The directory for the Spark data warehouse. Default value: ``hdfs:///user/spark/warehouse``.

.. spark.stage.attempt.ignoreOnDecommissionFetchFailure is AWS only?

**spark.stage.attempt.ignoreOnDecommissionFetchFailure**
   Specifies if failed fetches of shuffle blocks from worker nodes in decommissioned states count toward the maximum number of consecutive fetch failures. Default value: ``true``.

**spark.submit.pyFiles**
   A comma-separated list of files that are added to the ``PYTHONPATH`` for use by the |versive| platform and the |vse|. Default value: ``=${INSTALL_ROOT_DIR}/pkg/crepe-apt.zip``. 

**spark.yarn.appMasterEnv.SPARK_PUBLIC_DNS**
   An environment variable to be passed to the YARN application driver. Default value: ``$(hostname -f)``.

**spark.yarn.dist.files**
   A comma-separated list of files to placed in the working directory for each worker node. Default value: ``/etc/spark/conf/hive-site.xml``.

**spark.yarn.dist.archives**
   A comma-separated list of archives to be extracted into the working directory for each worker node. Default value: ``=${INSTALL_ROOT_DIR}/pkg/cr.zip``.

**spark.yarn.executor.memoryOverhead**
   The amount of off-heap memory (in megabytes) to be allocated for each worker node. Default value: ``12288``.

   When this value is too low an error messsage similar to "Container killed by YARN for exceeding memory limits. x.x GB of x GB physical memory used." If this error message is returned, increase this value.

**spark.yarn.historyServer.address**
   The address for the Spark history server. Default value: ``ip-172-23-0-146.us-west-2.compute.internal:18080``.



Time Durations
++++++++++++++++++++++++++++++++++++++++++++++++++
Properties that specify some time duration should be configured with a unit of time. The following format is accepted:

* 25ms (milliseconds)
* 5s (seconds)
* 10m or 10min (minutes)
* 3h (hours)
* 5d (days)
* 1y (years)

Property Sizes
++++++++++++++++++++++++++++++++++++++++++++++++++
Properties that specify a byte size should be configured with a unit of size. The following format is accepted:

* 1b (bytes)
* 1k or 1kb (kibibytes = 1024 bytes)
* 1m or 1mb (mebibytes = 1024 kibibytes)
* 1g or 1gb (gibibytes = 1024 mebibytes)
* 1t or 1tb (tebibytes = 1024 gibibytes)
* 1p or 1pb (pebibytes = 1024 tebibytes)

.. 
.. Environment Variables
.. ==================================================
.. .. warning:: copypasta for now ... need to identify if any of these are non-default for the VSE or platform. Or if we add new ones. Or what.
.. 
.. Certain Spark settings can be configured through environment variables, which are read from the conf/spark-env.sh script in the directory where Spark is installed (or conf/spark-env.cmd on Windows). In Standalone and Mesos modes, this file can give machine specific information such as hostnames. It is also sourced when running local Spark applications or submission scripts.
.. 
.. .. note:: The conf/spark-env.sh file does not exist by default when Spark is installed. However, you can copy conf/spark-env.sh.template to create it. Make sure you make the copy executable.
.. 
.. The following variables can be set in spark-env.sh:
.. 
.. .. list-table::
..    :widths: 200 420
..    :header-rows: 1
.. 
..    * - Environment Variable
..      - Description
..    * - **JAVA_HOME**
..      - Location where Java is installed (if it's not on your default PATH).
..    * - **PYSPARK_PYTHON**
..      - Python binary executable to use for PySpark in both driver and workers (default is python2.7 if available, otherwise python). Property spark.pyspark.python take precedence if it is set
..    * - **PYSPARK_DRIVER_PYTHON**
..      - Python binary executable to use for PySpark in driver only (default is PYSPARK_PYTHON). Property spark.pyspark.driver.python take precedence if it is set
..    * - **SPARKR_DRIVER_R**
..      - R binary executable to use for SparkR shell (default is R). Property spark.r.shell.command take precedence if it is set
..    * - **SPARK_LOCAL_IP**
..      - IP address of the machine to bind to.
..    * - **SPARK_PUBLIC_DNS**
..      - Hostname your Spark program will advertise to other machines.
.. 
.. In addition to the above, there are also options for setting up the Spark standalone cluster scripts, such as number of cores to use on each machine and maximum memory.
.. 
.. Since spark-env.sh is a shell script, some of these can be set programmatically – for example, you might compute SPARK_LOCAL_IP by looking up the IP of a specific network interface.
.. 
.. .. note:: When running Spark on YARN in cluster mode, environment variables need to be set using the spark.yarn.appMasterEnv.[EnvironmentVariableName] property in your conf/spark-defaults.conf file. Environment variables that are set in spark-env.sh will not be reflected in the YARN Application Master process in cluster mode. See the YARN-related Spark Properties for more information.
.. 




.. _installer-yarn-site-xml:

yarn-site.xml
--------------------------------------------------
The ``yarn-site.xml`` file stores the configuration for the YARN Resource Manager that runs on the primary node in in the |vse| cluster, and then also the YARN Node Manager that runs on each replica node in the cluster.

.. note:: This configuration file is used only when the |vse| is deployed on-premises and when using YARN to manage the cluster.

Add Scheduler Class
++++++++++++++++++++++++++++++++++++++++++++++++++
Add the scheduler class to the ``yarn-site.xml`` file:

.. code-block:: text

   <property>
     <name>yarn.resourcemanager.scheduler.class</name>
     <value>org.apache.hadoop.yarn.server.resourcemanager.scheduler.capacity.CapacityScheduler</value>
   </property>

and then restart the YARN resource manager:

.. code-block:: console

   $ sudo service hadoop-yarn-resourcemanager restart

.. TODO: Isn't this scheduler class there by default?








IPython
--------------------------------------------------
`IPython <http://ipython.org/>`_ is an interactive shell for computing that provides a browser-based notebook with support for data visualization, such as inline plots. IPython and supporting modules are included with the |versive| platform beginning with version 2.8.4. Training demos and data using IPython are distributed as a separate .tgz file.

Set up IPython
++++++++++++++++++++++++++++++++++++++++++++++++++
IPython must first be set up and configured:

#. Connect to the primary node via SSH.
#. At the command line, enter the following to launch IPython and create the profile directory:

   .. code-block:: console

      $ ipython notebook --no-browser

   The ``--no-browser`` option prevents IPython from starting the browser on the primary node.

#. Enter ``Ctrl-C`` to exit IPython.
#. By default, IPython captures all logs and displays them in pink. To disable logging, navigate to ``~/.ipython/profile_default/startup`` on the primary node and create a file called ``disable-warnings.py`` that contains the following:

   .. code-block:: none

      import logging
      logging.disable(50)

#. Optional. On the client computer, create an alias for tunneling into the primary node. Open the ``.profile`` file and add the following:

   .. code-block:: console

      alias inotebook="ssh -f -N -L 8888:localhost:8888 \
      <primary_IP_address> && open http://localhost:8888"

   This command creates an alias that uses only "inotebook" to connect to the primary node running IPython, and then open it in the browser on the client computer.

#. Reload the ``.profile`` file:

   .. code-block:: console

      $ source ~/.profile

#. IPython is now set up.

Run IPython
++++++++++++++++++++++++++++++++++++++++++++++++++
To run IPython:

#. On the primary node, start IPython:

   .. code-block:: console

      $ ipython notebook --no-browser

#. From the client computer, tunnel into the primary node. If an alias was created, enter:

   .. code-block:: console

      $ inotebook

   If an alias was not created, enter:

   .. code-block:: console

      $ ssh -f -N -L 8888:localhost:8888 <primary_IP_address> && open http://localhost:8888

   IPython notebook will open in the browser on your computer.

#. When finished, quit IPython by entering ``Ctrl-C`` on the primary node.





SSH
--------------------------------------------------
SSH keys are a way to identify trusted computers, but without involving passwords. This topic describes how to create and distribute SSH keys, and then enable passwordless login to connect to the nodes in the compute cluster.

* The instructions below are for a Mac or Linux computer.
* The examples use "account" for the account name; replace this with the name of the account that will be using the |versive| platform to build predictive models.
* For a Windows computer, use the instructions for `PuTTY <http://www.chiark.greenend.org.uk/%7Esgtatham/putty/>`__.

When an SSH key-pair is generated, a public key and a private key are created that are associated to each other. The private key resides on the machine from which the user who requires passwordless access will work. The public key resides on the primary node in the compute cluster. The two keys are associated with each other and are used to verify the user before granting access.

Generate SSH Key-Pair
++++++++++++++++++++++++++++++++++++++++++++++++++
To generate an SSH key-pair, do the following:

#. At the command line, navigate to the home directory:

   .. code-block:: console

      $ cd ~/

#. Create the SSH key pair:
	
   .. code-block:: console

      $ ssh-keygen -t rsa
      Generating public/private rsa key pair.

   The command will prompt for a file name and a passphrase for the key pairs:

   .. code-block:: console

      Enter file in which to save the key (/Users/account/.ssh/id_rsa): /Users/account/.ssh/id_rsa
      Enter passphrase (empty for no passphrase):
      Enter same passphrase again:
      Your identification has been saved in id_rsa.
      Your public key has been saved in id_rsa.pub.
      The key fingerprint is: 81:e4:03:8f:f9:19:82:49:1e:c0:b7:58:7b:12:99:c5 account@computer.local

   .. note:: If a file name is not specified, the default file ``id_rsa`` is created.

#. Verify that the keys are created. At the command line, navigate to the ``.ssh`` directory:

   .. code-block:: console

      $ cd ~/.ssh

   List all keys:
	
   .. code-block:: console

      $ ls -all 

#. Create the ``.ssh`` directory on the primary node. This will require the IP address of the primary node.

   .. code-block:: none

      $ ssh account@10.250.57.149 mkdir -p .ssh
        The authenticity of host '10.250.57.149' can't be established.
        RSA key fingerprint is d6:53:94:43:b3:cf:d7:e2:b0:0d:50:7b:17:32:29:2a.
        Are you sure you want to continue connecting (yes/no)? yes
        Warning: Permanently added '10.250.57.149' (RSA) to the list of known hosts.

#. Upload the public key (``id_rsa.pub``) to the primary node, and then add it the list of authorized keys:

   .. code-block:: none

      $ cat .ssh/id_rsa.pub | ssh account@10.250.57.149 'cat >> .ssh/authorized_keys' account@10.250.57.149's password:

#. Verify the connection:

   .. code-block:: console

      $ ssh account@10.250.57.149

   .. note:: If the connection does not work, the permissions for the ``.ssh` directory and ``authorized_keys`` file on the primary node may need to be set:

      .. code-block:: console

         $ ssh account@10.250.57.149 "chmod 700 .ssh; chmod 640 .ssh/authorized_keys"



Command-line
--------------------------------------------------
The recommended way to install and configure the |versive| platform compute cluster is to use installer prompts or to pass the installer a configuration file. However, it is also possible to configure nodes in the compute cluster by specifying individual command-line options.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ sh ./cr-<version>.cc.<revision>.run -- --option=OPTION ...

.. note:: The second set of dashes is required.

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available:

``--base_service_port=PORT``
   A low TCP port that is used to begin assigning ports from which services listen. If not specified, will use the default ports created by the default installation of the |versive| platform.

``--clean=NODE``
   Uninstall the |versive| platform on the specified node in the cluster.

``--cluster_name=NAME``
   The name of the cluster.

``--data_dir=PATH``
   The absolute path to the location in which data is stored on each node.

``--install_dir=PATH``
   The absolute path to the location in which the |versive| platform is installed or upgraded.

``--ips=IP_ADDRESS IP_ADDRESS ...``
   A whitespace separated list of the IP address for each node in the cluster. The first IP address is the primary node.

``--service_user=USER_NAME``
   The name of the user account that owns all installation files and all running service processes.

``--user=USER_NAME``
   The name of the user account that has passwordless SSH access to all of the nodes in the cluster.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ sh ./cr-<version>.amd64.run -- --user=user --cluster_name=name \
     ips=10.250.57.149 10.250.9.95 --service_user=admin \
     --install_dir=opt/cr --data_dir=data/cr





