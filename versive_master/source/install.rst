.. 
.. versive, primary, security engine
.. 

==================================================
Install the Engine
==================================================

.. note:: This topic is an in-progress working topic for the steps necessary to install the |versive| platform and |vse|, including (but not limited to) the cluster itself, the platform/engine, and other necessary tools (like Zeppelin notebooks). This topic will reference installer.rst by design. It will likely be incorporated into the existing deploy_* group of topics, and then deprecated.

.. 
.. VSE on AWS
.. 
.. VSE On-premises
.. 



Zeppelin Notebooks
==================================================
Use a Zeppelin notebook to analyze the results of daily processing.

.. TODO: Need an actual intro.

The following steps are required to use Zeppelin notebooks with the |vse|:

#. Install the Zeppelin notebook on the primary node.
#. Create a dedicated YARN queue for use by the Zeppelin notebook.


Install Zeppelin
--------------------------------------------------
TBD install Zeppelin notebook on the primary node.

#. Install Zeppelin on the primary node.
#. For reference: https://zeppelin.apache.org/docs/0.6.0/install/install.html


Create YARN Queue
--------------------------------------------------
Create a YARN queue that is dedicated for use by the Zeppelin notebook:

#. Update the :ref:`installer-yarn-site-xml` file to ensure the Capacity Scheduler service is enabled.
#. Update the :ref:`installer-capacity-scheduler-xml` to add a second queue, define its capacity, specify who can access the queue, and then enable it.
#. Refresh the YARN queues.
#. Restart the YARN Resource Manager service.
#. Open Zeppelin Web UI located at http://<IP_ADDRESS_FOR_PRIMARY_NODE>:8080.
#. From the top-right **anonymous** drop-down menu, select **Interpreter**.
#. In the **spark** section, set the following values:

   Set **SPARK_HOME** to the Spark home directory, typically ``/usr/lib/spark``.

   Set **master** to ``yarn-client``.

   Set **spark.yarn.queue** to the name of the queue that is dedicated to the Zeppelin notebook.

#. Apply these changes, and then allow the Zeppelin notebook to restart the interpreter.


VSE Installer
==================================================
Run the VSE installer. Rougly (very):

#. Get the VSE Self-Extracting Archive:
#. Run this:

   .. code-block:: console

      $ aws s3 ls s3://contextrelevant-packages/images/vse/

#. At the bottom, you'll see a number of ones that start with VSE-.
#. If the one you want is there, for example platform 2.15.24.0 and app 1.4.34.0, get it to your machine with:

   .. code-block:: console

      $ VSE_VER=2.15.28.0-1.4.39.0        # or whichever one you want
      $ cd ~
      $ aws s3 cp s3://contextrelevant-packages/images/vse/VSE-$YFZ_VER/versive-yarn-packaging-$VSE_VER. .

#. If you don't see the version you want, do this:
#. Go here: http://172.18.77.205:8080/job/PicoTestPrototype/build?delay=0sec
#. Enter in the relevant details. Note: the platform/app release versions must have a .0 at the end!  Wow!
#. Click Build
#. Once it is done, use the above aws s3 ls command to confirm your VSE is there!

   .. note:: What could go wrong, and what should you do about it? Maybe you have problems accessing S3. Work with @poopsmith to get correct credentials. If you are trying to install outside the company you might need to get the release from an external URL. Maybe building the VSE on Party Jenkins doesn't work, or it fails the Sauron test. You could work around the test by getting the VSE from the unpublished area TODO add details from Sergey. You could contact #theme-valkyrie for help.

   .. code-block:: text

      Run VSE Self-Extracting Archive:
      $ ./versive-yarn-packaging-$VSE_VER.run
      Archive Extracted!
      Running procedure ~/vse-$VSE_VER/ops/1b_upgrade_install.sh
      Installing 'stage' version of VSE to ~/vse/vse-$VSE_VER.
         Driver copy of VSE app is located at ~/vse/vse-$VSE_VER/cr
         YFZ copy of VSE app is located at ~/vse/vse-$VSE_VER/cr.zip
      Borrowing settings from stuff from prod version of VSE located at ~/vse/vse-$OLD_VSE_VER
         NEW: Copying daily_run.sh to to ~/vse/vse-$VSE_VER
         Copying connect.json to ~/vse/vse-$VSE_VER/cr/etc
         Copying App Config from ~/vse/vse-$OLD_VSE_VER/daily_config.yml to ~/vse/vse-$VSE_VER/daily_config_copy.yml
         Copying Spark Config from ~/vse/vse-$OLD_VSE_VER/blah blah and updating paths
      Verifying Yarn Settings
      Cloning Results DB and migrating schema ...
      Starting UI server at ...
      Making symbolic link from ~/vse/vse-$VSE_VER to ~/vse/vse-stage
      Congratulations! Your new VSE is installed side by side with your prod version.
      Let's continue the process of upgrading to this new version by tests and other steps.
 
#. Self-test.


Heimdallr
==================================================
Heimdallr is a component of the |vse| that enables system reporting belargh xxxxx. placeholder.

Configure Heimdallr
--------------------------------------------------
Open the configuration file. Uncomment the following, and then update the values for your installation:

.. code-block:: yaml

   heimdallr:
     site: 'site-name'
     api_url: 'api-url'
     api_key: 'api-key'


.. TODO: align this with the pending heimdallr section in the configure.rst topic, which is also commented out
.. 
..   heimdallr:
..     site: 'string'
..     api_url: 'string'
..     api_key: 'string'
..     performance_warnings:
..       # see below
.. 

