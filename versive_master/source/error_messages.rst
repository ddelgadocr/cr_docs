.. 
.. versive, primary, security engine
.. 

==================================================
Error Messages
==================================================

This topic describes the errors that may be encountered when running the |versive| platform. ``CRError`` is the standard exception class for the |versive| platform that is used to display error codes and messages in the Python interpreter.

Error messages are grouped into the following categories:

* :ref:`Generic or system errors <error-messages-generic-or-system>`
* :ref:`Argument validation errors <error-messages-argument-validation>`
* :ref:`Load errors <error-messages-load>`
* :ref:`Save and checkpoint errors <error-messages-save-and-checkpoint>`
* :ref:`Table split, join, and merge errors <error-messages-table-split-join-merge>`
* :ref:`Transform errors <error-messages-transform>`
* :ref:`Model learning errors <error-messages-model-learning>`
* :ref:`Prediction and evaluation errors <error-messages-prediction-and-evaluation>`


Enable Stack Traces
==================================================
To improve debugging and to enable stack tracing, set the ``CR_FULL_ERROR`` environment variable to a non-empty value at runtime. For example:

.. code-block:: console

   export CR_FULL_ERROR=1

When ``CR_FULL_ERROR`` is set to a non-empty value:

* Warnings and errors for problematic usage of the |versive| platform API may be printed to standard output.
* Inner exceptions and stack traces may be printed for uncaught errors that are defined as part of the standard API exception class in the |versive| platform API.

.. TODO: The CRError(Exception) class is not currently documented in the platform API docs. See user_error.pyx for more info.


.. _error-messages-generic-or-system:

Generic or System
==================================================
.. TODO: Needs overview.

.. list-table::
   :widths: 100 250 250
   :header-rows: 1

   * - Error Code
     - Message
     - Additional Information
   * - **10000**
     - Unknown exception.
     - 
   * - **10001**
     - Invalid or missing argument.
     - Verify that all required arguments are included in the function call and that they are of the correct type. Often, this is the result of forgetting to include quotation marks around string values.
   * - **10002**
     - Invalid error ID; no such error message.
     - 
   * - **10003**
     - Operation not implemented.
     - 
   * - **10004**
     - Cluster not initialized. Call :ref:`api-initialize-cluster` first.
     - Cluster initialization prepares the compute nodes to do tasks.
   * - **10005**
     - Operating system error.
     - 
   * - **10007**
     - Running ``crstart`` failed.
     - 
   * - **10008**
     - Matplotlib is not available. The Matplotlib library is required to use plot functions.
     - The minimum supported version of Matplotlib is 1.1.1. A supported Matplotlib library is required to use plot functions. If necessary, reinstall Matplotlib to update the version.


.. _error-messages-argument-validation:

Argument Validation
==================================================
.. TODO: Needs overview.

.. list-table::
   :widths: 100 250 250
   :header-rows: 1

   * - Error Code
     - Message
     - Additional Information
   * - **11001**
     - The specified columns contain invalid characters.
     - A column name must begin with a letter, must contain only letters, numbers, and underscores, and must not contain spaces or special characters.
   * - **11002**
     - The specified columns contain non-numeric values. A column must contain only ``int`` or ``float`` data types.
     - Verify that the schema matches the data.
   * - **11003**
     - The specified columns contain were not found.
     - 
   * - **11004**
     - The specified columns in the ``value_columns`` argument may contain only ``string`` or ``int`` values.
     - Verify that the schema matches the data. See :ref:`api-count-frequencies`.
   * - **11005**
     - The values specified for the ``input_columns`` argument must be in a list.
     - Some functions with the argument ``input_columns`` require values to be passed as strings, even if only a single column is passed in.
   * - **11007**
     - One (or more) unexpected keyword arguments are present.
     - Verify that the arguments are spelled correctly.
   * - **11008**
     - Invalid session property key.
     - See :ref:`api-set-session-property`.
   * - **11009**
     - Invalid session property value.
     - See :ref:`api-set-session-property`.
   * - **11010**
     - Tried to resolve collisions between column names in the data table and new columns that would have been generated.
     - Consider renaming the existing columns in the data table before proceeding. See :ref:`api-rename-columns`.
   * - **12008**
     - The specified model type is invalid.
     - 
   * - **12009**
     - The connection with the specified name was not found.
     - Verify that the named connection is set up correctly.
   * - **12010**
     - The specified column contains non-numeric values. Must contain only ints or floats.
     - Verify that the schema matches the data.



.. _error-messages-load:

Load
==================================================
.. TODO: Needs overview.

.. list-table::
   :widths: 100 250 250
   :header-rows: 1

   * - Error Code
     - Message
     - Additional Information
   * - **15001**
     - The loaded data is attempting to use an invalid row terminator.
     - The supported row terminator is ``\n``.
   * - **15002**
     - Only one row of data was found, which may indicate an invalid row terminator. Automatic delimiter detection requires more than one row of data.
     - The supported row terminator is ``\n``.
   * - **15003**
     - Only one row of data was found, which may indicate an invalid row terminator. Automatic schema detection requires more than one row of data.
     - The supported row terminator is \n.
   * - **15004**
     - Too many rows have a different number of columns than are specified in the schema.
     - Verify that the schema and data match.
   * - **15005**
     - Only one row of data was found, and it has a different number of columns than are specified in the schema. This may indicate an invalid row terminator.
     - The supported row terminator is ``\n``.
   * - **15007**
     - Specified path appears to be a CR table directory. Use :ref:`api-load-crtable` to load CR tables.
     - Raw data files are converted to CR tables when they are first loaded.
   * - **15008**
     - Only once instance of a hint created using :ref:`api-create-parser-hint-maxerr` can be specified.
     - To specify the maximum number of errors differently for individual columns, use :ref:`api-create-parser-hint-maxerr`.
   * - **15009**
     - An inconsistent number of fields are using the delimiter.
     - 

		


.. _error-messages-save-and-checkpoint:

Save and Checkpoint
==================================================
.. TODO: Needs overview.

.. list-table::
   :widths: 100 250 250
   :header-rows: 1

   * - Error Code
     - Message
     - Additional Information
   * - **16001**
     - The path must end with .csv.
     - 
   * - **16002**
     - The path must end with .xls.
     - 
   * - **16003**
     - Unrecognized file extension.
     - Verify the file extension. See :ref:`api-load-tabular`.
   * - **16005**
     - The argument ``max_errors`` must specify a value greater than or equal to 0.
     - See :ref:`api-create-parser-hint-maxerr`.
   * - **16006**
     - The argument ``additional_markers`` has no value specified. Must specify one (or more) markers for missing values.
     - See :ref:`api-create-parser-hint-missing`.
   * - **16008**
     - The argument ``max_percent`` must specify a value greater than or equal to 0 or less than or equal to 100.
     - See :ref:`api-create-parser-hint-table-maxerr`.
   * - **17001**
     - The item with the specified key was not found among the checkpointed items in memory.
     - Call :ref:`api-get-checkpoints` to see what items are checkpointed.

	



.. _error-messages-table-split-join-merge:

Table Split, Join, Merge
==================================================
.. TODO: Needs overview.

.. list-table::
   :widths: 100 250 250
   :header-rows: 1

   * - Error Code
     - Message
     - Additional Information
   * - **21004**
     - The value specified for proportions is out of range. Values must be greater than 0.
     - See :ref:`api-split-table`.
   * - **21005**
     - At least two tables must be specified.
     - 
   * - **21006**
     - The schemas for the tables to be merged do not match.
     - Columns may need to be reordered or removed. See :ref:`api-select-columns` or :ref:`api-delete-columns`.
   * - **21007**
     - At least two values must be specified for proportions.
     - See :ref:`api-split-table`.
   * - **21008**
     - The column names for the tables to be merged do not match.
     - See :ref:`api-rename-columns`.
   * - **23001**
     - All join types except cross join require at least one value in ``key_columns``.
     - See :ref:`api-join`.
   * - **23002**
     - Both tables must have the same number of values in ``key_columns``.
     - See :ref:`api-join`.
   * - **23003**
     - The specified column or columns appear in both tables.
     - All columns in a table must have unique names.
   * - **23004**
     - The specified join type is invalid.
     - See :ref:`api-join`.
   * - **23005**
     - Cross joins must not have a value in ``key_columns``.
     - See :ref:`api-join`.
   * - **23006**
     - Cross join is not supported for large tables in ``right_table``. Try again with a smaller table.
     - See :ref:`api-join` or :ref:`api-delete-columns`.
   * - **23008**
     - The column types specified for ``key_columns`` in the two tables do not match.
     - Verify that the schema and data match.
   * - **24001**
     - The specified column already exists.
     - All columns in a table must have unique names.
   * - **24002**
     - Cannot merge; table schemas do not match.
     - See :ref:`api-rename-columns`.



.. _error-messages-transform:

Transform
==================================================
.. TODO: Needs overview.

.. list-table::
   :widths: 100 250 250
   :header-rows: 1

   * - Error Code
     - Message
     - Additional Information
   * - **25002**
     - The specified aggregation operation for :ref:`api-add-context-from` is invalid.
     - See :ref:`api-create-context-custom-op` or :ref:`api-create-context-op`.
   * - **25003**
     - The number of specified input columns does not match the user-defined function requirements.
     - See user-defined function logging functions for debugging help.
   * - **25004**
     - The ``value_columns`` argument must have at least one value.
     - See :ref:`api-count-frequencies`.
   * - **25005**
     - The ``group_by_columns`` argument must have at least one value. This argument is optional in several functions, but must contain at least one value where it is required.
     - 
   * - **25006**
     - A column may not be specified in both the ``group_by_columns`` and ``value_columns`` arguments.
     - See :ref:`api-count-frequencies`.
   * - **25007**
     - Column/aggregation pairs can occur only once.
     - 
   * - **25008**
     - The column specified in the ``timestamp_columns`` argument for the first table was not found. Verify the column names.
     - 
   * - **25009**
     - The column specified in the ``timestamp_columns`` argument for the ``from_table`` was not found.
     - See :ref:`api-add-context-from`.
   * - **25010**
     - The specified column for the operation not found in the ``from_table``.
     - See :ref:`api-add-context-from`.
   * - **25011**
     - The specified start and end points are invalid. End point must be later than start point.
     - Use negative values to specify times in the past.
   * - **25012**
     - The specified aggregation operation not found.
     - 
   * - **25015**
     - The offset(s) are out of order. The end point must be later than the start point.
     - Use negative values to specify times in the past.
   * - **25016**
     - Unknown op(s).
     - Define the operation using :ref:`api-create-context-custom-op` or :ref:`api-create-context-op`.
   * - **25017**
     - Unknown unit(s).
     - 
   * - **25018**
     - ``timestamp_columns``, ``group_by_columns``, and ``aggregation`` columns in the ``from_table`` must be unique.
     - See :ref:`api-add-context-from`.
   * - **25019**
     - Invalid column specified in ``timestamp_columns``.
     - Timestamp column must contain datetime data.
   * - **25020**
     - Cannot redefine built-in operation.
     - 
   * - **25022**
     - Exception occurred while executing a user-defined function.
     - 
   * - **25023**
     - The user-defined function did not return the correct number of items.
     - 
   * - **25024**
     - The column specified in ``group_by_columns`` argument for the first table was not found.
     - Verify the column names.
   * - **25025**
     - The column specified in the ``group_by_columns`` argument for the ``from_table`` was not found.
     - Verify the column names.
   * - **25026**
     - Log statements must be added from inside a user-defined function.
     - 



.. _error-messages-model-learning:

Model Learning
==================================================
.. TODO: Needs overview.

.. list-table::
   :widths: 100 250 250
   :header-rows: 1

   * - Error Code
     - Message
     - Additional Information
   * - **29006**
     - Target column used for learning has missing or non-numeric values. Remove the rows with missing/non-numeric values.
     - See :ref:`api-select-rows`.
   * - **29009**
     - The train and test table schemas do not match.
     - 
   * - **29010**
     - The train and tune table schemas do not match.
     - 
   * - **29011**
     - The target column is not present in train table.
     - 
   * - **29012**
     - The target column is not present in tune table.
     - 
   * - **29013**
     - Datetime columns are not supported as inputs to models.
     - 
   * - **29014**
     - The target column name cannot start with a number.
     - Column names must begin with a letter and may contain only letters, numbers, and underscores (no spaces or special characters).



.. _error-messages-prediction-and-evaluation:

Prediction and Evaluation
==================================================
.. TODO: Needs overview.

.. list-table::
   :widths: 100 250 250
   :header-rows: 1

   * - Error Code
     - Message
     - Additional Information
   * - **30004**
     - An exception while encoding data for prediction occurred.
     - 
   * - **31002**
     - The specified binary threshold was not within the required range of ``[0, 1]``.
     - See :ref:`api-customize-explanations`.
   * - **32001**
     - The specified metric type for learning curve is invalid.
     - See :ref:`api-measure-learning-curve`.
   * - **32002**
     - The specified metric type for measuring input importance is invalid.
     - See :ref:`api-measure-input-importance`.
   * - **32003**
     - The specified metric type for scoring predictions is invalid.
     - See :ref:`api-score-predictions`.
   * - **32005**
     - The specified metric types must be a list of strings.
     - 
   * - **32006**
     - The specified report style is invalid.	
     - 
   * - **32007**
     - Invalid table provided.
     - The table must be an output of :ref:`api-predict`.
   * - **32008**
     - The model has multiple targets.
     - A model can have only a single target.
   * - **32009**
     - The specified table is not the output of :ref:`api-predict`.
     - 
   * - **32010**
     - The specified model type is invalid.
     - 
