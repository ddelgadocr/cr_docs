.. 
.. versive, primary, security engine
.. 

==================================================
Extensions
==================================================

.. include:: ../../includes_python/extensions_package.rst

.. note:: See :doc:`python` for information about how to import modules, run Python code against the |versive| platform API, and recommendations for using Python in this environment.

.. _extension-package-setup:

Package Setup
==================================================
Running custom code as part of the engine is necessary. To set up custom code to be run at various phases in the application workflow, do the following:

#. Verify the location of the extension package as specified by the **customization_package_path** setting in :doc:`the configuration file </configure>`.

#. In this location, start with the default extension package that is provided by the engine, and then extend it. The extension package is a single Python file. All extensions must be located in this file.

#. Each user-defined function that is added to the extension package should be located in this file. To associate a user-defined function with a phase in the application workflow, update the function-related settings in the appropriate section of the configuration file to specify the name of the user-defined function. 

#. A Bash script may be used to define a list of settings that configure any desired pre-phase behavior. Update the **pre_phase** setting that is located in the :ref:`configure-drive` section of the configuration file to specifiy the name of this script.

.. _extension-package-function-settings:

Function Settings
==================================================
Each user-defined function must be associated with a phase via specific settings in :doc:`the configuration file </configure>`:

.. list-table::
   :widths: 200 200 200
   :header-rows: 1

   * - Phase
     - Configuration Section
     - Setting Names
   * - **drive**
     - :ref:`configure-drive`
     - **pre_phase**
   * - **generate_modeling_table**
     - :ref:`configure-entity-attribute-sources`
     - **generate**
   * - 
     - :ref:`configure-generate-modeling`
     - **on_load_function**

       **on_save_function**
   * - **learn_models**
     - :ref:`configure-modeling`
     - **experimentation**

       **function**
   * - 
     - :ref:`configure-sampling`
     - **function**
   * - **predict**
     - :ref:`configure-predict`
     - **function**
   * - **postprocess**
     - :ref:`configure-results`
     - **details_formatter_factory_function**

       **formatter_factory_function**


.. _extension-package-py:

extension.py
==================================================
The name of the extension package is ``extension.py``.

.. _extension-package-example:

Example File
--------------------------------------------------
.. warning:: The following example shows the ``extension.py`` file, highly abbreviated. They are often different because they need to handle per-customer use cases.

.. code-block:: python

   import cr
   import cr.risp_v1 as risp
   from crepe.apt.transforms_category import flexi_categorize
   from crepe.apt.compat import make_column_metadata as Column

   ...

   def ms_to_seconds(duration):
     try:
       return duration / 1000.0
     except:
      return None

   ...




.. _extension-package-custom-blocks:

Blocks
==================================================
A custom block handles functionality that is not handled by an existing block from the blocks library. Custom blocks should be added to the :doc:`extension package <extensions>`.

.. note:: Blocks support Spark DataFrames. Custom blocks should be built using the Spark DataFrame API. A data frame is a concept of statistical software that generally refers to tabular data, which is a data structure that is represented by cases (rows) and where each case contains observations and measurements stored as column data. A Spark DataFrame is a distributed collection of data that is grouped into named columns. Apache Spark has a mature API for interacting with data frames: the Spark DataFrame API. All blocks (both library and custom) use `the Python-based Spark DataFrame API <https://spark.apache.org/docs/1.6.1/api/python/pyspark.sql.html#pyspark.sql.DataFrame/>`__. All of the functions in the Spark DataFrame API are supported for use with blocks. Refer to the Spark DataFrame API documentation for more information about each of the individual functions.

.. 
.. .. _extension-package-custom-blocks-types:
.. 
.. Block Types
.. --------------------------------------------------
.. Blocks provide four base classes as interfaces to data frames. Each base class provides specific input and output rules for tabular data:
.. 
.. .. list-table::
..    :widths: 150 250 250
..    :header-rows: 1
.. 
..    * - Base Class
..      - Input Rules
..      - Output Rules
..    * - BundleBlock
..      - One (or more) data frames.
..      - One (or more) data frames.
..    * - FrameBlock
..      - One Spark DataFrame; input columns are assumed to be identical to output, unless declared in the configuration file.
..      - One Spark DataFrame; output columns are assumed to be identical to input, unless declared in the configuration file.
.. 
.. 
.. .. _extension-package-block-examples:
.. 
.. EXAMPLES
.. --------------------------------------------------
.. Examples for custom blocks ensure that users can review the expected inputs and outputs of the block via output of the :ref:`command-bloxplain` command-line tool. For example, the input table:
.. 
.. .. code-block:: sql
.. 
..    -------------- ---------------
..     protocol:str   src_port: int
..    -------------- ---------------
..     TCP            12000
..     UDP            13000
..    -------------- --------------
.. 
.. and the output table:
.. 
.. .. code-block:: sql
.. 
..    -------------- -------------- ------------------------
..     protocol:str   src_port:int   output_column_name:str
..    -------------- -------------- ------------------------
..     TCP            12000          TCP_NOT_SNMP
..     UDP            13000          UDP
..    -------------- -------------- ------------------------
.. 
.. Examples define the input columns, and then the output. The structure of the EXAMPLES sections depend on the block type: :ref:`BundleBlock <extension-package-bundleblock-examples>` vs. :ref:`FrameBlock <extension-package-frameblock-examples>`.
.. 
.. .. _extension-package-bundleblock-examples:
.. 
.. BundleBlock EXAMPLES
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. All bundle blocks must define one (or more) examples that describe the input and output behavior of the block. These examples are added inline within the custom block by using the ``EXAMPLES`` structure, as shown below:
.. 
.. .. code-block:: python
.. 
..    EXAMPLES = [
..      BlockExample(
..        title='title',
..        config={},
..        input=key{
..          'key1_name': ExampleTable(
..            columns=[('column_name1', datatype), ('column_name2', datatype), ...],
..            rows=[
..              ('row_value',),
..              ('row_value',),
..              ('...',)]
..          ),
..          'key2_name': ExampleTable(
..            columns=[('column_name1', datatype), ('column_name2', datatype), ...],
..            rows=[
..              ('row_value',),
..              ('row_value',),
..              ('...',)]
..          ),
..        },
..        output={
..          'key4_name': ExampleTable(
..            columns=[('column_name1', datatype), ('column_name2', datatype), ...],
..            rows=[
..              ('row_value',),
..              ('row_value',),
..              ('...',)]
..          ),
..        }
..      )
.. 
.. where:
.. 
.. * ``title`` is a string that describes the block example.
.. * ``config`` specifies any block-specific settings that are necessary for the input and output example. This setting should be left empty when there are no block-specific settings. For example: ``config={}``.
.. * ``input`` defines the input as a table with the column list first, and then a list of rows in the table; ``key1_name`` and ``key2_name`` are input keys that are defined for the block under ``input_spec``. For ``columns``, valid datatypes are ``datetime``, ``int``, ``float``, and ``str``.
.. * ``output`` defines the output as a table, and then a list of rows in the table; ``key4_name`` is an input key that is defined for the block under ``output_spec``. For ``columns``, valid datatypes are ``datetime``, ``int``, ``float``, and ``str``.
.. 
.. .. note:: The ``config``, ``input``, and ``output`` sections are unique to each block. As such, the amount and type of information required for each of these three sections will vary.
.. 
.. The following example shows example input and output for a collect stats block:
.. 
.. .. code-block:: python
.. 
..    EXAMPLES = [
..      BlockExample(
..        title='Example usage',
..        config={},
..        input={
..          'parsed_row_count': ExampleTable(
..            columns=[('Table', str), ('NumRows', int)],
..            rows=[('parsed', 24255)]),
..          'parsed_error_row_count': ExampleTable(
..            columns=[('Table', str), ('NumRows', int)],
..            rows=[('parsed_errors', 245)])},
..        output={
..          'parsed_row_count_stats': ExampleTable(
..            columns=[('Error_percentage', float)],
..            rows=[(1.0,)])})
..    ]
.. 
.. This inputs two tables:
.. 
.. .. code-block:: sql
.. 
..    ------------------
..     parsed_row_count 
..    ------------------
..     1                 
..     2                 
..     ...               
..     24255             
..    ------------------
.. 
.. and:
.. 
.. .. code-block:: sql
.. 
..    ---------------
..     parsed_errors 
..    ---------------
..     1              
..     2             
..     ...            
..     245           
..    ---------------
.. 
.. and then outputs the following table:
.. 
.. .. code-block:: sql
.. 
..    ------------------
..     Error_percentage  
..    ------------------
..     1.0              
..    ------------------
.. 
.. 
.. .. _extension-package-frameblock-examples:
.. 
.. FrameBlock EXAMPLES
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. All frame blocks must define one (or more) examples that describe the input and output behavior of the block. These examples are added inline within the custom block by using the ``EXAMPLES`` structure, as shown below:
.. 
.. .. code-block:: python
.. 
..    EXAMPLES = [
..      BlockExample(
..        title='title',
..        config={},
..        input=ExampleTable(
..          columns=[('column_name1', datatype), ('column_name2', datatype), ...],
..          rows=[
..            ('row_value',),
..            ('row_value',),
..            ('...',)]
..        ),
..        output=ExampleTable(
..          columns=[('column_name1', datatype), ('column_name2', datatype), ...],
..          rows=[
..            ('column1_value', 'column2_value', ...),
..            ('column1_value', 'column2_value', ...),
..            ('...',)]
..        ),
..      )
..    ]
.. 
.. where:
.. 
.. * ``title`` is a string that describes the block example.
.. * ``config`` specifies any block-specific settings that are necessary for the input and output example. This setting should be left empty when there are no block-specific settings. For example: ``config={}``.
.. * ``input`` defines the input as a table with the column list first, and then a list of rows in the table. For ``columns``, valid datatypes include ``datetime``, ``int``, ``float``, and ``str``.
.. * ``output`` defines the output as a table, and then a list of rows in the table. For ``columns``, valid datatypes include ``datetime``, ``int``, ``float``, and ``str``.
.. 
.. .. note:: The ``config``, ``input``, and ``output`` sections are unique to each block. As such, the amount and type of information required for each of these three sections will vary.
.. 
.. The following example shows example input and output for a merge table block:
.. 
.. .. code-block:: python
.. 
..    EXAMPLES = [
..      BlockExample(
..        title='An example blocks example!',
..        config={'columns': [{'name': 'src_ip', 'type': 'str'}],
..                'category_type': 'ip'},
..        input=ExampleTable(
..          columns=[('src_ip', str)],
..          rows=[
..            ('10.9.8.7',),
..            ('8.8.8.8',),
..            ('127.0.0.4',),
..            ('216.34.181.45',)]
..        ),
..        output=ExampleTable(
..          columns=[('src_ip', str), ('src_ip_category', str)],
..          rows=[
..            ('10.9.8.7', 'internal'),
..            ('8.8.8.8', 'external'),
..            ('127.0.0.4', 'internal'),
..            ('216.34.181.45', 'external')]
..        )
..      )
..    ]
.. 
.. This inputs the following table:
.. 
.. .. code-block:: sql
.. 
..    +-----------------+
..    | src_ip          |
..    +-----------------+
..    | 10.9.8.7        | 
..    | 8.8.8.8         | 
..    | 127.0.0.4       | 
..    | 216.34.181.45   | 
..    +-----------------+
.. 
.. and then outputs the following table:
.. 
.. .. code-block:: sql
.. 
..    --------------- -----------------
..     src_ip          src_ip_category 
..    --------------- -----------------
..     10.9.8.7        internal        
..     8.8.8.8         external        
..     127.0.0.4       internal        
..     216.34.181.45   external        
..    --------------- -----------------
.. 
.. 
.. .. _extensions-custom:
.. 
.. Extending Blocks
.. --------------------------------------------------
.. A custom block handles functionality that is not handled by an existing block from :ref:`the blocks library <blocks-library>`.
.. 
.. * The Spark DataFrame API is the default (and recommended) way to define how each block will interact with the engine pipeline; Blocks and block graphs defined entirely by the Spark DataFrame API will allow Spark to determine the best ways to optimize data transformations and processing
.. * Custom blocks may be authored; custom blocks are defined in the :doc:`extension package </extensions>` using Python 2.7 and the Spark DataFrame API 1.6.1 (or higher)
.. 
.. 
.. .. warning:: If a custom block is implemented in ``blocks_classes.py`` it must be added to ``imports`` in ``extensions.py``.
.. 
.. .. note:: The best place to start with a custom block is to write out the the input and output rules---the column contrats---that will be required for the tabular data that is processed by the custom block. Don't worry about how the block will build and transform this data until after the input and output rules are defined. The next step is to define the configuration necessary to enforce the input and output rules. Then add the blocks to a block graph and verify the blocks fit together correctly. Finally, design the implementation for the block under the call method (``_inner_call``).
.. 
.. 
.. .. _extensions-custom-spark-dataframe-api:
.. 
.. Spark DataFrame API
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. A Spark DataFrame is a distributed collection of data that is grouped into named columns. Apache Spark has a mature API for interacting with dataframes: the Spark DataFrame API. All blocks (both library and custom) use `the Python-based Spark DataFrame API <https://spark.apache.org/docs/1.6.1/api/python/pyspark.sql.html#pyspark.sql.DataFrame/>`__.
.. 
.. .. note:: More examples of how to perform common data transformations with Spark DataFrames are in the `DataFrames and Datasets Guide <https://spark.apache.org/docs/1.6.0/sql-programming-guide.html#dataframe-operations/>`_.
.. 
.. The following example shows a block that uses a dataframe to input a table of users, filter that to only users younger than 21 years of age, and then to find the oldest members by gender:
.. 
.. .. code-block:: python
.. 
..    df = input_frame
..    df = df.filter(df.age < 21)
..    df = df.select(df.name, df.age + 1, df.gender)
..    df = df.groupBy("gender").agg({"age": "max"})
.. 
.. .. warning:: Gonna pull this example unless it's able to become specific to using the Versive Security Engine, which doesn't care at all about gender, people's ages, or number of users :)
.. 
.. All of the functions in the Spark DataFrame API are supported for use with blocks. Refer to the Spark DataFrame API documentation for more information about each of the individual functions.
.. 
.. 
.. .. _extensions-custom-base-classes:
.. 
.. Base Classes
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. A dataframe is a concept of statistical software that generally refers to tabular data, which is a data structure that is represented by cases (rows) and where each case contains observations and measurements stored as column data.
.. 
.. Blocks provide four base classes as interfaces to dataframes. Each base class provides specific input and output rules for tabular data:
.. 
.. .. list-table::
..    :widths: 150 250 250
..    :header-rows: 1
.. 
..    * - Base Class
..      - Input Rules
..      - Output Rules
..    * - ``BundleBlock``
..      - One (or more) dataframes; input columns are assumed to be identical to output, unless declared in the configuration file.
..      - One (or more) dataframes; output columns are assumed to be identical to input, unless declared in the configuration file.
..    * - ``FrameBlock``
..      - One Spark DataFrame; input columns are assumed to be identical to output, unless declared in the configuration file.
..      - One Spark DataFrame; output columns are assumed to be identical to input, unless declared in the configuration file.
.. 
.. 
.. .. note:: There are use cases where zero dataframes may be input to a block, such as to enable the parsing of raw data into tabular data based on the raw data content of log files that are ingested by the engine.
.. 
..    The :ref:`blocks-library-parse` library block is an example of a block that has been designed to input zero dataframes. It is typically the first block in a block graph, ingests raw log file data from various data sources, and then parses that data into valid tabular formats for use by subsequent blocks in the block graph. The configuration for the ``ParseBlock`` tells the engine how to understand the raw data, how to correctly parse that data, and then how to output that data into a tablular data format that can be understood by all other blocks in that block graph.
.. 
.. The input and output rules for a custom block are inherited from the base class and as such the first step in building a custom block is to decide the base class from which the custom block is derived.
.. 
.. For more information about each of the individual base classes, see the following sections:
.. 
.. * :ref:`extensions-custom-base-class-bundleblock`
.. * :ref:`extensions-custom-base-class-frameblock`
.. 
.. 
.. .. _extensions-custom-valid-data-types:
.. 
.. Valid Data Types
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. Required, optional, and referenced configuration settings may be declared for custom blocks. All declared configuration settings must one of the following valid data types:
.. 
.. * ``DictParser_``
.. * ``EnumParser_``
.. * ``ListParser_``
.. * ``parse_bool_``
.. * ``parse_float_``
.. * ``parse_int_``
.. * ``parse_ndarray_``
.. * ``parse_str_``
.. * ``PartitionsConfig``
.. 
.. 
.. 
.. .. _extensions-custom-column-contracts:
.. 
.. Column Contracts
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. A block defines what columns are expected as both input and output, and then saves that output as a ``FrameSpec`` object, which is a type of configuration object.
.. 
.. In configuration, it looks like:
.. 
.. .. code-block:: yaml
.. 
..    id:
..      'key1': 'value1'
..      'key2': 'value2'
..    columns:
..      - name: 'column1'
..        type: str
..      - name: 'column2'
..        type: int
..    extra_columns: ec_passed_through
.. 
.. where:
.. 
.. * The main contract is within the ``columns`` list, which specifies the names and types of each column that is expected
.. * The ``id`` list retrieves the key-value pair that defines the contract in code
.. * The ``extra_columns`` setting specifies the policy to apply to extra columns in the tabular data. Extra columns are columns not specfied as input or output in the configuration. Default value: ``ec_passed_through``.
.. 
..   Use ``ec_forbidden`` to prevent unspecified columns from being added to the tabular data output.
.. 
..   Use ``ec_ignored`` to IGNORE_SOMETHING.
.. 
..   Use ``ec_passed_through`` to pass all unspecified columns in the input tabular data to the output tabular data.
.. 
.. For example, the following code uses ``FrameSpec.of_raw`` to create a ``FrameSpec`` object that is similar to a configuration object. It specifies the ``KeyDict`` object, which is a list of key-value pairs, and then a list of column objects, which is a list of name-type pairs:
.. 
.. .. code-block:: python
.. 
..    frame = FrameSpec.of_raw_({'id': {'name': 'parsed'},
..                              'columns':
..                              [{'name': 'hostname',
..                                'type': 'str'},
..                               {'name': 'bytes_in',
..                                'type': 'int'}]})  
.. 
.. This frame enables configuration code similar to:
.. 
.. .. code-block:: yaml
.. 
..    id:
..      'name': 'parsed'
..    columns:
..      - name: 'hostname'
..        type: str
..      - name: 'bytes_in'
..        type: int
..    extra_columns: ec_passed_through
.. 
.. ``FrameSpec`` objects are mutable like any other configuration object and may be referenced in the following ways.
.. 
.. To return the ``KeyDict`` object:
.. 
.. .. code-block:: python
.. 
..    frame.id
.. 
.. returns ``KeyDict({'name': 'parsed'}``.
.. 
.. To return the name of a ``KeyDict`` object:
.. 
.. .. code-block:: python
.. 
..    frame.id['name']
.. 
.. returns ``'parsed'``.
.. 
.. To return the ``ColumnSpec`` object:
.. 
.. .. code-block:: python
.. 
..    frame.columns
.. 
.. returns ``list[ColumnSpec]``.
.. 
.. To return the name of a ``ColumnSpec`` object:
.. 
.. .. code-block:: python
.. 
..    frame.columns[0].name
.. 
.. returns ``''hostname'``.
.. 
.. To return the data type of a ``ColumnSpec`` object:
.. 
.. .. code-block:: python
.. 
..    frame.columns[1].type
.. 
.. returns ``'int'``.
.. 
.. 
.. 
.. .. _extensions-custom-base-class-bundleblock:
.. 
.. BundleBlock
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. A bundle block is useful for interacting with multiple sets of tabular data and their associated rows, observations, and measurements, for both input and output. A bundle block:
.. 
.. * Inputs multiple dataframes
.. * Outputs multiple dataframes
.. * Output columns are identical to input columns, unless otherwise specified in the configuration for that block
.. 
.. Interface
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The interface for a custom block that uses the ``BundleBlock`` as the base block is:
.. 
.. .. code-block:: python
.. 
..    class CustomBlockName(BundleBlock)
..      """
..      A description of the custom block, what it does, how it
..      is implemented within the block graph.
..      """
.. 
..      class ConfigParser(BundleBlock.ConfigParser):
..        ++remove_this++_required_elements = [
..          ('<setting>', data_type),
..          ...
..        ]
..        ++remove_this++_optional_elements = [
..          ('<setting>', data_type, default_value),
..          ...
..        ]
..        ++remove_this++_referenced_elements = [
..          '<setting>',
..          ...
..        ]
.. 
..        def validate(self)
..          ... validation for required/optional elements goes here ...
.. 
..      def __init__(self, actual_spec, block_config):
..        ... implemented by default ...
..        ... rarely overridden ...
.. 
..      def input_spec(self): 
..        ... optional. declares block inputs ...
..        ... some code ...
.. 
..      def output_spec(self):
..        ... required. declares block outputs ...
..        ... some code ...
.. 
..      def _inner_call(self, input_bundle):
..        ... required. declares block transforms ...
..        ... some code ...
.. 
.. 
.. ConfigParser
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. A custom block typically declares block-specific configuration settings by subclassing the ``ConfigParser`` class for this block base type:
.. 
.. .. code-block:: python
.. 
..    class ConfigParser(BundleBlock.ConfigParser):
.. 
.. Three types of configuration settings may be declared: required, optional, and referenced.
.. 
.. * A required setting must be specified in the configuration for this block. The ``_required_elements`` list declares all settings that are required by this block.
.. * An optional setting may be specified in the configuration; if not specified, the default value is applied. The ``_optional_elements`` list declares all settings that may be specified, along with their default values.
.. * A referenced setting is inherited from a setting in the configuration file that is specified outside of the configuration for this block. This block will inherit the values for referenced settings. The ``_referenced_elements`` list declares all settings this block for which values for referenced settings will be inherited.
.. 
.. .. note:: At least one required, optional, or referenced configuration setting must be declared for a custom block.
.. 
.. Custom blocks are subclassed from one of these base block class and inherit the ``ConfigParser`` schema. The ``ConfigParser`` schema for a custom block has the following syntax:
.. 
.. .. code-block:: python
.. 
..    class ConfigParser(BundleBlock.ConfigParser):
..      ++remove_this++_required_elements = [
..        ('setting_name', data_type),
..        ...
..      ]
.. 
..      ++remove_this++_optional_elements = [
..        ('setting_name', data_type, 'default_value'),
..        ...
..      ]
..      ++remove_this++_referenced_elements = [
..        'setting_name',
..        ...
..      ]
.. 
.. where:
.. 
.. * One of ``_required_elements``, ``_optional_elements``, or ``_referenced_elements`` must be declare at least one setting
.. * A setting that is listed under ``_required_elements`` must specify the ``'setting_name'`` and :ref:`a valid data type <extensions-custom-valid-data-types>`.
.. * A setting that is listed under ``_optional_elements`` must specify the ``'setting_name'``, :ref:`a valid data type <extensions-custom-valid-data-types>`, and the ``'default_value'``. If an optional setting is not specified in the configuration for a block in the block graph, the default value is applied.
.. * A setting that is listed under ``_referenced_elements`` is the ``'setting_name'`` that is inherited from elsewhere in the configuration file.
.. 
..   .. warning:: Changes to referenced configuration settings---either via updates to specified values or via some type of programmatic action---are picked up by any block that references that configuration setting.
.. 
.. ++remove_this++__init__
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The ``__init__`` method is implemented by default for this block type and should not require any customization.
.. 
.. input_spec
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The ``input_spec`` method is implemented by default for this block type and should not require any customization. With the exception of the :ref:`blocks-library-parse` library block, all bundle blocks are assumed to receive their input from the previous block in the block graph, which determins the columns that are input to this block.
.. 
.. output_spec
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The ``output_spec`` method must create the expected column names and types, and then return those columns as a ``BundleSpec`` container object. A ``BundleSpec`` is a dictionary of keys and values, where the keys are ``KeyDict`` objects (similar to a Python dictionary object) and the values are ``DataFrames``. For example:
.. 
.. .. code-block:: python
.. 
..    frame = f
..    frame_errors = e
..    output_key = KeyDict({'stage': 'parsed'})
..    errors_key = KeyDict({'stage': 'parsed_error'})
..    output_bundle = FrameBundle({output_key: frame,
..                                 errors_key: frame_errors})
..    return output_bundle
.. 
.. ++remove_this++_inner_call
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The ``_inner_call`` method produces one (or more) ``FrameBundle`` objects into which the block's output is stored as tabular data. The dataframes that are produced by the ``_inner_call`` method must match the columns defined in the ``output_spec`` method or else the engine will stop and report an error.
.. 
.. The keys in the ``FrameBundle`` object in a ``BundleBlock`` must be created inside the ``_inner_call`` method. A ``FrameBundle`` is a dictionary where they keys are ``KeyDict`` objects (similar to the Python dictionary object) and the values are dataframes.
.. 
.. For example:
.. 
.. .. code-block:: python
.. 
..    frame = f        # A Spark DataFrame
..    frame_errors = e # Another Spark DataFrame
..    output_key = KeyDict({'stage': 'parsed'})
..    errors_key = KeyDict({'stage': 'parsed_error'})
..    output_bundle = FrameBundle({output_key: frame,
..                                 errors_key: frame_errors})
..    return output_bundle
.. 
.. 
.. Example Block
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The following example shows a simple ``BundleBlock`` that takes input from two blocks, and then produces a single output:
.. 
.. .. code-block:: python
.. 
..    class SimpleStandardizeBlock(BundleBlock):
..      def _inner_call(self, in_bundle):
..        in_bundle[{'stage': 'lookup'}]
..        input_frame = in_bundle[{'stage': 'parsed'}]
..        output_frame = frame_standardize(input_frame)
..        output_key = self.contract_output_spec.frames[0].id
..        assert isinstance(output_key, KeyDict)
..        output_bundle = FrameBundle({output_key: output_frame})
..        return output_bundle
.. 
.. 
.. Example Configuration
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The following example shows configuration for a custom ``BundleBlock`` that takes input from two blocks, and then produces a single output:
.. 
.. .. code-block:: yaml
.. 
..    simple_standardize:
..      class_name:
..        symbol: 'SimpleStandardizeBlock'
..      inputs:
..        frames:
..          - id:
..            name: output_from_parse
..            columns:
..              - name: user
..                type: str
..              - name: source_ip
..                type: str
..            extra_columns: ec_passed_through
..          - id:
..            name: output_from_lookup
..            columns:
..              - name: dummy
..                type: str
..            extra_columns: ec_forbidden
..      outputs:
..        frames:
..          - id:
..              name: output_from_standardize
..          columns:
..              - name: hostname
..                type: str
..              - name: source_ip
..                type: str
..              - name: user
..                type: str
..              - name: user_real
..                type: str
..          extra_columns: ec_passed_through
.. 
.. 
.. 
.. .. _extensions-custom-base-class-frameblock:
.. 
.. FrameBlock
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. A frame block is useful for interacting with tabular data and its associated rows, observations, and measurements, such as adding columns or replacing the contents of columns that already exist in the tabular data. A frame block:
.. 
.. * Inputs a single dataframe as a Spark DataFrame
.. * Outputs a single dataframe as a Spark DataFrame
.. * Output columns are identical to input columns, unless otherwise specified in the configuration for that block
.. 
.. Interface
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The interface for a custom block that uses the ``FrameBlock`` as the base block is:
.. 
.. .. code-block:: python
.. 
..    class CustomBlockName(FrameBlock)
..      """
..      A description of the custom block, what it does, how it
..      is implemented within the block graph.
..      """
.. 
..      class ConfigParser(FrameBlock.ConfigParser):
..        ++remove_this++_required_elements = [
..          ('<setting>', data_type),
..            ...
..        ]
..        ++remove_this++_optional_elements = [
..          ('<setting>', data_type, default_value),
..           ...
..        ]
..        ++remove_this++_referenced_elements = [
..          '<setting>',
..          ...
..        ]
.. 
..        def validate(self)
..          ... define validation for configuration ...
.. 
..      def __init__(self, actual_spec, block_config):
..        ... implemented by default ...
..        ... rarely overridden ...
.. 
..      def input_spec(self):
..        ... implemented by default ...
..        ... rarely overridden ...
.. 
..      def output_spec(self):
..        ... required ...
..        ... type: FrameeSpec ...
..        ... create the expected column names and types ...
..        ... return the expected column names and types ...
.. 
..      def _inner_call(self, input_frame):
..        ... required ...
..        ... input_frame is a Spark DataFrame ...
..        ... returns a Spark DataFrame ...
..        ... some code ...
.. 
.. 
.. ConfigParser
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. A custom block typically declares block-specific configuration settings by subclassing the ``ConfigParser`` class for this block base type:
.. 
.. .. code-block:: python
.. 
..    class ConfigParser(FrameBlock.ConfigParser):
.. 
.. Three types of configuration settings may be declared: required, optional, and referenced.
.. 
.. * A required setting must be specified in the configuration for this block. The ``_required_elements`` list declares all settings that are required by this block.
.. * An optional setting may be specified in the configuration; if not specified, the default value is applied. The ``_optional_elements`` list declares all settings that may be specified, along with their default values.
.. * A referenced setting is inherited from a setting in the configuration file that is specified outside of the configuration for this block. This block will inherit the values for referenced settings. The ``_referenced_elements`` list declares all settings this block for which values for referenced settings will be inherited.
.. 
.. .. note:: At least one required, optional, or referenced configuration setting must be declared for a custom block.
.. 
.. Custom blocks are subclassed from one of these base block class and inherit the ``ConfigParser`` schema. The ``ConfigParser`` schema for a custom block has the following syntax:
.. 
.. .. code-block:: python
.. 
..    class ConfigParser(FrameBlock.ConfigParser):
..      ++remove_this++_required_elements = [
..        ('setting_name', data_type),
..        ...
..      ]
.. 
..      ++remove_this++_optional_elements = [
..        ('setting_name', data_type, 'default_value'),
..        ...
..      ]
..      ++remove_this++_referenced_elements = [
..        'setting_name',
..        ...
..      ]
.. 
.. where:
.. 
.. * One of ``_required_elements``, ``_optional_elements``, or ``_referenced_elements`` must be declare at least one setting
.. * A setting that is listed under ``_required_elements`` must specify the ``'setting_name'`` and :ref:`a valid data type <extensions-custom-valid-data-types>`.
.. * A setting that is listed under ``_optional_elements`` must specify the ``'setting_name'``, :ref:`a valid data type <extensions-custom-valid-data-types>`, and the ``'default_value'``. If an optional setting is not specified in the configuration for a block in the block graph, the default value is applied.
.. * A setting that is listed under ``_referenced_elements`` is the ``'setting_name'`` that is inherited from elsewhere in the configuration file.
.. 
..   .. warning:: Changes to referenced configuration settings---either via updates to specified values or via some type of programmatic action---are picked up by any block that references that configuration setting.
.. 
.. ++remove_this++__init__
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The ``__init__`` method is implemented by default for this block type and should not require any customization.
.. 
.. input_spec
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The ``input_spec`` method is implemented by default for this block type and should not require any customization.
.. 
.. output_spec
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The ``output_spec`` method must create the expected column names and types, and then return those columns as a Spark DataFrame. A ``FrameBlock`` often returns the input columns as the output:
.. 
.. .. code-block:: python
.. 
..    def output_spec(self):
..      return self.input_spec
.. 
.. In some cases, a ``FrameBlock`` returns the input columns plus any columns that were specified in the configuration for the block as the output:
.. 
.. .. code-block:: python
.. 
..    def output_column_name(self):
..      if self.block_config.output_column_name:
..        return self.block_config.output_column_name
..      return '_'.join([col.name for col in self.block_config.columns])
.. 
.. ++remove_this++_inner_call
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The ``_inner_call`` method uses the Spark DataFrame API to produce a Spark DataFrame into which the block's output is stored as tabular data. The dataframe that is produced by the ``_inner_call`` method must match the columns defined in the ``output_spec`` method or else the engine will stop and report an error.
.. 
.. The following example shows a block that copies its input columns:
.. 
.. .. code-block:: python
.. 
..    def _inner_call(self, frame):
..      table = frame_to_table(frame)
..      udf = CleanupRegex(retain_raw_hostname=False)
..      for col in self.block_config.columns:
..        table = risp.replace_columns(table, col.name, Column(col.name, str), udf)
..      return table_to_frame(table)
.. 
.. The code block uses the ``udf`` property to represent a custom function, such as one that cleans up regular expressions that have parsed incorrectly, such as removing extra characters and ensuring that the hostname is propertly represented across systems.
.. 
.. Example Block
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The following example shows a custom ``FrameBlock``:
.. 
.. .. code-block:: python
.. 
..    class KeepBackupsBlock(FrameBlock):
..      """
..      Copy columns, prepending the prefix to the original name.
..      If no prefix is passed in from the configuration, the default is 'raw_'.
..      """
..      class ConfigParser(FrameBlock.ConfigParser):
..        ++remove_this++_required_elements = [('columns', ListParser_(ColumnSpec))]
..        ++remove_this++_optional_elements = [('prefix', parse_str_, 'raw_')]
.. 
..      def __init__(self, actual_spec, block_config):
..        super(KeepBackupsBlock, self).__init__(actual_spec, block_config)
..        cols_not_in_frame = [col for col in self.block_config.columns if col not in self.input_spec.columns]
..        if cols_not_in_frame:
..          raise ValueError("Columns to backup were not found in the input frame: {0}".format(cols_not_in_frame))
.. 
..      def output_spec(self):
..        new_columns = [ColumnSpec.of_raw_({'name': self.block_config.prefix + col.name,
..                                           'type': col.type})
..          for col in self.block_config.columns]
..          output_spec = copy.deepcopy(self.input_spec)
..          output_spec.columns.extend(new_columns)
..          return output_spec
.. 
..      def _inner_call(self, frame):
..        table = frame_to_table(frame)
..        udf = CleanupRegex(retain_raw_hostname=False)
..        for col in self.block_config.columns:
..          table = risp.replace_columns(table, col.name, Column(col.name, str), udf)
..        return table_to_frame(table)
.. 
.. Example Configuration
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The following example shows configuration for a custom block named ``KeepBackupsBlock`` that is based on the ``FrameBlock`` block type. The ``bkups`` block takes successfully parsed data and outputs a table with columns for source IP address and timestamp data. The ``ListParser_`` data type creates the ``columns`` list, in which each column is specified by a ``name`` and a data ``type``:
.. 
.. .. code-block:: yaml
.. 
..    std_graph:
..      depends: 
..        bkups:
..          - block_name: 'parsed'
..            frame_key: {}
..      blocks:
..        bkups:
..          class_name:
..            symbol: KeepBackupsBlock
..          columns:
..            - name: source_ip
..              type: str
..            - name: timestamp
..              type: str
.. 
.. 
.. 
.. Advanced Blocks
.. ++++++++++++++++++++++++++++++++++++++++++++++++++
.. The following sections describe advanced blocks configuration and design approaches that are optional.
.. 
.. 
.. Inherited Settings
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. Some types of settings in the configuration are actually defined by classes that are referenced as a "setting" but are expanded in the configuration file itself to be multiple settings. For example, paritions:
.. 
.. .. code-block:: yaml
.. 
..    ...
..      ++remove_this++_optional_elements = [
..        ('partitions', PartitionsConfig, PartitionsConfig.default_node()),
..        ('delimiter', parse_str_, None),
..        ]
..    ...
.. 
.. are actually a group of settings controlled by ``PartitionsConfig``. When that setting group is used to define configuraiton, it enables the following settings in the configuration file for that block:
.. 
.. * ``per_worker_after_load_partitions``
.. * ``per_worker_before_save_partitions``
.. * ``total_after_load_partitions``
.. * ``total_before_save_partitions``
.. 
.. Instead the partitioning behavior could be implemented like this (code block edited to fit the page):
.. 
.. .. code-block:: python
.. 
..    class PartitionsConfig(ConfigNodeWithDefault):
..      ++remove_this++_optional_elements = [
..        ('total_after_load_partitions', parse_int_, None),
..        ('per_worker_after_load_partitions', parse_int_, None),
..        ('total_before_save_partitions', parse_int_, None),
..        ('per_worker_before_save_partitions', parse_int_, None),
..       ]
.. 
..      def validate(self):
..        super(PartitionsConfig, self).validate()
..    
..        if self.total_after_load_partitions
..          and self.per_worker_after_load_partitions:
..          raise ValueError('error message is here.')
..        if self.total_before_save_partitions
..          and self.per_worker_before_save_partitions:
..          raise ValueError('error message is here.')
.. 
.. but since it already exists in the configuration file, and since the partitioning behavior is intended to be used the same way across all engine phases for all configuration types, it's preferable to just reference the class itself via the blocks configuration.
.. 
.. 
.. Override __init__
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. The ``__init__`` method is rarely overridden by customized code. The following example shows how to override the ``__init__`` method to perform extra checking of the schema that is input to the block, and then verify that the columns specified in the configuration are actually present in the input.
.. 
.. .. warning:: This example might not be the "right way to do this" and is therefore a hack just to show that it can be done. Talk with engineering before implementing any crazy stuff like this. kthxbi.
.. 
.. .. code-block:: python
.. 
..    def __init__(self, actual_spec, block_config):
..      super(BackupFrameBlock, self).__init__(actual_spec, block_config)
..      cols_not_in_frame = [col for col in self.block_config.columns
..                           if col not in self.input_spec.columns]
..      if cols_not_in_frame:
..        raise ValueError("Columns to backup were not found in the input frame:
.. 	     {0}".format(cols_not_in_frame))
.. 
.. 







.. _extension-package-webui-drilldowns:

Web UI Drilldowns
==================================================
A drilldown provides additional Web-based context for anomalies that are discovered by |vse|. A drilldown must be defined in the extension package before it can be visible from the Web UI. Each drilldown that is defined in the extension package uses the ``DrillDownBase`` class to specify which anomalies are to be included in that drill down, and then how to present the information about that anomaly type in the Web UI. 

DrillDownBase Class
--------------------------------------------------
``DrillDownBase`` is a class that provides the framework to define anomaly drilldowns for use in the |vse| Web UI. ``DrillDownBase`` is implemented in the extension package.

The structure of a drilldown is as follows:

.. code-block:: python

   class DrillDownBase(object):
     __metaclass__ = abc.ABCMeta

     supported_measurements = []

     def __init__(self, lazy_ad_loader):
       self.lazy_ad_loader = lazy_ad_loader

     @abc.abstractmethod
     def get_columns(self):
       pass

     def get_column_unit_map(self):
       return {}

     def get_friendly_column_name_map(self):
       return {}

     @abc.abstractmethod
     def generate_drilldowns(self, anomalies, **kwargs):
       pass


Supported Measurements
++++++++++++++++++++++++++++++++++++++++++++++++++
Use ``supported_measurements`` to specify a list of target measurements that are defined in the configuration file, and to be used by this drilldown. For example:

.. code-block:: yaml 
 
   cumulative_proxyweb_external_bytes_out_sum_7_day

is picked up by:

.. code-block:: python

   ...

     supported_measurements = [proxyweb_external_bytes_out_sum]



Add Columns
++++++++++++++++++++++++++++++++++++++++++++++++++
Use ``get_columns`` to specify a list of column names to be added to all tables that are generated for this drilldown. The order of these columns should match the order of columns produced by ``cr.DataTables``. For example:

.. code-block:: python

   def get_columns(self):
     return ['date', 'fqdn', 'bytes_out', 'publication_ratio']

Map Columns to Units
++++++++++++++++++++++++++++++++++++++++++++++++++
Optional. Use ``get_column_unit_map`` to return a dictionary of column names and column units. The column names must exist in the list of column names defined by ``get_columns``. The column units must be one of ``UNIT_BYTES``, ``UNIT_PORTS``, ``UNIT_SERVICES``, ``UNIT_SYSTEMS`` or ``UNIT_UNKNOWN``. For example:

.. code-block:: python

   def get_column_unit_map(self):
     return { 'bytes_out': UNIT_BYTES }

Assign Friendly Names
++++++++++++++++++++++++++++++++++++++++++++++++++
Optional. Use ``get_friendly_column_name_map`` to return a dictionary that maps actual column names to the string used for the column in the Web UI. The column names must exist in the list of columns defined by ``get_columns``.

.. code-block:: python

   def get_friendly_column_name_map(self):
     return { 'date': 'Date',
              'fqdn': 'FQDN',
              'bytes_out': 'Bytes Transferred',
              'publication_ratio': 'Publication Ratio' }


Generate Drilldowns
++++++++++++++++++++++++++++++++++++++++++++++++++
Use ``generate_drilldowns`` to input a collection of anomaly objects (where an anomaly object is a row in the details.csv table), and then returns a dictionary of drilldown tables, one per anomaly, where each key is a unique anomaly identifier and each value is a ``cr.Datatable`` that represents the drilldown for that anomaly. For example:

.. code-block:: python

   def generate_drilldowns(self, anomalies, **kwargs):
     drilldown_map = {}
     for anomaly in anomalies:
       rows = []
       for breakdown in anomaly['window_breakdown']:
         breakdown_str = breakdown.get('drilldown', None)
           if breakdown_str:
             breakdown_list = json.loads(breakdown_str)
             for target in breakdown_list:
             rows.append((breakdown['occurred_on'], target))

Each anomaly that is derived from multiday measurements may use the ``window_breakdown`` property to get a collection of objects:

.. code-block:: python

   anomalies[0]['window_breakdown']

or the risk score for a single day in the window:

.. code-block:: python

   anomalies[0]['window_breakdown'][0]['risk_score']


Drilldown Examples
--------------------------------------------------
The following examples show how to build drilldowns for the Web UI

DNS Records
++++++++++++++++++++++++++++++++++++++++++++++++++
The following example shows how to build a drilldown that uses multiday measurements for A, AAAA, and PTR DNS records to discover anomalies in DNS data, and then present a drilldown in the Web UI that shows for each anomaly, the date and target that has been specified for recon measurements in the configuration file.

The configuration file targets:

.. code-block:: yaml

   modeling:
     ...
     behaviors:
       - key: recon_via_dns
         stages: [3]
         targets:
           dns_internal_A_named_domain_list_multi_day_count_distinct_fill_default_lookback_0_30_days: weak
           dns_internal_AAAA_named_domain_list_multi_day_count_distinct_fill_default_lookback_0_30_days: weak
           dns_internal_PTR_dest_ip_list_multi_day_count_distinct_fill_default_lookback_0_30_days: strong

The drilldown in the extension package:

.. code-block:: python

   class DNSCountDistinctDrillDown(DrillDownBase):
     supported_measurements = ['dns_internal_AAAA_named_domain_list',
                               'dns_internal_A_named_domain_list',
                               'dns_internal_PTR_dest_ip_list']

     def get_columns(self):
       return ['Date', 'Target']
 
     def generate_drilldowns(self, anomalies, **kwargs):
       drilldown_map = {}
       for anomaly in anomalies:
         rows = []
         for breakdown in anomaly['window_breakdown']:
           breakdown_str = breakdown.get('drilldown', None)
             if breakdown_str:
               breakdown_list = json.loads(breakdown_str)
               for target in breakdown_list:
               rows.append((breakdown['occurred_on'], target))

       drilldown_table = None

       if len(rows) > 0:
         drilldown_table = DataTable.create(rows, column_names=self.get_columns())
         drilldown_map[anomaly['id']] = drilldown_table

       return drilldown_map


Sum of Bytes Sent
++++++++++++++++++++++++++++++++++++++++++++++++++
The following example shows how to build a drilldown that uses measurements for date, fully-qualified domain name (FQDN), bytes sent, and the publication ratio (which looks for unusually high publication ratios):

.. code-block:: python

   class NetflowExternalBytesOutDrillDown(DrillDownBase):

     supported_measurements = ['netflow_external_bytes_out_sum']

     def get_columns(self):
       return ['date', 'fqdn', 'bytes_out', 'publication_ratio']

     def get_column_unit_map(self):
       return { 'bytes_out': UNIT_BYTES }

     def get_friendly_column_name_map(self):
       return { 'date': 'Date',
                'fqdn': 'FQDN',
                'bytes_out': 'Bytes Transferred',
                'publication_ratio': 'Publication Ratio' }

     def generate_drilldowns(self, anomalies, **kwargs):
       assert len(set([a['window_start'] for a in anomalies])) == 1
       assert len(set([a['window_end'] for a in anomalies])) == 1
       lookup_dates = self.get_anomaly_dates_in_window(anomalies[0])
       hosts = set([a['host'] for a in anomalies])

       drilldown_table_ad_keys = [t for t in self.lazy_ad_loader if t['dataset'] ==
         'transfer_drilldown_table' and t['start'] in lookup_dates]
       drilldown_tables = []
 
       for ad_key in drilldown_table_ad_keys:
         drilldown_table = self.lazy_ad_loader[ad_key]
         drilldown_table = risp.select_rows(drilldown_table,
                                            ['host', 'datasource'],
                                            lambda x,y: x in hosts and y == 'netflow')
         drilldown_tables.append(drilldown_table)
         relevant_drilldowns = self.merge_table_list(drilldown_tables)
         relevant_drilldowns._data.cache()

         drilldown_map = {}

         for anomaly in anomalies:
           table = risp.select_rows(relevant_drilldowns,
                                    ['host', 'fqdn'],
                                    lambda x,y: x == anomaly['host'] and y == anomaly['fqdn'])
           table = risp.select_columns(table, ['date', 'fqdn', 'bytes_out', 'publication_ratio'])
           drilldown_map[anomaly['id']] = table

     return drilldown_map
