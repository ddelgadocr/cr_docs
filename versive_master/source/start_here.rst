.. 
.. versive, primary, security engine
.. 

==================================================
Start Here
==================================================

.. image:: ../../images/under_construction.svg
   :width: 600 px
   :align: center

.. warning:: This topic is under review. The steps below are true for the pre-blocks version of the security engine.

To start working with |vse|, follow the steps below to familiarize yourself with the application and begin processing data. The engine must already be installed and connected to your data storage cluster.
 
#. Review :ref:`the phases that consitute the end-to-end application workflow <workflow-run-security-engine>`.

#. Verify that the raw data to be ingested is a :ref:`supported data source type <process-data-sources>`. Using DNS, netflow, and proxy data is built into the engine, as well as support for building entity attribute tables from that raw data. All other data types will need to be ingested as raw data that is supported by custom code for parsing that data and then building it into DataTables. 

#. Inspect the format of the raw data. Is it in a tabular format? Is it unstructured, such as from a log file? Tabular data can be injested using the :ref:`api-load-tabular` function. Unstructured data must be :ref:`parsed using regular expressions <process-data-yaml-patterns>`.

#. Determine if the optional **prepare** phase needs to be run prior to parsing data. If raw data needs to be resharded to speed up processing, large blocks of text need to be broken down, or Unicode characters need to be removed, the **prepare** phase should be run.

#. Review :ref:`the canonical form <process-data-sources>`---the required columns---for DNS, flow, and proxy data. If the raw data for these source types differs from the data required by the canonical form, these differences must be reconciled.

   Are there required columns missing from the raw data? Is there a way to work around this? Or does the raw data need to be of a higher quality?

   Are additional columns present in the raw data that may help improve the data science approach? Should those columns be added to the canonical form for this source type?

#. If the raw data is unstructured, create the :ref:`regular expressions required to parse that data <process-data-yaml-patterns>` into the columns required by the source type, as well as any additional columns to be included in the model. Use :ref:`regex_debugger.py <process-data-regex-debugger>` to debug and validate regular expressions as needed.

#. Create the configuration file using only the :ref:`configure-required-settings` as the starting point, and then add configuration settings as the customer environment and desired results require.

#. Run ``self_test.py`` to verify that the engine successfully runs end-to-end on synthentic data.

#. Run ``site_test.py`` to verify that the engine successfully runs end-to-end on a sample of real data. This can be done in "tiny" mode with a single processor or (preferably) on "medium" mode with multiple processors. Sample datasets are necessary and must be of the appropriate size so that a tiny set completes in ~10 minutes and a medium set complets in ~1 hour.

   When run daily using the same configuration file as is used with the full dataset, these tests provide a quality heartbeat.

#. Run the :ref:`command-drive` command to learn an initial model, and then review the results.

#. After an initial model is learned successfully, :doc:`experiment with all of the configuration settings </configure>` to tune and improve the model quality.

#. Consider using :ref:`a generated table <model-data-generated-table>` to provide additional context that can improve model quality.

#. Determine at what phases :doc:`additional extensions </extensions>` are necessary.







