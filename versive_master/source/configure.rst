.. 
.. versive, primary, security engine
.. 

==================================================
Configuration
==================================================

The configuration file (``config.yaml``) specifies all of the settings (both required and optional) needed to configure each of the phases of the engine, including specifying any site-specific extensions.

The configuration file is divided into several sections that provide many properties that determine how to process data in the engine, such as how to ingest the various data sources, what transforms or aggregations to apply, how to learn the models, and more.

.. warning:: The engine does not support hyphens in column names; hyphens in column names are converted to underscores automatically.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

.. _configure-global-settings:

Global Settings
==================================================
Global configuration settings are listed at the top of the configuration file. These settings are common to all data sources and phases.

Usage
--------------------------------------------------
The configuration file has some global settings and sections to configure groups of application phase-specific settings.

.. code-block:: yaml

   assert_dataframe_propagation: True
   base_path: '/path/to/working/directory'
   consumption_ratio_threshold: 3.0
   customization_package_path: '/path/to/extensions.py'
   date_path: '%Y%M%d/data'
   default_connection: 'named_connection_to_cluster'
   encoding: 'string'
   exclusivity_threshold: 1.0
   list_limit: 500
   log_level: 'DEBUG'
   partitions:
     - per_worker_after_load_partitions: int
     - per_worker_before_save_paritions: int
     - total_after_load_partitions: int
     - total_before_save_partitions: int
   performance_warnings:
     allowed_only: []
     disallow: []
     ignore: []
   prohibit_rdd_column_types_detection: False
   publication_ratio_threshold: 2.0
   retain_all_modeling_columns: False
   retry_delay_seconds: 60
   spark_cluster_args:
     - 'argument'
     - ...
   block_graphs:
     ...
   category_patterns:
     ...
   check_quality:
     ...
   drive:
     ...
   entity_attribute_sources:
     ...
   flighting:
     ...
   generate_lookup         # global to all data sources
     ...
   generate_modeling:
     ...
   modeling:
     ...
   multi_day_features:
     ...
   predict:
     ...
   results:
     ...
   sampling:
     ...
   workspaces:
     ...

.. TODO: config, not documented:
.. 
..   heimdallr:
..     site: 'string'
..     api_url: 'string'
..     api_key: 'string'
..     performance_warnings:
..       # see below
.. 




.. note:: The order of settings in the configuration file is not important. As long as the settings are listed in the correct groupings, that required settings are present (with correct values), and that all values are parsed correctly, the configuration file is valid. You may order the settings in any manner you desire.

Settings
--------------------------------------------------
The top-level group of settings is described below, with the global settings listed first followed by groups of settings typically (but not always) associated with phases in the engine workflow.

**assert_dataframe_propogation** (bool, optional)
   .. include:: ../../includes_terms/term_table_dataframe.rst

   Specifies if a DataFrame is used for the underying compute infrastructure. Default value: ``True``.

**base_path** (str, required)
   The path to a directory on the master node, in which the subdirectories for inputs to and outputs from the engine are located.

   .. note:: Change the base path to force the regeneration of data. This may be necessary after an upgrade of the |vse| or the |versive| platform. The **first_date** setting in the :ref:`configure-multi-day-features` section of the configuration file specifies the date from which regeneration is performed. If the typical reporting window is 4 weeks, then set **first_date** to 5 weeks prior to the current date to regenerate the full data set. If the reporting window does not have historical model inputs, an shorter timeframe may be specified, such as 2 weeks.

**customization_package_path** (str, optional)
   The path to a Python file **or** the path to a directory in which Python files required by the :doc:`extension package </extensions>` are located.

**date_path** (str, required)
   A string that identifies the format used for naming directories by date and time. The format must be defined using :ref:`strptime syntax <python-strptime-syntax>` and may include a /data subdirectory. For example: ``"%Y%M%d/data"``.

**default_connection** (str, required)
   The named connection to the storage cluster that must already be set up (via **cr connect add**).

**encoding** (str, optional)
   The encoding type. For example: ``ISO-8859-1``. Default value: ``None`` (no encoding type specified).

**list_limit** (int, optional)
   The upper limit on list size when aggregating using ``list`` or ``count_distinct`` operations. Use this setting to help prevent running out of memory errors in situations where there are a large list of hosts. Default value: ``500``.

   Use **list_limit** within the **aggregate** setting group to override this setting.

**log_level** (enum, optional)
   The logging level for messages saved to the log file. Possible values: ``CRITICAL``, ``DEBUG``, ``ERROR``, ``INFO``, or ``WARNING``. Default value: ``INFO``.

.. include:: ../../includes_config/partitions.rst

**performance_warnings** (optional)
   .. include:: ../../includes_config/performance_warnings.rst

**prohibit_rdd_column_types_detection** (bool, optional)
   Specifies if calls to get column types will fail. Set to ``True`` to attempt to catch multiple materializations of same tables (RDDs). Default value: ``False``.

**retain_all_modeling_columns** (bool, optional)
   Specifies if columns that contain lists or sets are kept in the modeling table. Columns such as lists of domains or destination IP addresses that are associated with the source entity can consume significant memory and are dropped by default when the modeling table is loaded. Default value: ``False``.

   .. note:: Lists of users and any list or set columns specified by the ``extra_columns_in_details``, ``extra_host_columns_in_details``, or ``extra_user_columns_in_details`` settings in the :ref:`configure-results` section are always retained.

**retry_delay_seconds** (int, optional)
   Use to specify the amount of time (in seconds) to wait before re-running a failed **drive** command. Default value: ``60``.

**spark_cluster_args** (list of str, optional)
   One (or more) cluster arguements for Spark. For example: ``spark.ui.showConsoleProgress`` or ``spark.sql.broadcastTimeout``.

The following settings represent the top-level settings for specific groups of settings related to specific phases in the application:

**category_patterns** (optional)
   For more information about this configuration section, see :ref:`configure-category-patterns`.

.. 
.. **check_quality** (optional)
..    For more information about this configuration section, see :ref:`configure-check-quality`.
.. 

**drive** (optional)
   For more information about this configuration section, see :ref:`configure-drive`.

**entity_attribute_sources** (optional)
   For more information about this configuration section, see :ref:`configure-entity-attribute-sources`.

**flighting** (optional)
   For more information about this configuration section, see :ref:`configure-flighting`.

**generate_lookup** (optional)
   For more information about this configuration section, see :ref:`configure-generate-lookup-global`

**generate_modeling** (optional)
   For more information about this configuration section, see :ref:`configure-generate-modeling`.

**modeling** (optional)
   For more information about this configuration section, see :ref:`configure-modeling`.

**multi_day_features** (required)
   For more information about this configuration section, see :ref:`configure-multi-day-features`.

**predict** (optional)
   For more information about this configuration section, see :ref:`configure-predict`.

**results** (optional)
   For more information about this configuration section, see :ref:`configure-results`.

**sampling** (optional)
   For more information about this configuration section, see :ref:`configure-sampling`.

**workspaces** (optional)
   For more information about this configuration section, see :ref:`configure-workspaces`.







.. 
.. .. _configure-aggregate:
.. 
.. aggregate
.. ==================================================
.. **aggregate** is an optional subsection under **sources** that specifies the default aggregations to turn on (or off) and also specifies additional aggregations to be applied to the named data source. An aggregation can be simple aggregations that are already defined in the API for the engine, such as **sum** or **mean** or it can be a custom aggregation located in the :doc:`extension package </extensions>`.
.. 
.. Usage
.. --------------------------------------------------
.. The **aggregate** group of settings is located under **sources**, with one **aggregate** section defined for each source type:
.. 
.. .. code-block:: yaml
.. 
..    sources:
..      <source_type>:
..        aggregate:
..          additional:
..            - input: 'column_name'
..              list_limit: int
..              operation: 'operation'
..              replace_aggregate_column: True
..            ...
..          additional_attribute:
..            - attribute_source_name: 'name_of_table'
..              join: True
..              left_columns:
..                - 'column_name'
..                - ...
..            ...
..          after_save_functions:
..            - 'function_name'
..            - ...
..          assert_dataframe_propogation: False
..          block_names:
..            - block_name: 'block_name'
..              ...
..            - block_name: 'block_name'
..              ...
..          custom:
..            - inputs: ['column_name', 'column_name'...]
..              outputs: ['column_name', 'column_name', ...]
..              function: 'function_name'
..            ...
..          default_validation: True
..          list_limit: int
..          on_load_function: 'function_name'
..          on_save_function: 'function_name'
..          partitions:
..            - per_worker_after_load_partitions: int
..            - per_worker_before_save_paritions: int
..            - total_after_load_partitions: int
..            - total_before_save_partitions: int
..          replace_function: 'function_name'
..          turn_off:
..            - 'aggregation'
..            - ...
..          turn_on:
..            - 'aggregation'
..            - ...
..          use_categorical_aggregation: True
..          use_timestamp_type: True
..          validation
..            <column>:
..              missing:
..                count: int
..                percent: int
..            <column>:
..              regex:
..                count: int
..                pattern: 'string'
..                percent: float
.. 
.. .. include:: ../../includes_config/source_type_usage.rst
.. 
.. Settings
.. --------------------------------------------------
.. Each setting for the **aggregate** group of settings is described below, with the top-level **aggregate** setting listed first, after which all other settings are listed alphabetically.
.. 
.. **aggregate**
..    The parent setting group for the **aggregate** section.
.. 
.. **additional** (list of str, optional)
..    A list of pre-defined aggregations to be applied. Each aggregation is defined as a list of **input** and **operation** pairs.
.. 
..    **input** (str, required)
..       The column to use as the input to the operation.
.. 
..    **list_limit** (int, see **operation**)
..       If **operation** is set to ``count_distinct`` or ``list``, use to override the top-level **list_limit** value for this input/operation pair. If not specified, defaults to the **list_limit** value specified for the entire **aggregate** setting block.
.. 
..       When **operation** is set to ``count_distinct``, specifies the maximum number of entites counted.
.. 
..       When **operation** is set to ``list``, specifies the maximum number of aggregated entities in the result.  
.. 
..    **operation** (str, required)
..       The aggregate operation to apply. Possible values: ``categorical_sum``, ``count``, ``count_distinct``, ``count_ngrams``, ``empty``, ``hourly_sum``, ``list``, ``max``, ``mean``, ``median``, ``merge_counters``, ``min``, ``set``, ``stddev``, ``sum``, or ``tuple_set``.
.. 
..       When set to ``count_distinct``, the **list_limit** and **replace_aggregate_column** settings are required. When set to ``list``, the **list_limit** setting is required.
.. 
.. 	  .. list-table::
.. 	     :widths: 60 420
.. 	     :header-rows: 1
.. 
.. 	     * - Operation
.. 	       - Description
.. 	     * - ``count``
.. 	       - Use ``count`` to specify the total number of values for the group, not including missing values.
.. 	     * - ``count_distinct``
.. 	       - Use ``count_distinct`` to specify the maximum number of entities.
.. 	     * - ``count_ngrams``
.. 	       - Use ``count_ngrams`` to create a map from the ngram, and then show how many times that ngram occurs in the input column.
.. 	     * - ``empty``
.. 	       - Use ``empty`` to specify the number of missing values for the group.
.. 	     * - ``hourly_sum``
.. 	       - Use ``hourly_sum`` to replace each timestamp by its hour, and then count how many times each value occurs.
.. 	     * - ``list``
.. 	       - Use ``list`` to specify a list of all unique values, in alphanumeric order. Values that are not strings are converted to strings.
.. 	     * - ``max``
.. 	       - Use ``max`` to specify the maximum value for the group.
.. 	     * - ``mean``
.. 	       - Use ``mean`` to specify the arithmetic mean of the values for the group.
.. 	     * - ``median``
.. 	       - Use ``median`` to specify the 50th percentile value for the group. If the number of values in the group is large, this may be an approximaation.
.. 	     * - ``merge_counters``
.. 	       - Use ``merge_counters`` to merge all maps in the column, and then add together all values for the same keys.
.. 	     * - ``min``
.. 	       - Use ``min`` to specify the minimum value for the group.
.. 	     * - ``set``
.. 	       - Use ``set`` to specify a sorted list of all of the unique values in a column, similar to ``list``.
.. 	     * - ``stddev``
.. 	       - Use ``stddev`` to specify the standard deviation with N - 1 degrees of freedom of the values for the group.
.. 	     * - ``sum``
.. 	       - Use ``sum`` to specify the sum of the values for the group.
.. 	     * - ``tuple_set``
.. 	       - Use ``tuple_set`` to create a sorted list of all of the unique values in a column, similar to ``list``. Same as ``set``.
.. 
.. .. include:: ../../includes_config/additional_attribute.rst
.. 
.. .. include:: ../../includes_config/after_save_function_generic.rst
.. 
.. **assert_dataframe_propogation** (bool, optional)
..    Specifies if the dataset for a key must be aggregated successfully. Default value: ``False``. When ``True``, an error is returned if the aggregation is not successful.
.. 
.. .. include:: ../../includes_config/block_name.rst
.. 
.. **custom** (list, optional)
..    A list of custom integrations to apply. Each custom integration must define **inputs**, **outputs**, and a **function**. For example:
.. 
..    **inputs** (list of str, required)
..       A list of columns to use as inputs to the user-defined function specified by the **function** setting.
.. 
..    **outputs** (list of str, required)
..       A list of columns to output from the user-defined function specified by the **function** setting, and then added to the table.
.. 
..    **function** (str, required)
..       The name of a user-defined function that is located in the :doc:`extension package </extensions>` and runs at the end of the application phase for the **aggregate** command.
.. 
.. .. include:: ../../includes_config/default_validation.rst
.. 
.. **list_limit** (int, optional)
..    The upper limit on list size for all input/operation pairs when aggregatng hostname, users, or url_regdomains. Use this setting to help prevent running out of memory errors in situations where there are a large list of hosts.
.. 
..    Use the **list_limit** setting that is part of the **additional** grouping to override this value for a specific attribute.
.. 
.. **on_load_function** (str, optional)
..    The name of a user-defined function that is located in the :doc:`extension package </extensions>` and is run prior to determining valid aggregations.
.. 
.. **on_save_function** (str, optional)
..    When set to ``filter_phantom_rows``, rows where the sum of bytes out is equal to zero are removed from flow data sources.
.. 
.. .. include:: ../../includes_config/partitions.rst
.. 
.. .. include:: ../../includes_config/replace_function.rst
.. 
.. **turn_off** (list of str, optional)
..    A list of columns to be excluded from aggregations.
.. 
.. **turn_on** (list of str, optional)
..    A list of columns to be included with aggregations.
.. 
.. **use_categorical_aggregation** (bool, optional)
..    Specifies if a list of values that contains DNS entries are converted to integers. Default value: ``True``. This value should be set to ``True`` unless new code is being added to the pipeline and debugging is necessary. When ``False``, slower aggregation performance may occur. 
.. 
.. **use_timestamp_type** (bool, optional)
..    Specifies if timestamps for all aggregations are converted to hourly timestamps. Default value: ``True``.
.. 
.. .. include:: ../../includes_config/validation.rst
.. 



.. _configure-block-graphs:

block_graphs
==================================================
Use the **block_graphs** section to define a series of block graphs, and then for each block graph :ref:`all of the blocks in that graph <process-data-custom-blocks>` that are used by the engine for data processing. Each block graph is declared as a list under ``block_graphs``. Each block in within a block graph is declared along with any specific settings for that block, and then dependencies between blocks in the block graph, mappings, and outputs for the block graph are declared.

.. note:: Blocks that are configured to run in-place of engine phases should be declared in the configuration for that phase using the ``block_name`` setting.

Usage
--------------------------------------------------
**block_graph** is a top-level group of settings.

.. code-block:: yaml

   block_graphs:
     <block_graph_name>:
       blocks:
         <block_name>:
           class_name: 'BlockName' or 'function_name'
            ...                           # block-specific settings
         <block_name>:
            ...                           # additional blocks in graph
         depends:
           - block_name: 'block_name'     # or outside_block_graph
             frame_key: {'key': 'value', ...}
           ...
         inputs:
           - datasource: 'datasource_name'
             phase: 'phase'
             lookback: int
             offset: int

             --or--

           - datasource: 'datasource_name'
             phase: 'phase'
             daterange_start: '/path/to/end_date'
             daterange_end: '/path/to/start_date'
         mappings:
           - artifact_descriptor_pattern: {'key': 'value', ...}
           - bundle_key_pattern: {'key': 'value', ...}
         outputs:
           - block_name: 'block_name'     # or outside_block_graph
             frame_key: {'engine_phase': 'output'}
           ...


Settings
--------------------------------------------------
Each setting for the **block_graphs** group of settings is described below, with the top-level **block_graph** setting listed first, after which all other settings are listed alphabetically.

**block_graphs** (optional)
   The parent setting group for the **block_graphs** section.

**<block_graph_name>** (required)
   The name of an individual block graph declared under **block graphs**.

**blocks** (required)
   The top-level group for all blocks in a single block graph. Each block in the block graph is declared under this setting.

   **<block_name>** (required)
      One (or more) blocks must be declared in a block graph. Each block in the block graph is declared separately in the configuration. Additional settings that are specific to a block are declared as part of that block's configuration.

      .. note:: Blocks that are configured to run in-place of engine phases should be declared in the configuration for that phase using the ``block_name`` setting.

   **class_name** (required)
      The class name for the block or the name of a user-defined function that is located in the :doc:`extension package </extensions>`. For example, the library block used for parsing data is called the ``ParseBlock``. A block named `parser_block` that uses this block has configuration similar to:

      .. code-block:: yaml

         <block_graph_name>:
           blocks
             parser_block: 'ParseBlock'
               ...

**depends**
   A list of dependencies for blocks in the block graph.

   **block_name** (str, required)
      The name of a block that is declared in this block graph or data passed in from ``outside_block_graph``. 

   **frame_key** (dict, optional)
      A dictionary of dependencies. Required only for the :ref:`bundle block type <process-data-custom-blocks-types>`.

**inputs** (list, optional)
   A list of input dependencies on which this block depends.

   .. include:: ../../includes_config/depends_on.rst

**mappings** (list, optional)
   A mapping between bundle keys and output names.

   **artifact_descriptor_pattern** (dict, required)
      A dictionary of keys used to describe outputs.

   **bundle_key_pattern** (dict, required)
      A dictionary of keys used in the output bundle.

**outputs** (list, optional)
   A list of outputs for the block graph.

   **block_name** (str, required)
      The name of a block from which the output is generated.

   **frame_key** (dict, optional)
      The name of the output to be generated.


Example
--------------------------------------------------
.. include:: ../../includes/blocks_tutorial_yaml.rst






.. _configure-category-patterns:

category_patterns
==================================================
**category_patterns** is an optional section that configures how modeling inputs and targets are categorized. Each category pattern is an ordered list; each item in the list is applied sequentially.

The engine searches for the pattern in the value and skips to the next patterns if the value is not found. For example, to define two suffixes that identify internal domain names and then categorize all others as external, use an empty string as the pattern in the last pattern/category pair.

Usage
--------------------------------------------------
**category_patterns** is a top-level group of settings.

.. code-block:: yaml

   category_patterns:
     dns_request_type:
       - category: 'category_type'
         jvm_pattern: 'pattern'
         pattern: 'pattern'
         pattern_type: 'startswith'
       ...
     hostname:
       - category: 'category_type'
         jvm_pattern: 'pattern'
         pattern: 'pattern'
         pattern_type: 'startswith'
       ...
     ip:
       - category: 'category_type'
         jvm_pattern: 'pattern'
         pattern: 'pattern'
         pattern_type: 'startswith'
       ...


Settings
--------------------------------------------------
Each setting for the **category_patterns** group of settings is described below, with the top-level **category_patterns** setting listed first, after which all other settings are listed alphabetically.

**category_patterns** (optional)
   The parent setting group for the **category_patterns** section.

**dns_request_type** (list of str, optional)
   The section for configuring categorization of DNS requests in the **request_type** column. The DSN request category pattern is an ordered list; each item in the list is applied sequentially.

   **category** (str, required)
      The category to be applied when a value matches the pattern. Possible values: ``A``, ``AAAA``, and ``PTR``. If a row does not match any of the listed patterns for the DNS request, the category is marked ``None``.

      .. include:: ../../includes_terms/term_dns_record_a.rst

      .. include:: ../../includes_terms/term_dns_record_aaaa.rst

      .. include:: ../../includes_terms/term_dns_record_ptr.rst

   **jvm_pattern** (str, optional)
      The regular expression pattern to use when pattern matching is performed by the native Spark runtime. When ``pattern_type`` is set to ``regex`` this setting is required, along with the regular expression pattern defined by ``pattern``. For example: ``^172\\.(1[6789]|2\\d|30|31)\\.``.

   **pattern** (str, required)
      The regular expression pattern to use when pattern matching is performed by Pyspark. Required for all pattern types. For example: ``^172\\.(1[6789]|2\\d|30|31)\\.``.

   **pattern_type** (str, optional)
      The type of pattern matching to use. Possible values: ``endswith``, ``regex``, or ``startswith``. Default value: ``startswith``.

      If set to ``regex``, the ``jvm_pattern`` setting must specify the regular expression pattern to use with the native Spark runtime.

      If set to ``endswith`` or ``startswith``, use the ``pattern`` setting to specify the regular expression pattern.

      Use ``endswith`` to check if the value ends with the **pattern**.

      Use ``regex`` to use the **pattern** as a regular expression, and then search for values that match.

      Use ``startswith`` to check if the value starts with the **pattern**.

**hostname** (list of str, optional)
   The section for configuring categorization of domains as internal or external in the **url_regdomain** (registered domain names) and **named_domain** columns. If the **url_regdomain** row is empty for a hostname, **url_regdomain** is populated with data from the domain or IP address. The hostname category pattern is an ordered list; each item in the list is applied sequentially.

   **category** (str, required)
      The category to be applied when a value matches the pattern. Possible values: ``external`` or ``internal``. If a row does not match any of the listed patterns for the hostname, the category is marked ``None``.

      Use ``external`` to categorize an internal hostname.

      Use ``internal`` to categorize an external hostname.

   **jvm_pattern** (str, optional)
      The regular expression pattern to use when pattern matching is performed by the native Spark runtime. When ``pattern_type`` is set to ``regex`` this setting is required, along with the regular expression pattern defined by ``pattern``. For example: ``^172\\.(1[6789]|2\\d|30|31)\\.``.

   **pattern** (str, required)
      The regular expression pattern to use when pattern matching is performed by Pyspark. Required for all pattern types. For example: ``^172\\.(1[6789]|2\\d|30|31)\\.``.

   **pattern_type** (str, optional)
      The type of pattern matching to use. Possible values: ``endswith``, ``regex``, or ``startswith``. Default value: ``startswith``.

      If set to ``regex``, the ``jvm_pattern`` setting must specify the regular expression pattern to use with the native Spark runtime.

      If set to ``endswith`` or ``startswith``, use the ``pattern`` setting to specify the regular expression pattern.

      Use ``endswith`` to check if the value ends with the **pattern**.

      Use ``regex`` to use the **pattern** as a regular expression, and then search for values that match.

      Use ``startswith`` to check if the value starts with the **pattern**.

**ip** (list of str, optional)
   The section for configuring categorization of IP addresses in the **dest_ip** and **source_ip** columns. The IP address category pattern is an ordered list; each item in the list is applied sequentially.

   **category** (str, required)
      The category to be applied when a value matches the pattern. Possible values: ``external`` or ``internal``. If a row does not match any of the listed patterns for the IP address, the category is marked ``None``.

      Use ``external`` to categorize an external IP address.

      Use ``internal`` to categorize an internal IP address.

   **jvm_pattern** (str, optional)
      The regular expression pattern to use when pattern matching is performed by the native Spark runtime. When ``pattern_type`` is set to ``regex`` this setting is required, along with the regular expression pattern defined by ``pattern``. For example: ``^172\\.(1[6789]|2\\d|30|31)\\.``.

   **pattern** (str, required)
      The regular expression pattern to use when pattern matching is performed by Pyspark. Required for all pattern types. For example: ``^172\\.(1[6789]|2\\d|30|31)\\.``.

   **pattern_type** (str, optional)
      The type of pattern matching to use. Possible values: ``endswith``, ``regex``, or ``startswith``. Default value: ``startswith``.

      If set to ``regex``, the ``jvm_pattern`` setting must specify the regular expression pattern to use with the native Spark runtime.

      If set to ``endswith`` or ``startswith``, use the ``pattern`` setting to specify the regular expression pattern.

      Use ``endswith`` to check if the value ends with the **pattern**.

      Use ``regex`` to use the **pattern** as a regular expression, and then search for values that match.

      Use ``startswith`` to check if the value starts with the **pattern**.





.. 
.. .. _configure-check-quality:
.. 
.. check_quality
.. ==================================================
.. **check_quality** is used to change the assumptions for the :ref:`command-check-quality` command.
.. 
.. Two settings are required: **hidden_negprob** and **obvious_negprob**.
.. 
.. Usage
.. --------------------------------------------------
.. **check_quality** is a top-level group of settings.
.. 
.. .. code-block:: yaml
.. 
..    check_quality:
..      days_in_TC_window: 7
..      fraction_abnormal: 0.0005
..      granular_rows_cutoff: 5
..      ground_truth_filename: 'path/to/file.json'
..      hidden_negprob: 0.26
..      ndcg_rank_cutoff: 10
..      obvious_negprob: 0.999
..      probability_obvious: 0.25
..      reviewed_threats_filename: 'string'
.. 
.. 
.. Settings
.. --------------------------------------------------
.. Each setting for the **check_quality** group of settings is described below, with the top-level **check_quality** setting listed first, after which all other settings are listed alphabetically.
.. 
.. TODO: THESE SETTINGS ARE NEARLY IDENTICAL TO THE COMMAND-LINE OPTIONS FOR CHECK_QUALITY COMMAND. SYNC THOSE UP AND SOURCE THEM APPROPRIATELY. ALSO need to verify against the apt/check_quality topic to ensure that Art's feedback is present here for the values and such things.
.. 
.. **check_quality** (optional)
..    The parent setting group for the **check_quality** section.
.. 
.. **days_in_TC_window** (int, optional)
..    The number of days in the threat case window. Default value: ``7``.
.. 
.. **fraction_abnormal** (float, optional)
..    The fraction of entities that are assumed to be truly abnormal. Default value: ``1/2000`` or ``0.0005``.
.. 
.. **granular_rows_cutoff** (int, optional)
..    The number of rows to be shown for each host. Default value: ``5``.
.. 
.. **ground_truth_filename** (string, optional)
..    The path to a JSON file that contains a list of known campaigns, from which relevant metrics can be calculated. The JSON file should be similar to:
.. 
..    .. code-block:: javascript
..       
..       {
..         "campaign_name":
..           [
..             {
..               "host": "10.10.28.50",
..               "date": "2017-01-27",
..               "stages": "3",
..               "notes": "nmap"
..             },
..             {
..               "host": "10.10.28.50",
..               "date": "2017-01-27",
..               "stages": "4",
..               "notes": "scp"
..             },
..             {
..               "host": "10.10.29.110",
..               "date": "2017-01-28",
..               "stages": "5",
..               "notes": "scp"
..             },
..             ...
..           ]
..         "campaign_name":
..           [
..             ...
..           ]
..       }
.. 
..    where ``campaign_name`` is the name of the known campaign, ``host`` is the IP address associated with the known campaign, ``date`` is the date on which this known campaign was discovered, ``stages`` is the stages in which this known campaign have been detected, and ``notes`` is extra information about this campaign.
.. 
.. **hidden_negprob** (float, optional)
..    The negative probability score for a non-obvious abnormal entity. This value must be less than **obvious_negprob**. Default value: ``0.6``.
.. 
.. 
.. see: 30428d7
.. setting:
..      ndcg_rank_cutoff: 10
.. 
.. **ndcg_rank_cutoff** (int, optional)
..    Normalized Discounted Cumulative Gain (NDCG) is a measure of ranking quality that gives more credit to known, reviewed threats and to results that have higher relevance. This setting defines the maximum number of known, reviewed threats that may appear in the identified threat case list. The relevance ratings used to calculate NDCG are defined for known and defined in a JSON file. (See **reviewed_threats_filename**.) Default value: ``10``.
.. 
.. 
.. **obvious_negprob** (float, optional)
..    The negative probability score for an obvious abnormal entity. This value must be greater than **hidden_negprob**. Default value: ``0.999``.
.. 
.. **probability_obvious** (float, optional)
..    The probability that an abnormal entity is obvious and creates an outlier in a measurement. Default value: ``0.25``.

.. .. 
.. see: 30428d7
.. setting:
..      reviewed_threats_filename: 'path/to/file.json'
.. 
.. **reviewed_threats_filename** (int, optional)
..    The path to a JSON file that contains a list of reviewed threats, from which relevant metrics can be calculated. If the reviewed threats file is not present, all NDCG ranked threats are treated as ``DefinitelyNotRelevant``.
.. 
..    The JSON file should be similar to:
.. 
..    .. code-block:: javascript
..       
..       {
..         "threat_name": {
..           "relevance": "DefinitelyNotRelevant",
..             "behaviors": [{
..               "host": "10.10.29.112",
..               "date": "2017-05-25",
..               "stage": "4",
..               "notes": "Printer"
..             }]
..         },
..         "threat_name": {
..           "relevance": "ProbablyNotRelevant",
..           "behaviors": [{
..             "host": "172.16.24.1",
..             "date": "2017-01-27",
..             "stage": "3",
..             "notes": "Server"
..           },
..           {
..             "host": "172.16.24.2",
..             "date": "2017-01-28",
..             "stage": "5",
..             "notes": "Server"
..           },
..           {
..             "host": "10.10.29.110",
..             "date": "2017-02-03",
..             "stage": "5",
..             "notes": "To be investigated"
..           }]
..         },
..         "threat_name": {
..           "relevance": "ProbablyRelevant",
..             "behaviors": [{
..               "host": "10.10.29.112",
..               "date": "2017-01-25",
..               "stage": "4",
..               "notes": "Printer"
..             }]
..         },
..         "threat_name": {
..           "relevance": "UndeniablyRelevant",
..           "behaviors": [{
..             "host": "172.16.200.2",
..             "date": "2017-01-25",
..             "stage": "3",
..             "notes": "Server"
..           }]
..         },
..         ...
..       }
.. 
..    where ``relevance`` is one of ``DefinitelyNotRelevant``, ``ProbablyNotRelevant``, ``ProbablyRelevant``, or ``UndeniablyRelevant``, ``threat_name`` is the name of the reviewed threat, ``host`` is the IP address associated with the reviewed threat, ``date`` is the date on which this threat was reviewed, ``stage`` is the campaign stage to which this threat is associated, and ``notes`` is extra information about the reviewed threat.
.. 
.. 





 


.. _configure-drive:

drive
==================================================
**drive** is an optional section that configures options specific to running application phases via the **drive** command.

Usage
--------------------------------------------------
**drive** is a top-level group of settings.

.. code-block:: yaml

   drive:
     artifact_graph_filename: 'None'
     block_type_computation_phases: []
     command_extra_args: {}
     naming: V1_1
     pre_phase:
       - cmd_line: '/path/to/script'
       - resets_cluster: False
     timeout: 
       - phase_name: 9000
       - ...

Settings
--------------------------------------------------
Each setting for the **drive** group of settings is described below, with the top-level **drive** setting listed first, after which all other settings are listed alphabetically.

**drive** (optional)
   The parent setting group for the **drive** section.

**artifact_graph_filename** (str, optional)
   The name of the artifact graph output file that is saved to local storage. Set to ``None`` to skip saving an artifact graph. Default value: ``None``.

**block_type_computation_phases** (list of str, optional)
   A list of phases for which block type computations are run. This will prevent custom code from doing unncessary materializations, but will not affect results. Default value: ``[]``.

**command_extra_args** (dict, optional)
   Extra arguments to supply a command. Default value: ``{}``.

**naming** (enum, optional)
   The version of the naming format to use for **workspaces** configuration settings in input/output paths for each application phase. Possible values: ``V1_1`` or ``V2_0``. Default value: ``V1_1``.

   Use ``V1_1`` to create un-nested workspace directories. 

   Use ``V2_0`` to create nested workspace directories.

**pre_phase** (script, optional)
   A list of settings that configures any desired pre-phase behavior.

   **cmd_line** (str, required)
      The absolute path to a bash script to be run prior to any phases in the application workflow. For example, to run key renegotiation. This script must be in the same location as the path specified by the **customization_package_path** setting.
	  
   **resets_cluster** (bool, optional)
      Specifies if the cluster is reset after the script has finished running. Default value: ``False``.

**timeout** (dict, optional)
   The amount of time (in seconds) to wait before a phase times out. A non-default timeout value must be specified per phase. If the named phase runs longer than the specified amount of time, the processing for that phase is interrupted and an error is returned. This configuration setting is useful in situations where Spark is not responsive and is not reporting that state to the engine. Default value: ``9000`` (for all phases).






.. _configure-entity-attribute-sources:

entity_attribute_sources
==================================================
.. include:: ../../includes_terms/term_entity_attribute_table.rst

**entity_attribute_sources** is an optional section that is used to configure entity attribute tables that contain additional information about users or hosts to improve the quality of the modeling table. All configuration-defined entity attribute tables are processing as Parquet tables by the engine. For example, attributes for a user may include department or title whereas attributes for a host might distinguish between servers, personal computers, and so on.

Usage
--------------------------------------------------
**entity_attribute_sources** is a top-level group of settings that defines blocks to process data---parsing data, standardizing data, transforming data, and aggregating data---prior to modeling. For example, an entity attribute table named ``process_rows_count`` is associated with a block named ``count_rows``:

   .. code-block:: yaml

      block_graphs:
        count_rows:
          ...
          inputs:
            - datasource: proxyweb
              phase: parsed
            - datasource: proxyweb
              phase: parsed_error
        ...
      ...
      entity_attribute_sources:
        process_rows_count:      # the entity attribute table name
          block_name:
            - count_rows

.. TODO: The following warning will be deprecated with the 1.4 release. The 1.3 release moves this configuration to blocks (see above); a flag in the configuration file enables the previous configuration pattern.

.. warning:: Prior to |vse| 1.3, entity attribute tables were not configured using blocks. There were two ways to configure them: user-defined functions and configuration or only configuration. For blocks that are generated by a user-defined function:

   .. code-block:: yaml

      entity_attribute_sources:
        <data source>:
          after_save_functions:
            - 'function_name'
            - ...
          artifact_class_name: 'class_name'
          attribute_columns:
            - 'column_name'
            - 'column_name'
            - '...'
          block_name: 'block_name'
          breakdown_target_eats:
            - column_name: 'column_name'
              lookback_duration: '1d'
              lookback_step: '0d'
              target_eat: 'entity_attribute_table_name'
              target_measurement: 'target_name'
              transform: 'transform_name'
          breakdown_target_eats:
            - column_name: 'column_name'
              lookback_duration: '1d'
              lookback_step: '0d'
              target_eat: 'entity_attribute_table_name'
              target_measurement: 'target_name'
              transform: 'transform_name'
          depends_on:
            - datasource: 'datasource_name'
              is_ranged_artifact: False
              lookback: int
              offset: int
              phase: 'phase'

              --or--

            - datasource: 'datasource_name'
              daterange_end: '/path/to/start_date'
              daterange_start: '/path/to/end_date'
              is_ranged_artifact: True
              phase: 'phase'
          enable_deprecated_phases: True
          function_extra_args: ['string', 'string', ...]
          generate: 'string'
          key_columns: ['column', 'column', '...']
          multi_day_measures_params:
            measures:
              input_cols:
                - 'column_name'
                - 'column_name'
                - '...'
              lookback_window:
                custom_fill_type: (only when missing_values_policy: fill_custom)
                custom_fill_value: (only when missing_values_policy: fill_custom)
                lookback_duration: -3d
                lookback_step: -1d
                missing_values_policy: fill_default
              normalized: True
              state_limit: 1000
              transform_args:
                A_name: 'column_name'        # required for cross-tab
                B_name: 'column_name'        # required for cross-tab
                separator: '_'               # required for cross-tab
                stats: 'count_distinct_AxB'  # required for cross-tab
              transform_name: 'multi_day_count_distinct' 
              window_unit: 1d
          partitions:
            - per_worker_after_load_partitions: int
            - per_worker_before_save_paritions: int
            - total_after_load_partitions: int
            - total_before_save_partitions: int
          path_add_date_suffix: True
          produce_ranged_artifact: None
          replace_function: 'function_name'
          udf_source: 'LIBRARY'
   
   For blocks that are not generated by a user-defined function:

   .. code-block:: yaml

      entity_attribute_sources:
        <data source>:
          after_save_functions:
            - 'function_name'
            - ...
          artifact_class_name: 'class_name'
          attribute_columns:
            - 'column_name'
            - 'column_name'
            - '...'
          block_name: 'block_name'
          breakdown_target_eats:
            - column_name: 'column_name'
              lookback_duration: '1d'
              lookback_step: '0d'
              target_eat: 'entity_attribute_table_name'
              target_measurement: 'target_name'
              transform: 'transform_name'
          enable_deprecated_phases: True
          keep_all_entities: False
          key_columns: ['column', 'column', '...']
          modeling_groupby_keys: ['key', '...']
          multi_day_measures_params:
            measures:
              input_cols: 
                - 'column_name'
                - 'column_name'
                - ...
              lookback_window:
                custom_fill_type: (only when missing_values_policy: fill_custom)
                custom_fill_value: (only when missing_values_policy: fill_custom)
                lookback_duration: -3d
                lookback_step: -1d
                missing_values_policy: fill_default
              normalized: True
              transform_args:
                A_name: 'column_name'        # required for cross-tab
                B_name: 'column_name'        # required for cross-tab
                separator: '_'               # required for cross-tab
                stats: 'count_distinct_AxB'  # required for cross-tab
              transform_name: 'multi_day_count_distinct' 
              window_unit: 1d
          partitions:
            - per_worker_after_load_partitions: int
            - per_worker_before_save_paritions: int
            - total_after_load_partitions: int
            - total_before_save_partitions: int
          path: '/path/to/data'
          produce_ranged_artifact: None
          replace_function: 'function_name'
          type: 'PRE_MODELING_TABLE_ATTRIBUTE'
          udf_source: 'LIBRARY'
   




Settings
--------------------------------------------------
Each setting for the **entity_attribute_sources** group of settings is described below, with the top-level **entity_attribute_sources** setting listed first, after which all other settings are listed alphabetically.

**entity_attribute_sources** (optional)
   The parent setting group for the **entity_attribute_sources** section.

.. include:: ../../includes_config/block_name.rst

.. TODO: The following warning will be deprecated with the 1.4 release. The 1.3 release moves this configuration to blocks (see above); a flag in the configuration file enables the previous configuration pattern.

.. warning:: The following settings are only available for the |vse| 1.3 release and only when ``enable_deprecated_phases`` is set to ``True``. 
 
   **entity_attribute_sources** (optional)
      The parent setting group for the **entity_attribute_sources** section.

   .. include:: ../../includes_config/after_save_function_generic.rst

   **artifact_class_name** (str, optional)
      The class name for the type of table to output, such a standard table, a table collection, an empty table, a model, or a CSV table. For example: ``TableArtifact``. Default value: ``None``.

   **attribute_columns** (list of str, required)
      A list of additional columns to be joined into :ref:`a generated table <model-data-generated-table>`. This may be any (or all) of the columns in the current table. At least one attribute column must be specified.

   .. include:: ../../includes_config/block_name.rst

   **breakdown_target_eats** (list, optional)
      The configuration for one (or more) multi-unit measurements that share the same transform.

      **column_name** (str, required)
         The name of the input column from which the multi-unit target measurement was generated.

      **lookback_duration** (str, optional)
         The number of days in the lookback window, starting from the day defined by ``lookback_step``, and also the number of days retained in the state for multiday measurements. For example, if ``lookback_duration`` is ``3d`` the lookback window is three days. If the current date is June 20 and ``lookback_duration`` is ``3d`` and ``lookback_step`` is set to ``-1d``, the date on which the lookback window begins is June 19 and the lookback window includes June 17-19 (three days).

         .. note:: The duration of a lookback window can never be longer than the number of days for which there is data. For example, if ``lookback_duration`` is ``8d``, but there is only data for six days, the duration is effectively ``6d``.

      **lookback_step** (str, optional)
         The number of units (in days) to lookback from the current day and also the day from which multi-unit measures are calculated. For example: ``0d`` starts the lookback window from the current day and ``-1d`` starts the lookback window from the previous day.

      **target_eat** (str, required)
         The name of the multi-unit entity attribute table that generated the target measurement.

      **target_measurement** (str, required)
         A name of a multi-unit target measurement that is found in the modeling results.

      **transform** (str, required)
         The name of the method that transformed the multi-unit target measurement.

   **depends_on** (list, optional)
      If **generate** is specified, an optional list of settings may be used to define upstream dependencies that are required by the generated entity attribute table.

      .. include:: ../../includes_config/depends_on.rst

   **enable_deprecated_phases** (bool, optional)
      Specifies if entity attribute tables are configured using |vse| 1.2 (and previous) configuration patterns. This option will be deprecated in version 1.4, at which point all data processing for the ``parse``, ``standardize``, ``transform``, and ``aggregate`` phases must be done using a block graph and blocks from the blocks library. This setting enable the previous behavior to ensure that data processing may continue while the switch to blocks is made. Default value: ``False``.

   **function_extra_args** (list of str, optional)
      A list extra arguments to be applied to the generated table.

      .. include:: ../../includes_config/connectivity_graph_function_thresholds.rst

   **generate** (str, required, see **path**)
      The name of a user-defined function that is located in the :doc:`extension package </extensions>`. This function generates the entity attribute table when using the **drive** command or the **generate** command. Use the **depends_on** and **function_extra_args** settings to modify this behavior. This setting may not be specified if **path** is specified.

      If **udf_source** is set to ``CUSTOM``, this value must be the name of a user-defined function that is located in the :doc:`extension package </extensions>`. This function takes a CommandContext as an argument. The generated entity attribute tables must then be joined during the the **transform**, **aggregate**, or **generate_modeling_table** phases.

      If **udf_source** is set to ``LIBRARY``, then this value must be set to one of:

      **create_hostname_features**
         .. include:: ../../includes/entity_attribute_table_cse_hostname.rst

      **create_rank_features**
         .. include:: ../../includes/entity_attribute_table_cse_rank.rst

      **create_intermediate_rank_features**
         .. include:: ../../includes/entity_attribute_table_cse_rank_intermediate.rst

      **create_sharing_features**
         .. include:: ../../includes/entity_attribute_table_cse_sharing.rst

      **sessionize**
         .. include:: ../../includes/entity_attribute_table_cse_sessionize.rst

   **keep_all_entities** (bool, optional)
      Specifies if rows should be dropped when they do not contain evidence of activity for the date range associated with this modeling table. Default value: ``False``.

   **key_columns** (list of str, required)
      The name of the columns to use as a key. This is typically **user** or **hostname**.

      .. note:: If two (or more) values are specified for this setting, the **modeling_groupby_keys** setting must also be specified.

   **modeling_groupby_keys** (list of str, optional)
      Specifies one (or more) modeling tables to which an entity attribute table is joined. For example:

      .. code-block:: yaml

         entity_attribute_sources:
           an_entity_attribute_table:
             ...
             key_columns: [hostname, fqdn]
             modeling_groupby_keys: [day_host_fqdn_internal]
             ...

      will join the ``hostname`` and ``fqdn`` columns to the ``day_host_fqdn_internal`` modeling table.

      .. warning:: This setting is required when an entity attribute table has two (or more) values specified in ``key_columns``.

   **multi_day_measures_params** (optional)
      The configuraiton for a multi-unit table that contains measurements over multiple time units. Table state is remembered, and then applied to the future time unit measurements. One (or more) ``measures`` groups may be defined.

      .. include:: ../../includes_config/multiday_params.rst

   .. include:: ../../includes_config/partitions.rst

   **path** (str, required, see **generate**)
      The absolute path to a directory outside of the engine, in which static entity attribute tables are located. This setting may not be specified if **generate** is specified.

   **path_add_date_suffix** (bool, optional)
      Specifies if the date is added to the **path**. When ``True`` the entity attribute table will be read from ``<path>_<date>``. When ``False``, the entity attribute table will be read from ``<date>`` only. Default value: ``False``.

   **produce_ranged_artifact** (bool, optional)
      Specifies if the entity attribute table spans multiple days.

   .. include:: ../../includes_config/replace_function.rst

   **type** (enum, optional)
      The source type for this data source. Possible values: ``MODELING_TABLE_ATTRIBUTE`` (default) or ``PRE_MODELING_TABLE_ATTRIBUTE``.

      Use ``MODELING_TABLE_ATTRIBUTE`` to add attributes at the **generate_modeling_table** application phase. This is the default for entity attribute tables.

      Use ``PRE_MODELING_TABLE_ATTRIBUTE`` to add attributes prior to modeling table generation in the **aggregate** or **transform** phases.

   **udf_source** (str, optional)
      The location of a user-defined function, from which a generation function is loaded. Possible values: ``CUSTOM`` or ``LIBRARY``.

      Use ``CUSTOM`` to load a custom function specified by **generate** and located at the **customization_package_path**. When set to ``CUSTOM`` the **generate** setting must specify the name of a user-defined function that is located in the :doc:`extension package </extensions>`. This function takes a CommandContext as an argument. The generated entity attribute tables must then be joined during the the **transform**, **aggregate**, or **generate_modeling_table** phases.

      Use ``LIBRARY`` to load a predefined function from within the engine.
   




.. _configure-flighting:

flighting
==================================================
**flighting** is an optional subsection used to define features that require experimentation with real data prior to enabling them in production.

Usage
--------------------------------------------------
**flighting** is a top-level group of settings.

.. code-block:: yaml

   flighting:
     edit_distance_lookup_path: '/path/to/lookup_table'
     enable_common_subset_for_merge: True
     parse_user_agent: False


Settings
--------------------------------------------------
Each setting for the **flighting** group of settings is described below, with the top-level **flighting** setting listed first, after which all other settings are listed alphabetically.

**flighting** (required)
   The parent setting group for the **flighting** section.

**edit_distance_lookup_path** (str, optional)
   The absolute path to a lookup table used for faster edit distance calculations. This lookup table memorizes the reg domains for one day, and then uses them for faster calculations on subsequent days.

   If a value is specified or updated, the **generate_transition_table** command must be run. If the **domain_levenshtein** transform is enabled, the **transform** command will use that lookup table. Note that this lookup is different from the one used for **standardize_keys**.

**enable_common_subset_for_merge** (bool, optional)
   Specifies if multi-day modeling tables are merged so that only common sets of columns are used for learning,  predicting, and postprocessing. When ``False``, multi-day modeling tables are merged so that columns with ``None`` are inserted to ensure that all tables have identical sets of columms. Default value: ``True``.

**parse_user_agent** (bool, optional)
   Specifies if a table with **user_agent** columns will have additional columns for components of **user_agent** created. Default value: ``False``.

   When ``True``, the following columns are added:

   * **user_agent_browser_family**
   * **user_agent_browser_version_string**
   * **user_agent_os_family**
   * **user_agent_os_version_string**
   * **user_agent_device_family**
   * **user_agent_device_brand**
   * **user_agent_device_model**
   * **user_agent_is_mobile**
   * **user_agent_is_tablet**
   * **user_agent_is_pc**
   * **user_agent_is_touch_capable**
   * **user_agent_is_bot**







.. _configure-generate-lookup-global:

generate_lookup (global)
==================================================
**generate_lookup** is an optional subsection used to define how the global lookup table is generated.

Usage
--------------------------------------------------
The global **generate_lookup** section is located under **sources**, with one **generate_lookup** section defined for each source type:

.. code-block:: yaml

   sources:
     <source_type>:
       generate_lookup:
         after_save_functions:
           - 'function_name'
           - ...
         block_name: 'block_name'
         partitions:
           - per_worker_after_load_partitions: int
           - per_worker_before_save_paritions: int
           - total_after_load_partitions: int
           - total_before_save_partitions: int
         replace_function: 'function_name'

.. include:: ../../includes_config/source_type_usage.rst

Settings
--------------------------------------------------
Each setting for the **generate_lookup** group of settings is described below, with the top-level **generate_lookup** setting listed first, after which all other settings are listed alphabetically.

**generate_lookup** (optional)
   The parent setting group for the **generate_lookup** section.

.. include:: ../../includes_config/after_save_function_generic.rst

.. include:: ../../includes_config/block_name.rst

.. include:: ../../includes_config/partitions.rst

.. include:: ../../includes_config/replace_function.rst




.. 
.. .. _configure-generate-lookup-per-source:
.. 
.. generate_lookup (per-source)
.. ==================================================
.. **generate_lookup** is an optional subsection under **sources** that configures the number of days in the lookup window.
.. 
.. .. warning:: This section must be located under ``sources`` for a specific ``<source_type>`` and does not contain the same settings as the global :ref:`configure-generate-lookup-global` configuration settings.
.. 
.. Usage
.. --------------------------------------------------
.. The **generate_lookup** section is located under **sources**, with one **generate_lookup** section defined for each source type:
.. 
.. .. code-block:: yaml
.. 
..    sources:
..      <source_type>:
..        generate_lookup:
..          window_days: 1
.. 
.. .. include:: ../../includes_config/source_type_usage.rst
.. 
.. Settings
.. --------------------------------------------------
.. Each setting for the **generate_lookup** group of settings is described below, with the top-level **generate_lookup** setting listed first, after which all other settings are listed alphabetically.
.. 
.. **generate_lookup** (optional)
..    The parent setting group for the **generate_lookup** section, as it applies to a specific ``<source_type>``.
.. 
.. **window_days** (int, optional)
..    A number (in days of data) to include when generating the lookup table. This number is inclusive of the current day. Default value: ``1``.
.. 






.. _configure-generate-modeling:

generate_modeling
==================================================
**generate_modeling** is an optional section that is used to configure two user-defined functions that are located in the :doc:`extension package </extensions>`. These functions are run while the modeling table is generated. The first runs after all metadata tables have been joined, but before any features are computed. The second runs after features are computed, but before the joined modeling table is saved.

Usage
--------------------------------------------------
**generate_modeling** is a top-level group of settings.

.. code-block:: yaml

   generate_modeling:
     additional_features:
       subnet_labels:
         columns: ['label', 'address', 'mask']
         delimiter: 'string'
         has_headers: False
         path: '/path/to/CSV/file'
     block_names:
       - 'block_name'
       - 'block_name'
       - ...
     entity_aggregate_keys:
       host:
         - stage: int
           key_columns: ['date', 'hostname']
           categories: ['internal']  # or ['external']
       user:
         - stage: int
           key_columns: ['date', 'user']
           categories: ['internal']  # or ['external']
     join_broadcast_rows_limit: 50000
     partitions:
       - per_worker_before_save_paritions: int
       - total_after_load_partitions: int
   
       --or--
   
       - per_worker_after_load_partitions: int
       - total_before_save_partitions: int
     target_minimum_percent_not_none: float


Settings
--------------------------------------------------
Each setting for the **generate_modeling** group of settings is described below, with the top-level **generate_modeling** setting listed first, after which all other settings are listed alphabetically.

**generate_modeling** (optional)
   The parent setting group for the **generate_modeling** section.

**additional_features** (optional)
   Use to add additional features, by category. Available categories: ``subnet_labels``.

   **subnet_labels** (optional)
      Subnet labels can help the |vse| identify subsets of an internal network. Use a CSV file to specify a list of subnets for which labels should be applied. The |vse| will process the ``hostname`` and ``fqdn`` columns against the contents of this CSV file to add source and destination IP address labels to the modeling table.

      The ``subnet_labels`` settings group is the category for adding subnet labels to modeling tables. When subnet labels are added to modeling tables, the ``hostname`` and ``fqdn`` columns are processed against the IP addresses specified in a CSV file, after which ``src_ip_subnet_label`` and ``dest_ip_subnet_label`` columns are added to the modeling table.

      **columns** (str, optional)
         A list of three columns in the following order: ``label``, ``address``, and ``mask``.

      **delimiter** (str, optional)
         A string used to separate data fields. If a delimiter is not specified, it is inferred by the engine based on the contents of the CSV file specified by the ``path`` setting.

      **has_headers** (bool, optional)
         Specifies if the subnet has headers. Default value: ``False``.

      **path** (str, required)
         The path to a CSV file in which subnet labels are specified. The CSV file must have three columns named ``label``, ``address``, and ``mask``, and be specified in that order. 

         For subnets that use a network address and mask: ``label`` is the descriptive name assigned to the subnet, ``address`` is the IP address for that subnet, and ``mask`` is the subnet mask assigned the subnet.

         For subnets that use `Classless Inter-Domain Routing (CIDR) notation <https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing>`__: ``label`` is the descriptive name assigned to the subnet, ``address`` is the CIDR notation for the IP address for that subnet, and ``mask`` is left empty.

         For example, a file located at ``/security/subnet_labels.csv``:

         .. code-block:: none

            label,address,mask
            Management,10.10.2.0,255.255.255.0
            DMZ Guest,10.10.10.0,255.255.255.0
            Services,10.10.24.0,255.255.255.0
            Storage,10.10.18.0,255.255.255.0
            Test,10.10.25.0,255.255.255.0
            Users,10.10.28.0,255.255.252.0
            HDFS Cluster 2A,172.18.71.0/26,
            HDFS Cluster 2B,172.18.71.64/26,
            HDFS Cluster 2C,172.18.71.128/26,
            Engineering VPC,172.18.70.0/24,
            Admin Subnet,172.18.70.0/26,

         where the first six rows are subnets that use a network address and mask and the final five rows are subnets that use CIDR notation.

**block_names** (list of str, optional)
   A list of blocks from which the modeling table is generated. These blocks must be defined under :ref:`configure-block-graphs`. For example:

   .. code-block:: yaml

      block_names:
        - day_host_fqdn_external_modeling_table_graph
        - day_host_modeling_table_graph
        - day_host_fqdn_internal_modeling_table_graph

   .. warning:: See ``groupby_block_names``.

**entity_aggregate_keys** (optional)
   Filters columns by category and campaign stage.

   **categories** (list of str, optional)
      The category to use for filtering. Possible values: ``external`` and ``internal``.

   **key_columns** (list of str, optional)
      A list of column names to be used for filtering. For host filtering, defaults to ``'date', 'hostname'`` and for user filtering defaults to ``'date', 'user'``.

   **stage** (int, optional)
      The stage for which the filtering is performed.

**groupby_block_names** (list of str, optional)
   A map of groupby keys-to-block graph names, from which a modeling table is generated. Use this option to substitute block graphs for engine phases, such as if specific block graphs must be run by specific groupby keys during the **generate_modeling_table** phase. For example:

   .. code-block:: yaml

      groupby_block_names:
        - day_host_fqdn_external_modeling_table_graph
        - day_host_modeling_table_graph
        - day_host_fqdn_internal_modeling_table_graph

   .. warning:: See ``block_names``.

**join_broadcast_rows_limit** (int, optional)
   The maximum number of rows in a table before the engine will stop using broadcast when joining individual tables into the daily modeling table. Default value: ``50000``.

.. include:: ../../includes_config/partitions.rst

**target_minimum_percent_not_none** (float, optional)
   The percentage of values at which too many ``None`` values exist in the target columns of the modeling table. If any target column in the modeling table has too many ``None`` values, an error is returned.





.. _configure-modeling:

modeling
==================================================
**modeling** is an optional subsection used to configure how to learn a model to produce stage scores, including the option of using a user-defined function to run custom code for experimentation.

Given modeling tables spanning one or more days, where a modeling table has one row per entity-day, create a behavior model object that may consist of many sub-objects that represents what we have learned about behavior for these entities. For example, this object may consist of a set of models, one for each of a set of target features. Or it might just consist of the means and variances of those features.

Currently, this section won't contain directions on how to model/score, but rather metadata that a modeler/scorer may want to use. In particular, this configuration section will provide the mapping of columns in the modeling table to phases, as well as identification of context features.

Usage
--------------------------------------------------
**modeling** is a top-level group of settings.

.. code-block:: yaml

   modeling:
     add_weights:
       extra_args:
         - arg_name: value
           ...
       function: 'function_name'
     after_save_functions:
       - 'function_name'
       - ...
     allow_evictions: True
     annotations:
       - 'cumulative_column_name'
       - ...
     baseline_models:
       baseline_usage: 'never_override'
       metric: 'MALQ'
     behaviors:
       - description: 'description'
       - key: 'key_name'
       - notes: 'notes'
       - primary: True
       - stages: [3, 4, 5]
       - targets:
         target_name: level # see relevance_weights
         target_name: level # see relevance_weights
         ...
     block_name: 'block_name'
     context:
       - 'column_name'
       - 'column_name'
       - ...
     default_learning_options:
       bin_count: int
       binning_algorithm: None
       explorations: False
       model_type: LINEAR_REGRESSION
       regularization: L2
       time_tradeoff: int
     default_strategy:
       - strategy_name: 'comp_cdf'
       - top_k_threshold: 50
     drop_and_ignore_nones:
       - 'modeling_target'
       - 'modeling_target'
       - ...
     drop_zeros_from_training:
       - 'modeling_target'
       - 'modeling_target'
       - ...
     enrichments:
       add_timestamp_features: True
       none_strategy: 'default'
       pass_through:
         - input_column: 'column_name'
           targets:
             - 'target_name'
             - 'target_name'
             - ...
       past_median:
         - 'target_name'
         - 'target_name'
         - ...
     entities: ['user', 'host']
     experimentation: 'function_name'
     function: 'function_name'
     learning_options:
       <model>:
         bin_count: int
         binning_algorithm: None
         explorations: False
         model_type: LINEAR_REGRESSION
         regularization: L2
         time_tradeoff: int
     load_split_from_workspace: 'path/to/split'
     log_targets:
       - 'modeling_target'
       - 'modeling_target'
       - ...
     model_structure: flat
     per_model_context_overrides:
       <model>:
         - 'column_name'
         - 'column_name'
         - ...
     per_model_context_refinements:
       <model>:
         - 'column_name'
         - 'column_name'
         - ...
     relevance_weights:
       - circumstantial: 0.0
       - custom:
         - 'target_name': weight
         - 'target_name': weight
       - strong: 1.0
       - weak: 0.25
     replace_function: 'function_name'
     report_diagnostics: True
     save_diagnostics: True
     save_split: True
     save_summaries: True
     split:
       - date_column: 'column_name'
       - entity_column: 'column_name'
       - name: 'NoSplitStrategy'
       - proportions: int
       - split_seed: 'seed'
     strategies:
       <target_column>:
         - strategy_name: 'comp_cdf'
         - top_k_threshold: 50
     training_num_shards: 6
     use_feedback_columns: True

Settings
--------------------------------------------------
Each setting for the **modeling** group of settings is described below, with the top-level **modeling** setting listed first, after which all other settings are listed alphabetically.

**modeling** (required)
   The parent setting group for the **modeling** section.

**add_weights** (optional)
   Use to add weights to the modeling inputs for all models.

   **extra_args** (dict of str, optional)
      Additional arguments to be interpreted by the user-defined function specified by the **function** setting for the named target.

   **function** (str, optional)
      The name of a user-defined function that is located in the :doc:`extension package </extensions>` and adds weights to modeling inputs. The function takes a DataTable, entity, and configuration object, and then returns a DataTable.

.. include:: ../../includes_config/after_save_function_models.rst

.. include:: ../../includes_config/block_name.rst

**allow_evictions** (bool, optional)
   Specifies if degenerate models are evicted from analysis. Set to ``False`` to prevent degenerate models from being evicted from analysis. Default value: ``True``.

   A degenerate model is a model for which the variance parameter cannot be estimated or is zero. The most likely cause for a degenerate model is training data where the target value is always the same. This happens most often with small data samples.

   If the error ``ComboModel must contain at least one sub-model, but 'sub_models' list is empty``, all the models for that stage were evicted.

**annotations** (list of str, optional)
   A list of cumulative columns (that are not modeling targets) to be computed.

**baseline_models** (optional)
   Defines the usage of baseline models---models without inputs---for use by measurement models.

   **baseline_usage** (str, optional)
      The baseline behavior. Possible values: ``conditionally_override``, ``force_override``, or ``never_override``. Default value: ``never_override``.

      Use ``conditionally_override`` to build baseline models **or** standard models, whichever performs better on a test set. Use **metric** to specify how the model will be evaluated.

      Use ``force_override`` to only build baseline models, especially when historical data is not present.

      Use ``never_override`` to never build baseline models.

   **metric** (str, optional)
      If **baseline_usage** is set to ``conditionally_override``, evaluates models on a test set. Recommended metric: ``MALQ``. Default value: ``MALQ``. Available metrics are:

   .. list-table::
      :widths: 80 420
      :header-rows: 1

      * - Metric
        - Description
      * - **ACC**
        - .. include:: ../../includes_terms/term_metric_acc.rst
      * - **AUC**
        - .. include:: ../../includes_terms/term_metric_auc.rst
      * - **F1**
        - .. include:: ../../includes_terms/term_metric_f1.rst
      * - **LOGLOSS**
        - .. include:: ../../includes_terms/term_metric_logloss.rst
      * - **MAE**
        - .. include:: ../../includes_terms/term_metric_mae.rst
      * - **MALQ**
        - .. include:: ../../includes_terms/term_metric_malq.rst
      * - **MAPE**
        - .. include:: ../../includes_terms/term_metric_mape.rst
      * - **MULTILOGLOSS**
        - .. include:: ../../includes_terms/term_metric_multilogloss.rst
      * - **PRECISION**
        - .. include:: ../../includes_terms/term_metric_prec.rst
      * - **PREC@K**
        - .. include:: ../../includes_terms/term_metric_prec_at_k.rst
      * - **REC**
        - .. include:: ../../includes_terms/term_metric_rec.rst
      * - **REC@K**
        - .. include:: ../../includes_terms/term_metric_rec_at_k.rst
      * - **RMSE**
        - .. include:: ../../includes_terms/term_metric_rmse.rst

**behaviors** (list, optional)
   A section that describes suspicious patterns of behavior or activity that may indicate a security breach.

   **description** (str, optional)
      A description of the suspicious pattern or behavior.

   **key** (str, required)
      The label for the behavior. May be any combination of alphanumeric characters and underscores.

   **notes** (str, optional)
      Additional notes related to the suspicious pattern or behavior.

   **primary** (bool, optional)
      Specifies if the suspicious pattern or behavior is a primary indicator of an abnormality. If ``False``, the suspicious pattern or behavior is a secondary indicator. Default value: ``True``.

   **stages** (list of int, required)
      A list of integers that correspond to each campaign stage to which the **targets** are applied. For example, abnormailty in **proxy_web_bytes_in** is indicative of stages 3 (discovery) and 4 (capture).

   **targets** (dictionary, required)
      One (or more) columns to be used as measurement targets, with each measurement target and relevance weight listed individually. See **relevance_weights**.

**context** (list of str, optional)
   A list of columns in the modeling table to be used as inputs for **all** models. These columns may be from the entity attribute tables or be columns created in other ways, but they cannot be datetime columns. If unspecified, columns with capitalized names will be used as context columns.

**default_learning_options** (optional)
   The learning options to apply to all models. Use **learning_options** to override the default learning options on a per-model basis.

   **bin_count** (int, optional)
      .. include:: ../../includes_config/learning_options_bin_count.rst

   **binning_algorithm** (enum, optional)
      .. include:: ../../includes_config/learning_options_binning_algorithm.rst

   **explorations** (bool, optional)
      .. include:: ../../includes_config/learning_options_explorations.rst

   **model_type** (enum, optional)
      .. include:: ../../includes_config/learning_options_model_type.rst

   **regularization** (enum, optional)
      .. include:: ../../includes_config/learning_options_regularization.rst

   **time_tradeoff** (int, optional)
      .. include:: ../../includes_config/learning_options_time_tradeoff.rst

**default_strategy** (optional)
   The strategy to apply to all targets. Use **strategies** to override the default strategy on a per-target basis.

   **strategy_name** (enum, required)
      .. include:: ../../includes_config/modeling_strategy.rst

      .. include:: ../../includes_config/modeling_strategy_comp_cdf.rst

      .. include:: ../../includes_config/modeling_strategy_top_k_entities.rst

   **top_k_threshold** (int, optional)
      .. include:: ../../includes_config/modeling_strategy_top_k_entities_threshold.rst

**drop_and_ignore_nones** (list of str, optional)
   A list of modeling targets for which ``None`` values will be dropped instead of converted to zero. At learning time, models with targets in this list will ignore rows with ``None``-valued targets. At prediction time, rows with ``None``-valued targets will be assigned a surprise score of zero. For targets not in this list, ``None``-valued targets will simply be replaced with zeroes.

**drop_zeros_from_training** (list of str, optional)
   A list of modeling targets for which rows with zero-valued targets will be dropped at training time. Useful for a target distribution that is heavily dominated by zeros. A target that is specified in the **drop_zeros_from_training** list and is not specified in the **drop_and_ignore_nones** list will have rows with None-valued targets dropped from the training set.

**enrichments** (list, optional)
   A list of columns to use as inputs for specified models, in adition to the context columns used for **all** models. These columns are typically computed after the modeling table is built. Enrichments are applied to the modeling table before the handoff to extensions for the **learn_models** and **predict** application phases.

   **add_timestamp_features** (bool, optional)
      Specifies if weekday and weekend days are used as inputs to the modeling table. When ``True``, columns for **WEEKDAY_FEATURE** and **IS_WEEKEND** are added. Default value: ``True``.

   **none_strategy** (str, optional)
      The ``None`` handling strategy for historical percentiles when calculating the column-wise percentiles for rows. Possible values: ``default``, ``ignore``, ``impute``, and ``infectious``. Default value: ``default``. (The default behavior is the same as ``infectious``.)

      Use ``ignore`` to ignore ``None`` values.

      Use ``impute`` to replace ``None`` values with zero.

      Use ``infectious`` to leave column values of ``None`` as ``None``.

   **pass_through** (list, optional)
      A list of input/target pairs that define specific pass-through enrichments.

      **input_column** (str, required)
         The column to use as an input to all targets for this enrichment. This column must already exist in the modeling table prior to the **learn_models** application phase.

      **targets** (list of str, required)
         A list of modeling targets that use the enrichment defined by **input_column**.

   **past_median** (list of str, optional)
      A list of the target columns for which the median value is calculated over rolling windows, and then used as inputs to the models. The median is calculated over a trailing 4-week, 1, 2, 3, 4, 5, 6, and 7 day windows.

**entities** (list of enum, optional)
   A list of entities for which models are produced. Possible values: ``['user', 'host']``, ``['host']``, or ``['user']``. Default value: ``['user', 'host']``.

**experimentation** (str, optional)
   The name of a user-defined function that is located in the :doc:`extension package </extensions>` that completely overrides the **learn_models**, **predict**, and **postprocess** application phases when learning the model, scoring it, and then generating its results.

**function** (str, optional)
   The name of a user-defined function that is located in the :doc:`extension package </extensions>` and is run at the **end** of the **learn_models** application phase.

**learning_options** (optional)
   A section that defines various learning options to use for the named measurement model. These values override the default learning options that are specified by **default_learning_options**.

   **bin_count** (int, optional)
      .. include:: ../../includes_config/learning_options_bin_count.rst

   **binning_algorithm** (enum, optional)
      .. include:: ../../includes_config/learning_options_binning_algorithm.rst

   **explorations** (bool, optional)
      .. include:: ../../includes_config/learning_options_explorations.rst

   **model_type** (enum, optional)
      .. include:: ../../includes_config/learning_options_model_type.rst

   **regularization** (enum, optional)
      .. include:: ../../includes_config/learning_options_regularization.rst

   **time_tradeoff** (int, optional)
      .. include:: ../../includes_config/learning_options_time_tradeoff.rst

**load_split_from_workspace** (string, optional)
   The fully qualified path to a previously-saved modeling split table. This table must be in the workspace for the engine.

**log_targets** (list of str, optional)
   A list of modeling targets for which the target value will be log-transformed prior to model learning. This setting is typically only used for ``LINEAR_REGRESSION`` models, applied to non-negative targets with a large range. By default, no targets are transformed.

**model_structure** (enum, optional)
   The type of measurement model to learn. Possible values: ``flat`` and ``nested``. Default value: ``flat``.

   Use ``flat`` to learn a measurement model that simply predicts the expected measurement of a target.

   Use ``nested`` to learn a nested model that first predicts if a value is present, and then predicts the expected value provided that a value is present. The nested model type improves the handling of targets that may have null values. For exaple, a measurement model that predicts the average entropy of the URLs visited in a day will not have a value if no web browsing occurred on that day.

**per_model_context_overrides** (optional)
   A list of context columns to use for the named ``<model>``. These columns are used instead of the default context columns or global context columns defined by the **context** setting.

   The column names must exist in the modeling table. These columns may be from the entity attribute tables or be columns created in other ways, but they cannot be datetime columns. If unspecified, columns with capitalized names will be used as context columns.

   .. note:: Per-model context overrides (**per_model_context_overrides**) **and** per-model context refinements (**per_model_context_refinements**) may not be set for the same ``<model>`` One or the other is supported, but not both.

**per_model_context_refinements** (optional)
   A list of context columns to add or remove from the global set of context columns for the named ``<model>``.

   .. note:: Per-model context overrides (**per_model_context_overrides**) **and** per-model context refinements (**per_model_context_refinements**) may not be set for the same ``<model>`` One or the other is supported, but not both.

**relevance_weights** (list, optional)
   A list that specifies the levels used to determine which entities contain surprising behavior and may be assigned a noteworthy :ref:`surprise score <model-data-surprise-scores>`. **strong**, **weak**, and **circumstantial** must be specified. Custom levels for custom measurement targets may also be specified.

   **circumstantial** (float, required)
      The relative strength for circumstantial measurement targets. Must be greater than or equal to ``0.0``. Default value: ``0.0``.

      .. include:: ../../includes_config/modeling_relevance_weights_circumstantial.rst

   **custom** (list, optional)
      A list of measurement targets, and then a custom strength to be applied to that target.

   **strong** (float, required)
      The relative strength for strong measurement targets. Must be greater than or equal to ``0.0``. Default value: ``1.0``.

      .. include:: ../../includes_config/modeling_relevance_weights_strong.rst

   **weak** (float, required)
      The relative strength for weak measurement targets. Must be greater than or equal to ``0.0``. Default value: ``0.25``.

      .. include:: ../../includes_config/modeling_relevance_weights_weak.rst

.. include:: ../../includes_config/replace_function.rst

**report_diagnostics** (bool, optional)
   Specifies if diagnostics are reported (and sent) to |versive|. Default value: ``True``.

**save_diagnostics** (bool, optional)
   Specifies if diagnostics are saved. Default value: ``True``.

**save_split** (bool, optional)
   Specifies if the train, tune, and test set split is saved. Default value: ``False``.

**save_summaries** (bool, optional)
   Specifies if a summary of the modeling table is saved. Default value: ``True``.

**split** (optional)
   Specifies how a modeling table is split into three data tables, one to train data, one to tune data, and one to test data.

   **date_column** (str, optional)
      A date column in the modeling table.

   **entity_column** (str, optional)
      An entity column in the modeling table.

   **name** (str, optional)
      The name of the split strategy. Possible values: ``DateBasedSplitStrategy``, ``EntityBasedSplitStrategy``, ``NoSplitStrategy``, or ``RandomSplitStrategy``. Default value: ``NoSplitStrategy``.

      Use ``DateBasedSplitStrategy`` to train, tune, and test in chronological order based on **proportion** and a **date_column**.

      Use ``EntityBasedSplitStrategy`` to train and tune on different subsets of hosts based on **proportion**, a **date_column**, and an **entity_column**.

      Use ``NoSplitStrategy`` to train, tune, and test by using the full data set without splitting.

      Use ``RandomSplitStrategy`` to train, tune, and test data using a random split.

   **proportions** (list of int, optional)
      The proportions to use for splitting data. List up to 3 integers, the first for train, the second for tune, and the third for test. For example: ``[3, 1, 1]``.

   **split_seed** (str, optional)
      Determines how entity names are hashed, after which determines how entities are included in the train set and the tune data sets. When specified, the entities will be included into each split the same way from run to run.

**strategies** (optional)
   A section that defines the strategy to use for scoring model results for the named ``<target>``. Strategies are defined per measurement model, rather than per combo model. It is possible to set values for all strategies using the **default_strategy** setting.

   **strategy_name** (enum, required)
      .. include:: ../../includes_config/modeling_strategy.rst

      .. include:: ../../includes_config/modeling_strategy_comp_cdf.rst

      .. include:: ../../includes_config/modeling_strategy_top_k_entities.rst

   **top_k_threshold** (int, optional)
      .. include:: ../../includes_config/modeling_strategy_top_k_entities_threshold.rst

**training_num_shards** (int, optional)
   The number of shards to use when learning a model. Models learn better with a smaller number of shards. Default value: ``6``.

**use_feedback_columns** (bool, optional)
   Specifies if feedback columns are added to the modeling table. Feedback columns are used as inputs for learning a measurement model for a target. The naming convention for feedback columns appends ``_tags`` to associate the feedback column with the target column when building the ``details-with-feedback`` table during the **results** phase. For example: a target column named ``TARGET1`` will have a feedback column named ``TARGET1_tags``. Default value: ``True``.



.. _configure-multi-day-features:

multi_day_features
==================================================
**multi_day_features** is a required section that configures multi-day features. These configuration values are only applied when the **drive** command is run. If **generate_modeling_table** is run without invoking the **drive** command, most of these values may be provided via the command line.

Usage
--------------------------------------------------
**multi_day_features** is a top-level group of settings.

.. code-block:: yaml

   multi_day_features:
     additional_attribute:
       - attribute_source_name: 'name_of_table'
         join: True
         left_columns: 'column_name'
       ...
     first_date: 'date'
     internal_neighbors_limit: 100
     lookback_internal_neighbors: [7]
     multi_day_eat_lookback_windows:
       custom_fill_type: (only when missing_values_policy: fill_custom)
       custom_fill_value: (only when missing_values_policy: fill_custom)
       lookback_duration: -3d
       lookback_step: -1d
       missing_values_policy: fill_default
     none_strategy: 'ignore'



Settings
--------------------------------------------------
Each setting for the **multi_day_features** group of settings is described below, with the top-level **multi_day_features** setting listed first, after which all other settings are listed alphabetically.

**multi_day_features** (optional)
   The parent setting group for the **multi_day_features** section.

.. include:: ../../includes_config/additional_attribute.rst

**first_date** (str, required)
   The date at which the state begins, in ``YYYYMMDD`` format.

**internal_neighbors_limit** (int, optional)
   An integer that specifies the threshold below which exact information about the daily number of internal neighbors per entity is kept. Default value: ``250``.

   .. note:: Large values may substantially increase execution time and may cause out-of-memory situations because more intermediate data is required during processing.

**lookback_internal_neighbors** (list of int, optional)
   A list of integers that indicate the number of days to include in a time window for a "number of internal neighbors" aggregation. Default value:  ``[3]``.

   Each value specifies the number of days in the lookback windows for both recent (target) and historical lookback (model input) data. For example, the default value will produce two columns of lookback data:

   #. 3 days starting from the current day, inclusive (target)
   #. 3 days starting from 3 days ago (model input)

   and a value of ``[3, 7]`` means that four columns for lookback windows will be created:

   #. 3 days starting from the current day, inclusive of the current day (target)
   #. 3 days starting from 3 days ago (model input)
   #. 7 days starting from the current day, inclusive of the current day (target)
   #. 7 days starting from 7 days ago (model input)

**multi_day_eat_lookback_windows** (list, optional)
   Defines a global lookback window that is used by all multi-day transforms that do not have transform-specific settings defined in the :ref:`configure-entity-attribute-sources` section of the configuration file.

   .. include:: ../../includes_config/multiday_lookback.rst


**none_strategy** (str, optional)
   A string that indicates how historical percentile aggregations will treat missing values during aggregation. Possible values: ``ignore``, ``impute``, or ``infectious``. Default value: ``ignore``.

   Use ``ignore`` to calculate the column-wise percentile of given rows while ignoring rows that have ``None`` values.

   Use ``impute`` to calculate the column-wise percentile of given rows while replacing rows that have ``None`` values with zero.

   Use ``infectious`` to calculate the column-wise percentile of given rows. If a row's value is ``None``, the percentile of that row will be ``None``.







.. 
.. .. _configure-parse:
.. 
.. parse
.. ==================================================
.. **parse** is a required subsection under **sources** that defines regular expression parsing for the named data source, along with applying transforms to ensure the resulting data table matches the canonical form.
.. 
.. Usage
.. --------------------------------------------------
.. The **parse** section must be located under **sources**, with one **parse** section defined for each source type:
.. 
.. .. code-block:: yaml
.. 
..    sources:
..      <source_type>:
..        parse:
..          after_save_functions:
..            - 'function_name'
..            - ...
..          block_name: 'block_name'
..          canonicalize:
..            - function: 'function_name'
..              inputs: ['name']
..              type: record_filter     # or table_converter
..    
..            --or--
..    
..            - function: 'function_name'
..              inputs: ['name']
..              outputs:
..                - name: 'column_name'
..                  type: 'data_type'
..              type: record_converter
..          date_format: 'string'
..          default_validation: True
..          delimiter: 'parse'
..          encoding: 'string'
..          force_hostname_lowercase: True
..          force_user_lowercase: False
..          maximum_parse_error_percentage: float
..          partitions:
..            - per_worker_after_load_partitions: int
..            - per_worker_before_save_paritions: int
..            - total_after_load_partitions: int
..            - total_before_save_partitions: int
..          replace_function: 'function_name'
..          retain_raw_hostname: True
..          retain_raw_source_ip: True
..          retain_raw_user: True
..          schema: '/path/to/file.yaml'
..          strict: True
..          user_contains_domain: False
..          validation
..            <column>:
..              missing:
..                count: int
..                percent: int
..            <column>:
..              regex:
..                count: int
..                pattern: 'string'
..                percent: float
.. 
.. .. include:: ../../includes_config/source_type_usage.rst
.. 
.. Settings
.. --------------------------------------------------
.. Each setting for the **parse** group of settings is described below, with the top-level **parse** setting listed first, after which all other settings are listed alphabetically.
.. 
.. **parse** (required)
..    The parent setting group for the **parse** section.
.. 
.. .. include:: ../../includes_config/after_save_function_generic.rst
.. 
.. .. include:: ../../includes_config/block_name.rst
.. 
.. **canonicalize** (list, optional)
..    A list of settings used to configure transforms to apply at the end of the **parse** phase so the resulting DataTable matches the canonical form:
.. 
..    **function** (str, required)
..       The name of a user-defined function that is located in the :doc:`extension package </extensions>` and is run at the end of the application phase for the **parse** command.
.. 
..    **inputs** (list of str, optional)
..       A list of columns to be used as inputs to the user-defined function specified by the **function** setting.
.. 
..    **outputs** (dictionary of name and type, optional)
..       When **type** is set to ``record_converter``, a name/type pair is output from the user-defined function specified by the **function** setting, and then added to the returned DataTable. **name** is the column name. **type** must be one of ``datetime``, ``float``, ``int``, ``ndarray``, or ``str``.
.. 
..    **type** (enum, required)
..       The type of transform to apply. Possible values: ``record_converter``, ``record_filter``, or ``table_converter``.
.. 
..       Use ``record_converter`` to update a DataTable one record or row at a time by adding new columns, and then return a DataTable that contains only rows that meet the criteria defined by **function**.
.. 
..       Use ``record_filter`` to select rows from a DataTable that should be kept for further operations, and then return a new DataTable with columns replaced or added. The user-defined function may define columns to be added or replaced.
.. 
..       Use ``table_converter`` to transform a DataTable, and then return a new DataTable.
.. 
.. **date_format** (str, required)
..    The format used to represent datetime values in the named data source. Possible values are ``epoch`` or a timestamp in in Coordinated Universal Time (UTC) that is defined using :ref:`strptime syntax <python-strptime-syntax>`.
.. 
.. .. include:: ../../includes_config/default_validation.rst
.. 
.. **delimiter** (str, optional)
..    A string used to separate data fields. By default, there is no delimiter.
.. 
.. **encoding** (str, optional)
..    The encoding type to use when parsing data. For example: ``ISO-8859-1``. Default value: ``None`` (no encoding type specified).
.. 
.. **force_hostname_lowercase** (bool, optional)
..    Specifies if capital letters in hostnames are converted to lowercase. Default value: ``True``.
.. 
.. **force_user_lowercase** (bool, optional)
..    Specifies if capital letters in user names are converted to lowercase. Default value: ``False``.
.. 
.. **maximum_parse_error_percentage** (float, optional)
..    The percentage at which too many parser errors are present in parsed results. If the parsed results have too many parser errors, an error is returned.
.. 
.. .. include:: ../../includes_config/partitions.rst
.. 
.. .. include:: ../../includes_config/replace_function.rst
.. 
.. **retain_raw_hostname** (bool, optional)
..    Specifies if raw hostname information is retained or removed. Default value: ``True`` (retained).
.. 
.. **retain_raw_source_ip** (bool, optional)
..    Specifies if raw source IP information is retained or removed. Default value: ``True`` (retained).
.. 
.. **retain_raw_user** (bool, optional)
..    Specifies if raw user information is retained or removed. Default value: ``True`` (retained).
.. 
.. **schema** (str, required)
..    The absolute path to :ref:`a YAML file on the local file system <process-data-yaml-patterns>` that contains the regular expressions required to parse the raw data.
.. 
.. **strict** (bool, optional)
..    Specifies if only columns in canonical form may be present in the DataTable. When ``False``, columns in non-canonical form may be present. Default value: ``True``.
.. 
.. **user_contains_domain** (bool, optional)
..    Specifies if user data contains a domain name. Default value: ``False``.
.. 
.. .. include:: ../../includes_config/validation.rst
.. 







.. _configure-predict:

predict
==================================================
**predict** is an optional subsection used to configure the behavior of the engine during the **predict** application phase. Configuration changes made to this section are applied the next time the ``predict`` application phase is run.

Usage
--------------------------------------------------
**predict** is a top-level group of settings.

.. code-block:: yaml

   predict:
     after_save_functions:
       - 'function_name'
       - ...
     block_name: 'block_name'
     function: 'function_name'
     head_entity_target_count: int
     head_entity_target_mass: float
     interesting_measurement_regions:
       measurement_target: [value, value]
       ...
     noteworthy_surprise_rank: 0
     replace_function: 'function_name'

Settings
--------------------------------------------------
Each setting for the **predict** group of settings is described below, with the top-level **predict** setting listed first, after which all other settings are listed alphabetically.

**predict** (optional)
   The parent setting group for the **predict** section.

.. include:: ../../includes_config/after_save_function_predict.rst

.. include:: ../../includes_config/block_name.rst

**function** (str, optional)
   The name of a user-defined function that is located in the :doc:`extension package </extensions>` and is run via the **predict** command.

**head_entity_target_count** (int, optional)
   The number of items in a group that are assigned top weighting.

**head_entity_target_mass** (float, optional)
   The fraction of weight that is allocated to top-weighted rows.

**interesting_measurement_regions** (list, dictionary, optional)
   A list of measurement targets, and then for each measurement target a range of values that are of possible interest. This range is applied during the **predict** phase to ensure they affect stage-level surprise scores. By default all values are considered interesting.

   For example:

   .. code-block:: yaml

      interesting_measurement_regions:
        bytes_out_to_external_sum: [100000000, inf]  # at least 100 MB
        internal_neighbors_netflow_count_distinct: [5, inf]

   For both settings, ``inf`` specifies there is no maximum value.

   .. note:: Take into consideration the data that is input to the interesting measurement region. For example, if using the flow of bytes out of the network as input to a model, ensure that the range of values is configured to support the size of the data to be input.

   .. warning:: Use this setting carefully. When specified, if an observed measurement value falls outside the range of values that are of possible interest, the surprise score for that observed measurement value is set to zero.

**noteworthy_surprise_rank** (int, optional)
   Use to rank surprise scores for a target.  Default value: ``0``.

   For example, to generate a list of no more than the top 25 largest surprise scores for a target, set this to ``25``. If that target has only 10 surprise scores, its ranked list will contain only 10 items. Another example is to set this value to a very low percentage---0.5%, 1%, 2%, etc.---of the total number of machines in the network.

.. include:: ../../includes_config/replace_function.rst





.. 
.. .. _configure-prepare:
.. 
.. prepare
.. ==================================================
.. **prepare** is an optional subsection under **sources** that configures any preparation steps to run on raw data for the named data source prior to parsing.
.. 
.. Usage
.. --------------------------------------------------
.. The **prepare** section is located under **sources**, with one **prepare** section defined for each source type:
.. 
.. .. code-block:: yaml
.. 
..    sources:
..      <source_type>:
..        prepare:
..          function: 'function_name'
..          reshard: False
.. 
.. .. include:: ../../includes_config/source_type_usage.rst
.. 
.. Settings
.. --------------------------------------------------
.. Each setting for the **prepare** group of settings is described below, with the top-level **prepare** setting listed first, after which all other settings are listed alphabetically.
.. 
.. **prepare** (optional)
..    The parent setting group for the **prepare** section.
.. 
.. **function** (str, optional)
..    The name of a user-defined function that is located in the :doc:`extension package </extensions>` and is run at the end of the application phase for the **prepare** command.
.. 
.. **reshard** (bool, optional)
..    Specifies if data is to be resharded during this phase. Default value: ``False``.
.. 
.. .. include:: ../../includes_config/validation.rst
.. 








.. _configure-results:

results
==================================================
**results** is an optional subsection used to configure the behaviors of the **postprocess** application phase, which turns per-day scoring anomalies into threat cases that describe user and/or host behavior that the models have identified as anomalous. Each threat case corresponds to one (or more) campaign stages in the kill chain.

By default, anomaly details are in the format:

.. code-block:: none

   <target>=<actual> (expected <measurement>) NLL=<stage score>

For example:

.. code-block:: none

   123=456 (789) NLL=abc


Usage
--------------------------------------------------
**results** is a top-level group of settings.

.. code-block:: yaml

   results:
     after_save_functions:
       - 'function_name'
       - ...
     block_name: 'block_name'
     connectivity_graph_function: 'default_load_connectivity_graph'
     connectivity_graph_max_depth: None    # no limit
     connectivity_graph_source: 'None'
     details_formatter_factory_function: 'function_name'
     drilldown_table_in_memory_limit: 100000
     extra_columns_in_details: []
     extra_host_columns_in_details: []
     extra_user_columns_in_details: []
     filtration:
       - host_whitelist: 'whitelist_filename'
       - user_whitelist: 'whitelist_filename'
     formatter_factory_function: 'function_name'
     group_connected_host_anomalies: True
     in_memory_rows_limit: 10000
     in_memory_aggregated_rows_limit: 125000
     keep_threat_case_function: 'function_name'
     max_anomalies_per_entity_day_stage: 10
     max_behaviors_in_threat_case_list: 3
     max_campaign_stage: 5
     max_hosts_in_threat_case_list: 3
     max_threat_cases: 25
     max_users_in_threat_case_list: 3
     min_behavior_score_in_threat_case_list: 0.0
     min_campaign_stage: 3
     min_host_score_in_threat_case_list: 0
     min_nll_in_details: 0
     min_user_score_in_threat_case_list: 0
     nicknames:
       - 'column_name': "nickname"
       ...
     output_column_mappings: {
       - input_columns: 'column_name', 'column_name', ...
       - show_inputs: False
     }
     reason_templates:
       - 'column_name': "template"
       ...
     replace_function: 'function_name'
     required_stages:
       - [3, 4]
       - [5]
     score_threshold: '-inf'
     show_anomaly_details: True
     signal_threshold_percent: 5
     stage_score_weights:
       3: 1
       4: 1
       5: 1
     stage_thresholds:
       int: float
       ...
     target_drilldown_map:
       'target_name': 'class_name'
       ...
     threat_case_overlap_merge_percent: 33
     units: 'string'
     use_dns_data: False
     use_netflow_data: False
     use_proxy_data: False
     use_random_anomaly_ids: True
     use_random_gids: True
     warnings_table_partitions_per_worker: 1


Settings
--------------------------------------------------
Each setting for the **results** group of settings is described below, with the top-level **results** setting listed first, after which all other settings are listed alphabetically.

**results** (required)
   The parent setting group for the **results** section.

.. include:: ../../includes_config/after_save_function_postprocess.rst

.. include:: ../../includes_config/block_name.rst

**connectivity_graph_function** (str, optional)
   .. include:: ../../includes_config/connectivity_graph_function.rst

**connectivity_graph_max_depth** (int, optional)
   .. include:: ../../includes_config/connectivity_graph_max_depth.rst

**connectivity_graph_source** (str, optional)
   .. include:: ../../includes_config/connectivity_graph_source.rst

**details_formatter_factory_function** (str, optional)
   The name of a user-defined function that is located in the :doc:`extension package </extensions>` and determines how reasons in anomaly details are formatted.

**drilldown_table_in_memory_limit** (int, optional)
   The level below which drilldown tables are processed in-memory. Default value: ``100000``.

**extra_columns_in_details** (list of str, optional)
   A list of extra columns to incude in the detailed anomaly list for users and hosts. Extra columns are copied from the scored tables that are created by the **predict** command. The default value is an empty list (zero extra columns).

**extra_host_columns_in_details** (list of str, optional)
   A list of extra columns to incude in the detailed anomaly list for hosts. Extra columns are copied from the scored tables that are created by the **predict** command. The default value is an empty list (zero extra columns).

**extra_user_columns_in_details** (list of str, optional)
   A list of extra columns to incude in the detailed anomaly list for users. Extra columns are copied from the scored tables that are created by the **predict** command. The default value is an empty list (zero extra columns).

**filtration** (enum, optional)
   Users and/or hosts may be excluded from results by specifying them in a single-column text file that is stored on the default storage (typically HDFS).

   **host_whitelist** (str, optional)
      The name of the file in which any hosts to be excluded from the results are specified.

   **user_whitelist** (str, optional)
      The name of the file in which any users to be excluded from the results are specified.

**formatter_factory_function** (str, optional)
   The name of a user-defined function that is located in the :doc:`extension package </extensions>` and formats the results.

**group_connected_host_anomalies** (bool, optional)
   .. include:: ../../includes_config/group_connected_host_anomalies.rst

**in_memory_rows_limit** (int, optional)
   The number of rows in the warnings table under which calculations are performed in-memory on the master node. If the number of rows exceeds this number, calculations are performed using distributed operations. In-memory calculations are much faster than distributed calculations, but may cause out-of-memory exceptions. If out-of-memory exceptions are present, decrease this value. Set to ``0`` to disable. Default value: ``10000``.

**in_memory_aggregated_rows_limit** (int, optional)
   The number of rows under which tables are processed in-memory on the master node. If the number of rows exceeds this number, calculations are performed using distributed operations. In-memory calculations are much faster than distributed calculations, but may cause out-of-memory exceptions. Default value: ``125000``.

**keep_threat_case_function** (string, optional)
   .. include:: ../../includes_terms/term_threat_case_level.rst

   This setting defines the level at which threat cases are shown. Possible values: ``require_noteworthy_all_stages``, ``require_noteworthy_2_maybe_3_stages``,  ``require_noteworthy_2_out_of_3_stages``, ``require_noteworthy_per_host_2_out_3``, the name of a user-defined function, or ``None``. Default value: ``None``.

   Set to ``require_noteworthy_all_stages`` to only show threat cases that contain :ref:`at least one noteworthy anomaly (and associated surprise score) <model-data-noteworthy-scores>` in every campaign stage.

   Set to ``require_noteworthy_2_maybe_3_stages`` to only show threat cases that have a single host and :ref:`noteworthy anomalies in two of three stages <model-data-noteworthy-scores>` or threat cases that have multiple hosts and noteworthy anomalies in three of three stages.

   Set to ``require_noteworthy_2_out_of_3_stages`` to only show threat cases that contain :ref:`at least one noteworthy anomaly (and associated surprise score) <model-data-noteworthy-scores>` in at least two stages.

   Set to ``require_noteworthy_per_host_2_out_3`` to only show threat cases that have a single host and also contain :ref:`noteworthy anomalies in two of three stages <model-data-noteworthy-scores>`.

   Set to the name of a user-defined function that is located in the :doc:`extension package </extensions>` and specifies which threat cases are saved for a custom level.

**max_anomalies_per_entity_day_stage** (int, optional)
   For each campaign stage, the maximum number of anomalies to produce per entity (user or host), per day. Default value: ``10``.

**max_behaviors_in_threat_case_list** (int, optional)
   The maximum number of unique behaviors to show, per threat case, in the **top_behaviors** field of the threat case list. Default value: ``3``.

**max_campaign_stage** (int, optional)
   The stage at building a threat case should end, inclusive. Possible values: ``3``, ``4``, or ``5``. Use with **min_campaign_stage** to define the starting and ending stages. Default value: ``5``.

**max_hosts_in_threat_case_list** (int, optional)
   The maximum number of unique hosts to show, per threat case, in the **top_hosts** field of the threat case list. Default value: ``3``.

**max_threat_cases** (int, optional)
   The maximum number of threat cases that may be included in the threat case list. Default value: ``25``.

   .. note:: The number of threat cases created is often lower than this value because only threat cases that pass all of the filtering and acceptance criteria for threat case creation may be added to the threat case list.

**max_users_in_threat_case_list** (int, optional)
   The maximum number of unique users to show, per threat case, in the **top_users** field of the threat case list. Default value: ``3``.

**min_behavior_score_in_threat_case_list** (float, optional)
   The minimum behavior score of the behaviors to be included in the **top_behaviors** field of the threat case list. Default value: ``0.0``.

**min_campaign_stage** (int, optional)
   The stage at building a threat case should begin. Possible values: ``3``, ``4``, or ``5``. Use with **max_campaign_stage** to define the starting and ending stages. Default value: ``3``.

**min_host_score_in_threat_case_list** (float, optional)
   The minimum stage score of the hosts to be included in the **top_hosts** field of the threat case list. Default value: ``0``.

**min_nll_in_details** (float, optional)
   The minimum negative log likelihood (NLL) of the anomalies to be kept in each threat case's anomaly list. Each anomaly with an NLL below this threshold will be excluded from the generated threat case details. Use ``0`` to prevent anomalies from being excluded. Default value: ``0``.

**min_user_score_in_threat_case_list** (float, optional)
   The minimum stage score of the users to be included in the **top_users** field of the threat case list. Default value: ``0``.

**nicknames** (list of str, optional)
   One (or more) nicknames to help describe and/or apply context to results.

   .. include:: ../../includes_config/results_nicknames.rst

**output_column_mappings** (dict, optional)
   Use to add columns to the ``details`` table based on one (or more) input columns. The value of the output column is the value of the first input column that is not ``None`` or an empty string.

   **input_columns** (list, required)
      A list of columns to be added to the output table.

   **show_inputs** (bool, optional)
      Use to add the input columns to the ``details.csv`` table. Default value: ``False``.

   For example:

   .. code-block:: yaml

      output_column_mappings:
        fqdn:
          input_columns: ['url_host', 'dest_ip', 'url_regdomain']

   The column ``fqdn`` is added to ``details.csv``. The value assigned to ``fqdn`` is determined by the order of the columns listed in ``input_columns``. The value for the first column that is not ``None`` or an empty string is the value assigned to the ``fqdn`` column.

   Using the previous example, but with ``show_inputs`` set to ``True``, in addition to the ``fqdn`` column being assigned a value, the ``url_host``, ``dest_ip`` and ``url_regdomain`` columns are also added to ``details.csv``.

**reason_templates** (list of str, optional)
   A template that defines a string that is shown to users in the web user interface when feedback is present. A reason describes what occurred and is composed using words and variables. Possible variables:

   * Use ``{COL}`` for the column name, such as ``BYTES_IN``.
   * Use ``{DST}`` for the destination IP address or hostname.
   * Use ``{SRC}`` for the source IP address or hostname.
   * Use ``{VALUE}`` for the column value, such as ``234 MB``.

   A reason code may not contain a semi-colon or parentheses. If a reason template is defined for a result that also has a nickname defined, the reason template takes precedence.

   For example:

   * ``{SRC} is {VALUE}`` would build a reason code similar to "10.10.28.35 is 12345".
   * ``{VALUE} between {SRC} and {DST}`` would build a reason code similar to "VIDEO_FEED between 10.10.28.35 and some.url.com".

.. include:: ../../includes_config/replace_function.rst

**required_stages** (list of int, optional)
   A list of stages that must be present in the threat case before the threat case appears in the results. For example:

   .. code-block:: yaml

      required_stages:
        - [4, 5]
        - [3, 5]
        - [3, 4]

   means that the threat case must include support for

   * stages 4 and 5
   * stages 3 and 5
   * stages 3 and 4

   ensuring that before any threat case may appear in the results, it must appear in at least two stages. Default value:

   .. code-block:: yaml

      required_stages:
        - [3, 4]
        - [5]

   which means that a threat case must appear in stages 3 **and** 4 **or** stage 5 to appear in the results.

**score_threshold** (float, optional)
   The minimum total stage score of a threat case for the threat case to appear in the results. By default, the score threshold is set to the "lowest possible score", i.e. ``'-inf'`` (not a number).

**show_anomaly_details** (bool, optional)
   .. include:: ../../includes_config/results_show_anomaly_details.rst

**signal_threshold_percent** (int, optional)
   The percentage of top-scoring stage-level warnings to use when building threat cases. Default value: ``5`` (or "5%" of the top-scoring stage-level warnings are used to build threat cases).

**stage_score_weights** ({int, float}, optional)
   A mapping of campaign stages and weights. By default the weight for all stages (or for any stage not specified) is ``1``. When a stage is specified:

   * The score for that stage is multiplied by the specified weight
   * Scores in summary and details output will contain weighted scores
   * A column named ``unweighted_score`` is added that contains the original unweighted score
   * The strength for behaviors is unchanged in details, but is weighted in the top behaviors listed in the summary

**stage_thresholds** ({int, float}, optional)
   The minimum stage score for a campaign stage for the threat case to appear in the results. Default value: ``'-inf'`` (not a number).

**target_name** (dict of str, optional)
   A dictionary of target names and drilldown names. The target names are built by the pipeline during data processing and modeling. The drilldown name determines the locations from which this data will be available from the web UI. For example:

   .. code-block:: yaml

      flow_internal_SNMP_sum_lookback_0-30_days: 'IPDrillDown'
      flow_internal_TCPUDP_sum_lookback_0-30_days: 'IPDrillDown'

**threat_case_overlap_merge_percent** (float, optional)
   .. include:: ../../includes_config/threat_case_overlap_merge_percent.rst

**units** (list of str, optional)
   One (or more) units of measurements, such as ``bytes``, ``logins``, ``ports``, ``services``, ``systems``, or ``unknown``. These units of measurements are matched with targets and are used to help display the finding in the |vse| web user interface. Each unit of measurement should specify the target, and then the unit.

   .. code-block:: yaml

      target_name: "unit"
      target_name: "unit"
      ...

   The units of measurement are matched with the target in the ``config.json`` file for the |vse| web user interface, where the ``statistic``, ``activity``, and ``method`` are specified:

   .. code-block:: javascript

       "target_name": {
         "statistic": "",
         "activity": "",
         "method": ""
       },

   For example, if ``logins`` is the unit of measurement, and then the settings in the ``config.json`` file are:

   .. code-block:: javascript

       "target_name": {
         "statistic": "successful",
         "activity": "were logged",
         "method": "this event type"
       },

   then for 100 associated findings the web user interface would display "100 successful logins were logged via this event type".

**use_dns_data** (bool, optional)
   Specifies if aggregated DNS data is used when building connectivity graphs, and then if those graphs are used to identify links between hosts and/or between hosts and users. Default value: ``False``.

   .. warning:: It is recommended to build connectivity graphs that identify links between hosts and/or between hosts and users by setting the ``use_netflow_data`` setting to ``True``.

**use_netflow_data** (bool, optional)
   Specifies if aggregated netflow data is used when building connectivity graphs, and then if those graphs are used to identify links between hosts and/or between hosts and users. Default value: ``False``.

**use_proxy_data** (bool, optional)
   Specifies if aggregated proxy data is used when building connectivity graphs, and then if those graphs are used to identify links between hosts and/or between hosts and users. Default value: ``False``.

**use_random_anomaly_ids** (bool, optional)
   Specifies if UUIDs are generated randomly or are hash-based identifiers. Set to ``True`` to generate random UUIDs for threat cases. Set to ``False`` for hash-based identifiers. Default value: ``True``.

**use_random_gids** (bool, optional)
   Specifies the type of unique identifier to use with threat cases: GUID or UUID. Set to ``True`` to generate a GUID as the threat case identifier. Set to ``False`` to generate a UUID. Default value: ``True``.

**warnings_table_partitions_per_worker** (int, optional)
   The number of partitions to use during postprocessing. The default value is fine for most data sets. If out-of-memory errors are present with larger data sets, increase this value (to increase the number of partitions). Default value: ``1``.






.. _configure-sampling:

sampling
==================================================
**sampling** is an optional subsection used to configure how to sample data before processing it through each application phase, including providing an option for using a user-defined function to run custom code for sampling.

Running the engine on the full dataset can be time-consuming. For more rapid iteration during model development, sample the data before running the application, and then inspect the output of every phase, including the modeling table.

.. note:: Data is not sampled randomly to provide better modeling results. In contrast, the **subsample** argument for data loading functions in the |versive| platform is random and may not provide all of the relevant data to model the behavior for a particular entity.

Usage
--------------------------------------------------
**sampling** is a top-level group of settings.

.. code-block:: yaml

   sampling:
     by: user
     function: 'function_name'
     percent: 100
     skip_values:
       - '-'
       - ':'
       - ...
     sources:
       - 'dataset_name'
       - 'dataset_name'
       - ...


Settings
--------------------------------------------------
Each setting for the **sampling** group of settings is described below, with the top-level **sampling** setting listed first, after which all other settings are listed alphabetically.

**sampling** (required)
   The parent setting group for the **sampling** section.

**by** (enum, required)
   The entity from which data is sampled. Possible values: ``source_ip`` or ``user``.

**function** (str, optional)
   The name of a user-defined function that is located in the :doc:`extension package </extensions>` for use with sampling.

**percent** (int, required)
   An integer between ``1`` and ``100`` that indicates the amount of data to retain. Default value: ``100``.

**skip_values** (list of str, optional)
   A list of values that, if present, indicate a row should not be included in the sampled data. Include quotation marks if the value is a YAML indicator character. For example: ``"-"``, ``":"``, ``"["``, or ``"]"``. ``None`` values are always skipped. Default value: ``None``.

**sources** (list of str, optional)
   One (or more) datasets to be sampled. By default, every table that contains the **sampling by** column will be sampled.




.. 
.. .. _configure-sources:
.. 
.. sources
.. ==================================================
.. **sources** is a required section that specifies the configuration for each data source. The **sources** section has six defined subsections for each ``<data_source>``:
.. 
.. Usage
.. --------------------------------------------------
.. **sources** is a top-level group of settings.
.. 
.. .. code-block:: yaml
.. 
..    sources:
..      <source_type>:
..        merge_group: 'group_name'
..        path: '/path/to/text_data/'
..        table_type: 'table_type'
..        use_for_lookup: True
..        use_for_modeling: True
.. 
.. .. include:: ../../includes_config/source_type_usage.rst
.. 
.. Settings
.. --------------------------------------------------
.. Each setting for the **sources** group of settings is described below, with the top-level **sources** setting listed first, after which all other settings are listed alphabetically.
.. 
.. **sources** (required)
..    The parent setting group for the **sources** section.
.. 
.. **merge_group** (str, optional)
..    The name of the dataset into which this data is merged prior to aggregation. By default, the dataset to which this data is merged is the first from the list of data sources, as determined by alphabetical order. Only one dataset in a merge_group may define **aggregate** settings if there is more than one dataset.
.. 
.. **path** (str, required)
..    The absolute path to the directory in which raw data files for this data source are located. For example: ``/security/netflow/netflow/``.
.. 
.. **table_type** (str, required)
..    The source type for this table, which determines what canonical table against which validation is run. Possible values: ``AuthenticationTable``, ``DHCPTable``, ``DNSTable``, ``NetFlowTable``, or ``ProxyTable``. Multiple data sources may exist for the same source type, such as two proxy data sources.
.. 
.. **use_for_lookup** (bool, optional)
..    Specify if the named data source is used when generating the lookup table when running the **drive** command. Default value: ``True``.
.. 
.. **use_for_modeling** (bool, optional)
..    Specify if the named data source is used when generating the modeling table when running the **drive** command. Default value: ``True``.
.. 




.. 
.. .. _configure-standardize-keys:
.. 
.. standardize_keys
.. ==================================================
.. **standardize_keys** is an optional subsection under **sources** that specifies the function to used when standardizing keys for the named data source.
.. 
.. Usage
.. --------------------------------------------------
.. The **standardize_keys** section is located under **sources**, with one **standardize_keys** section defined for each source type:
.. 
.. .. code-block:: yaml
.. 
..    sources:
..      <source_type>:
..        standardize_keys:
..          after_save_functions:
..            - 'function_name'
..            - ...
..          block_name: 'block_name'
..          function: 'function_name'
..          replace_function: 'function_name'
.. 
.. .. include:: ../../includes_config/source_type_usage.rst
.. 
.. Settings
.. --------------------------------------------------
.. Each setting for the **standardize_keys** group of settings is described below, with the top-level **standardize_keys** setting listed first, after which all other settings are listed alphabetically.
.. 
.. **standardize_keys** (optional)
..    The parent setting group for the **standardize_keys** section.
.. 
.. .. include:: ../../includes_config/after_save_function_generic.rst
.. 
.. .. include:: ../../includes_config/block_name.rst
.. 
.. **function** (str, optional)
..    The name of a user-defined function that is located in the :doc:`extension package </extensions>` and is run at the end of the application phase for the **standardize_keys** command.
.. 
.. .. include:: ../../includes_config/replace_function.rst
.. 




.. 
.. .. _configure-transform:
.. 
.. transform
.. ==================================================
.. **transform** is an optional subsection under **sources** that configures transforms for a particular data source, including disabling default transform and applying custom transforms when required. An entity attribute table and/or additional attributes may be added prior to transform operations.
.. 
.. .. note:: The engine does not provide the ability to insert custom transforms between the default transforms. If a greater degree of extension is required, first define a custom data type, and then override the transform method.
.. 
.. Usage
.. --------------------------------------------------
.. The **transform** section is located under **sources**, with one **transform** section defined for each source type:
.. 
.. .. code-block:: yaml
.. 
..    sources:
..      <source_type>:
..        transform:
..          add_datasource_name_column: False
..          additional_attribute:
..            - attribute_source_name: 'name_of_table'
..              join: True
..              left_columns:
..                - 'column_name'
..                - ...
..            ...
..          after_save_functions:
..            - 'function_name'
..            - ...
..          block_name: 'block_name'
..          default_validation: True
..          function: 'function_name'
..          partitions:
..            - per_worker_after_load_partitions: int
..            - per_worker_before_save_paritions: int
..            - total_after_load_partitions: int
..            - total_before_save_partitions: int
..          replace_function: 'function_name'
..          top_url_list_path: '/path/to/file.txt'
..          turn_off: [ 'transform_name', 'transform_name', ... ]
..          validation
..            <column>:
..              missing:
..                count: int
..                percent: int
..            <column>:
..              regex:
..                count: int
..                pattern: 'string'
..                percent: float
.. 
.. .. include:: ../../includes_config/source_type_usage.rst
.. 
.. Settings
.. --------------------------------------------------
.. Each setting for the **transform** group of settings is described below, with the top-level **transform** setting listed first, after which all other settings are listed alphabetically.
.. 
.. **transform** (optional)
..    The parent setting group for the **transform** section.
.. 
.. **add_datasource_name_column** (bool, optional)
..    Specifies if the name of the data source is added as a column. Default value: ``False``.
.. 
.. .. include:: ../../includes_config/additional_attribute.rst
.. 
.. .. include:: ../../includes_config/after_save_function_generic.rst
.. 
.. .. include:: ../../includes_config/block_name.rst
.. 
.. .. include:: ../../includes_config/default_validation.rst
.. 
.. **function** (str, optional)
..    The name of a user-defined function that is located in the :doc:`extension package </extensions>` and is run at the end of the application phase for the **transform** command.
.. 
.. .. include:: ../../includes_config/partitions.rst
.. 
.. .. include:: ../../includes_config/replace_function.rst
.. 
.. **top_url_list_path** (str, optional)
..    The absolute path to a text file on the local file system that contains the most commonly visited URLs.
.. 
.. **turn_off** (list of str, optional)
..    A list of default transforms to be disabled.
.. 
..    .. note:: Aggregations for ``authn``, ``dhcp``, ``dns``, ``proxy``, and entity attribute table source types are run by default.
.. 
.. .. include:: ../../includes_config/validation.rst
.. 







.. _configure-workspaces:

workspaces
==================================================
**workspaces** is an optional subsection that configures the subdirectories under which artifacts are stored for each of the phases run when using the **drive** command. Using subdirectories allows data for different modeling approaches to be organized in separate directories. For example, a configuration may be initially developed on a sample of the full data set and artifacts may be created against that sample data set, in addition to artifacts created for the full data set.

.. warning:: The transition table cannot be generated when using the **drive** command, which means that entropy transforms are skipped.

.. TODO: What does that warning ^^^ actually mean?

Directory Patterns
--------------------------------------------------
There are two directory patterns available to define naming formats for subdirectories in the engine workspace:

* Default. Set **naming** in the **drive** configuration set to ``V1_1``.
* Nested. Set **naming** in the **drive** configuration set to ``V2_0``.

See the following sections for more details about each directory pattern.


.. TODO: Make sure this is covered.

.. There are two naming formats available to use for each of the subdirectories:
.. V1_1 and V2. Depending on the naming format specified, the way workspaces set
.. for multiple phases are combined will differ. To configure the version to use,
.. set **naming** in the **drive** section of the configuration file.

.. In the paths, the <groupby_key> is set at the command line for **aggregate**
.. and **generate_modeling_table** and is either "day_user" or "day_host". If
.. you use the V1_1 version for workspace naming (set by **naming** in the
.. **drive** section of the configuration file) and run **drive**, the
.. accepted values are the same. However, if you run ``drive`` with the V2 version
.. for workspace naming, the accepted values are "host" and "user".

.. With **drive**, the application will produce artifacts for both keys, because
.. you currently cannot use ``--groupby_key`` with **drive** to specify a single
.. groupby_key.

.. The <entity> is set in the **modeling** section of the configuration file for
.. the **learn_models**, **predict**, and **postprocess** phases and is
.. either "user" or "host". The application workflow will produce artifacts
.. for both entities, because you currently cannot use **entities** to process data
.. for only a single entity.

Default
++++++++++++++++++++++++++++++++++++++++++++++++++
The default directory structure for the workspace does not nest workspaces and produces output under:

.. code-block:: none

   <base_path>/<phase_name>

The default workspace name for the ``learn_models``, ``predict``, and ``postprocess`` workspaces is ``default``; all other workspaces default to ``None``. ``<groupby_key>`` is one of ``day_user`` or ``day_host``.

The following list shows the paths for each phase:

**prepare**
   ``<base_path>/prepared/<data_source>_<date>``

   or

   ``<base_path>/prepared/<workspace>/<data_source>_<date>``

**parse**
   ``<base_path>/parsed/<data_source>_<date>``

   or

   ``<base_path>/parsed/<workspace>/<data_source>_<date>``

**generate_transition_table**
   ``<base_path>/transition_table/<transition_table_name>``

**generate_lookup_table**
   ``<base_path>/lookup_table/<lookup_table_name>``

   or

   ``<base_path>/lookup_table/<workspace>/<lookup_table_name>``

**standardize_keys**
   ``<base_path>/standardized/<data_source>_<date>``

   or

   ``<base_path>/standardized/<workspace>/<data_source>_<date>``

**transform**
   ``<base_path>/transformed/<data_source>_<date>``

   or

   ``<base_path>/transformed/<workspace>/<data_source>_<date>``

**aggregate**
   ``<base_path>/agg/<groupby_key>/<data_source>_<date>``

   or

   ``<base_path>/agg/<groupby_key>/<workspace>/<data_source>_<date>``

**generate_modeling_table**
   ``<base_path>/modeling_table/<groupby_key>/<modeling_table_name>_<date>``

   or

   ``<base_path>/modeling_table/<groupby_key>/<workspace>/<modeling_table_name>_<date>``

**learn_models**
   ``<base_path>/models/<data_sets>/<workspace>/<start>-<end>/<entity>``

**predict**
   ``<base_path>/scored/<workspace>/<start>-<end>/<entity>/<phase>/<campaign stage>``

**postprocess**
   ``<base_path>/results/<workspace>/<start>-<end>/<entity>``

**import_results_feedback**
   As specified in **entity_attribute_sources**.

For example, if the **workspaces** section defines:

.. code-block:: yaml

   workspaces:
     parse: thirty_percent
     generate_modeling_table: w_pipeline
     learn_models: experiment

The following directories are output:

* ``<base_path>/parsed/thirty_percent/<data_source>_<date>``
* ``<base_path>/lookup_table/<lookup_table_name>``
* ``<base_path>/standardized/<data_source>_<date>``
* ``<base_path>/transformed/<data_source>_<date>``
* ``<base_path>/agg/<data_source>_<date>``
* ``<base_path>/modeling_table/<groupby_key>/w_pipeline/<modeling_table_name>_<date>``
* ``<base_path>/models/<data_sets>/experiment/<start>-<end>/<entity>``
* ``<base_path>/scored/default/<start>-<end>/<entity>/<phase>/<campaign stage>``
* ``<base_path>/results/default/<start>-<end>/<entity>``


Nested
++++++++++++++++++++++++++++++++++++++++++++++++++
The directory structure for the workspace may be nested and, if nested, produces output under:

.. code-block:: none

   <base_path>/<groupby_key>

or:

.. code-block:: none

   <base_path>/<entity>

The default workspace name for all phases is ``hanto``. (``default`` is ignored as a workspace.) If a workspace is repeated for adjoining phases, it will be ignored. For example, hanto.foo.bar.default and hanto.foo.foo.bar are both interpreted as hanto.foo.bar. ``<groupby_key>`` is one of ``user`` or ``host``.

.. TODO: FIX THIS "Workspaces are nested in phase order: ``prepare``, ``parse``, ``generate_??????????????``, ``generate_lookup_table``, ``standardize_keys``, ``transform``, ``aggregate``, ``generate_modeling_table``, ``learn_models``, ``predict``, and then ``postprocess``. This helps make it easier to run the engine with multiple configuration variations."

phase components are joined with a period (``.``). This allows multiple variations---``A.1``, ``A.2``, ``B.1``, ``B.2``, and so on---and prevents variations from attempting to write to the same locations.

The following list shows the paths for each phase:

**prepare**
   ``<base_path>/<workspace>/prepared/<data_source>/<start>-<end>``

**parse**
   ``<base_path>/<workspace>/parsed/<data_source>/<start>-<end>``

**generate_transition_table**
   ``<base_path>/transition_table/<transition_table_name>``

**generate_lookup_table**
   ``<base_path>/<workspace>/lookup_table/<lookup_table_name>``

**standardize_keys**
   ``<base_path>/<workspace>/standardized/<data_source>/<start>-<end>``

**transform**
   ``<base_path>/<workspace>/transformed/<data_source>/<start>-<end>``

**aggregate**
   ``<base_path>/<workspace>/agg/<data_source>/<start>-<end>``

**generate_modeling_table**
   ``<base_path>/<groupby_key>/<workspace>/modeling_table/<modeling_table_name>_<date>``

**learn_models**
   ``<base_path>/<entity>/<workspace>/models/<datasets>/<workspace>/<start>-<end>``

**predict**
   ``<base_path>/<entity>/<workspace>/scored/<start>-<end>/<phase>/<campaign stage>``

**postprocess**
   ``<base_path>/<entity>/<workspace>/results/<start>-<end>``

**import_results_feedback**
   As specified in **entity_attribute_sources**.


For example, if the **workspaces** section defines:

.. code-block:: yaml

   workspaces:
     parse: thirty_percent
     generate_modeling_table: w_pipeline
     learn_models: experiment

The following directories are output:

* ``<base_path>/hanto.thirty_percent/parsed/<data_source>_<date>``
* ``<base_path>/hanto.thirty_percent/lookup_table/<lookup_table_name>``
* ``<base_path>/hanto.thirty_percent/standardized/<data_source>/<start>-<end>``
* ``<base_path>/hanto.thirty_percent/transformed/<data_source>/<start>-<end>``
* ``<base_path>/hanto.thirty_percent/agg/<data_source>/<start>-<end>``
* ``<base_path>/<groupby_key>/hanto.thirty_percent.w_pipeline/modeling_table/<modeling_table_name>_<date>``
* ``<base_path>/<entity>/hanto.thirty_percent.w_pipeline.experiment/models/<data_sets>/<workspace>/<start>-<end>``
* ``<base_path>/<entity>/hanto.thirty_percent.w_pipeline.experiment/scored/<start>-<end>/<phase>/<campaign stage>``
* ``<base_path>/<entity>/hanto.thirty_percent.w_pipeline.experiment/results/<data_source>/<start>-<end>``


Usage
--------------------------------------------------
**workspaces** is a top-level group of settings.

.. code-block:: yaml

   workspaces:
     generate: 'subdirectory_name'
     generate_lookup_table: 'subdirectory_name'
     generate_modeling_table: 'subdirectory_name'
     learn_models: 'subdirectory_name'
     postprocess: 'subdirectory_name'
     predict: 'subdirectory_name'


Settings
--------------------------------------------------
Each setting for the **workspaces** group of settings is described below, with the top-level **workspaces** setting listed first, after which all other settings are listed alphabetically.

**workspaces** (optional)
   The parent setting group for the **workspaces** section.

**generate** (str, optional)
   A subdirectory to be added to the output path for the **generate_transition_table** phase. For example:

   .. code-block:: yaml

      <base_path>/transition_table/<transition_table_name>

**generate_lookup_table** (str, optional)
   A subdirectory to be added to the output path for the **generate_lookup_table** phase. For example:

   .. code-block:: yaml

      <base_path>/hanto.<subdirectory>/lookup_table/<lookup_table_name>

**generate_modeling_table** (str, optional)
   A subdirectory to be added to the output path for the **generate_modeling_table** phase. For example:

   .. code-block:: yaml

      <base_path>/<groupby_key>/hanto.<subdirectory>/modeling_table/<modeling_table_name>_<date>

**learn_models** (str, optional)
   A subdirectory to be added to the output path for the **learn_models** phase. Default value: ``'default'``. For example:

   .. code-block:: yaml

      <base_path>/<entity>/hanto.<subdirectory>/models/<datasets>/hanto.<subdirectory>/<start>-<end>

**import_results_feedback** (str, optional)
   As specified in **entity_attribute_sources**.

**predict** (str, optional)
   A subdirectory to be added to the output path for the **predict** phase. Default value: ``'default'``. For example:

   .. code-block:: yaml

      <base_path>/<entity>/hanto.<subdirectory>/scored/<start>-<end>/<phase>/<campaign stage>

**postprocess** (str, optional)
   A subdirectory to be added to the output path for the **postprocess** phase. Default value: ``'default'``. For example:

   .. code-block:: yaml

      <base_path>/<entity>/hanto.<subdirectory>/results/<start>-<end>





.. _configure-required-settings:

Example Configuration File
==================================================
The configuration file for the |vse| starts out with only a few required settings:

.. code-block:: yaml

   base_path: '/path/to/working/directory'
   date_path: '%Y%M%d/data'
   default_connection: 'named_connection_to_cluster'
   multi_day_features:
     first_date: 'date'
   sources:
     <source_type>:
       path: '/path/to/text_data/'
       table_type: 'table_type'
       parse:
         date_format: 'format_type'
         schema: '/path/to/file.yaml'

The previous example shows these settings with their default values. These settings represent the minimum set of data against which the |vse| can run, including the path to the data and the type of data to be collected. Please refer to the rest of this topic for more information about customizing and tuning this configuration file. 
