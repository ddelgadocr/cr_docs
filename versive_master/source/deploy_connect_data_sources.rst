.. 
.. versive, primary, security engine
.. 

==================================================
Connect to Data Sources
==================================================

The |versive| platform can retrieve and process data that is stored in Hadoop Distributed File System (HDFS), Amazon S3, or any other file system with a unified view. Multiple compute clusters for the |versive| platform may be running, each with its own authenticated user with varying levels of access to data.

.. image:: ../../images/arch_data_sources.svg
   :width: 600 px
   :align: center

The sections below describe how to connect to data located in these cluster types.


.. _deploy-connect-data-sources-requirements:

Requirements
==================================================
The |versive| platform and the engine must already be installed and configured on the compute cluster before a data source may be connected.


.. _deploy-connect-data-sources-named-connections:

Named Connections
==================================================
.. include:: ../../includes/named_connections.rst

Configure the named connection from the :ref:`cmd-cr-connect` group of commands

.. note:: HDFS data storage requires Kerberos authentication. Use a :ref:`keytab file <deploy-connect-data-sources-hdfs-keytab-file>` **or** a :ref:`delegation token <deploy-connect-data-sources-hdfs-token>`.

.. _deploy-connect-data-sources-amazon-s3:

Amazon S3
==================================================
.. include:: ../../includes/cmd_cr_connect_add_amazon_s3.rst

.. _deploy-connect-data-sources-hdfs-keytab-file:

HDFS Keytab File
==================================================
.. include:: ../../includes/cmd_cr_connect_add_hdfs.rst


.. _deploy-connect-data-sources-hdfs-token:

HDFS Token
==================================================
The |versive| platform may connect to data that is stored on an an HDFS storage cluster using a delegation token and Kerberos authentication.

.. warning:: Delegation tokens expire periodically, which can lead to downtime. Using a :ref:`keytab file <deploy-connect-data-sources-hdfs-keytab-file>` is recommended.

.. note:: Before connecting the |versive| platform to the storage cluster:

   #. Install Kerberos tools on the HDFS storage cluster.
   #. Configure Kerberos authentication to use a delegation token that periodically expires.
   #. Configure two tokens to renew on a schedule that maintains access to the HDFS host. Using two tokens with overlapping expiration windows will help prevent authentication issues.

   Additional information about setting up Kerberos authentication is available at `MIT Kerberos Documentation <http://web.mit.edu/kerberos/krb5-devel/doc/index.html>`_.

After Kerberos is configured on the HDFS storage cluster, make note of HDFS hostname, Kerberos settings, and user account to use for Kerberos authentication.

**To connect to data on an HDFS storage cluster using a token**

#. SSH into the primary node and get a ticket-granting ticket to authenticate the user with the Kerberos Key Distribution Center (KDC):

   .. code-block:: console

      $ kinit <USER>

#. Verify that the token exists:

   .. code-block:: console

      $ klist

#. Request a delegation token from the Kerberos-enabled HDFS service:

   .. code-block:: console

      $ curl -i "http://<HOST>:<PORT>/webhdfs/v1/?op=GETDELEGATIONTOKEN&renewer=<USER>"

   The user encoded in the token is the one set up for Kerberos authentication.

#. Set up named connections by using the :ref:`cmd-cr-connect-add` command:

   .. code-block:: console

      $ cr connect add --name=NAME --type=TYPE host=IP_ADDRESS \
        user=USER delegation_token=TOKEN

   For example:

   .. code-block:: console

      $ cr connect add --name="hdfs" --type="hdfs" host="10.213.168.179" \
        user="hdfs" delegation_token="4xNjg...MjA"

#. Verify the named connection is set up correctly by using the :ref:`cmd-cr-connect-list` command:

   .. include:: ../../includes/cmd_cr_connect_list_hdfs.rst
