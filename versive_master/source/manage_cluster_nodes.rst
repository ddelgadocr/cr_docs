.. 
.. versive, primary, security engine
.. 

==================================================
Manage Nodes
==================================================

After a computing cluster has set up and configured the first time it may be modified, such as adding more nodes, removing nodes, or moving a node from one cluster to another to more effectively balance compute resoures:

* :ref:`manage-cluster-nodes-add-node`
* :ref:`manage-cluster-nodes-change-the-primary-node`
* :ref:`manage-cluster-nodes-cluster-is-unresponsive`
* :ref:`manage-cluster-nodes-move-nodes`
* :ref:`manage-cluster-nodes-recreate-cluster`
* :ref:`manage-cluster-nodes-remove-node`
* :ref:`manage-cluster-nodes-repair-cluster`

Use the :ref:`cmd-cr-cluster`, :ref:`cmd-cr-connect`, :ref:`cmd-cr-install`, :ref:`cmd-cr-node`, and :ref:`cmd-cr-service` groups of commands to interact with the compute cluster from the command shell.


.. _manage-cluster-nodes-add-node:

Add Node to Cluster
==================================================
Use the following steps to add nodes to an existing cluster.

.. TODO: can the 2nd and 3rd paragraphs in step 1 be removed?

#. In order to add a node to an existing cluster, the |versive| platform and the engine must first be installed on the node. This, effectively, creates a single-node cluster.

   For the |versive| platform, complete the installation steps. However, copy the installation package to the node, not the primary node, and then complete the remaining steps.

   For the engine, after the platform install is complete, copy the installation package to the new node, not the primary node, complete the remaining steps required by the platform installation, and then complete the remaining steps for the engine install.

#. From the single-node cluster, free a node:

   .. code-block:: console

      $ cr cluster free --node=IP_ADDRESS

#. Verify the node is free:

   .. code-block:: console

      $ cr cluster view

#. Log out from the node.

#. Connect to the primary node in the existing cluster via SSH.

#. From the primary node, add the node by specifying its IP address:

   .. code-block:: console

      $ cr cluster add —-node=IP_ADDRESS

#. Activate the node in the cluster:

   .. code-block:: console

      $ cr cluster activate —-node=IP_ADDRESS —-cluster=NAME

#. Verify that all nodes are healthy:

   .. code-block:: console

      $ cr cluster status

   All nodes in the cluster, including the one just added, should appear in the printed output.


.. _manage-cluster-nodes-change-the-primary-node:

Change the Primary Node
==================================================
The primary node must be configured before any of the compute nodes in a compute cluster. To change the primary node, first free all the nodes in the cluster, then configure a new primary node, reactivate all of the compute nodes, and then assign them to the cluster.

#. Inspect the existing configuration and make note of the IP address of the primary node and the IP addresses of other nodes in the cluster. At the command line from any node in the cluster:

   .. code-block:: console

      $ cr cluster view

#. Move every node in the cluster to the list of free nodes. For each node:

   .. code-block:: console

      $ cr cluster free --node=IP_ADDRESS

#. Log in to the node chosen to be the new primary node, recreate the cluster, and then make the current node the primary:

   .. code-block:: console

      $ cr cluster activate --node=IP_ADDRESS --cluster=NAME --create

#. Activate the remaining nodes. For each node to be reactivated:

   .. code-block:: console

      $ cr cluster activate --node=IP_ADDRESS  --cluster=NAME






.. _manage-cluster-nodes-cluster-is-unresponsive:

Cluster is Unresponsive
==================================================
If the cluster is not responding, run:

.. code-block:: console

   $ cr cluster status

and attempt to diagnose the specific problem. If the problem is not one of the following, :ref:`repair the cluster <manage-cluster-nodes-repair-cluster>`:

* Command times out
* Slow heartbeat
* Nodes are operational, cluster remains unreponsive


Command Times Out
--------------------------------------------------
If a command times out, but the primary node is operational, then services are not working on the primary node. On the primary node, do the following:

#. Start the services on the primary node:

   .. code-block:: console

      $ cr service start

#. Restart all services on all nodes:

   .. code-block:: console

      $ cr cluster reset

#. Restart the primary node.


Heartbeat > 60 Seconds
--------------------------------------------------
A node with a heartbeat greater than 60 seconds suggests that it may have been shut down. On the node with a slow heartbeat, manually verify that the node is operational:

#. Stop services on the node:

   .. code-block:: console

      $ cr service stop

#. Start services on the node:

   .. code-block:: console

      $ cr service start

#. If the node continues to have a slow heartbeat, restart the node and then run ``cr cluster status``.


Nodes Run, Cluster Down
--------------------------------------------------
If all nodes in the cluster are running, but the cluster itself is still unresponsive, restart services on all nodes in the cluster.

#. From any node in the cluster run:

   .. code-block:: console

      $ cr cluster reset

#. If that does not work, reset the cluster manually. For example:

   .. code-block:: console

      $ ps aux | grep python

#. Identify services related to the |versive| platform and their process IDs (PIDs). For each PID, run:

   .. code-block:: console

      $ kill PID

#. Hard restart all services:

   .. code-block:: console

      $ cr service start --hard



.. _manage-cluster-nodes-move-nodes:

Move Nodes
==================================================
A node may be moved from one cluster to another. Nodes that have been part of an existing cluster must be returned to an unconfigured state prior to being assigned to a new cluster.

#. From any node in the cluster, return a node to an unconfigured state and inform the rest of the cluster that the node is removed:

   .. code-block:: console

      $ cr cluster delete --node=IP_ADDRESS

#. From the unconfigured node, confirm that the state change worked:

   .. code-block:: console

      $ cr cluster view

#. Activate the node in the second cluster:

   .. code-block:: console

      $ cr cluster activate --node=IP_ADDRESS --cluster=NAME



.. _manage-cluster-nodes-recreate-cluster:

Recreate Cluster
==================================================
A non-operational compute cluster cannot be fixed by restarting platform services and must be recreated. A non-operational compute cluster is seen in situations like:

* A cluster error cannot be resolved by running the ``cr cluster status`` command.
* The IP address for a node was changed.
* Multiple dead nodes must be removed from the cluster.

The following steps recreate a non-operational compute cluster. No data will be lost in this process.

#. Inspect the existing configuration:

   .. code-block:: console

      $ cr cluster view

   Make note of the IP address of the primary node, and then the IP addresses for all other nodes in the cluster.

#. Reset every node in the cluster to an unconfigured state:

   .. code-block:: console

      $ cr node hardreset --node=IP_ADDRESS

#. Log in to the new primary node, and then recreate the cluster using the original cluster name:

   .. code-block:: console

      $ cr cluster create CLUSTER_NAME

#. Add the remaining nodes to the cluster. For each node, first run:

   .. code-block:: console

      $ cr cluster add --node=IP_ADDRESS

   and then:

   .. code-block:: console

      $ cr cluster activate --node=IP_ADDRESS --cluster CLUSTER_NAME


.. _manage-cluster-nodes-remove-node:

Remove Node from Cluster
==================================================
The following steps remove a node from an existing cluster. Substitute the actual IP addresses for nodes to be added for the ``IP_ADDRESS`` value shown in the example.

.. warning:: If more than one dead node exists in the cluster, :ref:`the cluster must be repaired <manage-cluster-nodes-repair-cluster>`.

#. Verify the state of the cluster:

   .. code-block:: console

      $ cr cluster status

#. Make note the IP address for the node to be removed.

#. To free the node from cluster, run:

   .. code-block:: console

      $ cr cluster free —-node=IP_ADDRESS

#. Verify that the node has been added to the list of free nodes:

   .. code-block:: console

      $ cr cluster view

   The freed node will be listed in the "Free nodes" section.

#. Delete the node from the cluster:

   .. code-block:: console

      $ cr cluster delete —-node=IP_ADDRESS

#. To verify that the node has been deleted:

   .. code-block:: console

      $ cr cluster view

   The node should not be listed in the command output.

#. Connect via SSH to the deleted node:

   .. code-block:: console

      $ ssh IP_ADDRESS

#. Remove the contents of the ``/opt/cr`` installation directory

   .. code-block:: console

      $ rm -rf /opt/cr/*

   .. note:: The default installation directory is ``/opt/cr``. If the |versive| platform is installed to a different location, substitute that path for ``/opt/cr``.

#. Remove all files in ``/opt/cr``. If there is a permissions error, make sure that permissions are correct. In some cases, permissions must be changed recursively to ``a+rw``:
   
   .. code-block:: console

      $ chmod -R a+rw /opt/cr



.. _manage-cluster-nodes-repair-cluster:

Repair Cluster
==================================================
If the primary node in the cluster is unresponsive---such as if a ping to the node fails or if the ``cr cluster status`` command that is run from a replica node in the cluster shows no status for the primary node---this indicates a server hardware failure on the primary node.

If the primary node has failed and is otherwise unrecoverable, the cluster must be repaired.

#. Inspect the existing configuration:

   .. code-block:: console

      $ cr cluster view

   Make note of the IP address of the all nodes for which an IP addresses is available. (These are the replica nodes in the cluster.)

#. Remove each replica node from the cluster:

   .. code-block:: console

      $ cr cluster free --node=IP_ADDRESS

   and then run:

   .. code-block:: console

      $ cr node hardreset --node=IP_ADDRESS

   This will delete the cr.json file located at ``/opt/cr/etc`` and remove any associations with the failed primary node.

#. Log in to the new primary node, and then recreate the cluster using a unique cluster name:

   .. code-block:: console

      $ cr cluster create CLUSTER_NAME

#. Activate nodes into the cluster. For each node to be added run:

   .. code-block:: console

      $ cr cluster activate --node=IP_ADDRESS --cluster=CLUSTER_NAME


hadoop fs Commands
==================================================
Use the following ``hadoop fs`` commands to monitor engine phases:

To view how far data processing has progressed at this application phase:

.. code-block:: console

   $ hadoop fs -ls root_path/*/datasource_20171*

To view how much data has been processed for a particular day:

.. code-block:: console

   $ hadoop fs -ls root_path/*/*_20171223

To compare the data at this phase to that for similar days to see if the size seems reasonable:

.. code-block:: console

   $ hadoop fs -du -s -h root_path/phase/dataset_201712*

To compare the data at this application phase to that at previous phases to see if the size seems reasonable:

.. code-block:: console

   $ hadoop fs -du -s -h root_path/*/datasource_20171223


