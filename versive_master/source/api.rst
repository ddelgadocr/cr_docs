.. 
.. versive, primary, security engine
.. 

==================================================
Extension APIs
==================================================

The |risp_full| may be used to build out customized table and modeling behaviors for the engine.

.. TODO: Is there a way to just abstract the "RISP" part out of the documentation? There's a requirement to import the cr.user_v1 as risp, but outside that it's just a collection of functions, right? The CSE API, pretty much.

.. TODO: The whole `risp.` pieces in the examples probably needs to be scrubbed out. The correct CONTEXT for these functions are HOW THEY ARE USED IN A CUSTOMIZATION.PY FILE FOR THE PURPOSES OF CUSTOMIZING CSE. The context is not running some python emulator from a command shell, for example.

.. TODO: If this ``cr.datatable.DataTable`` is just a "table" then let's change all that to "table" or "data table" in the sentences, but maybe not the data types.

.. 
.. Using Python
.. 
.. Python is the programming language to use for extending the engine.
.. 
.. * MODES  :ref:`batch mode <python-batch-mode>`, :ref:`interactive mode <python-interactive-mode>`, or :ref:`python-ipython`
.. * IMPORTING_MODULES    
.. * STRPTIME SYNTAX
.. 

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.


Requirements
==================================================
To use the |risp|, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.risp_v1 as risp

and then for each use of a function in this API within the customization package add ``risp.`` as a prefix to the function name. For example:

.. code-block:: python

   risp.add_columns()

or:

.. code-block:: python

   risp.classification_summary()


.. 
.. Data Types
.. ==================================================
.. 
.. The following data types are in this API:
.. 
.. * bool
.. * cr.datatable.DataTable
.. * cr.science.Model
.. * cr.science.PredictionTable
.. * DataTable (same as cr.datatable.DataTable?)
.. * dict
.. * float
.. * function
.. * hint
.. * int
.. * list of ColumnMetadata (see :ref:`api-add-columns`)
.. * list of cr.datatable.DataTable
.. * list of float
.. * list of hints
.. * list of str
.. * list of tuples
.. * LocalDataTable
.. * object
.. * Plot
.. * str
.. * tuple
.. * tuple of str
.. 
.. Some odd ones:
.. 
.. * ({key: [str]}, required) (see flexi_categorize in RISP)
.. * (fn(category cols) -> key, optional) (see flexi_categorize in RISP)
.. * {key: [str]} (flexi_categorize has this)
.. * tuple/custom op[/args] or list of tuples/custom ops[/args] (this is in aggregate)
.. 
.. 
.. TODO: integrate this (it's from a troubleshooting topic):
.. 
.. Lists contain multiple items where order is not important and are enclosed in square brackets. For example, "[item1, item2, item3]".
.. 
.. Tuples contain multiple items where order is important, such as when indicating the name of columns that correspond to one another in different tables. Tuples are enclosed in parentheses. For example, "(column1, column2)".
.. 
.. You may encounter problems if you forget to include the closing bracket or parentheses, forget commas separating items, or forget quotation marks around strings inside a list or tuple.
.. 





Function Groups
==================================================

Tables:

* :ref:`api-add-columns`
* :ref:`api-add-context`
* :ref:`api-add-context-from`
* :ref:`api-aggregate`
* :ref:`api-count-frequencies`
* :ref:`api-count-rows`
* :ref:`api-create-context-custom-op`
* :ref:`api-create-context-op`
* :ref:`api-create-custom-op`
* :ref:`api-create-parser-hint-date`
* :ref:`api-create-parser-hint-maxerr`
* :ref:`api-create-parser-hint-missing`
* :ref:`api-create-parser-hint-table-maxerr`
* :ref:`api-delete-columns`
* :ref:`api-flexi-categorize`
* :ref:`api-generate-rows`
* :ref:`api-get-column-descriptions`
* :ref:`api-get-column-names`
* :ref:`api-get-delimiter`
* :ref:`api-get-example-rows`
* :ref:`api-join`
* :ref:`api-load-crtable`
* :ref:`api-load-csv`
* :ref:`api-load-tabular`
* :ref:`api-merge-tables`
* :ref:`api-plot-bar`
* :ref:`api-plot-histogram`
* :ref:`api-plot-line`
* :ref:`api-plot-scatter`
* :ref:`api-relate`
* :ref:`api-remove-duplicate-rows`
* :ref:`api-rename-columns`
* :ref:`api-replace-columns`
* :ref:`api-save-crtable`
* :ref:`api-save-csv`
* :ref:`api-save-plot`
* :ref:`api-save-table-schema`
* :ref:`api-save-xls`
* :ref:`api-select-columns`
* :ref:`api-select-rows`
* :ref:`api-split-table`
* :ref:`api-summarize-table`

Models:

* :ref:`api-classification-summary`
* :ref:`api-customize-explanations`
* :ref:`api-describe-model`
* :ref:`api-learn-model`
* :ref:`api-load-model`
* :ref:`api-measure-confusion-matrix`
* :ref:`api-measure-input-importance`
* :ref:`api-measure-learning-curve`
* :ref:`api-measure-score-curve`
* :ref:`api-predict`
* :ref:`api-save-model`
* :ref:`api-score-predictions`
* :ref:`api-summarize-model`

Checkpoints for already-saved tables and models:

* :ref:`api-checkpoint`
* :ref:`api-get-checkpoints`
* :ref:`api-load-checkpoint`

Custom functions:

* :ref:`api-get-udf-debug-logs`
* :ref:`api-udf-debug-log`

Platform:

* :ref:`api-get-connections`
* :ref:`api-get-session-property`
* :ref:`api-get-versions`
* :ref:`api-initialize-cluster`
* :ref:`api-set-session-property`
* :ref:`api-shutdown-cluster`


.. _api-add-columns:

add_columns()
==================================================
The :ref:`api-add-columns` function adds one (or more) columns to a table.

.. note:: Use the :ref:`api-join` function to add columns from another table. Use the :ref:`api-replace-columns` function to add columns and remove the input columns.

Usage
--------------------------------------------------

.. code-block:: python

   risp.add_columns(table,
                    input_columns,
                    new_columns,
                    udf,
                    description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**input_columns** (str or list of str, required)
   One (or more) columns to be used as inputs to the custom function.

**new_columns** (str or list of str, required)
   One (or more) column names to add to the table.

.. TODO: Investigate this: we can't link it for now: cr.datatable.ColumnMetadata.create(). The data type can be ``ColumnMetadata`` or ``list of ColumnMetadata``, in addition to the str/list of str.

**table** (cr.datatable.DataTable, required)
   The table to which columns generated by the custom function are added.

**udf** (function, required)
   A custom function that is used to generate the data in the new column (or columns). The number of values taken as inputs in the custom function must equal the number of columns in ``input_columns``. The number of items returned by the custom function (as either a single item or multiple items in a tuple) must equal the number of columns in ``new_columns``.

Returns
--------------------------------------------------
A cr.datatable.DataTable with the new columns added.

Example
--------------------------------------------------

.. code-block:: python

   def cut_in_half(a):
     half = .5 * a
     return half
   
   new_table = risp.add_columns(old_table,
                                input_columns='full',
                                new_columns='half',
                                udf=cut_in_half)
   
   def find_min_max(a, b):
     return (min(a, b), max(a, b))
   
   new_table = risp.add_columns(old_table,
                                input_columns=['F1', 'F2'],
                                new_columns=['min', 'max'],
                                udf=find_min_max)



.. _api-add-context:

add_context()
==================================================
.. include:: ../../includes_api/function_add_context.rst

.. note:: Use the :ref:`api-add-context-from` function to aggregate data from another table.

Usage
--------------------------------------------------

.. code-block:: python

   add_context(table,
               timestamp_column,
               context_ops,
               group_by_columns=None,
               description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**context_ops** (tuple or list of tuples, required)
   A tuple or a list of tuples that contain context operations that describe how to aggregate the data.

   Use the :ref:`api-create-context-op` function to create context operations using simple pre-defined aggregation functions.

   Use the :ref:`api-create-context-custom-op` function to create context operations that are defined by a custom function.

**description** (str, optional)
   A string to be added to log output.

**group_by_columns** (str or list of str, optional)
   A column or a list of columns that contain the categorical information by which the table data is grouped. Values are aggregated only for rows that share the same categories in these columns.

**table** (cr.datatable.DataTable, required)
   The table to which columns are added.

**timestamp_column** (str, required)
   The column in which timestamp data is located.

Returns
--------------------------------------------------
A cr.datatable.DataTable with the new columns added that contain data aggregated according to ``context_ops``.

Example
--------------------------------------------------
The following example will create a table that includes two new columns: ``bid_mean`` and ``ask_mean``, which contain the average for the 10-second interval that brackets the current row:

.. code-block:: python

   bid_op = risp.create_context_op(new_column='bid_mean',
                                   input_column='bid',
                                   aggregate_op='mean',
                                   start_offset=-5,
                                   end_offset=5,
                                   unit='seconds')
   ask_op = risp.create_context_op(new_column='ask_mean',
                                   input_column='ask',
                                   aggregate_op='mean',
                                   start_offset=-5,
                                   end_offset=5,
                                   unit='seconds')
   agg_table = risp.add_context(data,
                                timestamp_column='timestamp',
                                context_ops=[bid_op, ask_op],
                                group_by_columns=['gb_col1',
                                                  'gb_col2'])




.. _api-add-context-from:

add_context_from()
==================================================
.. include:: ../../includes_api/function_add_context_from.rst

.. note:: Use the :ref:`api-add-context` function to aggregate data from the same table. Use :ref:`api-create-context-op` to create context operations using predefined aggregation functions, such as sum or average. Use :ref:`api-create-custom-op` to create context operations from a custom function.

Usage
--------------------------------------------------

.. code-block:: python

   add_context_from(table,
                    from_table,
                    timestamp_columns,
                    context_ops,
                    group_by_columns=None,
                    description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**context_ops** (tuple or list of tuples, required)
   The context operation (or context operations) that describe how to aggregate data. Context operations are created using the :ref:`api-create-context-op` function.

   .. warning:: If context operations are created with ``unit='rows'``, the interval may contain a different number of rows, depending on if there is an exact match between the timestamps in the primary and secondary tables. For example, if ``start_offset=-2``, ``end_offset=2``, and the ``from_table`` does not contain a row with a timestamp that exactly matches a row in the primary table, the interval will contain four rows. If there is an exact match, the interval will contain five rows (the matching row and two from before and after).

**description** (str, optional)
   A string to be added to log output.

**from_table** (cr.datatable.DataTable, required)
   The table from which data used for aggregation is located.

**group_by_columns** (str, tuple of str, list of str, or list of tuples, optional)
   The columns that contain the categorical information by which the table data is grouped. If the column names are the same in each table, provide a string or list of strings. If the column names differ in each table, provide a two-item tuple or a list of two-item tuples that contain the column names in ``table`` and ``from_table``, respectively.

**table** (cr.datatable.DataTable, required)
   The table to which columns are added.

**timestamp_columns** (str or tuple of str, required)
   A column or a list of columns that contain the timestamp data. If column names are identical in multiple tables, use a string. If column names are not identical in multiple tables, use a two-item tuple that contains the column names in ``table`` and ``from_table`` respectively.

Returns
--------------------------------------------------
A cr.datatable.DataTable with new columns added that contain data aggregated according to ``context_ops``.

Example
--------------------------------------------------

.. TODO: the following example CLEARLY has nothing to do with CSE. need a new example!

.. code-block:: python

   bid_op = risp.create_context_op(new_column='bid_mean',
                                   input_column='bid',
                                   aggregate_op='mean',
                                   start_offset=-5,
                                   end_offset=5,
                                   unit='seconds')
   ask_op = risp.create_context_op(new_column='ask_mean',
                                   input_column='ask',
                                   aggregate_op='mean',
                                   start_offset=-5,
                                   end_offset=5,
                                   unit='seconds')
   agg_table = risp.add_context_from(data,
                                     from_table=satellite_data,
                                     timestamp_columns=('timestamp',
                                                        'sat_timestamp'),
                                     context_ops=[bid_op, ask_op],
                                     group_by_columns=['gb_col1',
                                                      ('gb_col2',
                                                       'gb_col2_in_sat')])

The resulting table includes two new columns---``bid_mean`` and ``ask_mean``---that contain the average values for the 10-second interval that brackets the current row.



.. _api-aggregate:

aggregate()
==================================================
The :ref:`api-aggregate` function aggregates data in a table by grouping according to the data in one or more columns and then performing operations, such as calculating the mean or standard deviation, on the data in each group.

.. TODO: Integrate the following:
.. 
.. To create custom ops that aggregate data according to a user-defined function (UDF), use cr.risp_v1.create_custom_op().
.. 
.. You can group by more than one column of data, such as year and then month, by providing multiple column names in group_by_columns. Avoid None values in the columns indicated by group_by_columns, which may cause incorrect results.
.. 
.. See also:
.. To create a table that shows how frequently a particular value appears in each category defined in group_by_columns, use cr.risp_v1.count_frequencies().
.. 

Usage
--------------------------------------------------

.. code-block:: python

   aggregate(table,
             group_by_columns,
             aggregate_ops)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**aggregate_ops** (tuple/custom op[/args] or list of tuples/custom ops[/args], required)
   The aggregate operation to perform. This may be defined by a custom operation or by a tuple that contains a column name and an operation. For a tuple:

   .. code-block:: python

      [('column', 'aggregate_function'),
       ('column', 'aggregate_function') ]

   where:

   column (str):
      The column containing data on which the aggregate function will be applied.

   aggregate_function (str):
      The operation to carry out on the aggregated data in that column. The following functions are available:

      .. list-table::
         :widths: 150 350
         :header-rows: 1

         * - Function
           - Description
         * - ``args``
           - Potential argument of aggregate function. Makes sense only for some aggregate functions, such as ``categorical_sum``.
         * - ``categorical_sum``
           - Needs extra argument in the tuple, list of values. Aggregation (virtually) replaces each value by its position in that list and then counts how many times each value occurs in the group. Result is list of length N + 1, where 0th element is count of ``args[0]``, etc., and last element is count of keys that were not in ``args``.
         * - ``count``
           - The total number of values for the group, not including missing values.
         * - ``count_ngrams``
           - Input column should have type str. ``args_arguments`` is single integer, a length of ngrams. Creates a map from the ngram (substring of length N) into counter, showing how many times that ngram occurs in the input column.
         * - ``empty``
           - The number of missing values for the group.
         * - ``hourly_sum``
           - Input column should have type ``Timestamp``. ``args_arguments`` is a single argument, a list of values, typically of length ``24``, but some values can be omitted if uninterested. Aggregation (virtually) replaces each timestamp by its hour and then counts how many times each value occurs in the table. Result is list of length N + 1, where 0th element is count of ``args_argument[0]``, etc., and last element is count of keys that were not in the ``args_argument``.
         * - ``list``
           - A table that contains a list of all the unique values in a column, in alphanumeric order. Values that are not strings will be converted to strings. ``args_arguments``, if present, is the limit on resulting list size.
         * - ``max``
           - The maximum value for the group.
         * - ``mean``
           - The arithmetic mean of the values for the group.
         * - ``median``
           - The 50th percentile value for the group. This may be an approximation if the number of values in the group is large.
         * - ``merge_counters``
           - Input column should have type ``TypedMap(some type, int)``. That is type of the result as well. Aggregate merges all maps in the column, adding together all values for the same keys.
         * - ``min``
           - The minimum value for the group.
         * - ``set``
           - Input column should have type ``int``, ``float``, ``str``, or ``TypedList`` (``int``, ``float``, or ``str``). Creates a sorted list of all of the unique values in a column, similar to the ``list`` aggregate function. ``args_arguments``, if present, is the limit on resulting list size.
         * - ``stddev``
           - The standard deviation with n-1 degrees of freedom of the values for the group.
         * - ``sum``
           - The sum of the values for the group.
         * - ``tuple_set``
           - Same as ``set``.

**group_by_columns** (str or list of str, required)
   A single column or list of columns that contain the categorical information by which the table data will be grouped.

**table** (cr.datatable.DataTable, required)
   The table containing the data to be aggregated.

Returns
--------------------------------------------------
A cr.datatable.DataTable that contains columns for each of the ``group_by_columns`` and for each of the specified ``aggregate_ops``. The number of rows is equal to the number of distinct values or bins in ``group_by_columns``.

Example
--------------------------------------------------

The following table has three columns: time, customer_id, and amount. The example will provide the mean, sum, and spread for all transactions per customer:

.. code-block:: python

   spread_fn = lambda b : max(b) - min(b) if b else None
   example_op = risp.create_custom_op(new_columns='spread',
                                      input_columns='amount',
                                      udf=spread_fn)
   per_cust = risp.aggregate(data,
                             group_by_columns='customer_id',
                             aggregate_ops=[('amount', 'mean'),
                                            ('amount', 'sum'),
                                             example_op])
   risp.get_column_names(per_cust)
   ['customer_id', 'amount_mean', 'amount_sum', 'spread']




.. _api-classification-summary:

classification_summary()
==================================================
The :ref:`api-classification-summary` function calculates per-class classification metrics from a table of predictions produced from a binary or multiclass classification model.

Usage
--------------------------------------------------

.. code-block:: python

   classification_summary(table,
                          threshold=0.5)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.science.PredictionTable, required)
   A table that contains inputs, a column of observed values of the target, and a column of predicted values of the target. Obtained by running the :ref:`api-predict` function on a test table.

**threshold** (float, optional)
   A number between 0 and 1 that determines the threshold for predictions to be classified as either 0 or 1 (binary classification only). For example, if threshold=0.6 and the predicted value is 0.7, it will be classified as 1. Default value: ``0.5``.

Returns
--------------------------------------------------
A LocalDataTable containing columns corresponding to a particular classification metric and rows corresponding to each class.

Example
--------------------------------------------------

.. code-block:: python

   model = risp.learn_model(target='label',
                            model='MULTI_CLASSIFICATION',
                            train_table=train,
                            tune_table=tune)
   predictions = risp.predict(model,
                              test)
   risp.classification_summary(predictions,
                               threshold=0.5)
   

For example:

.. code-block:: sql

   ----------- ----------- -------- ------- ---------- ---------
    table       Precision   Recall   F1      Accuracy   Support
   ----------- ----------- -------- ------- ---------- ---------
    class 0     0.644       0.887    0.746   0.860      772.0
    class 1     0.755       0.823    0.788   0.893      771.0
    class 2     0.741       0.617    0.673   0.856      806.0
    class 3     0.687       0.653    0.670   0.830      907.0
    class 4     0.711       0.554    0.623   0.839      824.0
    aggregate   0.708       0.707    0.707   0.702      4080.0
   ----------- ----------- -------- ------- ---------- ---------







.. _api-count-frequencies:

count_frequencies()
==================================================
The :ref:`api-count-frequencies` function groups data in a table according to categorical data in one (or more) columns, and then count how frequently a particular value appears in one (or more) columns for each defined category.

Usage
--------------------------------------------------

.. code-block:: python

   count_frequencies(table,
                     group_by_columns,
                     value_columns,
                     limit=None,
                     description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**group_by_columns** (str or list of str, required)
   A single column or a list of columns that define each category in the table. A single row is produced for each unique value. Columns must contain string values.

**limit** (int, optional)
   The maximum number of values to count in ``value_columns``. Only the most frequently occurring values will appear in the resulting table, capped at approximately this number.

**table** (cr.datatable.DataTable, required)
   The table in which the data to be analyzed is located.

**value_columns** (str or list of str, required)
   A single column or a list of columns that contain the values to be counted. A new column is created for each unique value.

Returns
--------------------------------------------------
A cr.datatable.DataTable with a row for each category in ``group_by_columns`` and a column for each unique value in ``value_columns``. This table shows how frequently each value for ``value_columns`` appears in each category defined by ``group_by_columns``.

Example
--------------------------------------------------

.. code-block:: python

   sales_data = risp.load_crtable(path='home/user/dir/')
   count = risp.count_frequencies(data,
                                  group_by_columns='foo',
                                  value_columns='bar')
   risp.get_example_rows(count)



.. _api-count-rows:

count_rows()
==================================================
.. include:: ../../includes_api/function_count_rows.rst

Usage
--------------------------------------------------

.. code-block:: python

   count_rows(table)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table for which rows are counte.

Returns
--------------------------------------------------
The row count for a table.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.load_crtable(path='mytable/')
   risp.count_rows(data)



.. _api-create-context-custom-op:

create_context_custom_op()
==================================================
The :ref:`api-create-context-custom-op` function creates a context operation that aggregates data within a specified interval, as defined by a custom function.

.. note:: Simple predefined aggregate operations are available from the :ref:`api-create-context-op` function.

Usage
--------------------------------------------------

.. code-block:: python

   create_context_custom_op(new_columns,
                            input_columns,
                            udf,
                            start_offset,
                            end_offset,
                            unit,
                            include_start=True,
                            include_end=False,
                            max_window=None,
                            **kwargs)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**end_offset** (int, required)
   Relative to the current row's timestamp, the point at which the aggregation interval ends. Negative values move back in time; positive values move forward.

**include_end** (bool, optional)
   If ``True``, include the exact match of the end offset in the interval. If ``False``, do not include exact matches. Default value: ``False``.

**include_start** (bool, optional)
   If ``True``, include the exact match of the start offset in the interval. If ``False``, do not include exact matches. Default value: ``True``.

**input_columns** (str, tuple of str, or list of str, required)
   A list of column names to aggregate.

**max_window** (int, optional)
   If ``unit`` is set to ``rows``, the number of seconds in either direction of the current time that defines the window of time within which rows must occur to be included in the interval.

**new_columns** (str, tuple of str, or list of str, required)
   A list of column names to add to a table.

**start_offset** (int, required)
   Relative to the current row's timestamp, the point at which the aggregation interval begins. Negative values move back in time; positive values move forward.

**udf** (function, required)
   The name of a custom function that defines how to aggregate the data for the columns specified by ``input_columns``. If ``input_columns`` has one column, then the custom function should accept a list of values as input. If ``input_columns`` has more than one column, then the custom function should accept a list of lists where each list's length equals the number of columns in ``input_columns``. The number of items the custom function returns (either as a single item or multiple items in a tuple) must be equal to the number of columns in ``new_columns``.

**unit** (str, required)
   The unit by which an offset is measured. Possible values: ``seconds``, ``minutes``, ``hours``, ``days`` (measured in 24-hour increments from the current row), and ``rows``.

Returns
--------------------------------------------------
A tuple that defines the context operation to perform.

.. note:: Pass the results of this function to the :ref:`api-add-context` and :ref:`api-add-context-from` functions.

Example
--------------------------------------------------

.. code-block:: python

   spread_fn = lambda l : max(l) - min(l) if l else None
   bid_op = risp.create_context_custom_op(new_columns='bid_mean',
                                          input_columns='bid',
                                          udf=spread_fn,
                                          start_offset=-5,
                                          end_offset=5,
                                          unit='seconds')




.. _api-create-context-op:

create_context_op()
==================================================
The :ref:`api-create-context-op` function creates a context operation that aggregates data within a specified interval.

.. note:: Use the :ref:`api-create-context-custom-op` function to define a custom aggregation.

Usage
--------------------------------------------------

.. code-block:: python

   create_context_op(new_column,
                     input_column,
                     aggregate_op,
                     start_offset,
                     end_offset,
                     unit,
                     include_start=True,
                     include_end=False,
                     max_window=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**aggregate_op** (str, required)
   The operation to carry out on the aggregated data in that column. The following operations are available:

   .. list-table::
      :widths: 150 350
      :header-rows: 1

      * - Function
        - Description
      * - ``count``
        - The total number of values in the interval, not including missing values.
      * - ``diff``
        - The difference between the last value in the interval and the first value in the interval.
      * - ``div``
        - The last value in the interval divided by the first value in the interval.
      * - ``empty``
        - The number of missing values in the interval. ``None`` values are ignored, except when set to ``empty``.
      * - ``first``
        - The first value in the interval.
      * - ``last``
        - The last value in the interval.
      * - ``list``
        - A comma-separated list of unique values in the interval, as a string.
      * - ``max``
        - The maximum value in the interval.
      * - ``mean``
        - The arithmetic mean of the values in the interval.
      * - ``median``
        - The 50th percentile value in the interval. This may be an approximation if the number of values in the interval is large.
      * - ``min``
        - The minimum value in the interval.
      * - ``pct_chg``
        - The percent change from the first value in the interval to the last value in the interval.
      * - ``sem``
        - The standard error of the mean in the interval.
      * - ``stddev``
        - The standard deviation with n-1 degrees of freedom of the values in the interval.
      * - ``sum``
        - The sum of the values in the interval.

**end_offset** (int, required)
   Relative to the current row's timestamp, the point at which the aggregation interval ends. Negative values move back in time; positive values move forward.

**include_end** (bool, optional)
   If ``True``, include the exact match of the end offset in the interval. If ``False``, do not include exact matches. Default value: ``False``.

**include_start** (bool, optional)
   If ``True``, include the exact match of the start offset in the interval. If ``False``, do not include exact matches. Default value: ``True``.

**input_columns** (str, tuple of str, or list of str, required)
   A list of column names to aggregate.

**max_window** (int, optional)
   If ``unit`` is set to ``rows``, the number of seconds in either direction of the current time that defines the window of time within which rows must occur to be included in the interval.

**new_columns** (str, tuple of str, or list of str, required)
   A list of column names to add to a table.

**start_offset** (int, required)
   Relative to the current row's timestamp, the point at which the aggregation interval begins. Negative values move back in time; positive values move forward.

**unit** (str, required)
   The unit by which an offset is measured. Possible values: ``seconds``, ``minutes``, ``hours``, ``days`` (measured in 24-hour increments from the current row), and ``rows``.

Returns
--------------------------------------------------
A tuple that defines the context operation to perform. The results of this context operation may then be passed to the :ref:`api-add-context` or :ref:`api-add-context-from` functions.

Example
--------------------------------------------------

.. code-block:: python

   bid_op = risp.create_context_op(new_column='bid_mean',
                                   input_column='bid',
                                   aggregate_op='mean',
                                   start_offset=-5,
                                   end_offset=5,
                                   unit='seconds')
   agg_table = risp.add_context(data,
                                timestamp_column='timestamp',
                                context_ops=bid_op)



.. _api-create-custom-op:

create_custom_op()
==================================================
The :ref:`api-create-custom-op` function creates an operation that processes data according to a custom function. May be used with other aggregation functions, such as :ref:`api-aggregate`.

Usage
--------------------------------------------------

.. code-block:: python

   create_custom_op(new_columns,
                    input_columns,
                    udf)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**input_columns** (str, tuple of str, or list of str, required)
   A list of column names to aggregate.

**new_columns** (str, tuple of str, or list of str, required)
   A list of column names to add to a table.

**udf** (function, required)
   The name of a custom function that defines how to process the data. If ``input_columns`` has one column, then the custom function should accept a list of values as input. If ``input_columns`` has more than one column, then the custom function should accept a list of lists where each list's length equals the number of columns in ``input_columns``. The number of items the custom function returns (either as a single item or multiple items in a tuple) must be equal to the number of columns in ``new_columns``.

Returns
--------------------------------------------------
An object that defines the operation to perform. The results of this context operation may then be passed to the :ref:`api-aggregate` function.

Example
--------------------------------------------------

.. code-block:: python

   spread_fn = lambda b : max(b) - min(b) if b else None
   bid_op = risp.create_custom_op(new_columns='bid_spread',
                                  input_columns='bid',
                                  udf=spread_fn)

   per_cust = risp.aggregate(data,
                             group_by_columns=['customer_id'],
                             aggregate_ops=[('trx_amount', 'mean'),
                                            ('trx_amount', 'sum'),
                                             bid_op])






.. _api-create-parser-hint-date:

create_parser_hint_date()
==================================================
The :ref:`api-create-parser-hint-date` function creates a hint for parsing datetime values in a column when loading or importing a table.

Usage
--------------------------------------------------

.. code-block:: python

   create_parser_hint_date(column,
                           date_format)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str, required)
   The column that contains the datetime values to which this hint applies. If this value is "*", the hint is applied to all columns.

**date_format** (str, required)
   A string, in :ref:`strptime syntax <python-strptime-syntax>`, that identifies the format to use for date and time. This string is composed of several identifiers that denote the various fields of the date and time, along with any separators used for delimiting date and time fields.

Returns
--------------------------------------------------
A hint that may be passed to :ref:`api-load-tabular`, :ref:`api-load-crtable`, or ``cr.data_ingest_v1.load_text()``.

.. TODO: figure out that load_text() function, deprecate or ADD it to the doc.

Example
--------------------------------------------------

.. code-block:: python

   hint1 = risp.create_parser_hint_date(column='first_date',
                                        date_format='%Y-%m-%d %H:%M:%S')
   hint2 = risp.create_parser_hint_date(column='second_date',
                                        date_format='%Y-%m-%d %H:%M:%S')
   table = risp.load_crtable(path='myproject/',
                             hints=[hint1, hint2])



.. _api-create-parser-hint-maxerr:

create_parser_hint_maxerr()
==================================================
The :ref:`api-create-parser-hint-maxerr` function creates a hint that sets the maximum number of cell parser errors that may occur in a column (as a table is loaded) before an exception is raised.

.. note:: To create a hint that sets the maximum percentage of line parser errors allowed for an entire table, use the :ref:`api-create-parser-hint-table-maxerr` function.

Usage
--------------------------------------------------

.. code-block:: python

   create_parser_hint_maxerr(column,
                             max_errors=int)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str, required)
   The column to which this hint applies.

**max_errors** (int, required)
   The maximum number of cell parser errors for a column. When this value is exceeded an exception is raised. This value must be greater than ``0`` or an exception is raised. Missing values do not count as errors, but malformed values do. For example, a string value in a numeric column. Malformed rows count as errors for all columns in that row.

Returns
--------------------------------------------------
A hint that may be passed to :ref:`api-load-tabular`, :ref:`api-load-crtable`, or ``cr.data_ingest_v1.load_text()``.

.. TODO: figure out that load_text() function, deprecate or ADD it to the doc.

Example
--------------------------------------------------

.. code-block:: python

   hint3 = risp.create_parser_hint_maxerr(column='my_date',
                                          max_errors=500)
   table = risp.load_crtable(path='myproject/',
                             hints=hint3)




.. _api-create-parser-hint-missing:

create_parser_hint_missing()
==================================================
.. include:: ../../includes_api/function_create_parser_hint_missing.rst

Usage
--------------------------------------------------

.. code-block:: python

   create_parser_hint_missing(column,
                              additional_markers)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str, required)
   The column to which this hint applies.

**additional_markers** (str or list of str, required)
   A string (or list of strings) to be treated as markers for missing values in the column. Values are specified as strings, even if the value to be replaced is numeric. This value must specifiy one (or more) markers or an exception is raised.

Returns
--------------------------------------------------
A hint that may be passed to the following |versive| platform functions:

* :ref:`api-load-tabular`
* :ref:`api-load-crtable`

.. 
.. cannot be part of the security engine API as there is no valid link to the data ingest API page
.. 
.. * :ref:`api-data-ingest-load-text`
.. 

Example
--------------------------------------------------
The following examples show how to use parser hints to find missing values and labels:

**Find missing values**

.. include:: ../../includes_api/function_create_parser_hint_missing_example_value.rst

**Find missing labels**

.. include:: ../../includes_api/function_create_parser_hint_missing_example_label.rst

**Find multiple hints**

The following example shows how to use multiple hints:

.. code-block:: python

   hint0 = risp.create_parser_hint_missing(column='mynumber',
                                           additional_markers=['0'])
   hintunknown = risp.create_parser_hint_missing(column='mystring',
                                                 additional_markers=['UNKNOWN'])
   table = risp.load_crtable(path='myproject/',
                             hints=[hint0, hintunknown])


.. _api-create-parser-hint-table-maxerr:

create_parser_hint_table_maxerr()
==================================================
The :ref:`api-create-parser-hint-table-maxerr` function creates a hint that sets the maximum percentage of line parser errors that may occur in a table (as that table is loaded) before an exception is raised. This function validates only that each line contains the correct number of items. It does not validate that items are of the correct type.

.. note:: To create a hint to set the maximum number of cell parser errors allowed per column, use the :ref:`api-create-parser-hint-maxerr` function.

.. warning:: Only one hint of this type may be specified when loading a table.

Usage
--------------------------------------------------

.. code-block:: python

   create_parser_hint_table_maxerr(max_percent)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**max_percent** (float, required)
   The maximum percentage of line parser errors for a table. When this value is exceeded an exception is raised. This value must be greater than ``0`` and less than ``100`` or an exception is raised.


Returns
--------------------------------------------------
A hint that may be passed to :ref:`api-load-tabular`, :ref:`api-load-crtable`, or ``cr.data_ingest_v1.load_text()``.

.. TODO: figure out that load_text() function, deprecate or ADD it to the doc.

Example
--------------------------------------------------

.. code-block:: python

   hint3 = risp.create_parser_hint_table_maxerr(5.0)
   table = risp.load_crtable(path='myproject/',
                             hints=hint3)




.. _api-customize-explanations:

customize_explanations()
==================================================
The :ref:`api-customize-explanations` function customizes the settings for prediction explanations, which returns reasons for or against each prediction the model makes.

.. warning:: This function completey overrides all prior settings. For example, to ignore some inputs and also set nicknames, both arguments must be specified.

Usage
--------------------------------------------------

.. code-block:: python

   customize_explanations(model,
                          num_reasons=3,
                          skip_inputs=None,
                          input_nicknames=None,
                          show_counter_arguments=False,
                          show_strength=None,
                          binary_threshold=0,
                          **kwargs)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**binary_threshold** (float, optional)
   For binary classification models, a value between 0 and 1 that defines the threshold for predictions to be treated as class 1 or class 0. Reasons are usually given that support the predicted class.

**input_nicknames** (dict, optional)
   Dictionary that defines how to display inputs in prediction explanations. The keys are actual input names, and the values are the replacement text to use in the reasons. For example, the input "ticker_eq_ABC_last3" can be replaced with "the customer bought ABC stock within the last 3 days" in the reasons. Inputs without nicknames are shown with their actual names. By default, no nicknames are used.

**model** (cr.science.Model, required)
   The model for which explanations are customized.

**num_reasons** (int, optional)
   The number of reasons to return in explanations. Default value: ``3``. Set to ``-1`` to return all available reasons.

**show_counter_arguments** (bool, optional)
   If ``True``, explanations also include the strongest reasons against the prediction. Default value: ``False``.

**show_strength** (bool, optional)
   If ``True``, explanations include the relative strength of inputs in determining the prediction. This is primarily useful for troubleshooting. Default value: ``False``.

**skip_inputs** (str or list of str, optional)
   Single input or list of inputs for which related reasons should never be displayed.

Returns
--------------------------------------------------
None.

Examples
--------------------------------------------------

Customize model to show five reasons and use two nicknames:

.. code-block:: python

   input_nicknames = {'X1': 'spread yesterday',
                      'X2': 'price at close'}
   risp.customize_explanations(model,
                               num_reasons=5,
                               input_nicknames=input_nicknames)

Clear custom settings:

.. code-block:: python

   risp.customize_explanations(model)




.. _api-delete-columns:

delete_columns()
==================================================
The :ref:`api-delete-columns` function deletes one (or more) columns from a table.

Usage
--------------------------------------------------

.. code-block:: python

   delete_columns(table,
                  columns,
                  description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**columns** (str or list of str, required)
   The column (or columns) to be removed from the table.

**description** (str, optional)
   A string to be added to log output.

**table** (cr.datatable.DataTable, required)
   The table that contains the columns to be deleted.

Returns
--------------------------------------------------
A table that does not contain the specified columns.

Example
--------------------------------------------------

.. TODO: The following example isn't a monolith of code. It's three code blocks and two returns and it should be presented in more of a narrative. For now, it's just a monolith of code.

.. code-block:: python

   data = risp.load_crtable(path='home/testdata02/')
   risp.get_column_names(data)
   
   ['id', 'teamtwo', 'usetwo', 'efgtwo', 'RTGtwo', 'clocktwo']
   
   new_data = risp.delete_columns(data, columns=['teamtwo',
                                                 'usetwo',
                                                 'clocktwo'])
   risp.get_column_names(new_data)
   
   ['id', 'efgtwo', 'RTGtwo']




.. _api-describe-model:

describe_model()
==================================================
The :ref:`api-describe-model` function lists details about weights and inputs in a model.

Usage
--------------------------------------------------

.. code-block:: python

   describe_model(model,
                  show_zeros=False)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**model** (cr.science.Model, required)
   The model to describe.

**show_zeros** (bool, optional)
   If ``True``, include zero values in the inputs. Default value: ``False``.

Returns
--------------------------------------------------
A LocalDataTable containing the model details, showing how data values are binned across a given interval and how much weight each bin has on the model predictions. Large positive values indicate the bin has a large positive weight and large negative values indicate the bin has a large negative weight. The table contains one row for each bin.

.. note:: This function returns a lot of detailed information that can be difficult to interpret. To get a simpler high-level model summary, use the :ref:`api-summarize-model` function.

Example
--------------------------------------------------

.. code-block:: python

   model = risp.learn_model(target='T',
                            model_type='LINEAR_REGRESSION',
                            train_table=train,
                            tune_table=tune,
                            input_columns=['X1', 'X2', 'X3'])
   risp.describe_model(model)

will print information similar to:

.. code-block:: sql

   -------- --------------------------------- -------------------
    Weight   Feature                           Type
   -------- --------------------------------- -------------------
    59.1     X1 less than -2.2                 quantized numeric
    42.7     X2 between 0 and 100              quantized numeric
    24.5     X2 greater than or equal to 202   quantized numeric
    22.7     X3 frequency less than 5          string frequency
    2.0      X1 between 0 and 3                quantized numeric
    -4.3     X3 frequency between 5 and 10     string frequency
    -31.6    X1 greater than 53                quantized numeric
   -------- --------------------------------- -------------------




.. _api-flexi-categorize:

flexi_categorize()
==================================================
The :ref:`api-flexi-categorize` function allows custom categories to be defined, a category transform that simplifies applying categories to columns, and the ability to choose which columns to pivot, and then choose which category values to apply. (Other category transforms have a fixed set of categories and produce all variations of category and pivot columns.) 

.. TODO: What "other category transforms?"

Usage
--------------------------------------------------

.. code-block:: python

   flexi_categorize(table,
                    category_cols,
                    category_to_pivot,
                    categorize_fn=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**categorize_fn** (fn(category cols) -> key, optional)
   A transform on the category column values that determines ``key`` values that may be used by the ``category_to_pivot`` argument. For example:

   .. code-block:: python

      def _categorize_function(category_column):
        return "_".join([category_column, 'to'])

   .. warning:: The default function is:

      .. code-block:: python

         identity_categorize(*category_values)

      This function converts the values of the category columns into keys for pivoting and returns those values unchanged as a tuple. If a custom function is used, its outputs must be hashable. If its output is a list or a tuple, the parts are concatenated into a string when forming column name prefixes in the pivoted tables. For example:

      .. code-block:: python

         identity_categorize('internal', 'server')

      uses ``internal_server`` for the column name prefixes.

**category_cols** (list of str or tuple of str, required)
   An ordered list of one (or more) columns in the input table, from which categories are determined.

**category_to_pivot** ({key: [str]}, required)
   For each possible category value, a tuple of columns to pivot. The ``key`` is the type returned by the ``categorize_fn`` function.

**table** (cr.datatable.DataTable, required)
   The table to be input, and then transformed.

Returns
--------------------------------------------------
A transformed table that has the pivot columns completed.

Example
--------------------------------------------------
Assume an input table named "input_table" has the following columns:

* ``source_category``
* ``computer_type``
* ``bytes_in``
* ``bytes_out``

and that the outcome of using ``flexi_categorize`` is to create two new columns:

* ``external_server_bytes_in``
* ``internal_client_bytes_out``

The ``flexi_categorize`` block would be similar to:

.. code-block:: python

   output_table = risp.flexi_categorize(input_table,
                                        ('source_category', 'computer_type'),
                                        category_to_pivot = {
                                        ('external', 'server'): ('bytes_in', ),
                                        ('internal', 'client'): ('bytes_out', ),
                                        },)

and is output to ``output_table``. For every row in the output table:

* If the ``source_category`` value is ``external`` and ``computer_type`` is ``server``, the value of ``external_server_bytes_in`` is the value of the ``bytes_in`` column from the input table.
* If the ``source_category`` value is ``internal`` and ``computer_type`` is ``client``, the value of ``internal_client_bytes_out`` is the value of the ``bytes_out`` column from the input table.




.. _api-generate-rows:

generate_rows()
==================================================
The :ref:`api-generate-rows` function adds to or removes from one (or more) rows in a table using a custom function that operates on the row values in existing columns. For example, take a row that contains multiple delimited values in a column, and then split it into multiple rows with each row containing a single value in a new column.

.. note:: Use the :ref:`api-join` function to add columns from another table. Use the :ref:`api-replace-columns` function to add columns and remove the input columns.

Usage
--------------------------------------------------

.. code-block:: python

   generate_rows(table,
                 input_columns,
                 new_columns,
                 udf,
                 description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**input_columns** (str or list of str, required)
   One (or more) columns to use as inputs to the custom function.

**new_columns** (str or list of str, required)
   One (or more) column names to add to the table.

.. TODO: Investigate this: we can't link it for now: cr.datatable.ColumnMetadata.create(). The data type can be ``ColumnMetadata`` or ``list of ColumnMetadata``, in addition to the str/list of str. Could this also say "If a list of column metadata is already known, one (or more) column metadata entries to add to the table." See also other functions with similar. And see this caveat specific to the appearance in this function: "Currently, only name and type properties of column metadata are propagated to the new DataTable."

**udf** (function, required)
   The custom function that generates the column data. The number of values the custom function takes as inputs must equal the number of columns in ``input_columns``. The number of items in each value of the iterator that the custom function returns (either as a single item or multiple items in a tuple) must be equal to the number of columns in ``new_columns``. If the custom function returns ``None``, the row is removed from the table.

**table** (cr.datatable.DataTable, required)
   The table that contains the columns to which the custom function is applied, and also to which the new rows with new columns will be added.

Returns
--------------------------------------------------
A cr.datatable.DataTable with the new columns added and, depending on the values returned by the custom function, rows removed or added.

Example
--------------------------------------------------

.. code-block:: python

   def risp.split_row(row):
     return row.split(',')
   new_table = risp.generate_rows(old_table,
                                  input_columns='locations',
                                  new_columns='location',
                                  udf=split_row)



.. _api-get-column-descriptions:

get_column_descriptions()
==================================================
The :ref:`api-get-column-descriptions` function gets the columns and the data type of each column of a table.

Usage
--------------------------------------------------

.. code-block:: python

   get_column_descriptions(table)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table from which a schema is retrieved.

Returns
--------------------------------------------------
A table that contains column indices, names, and data types.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.load_crtable(path='home/user/data')
   risp.get_column_descriptions(data)

returns a table similar to:

.. code-block:: sql

   ------- ------ ------
    Index   Name   Type
   ------- ------ ------
    0       foo    str
    1       bar    int
   ------- ------ ------



.. _api-get-column-names:

get_column_names()
==================================================
.. include:: ../../includes_api/function_get_column_names.rst

.. note:: Use :ref:`api-get-column-descriptions` to get more details about each column in the table.

Usage
--------------------------------------------------

.. code-block:: python

   get_column_names(table)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table from which column names will be retrieved.

Returns
--------------------------------------------------
A list of columns in a table.

Example
--------------------------------------------------

.. code-block:: python

   risp.get_column_names(results)

returns a list of columns similar to:

.. code-block:: python

   ['predicted_a', 'a', 'b', 'c', 'd']





.. _api-get-delimiter:

get_delimiter()
==================================================
The :ref:`api-get-delimiter` function gets the delimiter for a table that has been previously loaded. This is useful to verify the delimiter after loading a table in which the delimiter was automatically detected.

Usage
--------------------------------------------------

.. code-block:: python

   get_delimiter(table)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   A table previously returned by :ref:`api-load-tabular` or :ref:`api-load-crtable`.

Returns
--------------------------------------------------
A delimiter string of 1, 2, or 3 characters.

Example
--------------------------------------------------

.. code-block:: python

   triage_table = risp.load_tabular(path='myproject/data.csv',
                                    validate=True, subsample=True)
   risp.get_delimiter(triage_table)
   ','
   full_table = risp.load_tabular(path='myproject/data.csv',
                                  delimiter=',')






.. _api-get-example-rows:

get_example_rows()
==================================================
The :ref:`api-get-example-rows` function retrieves a list of rows from a table.

.. note:: Use :ref:`api-load-tabular` or :ref:`api-load-crtable` with ``subsample=True`` to get a random subsample of rows against which computations may be performed. Use :ref:`api-split-table` to split a table into multiple tables. Use :ref:`api-summarize-table` to view summary statistics for a table.

Usage
--------------------------------------------------

.. code-block:: python

   get_example_rows(table,
                    count=10,
                    sort_by_column=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**count** (int, optional)
   The number of rows to return from the table. Default value: ``10``.

   .. warning:: The maximum number of rows that may be returned is limited only by the memory on the master node and/or the number of rows actually in the table. This function will not return a random sample of rows and the returned table may not have computations performed on it, so keep memory limitations in mind before using this function to retrieve a large number of rows.

**sort_by_column** (str, optional)
   The column from which results are sorted. If specified, results are sorted from highest to lowest based on the value in the column. This is useful to view rows based on a high score or some other attribute.

**table** (cr.datatable.DataTable, required)
   The table from which rows are retrieved.

.. TODO: Is ``count=10`` the actual default?

Returns
--------------------------------------------------
A table with the number of rows specified by ``count``.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.load_crtable(path='home/user/data')
   risp.get_example_rows(data, count=2)

returns a table similar to:

.. code-block:: python

   ------------ ------ --------
    date         name   number
   ------------ ------ --------
    2014-09-01   foo    500.00
    2014-09-13   bar    325.00
   ------------ ------ --------





.. _api-join:

join()
==================================================
The :ref:`api-join` function joins two tables based on key columns. No two non-key columns can have the same name.

.. note:: Use the :ref:`api-relate` function to identify the ``key_columns`` available for joining tables. Use the :ref:`api-merge-tables` function to combine tables in a way that adds rows to matching columns.

Usage
--------------------------------------------------

.. code-block:: python

   join(left_table,
        right_table,
        key_columns,
        join_type='LEFT_OUTER',
        description='',
        secondary_table_is_small=False)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**join_type** (str, optional)
   The type of join to perform. Possible values: ``CROSS``, ``FULL_OUTER``, ``INNER``, ``LEFT_OUTER``, or ``RIGHT_OUTER``. Default value: ``LEFT_OUTER``.

   ``CROSS``: The Cartesian product of rows in each of the two tables; there are no ``key_columns``.

   ``FULL_OUTER``: Items in either table for which there is no counterpart in the other table will have ``None``-filled values.

   ``INNER``: Items in either table for which there is no counterpart in the other table will not be included in the resulting table.

   ``LEFT_OUTER``: Items in the left table for which there is no counterpart in the right table will have ``None``-filled values.

   ``RIGHT_OUTER``: Items in the right table for which there is no counterpart in the left table will have ``None``-filled values.

**key_columns** (list of str or list of tuples, required)
   The columns on which two tables are joined. If the column names are the same in each table, specify the columns as a list of strings. If the column names are different, specify the columns as a list of two-item tuples that contain the column names in the left and right tables, respectively.

**left_table** (cr.datatable.DataTable, required)
   The first table to be joined.

**right_table** (cr.datatable.DataTable, required)
   The second table to be joined.

**secondary_table_is_small** (bool, optional)
   If ``True``, enable optimizations for ``LEFT_OUTER`` and ``INNER`` joins. ``RIGHT_OUTER`` joins can be switched in some cases (if the schema allows it), to effectively perform that join as if it is a ``LEFT_OUTER`` join. The secondary (right) table must be significantly smaller than the primary.

.. 
.. commented out, but noted in channels; save for if/when this topic discusses more than just RISP
.. 
..    .. note:: The ``pyspark.sql.functions.broadcast(df)`` function may be used (in some cases) to mark a table as small enough to join in-memory.
.. 

Returns
--------------------------------------------------
A cr.datatable.DataTable.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.load_crtable(path='home/testdata02/')
   moredata = risp.load_crtable(path='home/testdata03/')
   risp.get_column_names(data)
   ['id', 'teamtwo', 'usetwo', 'efgtwo', 'RTGtwo', 'clocktwo']
   risp.get_column_names(moredata)
   ['teamtwo', 'grade']
   joined = risp.join(data, moredata, key_columns=['teamtwo'],
                      join_type='LEFT_OUTER')
   risp.get_column_names(joined)
   ['id', 'teamtwo', 'usetwo', 'efgtwo', 'RTGtwo', 'clocktwo', 'grade']





.. _api-learn-model:

learn_model()
==================================================
The :ref:`api-learn-model` function learns a model from train and tune tables.

.. TODO: INTEGRATE THIS:
.. 
.. Notes:
.. If you are working on a large data set with many inputs, you should use this function on a multi-node cluster.
.. 
.. If personally identifiable information (PII) is included in the strings of the target column, that information will be included in the resulting model.
.. 
.. See also:
.. Use cr.risp_v1.split_table() to create the train and tune tables for learning a new model.



Usage
--------------------------------------------------

.. code-block:: python

   learn_model(target,
               model_type,
               train_table,
               tune_table,
               input_columns=None,
               description='',
               exploration='BASIC',
               **kwargs)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**exploration** (str, optional)
   .. include:: ../../includes_terms/term_automated_exploration.rst

   .. include:: ../../includes_terms/term_automated_exploration_basic.rst

   .. include:: ../../includes_terms/term_automated_exploration_interactions.rst

   Set to ``BASIC`` for basic mode, set to ``INTERACTIONS`` for interactions mode, and to ``NONE`` to use all columns in the train table as features.

**input_columns** (list of str, optional)
   A list of columns to use as inputs for learning a model inputs. If not specified, all columns that are not the target are used as inputs.

**model_type** (str, required)
   The output type for the model. Available values are: ``BINARY_CLASSIFICATION``, ``MULTI_CLASSIFICATION``, or ``LINEAR_REGRESSION``.

   .. include:: ../../includes_terms/term_model_type_binary_classification.rst

   .. include:: ../../includes_terms/term_model_type_linear_regression.rst

   .. include:: ../../includes_terms/term_model_type_multiclass_classification.rst

**target** (str, required)
   The column containing the value to be predicted.

**train_table** (cr.datatable.DataTable, required)
   .. include:: ../../includes_terms/term_table_train.rst

**tune_table** (cr.datatable.DataTable, required)
   .. include:: ../../includes_terms/term_table_tune.rst

Returns
--------------------------------------------------
A cr.science.Model.

Example
--------------------------------------------------

.. code-block:: python

   train, tune, test = risp.split_table(data,
                                   proportions=[0.6, 0.2, 0.2])
   model = risp.learn_model(target='Price',
                            model_type='LINEAR_REGRESSION',
                            train_table=train,
                            tune_table=tune,
                            input_columns=['a', 'b', 'c'],
                            exploration='INTERACTIONS')

will print information similar to:

.. code-block:: sql

   ------------ ------------------
    Property      Value
   ------------ ------------------
    model type    Numeric
    target        SalePrice
    input         YrRenovated
    input         FpFreestanding
   ------------ ------------------




.. _api-load-crtable:

load_crtable()
==================================================
The :ref:`api-load-crtable` function loads a previously-saved table into a working session, after which additional modification and modeling may be done.

.. note:: Use :ref:`api-load-tabular` to load tabular data. Use :ref:`api-create-parser-hint-date`, :ref:`api-create-parser-hint-maxerr`, :ref:`api-create-parser-hint-missing`, or :ref:`api-create-parser-hint-table-maxerr` to create parsing hints.

Usage
--------------------------------------------------

.. code-block:: python

   load_crtable(path,
                connection=None,
                validate=True,
                subsample=False,
                columns=None,
                hints=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**columns** (str or list of str, optional)
   A column or a list of columns to be loaded. If this argument is not specified, all columns are loaded.

**connection** (str, optional)
   The named connection to the storage cluster. Default value: the connection set by the :ref:`api-initialize-cluster` function.

**hints** (hint or list of hints, optional)
   A hint or a list of hints passed from :ref:`api-create-parser-hint-date`, :ref:`api-create-parser-hint-maxerr`, :ref:`api-create-parser-hint-missing`, or :ref:`api-create-parser-hint-table-maxerr` that aid in parsing the data.

**path** (str, required)
   The path to the directory in which the table contents are located.

**subsample** (bool, optional)
   If ``True``, load only a portion of the table. If ``False``, load the entire table. Data is subsampled randomly, which could lead to misleading results in time-series data.

**validate** (bool, optional)
   If ``True``, validate the table against its schema at load time. If ``False``, postpone schema validation until the table is materialized for subsequent operations.

Returns
--------------------------------------------------
A cr.datatable.DataTable previously saved with :ref:`api-save-crtable`.

Example
--------------------------------------------------

.. code-block:: python

   table = risp.load_crtable(path='myproject/data/',
                             validate=True, subsample=True,
                             connection='hdfs',
                             hints=[hint1, hint2])





.. _api-load-csv:

load_csv()
==================================================
The :ref:`api-load-csv` function is deprecated. Use the :ref:`api-load-tabular` function to load CSV files.




.. _api-load-model:

load_model()
==================================================
The :ref:`api-load-model` function loads a previously saved model for additional modification or to make predictions.

Usage
--------------------------------------------------

.. code-block:: python

   load_model(path,
              connection=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**connection** (str, optional)
   The named connection to the storage cluster. If not specified, will use the default connection set with :ref:`api-initialize-cluster`.

**path** (str, required)
   The path to the model in storage.

Returns
--------------------------------------------------
A cr.science.Model.

Example
--------------------------------------------------

.. code-block:: python

   model = risp.load_model(path='home/test/model',
                           connection='hdfs')




.. _api-load-tabular:

load_tabular()
==================================================
.. include:: ../../includes_api/function_load_tablular.rst



.. note:: Use :ref:`api-load-crtable` to load a previously-saved table. Use :ref:`api-create-parser-hint-date`, :ref:`api-create-parser-hint-maxerr`, :ref:`api-create-parser-hint-missing`, or :ref:`api-create-parser-hint-table-maxerr` to create parsing hints.

.. TODO: What to do with this: "For more advanced data ingestion functions, use the Data Ingest Library (cr.data_ingest_v1).".

Usage
--------------------------------------------------

.. code-block:: python

   load_tabular(path,
                connection=None,
                validate=True,
                subsample=False,
                schema_file=None,
                delimiter=None,
                has_headers=False,
                columns=None,
                hints=None,
                path_filter=None,
                generate_parse_errors=False)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**columns** (str or list of str, optional)
   A column or a list of columns to be loaded. If this argument is not specified, all columns are loaded.

**connection** (str, optional)
   The named connection to the storage cluster. Default value: the connection set by the :ref:`api-initialize-cluster` function.

   .. warning:: If the ``use_single_processor`` argument for the :ref:`api-initialize-cluster` function is set to ``False``, tables cannot be loaded or saved using a local connection because it is not a distributed store. Only CSV or Excel files may be saved to local storage.

**delimiter** (str, optional)
   The string used in data files to separate fields. If a value is not specified, the delimiter is detected automatically.

**generate_parse_errors** (bool, optional)
   If ``True``, parsing errors are generated as part of a returned table.

**has_headers** (bool, optional)
   If ``True``, contents of the first row are treated as column headers.

**hints** (hint or list of hints, optional)
   A hint or a list of hints passed from :ref:`api-create-parser-hint-date`, :ref:`api-create-parser-hint-maxerr`, :ref:`api-create-parser-hint-missing`, or :ref:`api-create-parser-hint-table-maxerr` that aid in parsing the data.

**path** (str, required)
   The path to a file or directory in which the data is located. If this path is to a directory, all files in that directory will be loaded, and then merged into a single table.

**path_filter** (function, optional)
   A custom function that takes a list of file paths, and then returns a filtered list that contains only the file paths to be loaded.

**schema_file** (str, optional)
   The path to the schema file. If a value is not specified, the schema is automatically detected.

.. TODO: schema file?

**subsample** (bool, optional)
   If ``True``, only a portion of a table is loaded. If ``False``, the entire table is loaded.

   .. note:: Data is subsampled randomly, which may lead to misleading results with time-series data.

**validate** (bool, optional)
   If ``True``, validate a table against its schema at load time. If ``False``, schema validation is postponed until after the table is materialized for subsequent operations.

Returns
--------------------------------------------------
A cr.datatable.DataTable with the specified number of columns and details.

Example
--------------------------------------------------

The following example shows how to load and automatically join multiple files from a directory. It uses the ``path_filter`` arguments to load only those files that contain '2017' in the file name.

.. code-block:: python

   def filter (path):
     return [f for f in path if '2017' in f]
     table = risp.load_tabular(path='purchase_records/',
                               validate=True,
                               subsample=True,
                               connection='hdfs',
                               schema_file='purchase_records/schema.txt',
                               path_filter=filter)








.. _api-measure-confusion-matrix:

measure_confusion_matrix()
==================================================
.. include:: ../../includes_api/function_measure_confusion_matrix.rst

Usage
--------------------------------------------------

.. code-block:: python

   measure_confusion_matrix(table,
                            threshold=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.science.PredictionTable, required)
   A table that contains inputs, a column of observed values of the target, and a column of predicted values of the target. Obtained by running the :ref:`api-predict` function on a test table.

**threshold** (float, optional)
   A number between 0 and 1 that determines the threshold for predictions to be classified as either 0 or 1 (binary classification only). For example, if ``threshold=0.6`` and the predicted value is 0.7, it will be classified as 1. Default value: ``0.5``.

Returns
--------------------------------------------------
A LocalDataTable containing columns and rows equal to the number of classes. The columns show the predicted records in each class and the rows show the actual records in each class.

For example, predictions from a binary classification model would return a table with two columns and two rows. Clockwise from the upper left cell this would show the number of true positives, false positives, true negatives, and false negatives.

Example
--------------------------------------------------

.. code-block:: python

   risp.learn_model(target='appetency',
                    model_type='BINARY_CLASSIFICATION',
                    train_table=train,
                    tune_table=tune)
   risp.predict(model,
                table=test,
                return_reasons=True)
   risp.measure_confusion_matrix(results,
                                 threshold=0.5)

will print information similar to:

.. code-block:: python

   Predicted (down), Actual (across)   0        1
   0                                   9821.0   173.0
   1                                   75.0     4.0




.. _api-measure-input-importance:

measure_input_importance()
==================================================
.. include:: ../../includes_api/function_measure_input_importance.rst

Usage
--------------------------------------------------

.. code-block:: python

   measure_input_importance(target,
                            model_type,
                            train_table,
                            tune_table,
                            test_table,
                            input_columns=None,
                            style='one-out',
                            steps=None,
                            metric=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**input_columns** (list of str, optional)
   A list of column names in the data to use as model inputs. If not specified, all columns that are not the target are used as inputs.

**metric** (str, optional)
   The scoring metric used to measure model quality and to help identify the model's learning curve. Available metrics are:

   .. list-table::
      :widths: 80 420
      :header-rows: 1

      * - Metric
        - Description
      * - **AUC**
        - .. include:: ../../includes_terms/term_metric_auc.rst
      * - **F1**
        - .. include:: ../../includes_terms/term_metric_f1.rst
      * - **LOGLOSS**
        - .. include:: ../../includes_terms/term_metric_logloss.rst
      * - **MAE**
        - .. include:: ../../includes_terms/term_metric_mae.rst
      * - **MAPE**
        - .. include:: ../../includes_terms/term_metric_mape.rst
      * - **PREC**
        - .. include:: ../../includes_terms/term_metric_prec.rst
      * - **REC**
        - .. include:: ../../includes_terms/term_metric_rec.rst
      * - **RMSE**
        - .. include:: ../../includes_terms/term_metric_rmse.rst

**model_type** (str, required)
   The output type for the model. Available values are: ``BINARY_CLASSIFICATION``, ``MULTI_CLASSIFICATION``, or ``LINEAR_REGRESSION``.

   .. include:: ../../includes_terms/term_model_type_binary_classification.rst

   .. include:: ../../includes_terms/term_model_type_linear_regression.rst

   .. include:: ../../includes_terms/term_model_type_multiclass_classification.rst

**steps** (int, optional)
   The maximum number of iterations for ``backward`` or ``forward`` style reports. If a value is not specified, a full report that iterates through every input will be generated. This value is ignored for ``one-in`` and ``one-out`` reports.

**style** (str, optional)
   The report style used for analysis. Possible styles: ``backward``, ``forward``, ``one-in``, and ``one-out``.

   .. list-table::
      :widths: 80 420
      :header-rows: 1

      * - Report
        - Description
      * - **backward**
        - .. include:: ../../includes_terms/term_report_backward.rst
      * - **forward**
        - .. include:: ../../includes_terms/term_report_forward.rst
      * - **one-in**
        - .. include:: ../../includes_terms/term_report_one_in.rst
      * - **one-out**
        - .. include:: ../../includes_terms/term_report_one_out.rst

**target** (str, required)
   The column containing the value to be predicted.

**test_table** (cr.datatable.DataTable, required)
   .. include:: ../../includes_terms/term_table_test.rst

**train_table** (cr.datatable.DataTable, required)
   .. include:: ../../includes_terms/term_table_train.rst

**tune_table** (cr.datatable.DataTable, required)
   .. include:: ../../includes_terms/term_table_tune.rst

Returns
--------------------------------------------------
A LocalDataTable containing the metric scores for each model learned.

Example
--------------------------------------------------

First, split the table.

.. code-block:: python

   triple = risp.split_table(table,
                             proportions=[60, 20, 20])
   train, tune, test = triple

Then generate a one-in report.

.. code-block:: python

   report = risp.measure_input_importance(target='Target',
                                          model_type='LINEAR_REGRESSION',
                                          train_table=train,
                                          tune_table=tune,
                                          test_table=test,
                                          style='one-in',
                                          metric='RMSE')

Then print the report:

.. code-block:: python

   print report
   Metric Name                                 RMSE
   Report Style                                one-in
   Best Score                                  159548.0
   Num. Models Tried                           147
   Variable(s): all input variables            159548.0
   Variable(s): include (BldgGrade)            180204.6875






.. _api-measure-learning-curve:

measure_learning_curve()
==================================================
.. include:: ../../includes_api/function_measure_learning_curve.rst

.. warning:: When working on a large data set with many features, it is recommended to run this function on a multi-node cluster.

Usage
--------------------------------------------------

.. code-block:: python

   measure_learning_curve(target,
                          model_type,
                          train_table,
                          tune_table,
                          test_table,
                          input_columns=None,
                          metric=None,
                          max_train_size=150000,
                          key=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**input_columns** (list of str, optional)
   A list of column names to use as model inputs. If a value is not specified, all columns that are not the target are used as inputs.

**key** (str, optional)
   Key to use to checkpoint the model learned using the largest training set considered.

**max_train_size** (int, optional)
   The maximum number of rows to use when learning a model.

**metric** (str, optional)
   The scoring metric used to measure model quality. Available metrics are:

   .. list-table::
      :widths: 80 420
      :header-rows: 1

      * - Metric
        - Description
      * - **AUC**
        - .. include:: ../../includes_terms/term_metric_auc.rst
      * - **F1**
        - .. include:: ../../includes_terms/term_metric_f1.rst
      * - **LOGLOSS**
        - .. include:: ../../includes_terms/term_metric_logloss.rst
      * - **MAE**
        - .. include:: ../../includes_terms/term_metric_mae.rst
      * - **MAPE**
        - .. include:: ../../includes_terms/term_metric_mape.rst
      * - **PREC**
        - .. include:: ../../includes_terms/term_metric_prec.rst
      * - **REC**
        - .. include:: ../../includes_terms/term_metric_rec.rst
      * - **RMSE**
        - .. include:: ../../includes_terms/term_metric_rmse.rst

**model_type** (str, required)
   The output type for the model. Available values are: ``BINARY_CLASSIFICATION``, ``MULTI_CLASSIFICATION``, or ``LINEAR_REGRESSION``.

   .. include:: ../../includes_terms/term_model_type_binary_classification.rst

   .. include:: ../../includes_terms/term_model_type_linear_regression.rst

   .. include:: ../../includes_terms/term_model_type_multiclass_classification.rst

**target** (str, required)
   The column containing the value to be predicted.

**test_table** (cr.datatable.DataTable, required)
   .. include:: ../../includes_terms/term_table_test.rst

**train_table** (cr.datatable.DataTable, required)
   .. include:: ../../includes_terms/term_table_train.rst

**tune_table** (cr.datatable.DataTable, required)
   .. include:: ../../includes_terms/term_table_tune.rst

Returns
--------------------------------------------------
A LocalDataTable that contains the training set size and metric score.

Example
--------------------------------------------------
First split the data:

.. code-block:: python

   train, tune, test = risp.split_table(table,
                                        proportions=[60, 20, 20])

Then measure the learning curve:

.. code-block:: python

   lc = risp.measure_learning_curve(target='a',
                                    model_type='LINEAR_REGRESSION',
                                    train_table=train,
                                    tune_table=tune,
                                    test_table=test,
                                    input_columns=['b', 'c'],
                                    key='lc_model')

Then view the learning curve:

.. code-block:: python

   print lc
   Training set size   Score (RMSE)
   64                  2.0253051927
   128                 1.41870292914
   256                 1.30678385209
   512                 1.28354649346
   1024                1.27024370338
   2048                1.25774672241
   4096                1.24958806604
   8192                1.24639283485
   11912               1.24612843818

Use the model produced by this function.

.. code-block:: python

   model = risp.load_checkpoint('lc_model')
   risp.predict(model, table=new_data)




.. _api-measure-score-curve:

measure_score_curve()
==================================================
.. include:: ../../includes_api/function_measure_score_curves.rst

Usage
--------------------------------------------------

.. code-block:: python

   measure_score_curve(table,
                       curve_type,
                       top_k=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**curve_type** (str, required)
   A curve type used to measure model quality. Available values are:

   ``PREC@K``: Precision at K curve. Plots the proportion of predictions in the ``top_k`` that were true positives.

   ``REC@K``: Recall at K curve. Plots the proportion of true positives that were predicted in the ``top_k``.

   ``ROC``: Receiver operating characteristic curve. Plots the true positive rate against the false positive rate for different thresholds on the predicted probabilities.

**table** (cr.science.PredictionTable, required)
   A table that contains inputs, a column of observed values of the target, and a column of predicted values of the target. This table is obtained by running the :ref:`api-predict` function on a test table.

**top_k** (int, optional)
   The number of top samples to use for ``PREC@K`` and ``REC@K`` calculations. Ignored for the ``ROC`` curve type. The samples are ordered by predicted probability in descending order.

Returns
--------------------------------------------------
A LocalDataTable containing two columns. The columns vary by curve type. Use :ref:`api-plot-line` to view the curves graphically.

Example
--------------------------------------------------

.. code-block:: python

   model = risp.learn_model(target='appetency',
                            model_type='BINARY_CLASSIFICATION',
                            train_table=train,
                            tune_table=tune)
   results = risp.predict(model,
                          table=test,
                          return_reasons=True)
   roc = risp.measure_score_curve(results,
                                  curve_type='ROC')
   risp.plot_line(roc,
                  x_column=0,
                  y_columns=1)




.. _api-merge-tables:

merge_tables()
==================================================
The :ref:`api-merge-tables` function merges two (or more) tables by appending their rows. All columns in the tables to be merged must be present in at least one of the tables. When multiple tables contain identical rows, the resulting table will include a row for each occurance of the row in the original table.

.. note:: Use the :ref:`api-join` function to combine tables in a way that adds new columns rather than new rows.
 
Usage
--------------------------------------------------

.. code-block:: python

   merge_tables(tables,
                description='',
                repartition=True,
                skip_type_check=False)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

.. include:: ../../includes_api/function_merge_tables_repartition.rst

**skip_type_check** (bool, optional)
   If ``True``, skip all validation on column types; use this only when all tables to be merged have identical columns. If ``False``, columns with same names will be checked to have compatible types prior to merging.

**tables** (list of cr.datatable.DataTable, required)
   Two (or more) tables to be merged.

Returns
--------------------------------------------------
A cr.datatable.DataTable that contains all rows merged from the original tables.

Example
--------------------------------------------------

.. code-block:: python

   risp.count_rows(data)
   1000
   risp.count_rows(datatwo)
   1000
   risp.count_rows(datathree)
   1000
   merged_table = risp.merge_tables([data, datatwo, datathree])
   risp.count_rows(merged_table)
   3000






.. 
.. TODO: should the following images be in the doc? Maybe? If yes, then they shouldn't just be shoved at the end of the file, but rather must be integrated into the actual sections to which the individual images are related.
.. 
.. See the charts below for examples of the output from plotting functions.
.. 
.. .. image:: ../images/platform/roc_plot.png
.. 
.. .. image:: ../images/platform/lcplot.png
.. 
.. .. image:: ../images/platform/histo.png
.. 
.. .. image:: ../images/platform/scatter.png
.. 



.. _api-plot-bar:

plot_bar()
==================================================
The :ref:`api-plot-bar` function uses tables produced from the :ref:`api-get-example-rows`, :ref:`api-measure-input-importance`, :ref:`api-score-predictions`, or :ref:`api-measure-learning-curve` functions to create a bar chart with the y-axis columns plotted against the x-axis.

Usage
--------------------------------------------------

.. code-block:: python

   plot_bar(table,
            x_column,
            y_columns,
            title=None,
            x_label=None,
            y_label=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (LocalDataTable, required)
   The table from which the data is to be plotted.

**title** (str, optional)
   The title for the bar chart. Default value: ``None``.

**x_column** (str or int, required)
   The name or index of the column to use on the x-axis.

**x_label** (str, optional)
   The label for the x-axis. If not specified, the value for ``x_column`` is used as the ``x_label``.

**y_columns** (str, int, tuple of str, tuple of int, list of str, or list of int, required)
   The names or indices of the columns to plot against the x-axis.

**y_label** (str or list of str, optional)
   The label for the y-axis. If not specified, the ``y_columns`` names are used as the ``y_label``.

Returns
--------------------------------------------------
An object that holds a reference to the plot. This object may be passed to the :ref:`api-save-plot` function.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.get_example_rows(table)
   bar_plot = risp.plot_bar(data,
                            x_column='Date',
                            y_columns='Name',
                            title='Title')





.. _api-plot-histogram:

plot_histogram()
==================================================
The :ref:`api-plot-histogram` function creates a histogram for the given column to represent the distribution of the data. Plot table data may be sourced from the :ref:`api-get-example-rows`, :ref:`api-measure-input-importance`, :ref:`api-score-predictions`, and :ref:`api-measure-learning-curve` functions.

Usage
--------------------------------------------------

.. code-block:: python

   plot_histogram(table,
                  column,
                  title=None,
                  x_label=None,
                  num_bins=25)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str or int, required)
   The name or index of the column to use.

**num_bins** (int, optional)
   The number of bins against which data is divided. Default value: ``25``.

**table** (LocalDataTable, required)
   The table that contains the data to be plotted.

**title** (str, optional)
   The title for the histogram. Default value: ``None``.

**x_label** (str, optional)
   The label for the x-axis. If not specified, the value of ``column`` is used to label the x-axis.

Returns
--------------------------------------------------
An object that holds a reference to the plot. This object may be passed to the :ref:`api-save-plot` function.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.get_example_rows(table, count=10000)
   histogram = risp.plot_histogram(data,
                                   column='Price',
                                   title='Price Distribution',
                                   num_bins=50)




.. _api-plot-line:

plot_line()
==================================================
The :ref:`api-plot-line` function creates a line chart with the y-axis columns plotted against the x-axis. Plot table data may be sourced from the :ref:`api-get-example-rows`, :ref:`api-measure-input-importance`, :ref:`api-score-predictions`, and :ref:`api-measure-learning-curve` functions.

Usage
--------------------------------------------------

.. code-block:: python

   plot_line(table,
             x_column,
             y_columns,
             title=None,
             x_label=None,
             y_label=None,
             x_range=None,
             y_range=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (LocalDataTable, required)
   The table that contains the data against which the line chart is plotted.

**title** (str, optional)
   The title for the chart. Default value: ``None``.

**x_column** (str or int, required)
   The name or index of the column to use on the x-axis.

**x_label** (str, optional)
   The label for the x-axis. If not specified, the value of ``x_column`` is used.

**x_range** (tuple, optional)
   A 2-tuple that defines the minimum and maximum values to display on the x-axis. Default value: ``None``.

**y_columns** (str, int, tuple of str, tuple of int, list of str, or list of int, required)
   The names or indices of the columns to plot against the x-axis.

**y_label** (str, optional)
   The label for the x-axis. If not specified, the values of ``y_columns`` are used.

**y_range** (tuple, optional)
   A 2-tuple that defines the minimum and maximum values to display on the y-axis. Default value: ``None``.

Returns
--------------------------------------------------
An object that holds a reference to the plot. This object may be passed to the :ref:`api-save-plot` function.

Example
--------------------------------------------------

.. code-block:: python

   model = risp.learn_model(target='appetency',
                            model_type='BINARY_CLASSIFICATION',
                            train_table=train,
                            tune_table=tune)
   results = risp.predict(model,
                          table=test,
                          return_reasons=True)
   roc = risp.measure_score_curve(results,
                                  curve_type='ROC')
   risp.plot_line(roc,
                  x_column='False positive rate',
                  y_columns='True positive rate')





.. _api-plot-scatter:

plot_scatter()
==================================================
The :ref:`api-plot-scatter` function creates a scatter plot with the y-axis columns plotted against the x-axis column. Plot table data may be sourced from the :ref:`api-get-example-rows`, :ref:`api-measure-input-importance`, :ref:`api-score-predictions`, and :ref:`api-measure-learning-curve` functions.

Usage
--------------------------------------------------

.. code-block:: python

   plot_scatter(table,
                x_column,
                y_columns,
                title=None,
                x_label=None,
                y_label=None,
                labels_column=None,
                x_range=None,
                y_range=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**labels_column** (str or int, optional)
   The name or index of the column in which labels for each point in the scatter plot are located.

**table** (LocalDataTable, required)
   The table that contains the data against which the scatter chart is plotted.

**title** (str, optional)
   The title for the chart. Default value: ``None``.

**x_column** (str or int, required)
   The name or index of the column to use on the x-axis.

**x_label** (str, optional)
   The label for the x-axis. If not specified, the value of ``x_column`` is used.

**x_range** (tuple, optional)
   A 2-tuple that defines the minimum and maximum values to display on the x-axis. Default value: ``None``.

**y_columns** (str, int, tuple of str, tuple of int, list of str, or list of int, required)
   The names or indices of the columns to plot against the x-axis.

**y_label** (str, optional)
   The label for the x-axis. If not specified, the values of ``y_columns`` are used.

**y_range** (tuple, optional)
   A 2-tuple that defines the minimum and maximum values to display on the y-axis. Default value: ``None``.

Returns
--------------------------------------------------
An object that holds a reference to the plot. This object may be passed to the :ref:`api-save-plot` function.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.get_example_rows(table,
                                count=10000)
   scatter_plot = risp.plot_scatter(data,
                                    x_column='SqFtTotLiving',
                                    y_columns='SalePrice')






.. _api-predict:

predict()
==================================================
The :ref:`api-predict` function uses a previously learned model on a data table to predict the target. For example, a test table that contains the known values for a target or a table that contains new data in which a target is unknown. In all cases, a data table must contain the same columns that were used as inputs when learning the model.

.. note:: After using this function to obtain predictions from a test table, use the :ref:`api-score-predictions` function to score prediction quality.

Usage
--------------------------------------------------

.. code-block:: python

   predict(model,
           table,
           return_reasons=False)


Arguments
--------------------------------------------------
The following arguments are available to this function:

**model** (cr.science.Model, required)
   The model to use.

**return_reasons** (bool, required)
   If ``True``, include columns that describe the reasons for the predictions. Default value: ``False``.

**table** (cr.science.PredictionTable, optional)
  The data the model will use to obtain predictions.

Returns
--------------------------------------------------
A cr.science.PredictionTable that includes columns for all inputs, a column for predicted values, and, optionally, columns containing reason codes.

Example
--------------------------------------------------

First, learn a model:

.. code-block:: python

   model = risp.learn_model(target='a',
                            model_type='LINEAR_REGRESSION',
                            train_table=train,
                            tune_table=tune,
                            input_columns=['b', 'c', 'd'])

Then, use the model to obtain predictions from a table with the same input columns as those used to train and tune the model. The following example uses a test table in which the true values of the prediction target are known.

.. code-block:: python

   results = risp.predict(model,
                          table=test,
                          return_reasons=True)
   column_names(results)
   ['predicted_a', 'a', 'b', 'c', 'd', 'Reason_Code_1']
   risp.save_csv(results,
                 path='example/predictions.csv')





.. _api-relate:

relate()
==================================================
The :ref:`api-relate` function identifies columns in two tables that contain overlapping data by measuring the Jaccard distance for each pair of columns. This can help identify the correct columns to use when joining two tables.

.. note:: Use the :ref:`api-join` function to join two tables according to the columns identified by this function.

Usage
--------------------------------------------------

.. code-block:: python

   relate(left_table,
          right_table,
          left_columns=None,
          right_columns=None,
          description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**left_columns** (list of str, optional)
   A list of column names from ``left_table`` to analyze. If this value is not set, all columns are used.

**left_table** (cr.datatable.DataTable, required)
   The first table to be analyzed.

**right_columns** (list of str, optional)
   A list of column names from ``right_table`` to analyze. If this value is not set, all columns are used.

**right_table** (cr.datatable.DataTable, required)
   The second table to be analyzed.

Returns
--------------------------------------------------
A LocalDataTable describing the relationships between ``left_table`` and ``right_table``. The left coverage and right coverage values range from 0.0 to 1.0 and indicate the percentage of rows whose values have a counterpart in the other column. The coverage rate refers to the number of table cells that match, not the number of unique values that match. For example, if a column pair's left coverage rate is 0.5, then half of its values are matched in the right column.

Example
--------------------------------------------------

In this example, there are two potential join keys returned. Review the data to determine which is the best to use.

.. code-block:: python

   data_A = risp.load_crtable(path='home/user/data_A/')
   data_B = risp.load_crtable(path='home/user/data_B/')
   relate(data_A, data_B)

will print information similar to:

.. code-block:: sql

   ------------- -------------- --------------- ----------------
    Left_Column   Right_Column   Left_Coverage   Right_Coverage
   ------------- -------------- --------------- ----------------
    userid        id             0.95            0.23
    groupid       groupid        0.42            0.99
   ------------- -------------- --------------- ----------------




.. _api-remove-duplicate-rows:

remove_duplicate_rows()
==================================================
The :ref:`api-remove-duplicate-rows` function removes duplicate rows in a table.

Usage
--------------------------------------------------

.. code-block:: python

   remove_duplicate_rows(table,
                         key_columns=None,
                         description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**key_columns** (str, list of str, tuple of str, or list of tuples, required)
   The column or columns used to determine if duplicate rows are present. If more than one row has the same value in these columns, an arbitrary row is selected and returned. The values in all other columns are ignored. If ``key_columns`` is not specified, the values in all columns are used to determine duplicate rows.

**table** (cr.datatable.DataTable, optional)
   The table in which duplicate rows are located.

Returns
--------------------------------------------------
A cr.datatable.DataTable with duplicate rows removed.

Example
--------------------------------------------------

.. code-block:: python

   new_table = risp.remove_duplicate_rows(old_table)
   new_table = risp.remove_duplicate_rows(old_table,
                                          key_columns=['F1', 'F2'])



.. _api-rename-columns:

rename_columns()
==================================================
The :ref:`api-rename-columns` function renames one (or more) columns in a table.

.. note:: The new column names may exist in the list of original column names; however, each table must have unique column names.

Usage
--------------------------------------------------

.. code-block:: python

   rename_columns(table,
                  name_changes,
                  description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**name_changes** (tuple or list of tuples, required)
   One (or more) tuples that contain the column's existing name, and then its intended new name.

**table** (cr.datatable.DataTable, optional)
   The table in which a column will be renamed.

Returns
--------------------------------------------------
A cr.datatable.DataTable that contains the same data with new column names.

Example
--------------------------------------------------

.. code-block:: python

   risp.get_column_names(data)
   ['a', 'b', 'c', 'r', 's']
   renamed_data = risp.rename_columns(data,
                                      name_changes=[('r', 'length_r'),
                                      ('s', 'length_s')])
   risp.get_column_names(renamed_data)
   ['a', 'b', 'c', 'length_r', 'length_s']




.. _api-replace-columns:

replace_columns()
==================================================
The :ref:`api-replace-columns` function replaces one (or more) columns in a table by using a custom function that operates on the row values in existing columns. For example, replacing two columns with a single column that contains the maximum value for the row.

.. TODO: finish below
.. 
.. The new column names may exist in the original column names, but each table must have unique column names.
.. See also:
.. To add columns without removing the input columns, use cr.risp_v1.add_columns(). To leave the data unchanged but change column names, use cr.risp_v1.rename_columns().
.. 

Usage
--------------------------------------------------

.. code-block:: python

   replace_columns(table,
                   input_columns,
                   new_columns,
                   udf,
                   description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**input_columns** (str or list of str, required)
   The column or columns to use as input to the custom function. These columns will be removed from the table that is returned.

**new_columns** (str or list of str, required)
   The name or ColumnMetadata of the new column or columns to be added to the table.

.. TODO: `new_columns` says "See cr.datatable.ColumnMetadata.create() for more information."

**table** (cr.datatable.DataTable, required)
   The table in which the columns to be replaced are located.

**udf** (function, required)
   The name of the function that generates the data in the new column or columns. The number of values the custom function takes as inputs must equal the number of columns in ``input_columns``. The number of items the custom function returns (either as a single item or multiple items in a tuple) must be equal to the number of columns in ``new_columns``.

Returns
--------------------------------------------------
A cr.datatable.DataTable that no longer include the specified input columns and includes the new columns containing values returned by the custom function.

Example
--------------------------------------------------

.. code-block:: python

   risp.get_column_names(table1)
   ['a', 'b', 'c', 'd']
   def sum_total(x, y):
     total = x + y
     return total
   table2 = risp.replace_columns(table1,
                                 input_columns=['a', 'b'],
                                 new_columns='total',
                                 udf=sum_total)
   risp.get_column_names(table2)
   ['total', 'b', 'c', 'd']
   def find_min_max(x, y):
     return (min(x, y), max(x, y))
   table3 = risp.replace_columns(table1, input_columns=['a', 'b'],
                                 new_columns=['min', 'max'],
                                 udf=find_min_max)
   risp.get_column_names(table3)
   ['min', 'max', 'c', 'd']



.. _api-save-crtable:

save_crtable()
==================================================
.. include:: ../../includes_api/function_save_crtable.rst

.. note:: Use the :ref:`api-save-csv` function to save a table to CSV format. Use the :ref:`api-checkpoint` function checkpoint a table in memory.

Usage
--------------------------------------------------

.. code-block:: python

   save_crtable(table,
                path,
                connection=None,
                file_format=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**connection** (str, optional)
   The named connection to the storage cluster. Default value: the connection set by the :ref:`api-initialize-cluster` function.

   .. warning:: If the ``use_single_processor`` argument for the :ref:`api-initialize-cluster` function is set to ``False``, tables cannot be loaded or saved using a local connection because it is not a distributed store. Only CSV or Excel files may be saved to local storage.

**file_format** (str, optional)
   Identifies the file format. Available file formats will vary, depending on the compute version selected by the :ref:`api-initialize-cluster` function. If not specified, the table is saved in a delimited text format.

**path** (str, required)
   The path to the directory where the table contents will be saved.

**table** (cr.datatable.DataTable, required)
   The table to save.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   risp.save_crtable(my_table,
                     path='mytable/',
                     connection='hdfs')




.. _api-save-csv:

save_csv()
==================================================
.. include:: ../../includes_api/function_save_csv.rst

.. warning:: The contents of the data table must be smaller than the amount of available memory on the primary node on the compute cluster for this function to process successfully.

Usage
--------------------------------------------------

.. code-block:: python

   save_csv(table,
            path,
            connection=None,
            delimiter=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**connection** (str, optional)
   The named connection to the storage cluster. Default value: the connection set by the :ref:`api-initialize-cluster` function.

   .. warning:: If the ``use_single_processor`` argument for the :ref:`api-initialize-cluster` function is set to ``False``, tables cannot be loaded or saved using a local connection because it is not a distributed store. Only CSV or Excel files may be saved to local storage.

**delimiter** (str, optional)
   The string to be used in the exported file to separate fields. If not specified, a comma is used.

**path** (str, required)
   The path to the location in which the file is saved, including file name ending in .csv.

**table** (cr.datatable.DataTable, required)
   The table to save.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   risp.save_csv(table,
                 path='myproject/data_export.csv',
                 connection='hdfs',
                 delimiter=';')

The content in ``data_export.csv`` will be similar to:

.. code-block:: python

   col_name1;col_name2;col_name3 val1;val2;val3 val4;val5;val6

.. the original code block and example used three at symbols, which threw a Sphinx warning so changed to a semi-colon.





.. _api-save-model:

save_model()
==================================================
.. include:: ../../includes_api/function_save_model.rst

.. warning:: When the ``use_single_processor`` argument for the :ref:`api-initialize-cluster` function is set to ``False``, tables cannot be loaded or saved using a local connection because it is not a distributed store. Only :ref:`comma-separated value (CSV) files <api-save-csv>` or :ref:`Microsoft Excel files <api-save-xls>` may be saved to local storage.

.. note:: Use the :ref:`api-checkpoint` function to save a copy of the model in-memory.

Usage
--------------------------------------------------

.. code-block:: python

   save_model(model,
              path,
              connection=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**connection** (str, optional)
   The named connection to the storage cluster. Default value: the connection set by the :ref:`api-initialize-cluster` function.

**model** (cr.science.Model, required)
   The model to save.

**path** (str, required)
   The path to the location in which the model is saved, including the model name.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   risp.save_model(model,
                   path='username/savedmodels/model1',
                   connection='hdfs')





.. _api-save-plot:

save_plot()
==================================================
The :ref:`api-save-plot` function saves a plot from a working session to a file on storage.

Usage
--------------------------------------------------

.. code-block:: python

   save_plot(plot,
             path,
             connection=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**connection** (str, optional)
   The named connection to the storage cluster. Default value: the connection set by the :ref:`api-initialize-cluster` function.

**path** (str, required)
   The path to the location in which the file is saved, including file name ending in .png.

**plot** (Plot, optional)
   The plot to be saved.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   risp.save_plot(scatter_plot,
                  path='myproject/price_vs_volume.png',
                  connection='hdfs')



.. _api-save-table-schema:

save_table_schema()
==================================================
The :ref:`api-save-table-schema` function saves the schema for a table to a text file. This function is useful for manually adjusting a schema that was automatically detected. For example, automatically detecting a schema from a subsampled table, making manual adjustments, and then loading the full table with the adjusted schema file.

Usage
--------------------------------------------------

.. code-block:: python

   save_table_schema(table,
                     path,
                     connection=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**connection** (str, optional)
   The named connection to the storage cluster. Default value: the connection set by the :ref:`api-initialize-cluster` function.

**path** (str, required)
   The path to the location in which the schema is saved, including the file name ending in .txt.

**table** (cr.datatable.DataTable, optional)
   The table described by the schema.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   triage_table = risp.load_tabular(path='myproject/data.csv',
                                    validate=True, subsample=True,
                                    connection='hdfs')
   risp.save_table_schema(triage_table,
                          path='myproject/schema.txt',
                          connection='hdfs')
   risp.get_delimiter(triage_table)
   ','
   full_table = risp.load_tabular(path='myproject/data.csv',
                                  schema_file='schema.txt',
                                  delimiter=',',
                                  connection='hdfs')




.. _api-save-xls:

save_xls()
==================================================
.. include:: ../../includes_api/function_save_xls.rst

Usage
--------------------------------------------------

.. code-block:: python

   save_xls(table,
            path,
            connection=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**connection** (str, optional)
   The named connection to the storage cluster. Default value: the connection set by the :ref:`api-initialize-cluster` function.

**path** (str, required)
   The path to the location in which the file is saved, including file name ending in .xls.

**table** (cr.datatable.DataTable, required)
   The table to be exported.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   risp.save_xls(table,
                 path='myproject/data_export.xls',
                 connection='hdfs')

.. TODO: VERIFY "The content in data_export.xls will be in Excel 97 format." ... seems a bit ... old?





.. _api-score-predictions:

score_predictions()
==================================================
.. include:: ../../includes_api/function_score_predictions.rst

.. warning:: Before using this function, use the :ref:`api-predict` function to obtain predictions.

Usage
--------------------------------------------------

.. code-block:: python

   score_predictions(table,
                     metrics=None,
                     group_by_columns=None,
                     threshold=None,
                     top_k=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**group_by_columns** (str or list of str, optional)
   A single column or list of columns by which to group results. Metrics are returned for each distinct category.

**metrics** (str or list of str, optional)
   A single scoring metric or a list of scoring metrics that are used to measure model quality. Available scoring metrics:

   .. list-table::
      :widths: 80 420
      :header-rows: 1

      * - Metric
        - Description
      * - **ACC**
        - .. include:: ../../includes_terms/term_metric_acc.rst
      * - **AUC**
        - .. include:: ../../includes_terms/term_metric_auc.rst
      * - **F1**
        - .. include:: ../../includes_terms/term_metric_f1.rst
      * - **LOGLOSS**
        - .. include:: ../../includes_terms/term_metric_logloss.rst
      * - **MAE**
        - .. include:: ../../includes_terms/term_metric_mae.rst
      * - **MAPE**
        - .. include:: ../../includes_terms/term_metric_mape.rst
      * - **PREC**
        - .. include:: ../../includes_terms/term_metric_prec.rst
      * - **PREC@K**
        - .. include:: ../../includes_terms/term_metric_prec_at_k.rst
      * - **REC**
        - .. include:: ../../includes_terms/term_metric_rec.rst
      * - **REC@K**
        - .. include:: ../../includes_terms/term_metric_rec_at_k.rst
      * - **RMSE**
        - .. include:: ../../includes_terms/term_metric_rmse.rst

**table** (cr.science.PredictionTable, required)
   A table that contains inputs, a column that contains observed values for the target, and a column that contains predicted values for the target. This table is created by running the :ref:`api-predict` function on a test table.

**threshold** (float, optional)
   A number between 0 and 1 that determines the threshold for predictions to be classified as either 0 or 1 (binary classification only). For example, if threshold=0.6 and the predicted value is 0.7, it will be classified as 1. Default is 0.5. Used with PREC, REC, and F1 metrics and ignored for all others.

**top_k** (int, optional)
   The number of top samples to use for PREC@K and REC@K calculations (binary classification only). Ignored for all other metrics. The samples are ordered by predicted probability in descending order.

Returns
--------------------------------------------------
A LocalDataTable containing the model score and baseline model score measured using each of the metrics selected and that may be grouped by the values in ``group_by_columns``.

The baseline model predicts the mean of the target column for every row. The mean is computed from the full training data. For MULTI_CLASSIFICATION data, a vector of class frequency probabilities is used for the mean. For many metrics, this baseline shows the expected accuracy from random guessing.

Examples
--------------------------------------------------


**Score predictions for RMSE, MAPE, and MALQ**

Generate the score predictions for RMSE, MALQ, and MAPE:

.. code-block:: python

   results = risp.predict(model,
                          table=test_table)
   results_quality = risp.score_predictions(results,
                                            metrics=['RMSE',
                                                     'MALQ',
                                                     'MAPE'])

and then print the results:

.. code-block:: python

   print results_quality

to view information similar to:

.. code-block:: python

   RMSE    MALQ    MAPE    RMSE (base)    MALQ (base)    MAPE (base)
   1.23    1.04   0.31    1.32           1.09          0.43


**Score predictions for RMSE, grouped by color**

.. code-block:: python

   results_quality = risp.score_predictions(results,
                                            metrics='RMSE',
                                            group_by_columns=['color'])

and then print the results:

.. code-block:: python

   print results_quality

to view information similar to:

.. code-block:: python

   (color)     RMSE    RMSE (base)
   blue        1.33    1.49
   red         1.54    1.67






.. _api-select-columns:

select_columns()
==================================================
The :ref:`api-select-columns` function selects one or more columns, and then uses them in a new table.

Usage
--------------------------------------------------

.. code-block:: python

   select_columns(table,
                  columns,
                  description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**columns** (str or list of str, required)
   One (or more) columns to keep in the new table. Columns in the new table are in the same order as the list of columns specified for this argument.

**description** (str, optional)
   A string to be added to log output.

**table** (cr.datatable.DataTable, required)
   The table from which columns are selected.

Returns
--------------------------------------------------
A cr.datatable.DataTable that contains only the specified columns.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.load_crtable(path='home/testdata02/')
   risp.get_column_names(data)
   ['id', 'teamtwo', 'usetwo', 'efgtwo', 'RTGtwo', 'clocktwo']
   new_data = risp.select_columns(data,
                                  columns=['usetwo',
                                           'teamtwo',
                                           'clocktwo'])
   risp.get_column_names(new_data)
   ['usetwo', 'teamtwo', 'clocktwo']



.. _api-select-rows:

select_rows()
==================================================
The :ref:`api-select-rows` function selects a subset of rows in a table, and then uses a custom function to determine which rows should be retained and which rows should be deleted. For example, rows that have values that meet certain criteria may be retained, and then all other rows deleted.

Usage
--------------------------------------------------

.. code-block:: python

   select_rows(table,
               input_columns,
               udf,
               description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**input_columns** (str or list of str, required)
   One (or more) columns to use as inputs to the custom function.

**table** (cr.datatable.DataTable, required)
   The table from which rows are selected.

**udf** (function, required)
   A custom function that defines which rows to select, and then specifies if the row is retained or deleted. Return ``1`` to retain a row, return ``0`` to delete it.

Returns
--------------------------------------------------
A cr.datatable.DataTable that contains only those rows that meet the criteria defined by the custom function.

Example
--------------------------------------------------
The following example shows a custom function that returns 1 when an even number is input, and then applies that function:

.. code-block:: python

   def evens(v):
     return v % 2 == 0
   risp.count_rows(table)
   10493
   evens = risp.select_rows(table,
                            input_columns=['id'],
                            udf=even_function)
   risp.count_rows(evens)
   5309



.. _api-split-table:

split_table()
==================================================
The :ref:`api-split-table` function splits a table into two (or more) tables.

.. note:: After splitting a table into multiple tables, use :ref:`api-learn-model` to learn a model from the data.

Usage
--------------------------------------------------

.. code-block:: python

   risp.split_table(table,
                    proportions=[int, int, int, ...],
                    sort_by_column=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**proportions** (list of float, list of int, optional)
   A list that defines how much data to include in each table as a proportion of the sum of the values in the list. (The total of this list does not need to be 100.) Default value: ``[60, 20, 20]``.

   .. note:: When splitting data in preparation for learning a model, provide at least three values: one for training the table, one for tuning the table, and one (or more) for testing the table.

**sort_by_column** (str, optional)
   The column for which the original table is split. For example, if splitting by a timestamped column, the first table will contain the earliest records and the last table will contain the last records. When ``None``, data is split randomly. Default value: ``None``.

**table** (cr.datatable.DataTable, required)
   The table to be split.

Returns
--------------------------------------------------
A number of data tables equal to the values specified in ``proportions``.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.load_crtable(path='home/test/')
   data_triple = risp.split_table(data,
                                  proportions=[0.8, 0.4, 0.2],
                                  sort_by_column='ID')
   data_train, data_tune, data_test = data_triple
   data_quads = risp.split_table(data,
                                 proportions=[4, 2, 2, 2])
   data_train, data_tune, data_test, reserve = data_quads






.. _api-summarize-model:

summarize_model()
==================================================
The :ref:`api-summarize-model` function measures the accuracy of a model by using a test set of data as a baseline, iterating over all inputs, replacing data for each input in the test with noise, and then comparing the accuracy of the predictions to the baseline accuracy.

A large decrease in accuracy indicates a very important input (large percentage). A small decrease in accuracy indicates a less important input (small percentage). If replacing an input with noise actually increases the accuracy, this indicates that the input leads to overfitting and should be removed. The relative importance of such inputs is expressed as negative percentage.

A model summary is returned that lists the relative importance of each input, expressed as a percentage, which helps identify the most important inputs when making predictions.

.. note:: When the ``exploration`` argument for the :ref:`api-learn-model` function is set to ``INTERACTIONS`` the ModelSummary may also list the relative importance of the interaction between two inputs.

Usage
--------------------------------------------------

.. code-block:: python

   risp.summarize_model(model,
                        table,
                        display_limit=25)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**display_limit** (int, optional)
   The number of rows to show in the summary. Set to ``None`` to show all inputs. Default value: ``25``.

**model** (cr.science.Model, required)
   The model to summarize.

**table** (cr.datatable.DataTable, required)
   The table in which data for measuring input importance is located. In general, this is not the same data that is used to learn a model; however, in some cases comparing the results from separate runs of training and testing data is desired.

Returns
--------------------------------------------------
A ModelSummary that can be printed.

Example
--------------------------------------------------

.. code-block:: python

   model = risp.learn_model(target='SalePrice',
                            model_type='LINEAR_REGRESSION',
                            train_table=train,
                            tune_table=tune,
                            exploration='INTERACTIONS')
   summary = risp.summarize_model(model,
                                  test_data,
                                  display_limit=3)

will print information similar to:

.. code-block:: python

   print summary
   Number of input columns: 20
   Score (RMSE): 151990.2498
   Baseline (RMSE): 257434.6353
   Relative Importance (%)   Inputs                    Score Without (RMSE)
   45.61                     BldgGrade                 202763.28
   12.83                     YrBuilt                   166269.86
   9.89                      Address                   163005.53
   5.55                      SqFtTotLiving             158164.77
   3.22                      SqFt2ndFloor              155578.81
   2.61                      SqFt1stFloor              154901.35
   2.58                      SqFtTotBasement           154866.38




.. _api-summarize-table:

summarize_table()
==================================================
.. include:: ../../includes_api/function_summarize_table.rst

.. note:: Uncommon values may be combined into a group when calculating statistics. If the output contains ``__FREQUENCY_ELIDED__``, this indicates a group is filled with miscellaneous uncommon values. If a column contains only ``None`` values, the table summary displays ``0`` for ``appx_unique`` and ``N/A`` for other statistics.

Usage
--------------------------------------------------

.. code-block:: python

   risp.summarize_table(table,
                        columns=None,
                        max_unique=250,
                        num_quantiles=20)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**columns** (str or list of str, optional)
   A column or a list of columns to be analyzed. If this argument is not specified, all columns are analyzed.

**max_unique** (int, optional)
   The maximum number of unique values to track before approximating. Default value: ``250``.
   
   .. warning:: Use ``0`` to track all unique values, though it may exceed memory limits.

**num_quantiles** (int, optional)
   The number of quantiles to return. May be an integer between ``1`` and ``1000`` (inclusive). Default value ``20``.

   The size of the array returned is actually this value+1 because it is inclusive of both ends. For example if ``num_quantiles`` is 4 (commonly referred to in English as quartiles), then the array will have 5 elements for the values at 0%, 25%, 50%, 75%, 100%.

**table** (cr.datatable.DataTable, required)
   The table to be analyzed.

Returns
--------------------------------------------------
A LocalDataTable that contains the summary statistics listed below for each column. Columns containing integers return all summary statistics. Columns containing floats return all but ``appx_unique`` and the top 5 values. Columns containing strings return ``count``, ``empty``, ``appx_unique``, and the top 5 values.

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Statistic
     - Description
   * - ``#1-5 values``
     - The five most common values and number of times they each appear.
   * - ``appx_unique``
     - The approximate number of unique values in the column.
   * - ``count``
     - The total number of values, not including missing values.
   * - ``empty``
     - The number of missing values.
   * - ``max``
     - The maximum value.
   * - ``mean``
     - The arithmetic mean of the values.
   * - ``median``
     - The 50th percentile value for the group. This may be an approximation if the number of values in the group is large.
   * - ``min``
     - The minimum value.
   * - ``quantiles``
     - The quantiles of values for the group. This may be an approximation if the number of values in the group is large. The quantiles are in the form of a list of tuples where the first element of the tuple is the quantile, and the second is the value at that quantile.
   * - ``stddev``
     - The standard deviation with n-1 degrees of freedom of the values.
   * - ``sum``
     - The sum of the values.

Example
--------------------------------------------------

.. code-block:: python

   risp.summarize_table(data,
                        num_quantiles=2)

will print summary data similar to:

.. code-block:: python

   statistic       my_float
   count           398
   empty           0
   min             8.0
   max             24.8
   mean            15.5680904523
   median          16.2
   quantiles       [(0.0, 8.0), (0.5, 16.2), (1.0, 24.8)]
   stddev          2.75768892981
   sum             6196.1








.. _api-checkpoint-functions:

Checkpoints Functions
==================================================
.. TODO: add a generic intro for "A checkpoint is ..."

A checkpoint stores an item in-memory so they may be retrieved later.

The following functions manage checkpoints for tables and models that have already been saved.

* :ref:`api-checkpoint`
* :ref:`api-get-checkpoints`
* :ref:`api-load-checkpoint`

.. note:: Use the :ref:`api-save-crtable` function to save a table. Use the :ref:`api-save-model` function to save a model. The ``use_single_processor`` argument in the :ref:`api-initialize-cluster` function controls how tables and models are loaded or saved across sessions.



.. _api-checkpoint:

checkpoint()
--------------------------------------------------
.. include:: ../../includes_api/function_checkpoint.rst

.. note::  Use the :ref:`api-get-checkpoints` function to view all checkpointed items. Use the :ref:`api-load-checkpoint` function to retrieve a checkpointed item.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   checkpoint(item, key)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**item** (cr.datatable.DataTable, required)
   The table or model to checkpoint.

.. TODO: Also the ``cr.science.model`` data type.

**key** (str, optional)
   A key to identify the table or model in memory.

   .. note:: A checkpoint is overwritten when a key already exists for a table or model. Memory limitations may affect the performance of checkpoints. If memory is not available, older checkpointed items may take longer to retrieve. Re-using the same key will use the available memory in the most efficient manner.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   risp.checkpoint(table,
                   key='clean_primary')
   risp.get_checkpoints()

will print information similar to:

.. code-block:: sql

   --------------- ----------- ----------------------------
    Key             Item Type   Create Time
   --------------- ----------- ----------------------------
    clean_primary   table       2015-03-02 18:20:27.333698
   --------------- ----------- ----------------------------



.. _api-get-checkpoints:

get_checkpoints()
--------------------------------------------------
The :ref:`api-get-checkpoints` function gets a list of all of the tables and/or models that are checkpointed in memory.

.. note:: Use :ref:`api-load-checkpoint` to retrieve a checkpointed item.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_checkpoints(item_type=None)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**item_type** (str, required)
   The type of objects to list. Possible values: ``table``, ``model``, or ``None``. If ``None`` is specified, both tables and models are returned.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A LocalDataTable containing the key, type, and creation timestamp for each item.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   data = risp.load_crtable(path='home/testdata02/')
   risp.checkpoint(data,
                   key='data_raw')
   risp.get_checkpoints('table')

will print information similar to:

.. code-block:: sql

   --------- ----------- ----------------------------
    Key       Item Type   Create Time
   --------- ----------- ----------------------------
    data_raw  table       2015-02-25 18:43:16.758198
   --------- ----------- ----------------------------



.. _api-load-checkpoint:

load_checkpoint()
--------------------------------------------------
The :ref:`api-load-checkpoint` function retrieves a table or model checkpointed in memory with the provided key.

.. note:: Use :ref:`api-checkpoint` to create a checkpoint. Use :ref:`api-get-checkpoints` to view all checkpointed items.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_checkpoint(key)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**key** (str, required)
   A key to identify the table or model in memory.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A cr.datatable.DataTable.

.. TODO: originally said "A cr.datatable.DataTable or cr.science.model."

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   risp.checkpoint(table,
                   key='clean_primary')
   risp.get_checkpoints('table')

will print information similar to:

.. code-block:: python

   Key             Item Type   Create Time
   clean_primary   table       2015-03-02 18:20:27.333698

and then clean the table:

.. code-block:: python

   table = risp.load_checkpoint('clean_primary')




Custom Function Logs
==================================================
.. TODO: add a generic intro for "A custom function is ..."

The following functions create and return log statements for custom functions.

* :ref:`api-get-udf-debug-logs`
* :ref:`api-udf-debug-log`


.. _api-get-udf-debug-logs:

get_udf_debug_logs()
--------------------------------------------------
.. include:: ../../includes_api/function_get_udf_debug_logs.rst

.. note:: Use the :ref:`api-udf-debug-log` function to record a log statement from inside a custom function.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_udf_debug_logs(table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The name of the table for which log statements are returned.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A cr.datatable.DataTable that lists the logs including ``timestamp``, ``opaque_id``, and ``statement``. The ``opaque_id`` can be used to group messages sent from the same node.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   def cut_in_half(a):
     if not isinstance(a, int):
       risp.udf_debug_log('Got a non-integer %s' % a)
         return None
     else:
       return half * 0.5
   new_table = risp.add_columns(old_table,
                                input_columns='full',
                                new_columns='half',
                                udf=cut_in_half)
   print risp.get_udf_debug_logs(new_table)




.. _api-udf-debug-log:

udf_debug_log()
--------------------------------------------------
.. include:: ../../includes_api/function_udf_debug_log.rst

.. note:: Use the :ref:`api-get-udf-debug-logs` function to gather all the log statements written by a custom function.


Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   udf_debug_log(statement)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**statement** (str, required)
   The log statement to be recorded.

   .. note:: A log statement may not exceed 200 characters. Log statements that exceed 200 characters are truncated.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   def cut_in_half(a):
     if not isinstance(a, int):
       risp.udf_debug_log('Got a non-integer %s' % a)
         return None
     else:
       return half * 0.5
   new_table = risp.add_columns(old_table,
                                input_columns='full',
                                new_columns='half',
                                udf=cut_in_half)




Platform Functions
==================================================
.. TODO: add a generic intro for "The platform table is ..."

The following functions may be used to interact with the platform, such as getting connections to data repositories, initializing the cluster, and so on.

* :ref:`api-get-connections`
* :ref:`api-get-session-property`
* :ref:`api-get-versions`
* :ref:`api-initialize-cluster`
* :ref:`api-set-session-property`
* :ref:`api-shutdown-cluster`



.. _api-get-connections:

get_connections()
--------------------------------------------------
The :ref:`api-get-connections` function gets a list of available named connections to data repositories.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_connections()

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A LocalDataTable that lists the available connections, including name, description, storage type, and whether it is default.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   risp.get_connections()

will print information similar to:

.. code-block:: sql

   ------- ----------------------- ------- ---------
    Name    Description             Type    Default
   ------- ----------------------- ------- ---------
    local   Storage on local disk   local   False
    hdfs    Storage on HDFS         hdfs    True
   ------- ----------------------- ------- ---------



.. _api-get-session-property:

get_session_property()
--------------------------------------------------
The :ref:`api-get-session-property` function gets the current value of a session property.

.. note:: Use the :ref:`api-set-session-property` to update a property.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_session_property(key)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**key** (str, required)
   The key to identify the property. Possible values: ``risp.Session.DEBUG_EACH_STEP`` or ``risp.Session.DEBUG_TAG``.

   Use ``risp.Session.DEBUG_EACH_STEP`` to determine whether to immediately evaluate a table created by a function call and print example rows.

   Use ``risp.Session.DEBUG_TAG`` to get the description captured in the system for each call.

.. TODO: Are those actually the correct values? Is the ``risp.Session.`` needed? Or is that just because all the other examples seem to assume running Python in a shell? What would this actually look like in the customization package?

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
The value for the key.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   is_debug = risp.get_session_property(risp.Session.DEBUG_EACH_STEP)



.. _api-get-versions:

get_versions()
--------------------------------------------------
The :ref:`api-get-versions` function gets the versions of the installed platform, API, and applications.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_versions()

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A LocalDataTable listing installed components that includes short name, long name, version number, and tags that provide additional information.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   risp.get_versions()

will print information similar to:

.. code-block:: sql

   -------- --------------------------- --------- ------
    Name     Long Name                   Version   Tags
   -------- --------------------------- --------- ------
    crplat   Context Relevant Platform   2.7.8.0
    risp     Context Relevant RISP       1.0
   -------- --------------------------- --------- ------



.. _api-initialize-cluster:

initialize_cluster()
--------------------------------------------------
The :ref:`api-initialize-cluster` function initializes the cluster to prepare compute nodes to process data during a session and to activate default logging. This function must called first. In general, distributed processing on a multi-node cluster is preferred over single core processing and local connections.

.. note:: Use :ref:`api-get-connections` to view all of the existing, available connections.

.. TODO: Where is APPLICATION_CODE_PATH?

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   initialize_cluster(use_single_processor=False,
                      default_connection=None,
                      compute_version=None,
                      cluster_args=None)


Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**APPLICATION_CODE_PATH** (list of str, optional)
   A comma-separated list of directory paths to serialize and send to every node in the cluster. These directories contain any Python code that is not directly imported by an application.

**cluster_args** (dict, optional)
   Additional arguments to be passed to the cluster.

**compute_version** (int, optional)
   The version of the compute infrastructure to use. This setting overrides the system-wide default setting. Possible values: ``COMPUTE_VERSION_SPARK``.

   Use ``COMPUTE_VERSION_SPARK`` for the Spark compute infrastructure. If ``use_single_processor`` is ``True``, this will use a local master. Otherwise, it will connect to a Spark master on the local machine at the default port of 7077.

**default_connection** (str, optional)
   The default connection to the storage cluster to use for load and save operations. If a value is not set here, a connection must be set for each load and save operation.

**use_single_processor** (bool, optional)
   If ``True``, process data on only one core of the master node. When a cluster is initialized, tables and models that are checkpointed to memory may not be available until the next session.

   If ``False``, distribute processing across all nodes in the compute cluster. When a cluster is initializedr, tables and models may not be loaded or saved using a local connection because it is not a distributed store. A CSV or Excel file may be saved to local storage instead.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++

**Initialize an HDFS cluster**

.. code-block:: python

   risp.initialize_cluster(use_single_processor=True,
                           default_connection='hdfs')


**Load CSV data from a location in HDFS**

Open up the ``crpython`` interpreter:

.. code-block:: console

   $ crpython

and then load the data:

.. code-block:: python

   path = 'path/to/location/on/hdfs/for/data/file.csv'  # 'hdfs://' not required
   import cr.risp_v1 as risp

   risp.initialize_cluster()
   t = risp.load_csv(path=path,
                     connection='hdfs_connection_name',
                     has_headers=True)

   risp.count_rows(t)
   risp.get_column_names(t)



.. _api-set-session-property:

set_session_property()
--------------------------------------------------
The :ref:`api-set-session-property` function sets the value of a session property.

.. note:: Use :ref:`api-get-session-property` to get the current value of a property.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   set_session_property(key, value)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**key** (str, required)
   The key to identify the property. Possible values: ``risp.Session.DEBUG_EACH_STEP`` or ``risp.Session.DEBUG_TAG``.

   Use ``risp.Session.DEBUG_EACH_STEP`` to determine whether to immediately evaluate a table created by a function call and print example rows.

   Use ``risp.Session.DEBUG_TAG`` to get the description captured in the system for each call.

**value** (object, optional)
   The value for the ``key``. For a ``risp.Session.DEBUG_EACH_STEP`` key, either ``True`` or ``False``. For a ``risp.Session.DEBUG_TAG`` key, a string.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   risp.set_session_property(risp.Session.DEBUG_EACH_STEP,
                             value=True)




.. _api-shutdown-cluster:

shutdown_cluster()
--------------------------------------------------
The :ref:`api-shutdown-cluster` function shuts down the connection to the compute cluster at the end of a session.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   shutdown_cluster()

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   initialize_cluster()
   data = risp.load_crtable(path='myproject/')
   risp.shutdown_cluster()
