.. 
.. xxxxx
.. 

==================================================
Site Map
==================================================

.. warning:: This is a wholly internal set of docs for Versive, about documentation guidelines and how to build the documentation.


.. Hide the TOC from this file.

.. toctree::
   :hidden:

   brand_guidelines
   docs_builds
   docs_changes
   license
   names
   pdf_builds
   pie_vs_cake
   style_guide
