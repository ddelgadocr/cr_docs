..
.. All content courtesy of KyleFactz.com
..

==================================================
Pie vs. Cake
==================================================

This topic contains links to documentation of the similarities and differences between pie and cake.

.. warning:: Consumption of the following content may cause you to fly into a violent rage or descend into intractable malaise and mutter to yourself incoherently, depending on your Meyers-Briggs personality type.

Origin of the Debate
==================================================

On Pi Day 2017 a debate arose that threatened to tear Versive (formerly Context Relevant) apart. In the throes of a dessert coma the likes of which had never been seen, employees began to contemplate the nature of their existence.

One particular unanswered question came to symbolize the ultimate uncertainty shared by every inhabitant of this shared hallucination we so blithely label "existence."

The question: What is the difference between Pie and Cake?

Is it the shape? Presence or absence of pastry crust? Sweetness vs. Savoriness? Compatibility with birthday candles?

Agreed-Upon Facts
==================================================

* Both pies and cakes may be sweet or savory.
* Both pies and cakes may be round or square.
* Both pies and cakes may have filling.
* Neither pies nor cakes are a superset or subset of the other. They are separate categories.
* Only cakes may have icing.
* Only cakes may be graced with birthday candles.
* Only pies have pastry crusts.
* A pie will always have at least two distinct parts.
* A pie must be cylindrical, at least 3x wider than it is tall, flaring outwards toward the top.

Points of Contention
==================================================

**Point:** Anything with a pastry crust is a pie.

**Counterpoint:** Shepherd's pie and hot pockets both have pastry crusts, but are not pies.

**Point:** You would never put birthday candles in a pie.

**Counterpoint:** Yes you would.

**Point:** Is a tart a pie?

**Counterpoint:** That's not a point, that's just another question!

**Point:** If you took a sandwich, wrapped it in pastry crust, and called it "sandwich pie", it would be a pie, no?

**Counterpoint:** ... o_O ... no.

**Point:** So!?

**Counterpoint:** Is a hot dog a sandwich?

**Point:** I am eating an open-faced sandwich, which is not actually a sandwich.

**Counterpoint:** GTFO.

**Point:** Don't even start!

**Counterpoint:** ...

**Point:** Why is it "pie vs. cake"? Shouldn't it be "cake vs. pie"?

**Counterpoint:** Bigly.


The Truth About Pie vs. Cake
==================================================

"Quiet the mind, and the soul will speak" - Majaya Sati Bhagavati


Images
==================================================

.. list-table::
   :widths: 100 260 260
   :header-rows: 1

   * - Image
     - Pie
     - Cake
   * - The Usual Suspects
     - .. image:: ../../images/pie.jpg
          :width: 250 px
          :align: left
     - .. image:: ../../images/cake.jpg
          :width: 250 px
          :align: left

Get Off My Lawn
==================================================

If you want a real existential crisis, try figuring out the `definition of a sandwich <https://www.theatlantic.com/video/index/379944/what-is-a-sandwich-no-seriously-though/>`__.
