.. 
.. this topic should never be copied into and/or included with any non-primary documentation set
.. 

==================================================
Writing the Documentation
==================================================

The documentation for |versive| is `built using Sphinx, an open source content management tool <http://www.sphinx-doc.org/en/stable/>`__ that uses |rst| as its source language. |rst| may be authored using your favorite text editor and is saved with the ``.rst`` file extension.

Sphinx was originally created to support documenting Python projects, but has evolved into being a useful tool for publishing collections of technical documentation to a static web site.

See the :doc:`style guide <style_guide>` for more information about how to author documentation at |versive|. See the :doc:`build guidelines <docs_builds>` for information about how to build the internal-facing docs and the public-facing docs.


Writing Process
==================================================
The following sections discuss requirements for the writing process at |versive|.

Pull Requests
--------------------------------------------------
The easiest way to suggest improvements to the documentation is to use the Bitbucket web user interface and create a pull request. The Bitbucket web user interface will allow you to type changes directly in their text editor and complete the entire process right there in just a few easy steps. It only takes a couple of minutes to submit changes to the documentation. Do it for the betterment of humankind.

**Create a pull request**

#. Navigate to https://bitbucket.org/contextrelevantinc/cr_docs/.
#. In the left navigation, choose **Source**.
#. In the list of source directories choose **versive_master/source**.

   The ``versive_master`` directory contains all of the topics at http://docs/ and the HTML filenames on the docs site correspond exactly to the RST filenames in this directory.

#. Click the name of a file. This opens a more detailed view of that file. In the top right, just above the content pane, click **Edit**.
#. This opens a text editor. Make your changes. For questions about specific aspects of Sphinx formatting, see http://docs/internal/style_guide.html.
#. When finished, click the **Commit** button in the bottom right. This opens the **Commit changes** dialog box.
#. In the **Commit changes** dialog box, update the commit message, select the **Create a pull request for this change** check box, and then click **Commit**.

   This will create a pull request in the cr_docs repo.
#. Notify any reviewers.

**Review a pull request**

#. Navigate to https://bitbucket.org/contextrelevantinc/cr_docs/.
#. In the left navigation, choose **Source**.
#. If the change is OK to merge, click **Merge**.
#. In some cases, comments may be necessary. Use the standard left-side commenting functionality in Bitbucket for commenting.

.. warning:: It's pretty easy to run Sphinx locally. If you are concerned that your pull request will break the build, doing a quick local build of Sphinx to verify the change is recommended. The docs builds take ~30 seconds. Really super easy.

reStructuredText
--------------------------------------------------
Authoring documentation using |rst| is easy. It's a text file with space-delimited formatting.

* Use a good text editor such as TextMate, Atom, or EditPad Pro.
* Use only space characters to format whitespace. Using tabs may not break the build, but they often break the formatting. Sometimes tabs break the build too. Spaces are better.
* Sphinx adds directives for more "advanced" formatting. Directives always start with a double period and end with a double colon: ``.. some-directive::``. Within directives, the same whitespace formatting rules apply. Use only directives outlined in this style guide. (|versive| documentation does not support using all of the directives in Sphinx, just the ones in the :doc:`style guide <style_guide>`.)

Use Good Text Editors
--------------------------------------------------
Many text editors, especially those embedded in command shell workflows, tend to create hard wrapping in the documentation similar to::

   This is a topic that has a hard wrapping
   at forty characters as an example of
   hard wrapping in documentation. This
   type of formatting technically builds
   fine (mostly), but it can introduce
   some challenges with ensuring that
   Sphinx-authored content builds as
   nicely as we want it to.

This is super annoying from a technical writing perspective, especially when trying to edit that content in a normal text editor. Technical writers often work on multiple topics at the same time (either via tabs in a text editor or in multiple windows) and rely on the text wrapping at the window width. Topics should not wrap at some arbitrary width and should instead wrap with the page size to make the whitespace within paragraphs reliably visible no matter the size of the text editor window.

Tabs? Evil!
--------------------------------------------------
Tabs are evil. |rst| topics are formatted one whitespace character at a time. This can be annoying when formatting large code blocks and tables, but that's the way it is. Tabs will break the formatting and may break the build because Sphinx relies on whitespace to determine the correct formatting for the content. Tabs often show as whitespace in a text editor and are difficult to spot.

.. note:: `Developers that use spaces make more money than those who use tabs. <https://stackoverflow.blog/2017/06/15/developers-use-spaces-make-money-use-tabs/>`__ It's on StackOverflow, so it must be true!

Copypasta
--------------------------------------------------
Copying and pasting (AKA "copypasta") from non-text file formats, such as Confluence and Word documents will cause you some real pain, especially with quotations, tabs, and other invisible hidden characters. In rare cases after copy/paste, the character you see in the text file isn't the character that is actually in the text file. This is super annoying. If you copy/paste from somewhere into |rst| files, expect to do some work to clean that up.



.. _docs-changes-blocks:

Blocks
==================================================
This section describes how to update the reference documentation for blocks that are added/changed in the blocks library.

**To add a block to the blocks library**

#. Open the ``cr_docs`` repository.

   .. note:: This repository does not use ``repo-push``, run any tests, or have any dependencies on ``apps_repo``, ``repo``, or ``tools``.
#. Copy the following code block. It's the template for each block in the blocks reference::

      .. _blocks-library-blockname:

      BlockName
      --------------------------------------------------
      Use an xxxxx block to xxxxx.

      Usage
      ++++++++++++++++++++++++++++++++++++++++++++++++++

      .. code-block:: yaml
   
         block_graphs:
           <block_graph>:
             ...
             blocks:
               <block_name>:
                 class_name:
                   symbol: 'BlockName'
                 ... complete the config as necessary
               ...
             ...

      Settings
      ++++++++++++++++++++++++++++++++++++++++++++++++++
      The following configuration settings are available to this block:

      **class_name** (required, str)
         The name of the block. Must be set to ``symbol: 'BlockName'``.

      **setting_name** (optional, datatype)
         description.

      Examples
      ++++++++++++++++++++++++++++++++++++++++++++++++++
      The following configuration will xxxxx:
      
      .. code-block:: yaml
      
         xxxxx:
           class_name:
             symbol: BlockName
           ... rest of configuration for this example goes here
      
      A table with the following inputs:
      
      .. code-block:: sql
      
         --------- ---------
          column1   column2
         --------- ---------
          value     value   
          value     value   
          value     value   
         --------- ---------
      
      will be updated to:
      
      .. code-block:: sql
      
         --------- --------- ---------
          column1   column2   column3 
         --------- --------- ---------
          value     value     value   
          value     value     value   
          value     value     value   
         --------- --------- ---------
      

   .. note:: The **Examples** section in the template is representative. The input and output tables will need to be adjusted appropriately. Try to keep the # of columns within the visible page width. This is somewhere between 3-5 actual columns, depending on the width of each column. These are examples of usage and should be tailored so that the presentation fits within the format.

#. Paste the copied template into the correct location in the ``/cr_docs/versive_master/blocks.rst`` file. Blocks are listed alphabetically, always.

#. Complete the template. (Look for ``xxxxx`` and ``BlockName`` for placeholder indicators.)

   There are four sections: a description (required), a usage block (required), a list of configuration settings that are listed alphabetically using a definition list (required), and (optional, but recommended) at least one configuration example that shows the block being used in a realistic situation, with actual values, correct inputs and outputs.

#. The description should be short and concise. ``Use the xxxxx block to xxxxx.`` is the pattern to follow. Fill in the ``xxxxx`` placeholders with correct information.

#. The usage block should show the default values (when they exist) or should represent the data type for each property with a value that's obvious. For example, an input that takes a column name (string) can be shown as ``input: 'column_name'``. The usage block should be in a ``.. code-block:: none`` code block and should show all settings available to this block.

#. The list of configuration settings should be in alphabetical order, and then in a definition list like this::

      **setting_name** (data_type, required-or-optional)
         A short, concise description. Default value: ``value``.

   .. note:: To see what fill and complete :ref:`definition lists <definition_lists>` look like, see the file ``/cr_docs/versive_master/configure.rst``. The indention can be tricky in a definition list when it's required to go deeper than 2 levels.

#. The examples should have an introduction similar to "The following example shows using the xxxxx block to aggregate parsed data, and then join it into a single column for no good reason." and then an example of that block's configuration. The example configuration should be in a ``.. code-block:: sql`` code block (which is configured to display the input and output in a box named "DATA TABLE"). Fill in the ``xxxxx`` placeholder with a nice safe introduction. Use the input/output table pair to illustrate the expected inputs and outputs for the block, based on the example configuration.

#. Add the block to the left navigation. In the ``/cr_docs/versive_master/_templates/nav-docs.html`` file, search for ``"title": "Blocks",``. All of the blocks in the library are listed underneath this section. All blocks, without exception, are listed in alphabetical order. Copy a section for an existing block, paste it into the correct location, and then update it for the new block. These sections look like this::

       {
         "title": "NameOfBlock",
         "hasSubItems": false,
         "url": "/blocks.html#nameofblock"
       },

   There are two strings that need to be updated. For example, from the previous code sample update ``NameOfBlock`` and ``nameofblock`` to be the actual name of the new block. These two elements create the left-side navigation as well as the link to the anchor reference for that block on the page blocks.html.

#. Add :ref:`an internal reference link <links-internal-reference>` for the block to the ``/cr_docs/versive_master/index.rst`` page, under the section named "Process Data" in the subsection named "Blocks Library". Each block should be listed alphabetically, like this::

       | :ref:`blocks-library-templateblock`

   The pipe (``|``) ensures some visual separation between the block names. One space before and one space after the pipe. The ``templateblock`` part of the name is what's used to determine the correct alphabetical order.

#. Create a pull request for the ``cr_docs`` repo and alert the #docs channel in Slack. The docs team will merge the pull request and may do some editing after the merge. The blocks will be built to the http://docs/ site and the individual who created the pull request will be alerted that the page is published. A quick review should be all that's necessary.


**To edit a block in blocks library**

#. Open the ``cr_docs`` repository.

   .. note:: This repository does not use ``repo-push``, run any tests, or have any dependencies on ``apps_repo``, ``repo``, or ``tools``.
#. Open the ``/cr_docs/versive_master/blocks.rst`` file and find the block to be changed.
#. Make your changes.
#. Create a pull request for the ``cr_docs`` repo and alert the #docs channel in Slack. The docs team will merge the pull request and may do some editing.

**To remove a block from the blocks library**

#. Open the ``cr_docs`` repository.

   .. note:: This repository does not use ``repo-push``, run any tests, or have any dependencies on ``apps_repo``, ``repo``, or ``tools``.

#. In the ``/cr_docs/versive_master/blocks.rst`` file, find the block to be removed, and then delete all of its sections (title and description, internal reference link, usage, settings, and examples).

#. In the ``/cr_docs/versive_master/_templates/nav-docs.html`` file, find the block to be removed, and then delete that entry. The full entry for the block in this file must be removed; they are all similar to::

       {
         "title": "TemplateBlock",
         "hasSubItems": false,
         "url": "/blocks.html#templateblock"
       },

#. In the ``/cr_docs/versive_master/index.rst`` page, find the block to be removed, and then delete it.
#. Create a pull request for the ``cr_docs`` repo and alert the ``#docs`` channel in Slack. The docs team will merge the pull request and may do some editing.



.. _docs-changes-configure:

Configuration Settings
==================================================
This section describes how to update the reference documentation for configuration settings.

**To add a configuration setting**

#. Open the ``cr_docs`` repository.

   .. note:: This repository does not use ``repo-push``, run any tests, or have any dependencies on ``apps_repo``, ``repo``, or ``tools``.

#. Find the major section in the configuration file into which this configuration setting is to be added. All of the major sections---**category_patterns**, **entity_attribute_sources**, **modeling**, **results**, and so on---are listed alphabetically.

#. Add the configuration setting to two locations: the usage block and the list of settings.

#. Settings are added to the usage block alphabetically with placeholder data. If the setting has a default value, use the default value in the usage block. If the setting does not have a default value, use the data type as the value, unless it is a string, in which case use a string that shows the value. For example, a section of **results** looks like this::

      relevance_weights:
        - circumstantial: 0.0
        - custom:
          - 'target_name': weight
          - 'target_name': weight
        - strong: 1.0
        - weak: 0.25
      replace_function: 'function_name'
      report_diagnostics: True
   
#. Settings are added to the settings list alphabetically in a :ref:`definition list <definition_lists>`. If a settings group has sub-settings, those settings are also listed alphabetically. For example::

      **relevance_weights** (list, optional)
         A list that specifies the levels used to determine which entities contain surprising behavior and may be assigned a noteworthy :ref:`surprise score <model-data-surprise-scores>`. **strong**, **weak**, and **circumstantial** must be specified. Custom levels for custom measurement targets may also be specified.

         **circumstantial** (float, required)
            The relative strength for circumstantial measurement targets. Must be greater than or equal to ``0.0``. Default value: ``0.0``.

         **custom** (list, optional)
            A list of measurement targets, and then a custom strength to be applied to that target.

         **strong** (float, required)
            The relative strength for strong measurement targets. Must be greater than or equal to ``0.0``. Default value: ``1.0``.

         **weak** (float, required)
           The relative strength for weak measurement targets. Must be greater than or equal to ``0.0``. Default value: ``0.25``.

      **report_diagnostics** (bool, optional)
         Specifies if diagnostics are reported (and sent) to |versive|. Default value: ``True``.


**To edit a configuration setting**

#. Open the ``cr_docs`` repository.

   .. note:: This repository does not use ``repo-push``, run any tests, or have any dependencies on ``apps_repo``, ``repo``, or ``tools``.
#. Open the ``/cr_docs/versive_master/configure.rst`` file and find the setting to be changed.
#. Make your changes.
#. Create a pull request for the ``cr_docs`` repo and alert the #docs channel in Slack. The docs team will merge the pull request and may do some editing.

**To remove a configuration setting**

#. Open the ``cr_docs`` repository.

   .. note:: This repository does not use ``repo-push``, run any tests, or have any dependencies on ``apps_repo``, ``repo``, or ``tools``.
#. Open the ``/cr_docs/versive_master/configure.rst`` file and find the setting to be removed.
#. Make your changes.
#. Create a pull request for the ``cr_docs`` repo and alert the #docs channel in Slack. The docs team will merge the pull request and may do some editing.


.. _docs-changes-release-notes:

Release Notes
==================================================
This section describes how to update the release notes for the |vse| and for the |versive| platform.

A quick comment about release notes. There are three main sections:

* **What's New** This section covers new features and new functionality. Things that were not in the product prior to this release. For example, entire new features, new command line arguments, new configuration settings. This content should be similar to the content for this new feature/functionality that appears elsewhere in the documentation, including potentially using the ``.. includes::`` directive to pull in a literally-exact snippet, but at least some type of abridged version. The list of what's new should be structured using headers, with new features getting their own headers, configuration changes grouped together, and so on.

* **What's Changed** This section covers changed features and functionality, including deprecations, fixed bugs, and so on. In some cases, these may be verbose, whereas others may be a simple statement. The list of what's changed should be an unordered list.

* **Known Issues** This section covers things a customer may need to know. These may just be simple caveats about application or user behaviors, but may also be workarounds to breaking bugs (that are not yet fixed). In some cases, these may be verbose, whereas others may be a simple statement. The list of known issues should be an unordered list.

.. note:: There are release notes `for the Versive Security Engine <../../release_notes.html>`_ and `for the Versive platform <../platform/release_notes.html>`_.

**To add an item to the release notes**

#. Open the ``cr_docs`` repository.

   .. note:: This repository does not use ``repo-push``, run any tests, or have any dependencies on ``apps_repo``, ``repo``, or ``tools``.
#. In the ``/cr_docs/versive_master/release_notes.rst`` file, determine the type of release note: what's new, what's changed, or known issue. Use the following questions to identify the section if you aren't sure:

   Was it in the product before? No? Add to What's New. Yes? See next question.

   Did it change at a feature level in a way that's visible to a customer? Yes? Add to What's Changed. No? See next question.

   Does it affect the customer (positively or negatively), yet is neither something new or something that's changed? Yes? Add to known issues. No? Probably doesn't belong in the release notes.

#. Add the information to the appropriate section.
#. Create a pull request for the ``cr_docs`` repo and alert the ``#docs`` channel in Slack. The docs team will merge the pull request and may do some editing. The blocks will be built to the http://docs/ site and the individual who created the pull request will be alerted that the page is published. A quick review should be all that's necessary.
