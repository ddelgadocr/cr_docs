..
.. xxxxx
..

==================================================
Naming Names
==================================================

This topic contains tips for good naming habits at |versive|. When coming up with names:

#. Use simple words that lots of people can understand.
#. Use logical groupings; be consistent about them.
#. Save more difficult words for when they are necessary.
#. Limit (whenever possible) the use of jargon.
#. Don't use our company name or our official product names as part of other names.
#. Only use 3rd party names when required.

**Example naming exercise**

Ned needs to build a new block in the block library that helps the engine create standard dates within a single day for successful processing. Ned is in a hurry so uses ``RegularizeCrossDayBoundryBlock`` to get the code moving.

Before Ned checks in code, Ned asks around to help identify the correct name for this block. Initial conversations help clarify that this block ensures that all of the dates within a single processing period (which spans 24 hours, but may have more than one actual calendar date) are handled as if they have a single calendar date.

Ned switches to ``FilterToSingleDayBlock``, which is more accurate and because nobody knows what a cross-day boundary is without using Google. But filtering data isn't exactly what's going on here. It's more like standardizing data. So after additional thoughts, conversations, and consideration of feedback Ned settles on ``StandardizeDatesBlock`` as the best name for the block.


Verb Patterns
==================================================
Use a verb pattern at the start of an API function, a block, or a command whenever possible. This helps the user more quickly infer what this piece of functionality is supposed to do.

* Work to ensure that specific verbs (and/or verb groups) imply specific things.
* Keep the final names short, where by "short" we mean "fewer than ~28 characters" to help improve readability and presentation. (Remember that names often end up in left-side documentation navigation or as column names in a user interface.)

.. note:: For an excellent example of a good naming pattern, see the list of processors here: https://nifi.apache.org/docs.html.

.. note:: All the verbs are in **ALL CAPS**.

Generic Verbs
--------------------------------------------------
The following list of verbs represent a naming pattern that is generally consistent with software applications all over this planet. While we don't have to follow other people's patterns literally, we shouldn't be inconsistent either.

**ADD**, **DELETE**, **EDIT**, **VIEW**
   These verbs work together because most things that can be added can also be deleted, edited, and viewed. For example: a file, a user account. This is comparable to CRUD (create, remote, update, delete), but with the benefit of having fewer characters. As we all know, fewer characters means more space.

   We "delete a user account" from the system (unless we **DISABLE** it).

**ADD**, **REMOVE**
   These verbs work together when the object is part of a list or group that has some type of permanence. For example: a list of users, where the list always exists, but the users that belong to it changes over time.

   We "remove a user" from a list of users.

**OPEN**, **CLOSE**
   Hopefully obvious. Don't use these for non-obvious things.

**ENABLE**, **DISABLE**
   Something that was enabled, but is now disabled, still exists in the system in a way that allows it to be reenabled. But otherwise, hopefully obvious. Don't use these for non-obvious things.

**SAVE**, **LOAD**
   Hopefully obvious. Don't use these for non-obvious things.

**MARK**
   We use this one in our web user interface for the |vse| to allow a user to flag a threat case as something that's already been reviewed. "Mark as seen" for example.

**USE**
   This is the recommended prefix for all settings, blocks, whatnots where the user is offered a Boolean choice. The full name of the setting, block, whatnot should remain neutral to either condition (true or false). For example: a ``use_dns_data`` setting (or a ``UseDNSBlock``) with a configured value of ``True`` or ``False``.

**COPY**, **PASTE**
   Hopefully obvious. Don't use these for non-obvious things.

**GET**, **PUT**, **POST**, **DELETE**
   Anything that's directly related to a REST API call should use the same verb as part of its own naming convention. We have a small API that's part of our web user interface for the |vse| and may (over time) have other elements that rely on REST APIs. Keep this in our back pocket for future use by not using these verbs in other situations.

**BACKUP**, **RESTORE** 
   Should be limited to files, tables, data stores, and such things. We might back up a CSV file. We might restore a database.

**PARTITION**
   During the operation of the engine, we may partition data. 

**ENCRYPT**, **DECRYPT**
   If we encrypt and/or decrypt data via configuration settings, blocks, or whatever, we should prefix those actions with one of these verbs. If not, then don't.

**START**, **STOP**, **RUN**, **DEBUG**
   These are verbs that relate to processes (start a process, stop a process), relate to programmatic activities (run a program, debug a program), etc. We don't have too many things like this, but if we do they are hopefully obvious. Don't use these for non-obvious things. Also see DRIVE.

**DRIVE**
   Our "run the engine" verb is DRIVE because that't the name of the command. This is fine as long as we're clear about it. (This should be ... remarkably easy for us!)

**RE**, **UN**, etc.
   Prefixes are cool. We should use them when they are meaningful. For example, you can partition a database and you can REPARTITION a database. You can delete a file and you can UNDELETE a file. There are many prefixes in the English language and this list isn't meant to imply those not on it cannot be used. Let's talk about each one of them as they come up.

   Same with suffixes, but without any examples.

**ALL**, **AS**
   Modifiers are also cool. We should use them when they are meaningful. For example, our web user interface allows a user to mark threat cases. You can **MARK ALL** threat cases or you can **MARK AS** a specific threat case. There may be other examples where using an extra word helps a name shine. Let's talk about each one of them as they come up.


|versive|-specific Verbs
--------------------------------------------------
The following verbs work should be used in a |versive|-specific context, such as naming API functions, blocks, configuration settings, as well as with the machine learning / data processing actions that take place within the context the security engine, the data its processing, and the pipeline that does it:

**AGGREGATE**, **GENERATE**, **PARSE**, **STANDARDIZE**, **TRANSFORM**
   These relate directly to "phases" that exist within the engine pipeline. We should resist using these verbs outside of this context.

   When the modeling side of the pipeline is added to blocks, we should expect some new verbs like **LEARN**, **MODEL**, **SAMPLE**, or **SHARE**. TBD!

**BUILD**, **CALCULATE**, **CATEGORIZE**, **CLEAN**, **COLLECT**, **COUNT**, **DESCRIBE**, **FILL**, **FILTER**, **FIND**, **JOIN**, **MERGE**, **PIVOT**, **REBUILD**, **RENAME**, **REPLACE**, **RESHAPE**, **SAMPLE**, **SELECT**
   These are verbs that describe what's happening to data as it moves through the pipeline. 

   We should be precise with these types of terms. For example, **RESHAPE** is a general term that `can refer to "pivoting", "stacking", and "unstacking" <https://pandas.pydata.org/pandas-docs/stable/reshaping.html>`_ in some areas of the Python development world.

   For example: ``AddColumnByCategoryBlock``. This block was originally named ``PivotBlock``. What it does is takes a list of column and category inputs, and then adds columns to a table based on those inputs. An early iteration of renaming the ``PivotBlock`` led to ``ReshapeCategoryBlock``, but after further discussion the most simple verb (add) was settled on, because ``AddColumnByCategoryBlock`` is the the closest name that matches what the block is actually doing.

**EXECUTE**
   Do **START** and/or **RUN** work just as well?

**SUM**
   Use **SUM** as a verb when the action is to combine values together into a single value.
