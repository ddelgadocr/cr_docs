.. 
.. this topic should never be copied into and/or included with any non-primary documentation set
.. 

==================================================
Building a PDF
==================================================

PDFs are necessary when a topic or article needs to be shared via email or for a customer that doesn't have access to the documentation. These should be shared on a case-by-case basis because once built, PDFs cannot be updated and consequently can become stale.

Requirements
==================================================
The following applications must be available to the local builds:

* WeasyPrint: http://weasyprint.org/
* The single-page theme: versive_singlepage

Install WeasyPrint
==================================================
The installation instructions for WeasyPrint says it's easy. But it may not be. After much trial and error and investigation of various error messages, this is what was necessary:

**Install WeasyPrint**

#. Run the following command to ensure permissons to directories:

   .. code-block:: console

      $ sudo chown -R $USER:admin /usr/local

   .. note:: If Homebrew is run without access to these directories, all kinds of difficult-to-resolve issues may arise with Cairo, Pango, and Libffi.

#. Run the following Homebrew command to install the most difficult dependencies for WeasyPrint:

   .. code-block:: console

      $ brew install python3 cairo pango gdk-pixbuf libffi

#. Run the following command to install WeasyPrint itself:

   .. code-block:: console

      $ pip install weasyprint

#. Run the following command to verify if WeasyPrint installed correctly:

   .. code-block:: console

      $ weasyprint --version

   This should return a statement similar to:

   .. code-block:: console

      WeasyPrint version 0.42.1

#. Run a test PDF build, by pointing to a page on the WeasyPrint website, and then building it locally to your root directory:

   .. code-block:: console

      $ weasyprint http://weasyprint.org ./weasyprint-website.pdf

   This will returns some CSS warnings and errors, but it also should build a PDF. Compare the URL to the PDF. This is how WeasyPrint works.


Theme
==================================================
PDFs use the ``versive_pdf`` theme to build the standalone HTML.


Build Standalone Pages
==================================================
Run the following command to build standalone pages:

.. code-block:: console

   $ sphinx-build -b html cr_docs/versive_singlepage/source/ cr_docs/output/cr_master/pdf/filename.pdf

These are the pages that will be built to PDF.




sphinx-build -b html open-source/versive_md/source/ open-source/output/versive/pdf/md.pdf

Build PDFs
==================================================
For each page in the http://docs/pdf/ output directory that is to be built as a PDF, run the following command:

.. code-block:: console

   $ weasyprint /path/to/input/filename.html ./cr_docs/output/cr_master/filename.pdf

For example, to print a PDF at the http://localhost/pdf/style_guide.html URL:

.. code-block:: console

   $ weasyprint http://localhost/pdf/style_guide.html ./cr_docs/output/cr_master/pdf/style_guide.pdf
