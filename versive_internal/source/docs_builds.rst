.. 
.. this topic should never be copied into and/or included with any non-primary documentation set
.. 

==================================================
Building the Documentation
==================================================

The documentation for |versive| is `built using Sphinx, an open source content management tool <http://www.sphinx-doc.org/en/stable/>`__ that uses |rst| as its source language. |rst| may be authored using your favorite text editor and is saved with the ``.rst`` file extension.

Sphinx was originally created to support documenting Python projects, but has evolved into being a useful tool for publishing collections of technical documentation to a static web site.

See the :doc:`style guide <style_guide>` for more information about how to author documentation at |versive|.

.. warning:: The documentation builds will be automated at some point in the future. Right now, however, they are built locally, and then uploaded to a BitBucket repository, from which the content is synchronized to the two publishing locations (as separate workflows).

Requirements
==================================================
The following applications must be available to the local builds:

* Sphinx, version 1.5.1 (or higher): http://www.sphinx-doc.org/en/stable/
* Sass, the manager for CSS templates in the ``_theme`` directory: http://sass-lang.com/
* SourceTree, a graphical interface for interacting with BitBucket repos: https://www.sourcetreeapp.com/
* https://github.com/tell-k/sphinxjp.themes.revealjs for building topics located at ``/cr_docs/versive_slides/source/``, which are HTML presentations with slides, similar to PowerPoint

cr_docs
==================================================
``cr_docs`` is the name of the repository used to author all internal- and public-facing content for all engines, platforms, and technical documentation.

Bitbucket
--------------------------------------------------
The ``cr_docs`` repository in BitBucket is the source repository for documentation at |versive|, including both internal and public-facing documentation. This repository is designed to ensure that content may be single-sourced content, themes, and other assets (like tokens and images) are available to all published documentation in exactly the same way.

repo-push
--------------------------------------------------
The ``cr_docs`` repository does not require the ``repo-push`` command. In addition, builds related to the ``cr_docs`` repository do not run any |versive|-specific software applications, do not trigger any tests, and have zero dependencies on any applications outside of what Sphinx and the ``cr_docs`` repo requires.

.. note:: The ``repo-push`` command does not support SourceTree, which is a required interface to Bitbucket for the ``cr_docs`` repo.


SourceTree
--------------------------------------------------
SourceTree is the graphical user interface for BitBucket. It's easy and runs git commands on behalf of the actions the user specifies. It creates branches, pulls, pushes, and all of other activities that are required.

SourceTree is supported for the documentation management at |versive| so as to be more inclusive to non-engineering roles that may want to contribute to the documentation. Using git itself is not difficult, but for some folks it's an additional barrier to being able to participate. SourceTree helps make it easier.

Guidelines
--------------------------------------------------
The following guidelines should be followed when building documentation:

#. Build the content from scratch just prior to committing it to Bitbucket. Delete the contents of the ``/cr_docs/output/`` or ``/cr_docs/output_public/`` directories to force a complete rebuild. This ensures that the search index contains an entry for all files in the doc collection.
#. Verify that each sub-directory is present with the correct names, etc.


Internal Site
==================================================
Documentation is published as |versive|-only as a internal tool. This requires access to the corporate network (either in the office or via VPN). This content may not be accessed from outside the corporate network.

The following sections describe what must be built, the location to which it is uploaded, and then the process to synchronize this content with the internal documentation web site.

.. note:: You may submit pull requests to the ``cr_docs`` repo without also building all of the various outputs. The outputs only need to be built and committed to the ``cr_docs`` repo by the individual who is going to update the internal docs site.

Internal Site Layout
--------------------------------------------------
The following collections are part of the internal documentation.

.. list-table::
   :widths: 200 200 200
   :header-rows: 1

   * - Doc Collection
     - Repo Directory Name
     - Description
   * - **Versive Security Engine**
     - ``cr_docs/versive_master/``
     - This doc collection contains content for the |vse|. All of this content is published as part of the public-facing doc collection for that engine.

       .. note:: At some point in the future, there will be more than one engine. At that point, the ``versive_master`` directory will be refactored to be neutral to engines and be updated to act more as an interstitial landing page. Each engine will get its own collection with a consistent naming pattern, such as ``versive_engine_security`` and ``versive_engine_etc``.
   * - **Versive Platform**
     - ``cr_docs/versive_platform/``
     - This doc collection contains content for all of the APIs in the |versive| platform. One API---the RISP API---is currently published as part of the public-facing documentation. 
   * - **Tutorials**
     - ``cr_docs/versive_tutorials/``
     - An experimental doc collection that contains tutorials, which are essentially walkthroughs and how-tos around how to use the engines, how to use the platform.
   * - **Support**
     - ``cr_docs/versive_support/``
     - An experimental doc collection that contains the runbook, a collection of management- and maintenance-focused topics. It will also evolve into being the KB for all engines and the platform.
   * - **Slide Decks**
     - ``cr_docs/versive_slides/``
     - This doc collection contains HTML slide presentations, similar to a PowerPoint deck. They are used to show variations of tutorials and overviews in a way that is similar to a presentation. They are generally 100% single-sourced and are, effectively, "free" outside of some publishing and maintenance. They are intended to offer some folks a different way of experiencing some content and are linked from the engine and platform doc collections where appropriate.
   * - **Internal**
     - ``cr_docs/versive_internal/``
     - This doc collection contains the documentation style and build guides, along with some random "fun" topics like the Pie vs. Cake discussion. These topics are 100% internal to |versive| and will never appear in any public-facing doc collection.

These docs collections are linked across the top-level navigation, though in the case of the engine and platform documentation, the links to the other sites are hidden.

.. warning:: The documentation for the Versive Security Engine and Versive Platform (API) are the only two documentation collections that are considered "complete" and "customer-ready". All other documentation collections are either in-process, somewhat experimental, or will (after more work is done) be elevated to the customer-ready public-facing site.

Build Internal Docs
--------------------------------------------------
Currently, each doc collection is built separately. The Sphinx builds are quick and it takes less than 5 minutes to build all of them.

All doc builds that will be published as updates to the internal documentation web site must be built to a specific location in the ``cr_docs`` repository:

.. list-table::
   :widths: 200 200
   :header-rows: 1

   * - Source Directory
     - Output Directory
   * - ``cr_docs/versive_master/``
     - ``cr_docs/output/cr_master/``
   * - ``cr_docs/versive_platform/``
     - ``cr_docs/output/cr_master/platform``
   * - ``cr_docs/versive_tutorials/``
     - ``cr_docs/output/cr_master/tutorials``
   * - ``cr_docs/versive_support/``
     - ``cr_docs/output/cr_master/support``
   * - ``cr_docs/versive_slides/``
     - ``cr_docs/output/cr_master/slides``
   * - ``cr_docs/versive_internal/``
     - ``cr_docs/output/cr_master/internal``

Each of these must build the source directory using Sphinx and putting the successful output into the output directory.

Internal Commands
--------------------------------------------------
The following commands will build each doc collection:

* ``$ sphinx-build -b html cr_docs/versive_master/source/ cr_docs/output/cr_master``
* ``$ sphinx-build -b html cr_docs/versive_platform/source/ cr_docs/output/cr_master/platform``
* ``$ sphinx-build -b html cr_docs/versive_tutorials/source/ cr_docs/output/cr_master/tutorials``
* ``$ sphinx-build -b html cr_docs/versive_support/source/ cr_docs/output/cr_master/support``
* ``$ sphinx-build -b html cr_docs/versive_slides/source/ cr_docs/output/cr_master/slides``
* ``$ sphinx-build -b html cr_docs/versive_internal/source/ cr_docs/output/cr_master/internal``

.. warning:: You may customize this however you like for your own local build workflow, as long as the right outputs get to the right spots. Until workflow automation is in-place, no additional tooling will be supported.


Update Internal Docs
--------------------------------------------------
The steps to updating, rebuilding, and then publishing the docs to the internal site are something like this.

**To add content and/or rebuild HTML**

#. Create a branch in SourceTree against the ``cr_docs`` repo.
#. Make your changes.
#. Use SourceTree "File status" view to stage and commit files. Commit messages should be short and sweet.
#. Run all of the builds for the internal site, building them to ``cr_docs/output/cr_master``. There should be directories under ``cr_master`` named ``internal``, ``platform``, ``slides``, ``support``, and ``tutorials``.
#. Commit the rebuilt HTML using a commit message of "Rebuild HTML".

**To update the internal documentation web server**

#. Log into the internal web server: ``ssh ubuntu@docs``.
#. Authenticate.
#. cd cr_docs
#. git pull
#. Bitbucket password.

.. warning:: Ask jscott@versive.com for the password required to authenticate to the internal docs server. You will need your Bitbucket password to be able to run a ``git pull`` command from the machine, which is already configured to know about the ``cr_docs/outputs/cr_master`` directory. Anything not under ``cr_master`` is not pulled.


Public-facing Site
==================================================

.. warning:: The public-facing documentation is currently published to http://versive-docs.herokuapp.com/index.html, which is not (yet) aliased to https://docs.versive.com/.

A subset of the internal documentation is published to a public-facing web site. Access to this web site requires authentication, but is otherwise accessible from anywhere outside of the |versive| corporate network.

.. note:: |versive| team members may use the internal credentials to access this information from outside the corporate network. Certain customers are provided credentials which they may share among their team members who may require access to the documentation.

   username: `guest`
   password: `sofa9358`

The following sections describe what must be built, the location to which it is uploaded, and then the process to synchronize this content with the public-facing documentation web site.

Public Site Layout
--------------------------------------------------
The following collections are part of the public-facing documentation.

.. list-table::
   :widths: 200 200 200
   :header-rows: 1

   * - Doc Collection
     - Repo Directory Name
     - Description
   * - **Versive Security Engine**
     - ``cr_docs/versive_public_vse/``
     - This doc collection contains content for the |vse|. All of this content is published as part of the public-facing doc collection for that engine.

       .. note:: This content is currently published as the root doc collection on the public-facing site. Either as part of the workflow automation work or at the point where a 2nd engine is available, this location will be updated to be an interstitial page, with engine-specific content moved to a more specific path.
   * - **Versive Platform**
     - ``cr_docs/versive_public_platform/``
     - This doc collection contains content for the RISP API, which (currently) is the only part of the |versive| platform API that is included with public-facing documentation. 


Build Public Docs
--------------------------------------------------
Currently, each doc collection is built separately. The Sphinx builds are quick and it takes less than 5 minutes to build all of them.

All doc builds that will be published as updates to the internal documentation web site must be built to a specific location in the ``cr_docs`` repository:

.. list-table::
   :widths: 200 200
   :header-rows: 1

   * - Source Directory
     - Output Directory
   * - ``cr_docs/versive_public_vse/``
     - ``cr_docs/output_public/security_engine/``
   * - ``cr_docs/versive_public_platform/``
     - ``cr_docs/output_public/security_engine/platform``

Each of these must build the source directory using Sphinx and putting the successful output into the output directory.

Public Commands
--------------------------------------------------
The following commands will build each doc collection:

* ``$ sphinx-build -b html cr_docs/versive_public_vse/source/ cr_docs/output_public/security_engine/``
* ``$ sphinx-build -b html cr_docs/versive_public_platform/source/ cr_docs/output_public/security_engine/platform/``

.. warning:: You may customize this however you like for your own local build workflow, as long as the right outputs get to the right spots. Until workflow automation is in-place, no additional tooling will be supported.


Update Public-facing Docs
--------------------------------------------------
The steps to updating, rebuilding, and then publishing the docs to the public-facing site are something like this.

**To add content and/or rebuild HTML**

#. Create a branch in SourceTree against the ``cr_docs`` repo.
#. Run all of the builds for the public-facing site, building them to ``cr_docs/output_public/security_engine``. There should be a directory under ``security_engine`` named ``platform``.
#. Commit the rebuilt HTML using a commit message of "Rebuild HTML".

**To update the internal documentation web server**

#. Log into the Heroku app via the command shell: ``heroku login``.
#. Authenticate using your email and password.
#. cd cr_docs
#. git checkout mv_hosted
#. Bitbucket password.

.. warning:: Ask jscott@versive.com for the password required to authenticate to the public-facing docs server. You will need a Heroku password to be able to run these commands for the Heroku app, which is already configured to know about the ``cr_docs/output_public/security_engine`` directory. Anything not under ``security_engine`` is not pulled.


