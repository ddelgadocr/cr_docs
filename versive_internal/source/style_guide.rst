.. 
.. this topic should never be copied into and/or included with any non-primary documentation set
.. 

.. 
.. commented out -- see section "Colored Text in Paragraphs" (also commented out)
.. 

.. role:: red

.. role:: blue

.. 

==================================================
Style Guide
==================================================

This is the style guide for |versive| documentation. It applies to all content that is produced by the Sphinx content management engine, including (but not limited to) reference documentation for the platform API and all engines that run on top of the platform.

|versive| documentation is authored using |rst|, which is a whitespace-delimited text-based format that is easy to use. |rst| is then built using Sphinx, a content management engine that builds static HTML, PDF, man page, and other important formats from |rst| files.

Style Guide Resources
==================================================
Anything on this page supercedes any style guidance you may find elsewhere. That said, the following resources are useful:

* `Google Developer Documentation Style Guide <https://developers.google.com/style/>`_
* `Microsoft Manual of Style (4th Edition) <https://www.amazon.com/Microsoft-Manual-Style-4th-Corporation/dp/0735648719/ref=sr_1_1?ie=UTF8&qid=1505325726&sr=8-1&keywords=microsoft+manual+of+style>`_ (available as a book only)

.. note:: Style guides such as Strunk & White or the Chicago Manual of Style can be handy, but we're not writing books or publishing magazines. If we had an O'Reily book for using the Versive platform, that content would conform to whatever style O'Reily uses (as per their editors). That said, for our documentation, when in doubt, and if not covered on this page, use the Google developer's style guide first and ask questions second.

Simple Formatting
==================================================
Use any of these formatting options within paragraphs and lists:

* **bold** Use two asterisks (``**``) around the word to apply bold formatting: ``**bold**``
* *italics* Use a single asterisk (``*``) around the word to apply italics formatting: ``*italics*``
* ``inline code string`` Use two backticks around the code string to apply code block formatting. For example:

  .. code-block:: none

     ``inline code string``

  renders as shown above at the start of this list item.

  .. note:: An inline code string should only be used within lists and paragraphs for function names, commands for command-line tools, and so on, and only in a way where the contents of that code string reads normally in a sentence. Use the code-block directive for anything else.

* One space vs. two spaces? There should only be one space after a sentence in a paragraph.

Lists
==================================================
Three types of lists are available: ordered, unordered, and definition.

Ordered Lists
--------------------------------------------------
An ordered list has each list item preceded by an ``#.`` followed by a space. For example:

.. code-block:: none

   #. one
   #. two
   #. three

Which will appear in the documentation like this:

#. one
#. two
#. three


Unordered Lists
--------------------------------------------------
An unordered list has each list item preceded by an ``*`` followed by a space. For example:

.. code-block:: none

   * one
   * two
   * three

Which will appear in the documentation like this:

* one
* two
* three


.. _definition_lists:

Definition Lists
--------------------------------------------------
A definition list is a specially formatted list that uses whitespace to indent the descriptive text underneath a word or a short phrase. This type of list is used to describe settings---such as command line parameters, API arguments, glossary terms, and so on. For example:

.. code-block:: none

   **list_item_one**
      The description must be indented three spaces.

   **list_item_two**
      The description must be indented three spaces.

Which will appear in the documentation like this:

**list_item_one**
   The description must be indented three spaces.

**list_item_two**
   The description must be indented three spaces.

Complex Definition Lists
++++++++++++++++++++++++++++++++++++++++++++++++++
A definition list may contain a definition list. For example, some configuration settings (already in a definition list) have specific additional settings that must also be in a definition lists. For example:

.. code-block:: none

   **list_item_one**
      The description must be indented three spaces.

      **sub_item_one**
         The definition list item must be indented three spaces and the description must be indented six spaces.

      **sub_item_two**
         The definition list item must be indented three spaces and the description must be indented six spaces.

   **list_item_one**
      The description must be indented three spaces.

Which will appear in the documentation like this:

**list_item_one**
   The description must be indented three spaces.

   **sub_item_one**
      The definition list item must be indented three spaces and the description must be indented six spaces.

   **sub_item_two**
      The definition list item must be indented three spaces and the description must be indented six spaces.

**list_item_one**
   The description must be indented three spaces.


Admonitions
==================================================
Admonitions are notes and warnings. Use notes to call out something important. Sometimes use notes as an extra helper for formatting a page. It's easy to get caught up with the correct use of notes vs. warnings, so let's use this:

A warning should be used when something will break if it's not followed correctly. A warning displays in a bright, obvious colored box and people will see them.

A note may be used to call something out (that won't break), might be unrelated to the current information (but still useful to the reader), or simply to provide some useful formatting on a page to break it up visually. (Remember: people skim documents more than they read them.) A note displays in a color similar in tone to the main colors in the documentation set and they are less obvious.


Notes
--------------------------------------------------
For notes, use ``.. note::`` as shown here:

.. code-block:: none

   .. note:: The words for the note itself.

Which will appear in the documentation like this:

.. note:: The words for the note itself.

Warnings
--------------------------------------------------
For warnings, use ``.. warning::`` as shown here:

.. code-block:: none

   .. warning:: The words for the warning itself.

Which will appear in the documentation like this:

.. warning:: The words for the warning itself.

Formatting Admonitions
--------------------------------------------------
Notes and warnings are essentially indented paragraph blocks that can have their own formatting. While not recommended for most situations, they may contain additional formatting. For example:

.. note:: This is a note. It contains a code block:

   .. code-block:: python

      def function(foo):
        if (some_thing):
          return bar
        else:
          return 0

   and some more words and then a warning because **something important**:

   .. warning:: This is a warning because code blocks don't look so good in notes and warnings:

      .. code-block:: python

         def function(bar):
           if (some_thing):
             return bar
           else:
             return 0

      so maybe don't use them like this OK?

   This type of formatting---admonitions within admonitions---should be used rarely.

.. 
.. commenting out because Versive doesn't use colored text like this, but it should be in the open source theme, at the point the theme is open sourced.
.. 

Colored Text in Paragraphs
==================================================
You can change the colors of text within paragraphs (and lists, and tables).

role Directive
--------------------------------------------------
At the top of the page in which colored text must be, add the directive:

.. code-block:: python

   .. role:: red

This enables the use of ``:red:`` as way to format text within paragraphs on this page. Add it to the top of the file, above the title, just like on this page (view the source).

Red Text
--------------------------------------------------
Text can be changed to red:

.. code-block:: python

   :red:`This paragraph will change to red, in its entirety.`

Just like this:

:red:`This paragraph will change to red, in its entirety.`


Blue Text
--------------------------------------------------
Text can be changed to blue:

.. code-block:: python

   :blue:`This paragraph will change to blue, in its entirety.`

Just like this:

:blue:`This paragraph will change to blue, in its entirety.`


Other Colors
--------------------------------------------------
Other colors may be added. Update the CSS to support them. In the ``_themes/versive/static/scss/_sphinx.scss`` file, copy: 

.. code-block:: none

   .red {
   	color: $versive-red;
   }

and paste that below the one that's there. Update ``.red`` to the name of the new color, and then update ``$versive-red`` to align to a color that's defined in the ``_themes/versive/static/scss/_config.scss`` file. Add the color to ``_config.scss`` if it's not already there.
 

Code Blocks
==================================================
For code samples (Python, YAML, JSON) and for commands run via the command line that appear in the documentation we want to set them in code blocks using the ``.. code-block::`` directive.


Line Emphasis
--------------------------------------------------
Individual lines in a code block may be emphasized. The presentation is similar to a yellow highlight in a book. The following example shows how to highlight lines 3 and 5 in a code block:

.. code-block:: none

   .. code-block:: python
      :emphasize-lines: 3,5
   
      def function(foo):
        if (some_thing):
          return bar
        else:
          return 0

will display as:

.. code-block:: python
   :emphasize-lines: 3,5

   def function(foo):
     if (some_thing):
       return bar
     else:
       return 0

None Blocks
--------------------------------------------------
For text that needs to be formatted as if it were a code block, but isn't actually code, such as the usage example for a Python function or the example shown right here in this section, use ``.. code-block:: none`` as shown here:

.. code-block:: none

   .. code-block:: none

      This is a none block. It's formatted as if it were code, but isn't actually code.

      Can include code-like things:

      function_foo()
        does: something
      end

Which will appear in the documentation like this:

.. code-block:: none

   This is a none block. It's formatted as if it were code, but isn't actually code.

   Can include code-like things:

   function_foo()
     does: something
   end



Python Blocks
--------------------------------------------------
For Python code blocks, use ``.. code-block:: python`` as shown here:

.. code-block:: none

   .. code-block:: python

      def function(foo):
        if (some_thing):
          return bar
        else:
          return 0

Which will appear in the documentation like this:

.. code-block:: python

   def function(foo):
     if (some_thing):
       return bar
     else:
       return 0


.. _style-guide-yaml-blocks:

YAML Blocks
--------------------------------------------------
For YAML code blocks, use ``.. code-block:: yaml`` as shown here:

.. code-block:: none

   .. code-block:: yaml

      config:
        - some_setting: 'value'
        - some_other_setting: 12345

Which will appear in the documentation like this:

.. code-block:: yaml

   config:
     - some_setting: 'value'
     - some_other_setting: 12345


.. _style-guide-table-blocks:

Table Blocks
--------------------------------------------------
Table blocks are used to show the inputs and outputs of processing data, such as with blocks reference documentation. For table code blocks, use ``.. code-block:: sql`` as shown here:

.. code-block:: none

   .. code-block:: sql

      --------- ---------
       column1   column2 
      --------- ---------
       value     value   
       value     value   
       value     value  
      --------- ---------

Which will appear in the documentation like this:

.. code-block:: sql

   --------- ---------
    column1   column2 
   --------- ---------
    value     value   
    value     value   
    value     value  
   --------- ---------



JSON Blocks
--------------------------------------------------
For JSON code blocks, use ``.. code-block:: javascript`` as shown here:

.. code-block:: none

   .. code-block:: javascript
      
      {
        "foo":
          [
            {
              "one": "12345",
              "two": "abcde",
              "three": "words"
            },
            ...
          ]
      }

Which will appear in the documentation like this:

.. code-block:: javascript
      
   {
     "foo":
       [
         {
           "one": "12345",
           "two": "abcde",
           "three": "words"
         },
         ...
       ]
   }


Command Shell Blocks
--------------------------------------------------
For command shell blocks, use ``.. code-block:: console`` as shown here:

.. code-block:: none

   .. code-block:: console

      $ cr service stop

Which will appear in the documentation like this:

.. code-block:: console

   $ cr service stop


Shell Script Blocks
--------------------------------------------------
For shell script blocks, use ``.. code-block:: bash`` as shown here:

.. code-block:: none

   .. code-block:: bash

      # The product and version information.
      readonly VERSIVE_PRODUCT="vse-app"
      readonly VERSIVE_VERSION="2.15.23-4"
      readonly VERSIVE_RELEASE_DATE="2017-12-15"

Which will appear in the documentation like this:

.. code-block:: bash

   # The product and version information.
   readonly VERSIVE_PRODUCT="vse-app"
   readonly VERSIVE_VERSION="2.15.23-4"
   readonly VERSIVE_RELEASE_DATE="2017-12-15"


Config File Blocks
--------------------------------------------------
For configuration file blocks that are not in Yaml or JSON (such as ``spark-defaults.conf``), use ``.. code-block:: text`` as shown here:

.. code-block:: none

   .. code-block:: text

      spark.setting.hours 1h
      spark.setting.option -Duser.timezone=UTC
      spark.setting.memory 20g

Which will appear in the documentation like this:

.. code-block:: text

   spark.setting.hours 1h
   spark.setting.option -Duser.timezone=UTC
   spark.setting.memory 20g

.. note:: We're using ``text`` because there are not specific lexers available for all of the various configuration files. The ``text`` lexer allows us to style the code block similar to all of the others, but will not apply any highlighting to the formatting within the code block.

Images
==================================================
Images may be embedded in the documentation using the ``.. image::`` directive. For example:

.. code-block:: none

   .. image:: ../../images/stages_all.svg
      :width: 600 px
      :align: center

with the ``:width:`` and ``:align:`` attributes being aligned underneath ``image`` in the block.

This image will appear in the documentation like this:

.. image:: ../../images/stages_all.svg
   :width: 600 px
   :align: center

Images should be SVG when only HTML output is desired. Printing to PDF from HTML pages requires PNG images.

Wildcard Images
--------------------------------------------------
Image filenames may be specified with a wildcard as the filename extension:

.. code-block:: none

   .. image:: ../../images/stages_all.*

When Sphinx builds the file, it will choose the best available image in the ``/images`` folder for the build output for any file named ``stages_all``. For example, if there are both ``stages_all.svg`` and ``stages_all.png`` files, when Sphinx builds HTML output, it will use the SVG file and when Sphinx builds PDF output it will prefer PNG over SVG.

Use the wildcard image pattern in any topic that's included across doc sets and deliverables to help ensure single-sourcing without breaking builds.

About Headers
==================================================
There are four supported header levels in the documentation: H1, H2, H3, H4. This ensures that the right-side navigation structure does not get too deep.

Technically, the length of the header in |rst| only needs to be as long as the words in the header itself, but our style guide requests that all headers are of the same length: 50 characters. This is for a couple of reasons:

#. If the actual string in the actual header is longer than 50 characters, it's way too long. In fact, if your header string is longer than 30 characters, work with the technical writing team to figure out how to make it shorter. Headers may be as long as the right-side navigation allows them to be, as long as they do not wrap in the sidebar.
#. By having all headers be the same length and also obviously much longer than the header string itself, it makes it easier to skim through large documents and see the structure of the topic.

The order of headers in Sphinx is particular. We use equals symbols (``=``) above and below the title of the page, and then below the H1 header, a dash symbol (``-``) below the H2 header, the plus symbol (``+``) below the H3 header, and the caret symbol (``^``) below the H4 header.

.. note:: For additional header level support (H5) use ``**bold**`` to set off the header from a paragraph. It renders the same way, but prevents the H5 header structure from being built into the right-side navigation. There are some examples in the documentation set of using the bold-as-H5 headers, but generally when you find yourself in a situation where you think you need an H5 header, restructuring your navigation so prevent the need for the H5 header is a better option. An H5 header formatted in this style will have the appearance of the H5 header, but will not appear in the right-side navigation structure and is not linkable as an anchor reference.

H1 Headers
==================================================
An H1 header appears in the documentation like this:

.. code-block:: none

   H1 Headers
   ==================================================
   An H1 header appears in the documentation like this.

Which will appear in the documentation like the actual header for this section.

H2 Headers
--------------------------------------------------
An H2 header appears in the documentation like this:

.. code-block:: none

   H2 Headers
   --------------------------------------------------
   An H2 header appears in the documentation like this.

Which will appear in the documentation like the actual header for this section.

H3 Headers
++++++++++++++++++++++++++++++++++++++++++++++++++
An H3 header appears in the documentation like this:

.. code-block:: none

   H3 Headers
   ++++++++++++++++++++++++++++++++++++++++++++++++++
   An H3 header appears in the documentation like this.

Which will appear in the documentation like the actual header for this section.

H4 Headers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
An H4 header appears in the documentation like this:

.. code-block:: none

   H4 Headers
   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   An H4 header appears in the documentation like this.

Which will appear in the documentation like the actual header for this section.


Tables
==================================================
Tables are always fun! There is only one type of table that is supported in the documentation: a list-table, which is built using the ``.. list-table::`` directive.

.. code-block:: none

   .. list-table::
      :widths: 200 400
      :header-rows: 1

      * - columnName
        - columnName
      * - **item1**
        - description
      * - **item2**
        - description

with the ``:widths:`` and ``:header-rows:`` attributes being aligned underneath ``list-table`` in the block. The number of rows (identified by the dashes (``-``) must equal the number of integers specified by ``:widths:``. The integers specified by ``:widths:`` also specifies the column width, from left to right.

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - columnName
     - columnName
   * - **item1**
     - description
   * - **item2**
     - description

.. warning:: There are other types of tables allowed in |rst|, but we're only going to use the ``.. list-table::`` type of tables.

Number of Columns
--------------------------------------------------
Question: How many columns should I put in my table?

Answer: Two, ideally. Maybe three. Sometimes four.

Remember that this topic may be published in multiple formats (HTML, PDF, man page, wherever) and the width available to a table in a particular format is often limited. If you think you need a 10 column table, rethink that (or go put it in Confluence). Four is the most you should ever need. And if the table is so wide that half of it runs off the right side of the page (and can't be seen), then why build that table? As a general rule, the width of the HTML output on our documentation website is a fine judge as to if the table is too wide for other formats.




Links
==================================================
There are three types of links:

#. :ref:`Internal reference <links-internal-reference>`
#. :ref:`Internal topic <links-internal-topic>`
#. :ref:`External <links-external>`

.. _links-internal-reference:

Internal Reference
--------------------------------------------------
There are two ways to link to internal headers across the doc set. First, a pre-requisite: the header to which the link is targeted must have an anchor. For example:

.. code-block:: none

   .. _anchor-name:

   Internal Reference
   --------------------------------------------------
   There are two ways to link to internal headers across the doc set. First, a pre-requisite: the header that is the target of the link must be tagged:

where the internal reference is the ``.. _anchor-name:``. The string "anchor-name" must be unique across the entire doc set, so the required pattern for these is <file-name-header-name>, like this:

.. code-block:: none

   .. _style-guide-yaml-blocks:

and then there are two ways to link to that anchor. The first will pull in the header name as the link:

.. code-block:: none

   :ref:`style-guide-yaml-blocks`

and the second will use the string you put there and will not pull in the header name as the link:

.. code-block:: none

   This links to some information about using :ref:`YAML code blocks <style-guide-yaml-blocks>` in your documentation.

These first example renders like this: :ref:`style-guide-yaml-blocks`. The second example is preferred and looks like the next sentence. This links to some information about using :ref:`YAML code blocks <style-guide-yaml-blocks>` in your documentation.


.. _links-internal-topic:

Internal Topic
--------------------------------------------------
There are two ways to link to internal topics across the doc set. The first will pull in the topic name as the link:

.. code-block:: none

   :doc:`blocks`

and the second will use the string you put there and will not pull in the header name as the link:

.. code-block:: none

   This links to some information about using :doc:`blocks </blocks>`  to build a pipeline.


.. _links-external:

External
--------------------------------------------------
Ideally, external links should just be the full URL: http://www.versive.com, for example.

In some cases the external URL is too big and/or you want to embed the external link naturally within a sentence:

.. code-block:: none

   `some link text here <http://www.versive.com>`_

For example: `some link text here <http://www.versive.com>`_

In some cases, Sphinx will throw warnings and errors if there are too many external links in the docs to the same places, so use a double underscore (``__``) at the end of the external link to stop those warnings/errors.

Linking topics together:

The ``:ref:`` links to an anchor on a page in the doc set, as long as the anchor is specified correctly immediately above the header for the section to which you want to link. For example:

.. code-block:: none

   :ref:`cmd-cr-service-list`

will link to the header tagged with:

.. code-block:: none

   .. `_cmd-cr-service-list`

There can be only one header with that name in the entire collection. The link itself resolves to the header string, which is why we want to limit use of this type of linking to only things that resolve exactly, such as function names, commands, settings groups, and so on. 



Includes
==================================================
The ``.. includes::`` directive allows a topic to be "pulled in" from the ``/includes`` directory.

For example:

.. code-block:: none

   .. includes:: ../../includes/terms.rst

will pull in the contents of that file right into the location of the directive.

Includes are best used as a full and complete paragraph, written and edited to standalone. If you have questions about how best to use these (they can be quite useful!) please talk to the documentation team.

Tokens
==================================================
Tokens are defined in the file ``names.txt`` located at ``cr_docs/tokens``. Each token is defined similar to:

.. code-block:: none

   .. |versive| replace:: Versive

When used in a sentence, use the ``|versive|`` to replace that with the string that follows ``replace::``. For example: |versive| (is the replacement string for ``|versive|``).

.. note:: Tokens should not be used in headers or topic titles. They will work---Sphinx will build them just fine---but any anchor references from the left-side navigation may not work.

.. warning:: Tokens can really slow the build down. Sphinx will check each file for tokens, and then check the tokens file to look for matches. Every time this happens, a kitten dies. So we want to keep the tokens file as small as possible. 

Site Templates
==================================================
Each docs collection has site templates that control the left-side navigation and the top-level navigation.

nav-docs.html
--------------------------------------------------
Each docs collection has its own left-side navigation structure. This is defined in the ``nav-docs.html`` file located in the ``_templates`` directory that belongs to that docs colletion.

This is a JSON file that adds to the theme template and builds out the left-side navigation structure. For example:

.. code-block:: javascript

   {% extends "!nav-docs.html" %}
   {% set active_product = "Versive Internal" %}
   {% set active_page_name = "Versive Internal" %}
   {% set active_version = "current" %}
   {% set navItems = [
     {
       "title": "Some Section",
       "image": "versive-mark-grey.svg",
       "subItems": [
         {
           "title": "Some Title",
           "hasSubItems": false,
           "url": "/some_file.html"
         },
         {
           "title": "Some Other File",
           "hasSubItems": false,
           "url": "/some_other_file.html"
         },
       ]
     },
   ] -%}

.. note:: The left-side navigation is built by hand and is not built automatically from header structures within topics. This is important! This is by design!

   The left-side navigation is located in an important spot in the web site. It's discoverable and easy to use. It's designed to provide a light structure that is easy browse via collapsing and expanding structures. The currently-selected topic is highlighted.

   The contents of the left-side navigation is intended to be arbitrary. It is built by hand and must be updated as the contents of topics change across the documentation collection. Because it is built by hand, the left-side navigation structure is not required to map exactly to header structures found within topics, allowing for alternate groupings of content, as necessary.

   The right-side navigation structure *is* built automatically based on the header structure within each topic. We get it (mostly) for free. It's useful when it's clean and has good patterns and is well-structured. That said, users generally won't use the right-side navigation structure for topic discovery.

   The right-side navigation will display up to 5 levels of headers (including the topic title). This is 2 levels deeper than the left-side navigation structure. So keep this in mind when building topics, and then determining how they should be added to the left-side navigation.



Navigation Structure
++++++++++++++++++++++++++++++++++++++++++++++++++
The left navigation structure is limited to up to four levels, the first being a mandatory (and arbitrary) group name, such as "Deploy" or "Operations" or "Reference". Levels two through four are available for linking to topics and/or anchor references in the doc collection. The following example shows the available structural options:

.. code-block:: javascript

   {
     "title": "Level 1",
     "image": "versive-mark-grey.svg",
     "subItems": [
       {
         "title": "Level 2 w/Children",
         "hasSubItems": true,
         "subItems": [
           {
             "title": "Level 3 w/Children",
             "hasSubItems": true,
             "subItems": [
               {
                 "title": "Level 4",
                 "hasSubItems": false,
                 "url": "/topic_name.html#anchor"
               },
               {
                 "title": "Level 4",
                 "hasSubItems": false,
                 "url": "/topic_name.html#anchor"
               },
             ]
           },
           {
             "title": "Level 3 Standalone",
             "hasSubItems": false,
             "url": "/topic_name.html#anchor"
           },
         ]
       },
       {
         "title": "Level 2 Standalone",
         "hasSubItems": false,
         "url": "/topic_name.html#anchor"
       },
     ]
   },


Levels
++++++++++++++++++++++++++++++++++++++++++++++++++
Up to four levels may be specified.

#. The first level (level 1) is not a link to content, but rather a grouping of content---Deploy, Configure, Reference, etc.
#. The second and third levels may specify a grouping of content.
#. The second through fourth levels may specify links to topics and/or headers within topics in the documentation collection. Links to topics must specify the filename and links to headers must specify the link as an anchor reference.

Levels map roughly to headers in the |rst| files in the documentation collection, just represent one level higher than the |rst| structure.

For example, say file ``foo.rst`` has this structure:

.. code-block:: none

   =====
   Title
   =====

   some content

   Header Level One
   ================
   some content

   Header Level Two
   ----------------
   some content

   Header Level Three
   ++++++++++++++++++
   some content

   Header Level Four
   ^^^^^^^^^^^^^^^^^
   some content

Any of these levels may be linked to from the left navigation using the anchor pattern. For example: ``"url": "/foo.html#header-level-two"``.

Navigation Paths
++++++++++++++++++++++++++++++++++++++++++++++++++
The navigation paths specified by the ``url`` setting is literal and must reflect the actual location in the website. For example, if the topic ``foo.html`` is located under ``/bar/`` the ``url`` setting must specify the full path: ``"url": "/bar/foo.html"``.

Active Product
++++++++++++++++++++++++++++++++++++++++++++++++++
The ``active_product`` setting specifies the name of the product for which this documentation collection exists.

.. code-block:: javascript

   {% set active_product = "Versive Internal" %}

.. warning:: This setting is unused and should (for now) contain the same exact string as the ``active_page_name`` setting.

Active Page Name
++++++++++++++++++++++++++++++++++++++++++++++++++
The ``active_page_name`` setting specifies the first entry at the top of the left navigation.

.. code-block:: javascript

   {% set active_page_name = "Versive Internal" %}

.. warning:: This setting is required and should be consistent with product naming and strings that are specified in the ``layout.html`` file for the links that are present across the top-level of the documentation web site.

Active Version
++++++++++++++++++++++++++++++++++++++++++++++++++
The ``active_version`` setting specifies the application version to which this documentation collection applies. For example: "current" (for the current version of the active product) or "1.1.1", "1.1.2", etc.

.. code-block:: javascript

   {% set active_version = "current" %}

.. warning:: This setting is unused because |versive| does not create version-specific documentation collections. Specify all active versions as "current" for now.




layout.html
--------------------------------------------------
Each docs collection has its own top-level navigation structure, though ideally these are synchronized across the docs set. This is defined in the ``layout.html`` file located in the ``_templates`` directory that belongs to that docs colletion.

``layout.html`` is a file in the theme used for |versive| documentation. Each doc collection may override the default theme layout, as necessary. Typically, this is only done for the top-level navigation links.

{% block nav_main %}
++++++++++++++++++++++++++++++++++++++++++++++++++
The ``nav_main`` block is what defines the navigation structure that appears across the top of the documentation web site. Each documentation collection has its own ``layout.html`` file, in which the available navigation is specified.

Some of the documentation collections are still in-progress---Tutorials, Support, Resources---and they are available from the http://docs/ internal documentation web site, but are not visibly linked from the Security Engine and Platform links.

Another example is on the public-facing site where the Platform API docs are not linkable from the Security Engine documentation.

It's all still there, just not visible from the navigation.



CSS Stylesheets
==================================================
CSS stylesheets are managed using Sass: http://sass-lang.com/install. The Sass templates are located in the theme: ``_themes/versive/static/scss``. Sass must be installed, and then run from the ``_themes/versive/static`` directory using the following command:

.. code-block:: console

   $ sass --watch scss/versive.scss:versive.css_t

After Sass is running, open the Sass template files in a text editor and make changes. When saved Sass will update the CSS file for the theme. The ``_themes/versive/static/scss`` directory has the following files:

* ``_config.scss`` defines templates for managing colors, fonts, and the version of Font Awesome that is used to render the icons that appear in the left-side navigation
* ``_nav-docs.scss`` defines the styles and presentation for the left-side navigation tree
* ``_nav.scss`` defines the navigation that exists across the top of the site
* ``_reset.scss`` should not be modified
* ``_sidebar-ad.scss`` should not be modified
* ``_sphinx.scss`` defines the styles and presentation for the actual documentation page
* ``_version-picker.scss`` is unused in the current theme
* ``versive.scss`` should only be modified when additiona custom underscore-prefixed pages are added to the Sass templates directory

The underscore-prefixed files are aggregated into the file located at ``_themes/versive/static/versive.css_t`` and are managed by the ``versive.scss`` file to generate the CSS file that is used by the theme for presenting the layout of the docs website.
