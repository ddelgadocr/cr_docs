/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/
@import url(https://fonts.googleapis.com/css?family=Roboto:400,600,700,900);
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  font: inherit; }

/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure,
footer, header, hgroup, menu, nav, section {
  display: block; }

blockquote:before, blockquote:after,
q:before, q:after {
  content: '';
  content: none; }

table {
  border-collapse: collapse;
  border-spacing: 0; }

* {
  box-sizing: border-box; }

/* Colors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *

/*
 * Specific to certain areas of the CSS files
 */
/*
 * The following are kinda generic, still might be used.
 */
/* Fonts
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $primary: 
 *   Montserrat is used for the top global navigation and the grey nav on the left,
 *   as well as the global flyout menu on the right (hidden by default).
 *
 * $secondary: 
 *   Helvetica Neue is used for the body text and the Table of Contents on the right.
 * 
 * $mono: 
 *   Consolas is used for monospace text. 
 */
/*
 * versive.css_t
 * ~~~~~~~~~~~~~~~
 *
 * Sphinx stylesheet -- versive theme.
 *
 * :copyright: None.
 * :license: This work is licensed under a Creative Commons 
 * Attribution 3.0 Unported License
 *
 */
/* -- the background color for the whole page and the borders for the topic page, header, footer */
/* -- IMPORTANT -- color is the color of the text on div.document; must be $versive-dark-blue or $versive-dark-grey */
body {
  font-family: "Roboto", "Helvetica Neue", "Helvetica", Arial, sans-serif;
  font-weight: 400;
  font-size: 14px;
  letter-spacing: -0.00em;
  line-height: 1.5;
  background-color: #ffffff;
  color: #424242;
  width: 100%;
  min-width: 270px; }

/* -- the actual topic background, must be white all the time no exceptions */
div.document {
  background-color: #ffffff;
  text-align: left;
  background-image: url(contents.png);
  background-repeat: repeat-x;
  max-width: 1295px;
  margin: 0 auto;
  width: 100%; }

/* -- the vertical line between the topics and the TOC */
div.bodywrapper {
  margin: 0; }
  div.bodywrapper img {
    max-width: 100%; }

div.documentwrapper {
  background: #ffffff; }

div.body-wrap {
  width: 100%;
  background: #e8e8e8; }

div.body {
  margin: 0;
  padding: 0.5em 20px 20px 20px; }

div.related {
  font-size: 1em; }

/* -- the borders above and below the header and footer */
div.related ul {
  background-color: #ffffff;
  height: 1.8em;
  border-top: 4px solid #bdbdbd;
  border-bottom: 4px solid #50a9ca; }

div.related ul li {
  margin: 0;
  padding: 0;
  height: 2em;
  float: left; }

div.related ul li.right {
  float: right;
  margin-right: 5px; }

/* -- the Sphinx-specific navigation text located just below the header and just above the footer */
div.related ul li a {
  margin: 0;
  padding: 0 8px 0 5px;
  line-height: 1.75em;
  color: #355d71; }

div.related ul li a:hover {
  color: #db642a; }

div.sphinxsidebarwrapper {
  padding: 0; }

div.sphinxsidebarwrapper > ul > li {
  list-style-type: none; }

div.sphinxsidebar {
  display: none;
  position: relative;
  z-index: 1;
  margin: 0;
  padding: 0.5em 15px 15px 0;
  width: 250px;
  float: right;
  font-size: 1em;
  text-align: left; }

/* -- The colors of the TOC sidebar; h3 is headers, a is the text links and both should be same color */
div.sphinxsidebar h3, div.sphinxsidebar h4 {
  margin: 1em 0 0.5em 0;
  font-size: 1em;
  font-weight: bold;
  padding: 0.1em 0 0.1em 0.5em;
  color: #ffffff;
  background-color: #424242; }

div.sphinxsidebar a {
  padding: 0.1em 0 0.1em 0.5em;
  text-decoration: none; }

div.sphinxsidebar h3 a {
  color: #ffffff; }

div.sphinxsidebar ul {
  padding-left: 1.5em;
  padding: 0;
  line-height: 130%;
  list-style-type: disc; }

div.sphinxsidebar ul ul {
  margin-left: 20px; }

/*
 * footer
 * ~~~~~~~~~~~~~~~~~~~
 *
 */
div.footer {
  background-color: #355d71;
  color: #ffffff;
  padding: 10px 20px 10px 20px;
  clear: both;
  font-size: 0.8em;
  text-align: left; }

/* -- body styles ----------------------------------------------------------- */
p {
  margin: 0 0 0.5em 0; }

.documentwrapper img {
  margin: 0.5em; }

.literal {
  font-family: 'Consolas', 'Deja Vu Sans Mono', 'Bitstream Vera Sans Mono', monospace;
  font-size: 14px;
  letter-spacing: 0.015em;
  line-height: 100%;
  padding: 0.0em;
  border: 0px solid #bdbdbd;
  border-bottom: 1px solid #bdbdbd;
  background-color: #fdfdfd;
  margin: 0 0 0.5em 0; }

a {
  color: #355d71;
  text-decoration: none; }

a:hover {
  color: #db642a;
  text-decoration: none; }

dd {
  margin-top: 3px;
  margin-bottom: 10px;
  margin-left: 30px; }

dl {
  margin-bottom: 15px; }

strong {
  font-weight: bold; }

div.body a {
  text-decoration: underline; }

div.body a:hover {
  color: #db642a;
  text-decoration: underline; }

em {
  font-style: italic; }

/* -- header styles, basically colored underlines and decreasing border bottom sizes */
/* -- h1 is the topic title and the rest of the headers go from there */
.versive-docs h1 {
  padding: 0.4em 0 0.15em 0;
  font-size: 1.85em;
  font-weight: bold;
  color: #355d71;
  border-bottom: solid 5px #757575;
  margin: 0 0 0.5em 0; }

.versive-docs h2 {
  padding: 0.9em 0 0.15em 0;
  font-size: 1.65em;
  font-weight: bold;
  color: #355d71;
  border-bottom: solid 4px #757575;
  margin: 0 0 0.5em 0; }

.versive-docs h3 {
  padding: 0.9em 0 0.15em 0;
  font-size: 1.45em;
  font-weight: bold;
  color: #355d71;
  border-bottom: solid 1px #757575;
  margin: 0 0 0.5em 0; }

.versive-docs h4 {
  padding: 0.7em 0 0.15em 0;
  font-size: 1.25em;
  font-weight: normal;
  color: #355d71;
  border-bottom: solid 0.5px #757575;
  margin: 0 0 0.5em 0; }

.versive-docs h5 {
  padding: 0.7em 0 0.15em 0;
  font-size: 1.15em;
  font-weight: normal;
  color: #355d71;
  border-bottom: solid 0.2px #757575;
  margin: 0 0 0.5em 0; }

div.body h1 a, div.body h2 a, div.body h3 a, div.body h4 a, div.body h5 a, div.body h6 a {
  color: #ffffff !important; }

h1 a.anchor, h2 a.anchor, h3 a.anchor, h4 a.anchor, h5 a.anchor, h6 a.anchor {
  display: none;
  margin: 0 0 0 0.3em;
  padding: 0 0.2em 0 0.2em;
  color: #ffffff !important; }

h1:hover a.anchor, h2:hover a.anchor, h3:hover a.anchor, h4:hover a.anchor,
h5:hover a.anchor, h6:hover a.anchor {
  display: inline; }

h1 a.anchor:hover, h2 a.anchor:hover, h3 a.anchor:hover, h4 a.anchor:hover,
h5 a.anchor:hover, h6 a.anchor:hover {
  color: #ffffff;
  background-color: #ffffff; }

a.headerlink {
  color: #ffffff !important;
  font-size: 1em;
  margin-left: 6px;
  padding: 0 4px 0 4px;
  text-decoration: none !important; }

a.headerlink:hover {
  background-color: #ffffff;
  color: #ffffff !important; }

cite, code, tt {
  font-family: 'Consolas', 'Deja Vu Sans Mono', 'Bitstream Vera Sans Mono', monospace;
  font-size: 0.95em;
  letter-spacing: 0.01em; }

tt {
  background-color: #e3e3e3;
  border-bottom: 1px solid #bdbdbd;
  color: #355d71; }

tt.descname, tt.descclassname, tt.xref {
  border: 0; }

hr {
  border: 1px solid #355d71;
  margin: 2em; }

a tt {
  border: 0;
  color: #db642a; }

a tt:hover {
  color: #db642a; }

pre {
  font-family: 'Consolas', 'Deja Vu Sans Mono', 'Bitstream Vera Sans Mono', monospace;
  font-size: 0.95em;
  letter-spacing: 0.015em;
  line-height: 110%;
  padding: .5em;
  border: 2px solid #eeeeee;
  border-color: #eeeeee;
  background-color: #ffffff;
  margin: 0 0 0.5em 0; }

pre a {
  color: inherit;
  text-decoration: underline; }

td.linenos pre {
  padding: 0.5em 0; }

/* -- has $versive-red for testing */
div.quotebar {
  background-color: #db642a;
  max-width: 250px;
  float: right;
  padding: 2px 7px;
  border: 1px solid #db642a; }

/* -- has $versive-red for testing */
div.topic {
  background-color: #db642a; }

/* -- table styles */
div.section {
  max-width: 100%; }

table.docutils {
  max-width: 100%;
  border: 0;
  border-collapse: separate;
  border-spacing: 4px;
  margin: 0 0 0.5em 0; }

table.docutils tr {
  max-width: 100%; }

table.docutils th {
  padding: 1px 8px 1px 5px;
  border-top: 0;
  border-left: 0;
  border-right: 0;
  border-bottom: 1px solid #355d71;
  background-color: #355d71;
  color: #ffffff;
  font-weight: bold;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px; }

table.docutils td {
  padding: 5px 5px 5px 5px;
  border-top: 0;
  border-left: 0;
  border-right: 0;
  border-bottom: 1px dashed #355d71; }

table.docutils td, table.docutils th {
  white-space: normal; }

table.field-list td, table.field-list th {
  border: 0 !important; }

table.footnote td, table.footnote th {
  border: 0 !important; }

th {
  text-align: left;
  padding-right: 5px; }

table.citation {
  border-left: solid 1px gray;
  margin-left: 1px; }

table.citation td {
  border-bottom: none; }

div.admonition, div.warning {
  font-size: 0.9em;
  margin: 1em 0 1em 0;
  border: 1px solid #93c94c;
  background-color: #ffffff;
  padding: 0; }

div.admonition p, div.warning p {
  margin: 0.5em 1em 0.5em 1em;
  padding: 0; }

div.admonition pre, div.warning pre {
  margin: 0.4em 1em 0.4em 1em; }

div.admonition p.admonition-title,
div.warning p.admonition-title {
  margin: 0;
  padding: 0.1em 0 0.1em 0.5em;
  color: #ffffff;
  border-bottom: 0px solid #93c94c;
  font-weight: bold;
  background-color: #93c94c; }

div.warning {
  border: 1px solid #db642a; }

div.warning p.admonition-title {
  background-color: #db642a;
  border-bottom-color: #db642a; }

div.admonition ul, div.admonition ol,
div.warning ul, div.warning ol {
  margin: 0.1em 0.5em 0.5em 3em;
  padding: 0; }

div.versioninfo {
  margin: 1em 0 0 0;
  border: 1px solid #ffffff;
  background-color: #ffffff;
  padding: 8px;
  line-height: 1.3em;
  font-size: 0.9em; }

.viewcode-back {
  font-family: 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; }

/* -- styles for code-block differentiation by color, top-right title */
div.viewcode-block:target {
  background-color: #eed973;
  border-top: 1px solid #eeeeee;
  border-bottom: 1px solid #eeeeee; }

.highlight-none, .highlight-bash, .highlight-console, .highlight-javascript, .highlight-python, .highlight-yaml, .highlight-sql, .highlight-text {
  position: relative; }

.highlight-none:before, .highlight-bash:before, .highlight-console:before, .highlight-javascript:before, .highlight-python:before, .highlight-yaml:before, .highlight-sql:before, .highlight-text:before {
  z-index: 10;
  font-size: 9px;
  padding: .2em .6em;
  text-align: center;
  color: #424242;
  display: block;
  position: absolute;
  border-radius: 0 3px 0 3px;
  border-top: none;
  border-right: none;
  background-color: #eeeeee;
  top: 0;
  right: 0;
  height: 12px; }

.highlight-none pre {
  border-color: #ffffff; }

.highlight-none pre {
  background-color: #e3e3e3; }

.highlight-bash pre {
  border-color: #e3e3e3; }

.highlight-console pre {
  border-color: #78b2b6; }

.highlight-javascript pre {
  border-color: #eed973; }

.highlight-python pre {
  border-color: #355d71; }

.highlight-yaml pre {
  border-color: #50a9ca; }

.highlight-sql pre {
  border-color: #b27113; }

.highlight-text pre {
  border-color: #424242; }

.highlight-bash:before {
  content: 'SCRIPT';
  background-color: #e3e3e3;
  color: #424242; }

.highlight-console:before {
  content: 'SHELL';
  background-color: #78b2b6;
  color: #ffffff; }

.highlight-javascript:before {
  content: 'JSON';
  background-color: #eed973;
  color: #424242; }

.highlight-python:before {
  content: 'PYTHON';
  background-color: #355d71;
  color: #ffffff; }

.highlight-yaml:before {
  content: 'YAML';
  background-color: #50a9ca;
  color: #ffffff; }

.highlight-sql:before {
  content: 'TABLE';
  background-color: #b27113;
  color: #ffffff; }

.highlight-text:before {
  content: 'CONFIG FILE';
  background-color: #424242;
  color: #ffffff; }

.downloads .nav-main {
  margin-bottom: 20px; }

nav.nav-images {
  width: 100%;
  min-height: 500px;
  background: #ffffff;
  border-bottom: #ffffff; }
  nav.nav-images h1 {
    margin: 0;
    padding: 20px 0 0 20px;
    text-transform: uppercase;
    font-family: "Roboto", "Helvetica Neue", "Helvetica", Arial, sans-serif;
    font-size: 20px;
    font-weight: 700;
    line-height: 1;
    color: #355d71; }
    nav.nav-images h1 span {
      color: #ffffff; }

nav.nav-learn-versive {
  width: 100%;
  min-height: 60px;
  background: #78b2b6; }
  nav.nav-learn-versive h1 {
    margin: 0;
    padding: 20px 0 0 20px;
    text-transform: uppercase;
    font-family: "Roboto", "Helvetica Neue", "Helvetica", Arial, sans-serif;
    font-size: 20px;
    font-weight: 700;
    line-height: 1;
    color: #50a9ca; }
    nav.nav-learn-versive h1 span {
      color: #ffffff; }

nav.nav-pdf-title {
  width: 100%;
  min-height: 160px;
  background: #355d71;
  border-bottom: #ffffff; }
  nav.nav-pdf-title h1 {
    margin: 0;
    padding: 20px 0 0 20px;
    text-transform: uppercase;
    font-family: "Roboto", "Helvetica Neue", "Helvetica", Arial, sans-serif;
    font-size: 32px;
    font-weight: 700;
    line-height: 1;
    color: #50a9ca; }
    nav.nav-pdf-title h1 span {
      color: #ffffff; }

nav.nav-footer {
  width: 100%;
  min-height: 52px;
  background: #ffffff; }
  nav.nav-footer h1 {
    margin: 0;
    padding: 20px 0 0 20px;
    text-transform: none;
    font-family: "Roboto", "Helvetica Neue", "Helvetica", Arial, sans-serif;
    font-size: 12px;
    font-weight: 700;
    line-height: 1;
    color: #50a9ca; }
    nav.nav-footer h1 span {
      color: #355d71; }

nav.nav-spacer {
  width: 100%;
  min-height: 26px;
  background: #ffffff;
  border-bottom: #ffffff; }

.nav-inner {
  max-width: 1295px;
  margin: 0 auto;
  position: relative; }

.nav-links {
  margin: 0;
  clear: both;
  padding: 10px 17px;
  color: #757575;
  background: #ffffff;
  line-height: 2;
  font-family: "Roboto", "Helvetica Neue", "Helvetica", Arial, sans-serif;
  font-weight: 700;
  font-size: 15px;
  text-transform: uppercase; }
  .nav-links li {
    display: block;
    clear: both; }

#coverbg {
  position: absolute;
  top: 0px;
  right: 0px;
  width: 100%;
  height: 500px; }
  #coverbg img {
    width: 100%;
    height: 100%; }

#logo {
  position: absolute;
  top: 20px;
  left: 20px;
  width: 132px;
  height: 30px; }
  #logo img {
    width: 100%;
    height: 100%; }

.left {
  float: left; }

/*# sourceMappingURL=versive.css_t.map */
