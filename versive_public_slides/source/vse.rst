================================================
Versive Security Engine
================================================


.. revealjs::

 .. revealjs:: Versive Security Engine
    :data-transition: none

    .. image:: ../../images/vse_00.svg

    The |vse| analyzes the flow of data across a network, and then identifies anomalies within that data that correlate to known and expected behaviors that often occur when adversaries attempt to move data outside of the network.

 .. revealjs:: Versive Security Engine
    :data-transition: none

    .. image:: ../../images/vse_00a.svg

    #. Loads data from data sources
    #. Processes data from many sources into standardized datasets for machine analysis
    #. Models data to discover outliers, and then makes predictions
    #. Builds results for human analysis
    #. Learns and applies improved tuning for better machine analysis



 .. revealjs:: Threat Viewer (Web User Interface)
    :data-transition: none

    .. image:: ../../images/vse_00b.svg

    #. View the network traffic relationships by individual suspicious host in a threat case
    #. View the campaign score and individual stage scores
    #. View the traffic timeline for this suspicious host
    #. View traffic details for bytes in and out of the network for this suspicious host
    #. View the 30-day traffic timeline for all suspicious hosts in the threat case



 .. revealjs:: Data Sources, Data Lake
    :data-transition: none

    .. image:: ../../images/vse_00c.svg

    #. As much data as possible from as many sources as possible: proxy data, DNS data, flow data, and from 3rd-party data sources, such as Gigamon, BlueCoat, Cisco, and Illumio
    #. Uploaded to the |vse| on a daily basis.


 .. revealjs:: How does it work?
    :data-transition: none

    The following slides step through a high-level diagram that shows how the Versive Security Engine uses data in the data lake, processes that data, creates models and predictions, builds results, and then displays those results (as threat cases) in the Web user interface.

 .. revealjs:: Data Lake
    :noheading:
    :data-transition: none

    .. image:: ../../images/vse_01.svg

    #. A data lake stores raw proxy, DNS, and flow data


 .. revealjs:: Process Data
    :noheading:
    :data-transition: none

    .. image:: ../../images/vse_02.svg

    #. A data lake stores raw proxy, DNS, and flow data
    #. The security engine picks up data on a daily basis, after which it's processed and modeled


 .. revealjs:: Model Data
    :noheading:
    :data-transition: none

    .. image:: ../../images/vse_03.svg

    #. A data lake stores raw proxy, DNS, and flow data
    #. The security engine picks up data on a daily basis, after which it's processed and modeled
    #. Interesting data points are discovered


 .. revealjs:: Generate Threat Cases
    :noheading:
    :data-transition: none

    .. image:: ../../images/vse_04.svg

    #. A data lake stores raw proxy, DNS, and flow data
    #. The security engine picks up data on a daily basis, after which it's processed and modeled
    #. Interesting data points are discovered; threat cases are generated



 .. revealjs:: Review Interesting Threat Case
    :noheading:
    :data-transition: none

    .. image:: ../../images/vse_05.svg

    #. A data lake stores raw proxy, DNS, and flow data
    #. The security engine picks up data on a daily basis, after which it's processed and modeled
    #. Interesting data points are discovered; threat cases are generated
    #. The most interesting threat cases are fed to the threat viewer for human analysis; this threat case has three suspicious hosts


 .. revealjs:: Review: Suspicious Host #1
    :noheading:
    :data-transition: none

    .. image:: ../../images/vse_06.svg

    #. A data lake stores raw proxy, DNS, and flow data
    #. The security engine picks up data on a daily basis, after which it's processed and modeled
    #. Interesting data points are discovered; threat cases are generated
    #. The most interesting threat cases are fed to the threat viewer for human analysis; this threat case has three suspicious hosts
    #. Suspicious host #1


 .. revealjs:: Review: Suspicious Host #2
    :noheading:
    :data-transition: none

    .. image:: ../../images/vse_07.svg

    #. A data lake stores raw proxy, DNS, and flow data
    #. The security engine picks up data on a daily basis, after which it's processed and modeled
    #. Interesting data points are discovered; threat cases are generated
    #. The most interesting threat cases are fed to the threat viewer for human analysis; this threat case has three suspicious hosts
    #. Suspicious host #2


 .. revealjs:: Review: Suspicious Host #3
    :noheading:
    :data-transition: none

    .. image:: ../../images/vse_08.svg

    #. A data lake stores raw proxy, DNS, and flow data
    #. The security engine picks up data on a daily basis, after which it's processed and modeled
    #. Interesting data points are discovered; threat cases are generated
    #. The most interesting threat cases are fed to the threat viewer for human analysis; this threat case has three suspicious hosts
    #. Suspicious host #3


