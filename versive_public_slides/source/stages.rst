================================================
APT Campaign Stages
================================================


.. revealjs::

 .. revealjs:: APT Campaign Stages
    :data-transition: none

    .. include:: ../../includes/stage_overview.rst

 .. revealjs:: Stage: Planning
    :data-transition: none

    .. image:: ../../images/stages_planning.svg

    .. include:: ../../includes_terms/term_campaign_stage_planning.rst

 .. revealjs:: Stage: Access
    :data-transition: none

    .. image:: ../../images/stages_access.svg

    .. include:: ../../includes_terms/term_campaign_stage_access.rst


 .. revealjs:: Stage: Reconnaissance
    :data-transition: none

    .. image:: ../../images/stages_recon.svg

    .. include:: ../../includes_terms/term_campaign_stage_recon.rst


 .. revealjs:: Stage: Data Collection
    :data-transition: none

    .. image:: ../../images/stages_collection.svg

    .. include:: ../../includes_terms/term_campaign_stage_collection.rst


 .. revealjs:: Stage: Data Exfiltration
    :data-transition: none

    .. image:: ../../images/stages_exfiltration.svg

    .. include:: ../../includes_terms/term_campaign_stage_exfiltration.rst


 .. revealjs:: Recurring Stages
    :data-transition: none

    .. image:: ../../images/stages_all.svg

    .. include:: ../../includes_terms/term_campaign_stages_ongoing.rst
