.. 
.. xxxxx
.. 

==================================================
Site Map
==================================================

.. TODO: Remove the following before publishing to public-facing docs; replace with a proper intro

.. warning:: The support content here is built around the idea of "what if we leveraged our existing reference and how-to content to provide support-focused content" ... right now, this content is 100% sourced from http://docs/. Over time, that % would go down, as this is just showing how it would be maintained. But the point is that we can build this type of content. It's a few extra steps to maintain, but reusing content is much less expensive than creating it from scratch. It's also better than just copying and pasting it from elsewhere.

.. TODO: Remove the previous before publishing to public-facing docs; replace with a proper intro

**Apache Spark**: :ref:`runbook-apache-spark-multiple-sparkcontexts`

**Compute Cluster**: :ref:`runbook-compute-cluster-batch-script-failure` | :ref:`runbook-compute-cluster-rdd-sprockets`

**cpio Utility**: :ref:`runbook-cpio-utility-cannot-run` | :ref:`runbook-cpio-utility-cannot-write-output` | :ref:`runbook-cpio-utility-corrupt-file`

**Engine**: :ref:`runbook-engine-bad-command-line-statement` | :ref:`runbook-engine-existing-sparkcontexts`

**Error Messages** :ref:`Generic or system errors <error-messages-generic-or-system>` | :ref:`Argument validation errors <error-messages-argument-validation>` | :ref:`Load errors <error-messages-load>` | :ref:`Save and checkpoint errors <error-messages-save-and-checkpoint>` | :ref:`Table split, join, and merge errors <error-messages-table-split-join-merge>` | :ref:`Transform errors <error-messages-transform>` | :ref:`Model learning errors <error-messages-model-learning>` | :ref:`Prediction and evaluation errors <error-messages-prediction-and-evaluation>`

**Installer**: :ref:`runbook-installer-cr-json-file-issues` | :ref:`runbook-installer-directory-contents-changed` | :ref:`runbook-installer-error-executing-task` | :ref:`runbook-installer-question-subsystem-errors` | :ref:`runbook-installer-unexpected-file` | :ref:`runbook-installer-unreadable-directory`

**Modeling**: :ref:`runbook-modeling-binary-classification-failure` | :ref:`runbook-modeling-cannot-join-data-tables` | :ref:`runbook-modeling-cannot-locate-data-file` | :ref:`runbook-modeling-cannot-save-csv-excel` | :ref:`runbook-modeling-column-header-values` | :ref:`runbook-modeling-empty-columns` | :ref:`runbook-modeling-out-of-memory` | :ref:`runbook-modeling-slow-computations` | :ref:`runbook-modeling-slow-reporting`

**Network Access**: :ref:`runbook-network-access-interrupted` | :ref:`runbook-network-access-cannot-connect-to-node`

**Python**: :ref:`runbook-python-function-returns-bad-string` | :ref:`runbook-python-invalid-argument-type` | :ref:`runbook-python-lists-and-tuples` | :ref:`runbook-python-missing-exceptions` | :ref:`runbook-python-object-not-defined`

**User Access**: :ref:`runbook-user-access-authentication-error` | :ref:`runbook-network-access-incorrect-sudo`


.. Hide the TOC from this file.

.. toctree::
   :hidden:

   api
   error_messages
   glossary
   overview
   python
   runbook
