.. 
.. versive, public, security engine
.. 

==================================================
Threat Viewer
==================================================

.. TODO: Do not copy this from the internal page for the deploy_webui.rst file.

The web UI for the |vse|---sometimes referred to as the threat viewer---shows the results of the daily modeling runs for individual threat cases, campaign stages, and findings specific to each campaign stage, over a 30-day rolling window.

The web UI is installed and configured by the |versive| solutions engineering team, including the steps required to associate the modeling strategies with how the threat cases are shown in the web UI. If you have questions about the web UI---how the web UI works, how it's configured, and how the web UI relates to various aspects of modeling configuration, etc.---please discuss them with a member of the |versive| solutions engineering team.
