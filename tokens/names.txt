.. true tokens for feature/product names

.. |versive| replace:: Versive
.. |vse| replace:: Versive Security Engine

.. |risp| replace:: RISP API
.. |risp_full| replace:: Reduced Instruction Set Platform (RISP) API

.. |stage_plan| replace:: planning
.. |stage_access| replace:: access
.. |stage_recon| replace:: reconnaissance
.. |stage_collect| replace:: data collection
.. |stage_exfil| replace:: data exfiltration

.. |rst| replace:: reStructuredText
