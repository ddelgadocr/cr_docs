.. 
.. xxxxx
.. 

The extensions package is a Python package that contains the custom code needed for a particular deployment of the engine. This topic describes the extension package setup, the location of all custom function settings in the configuration file, and then provides examples of user-defined functions.
