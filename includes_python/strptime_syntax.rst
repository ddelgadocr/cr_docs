.. 
.. https://docs.python.org/3/library/datetime.html#module-datetime
.. 
.. The contents of this file are included in the following topics:
.. 
..    api_risp
..    configure
.. 

Strptime syntax is used to create strings composed of several IDs that denote the various fields of the date and time and any separators that may be included.

The following table describes the available identifiers:

.. list-table::
   :widths: 200 200 200
   :header-rows: 1

   * - ID
     - Description
     - Examples
   * - **%a**
     - Weekday as locale's abbreviated name.
     - Sun, Mon, ...
   * - **%A**
     - Weekday as locale's full name.
     - Sunday, ...
   * - **%w**
     - Weekday as a decimal number.
     - 0 (Sunday) .. 6
   * - **%d**
     - Day of the month as a zero-padded decimal number.
     - 01, 02, ..., 31
   * - **%b**
     - Month as locale's abbreviated name.
     - Jan, Feb, ...
   * - **%B**
     - Month as locale's full name.
     - January, ...
   * - **%m**
     - Month as a zero-padded decimal number.
     - 01, 02, ..., 12
   * - **%y**
     - Year without century as a zero-padded decimal number.
     - 00, 01, ..., 99
   * - **%Y**
     - Year with century as a decimal number.
     - 1970, 1988, 2013
   * - **%H**
     - Hour (24-hour clock) as a zero-padded decimal number.
     - 00, 01, ..., 23
   * - **%I**
     - Hour (12-hour clock) as a zero-padded decimal number.
     - 01, 02, ..., 12
   * - **%p**
     - Locale's equivalent of either AM or PM
     - AM, PM
   * - **%M**
     - Minute as a zero-padded decimal number
     - 00, 01, ..., 59
   * - **%S**
     - Second as a zero-padded decimal number
     - 00, 01, ..., 59
   * - **%f**
     - Microsecond as a decimal number, zero-padded on the left.
     - 000000, ...
   * - **%z**
     - The UTC offset in the form +HHMM or -HHMM.
     - +0000, -0400
   * - **%Z**
     - The time zone name.
     - UTC, EST, CST
   * - **%j**
     - Day of the year as a zero-padded decimal number.
     - 001, 002, ..., 366
   * - **%U**
     - Week number of the year (Sunday starts a week) as a zero-padded decimal number. All days in a new year preceding the first Sunday are considered to be in week 0.
     - 00, 01, ..., 53
   * - **%W**
     - Week number of the year (Monday starts a week) as a decimal number. All days in a new year preceding the first Monday are considered to be in week 0.
     - 00, 01, ..., 53
   * - **%c**
     - A locale's appropriate date and time representation.
     - Tue Aug 16
       21:30:00 1988
   * - **%x**
     - A locale's appropriate date representation.
     - 08/16/1988
   * - **%X**
     - A locale's appropriate time representation.
     - 21:30:00
   * - **%%**
     - A literal "%" character.
     - %
