.. 
.. xxxxx
.. 

Tables and models should be saved often to local storage or to a location on the storage cluster.

.. note:: Checkpointed computations held in-memory are not saved; however, the state of the model will be refreshed, especially for expensive computations associated with model metrics, learning curves, and scoring.

The following functions are available:

* .. include:: ../../includes_api/function_save_crtable.rst
* .. include:: ../../includes_api/function_save_csv.rst
* .. include:: ../../includes_api/function_save_model.rst
* .. include:: ../../includes_api/function_save_xls.rst
