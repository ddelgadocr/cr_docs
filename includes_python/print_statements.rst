.. 
.. xxxxx
.. 

When running a script, it can be difficult to identify the location at which a problem is occuring, especially when a user-defined function is used to join tables. The following functions should use ``print`` statements to verify data is being correctly removed, added, and modified:

* .. include:: ../../includes_api/function_count_rows.rst
* .. include:: ../../includes_api/function_get_column_names.rst
* .. include:: ../../includes_api/function_summarize_table.rst
