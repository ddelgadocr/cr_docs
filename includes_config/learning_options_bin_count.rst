.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

The number of bins to use. Use zero for no binning. Use ``None`` for default binning. Default value: ``None``.
