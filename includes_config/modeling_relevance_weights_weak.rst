.. 
.. this topic documents how to use a configuration setting
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
..    model_data
.. 

A weak measurement target can be useful for monitoring campaign behaviors, especially if multiple weak measurement targets are present for the same anomaly, but is limited due to indirect observation or by having a large amount of benign anomalies.
