.. 
..    xxxxx
.. 


Local and global thresholds for filtered connectivity graphs are specified as extra arguments in the :ref:`configure-entity-attribute-sources` section of the configuration file. For example:

.. code-block:: yaml

   graph_filter_threshold_global: 300
   graph_filter_threshold_local: 3

Only the highest weighted edges in the graph are kept (as specified by ``graph_filter_threshold_global``), and then within that set of edges, only a small number of edges are kept (as specified by ``graph_filter_threshold_local``).
