.. 
.. The contents of this file are included in the following topics:
.. 
..    configure, model_data
.. 

The ``comp_cdf`` modeling strategy generates a surprise score using a `cumulative distribution function (CDF) <https://en.wikipedia.org/wiki/Cumulative_distribution_function>`__. This strategy:

* Assumes that large target values are interesting
* Computes the probability of seeing a value as extreme (or more extreme) as the observed target value
* Assumes that the larger the target value, the lower the probability
* Derives a raw score based on the difference between the actual (observed) target value and the predicted target value
* Generates a surprise score that is returned in the range ``[0,1]``. This is used later for :ref:`risk scoring <model-data-risk-scores>`.

.. note:: If a target has a negative value, its score is likely to be very small and therefore uninteresting.
