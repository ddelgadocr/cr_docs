.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

The type of modeling table. Possible values: ``COUNT_REGRESSION``, ``LINEAR_REGRESSION``, and ``REGRESSION``. Default value: ``LINEAR_REGRESSION``.
