.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

The relative speed vs. quality tradeoff. Use this setting to provide high-level guidance for the techniques the system should use to either reduce model generation time or to take additional time to explore alternative settings, which may result in a higher quality model. Possible values: ``0``, ``10``, ``20``, or ``50``. Default value: ``None``.

Use ``0`` to return a baseline model (predicting target mean).

Use ``10`` to produce the best quality available with a predictable, quick training time.

Use ``20`` to allow the system some additional time to refind the model.

Use ``50`` to allow the system significantly more time to refine the model.

.. note:: Use the ``explorations`` setting **OR** the ``time_tradeoff`` setting--but not both settings--within the learning options settings group.
