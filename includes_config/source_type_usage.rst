.. 
.. this topic documents a common description of the usage code-block that is common
.. to all of the sub-groups for the sources attribute
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

where ``<source_type>`` represents data that is obtained from one of the following well-defined source types:

* ``dns``: DNS data that maps hostnames to IP addresses
* ``proxy``: Proxy data that tracks the flow of data into and out of the network
* The name of an entity attribute table that defines custom, static data used to add more context to the other four well-defined source types

.. 
.. removing the following (for now)
.. 
.. * ``authn``: Authentication events, such as login successes and failures
.. * ``dhcp``: DHCP data that maps proxy logs, IP addresses, and timestamps to users and hostnames 	
.. 