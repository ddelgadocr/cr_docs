.. 
.. The contents of this file are included in the following topics:
.. 
..    configure, model_data
.. 

The name of the strategy used for modeling. Possible values: ``comp_cdf`` and ``top_k_entities``. Default value: ``comp_cdf``.
