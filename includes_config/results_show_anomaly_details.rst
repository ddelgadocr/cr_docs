.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    customization_package
.. 

When **show_anomaly_details** is ``True``, anomaly details for individual measurement models are shown in the results:

.. code-block:: none

   <target>=<actual> (expected <predicted> because <reason list>) NLL=<score>

When ``False``, only comparisons of predicted vs. expected values are shown. For example:

.. code-block:: none

   <target>=<actual> (expected <predicted>) NLL=<score>

Results contain the following details:

**<target>**
   The measurement column name.

**<actual>**
   The measured value.

**<predicted>**
   The value predicted by the model in predicted_<target>.

**<score>**
   The negative log-likelihood (NLL) value in nll_<target>.

**<reason 1>**, **<reason 2>**, **<reason ...>**
   Reasons are listed in order, starting with the strongest. Each reason takes the following format:

   .. code-block:: none

      <input column> is <input value> [influence%]

   Influence shows the relative strength and direction of the reason. Positive influences push the prediction upwards, and negative influences push the prediction downward.


For example:

.. code-block:: python

   proxyweb_bytes_out_sum=500M (expected 25M because IS_WEEKEND is False [-50%];
                                percentile_proxyweb_bytes_out_sum_7_day is 100M [+25%];
                                DEPARTMENT is HR [-25%])
                                NLL=12345

