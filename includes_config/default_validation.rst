.. 
.. this topic documents configuration settings for the config.yml file
.. settings are defined at /crepes/apt/apt/config.py
.. settings specific to this topic are located in the ValidationConfig class
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

.. some of the existing doc had this:
.. 
.. ``default_validation`` (bool, optional)
..    Use to configure validations for ``authn``, ``dhcp``, ``dns``, ```proxy``, or an entity attribute table to be run at the end of the
..    :doc:`cmd_parse` phase. Set to ``False`` to disable validation. Default value: ``True``.
.. 
.. Not sure that's 100% true due to CR-9055
.. 


.. TODO: Need to check these against the data_source_foo.py files and verify the actual validations list.

**default_validation** (bool, optional)
   Specifies if default validation settings are added for DNS, flow, and proxy validations. Default value: ``True``.

   When ``True``, the following validations are added for DNS:

   .. code-block:: yaml

      validation:
        dest_ip:
          missing:
            - percent: 10.0
        source_ip:
          missing:
            - percent: 10.0
        timestamp:
          missing:
            - percent: 1.0

   When ``True``, the following validations are added for flow:

   .. code-block:: yaml

      validation:
        bytes_in:
          missing:
            - percent: 1.0
        bytes_out:
          missing:
            - percent: 1.0
        dest_ip:
          missing:
            - percent: 1.0
        dest_port:
          missing:
            - percent: 1.0
        protocol:
          missing:
            - percent: 1.0
        source_ip:
          missing:
            - percent: 1.0
        source_port:
          missing:
            - percent: 1.0
        timestamp:
          missing:
            - percent: 1.0
        user:
          missing:
            - percent: 1.0

   When ``True``, the following validations are added for proxy:

   .. code-block:: yaml

      validation:
        referer_uri:
          missing:
            - percent: 50.0
        source_ip:
          missing:
            - percent: 10.0
        timestamp:
          missing:
            - percent: 1.0


.. TODO: removed DHCP for now:

.. 
..    When ``True``, the following validations are added for DHCP:
.. 
..    .. code-block:: yaml
.. 
..       validation:
..         hostname:
..           missing:
..             - percent: 10.0
..         source_ip:
..           missing:
..             - percent: 10.0
..         timestamp:
..           missing:
..             - percent: 1.0
.. 
