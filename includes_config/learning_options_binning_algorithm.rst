.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

The binning algorithm for the modeling table. Possible values: ``None``, ``quantiles`` and ``tree``. Default value: ``None``.
