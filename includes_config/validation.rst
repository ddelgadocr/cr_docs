.. 
.. this topic documents configuration settings for the config.yml file
.. settings are defined at /crepes/apt/apt/config.py
.. settings specific to this topic are located in the ValidationConfig class
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

Use the **validation** section to add custom validations to the end of the phase.

.. 
.. 
.. For each validation output is returned that states whether the validation succeeded and a report of the results.
.. 
.. Does this mean that it's "recommended to test each column with just missing or regex?"
.. Or does this mean that missing+regex is also good? More small tests?
.. 
..     """Parses validation configuration and validates the table against
..     the configuration. Returns the output from validate_table() that
..     for each validation gives whether it succeeded and a report.
..     """
.. 


**validation**
   The parent setting group for the **validation** section. For each ``<column>``, additional validations to perform.

   **missing** (optional)
      Use an integer or a percent to check the named column for missing values, and then specify the maximum number of missing values that may occur before an error is raised. The maximum number may be expressed as a count, a percentage, or both.

      **count** (int, optional)
         An integer that specifies the maximum number of values that may fail before an error is raised.

      **percent** (float, optional)
         A value between ``0`` and ``100`` that specifies the percentage of values that may fail before an error is raised.

   **regex** (optional)
      Use a regular expression to parse the named column for validation, and then specify the maximum number of validations that may fail before an error is raised. The maximum number may be expressed as a count, a percentage, or both. See ``:ref:`regex-debugger``` for more information about degugging regular expressions.

      **count** (int, optional)
         An integer that specifies the maximum number of values that may fail before an error is raised.

      **pattern** (str, required)
         A regular expression that is used to parse the named column for validation.

      **percent** (float, optional)
         A value between ``0`` and ``100`` that specifies the percentage of values that may fail before an error is raised.
