.. 
.. this topic documents configuration settings for the config.yml file
.. settings are defined at /crepes/apt/apt/config.py
.. settings specific to this topic are located in the EntityAttributeReference class
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    release_notes for VSE 1.2
.. 


A global whitelist of performance warnings by performance warning type: allowed, disallowed, and ignored. The applied order of priority for performance warnings is:

#. **disallow**
#. **ignore**
#. **allowed_only**

.. note:: Adding items to the performance warnings whitelist should be done sparingly. If the engine encounters an unknown performance warning, the run may return a message that begins with::

      Error 10000: Unknown exception. Warning treated as error: Performance warning [T_Identifier]:

   where ``T_Identifier`` represents the name of the warning. This identifier may be added to the lists of allowed, disallowed, or ignored performance warnings.

Performance warning lists may be of the following types:

**allowed_only** (list of str)
   A list of warnings to allow. By default, all warnings are allowed. If warnings are specified in this list, then any warning that is not specified in this list will throw an exception.

**disallow** (list of str)
   A list of warnings to disallow. By default, no warnings are disallowed.

**ignore** (list of str)
   A list of warnings to ignore. By default, no warnings are ignored. Warnings that are specified by this list are ignored when they are thrown as an exception by the engine.

For example:

.. code-block:: yaml

   performance_warnings:
     allowed_only: [ 'DCategorizeUDF', 'DDateParser', 'DFormatDatetime', 'TCIDRLookup',
                     'TReplaceListsByCountersUDF', 'TWindowCountDistinct',
                     'TWindowCrossTabUDF', 'TWindowSumUDF', 'T_PipelineMapper',
                     'T_StandardizeKeysUDF', 'Tmerge_assignments', 'Tpost_op' ]
     disallow: []
     ignore: [ 'T_PipelineMapper' ]
