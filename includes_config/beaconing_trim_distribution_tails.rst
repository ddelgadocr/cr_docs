.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
.. 

**trim_distribution_tails** (bool, optional)
   Specifies if the distribution tails of beaconing raw features are trimmed when computing single-day statistics. Default value: ``True``. If ``True``, see ``proportion_to_trim`` and ``trim_threshold``.
