.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
.. 

**proportion_to_trim** (float, optional)
   If ``trim_distribution_tails`` is ``True``, the proportion of the distribution to trim. Default value: ``0.05``.
