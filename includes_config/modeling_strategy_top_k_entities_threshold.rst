.. 
.. The contents of this file are included in the following topics:
.. 
..    configure, model_data
.. 

.. TODO: Sync up the "Top K" description here, in config_strategies_top_k_threshold and glossary.

When **strategy_name** is set to ``top_k_entities``, the number of entities that are in the top K. Default value: ``50`` (and must be at least ``1``).
