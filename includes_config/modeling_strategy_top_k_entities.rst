.. 
.. The contents of this file are included in the following topics:
.. 
..    configure, model_data
.. 

The ``top_k_entities`` modeling strategy specifies the value for "K", which defines the probability of one (or more) abnormal entities ("K") being discovered at the top of a ranked list of entities.
