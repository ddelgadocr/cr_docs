.. 
..    xxxxx
.. 


The maximum depth of the connectivity graph. A node that is beyond this value is considered unreachable and is not included in the connectivity graph. Default value: ``None`` ("no limit").
