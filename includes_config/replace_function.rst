.. 
.. there are 4 after_save_function topics with the same description, but different code samples
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 


**replace_function** (str, optional)
   A function that replaces the command that is normally run for this phase. The name of this function may be the name of a built-in replace function (``dummy_phase`` or ``dumb_standardize_phase``) or it may be the name of a user-defined function that is located in the :doc:`extension package </extensions>`.

   Use ``dummy_phase`` to create a table with a single row and single column.

   Use ``dumb_standardize_phase`` to input columns, and then output those same columns, effectively performing no standardization of data.

   A user-defined replace function must be in the form of the CommandContext object.
