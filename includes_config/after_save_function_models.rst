.. 
.. there are 4 after_save_function topics with the same description, but different code samples
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 


**after_save_functions** (list of strings, optional)
   A list of user-defined functions that are located in the :doc:`extension package </extensions>` to be run after phase output is saved. Use these functions to add arbitrary validation code, descriptives, output statistics, etc. Functions are executed in the same order specified by the list. If a function raises an error, the remaining functions in the list are not called.

   For example:

   .. code-block:: python

      after_save(context,
                 artifact_descriptor,
                 (target=model, target=model, ...))
