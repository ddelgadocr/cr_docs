.. 
..    xxxxx
.. 


The percentage of user and/or host overlap between two threat cases, at which they are merged together into a single threat case. For example, a threat case has three hosts (A, B, and C), with host A directly connected to hosts B and C. Another threat case has four hosts (B, C, D, and E), with D directly connected to B, C, and E. Both threat cases have more than 33% overlap between individual hosts and will be merged into a single threat case. Default value: ``33`` (thirty-three percent).

.. note:: Merging threat cases may create a threat case that has a host connectivity distance greater than ``connectivity_graph_max_depth``.
