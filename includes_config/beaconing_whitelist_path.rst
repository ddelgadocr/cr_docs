.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
.. 

**whitelist_path** (str, optional)
   The absolute path to the directory in which :ref:`the beaconing whitelist file <define-behaviors-beaconing-whitelist>` is located on the storage cluster. (The named connection to the storage cluster is specified by the ``default_connection`` setting.)

   .. warning:: The beaconing whitelist file **must** be located at the path and named storage connection or else the contents of the beaconing whitelist cannot be applied.
