.. 
..    xxxxx
.. 


Specifies if anomalies that occur on connected hosts are grouped into the same threat case. For example, if anomalous behavior is detected on hosts A and B, and A and B are connected to one another, then group those anomalies into a single threat case in the threat case list. If ``False``, anomalies from multiple hosts will not be grouped. Default value: ``True``.

.. note:: When set to ``True``, the ``use_netflow_data`` setting should also be set to ``True`` to enable the generation of multihost results.
