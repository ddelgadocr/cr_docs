.. 
.. this topic documents the command located at /crepes/apt/apt/validate_stage.py
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

**partitions** (list, optional)
   Use this setting group to configure partitioning behavior when running the engine with Apache Spark, which requires that data be re-partitioned to the number of workers prior to a save, and then to a total after load.

   **per_worker_after_load_partitions** (int, optional)
      The number of after load partitions. Set this or **total_after_load_partitions**, but not both.

   **per_worker_before_save_partitions** (int, optional)
      The number of before save partitions. Set this or **total_before_save_partitions**, but not both.

   **total_after_load_partitions** (int, optional)
      The number of after load partitions. Set this or **per_worker_after_load_partitions**, but not both.

   **total_before_save_partitions** (int, optional)
      The number of before save partitions. Set this or **per_worker_before_save_partitions**, but not both.
