.. 
.. this topic documents how to use a configuration setting
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    results


For column names like:

.. code-block:: python

   percentile_bytes_out_sum
   proxyweb_bytes_out_sum

assign a nickname to show:

.. code-block:: python

   "median daily bytes out per week"
   "total bytes sent by entity"

like this:

.. code-block:: yaml

   results:
     nicknames:
       percentile_bytes_out_sum: "median daily bytes out per week"
       proxyweb_bytes_out_sum: "total bytes sent by entity"
