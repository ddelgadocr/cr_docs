.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
.. 

**number_of_folds** (int, optional)
   The number of folds to use when partitioning data for training models. Default value: ``5``.

   .. warning:: This value must be at least ``3``.
