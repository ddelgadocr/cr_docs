.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
.. 

**trim_threshold** (int, optional)
   If ``trim_distribution_tails`` is ``True``, the number of data points that must be present before the distribution may be trimmed. Default value: ``10``.
