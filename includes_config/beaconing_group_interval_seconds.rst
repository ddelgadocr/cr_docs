.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
.. 

**grouping_interval_seconds** (int, optional)
   The interval (in seconds) at which ``host``/``url_host`` pairs are grouped in the proxy transform table. Default value: ``1``.
