.. 
.. this topic documents configuration settings for the config.yml file
.. settings are defined at /crepes/apt/apt/config.py
.. settings specific to this topic are located in the PhaseConfigNode class
.. 


**block_names** (list of str, optional)
   A list of block names that are specified under the :ref:`configure-block-graphs` section of the configuration file.
