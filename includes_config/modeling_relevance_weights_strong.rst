.. 
.. this topic documents how to use a configuration setting
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
..    model_data
.. 

A strong measurement target is highly relevant for monitoring campaign behaviors due to direct obervation of the types of data movement that are present in a successful campaign.
