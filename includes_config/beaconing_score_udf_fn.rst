.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
.. 

**score_udf_fn** (str, required)
   The name of a user-defined function that is located in the :doc:`extension package </extensions>` and specifies how the default beaconing score is to be computed.
