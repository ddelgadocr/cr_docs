.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

Specifies if interactions between all pairs of input columns are explored during model training. Default value: ``False``.

.. note:: Use the ``explorations`` setting **OR** the ``time_tradeoff`` setting--but not both settings--within the learning options settings group.
