.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
.. 

**whitelist-location** (str, optional)
   The absolute path to the directory in which the malicious domains whitelist file is located on the storage cluster. (The named connection to the storage cluster is defined by the ``default_connection`` setting.)

   .. note:: This value may be identical to the one specified by another malicious domains-specific setting: ``top_url_list``.
