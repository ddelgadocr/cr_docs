.. 
.. this topic documents the command located at /crepes/apt/apt/validate_stage.py
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

**measures** (list, required)
   A multi-unit table and the list of measures to be computed for that table.

**input_cols** (list, required)
   A list of column names for which multi-unit measures are calcuated.

**lookback_windows** (list, optional)
   A list of lookback windows over which the measure is computed. For each input column, the number of measures equal to the number of lookback windows is produced. Each lookback window may define ``lookback_duration``, ``lookback_step``, ``missing_values_policy``, and (when ``missing_values_policy`` is set to ``fill_custom``) ``custom_fill_type``, and ``custom_fill_value``:

   .. include:: ../../includes_config/multiday_lookback.rst

**normalized** (bool, optional)
   Specifies if normalized or unormalized values are reported. If ``False``, all values in the column with the ``_denom`` suffix are set to ``1``. Default value: ``True``.

**state_limit** (int, optional)
   When ``transform_name`` is set to ``multi_day_count_distinct``, use to limit the number of tracked items. When set to ``multi_day_cross_tab``, use to limit the number of tracked A-B values. Default value: ``1000``.

**transform_args** (dictionary, optional)
   A dictionary of extra arguments to be applied to the named transform.

   When ``transform_name`` is set to ``multi_day_cross_tab``, the following extra arguments must be specified:

   **A_name** (str)
      The friendly name in the cross-tab table for input column A.

   **B_name** (str)
      The friendly name in the cross-tab table for input column B.

   **separator** (str)
      The string that separates column A and column B values in ``input_col``.

   **stats** (str)
      A comma-separated list of one (or more) statistics: ``median_marginal_B``, ``max_marginal_B``, ``count_distinct_A``, ``count_distinct_B``, ``count_distinct_AxB``.

**transform_name** (enum, required)
   The transform to be performed against all columns specified by ``input_column`` and the previous day's state to obtain the multi-unit measure. Possible values: ``multi_day_count_distinct`` or ``multi_day_cross_tab``. Default value: ``multi_day_count_distinct``.

   Use ``multi_day_cross_tab`` to use the :ref:`process-data-multi-day-crosstab` transform for multi-day lookback windows.

   Use ``multi_day_count_distinct`` to use the :ref:`process-data-multi-day-count-distinct` transform for multi-day lookback windows.

**window_unit** (str, optional)
    The frequency (in days) for which a multi-unit table is generated. Default value: ``1d``.
 