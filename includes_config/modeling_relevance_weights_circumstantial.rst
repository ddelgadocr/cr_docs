.. 
.. this topic documents how to use a configuration setting
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
..    model_data
..

A circumstantial measurement target may be useful for adding depth to threat case details to improve understanding of the results; however, only strong and weak measurement targets are used to determine the entities for which scores are calculated.
