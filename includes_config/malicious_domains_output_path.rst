.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
.. 

**output_path** (str, optional)
   The absolute path to the directory in which the output for malicious domains scoring is to be generated. (The named connection to the storage cluster is defined by the ``default_connection`` setting.)

   Two CSV files are generated. One for the top 100 whitelist-filtered domains and one for the top 100 whitelist-filtered domains that are uncategorized.
