.. 
.. this topic documents the command located at /crepes/apt/apt/validate_stage.py
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

**custom_fill_type** (str, optional)
   When ``missing_values_policy`` is set to ``fill_custom``, the type of value to be applied.

**custom_fill_value** (str, optional)
   When ``missing_values_policy`` is set to ``fill_custom``, the value to be applied.

**lookback_duration** (str, required)
   The number of days in the lookback window, starting from the day defined by ``lookback_step``, and also the number of days retained in the state for multiday measurements. For example, if ``lookback_duration`` is ``3d`` the lookback window is three days. If the current date is June 20 and ``lookback_duration`` is ``3d`` and ``lookback_step`` is set to ``-1d``, the date on which the lookback window begins is June 19 and the lookback window includes June 17-19 (three days).

   .. note:: The duration of a lookback window can never be longer than the number of days for which there is data. For example, if ``lookback_duration`` is ``8d``, but there is only data for six days, the duration is effectively ``6d``.

**lookback_step** (str, required)
   The number of units (in days) to lookback from the current day and also the day from which multi-unit measures are calculated. For example: ``0d`` starts the lookback window from the current day and ``-1d`` starts the lookback window from the previous day.

**missing_values_policy** (enum, optional)
   The policy to be applied to all units of missing data in the lookback window. Possible values: ``fill_custom``, ``fill_default``, or ``fill_none``. Default value: ``fill_default``.

   Use ``fill_custom`` to apply a custom fill value and type. A custom fill value and type must be specified using ``custom_fill_type`` and ``custom_fill_value``. This value may not be specified when the ``count_distinct`` transform is used.

   Use ``fill_default`` to apply transform-specific defaults to any missing value.

   Use ``fill_none`` to assign ``None`` to any missing value, and then not count them,  effectively reducing the window length by the number of missing units.
 