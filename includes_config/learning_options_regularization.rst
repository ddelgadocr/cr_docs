.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 

The kind of model regularization to use during learning. This controls the complexity of the model and improves its robustness to noisy training data. Possible values: ``L1`` or ``L2``. Default value: ``L2``.

Use ``L1`` when the data contains a large number of features with a low signal-to-noise ratio.

Use ``L2`` for slightly more accurate models.
