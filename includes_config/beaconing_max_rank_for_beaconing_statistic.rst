.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    define_behaviors
.. 

**max_rank_for_beaconing_statistic** (int, optional)
   The number of values from each beaconing feature that are assigned ranks. Higher scores are more indicative of beaconing. Ranks are multiplied to get a final score. Ranks that fall outside this distance from a beaconing feature are assigned a rank of ``1``. This value must be greater than ``0``. Default value: ``5000``.
