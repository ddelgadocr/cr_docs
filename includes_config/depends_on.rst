.. 
.. this topic documents configuration settings for the config.yml file
.. settings are defined at /crepes/apt/apt/config.py
.. settings specific to this topic are located in the EntityAttributeDependency class
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 


**datasource** (str, required)
   The name of the data source, typically the name of an entity attribute table defined under **entity_attribute_sources**.

**daterange_end** (str, optional)
   Relative to **base_path**, the path to the directory that contains the raw data for the end of the date range. The end date is exclusive. If specified, **daterange_start** must also be specified.

**daterange_start** (str, optional)
   Relative to **base_path**, the path to the directory that contains the raw data for the start of the date range. The start date is inclusive. If specified, **daterange_end** must also be specified. May not be specified if **lookback** is specified.

**groupby_key** (str, optional)
   The subset of keys from which an entity attribute table is generated.

**is_ranged_artifact** (bool, optional)
   Specifies if the dependency is on a ranged entity attribute table. A ranged entity attribute table uses ``daterange_end`` and ``daterange_start`` to specify the date boundary for the table, and not ``lookback`` or ``offset``.

**lookback** (int, optional)
   An integer that defines the number of days to include in the time window for the date range, inclusive of the current day. Use this setting instead of **daterange_end** and **daterange_start** to dynamically define the window.

**offset** (int, optional)
   The number of days in the lookback window to be offset.

**phase** (str, required)
   The name of the phase to which (and including) a data source is processed, after which the entity attribute table is generated. This data is processed up to and including that phase.
