.. 
.. this topic documents configuration settings for the config.yml file
.. settings are defined at /crepes/apt/apt/config.py
.. settings specific to this topic are located in the EntityAttributeReference class
.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
.. 


**additional_attribute** (list, optional)
   One (or more) additional attributes that specify the key for joining that attribute prior to performing aggregations. Use an **attribute_source_name** setting for each additional attribute:

   **attribute_source_name** (str, required)
      The name of the entity attribute table.

   **join** (bool, optional)
      Specifies if the attribute is joined to the entity attribute table prior to performing aggregations. When ``False``, the attribute is not joined automatically and that a custom process via an escape hatch will make use of the additional attribute. Default value: ``True``.

   **left_columns** (list of str, optional)
      The column in the entity attribute table to use for joining to the table prior to performing aggregations. By default, the **key_columns** in the entity attribute table.
