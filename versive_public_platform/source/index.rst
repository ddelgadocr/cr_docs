.. 
.. versive, platform, public
.. 

==================================================
Site Map
==================================================

This is the documentation for the |versive| Platform API, including

* How to deploy the platform
* Reference topics for the APIs that are part of the |versive| Platform API


Introduction
==================================================

:doc:`Glossary </glossary>`


Deploy
==================================================

**Platform**: :doc:`About the Platform </deploy_platform>` | :ref:`deploy-platform-requirements` | :ref:`deploy-platform-directory-structure` | :ref:`deploy-platform-preparation` | :ref:`deploy-platform-install-the-platform` | :ref:`deploy-platform-connect-to-data-sources` | 

**Platform Appendix**: :ref:`deploy-platform-ipython` | :ref:`deploy-platform-ssh` | :ref:`deploy-platform-command-line`

**Upgrade**: :ref:`deploy-platform-upgrade` | :ref:`deploy-platform-rollback` 

**Uninstall**: :ref:`deploy-platform-uninstall` 



API Functions
==================================================

**Python and the Versive Platform** :doc:`About Using Python with the Versive Platform </python>` | :ref:`python-import-modules` | :ref:`python-modes` | :ref:`python-interactive-mode` | :ref:`python-batch-mode` | :ref:`python-ipython` | :ref:`python-extension-package-mode` | :ref:`python-recommendations` | :ref:`python-checkpoint-often` | :ref:`python-column-hints` | :ref:`python-consistent-formatting` | :ref:`python-dates-as-strings` | :ref:`python-identify-information-leaks` | :ref:`python-learn-models-quickly` | :ref:`python-learning-curves` | :ref:`python-load-only-relevant-topics` | :ref:`python-print-statements` | :ref:`python-save-often` | :ref:`python-strptime-syntax` | :ref:`python-time-zones` | :ref:`python-validate-raw-data` | :ref:`python-user-defined-functions` | :ref:`python-missing-values` | :ref:`python-missing-values-parser-hints` | :ref:`python-missing-values-if-statements` | :ref:`python-lambda-expressions` | :ref:`python-logging-functions` | :ref:`python-module-help`

**RISP API** :doc:`About the RISP API </api_risp>` **Tables:** :ref:`api-add-columns` | :ref:`api-add-context` | :ref:`api-add-context-from` | :ref:`api-aggregate` | :ref:`api-count-frequencies` | :ref:`api-count-rows` | :ref:`api-create-context-custom-op` | :ref:`api-create-context-op` | :ref:`api-create-custom-op` | :ref:`api-create-parser-hint-date` | :ref:`api-create-parser-hint-maxerr` | :ref:`api-create-parser-hint-missing` | :ref:`api-create-parser-hint-table-maxerr` | :ref:`api-delete-columns` | :ref:`api-flexi-categorize` | :ref:`api-generate-rows` | :ref:`api-get-column-descriptions` | :ref:`api-get-column-names` | :ref:`api-get-delimiter` | :ref:`api-get-example-rows` | :ref:`api-join` | :ref:`api-load-crtable` | :ref:`api-load-csv` | :ref:`api-load-tabular` | :ref:`api-merge-tables` | :ref:`api-plot-bar` | :ref:`api-plot-histogram` | :ref:`api-plot-line` | :ref:`api-plot-scatter` | :ref:`api-relate` | :ref:`api-remove-duplicate-rows` | :ref:`api-rename-columns` | :ref:`api-replace-columns` | :ref:`api-save-crtable` | :ref:`api-save-csv` | :ref:`api-save-plot` | :ref:`api-save-table-schema` | :ref:`api-save-xls` | :ref:`api-select-columns` | :ref:`api-select-rows` | :ref:`api-split-table` | :ref:`api-summarize-table` **Models:** :ref:`api-classification-summary` | :ref:`api-customize-explanations` | :ref:`api-describe-model` | :ref:`api-learn-model` | :ref:`api-load-model` | :ref:`api-measure-confusion-matrix` | :ref:`api-measure-input-importance` | :ref:`api-measure-learning-curve` | :ref:`api-measure-score-curve` | :ref:`api-predict` | :ref:`api-save-model` | :ref:`api-score-predictions` | :ref:`api-summarize-model` **Checkpoints:** :ref:`api-checkpoint` | :ref:`api-get-checkpoints` | :ref:`api-load-checkpoint` **Custom functions:** :ref:`api-get-udf-debug-logs` | :ref:`api-udf-debug-log` **Platform:** :ref:`api-get-connections` | :ref:`api-get-session-property` | :ref:`api-get-versions` | :ref:`api-initialize-cluster` | :ref:`api-set-session-property` | :ref:`api-shutdown-cluster`







.. Hide the TOC from this file.

.. toctree::
   :hidden:

   api_risp
   cmd_cr
   deploy_connect_data_sources
   deploy_platform
   glossary
   python
