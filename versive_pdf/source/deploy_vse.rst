.. 
.. versive, primary, security engine
.. PDF output, so links are removed
.. 

==================================================
Deploy the |vse|
==================================================

.. include:: ../../includes_terms/term_vse.rst

The |vse| comprises the following:

#. A data lake to store raw data collected from various network data sources and from which the |vse| pulls data on a daily basis.
#. The |vse| itself for data processing, data modeling. Analysis of this data leads to discovery of anomalies and the generation of threat cases. A machine learning API---the |versive| platform---runs underneath the |vse|.
#. The threat viewer is a Web user interface that displays the top-ranked threat cases for human analysis.


Requirements
==================================================
The following sections detail the hardware (both per-node and per-cluster), software, and network requirements for the |versive| platform.

.. note:: The requirements for the engine that runs on top of the |versive| platform are generally the same as the requirements for the platform itself, especially with regard to cluster sizing, performance, and the size of data to be analyzed. However, in some cases the requirements for the engine will increase the requirements for the platform.


Software
--------------------------------------------------
The |versive| platform requires one of the following operating systems to be running on all nodes in the compute cluster:

* Red Hat Enterprise Linux (RHEL) 6.9 (or higher)
* CentOS 6.9 (or higher)

The following applications and libraries are required, but are typically included in the operating system distribution or by Amazon EMR for Amazon AWS deployents:

* Apache Spark 1.6.1, 1.6.2, or 1.6.3
* Hadoop 2.6 (or higher)
* YARN scheduler; set up YARN on all compute nodes in the cluster with the Fair Scheduler.
* JVM version 1.7.0_67

All other third-party packages required by the compute cluster are included with the installer for the |versive| platform.


Data Source
--------------------------------------------------
Data types must be provided to the engine as data sources pulled from an HDFS or Amazon S3 storage cluster.

.. list-table::
   :widths: 100 200 300
   :header-rows: 1

   * - Data Type
     - Possible Data Sources
     - Required?
   * - Flow
     - Raw flow data, Gigamon
     - Required

       .. note:: Raw flow data sources require nfdump tools to be installed. Customized scripts are then used to process this data into the canonical format, after which this data is uploaded to the |vse|. Ask your |versive| representative about using these customized scripts for use with flow data in your environment.
   * - Proxy
     - BlueCoat, ZScalar, Cato, ScanSafe
     - Optional
   * - DNS
     - 
     - Optional


Amazon VPC
==================================================
The |vse| may be deployed to an Amazon Virtual Private Cloud (VPC) that runs an Amazon Elastic Map Reduce (EMR) instance. This approach provides a managed Hadoop framework that runs Apache Spark and is capable of processing massive amounts of data across scalable Amazon EC2 instances. Flow, proxy, and DNS data that is to be analyzed by the |vse| may be staged for processing using HDFS or Amazon S3 storage.

**Setup Amazon AWS services**

#. Set up Amazon VPC. This VPC must support CIDR, network subnets, and routing for the VPC gateway.
#. For flow data sources, nfdump tools are used to collect, process, and then upload data to the |vse|. This may be uploaded to the HDFS storage in the Amazon EMR instance or it may be uploaded to an Amazon S3 bucket. An Amazon S3 bucket used for storing flow data requires Amazon Identity and Access Management (IAM) with the permissions configured to allow the VPC to access the S3 bucket.
#. The EMR instance will contain Amazon Elastic Compute Cloud (EC2), which require an Amazon EC2 key pairs for SSH credentials. The EC2 instances run the HDFS storage that is used by the |vse|.


Amazon AWS Parameters
--------------------------------------------------
The following paramaters provide all of the information to the |vse| that is necessary for the VPC, the S3 bucket (when used), and all EC2 instances running under the EMR instance:

**create_data_bucket**
   Specifies that the |vse| will use an S3 bucket for storage. Default value: ``True``.

**create_emr_cluster**
   Specifies that the |vse| is run in an Amazon EMR cluster. Default value: ``True``.

**create_key_pair**
   Specifies if an EC2 key pair is created automatically. Default value: ``True``.

**data_bucket_name**
   When ``create_data_bucket`` is ``True``, the name of the S3 data bucket.

**emr_master_type**
   The instance for the primary node in the compute cluster. Default value: ``m3.xlarge``.

**emr_worker_count**
   The number of workers, per replica node in the compute cluster. Default value: ``2``.

**emr_worker_type**
   The instance for all replica nodes in the compute cluster. Default value: ``m3.xlarge``.

**extra_tags**
   The identifier for this instance of the |vse|.

**long name**
   The long name of the VPC. The names of public and private instances use this long name, appended with ``-public`` or ``-private``.

**name**
   The name of the VPC.

**net_index**
   The /25 subdivision of the CIDR block. This setting determines CIDR block behaviors, as well as subnets for public and private CIDR blocks. This must be set to ``172.26.0.0/16``.

**region**
   The Amazon AWS region. This is used by the VPC. Default value: ``us-west-2``.

**repo_bucket_name**
   The name of the storage used by the |vse|. Default value: ``vsecloud-software``.

**working_bucket_name**
   When ``create_data_bucket`` is ``True``, the name of the S3 working bucket.




Preparation
==================================================
Prior to installing the |vse| platform, gather the following information about the nodes in the compute cluster:

#. Identify the node that will be the primary node, and then make note of its IP address.
#. Choose a name for the compute cluster.
#. Identify the user account that will install, administer, and build models with the platform. This account must be able to connect to all nodes using SSH without requiring a password, which may require root access.
#. The installation directory is ``$VSE_DIR/vse``. The account that owns the installation must have write access to this directory for all nodes in the cluster. When the self-installer is finished, all of the required directories for the installation are located under ``$VSE_DIR/vse``.
#. The data directory is ``$VSE_DIR/vse/vse``.
#. Start YARN services:

   On the primary node in the cluster---the resource manager---the following services must be started: ``hadoop-yarn-resourcemanager`` and ``hadoop-yarn-proxyserver``.

   On all replica nodes in the cluster---the node managers---the following services must be started: ``hadoop-yarn-nodemanager``.



Install the VSE
==================================================
Use the following steps to install the |vse|:

#. Get the self-installer for the |vse| from your |versive| representative. This installer will install the |vse|, the |versive| platform, the Web user interface, and all dependencies.
#. Add the self-installer to a directory on the primary node in the cluster. The installer for the |vse| may be run from any directory on the primary node. This directory is referred to as ``$VSE_DIR/vse``.
#. Run ``$VSE_DIR/vse/install/setup.sh`` to install the |vse|. This will install the platform, the engine, the Web user interface, and will ensure that Hadoop, Spark, and Python are configured correctly.
#. Run ``$VSE_DIR/vse/tools/self-test.sh`` to verify the installation. This will confirm that the installation process was completed and that the |vse| is ready.

When the installation process is complete, the following directory structure is created:

::

   $VSE_DIR/vse
   ├── vse-<version>
   │   ├── conf
   │   │   └── spark
   │   ├── vse
   │   ├── info
   │   ├── install
   │   ├── tools
   │   └── ui


.. 
.. section for "optional" configuration steps
.. should be written to link to other pages, and not be more than a list item on this page
.. 
.. steps like
.. 
.. LDAP
.. Spark tuning
.. 


Connect to Data Sources
==================================================
After the |versive| platform is installed and the engine is installed on top of it, connect to data sources. Data may be stored in a Hadoop Distributed File System (HDFS), Amazon S3, or any other file system with a unified view.

Flow Data Sources
--------------------------------------------------
For environments with flow data as a data source, install nfdump tools, and then work with your |versive| representative to add the following scripts to your environment: ``daily-backup-files.sh``, ``daily-nfcapd.sh``, and ``daily-nfdump-running-list.sh``. Flow data may be uploaded to an Amazon S3 bucket or to the HDFS store in the Amazon EMR instance.




