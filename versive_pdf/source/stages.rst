.. 
.. versive, primary, security engine
.. this page must declare images as PNG files; PNGs are required for HTML => PDF
.. 

==================================================
APT Campaign Stages
==================================================

.. TODO: remove the ALL_CAPS after it's determined if these need to be tokenized, capitalized, or whatever. Suggest lower-case: planning, access, recon, collection, and exfiltration.

.. include:: ../../includes_terms/term_campaign.rst

.. image:: ../../images/stages_all.png
   :width: 600 px
   :align: center

The |stage_recon|, |stage_collect|, and |stage_exfil| stages represent the most interesting stages because they contain a large amount of visible evidence that is discoverable with the right data inputs and the right amount of processing and are the stages against which the engine is focused.

The engine looks for confirmation of behaviors across all three stages, identifying the patterns for recurring activities and reducing false positives. An adversary doesn't typically perform a single |stage_recon|, |stage_collect|, and |stage_exfil| campaign, rather, once successful, they will return for additional |stage_recon|, |stage_collect|, and |stage_exfil| as long as the time, cost, and complexity of that effort still falls within the adversary's risk tolerance.


.. _stages-traditional-vs-versive:

Traditional vs. |versive|
==================================================
.. include:: ../../includes/stage_traditional_vs_versive.rst

.. image:: ../../images/stages_traditional_vs_versive.png
   :width: 600 px
   :align: center



.. _stages-planning:

Planning
==================================================
The |stage_plan| stage involves an adversary identifying a target, and then determining an approach that gives them a high chance of success.

.. image:: ../../images/stages_planning.png
   :width: 600 px
   :align: center

.. include:: ../../includes_terms/term_campaign_stage_planning.rst

.. include:: ../../includes/stage_planning_context.rst



.. _stages-access:

Access
==================================================
.. include:: ../../includes_terms/term_campaign_stage_access.rst

.. image:: ../../images/stages_access.png
   :width: 600 px
   :align: center

.. include:: ../../includes/stage_access_context.rst



.. _stages-recon:

Reconnaissance
==================================================

The |stage_recon| stage is the first of three stages---|stage_recon|, |stage_collect|, and |stage_exfil|---that are the primary focus of the engine.

.. image:: ../../images/stages_recon.png
   :width: 600 px
   :align: center

.. include:: ../../includes/stage_recon_intro.rst

.. include:: ../../includes_terms/term_campaign_stage_recon.rst

.. include:: ../../includes/stage_recon_context.rst



.. _stages-data-collection:

Data Collection
==================================================

The |stage_collect| stage is the second of three stages---|stage_recon|, |stage_collect|, and |stage_exfil|---that are the primary focus of the engine.

.. include:: ../../includes_terms/term_campaign_stage_collection.rst

.. image:: ../../images/stages_collection.png
   :width: 600 px
   :align: center

.. include:: ../../includes/stage_collection_context.rst



.. _stages-data-exfiltration:

Data Exfiltration
==================================================
The |stage_exfil| stage is the third of three stages---|stage_recon|, |stage_collect|, and |stage_exfil|---that are the primary focus of the engine.

.. include:: ../../includes/stage_exfiltration_intro.rst

.. image:: ../../images/stages_exfiltration.png
   :width: 600 px
   :align: center

.. include:: ../../includes_terms/term_campaign_stage_exfiltration.rst



.. _stages-recurring:

Recurring Stages
==================================================

.. include:: ../../includes/stages_ongoing_intro.rst

.. image:: ../../images/stages_all.png
   :width: 600 px
   :align: center

.. include:: ../../includes_terms/term_campaign_stages_ongoing.rst

.. include:: ../../includes/stages_ongoing_context.rst

