.. 
.. xxxxx
.. 

After irrelevant data is removed, review the summary statistics for the table to get a better understanding of the data. For each column, and depending on the data that is in the table itself, the ``summarize_table()`` function returns some (or all) of the following details:

* The five most common values
* The approximate number of unique units
* A count
* The number of empty rows
* The maximum value
* The minimum value
* The standard deviation
* The sum of all values 

**To review table statistics**

#. Summarize the table:

   .. code-block:: python

      >>> risp.summarize_table(table)

   This will return a table similar to:

   .. code-block:: python

      statistic         Major          Minor          SalePrice
      count             192443         192443         192443
      empty             0              0              0
      appx_unique       N/A            N/A            251+
      #1 value (count)  N/A            N/A            __FREQUENCY_ELIDED__ (104490)
      #2 value (count)  N/A            N/A            350000 (1227)
      #3 value (count)  N/A            N/A            300000 (1179)
      #4 value (count)  N/A            N/A            250000 (1133)
      #5 value (count)  N/A            N/A            325000 (1082)
      min               100.0          1.0            100001
      max               990600.0       9692.0         1998750
      mean              455734.464034  1440.51163202  428514.288745
      median            390490.0       310.0          350000
      stddev            289092.18828   2756.63826322  278870.37624
      sum               87702907462.0  277216380.0    82464575269

#. Create histograms to improve the visual understanding of the data.

   First, get 1000 rows of data from the LocalDataTable:

   .. code-block:: python

      >>> sample = risp.get_example_rows(table,
                                         count=1000)

   Create a histogram for sale prices:

   .. code-block:: python

      >>> sale_histo = risp.plot_histogram(sample,
                                           column='SalePrice')
      >>> risp.save_plot(sale_histo,
                         path=base_path+'sale_histo.png')
      >>> print "Sale price histogram saved to %s.\n" % (base_path+'sale_histo.png')

   Create a histogram for bedrooms:

   .. code-block:: python

      >>> bedroom_histo = risp.plot_histogram(sample,
                                              column='Bedrooms')
      >>> risp.save_plot(bedroom_histo,
                         path=base_path+'bedroom_histo.png')
      >>> print "Bedroom histogram saved to %s.\n" % (base_path+'bedroom_histo.png')

#. Checkpoint and save this version of the modified table to local storage. Checkpoint the table:

   .. code-block:: python

      >>> risp.checkpoint(table,
                          key='table2')
      >>> risp.get_checkpoints()

   This will return a table similar to:

   .. code-block:: python

      Key         Item Type   Create Time
      table1      table       2015-02-20 13:44:55:203857
      table2      table       2015-02-20 14:04:22:301805

   and then save the table:

   .. code-block:: python

      >>> risp.save_crtable(table,
                            path=base_path+'cleaned_table')

    This is the table that will be used to learn the model.

