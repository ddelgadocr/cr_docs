.. 
.. xxxxx
.. 

An initial model should be learned against a simple data set with minimal modification. It's best to build the initial model quickly, and then interatively tune that model by adding more data or calculations. The initial model is unlikely to be the model with which predictions will ultimately be made, but it's much easier to troubleshoot a model in the early stages.

**Learn an intial model**

#. Split the table into train, tune, and test tables. The default split is ``60, 20, 20``, but this split may be any number of floats or integers, and these values do not need to add up to ``1`` or ``100``:

   .. code-block:: python

      >>> train, tune, test = risp.split_table(table,
                                               proportions=[60, 20, 20])

#. Learn a linear regression model to predict sale price using the train and tune tables. The machine learning algorithms automatically learn many different models, automatically selecting the best one to train and tune using the inputs that are provided. Automatic exploration is set to ``interactions`` mode to explore the interactions between different inputs to identify the features used to learn the model:

   .. code-block:: python

      >>> model = risp.learn_model(target='SalePrice',
                                   model_type='LINEAR_REGRESSION',
                                   train_table=train,
                                   tune_table=tune,
                                   exploration='INTERACTIONS')

#. A checkpoint saves a model to memory so that it may be retrieved later:

   .. code-block:: python

      >>> risp.checkpoint(model,
                          key='model1')

