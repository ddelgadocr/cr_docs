.. 
.. xxxxx
.. 

Before you can begin walking through this tutorial, you must prepare and join the raw data file for the data that was downloaded from the King County (Washington state) web site.

.. note:: In the following Python script example the ``base_path`` setting will need to be updated for the the absolute path to your storage location. This example assumes that the |versive| platform is being run locally on a single node, so if this data is stored elsewhere, such as a remote repository for multi-node processing, the ``storage`` value used for the default connection will also require updating.

This tutorial uses interactive mode to walk through the process of building a simple linear regression model, except for the preparation of raw data, which uses a Python script and batch mode to perform initial data preparation, join two raw data files, and then save a data file to local storage that can be used by this tutorial.

Save the contents of the following code block to your local machine as ``housing_script.py``, and then run this script in batch mode:

.. code-block:: python

   storage='local'

   # Before initializing the cluster, this script defines several functions
   # that you can then call later in the script.

   import cr.risp_v1 as risp
   import datetime
   import dateutil

   # This function will select only properties that are improved residential
   # (property_class 3), are principally used for residential (principal_use 6),
   # and have land with a previously used building (property_type 3)

   def res_filter(property_class, principal_use, property_type):
     if (property_class == 8 and principal_use == 6 and property_type == 3):
       return 1
     else:
       return 0

   # This function returns properties that have only a single residence.

   def first_building(*args):
     return_value = True
     for x in args:
       return_value = return_value and x == 1
     return return_value

   # This function returns properties with prices between 100,000 and 2
   # million dollars.

   def price_range(x):
     return x > 100000 and x < 2e6

   risp.initialize_cluster(default_connection=storage)

   base_path = 'training_data/housing/'

   # Load the sales table and checkpoint it.

   sales = risp.load_tabular(path=base_path+'EXTR_RPSale.csv',
                             delimiter=',',
                             has_headers=True)
   risp.checkpoint(sales, key='sales')

   # Include only the type of properties specified by the res_filter function.

   sales = risp.select_rows(sales,
                            input_columns=['PropertyClass',
                                           'PrincipalUse',
                                           'PropertyType'],
                            udf=res_filter)

   # Select only the columns we are interested in.

   sales = risp.select_columns(sales,
                               columns=['Major', 'Minor', 'SalePrice'])
   risp.checkpoint(sales, key='sales')

   # Include rows with SalePrice values over 100k and under 2M.

   sales = risp.select_rows(sales,
                            input_columns='SalePrice',
                            udf=price_range)
   risp.checkpoint(sales, key='sales')

   # Load the property characteristics table.

   prop = risp.load_tabular(path=base_path+'EXTR_ResBldg.csv',
                            delimiter=',',
                            has_headers=True)
   risp.checkpoint(prop, key='prop')

   # Create a subset of the data that includes only single-family homes for which
   # the values in the BldgNbr and NbrLivingUnits columns are 1.

   prop = risp.select_rows(prop,
                           input_columns=['BldgNbr', 'NbrLivingUnits'],
                           udf=first_building)
   risp.checkpoint(prop, key='prop')

   # Select only the columns we are interested in.

   prop = risp.select_columns(prop,
                              columns=['Major', 'Minor', 'Address',
                                       'ZipCode', 'Stories', 'BldgGrade',
                                       'Condition', 'YrBuilt', 'YrRenovated',
                                       'FpSingleStory', 'FpFreestanding',
                                       'SqFt1stFloor', 'SqFtTotLiving',
                                       'SqFtTotBasement',
                                       'SqFtGarageAttached', 'Bedrooms',
                                       'BathHalfCount', 'Bath3qtrCount',
                                       'BathFullCount'])

   # Analyze sales and property tables to determine which columns we should
   # join the two tables on. Providing None for left_columns and right_columns
   # analyzes all possible columns.

   risp.relate(sales, prop, left_columns=None, right_columns=None)

   # Do an inner join on the Major and Minor columns and save this
   # as the master data file.

   master = risp.join(sales,
                      prop,
                      key_columns=[('Major', 'Major'),
                                   ('Minor', 'Minor')],
                      join_type='INNER')
   risp.checkpoint(master, key='master')
   risp.save_csv(master,
                 path=base_path+'housing_data.csv',
                 delimiter=',')
   print "Housing data table for use with first model tutorial saved to\
          %s.\n" % (base_path+'housing_data.csv')

