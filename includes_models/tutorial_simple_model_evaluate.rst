.. 
.. xxxxx
.. 

After the initial model is built, evaluate how well it meets your business objectives. The information that is gathered helps to inform how the model can be improved in subsequent iterations.

In the output, "baseline" or "base" indicates the score of the baseline model. The baseline model predicts the mean of the target column and the score that is returned is generally the same as a random guess. Some inputs are more or less important when making model predictions. To evaluate which ones are the most important, use a variety of report styles that are built into the ``measure_input_importance()`` function.

**To evaluate the initial model**

#. Summarize the model for the relative importance of each input in making predictions:

   .. code-block:: python

      >>> summary = risp.summarize_model(model,
                                         table=test)
      >>> print summary

   will print output similar to:

   .. code-block:: python

      Number of input columns: 49
      Score (RMSE): 187836.4956
      Baseline (RMSE): 278929.8065
      Relative Importance (%)   Inputs                             Score Without (RMSE)
      31.39                     BldgGradeVar                       198187.33
      4.68                      BldgGrade                          189378.38
      4.66                      StreetName                         189372.41
      3.66                      PcntComplete                       189044.23
      3.32                      BldgNbr & BldgGrade                188930.83
      3.32                      NbrLivingUnits & BldgGrade         188930.83
      2.56                      BrickStone                         188680.56
      1.77                      PcntNetCondition                   188418.87
      1.46                      SqFtTotLiving                      188317.27
      1.33                      SqFtUnfinHalf                      188275.14
      1.22                      YrBuilt                            188240.16
      1.1                       DirectionPrefix & ZipCode          188200.16
      0.97                      FinBasementGrade                   188154.74
      0.92                      YrRenovated                        188141.4
      0.92                      SqFtEnclosedPorch                  188140.88
      0.92                      BuildingNumber                     188139.53
      0.91                      BldgGradeVar & Obsolescence        188137.38
      0.87                      BuildingNumber & ZipCode           188124.77
      0.84                      DirectionSuffix                    188113.19
      0.74                      FpAdditional                       188079.82
      0.72                      Obsolescence                       188073.93
      0.72                      SqFt2ndFloor                       188073.27
      0.71                      ZipCode & YrBuilt                  188072.2
      0.68                      StreetName & BldgGrade             188059.75
      0.67                      BuildingNumber & DirectionSuffix   188056.15

#. Make predictions using the test table. The results of running predictions using a test table include a column for both the known sale price and the predicted sale price.

   .. code-block:: python

      >>> results = risp.predict(model,
                                 table=test)
      >>> risp.get_example_rows(results,
                                count=20)

#. Save the results to CSV:

   .. code-block:: python

      >>> risp.save_csv(results,
                        path='base_path+results.csv',
                        delimiter=',')

#. Score by root mean squared error (RMSE) and mean absolute percentage error (MAPE). These measurements are recommended for linear regression models. RMSE is measured in the same units as the goal variable (in this case, United States dollars) and MAPE is measured in percentage points. Both are a good measure of error; therefore, lower values indicate better predictions.

   .. code-block:: python

      >>> risp.score_predictions(results,
                                 metrics=['RMSE', 'MAPE'])

   will return output similar to:

   .. code-block:: python

      RMSE            MAPE            RMSE (base)     MAPE (base)
      187836.495612   35.2265073146   278929.806533   60.6047846511

#. Results can be grouped by values in a particular column. In this case, group the scores by ZIP code, and then review the results in a bar graph. Grouping by ZIP code can show how the accuracy of a model varies by location. Because only a single metric is returned, the group-by column can be passed as a string, instead of a list of strings.

   .. code-block:: python

      >>> scorebyZip = risp.score_predictions(results,
                                              metrics='RMSE',
                                              group_by_columns='ZipCode')
      >>> print scorebyZip

   will return output similar to:

   .. code-block:: python

      (ZipCode)   RMSE            RMSE (base)
                  242202.333786   322523.876786
      98104       14970.84375     28827.4913293
      95125       111740.5        321172.508671
      98000       105481.92357    181208.84075
      98001       126141.026914   213850.35958
      98002       106763.947958   225779.083109
      98003       78982.6538287   211206.949842
      98004       453975.78074    621968.867466
      98005       285917.855784   491898.04358

   .. code-block:: python

      >>> zip_bar = risp.plot_bar(scorebyZip,
                                  x_column='(ZipCode)',
                                  y_columns='RMSE')
      >>> risp.save_plot(zip_bar,
                         path=base_path+'zip_bar.png'

#. Group the scores by sale price and review in a scatter plot:

   .. code-block:: python

      >>> scorebyPrice = risp.score_predictions(results,
                                                metrics='RMSE',
                                                group_by_columns='SalePrice')
      >>> price_scatter = risp.plot_scatter(scorebyPrice,
                                            x_column=0,
                                            y_columns=1)
      >>> risp.save_plot(price_scatter,
                         path=base_path+'price_scatter.png'

#. .. include:: ../../includes_terms/term_report_one_in.rst

   Run a one-in report:

   .. code-block:: python

      >>> risp.measure_input_importance(target='SalePrice',
                                        model_type='LINEAR_REGRESSION',
                                        train_table=train,
                                        tune_table=tune,
                                        test_table=test,
                                        style='one-in',
                                        metric='RMSE')

   will return output similar to:

   .. code-block:: python

      Metric Name                    RMSE
      Report Style                   one-in
      Best Score                     191295.5625
      Num. Models Tried              350
      all input variables            191295.5625
      include (BldgGrade)            211394.453125
      include (SqFtTotLiving)        217912.59375
      include (SqFt1stFloor)         247960.75
      include (SqFt2ndFloor)         250685.96875
      include (FinBasementGrade)     254578.78125
      include (StreetName)           258260.234375
      include (AddnlCost)            260796.078125
      include (SqFtGarageAttached)   261397.84375
      include (BathFullCount)        262967.4375
      include (FpSingleStory)        265485.625
      include (Bedrooms)             266841.46875
      include (SqFtOpenPorch)        268375.21875
      include (SqFtFinBasement)      269001.59375
      include (SqFtTotBasement)      270427.34375
      include (YrBuilt)              272605.84375
      include (SqFtDeck)             272734.78125
      include (BathHalfCount)        272741.4375
      include (ZipCode)              273026.65625
      include (Stories)              273205.53125
      include (DirectionSuffix)      273353.875
      include (FpAdditional)         274065.3125
      include (FpMultiStory)         274124.1875
      include (BuildingNumber)       274460.5625
      include (DirectionPrefix)      274744.78125
      include (HeatSystem)           275050.15625
      include (StreetType)           276182.40625
      include (Bath3qtrCount)        276227.5625
      include (BrickStone)           276873.4375
      include (HeatSource)           276874.15625
      include (ViewUtilization)      277045.65625
      include (SqFtGarageBasement)   277564.40625
      include (Minor)                277973.90625
      include (DaylightBasement)     278434.125
      include (Major)                278506.875
      include (YrRenovated)          278539.0
      include (Condition)            278593.65625
      include (FpFreestanding)       278645.0
      include (Fraction)             278749.65625
      include (BldgGradeVar)         278823.84375
      include (SqFtEnclosedPorch)    278877.375
      include (SqFtHalfFloor)        278891.6875
      include (PcntComplete)         278917.0625
      include (BldgNbr)              278924.59375
      include (SqFtUnfinFull)        278934.65625
      include (SqFtUnfinHalf)        278935.84375
      include (PcntNetCondition)     278936.53125
      include (Obsolescence)         278938.5625
      include (NbrLivingUnits)       278939.375
      include (SqFtUpperFloor)       278942.53125

#. .. include:: ../../includes_terms/term_report_one_out.rst

   Run a one-out report:

   .. code-block:: python

      >>> risp.measure_input_importance(target='SalePrice',
                                        model_type='LINEAR_REGRESSION',
                                        train_table=train,
                                        tune_table=tune,
                                        test_table=test,
                                        style='one-out',
                                        metric='RMSE')

   will return output similar to:

   .. code-block:: python

      Metric Name                    RMSE
      Report Style                   one-out
      Best Score                     191281.6875
      Num. Models Tried              350
      all input variables            191281.6875
      exclude (SqFtGarageBasement)   194070.78125
      exclude (StreetType)           194086.140625
      exclude (SqFtHalfFloor)        194104.703125
      exclude (PcntComplete)         194131.90625
      exclude (SqFtOpenPorch)        194160.984375
      exclude (FinBasementGrade)     194166.140625
      exclude (SqFtUpperFloor)       194174.359375
      exclude (BrickStone)           194178.546875
      exclude (SqFtUnfinHalf)        194191.03125
      exclude (FpAdditional)         194233.875
      exclude (SqFtGarageAttached)   194251.71875
      exclude (YrRenovated)          194270.09375
      exclude (Bedrooms)             194271.03125
      exclude (Stories)              194298.15625
      exclude (FpSingleStory)        194319.015625
      exclude (SqFtTotBasement)      194324.78125
      exclude (BathFullCount)        194342.4375
      exclude (Minor)                194364.375
      exclude (SqFt2ndFloor)         194370.953125
      exclude (SqFtUnfinFull)        194389.8125
      exclude (AddnlCost)            194407.90625
      exclude (SqFt1stFloor)         194412.453125
      exclude (SqFtFinBasement)      194412.5625
      exclude (BldgGradeVar)         194437.359375
      exclude (BldgNbr)              194444.96875
      exclude (FpFreestanding)       194445.625
      exclude (NbrLivingUnits)       194468.265625
      exclude (HeatSource)           194474.484375
      exclude (BathHalfCount)        194475.578125
      exclude (FpMultiStory)         194495.234375
      exclude (SqFtDeck)             194617.765625
      exclude (HeatSystem)           194623.78125
      exclude (DaylightBasement)     194625.53125
      exclude (PcntNetCondition)     194625.5625
      exclude (Obsolescence)         194635.265625
      exclude (Bath3qtrCount)        194658.71875
      exclude (Major)                194685.28125
      exclude (ViewUtilization)      194693.578125
      exclude (ZipCode)              194722.46875
      exclude (SqFtEnclosedPorch)    194732.265625
      exclude (Fraction)             194768.484375
      exclude (DirectionPrefix)      194940.3125
      exclude (SqFtTotLiving)        195052.59375
      exclude (Condition)            195078.3125
      exclude (StreetName)           195104.5625
      exclude (BuildingNumber)       195106.984375
      exclude (YrBuilt)              195302.296875
      exclude (DirectionSuffix)      195699.609375
      exclude (BldgGrade)            199000.671875

#. .. include:: ../../includes_terms/term_model_learning_curve.rst

   Measure the learning curve and display it on a line graph:

   .. code-block:: python

      >>> curve = risp.measure_learning_curve(target='SalePrice',
                                              model_type='LINEAR_REGRESSION',
                                              train_table=train,
                                              tune_table=tune,
                                              test_table=test)
     >>> print curve
     >>> learning_curve = risp.plot_line(curve,
                                         x_column=0,
                                         y_columns=1)
     >>> risp.save_plot(learning_curve,
                        path=base_path+'learning_curve.png')

#. Save the model so that it may be used outside of this working session:

   .. code-block:: python

      >>> risp.save_model(model,
                          path=base_path+'first_model')

#. To learn the model, the |versive| platform bins the data across small intervals. Use the ``describe_model()`` function to learn more about this model. This function builds a table that contains model details, showing how data values are binned across a given interval and how much weight each bin has on the model predictions. Large positive values indicate the bin has a large positive weight. Large negative values indicate the bin has a large negative weight. The table contains one row for each bin. The output is similar to:

   .. code-block:: python

      >>> risp.describe_model(model)

   will return output similar to:

   .. code-block:: python

      Weight     Feature                                       Type
      430902.0   bias                                          numeric
      237103.0   FpFreestanding is None                        missing value
      229659.0   FpMultiStory is None                          missing value
      194150.0   ((feature 91 indicates the (composite)        interaction
                 value(s) from column(s) StreetName was
                 clustered with values that appeared with
                 frequency between 2.0000 and 3.0000)
      *          (feature 87 indicates the (composite)
                 value(s) from column(s) StreetName was
                 clustered with values that correlated with
                 an average goal value greater than
                 850000.0000)) is 1
      184775.0   ((feature 6 indicates column Major had a
                 value between 358276.0000 and 413930.0000)
      *          (((feature 97 indicates the (composite)       interaction
                 value(s) from column(s) StreetName was
                 clustered with values that appeared with
                 frequency between 12.0000 and 16.0000)
      *          (feature 87 indicates the (composite)
                 value(s) from column(s) StreetName was
                 clustered with values that correlated with
                 an average goal value greater than
                 850000.0000)) is 1)) is 1
      184775.0   ((feature 33 indicates column
                 BuildingNumber had a value between
                 2114.0000 and 3522.0000)
      *          (((feature 97 indicates the (composite)       interaction
                 value(s) from column(s) StreetName was
                 clustered with values that appeared with
                 frequency between 12.0000 and 16.0000)
      *          (feature 87 indicates the (composite)
                 value(s) from column(s) StreetName was
                 clustered with values that correlated with
                 an average goal value greater than
                 850000.0000)) is 1)) is 1


