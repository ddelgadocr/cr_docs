.. 
.. xxxxx
.. 

This tutorial has the following requirements:

* The |versive| platform is installed and is configured to run as a single-node cluster
* Named connections to the storage cluster are configured
* The data sources used by this tutorial are available as two downloads from the King County (Washington state) web site:

  `Real Property Sales <http://your.kingcounty.gov/extranet/assessor/Real%20Property%20Sales.zip>`_

  `Residential Building <http://your.kingcounty.gov/extranet/assessor/Residential%20Building.zip>`_

