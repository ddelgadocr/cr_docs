.. 
.. xxxxx
.. 

Load the raw data into RISP, first using a triage table to subsample and validate the data. Tabular data files are converted into DataTable tables for subsequent transformation and modeling by RISP. For example, comma-separated values (CSV) files or tab-separated values (TSV) files can be loaded, and then worked with them as a DataTable.

.. note:: For a new dataset, it is recommended to validate the data against the schema and load only a subsample of the data. This helps to quickly detect any problems that may be present prior to working with the full data set. Before loading the data, verify if the first row contains column headers or data and indicate that in the ``has_headers`` argument. Use the ``path`` argument to specify the directory in which data is located.

**To load the data set**

#. Load a portion of the data into a triage table.

   .. code-block:: python

      >>> triage_table = risp.load_tabular(path='housing_data.csv',
                                           validate=True,
                                           subsample=True,
                                           has_headers=True)

#. View example rows:

   .. code-block:: python

      >>> risp.get_example_rows(triage_table, count=5)

#. View the detected delimiter:

   .. code-block:: python

      >>> risp.get_delimiter(triage_table)

#. Save and review the schema file:

   .. code-block:: python

      >>> risp.save_table_schema(triage_table, path='schema.txt')

   .. include:: ../../includes_terms/term_file_schema.rst

   In this case, the automatically-generated schema file for the ``housing_data.csv`` file is similar to:

   .. code-block:: none

      Major   float
      Minor   float
      SalePrice   int
      Address   str
      ZipCode   int
      Stories   float
      BldgGrade   int
      Condition   int
      YrBuilt   int
      YrRenovated   int
      FpSingleStory   int
      FpFreestanding   int
      SqFt1stFloor   int
      SqFt2ndFloor   int
      SqFtTotLiving   int
      SqFtTotBasement   int
      SqFtGarageAttached   int
      Bedrooms   int
      BathHalfCount   int
      Bath3qtrCount   int
      BathFullCount   int

   .. note:: If the automatically-generated schema file contains mistakes, modify the saved file to correct them.

#. After the contents of the subsampled data have been examined and verfied, load the full CSV file:

   .. code-block:: python

      >>> raw_table = risp.load_tabular(path='housing_data.csv',
                                        validate=True,
                                        schema_file='schema.txt',
                                        delimiter=',', has_headers=True)

#. Review the column names:

   .. code-block:: python

      >>> risp.get_column_names(raw_table)

   returns a list of column names similar to:

   .. code-block:: python

      ['Major', 'Minor', 'SalePrice', 'Address', 'ZipCode', 'Stories',
       'BldgGrade', 'Condition', 'YrBuilt', 'YrRenovated', 'FpSingleStory',
       'FpFreestanding', 'SqFt1stFloor', 'SqFt2ndFloor', 'SqFtTotLiving',
       'SqFtTotBasement', 'SqFtGarageAttached', 'Bedrooms', 'BathHalfCount',
       'Bath3qtrCount', 'BathFullCount']

#. Save the raw DataTable so that it may be used later without having to reload it in CSV format:

   .. code-block:: python

      >>> risp.save_crtable(raw_table,
        ...                 path='raw_table/')

   .. note:: Use the ``load_crtable()`` function to reload this table.

