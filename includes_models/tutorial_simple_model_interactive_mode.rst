.. 
.. xxxxx
.. 

Start the Python interpreter in interactive mode, import the RISP module and optional third-party modules, and then initialize the cluster to prepare the compute nodes to work with the data.

**To start a working session in interactive mode**

#. Run the following command to start a working session in interactive mode:

   .. code-block:: bash

      $ crpython

#. Import the RISP module and optional third-party modules, such as ``itertools`` and ``datetime``:

   .. code-block:: python

      >>> import cr.risp_v1 as risp
      >>> import itertools
      >>> import datetime

#. The |versive| platform API cannot be accessed without initializing the compute cluster, which also sets the default connection to the storage cluster:

   .. code-block:: python

      >>> initialize_cluster(default_connection='local')

