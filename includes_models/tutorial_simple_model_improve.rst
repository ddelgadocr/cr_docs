.. 
.. xxxxx
.. 

After the initial model built, it's time to begin improving it. This is the point at which aggregating data should begin, along with joining tables,
transforming existing data to create new measurements, and so on. Ask questions about the data. For example:

* Does the data contain all factors that may affect the goal?
* Does the model make sense?
* Does theory suggest additional inputs?

**Iterate and improve a model**

The steps below describe additional ways to iterate and improve upon an initial model:

#. Save the table as a checkpoint so that it may be used later:

   .. code-block:: python

      >>> risp.get_checkpoints()

   will print output similar to:

   .. code-block:: python

      Key         Item Type   Create Time
      table1      table       2015-02-20 13:44:55:203857
      table2      table       2015-02-20 14:04:22:301805
      model1      model       2015-02-20 15:08:27.498882

#. Load the checkpointed table:

   .. code-block:: python

      >>> table = risp.load_checkpoint(key='table2')

#. Join additional data. For example, you might want to add the following kinds of data: Case-Shiller home price indices, macroeconomic data, economic indicators, time of year sold, time on market, interest rate, or lot size. Use the ``merge_tables()`` and ``join()`` functions to add these types of inputs.

#. Add context by time, such as by calculating mean, standard deviation, and other descriptive statistics for specified time intervals. For example, calculate the minimum and maximum sale price for all houses in the week leading up to the sale of a particular house.

   First create a context operation to define the time interval and operation to perform. Use the ``create_context_op()`` function for simple, pre-defined operations, such as average, minimum, maximum, or standard deviation. Use ``create_context_custom_op`` to define a custom operation with a user-defined function.

   Then apply the context operation to time-series data. Use the ``add_context()`` function if the time-series data is already in the existing data table. Use the ``add_context_from()`` function if the time-series data is in a secondary table.

#. Aggregate the data. For example, scoring results may indicate that location plays a large role in the accuracy of the model. The following iteration shows how to aggregate data by ZIP code, and then learn a new model that calculates by the average sale price, minimum and maximum by ZIP code:

   .. code-block:: python

      >>> agg_table = risp.aggregate(table,
                                     group_by_columns='ZipCode',
                                     aggregate_ops=[('SalePrice', 'mean'),
                                                    ('SalePrice', 'min'),
                                                    ('SalePrice', 'max')])
