.. 
.. xxxxx
.. 

Some columns or rows may contain data that is not relevant to the business goal, such as malformed or irrelevant data, to improve the rate at which computations can be made against this data. These columns and rows should be removed from the data because they will not be valuable for making predictions. For example, exports from databases often include information for the date and time at which a row was added or modified.

**To remove data from the data set**

#. In this dataset, the ``Address`` column is not needed because the predictions should be made without specific house identifiers. (General location information included in the ``ZipCode`` column is sufficient.)

   .. code-block:: python

      >>> table = risp.delete_columns(raw_table,
                                      columns='Address')
      >>> risp.get_column_names(table)

   will print output similar to:

      .. code-block:: python 
   
         ['Major', 'Minor', 'SalePrice', 'ZipCode', 'Stories', 'BldgGrade',
          'Condition', 'YrBuilt', 'YrRenovated', 'FpSingleStory',
          'FpFreestanding', 'SqFt1stFloor', 'SqFt2ndFloor', 'SqFtTotLiving',
          'SqFtTotBasement', 'SqFtGarageAttached', 'Bedrooms', 'BathHalfCount',
          'Bath3qtrCount', 'BathFullCount']

#. Create a table checkpoint.

   .. code-block:: python

      >>> risp.checkpoint(table,
                          key='table1')
      >>> risp.get_checkpoints()

   will print output similar to:

      .. code-block:: python 
   
         Key         Item Type   Create Time
         table1      table       2015-02-20 13:44:55:203857

#. Remove rows that don't meet specific criteria with a user-defined function or delete rows that don't meet certain criteria. For example, to predict sale prices for houses that have been recently built or renovated, first create a user-defined function that calculates which rows meet that criteria by returning ``1`` if a house has been built or removed after 1980, otherwise return ``0``:

      .. code-block:: python

         >>> def new_houses(built, renovated):
               if ((built > 1980) or (renovated > 1980)):
                 return 1
               else:
                 return 0

#. Pass the user-defined function to the ``select_rows()`` function to keep only the rows for which the user-defined function returns ``1``. Call the ``count_rows()`` function before and after this command to see how many rows were selected:

      .. code-block:: python

         >>> risp.count_rows(table)

   will print output similar to:

      .. code-block:: python 
   
         480474

      .. code-block:: python

         >>> table = risp.select_rows(table,
                                      input_columns=['YrBuilt', 'YrRenovated'],
                                      udf=new_houses)
         >>> risp.count_rows(table)

   will print output similar to:

      .. code-block:: python 
   
         192443

