.. 
.. xxxxx
.. 

This tutorial walks through the process of building a simple model using functions available from the |risp_full|.

The data used in the example contains housing data from King County (Washington state) and the model predicts property sale prices given a variety of inputs, such as square footage, building condition, and year built.
