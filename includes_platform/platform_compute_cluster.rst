.. 
.. xxxxx
.. 

All data processing and model building using the |versive| platform occurs on a set of networked computers or virtual machines known as a compute cluster. All data is pulled from a storage cluster running as a data lake in Amazon S3 or HDFS.

The standard configuration consists of:

* A single, primary node in the compute cluster that divides and schedules the tasks to be performed by individual nodes in the compute cluster and owns and maintains the configuration of all nodes in the compute cluster
* Individual secondary nodes in the compute cluster perform resource-intensive analysis and other system tasks

This type of distributed computing environment enables the scalability to handle modern big data sets and the speed to compete in rapidly changing markets. Add more nodes to the cluster as needed to reduce the time it takes the |versive| platform to process data.
