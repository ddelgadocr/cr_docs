.. 
.. xxxxx
.. 

The |versive| platform contains public API modules from which custom solutions may be built. Each module exposes a subset of the |versive| platform API and can be used to load datasets, transform that data, build models, and then use these models to make predictions. Familiarity with machine learning is not required to build successful models; familiarity with statistics is helpful. The |versive| platform API uses the Python programming language, which is easy to learn whether you are new to programming or experienced in other languages.
