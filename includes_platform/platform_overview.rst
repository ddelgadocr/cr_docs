.. 
.. xxxxx
.. 

The |versive| platform is a predictive analytics platform that enables companies to make better business decisions by leveraging their data to create predictive models. A predictive model uses machine learning algorithms to predict future events and behaviors based on historical observations, such as:

* Using network activity to predict security risks
* Preventing fraud
* Targeting customers more effectively

The |versive| platform accelerates the traditional model building process by simplifying and automating much of the time-consuming manual work needed to build and deploy these models. The |versive| platform functionality is accessed via the |versive| platform API by using Python commands that analyze and transform data in a variety of ways to build predictive models.
