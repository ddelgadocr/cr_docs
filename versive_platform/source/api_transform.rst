.. 
.. xxxxx
.. 

==================================================
Transform API
==================================================

The Transform API may be used to work with transforms.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Transform API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.transform as transform

and then for each use of a function in this API within the customization package add ``security.`` as the first part of the function name. For example:

.. code-block:: python

   transform.load_state()

or:

.. code-block:: python

   RowFilterTransform.apply()


The following functions may be used to build customized tables, models, and transaction behaviors:

* :ref:`api-transform-tabletransform`
* :ref:`api-transform-tabletransform-apply`
* :ref:`api-transform-tabletransform-load-state`
* :ref:`api-transform-tabletransform-migrate-saved-object`
* :ref:`api-transform-tabletransform-output-column-names`
* :ref:`api-transform-tabletransform-save-state`
* :ref:`api-transform-tabletransform-validate-columns`
* :ref:`api-transform-transformpipeline`
* :ref:`api-transform-transformpipeline-add-pipeline`
* :ref:`api-transform-transformpipeline-add-transform`
* :ref:`api-transform-transformpipeline-apply`
* :ref:`api-transform-transformpipeline-load`
* :ref:`api-transform-transformpipeline-load-state`
* :ref:`api-transform-transformpipeline-save`
* :ref:`api-transform-transformpipeline-save-state`
* :ref:`api-transform-transformpipeline-validate-columns`
* :ref:`api-transform-transformpipeline-validate-column-names`
* :ref:`api-transform-columnfiltertransform`
* :ref:`api-transform-columnfiltertransform-apply`
* :ref:`api-transform-rowfiltertransform`
* :ref:`api-transform-rowfiltertransform-apply`
* :ref:`api-transform-rowfiltertransform-load-state`
* :ref:`api-transform-rowfiltertransform-save-state`
* :ref:`api-transform-rowtransform`
* :ref:`api-transform-rowtransform-apply`
* :ref:`api-transform-rowtransform-load-state`
* :ref:`api-transform-rowtransform-save-state`
* :ref:`api-transform-datetimetransform`
* :ref:`api-transform-urltransform`
* :ref:`api-transform-useragentparsertransform`
* :ref:`api-transform-numbertocategorytransform`
* :ref:`api-transform-numbertocategorytransform-apply`
* :ref:`api-transform-numbertocategorytransform-calculate-col-bins`
* :ref:`api-transform-numbertocategorytransform-load-state`
* :ref:`api-transform-numbertocategorytransform-save-state`
* :ref:`api-transform-quantizeargs`
* :ref:`api-transform-findreplacetransform`
* :ref:`api-transform-findreplacetransform-apply`
* :ref:`api-transform-findreplacetransform-load-state`
* :ref:`api-transform-findreplacetransform-save-state`
* :ref:`api-transform-propertysettransform`
* :ref:`api-transform-propertysettransform-apply`
* :ref:`api-transform-propertysettransform-create`
* :ref:`api-transform-propertysettransform-load-state`
* :ref:`api-transform-propertysettransform-save-state`
* :ref:`api-transform-propertysetstate`
* :ref:`api-transform-propertysetstate-from-json`
* :ref:`api-transform-propertysetstate-to-json`
* :ref:`api-transform-propertysetstate-validate-table`
* :ref:`api-transform-rollingwindowtransform`
* :ref:`api-transform-rollingwindowtransform-apply`
* :ref:`api-transform-rollingwindowtransform-load-state`
* :ref:`api-transform-rollingwindowtransform-save-state`
* :ref:`api-transform-rollingwindowbycounttransform`
* :ref:`api-transform-rollingwindowbycounttransform-apply`
* :ref:`api-transform-sessionizationtransform`
* :ref:`api-transform-sessionizationtransform-apply`
* :ref:`api-transform-fillmissingrowstransform`
* :ref:`api-transform-fillmissingrowstransform-apply`
* :ref:`api-transform-fillmissingrowstransform-load-state`
* :ref:`api-transform-fillmissingrowstransform-migrate-saved-object`
* :ref:`api-transform-fillmissingrowstransform-save-state`
* :ref:`api-transform-lookuptransform`
* :ref:`api-transform-lookuptransform-apply`
* :ref:`api-transform-lookuptransform-load-state`
* :ref:`api-transform-lookuptransform-save-state`
* :ref:`api-transform-collapserowstransform`
* :ref:`api-transform-collapserowstransform-apply`
* :ref:`api-transform-collapserowstransform-load-state`
* :ref:`api-transform-collapserowstransform-save-state`
* :ref:`api-transform-collapserowswithreducetransform`
* :ref:`api-transform-collapserowswithreducetransform-apply`
* :ref:`api-transform-collapserowswithreducetransform-load-state`
* :ref:`api-transform-collapserowswithreducetransform-save-state`
* :ref:`api-transform-removeduplicaterowstransform`
* :ref:`api-transform-removeduplicaterowstransform-apply`
* :ref:`api-transform-removeduplicaterowstransform-create`
* :ref:`api-transform-removeduplicaterowstransform-load-state`
* :ref:`api-transform-removeduplicaterowstransform-save-state`
* :ref:`api-transform-reshapewidetransform`
* :ref:`api-transform-reshapewidetransform-create`
* :ref:`api-transform-reshapewidetransform-load-state`
* :ref:`api-transform-reshapewidetransform-save-state`
* :ref:`api-transform-statestrategy`
* :ref:`api-transform-statestrategy-collapse-state`
* :ref:`api-transform-statestrategy-explode-state`
* :ref:`api-transform-statestrategy-new-state`
* :ref:`api-transform-statestrategy-trim-state-and-get-values`
* :ref:`api-transform-statestrategy-update-state-and-get-values`
* :ref:`api-transform-statefulrollingwindowtransform`
* :ref:`api-transform-statefulrollingwindowtransform-apply`
* :ref:`api-transform-statefulrollingwindowtransform-derive-table-from-state`
* :ref:`api-transform-statefulrollingwindowtransform-load-state`
* :ref:`api-transform-statefulrollingwindowtransform-save-state`
* :ref:`api-transform-compressedstatestrategy`
* :ref:`api-transform-compressedstatestrategy-collapse-state`
* :ref:`api-transform-compressedstatestrategy-explode-state`
* :ref:`api-transform-contextcolumnserror`
* :ref:`api-transform-contextcolumnstransform`
* :ref:`api-transform-contextcolumnstransform-apply`
* :ref:`api-transform-contextcolumnstransform-create`





.. _api-transform-tabletransform:

TableTransform
==================================================
Base class for transforming an input data table to an output data table. Subclasses must override ``apply()``, ``save_state()``, ``load_state()``, and ``migrate_saved_object()``. They must also declare their own ``VERSION`` and ``COMPAT_VERSION`` class fields.

Usage
--------------------------------------------------

* Class: ``cr.transform.TableTransform``
* Bases: ``cr.version_base.Versionable``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``TableTransform`` class. For example:

.. code-block:: python

   table = cr.transform.TableTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   table.apply()

Properties
--------------------------------------------------
The following properties are available to this function:

**input_columns** (list, required)
   Names of the columns that will be consumed from the input table.

**output_columns** (list, required)
   Names of the columns that will be added to the output table.

**keep_other_columns** (bool, required)
   If True, keep the columns that are not in the output_columns list. Default is True.

**name** (str, required)
   Name of the transform (for debugging purposes).

**user_metadata** (dict, required)
   A dictionary containing user metadata to add to the columns generated by the transform. Each key must be an output column name, and the associated value is a dictionary of properties.

   .. note:: Any columns not mentioned will be retained unchanged in the output table unless keep_other_columns is set to False.

Class Fields
--------------------------------------------------
The following class fields are available to this function:

**VERSION** (str, required)
   Transform version identifier. Used to annotate saved transforms. Expected to have format 'x.x.x[.*]', e.g., '2.0.1.this_part_ignored' or '2.0.1'. Subclasses should declare their own VERSION field.

**COMPAT_VERSION** (str, required)
   Oldest version of the class that knows how to load the current version. Used to check for forward compatibility when loading saved transforms with older software versions. Subclasses should declare their own COMPAT_VERSION. For new code, this should be set to the version of the product that first included the new code.


Methods
--------------------------------------------------
The ``TableTransform`` class has the following methods:

* :ref:`api-transform-tabletransform-apply`
* :ref:`api-transform-tabletransform-load-state`
* :ref:`api-transform-tabletransform-migrate-saved-object`
* :ref:`api-transform-tabletransform-output-column-names`
* :ref:`api-transform-tabletransform-save-state`
* :ref:`api-transform-tabletransform-validate-columns`

.. _api-transform-tabletransform-apply:

table.apply()
--------------------------------------------------
Apply a transform to the input data. Subclasses must override this to provide more interesting functionality.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, input)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**input** (cr.data.DataTable, required)
   The input data table. It should contain all the columns listed in this transform's input_columns.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
The cr.data.DataTable that results from the transform. It should contain all of the columns listed in output_columns. It should also contain all original columns not listed in input_columns unless keep_other_columns is set to False.


.. _api-transform-tabletransform-load-state:

table.load_state()
--------------------------------------------------
Load the transform's state from props. Subclasses must override and call:

.. code-block:: python

   super(MyTransform, self).load_state(props)

as the first line to ensure the base class state is also restored.

.. note:: ``migrate_saved_object(props)`` will be called before calling ``load_state()``.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props** (dict, required)
   Dictionary holding the object's state.


.. _api-transform-tabletransform-migrate-saved-object:

table.migrate_saved_object()
--------------------------------------------------
Update a saved object to the latest version, if possible. Subclasses must always override this method. If migrating a saved object is unexpected or unsupported, this method will throw a ``NotImplementedError``.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   migrate_saved_object(cls,
                        saved_object,
                        curr_ver,
                        saved_ver)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**saved_object** (dict, required)
   The saved state of the object.

**curr_ver** (distutils.LooseVersion, required)
   The version of the object being run.

**saved_ver** (distutils.LooseVersion, required)
   The version of the object at save time.


.. _api-transform-tabletransform-output-column-names:

table.output_column_names()
--------------------------------------------------
Return a list of output column names that will be produced by this transform given the ``input_column_names``.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   output_column_names(self, input_column_names)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**input_column_names** (list, required)
   List of input_column_names.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
The output column names, or throws a ValueError if the set of input columns is invalid.


.. _api-transform-tabletransform-save-state:

table.save_state()
--------------------------------------------------
Store this transform's state in props. Subclasses must override and put the following as the first line of the method:

.. code-block:: python

   super(MyTransform, self).save_state(props, for_deployment)

This ensures the base class state is also saved.

.. note:: The keys and values in props must be picklable.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self, props, for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props** (dict, required)
   Dictionary to hold the object's state.

**for_deployment** (bool, required)
   A flag to save a transform for use on the execution server. For example, state may need to be saved from the training data to apply to the incoming data.


.. _api-transform-tabletransform-validate-columns:

table.validate_columns()
--------------------------------------------------
Verify that the input columns in the data are present and that the data doesn't already contain any output columns that aren't also input columns. Throws a ValueError if there is a mismatch.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   validate_columns(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data** (cr.data.DataTable, required)
   An input data table.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
The indices of the input columns.



.. _api-transform-transformpipeline:

TransformPipeline
==================================================
An ordered pipeline of dense-to-dense data transforms. The pipeline can be applied to a cr.data.DataTable to derive a new data table.

Usage
--------------------------------------------------

* Class: ``cr.transform.TransformPipeline``
* Bases: ``cr.version_base.Versionable``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``TransformPipeline`` class. For example:

.. code-block:: python

   tp = cr.transform.TransformPipeline()

and then use that variable to call the individual methods:

.. code-block:: python

   tp.apply()

Arguments
--------------------------------------------------
The following arguments are available to this class:

**debug_path** (str, optional)
   For debugging purposes, specify to save a cr.data.DataTable containing the first few rows after each transform is applied. The tables are stored at path + "/0", "/1", etc. If there were previously files there, they will be overwritten.

**debug_storage** (cr.storage.Storage, optional)
   What storage to use for debug saves. Default is local.

Methods
--------------------------------------------------
The ``TransformPipeline`` class has the following methods:

* :ref:`api-transform-transformpipeline-add-pipeline`
* :ref:`api-transform-transformpipeline-add-transform`
* :ref:`api-transform-transformpipeline-apply`
* :ref:`api-transform-transformpipeline-load`
* :ref:`api-transform-transformpipeline-load-state`
* :ref:`api-transform-transformpipeline-save`
* :ref:`api-transform-transformpipeline-save-state`
* :ref:`api-transform-transformpipeline-validate-columns`
* :ref:`api-transform-transformpipeline-validate-column-names`


.. _api-transform-transformpipeline-add-pipeline:

tp.add_pipeline()
--------------------------------------------------
Append all transforms in the other pipeline to this one.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   add_pipeline(self, other_pipeline)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**other_pipeline** (str, required)
   The name of a pipeline.



.. _api-transform-transformpipeline-add-transform:

tp.add_transform()
--------------------------------------------------
Append the given transform to the end of the pipeline.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   add_transform(self, transform)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**transform** (str, required)
   The name of a transform.



.. _api-transform-transformpipeline-apply:

tp.apply()
--------------------------------------------------
Apply the pipeline of transforms in order to the given data.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data** (cr.data.DataTable, required)
   The input data table. If the input data table does not contain all needed input columns for the pipeline, a ValueError is thrown.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A new cr.data.DataTable that is the output of the transformation pipeline, or the original data if the pipeline contains no transforms.



.. _api-transform-transformpipeline-load:

tp.load()
--------------------------------------------------
Load a pipeline from the specified path.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load(cls,
        path,
        storage='local',
        load_state=NO_STATE,
        **storargs)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**path** (str, required)
   Name of the pipeline file to load. If load_state is not NO_STATE, the path must be a directory.

**storage** (cr.storage.Storage, optional)
   The data store to save to. See cr.storage.create() for more details. Default is cr.storage.Local.

**load_state** (str, optional)
   Whether the individual transforms should attempt to load state and in what form. Allowed values are:

   ``NO_STATE``: Provides a clean transform.

   ``LOAD_FOR_EXECUTION``: Used on the execution server.

   ``LOAD_FOR_UPDATE``: Used to update state on offline servers, such as the cluster.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
The loaded pipeline.



.. _api-transform-transformpipeline-load-state:

tp.load_state()
--------------------------------------------------
Load the pipeline from the given object, migrating from an old version to the current version if necessary.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props** (dict, required)
   The object's properties.



.. _api-transform-transformpipeline-save:

tp.save()
--------------------------------------------------
Save the pipeline to the specified path, defaulting to local storage. This will overwrite any other file at the specified path.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save(self,
        path,
        storage='local',
        for_deployment=False,
        **storargs)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**path** (str, required)
   Name of the output pipeline file if ``for_deployment`` is False, or the name of the storage directory if ``for_deployment`` is True.

**storage** (cr.storage.Storage, optional)
   The data store to save to. See :ref:`api-storage-create` for more details. Default is cr.storage.Local.

**for_deployment** (bool, optional)
   If True, save stateful information so it can be deployed to an execution server or reloaded for later update. If you set this flag to True and storage is local, the state will probably not load properly later.



.. _api-transform-transformpipeline-save-state:

tp.save_state()
--------------------------------------------------
Returns information needed to save this object.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self,
              props,
              for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

**for_deployment** (bool)
   ...

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Nothing, but the props dictionary is updated.


.. _api-transform-transformpipeline-validate-columns:

tp.validate_columns()
--------------------------------------------------
Validate that a DataTable has the correct columns for a TransformPipeline.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   validate_columns(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data** (cr.data.DataTable, required)
   The input data table.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
The column names that will exist at the end of the pipeline. Throws a ValueError if the input table is invalid.


.. _api-transform-transformpipeline-validate-column-names:

tp.validate_column_names()
--------------------------------------------------
Validate that a list of input columns is correct for a TransformPipeline.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   validate_column_names(self,
                         columns)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**columns** (list, required)
   A list of column names.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
The column names that will exist at the end of the pipeline. Throws a ValueError if the list of names is invalid.



.. _api-transform-columnfiltertransform:

ColumnFilterTransform
==================================================
Class for filter that selects columns from a DataTable. Note that all columns that aren't in the list to keep are removed, so the same transform can be used on any table that contains those columns. The columns in the new table will be in the order specified by the list of columns to keep.

Usage
--------------------------------------------------

* Class: ``cr.transform.ColumnFilterTransform``
* Bases: ``cr.transform_base.TableTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``ColumnFilterTransform`` class. For example:

.. code-block:: python

   col = cr.transform.ColumnFilterTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   col.apply()

Arguments
--------------------------------------------------
The following arguments are available to this class:

**cols_to_keep** (list)
   Names of the columns to keep.

**name** (str)
   Name to associate with this transform.

Methods
--------------------------------------------------
The ``ColumnFilterTransform`` class has the following methods:

* :ref:`api-transform-columnfiltertransform-apply`


.. _api-transform-columnfiltertransform-apply:

col.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...



.. _api-transform-rowfiltertransform:

RowFilterTransform
==================================================
Class for transforms that remove rows from the data.

Usage
--------------------------------------------------

* Class: ``cr.transform.RowFilterTransform``
* Bases: ``cr.transform_base.TableTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``RowFilterTransform`` class. For example:

.. code-block:: python

   filter = cr.transform.RowFilterTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   filter.apply()

Methods
--------------------------------------------------
The ``RowFilterTransform`` class has the following methods:

* :ref:`api-transform-rowfiltertransform-apply`
* :ref:`api-transform-rowfiltertransform-load-state`
* :ref:`api-transform-rowfiltertransform-save-state`

Arguments
--------------------------------------------------
The following arguments are available to this class:

**input_columns** (list)
   Input columns needed for the filter.

**name** (str)
   Name to associate with this transform.

**filter** (function)
   Mapping from the input values to True or False. Rows assigned True will be kept; rows assigned False will be removed.


.. _api-transform-rowfiltertransform-apply:

filter.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-rowfiltertransform-load-state:

filter.load_state()
--------------------------------------------------
Load the transform's state from props.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props** (dict, required)
   Dictionary holding the object's state.


.. _api-transform-rowfiltertransform-save-state:

filter.save_state()
--------------------------------------------------
Store this transform's state in props. When saving for deployment, this transform becomes a no-op.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self,
              props,
              for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props** (dict, required)
   Dictionary to hold the object's state.

**for_deployment** (bool, required)
   ...





.. _api-transform-rowtransform:

RowTransform
==================================================
Class for transforms that operate on one row at a time.

Usage
--------------------------------------------------

* Class: ``cr.transform.RowTransform``
* Bases: ``cr.transform_base.TableTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``RowTransform`` class. For example:

.. code-block:: python

   row = cr.transform.RowTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   row.apply()

Arguments
--------------------------------------------------
The following arguments are available to this class:

**input_columns** (list, required)
   Columns that will be used to generate the new row.

**output_columns** (list, required)
   Columns to add to the row. This can either be a list of column names or a list of cr.datatable.ColumnMetadata if the types are known ahead of time. Currently, only name and type properties of column metadata are propagated to the new DataTable.

**name** (str, required)
   Name to associate with this transform.

**mapper** (function, required)
   Mapping function from the input values to a tuple of output values. That is, for n input columns, and m output columns, the function should take n parameters and return a tuple of length m.

**consume_input** (bool, optional)
   If True, consume the input columns. Default is False.

Methods
--------------------------------------------------
The ``RowTransform`` class has the following methods:

* :ref:`api-transform-rowtransform-apply`
* :ref:`api-transform-rowtransform-load-state`
* :ref:`api-transform-rowtransform-save-state`

.. _api-transform-rowtransform-apply:

row.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-rowtransform-load-state:

row.load_state()
--------------------------------------------------
Load the transform's state from props.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props** (dict, required)
   Dictionary holding the object's state.


.. _api-transform-rowtransform-save-state:

row.save_state()
--------------------------------------------------
Store this transform's state in props.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self, props, for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props** (dict, required)
   Dictionary to hold the object's state.

   .. note:: The keys and values in props must be picklable.





.. _api-transform-datetimetransform:

DatetimeTransform
==================================================
Given a representation of a date/time (datetime.datetime, unix time, ISO 8601 string, or something else that can be parsed as time), extract some important elements to be used as features (year, weekday, etc). If a timezone is not provided, the time is interpreted as UTC. In these cases, it might be useful to include a time-location conjunction feature.

Usage
--------------------------------------------------

* Class: ``cr.transform.DatetimeTransform``
* Bases: ``cr.transform_base.RowTransform``

Arguments
--------------------------------------------------
The following arguments are available to this function:

**input_column** (str, required)
   Name of input column to transform.

**output_column_prefix** (str, optional)
   Prefix for output column names. Output column names are made up of the prefix, an "_", and the values in OUTPUT_COLUMN_NAMES.

**format_string** (str, optional)
   Format of the date column as a specification for datetime.datetime.strptime().

**name** (str, optional)
   Name of the transform.

**only** (list, optional)
   List of values from OUTPUT_COLUMN_NAMES to compute. If unspecified, all are computed. For example:

   .. code-block:: python

      OUTPUT_COLUMN_NAMES() = ['Year', 'Month', 'Hour', 'Weekday', 'UTCOffset']


.. _api-transform-urltransform:

URLTransform
==================================================
Extract from a URL (or the contents of a GET request) various pieces of information, such as the protocol, query string, etc. The query string (if any) will always be returned as a JSON-serialized list of key-value pairs. If query_whitelist is a list of key names, then a new column will be created for each of the provided keys.

Usage
--------------------------------------------------

* Class: ``cr.transform.URLTransform``
* Bases: ``cr.transform_base.RowTransform``

Arguments
--------------------------------------------------
The following arguments are available to this function:

**input_column** (str, required)
   Name of the column to apply the transform to.

**output_column_prefix** (str, optional)
   Prefix for output column names. Output column names are made up of the prefix, an "_", and the values in OUTPUT_COLUMN_NAMES. Default is None.

**query_whitelist** (list, optional)
   A column is created for each name in this list that marks whether the query is in the URL.

**name** (str, optional)
   Name of the transform. Default is None.

   DEFAULT_ROW() = (None, None, None, None, None, None, None, None, None, None, None)

   NON_STR_OUTPUTS() = {'depth': <type 'int'>, 'port': <type 'int'>}

   OUTPUT_COLUMN_NAMES() = ['host', 'domain', 'regdomain', 'tld', 'port', 
                            'protocol', 'page_url', 'query', 'depth', 
                            'mime_type', 'mime_enc']

   i(x=0) = 10



.. _api-transform-useragentparsertransform:

UserAgentParserTransform
==================================================
Extract from a user agent string various pieces of information, such as the browser version and family, OS version and family, etc.

Usage
--------------------------------------------------

* Class: ``cr.transform.UserAgentParserTransform``
* Bases: ``cr.transform_base.RowTransform``

Arguments
--------------------------------------------------
The following arguments are available to this function:

**input_column** (str, required)
   Name of the column that has user agent string.

**output_column_prefix** (str, required)
   Prefix for output column names. Output column names are made up of the prefix, an "_", and the values in OUTPUT_COLUMN_NAMES. Default is None.

**name** (str, required)
   Name of the transform. Default is None.

   NON_STR_OUTPUTS() = {'is_pc': <type 'int'>, 'is_tablet': <type 'int'>,
                        'is_mobile': <type 'int'>, 'is_touch_capable': <type 'int'>,
                        'is_bot': <type 'int'>}

   OUTPUT_COLUMN_NAMES() = ['browser_family', 'browser_version_string',
                            'os_family', 'os_version_string', 'device_family',
                            'device_brand', 'device_model', 'is_mobile', 'is_tablet',
                            'is_pc', 'is_touch_capable', 'is_bot']



.. _api-transform-numbertocategorytransform:

NumberToCategory
==================================================
Replace a continuous (int or float) column, with a bucketized column. It can work with ClusterStringInputEncoder to handle formerly continuous columns.

Usage
--------------------------------------------------
 
* Class: ``cr.transform.NumberToCategoryTransform``
* Bases: ``cr.transform_base.RowTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``NumberToCategoryTransform`` class. For example:

.. code-block:: python

   ntc = cr.transform.NumberToCategoryTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   ntc.apply()

Arguments
--------------------------------------------------
The following arguments are available to this function:

**args_col_bins_dict** (dict, required)
   A dictionary mapping column names to a tuple. The second element of the tuple are the fenceposts for the bins. See calculate_col_bins for an explanation. The first part of the tuple are the quantize arguments used to create those bins. The reason for including the arguments in the tuple is to make some code paths unified between the early and late paths, and the important part of those arguments is the column name and the output column so we can rely on the base class to make sure we don't reference missing columns or generate duplicate columns. The dictionary can be None, in which case you must provide quantize_args_list.

   .. note:: The col_bins could have been computed by some mechanism different from the calculate_col_bins() class method.

**quantize_args_list** (list, required)
   Allows the transform to defer calculating the col_bins until the time of the first apply. Must be present if args_col_bins_dict is None; must not be present if args_col_bins_dict is not None. These are the keyword arguments to be passed to the calculate_col_bins() at the time of the first apply. The arguments are described in the documentation for that function. The reason we support this is in case you want to apply a NumberToCategory transform in a pipeline with it using the DataTable present at that point after the earlier part of the pipeline has run.

**name** (str, optional)
   Name of this transform.

**consume_input** (bool, optional)
   If True, include the input column and the output column; False to include only the output column. Default is False.

Methods
--------------------------------------------------
The ``NumberToCategoryTransform`` class has the following methods:

* :ref:`api-transform-numbertocategorytransform-apply`
* :ref:`api-transform-numbertocategorytransform-calculate-col-bins`
* :ref:`api-transform-numbertocategorytransform-load-state`
* :ref:`api-transform-numbertocategorytransform-save-state`


.. _api-transform-numbertocategorytransform-apply:

ntc.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-numbertocategorytransform-calculate-col-bins:

ntc.calculate_col_bins()
--------------------------------------------------
Calculate the quantization for multiple columns simultaneously, and returns a dictionary mapping the output columns names to the quantization. See calculate_col_bins_one(), which operates on a single column, for more details.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   calculate_col_bins(cls, data, quantize_args_list)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...

**quantize_args_list**
   ...


.. _api-transform-numbertocategorytransform-load-state:

ntc.load_state()
--------------------------------------------------
Load the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...


.. _api-transform-numbertocategorytransform-save-state:

ntc.save_state()
--------------------------------------------------
Save the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self, props, for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

**for_deployment**
   ...




.. _api-transform-quantizeargs:

QuantizeArgs
==================================================
Struct-like class for expressing the arguments sufficient to quantize a single int or float column.

Usage
--------------------------------------------------

* Class: ``cr.transform.QuantizeArgs``
* Bases: object

Arguments
--------------------------------------------------
The following arguments are available to this class:

**column** (str, required)
   Name of the column to bin that must be of type float or int.

**numbins** (int, optional)
   The number of bins, as defined by a number of fenceposts. Each bin is defined as in between two of the fenceposts with two implicit fenceposts of -inf and inf on each end. Default is 15.

**addNone** (bool, optional)
   If True, add an extra bin at the start for Nones. Otherwise, the rows with None will be included in another bin that also has non-None values. Default is False.

**strategy** (special, optional)
   Quantization strategy (algorithm), similar to continuous target discStrategy. Only QUANTILE_STRATEGY ('quantiles') supported for now.

**output_column** (str, optional)
   Name of the column to output. If not specified, the name of the input column + '_quantized' is used.



.. _api-transform-findreplacetransform:

FindReplaceTransform
==================================================
Transform for replacing strings with other strings.

Usage
--------------------------------------------------
 
* Class: ``cr.transform.FindReplaceTransform``
* Bases: ``cr.transform_base.TableTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``FindReplaceTransform`` class. For example:

.. code-block:: python

   fr = cr.transform.FindReplaceTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   fr.apply()

Arguments
--------------------------------------------------
The following arguments are available to this function:

**substitutions** (list, required)
   List of find-replace substitutions to make when applying this transform. Each substitution is defined by a (key, policy) tuple. What the key looks like can be complicated, but in the simplest case, it is a single column name. Values in the columns named by the key are looked up in the policy and replaced by the associated replacement value. If there is no match in the policy the value is left unchanged.

   A policy is either a dictionary or a list of regular expressions with replacement values.

   The key can be a single column name, in which case the policy is applied to the value of the single column. It can also be a tuple containing 1 or more columns, in which case the policy applies to multiple columns. For example, if the key is "foo_column_name", then the dictionary policy will look like {"foo_value1": "foo_value1_prime"}.

   If the key were ("foo_column_name", "bar_column_name"), then the dictionary policy would look like {("foo_value1", "bar_value1"): ("foo_value1_prime", "bar_value1_prime")}. Remember they need to be tuples, not lists, because lists are not invariant or hashable and so not suitable for dictionary keys.

**name** (str, required)
   User-defined name for this transform.

Methods
--------------------------------------------------
The ``FindReplaceTransform`` class has the following methods:

* :ref:`api-transform-findreplacetransform-apply`
* :ref:`api-transform-findreplacetransform-load-state`
* :ref:`api-transform-findreplacetransform-save-state`


.. _api-transform-findreplacetransform-apply:

fr.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-findreplacetransform-load-state:

fr.load_state()
--------------------------------------------------
Load the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...


.. _api-transform-findreplacetransform-save-state:

fr.save_state()
--------------------------------------------------
Save the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self,
              props,
              for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

**for_deployment**
   ...




.. _api-transform-propertysettransform:

PropertySetTransform
==================================================
Transform for adding a representation of properties to a table. Typically the transform is created with the create() method which builds a PropertySetState object needed to initialize the transform from a data table parameter.

The property set may have thousands of properties in it which are converted to a single column containing a string encoding of a sparse matrix row.

Each property represents a particular input being set to a particular value in a particular context.

DEFAULT_THRESHOLD_COUNT(x=0) = 100

Usage
--------------------------------------------------

* Class: ``cr.transform.PropertySetTransform``
* Bases: ``cr.transform_base.RowTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``PropertySetTransform`` class. For example:

.. code-block:: python

   ps = cr.transform.PropertySetTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   ps.apply()

Methods
--------------------------------------------------
The ``PropertySetTransform`` class has the following methods:

* :ref:`api-transform-propertysettransform-apply`
* :ref:`api-transform-propertysettransform-create`
* :ref:`api-transform-propertysettransform-load-state`
* :ref:`api-transform-propertysettransform-save-state`

.. _api-transform-propertysettransform-apply:

ps.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-propertysettransform-create:

ps.create()
--------------------------------------------------
Create a PropertySetTransform.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   create(input_columns=None,
          output_column=None,
          context_columns=None,
          wildcard_context_column=None,
          source_data=None,
          threshold_count=DEFAULT_THRESHOLD_COUNT,
          max_properties=None,
          name=None,
          token_delimiter=None)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**input_columns** (list, optional)
   Columns to collect properties from. Default is None.

**output_column** (str, optional)
   New column name. Default is None.

**context_columns** (list, optional)
   Columns to use for context. Default is None.

**wildcard_context_column** (str, optional)
   Name of a single context column to generate wildcard entries for. This adds properties with that column allowed to be any value in the context. Default is None.

**source_data** (cr.data.DataTable, optional)
   Data table to collect properties from. Any properties not in this initial table will be ignored when the transform is run later. Default is None.

**threshold_count** (int, optional)
   Minimum number of times a property must appear to be in the property set.

**max_properties** (int, optional)
   Maximum size of the property set. The properties that occur the most are kept if the set must be trimmed.

**name** (str, optional)
   Name to use for the transform

**token_delimiter** (str, optional)
   Optional. If present, each table cell will be split on this delimiter in order to generate multiple properties from a single cell.


.. _api-transform-propertysettransform-load-state:

ps.load_state()
--------------------------------------------------
Load the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...


.. _api-transform-propertysettransform-save-state:

ps.save_state()
--------------------------------------------------
Save the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self, props, for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

**for_deployment**
   ...



.. _api-transform-propertysetstate:

PropertySetState
==================================================
This class uses a data table and list of columns to build state for a PropertySetTransform. All of the input columns are scanned for property values which are further multiplexed based on the context columns. The input arguments are exactly the same as those in PropertySetTransform.create.

Usage
--------------------------------------------------

* Class: ``cr.transform.PropertySetState``
* Bases: object

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``PropertySetState`` class. For example:

.. code-block:: python

   pss = cr.transform.PropertySetState()

and then use that variable to call the individual methods:

.. code-block:: python

   pss.apply()

Methods
--------------------------------------------------
The ``PropertySetState`` class has the following methods:

* :ref:`api-transform-propertysetstate-from-json`
* :ref:`api-transform-propertysetstate-to-json`
* :ref:`api-transform-propertysetstate-validate-table`


.. _api-transform-propertysetstate-from-json:

pss.from_json()
--------------------------------------------------
Convert a JSON-formatted string into PropertySetState metadata.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   from_json(cls, in_json)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**in_json** (str, required)
   String in JSON format, previously retrieved via ``PropertySetState.to_json()``.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A PropertySetState object.


.. _api-transform-propertysetstate-to-json:

pss.to_json()
--------------------------------------------------
Convert the PropertySetState metadata into a JSON-formatted string.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   to_json(self)

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A string in JSON format.



.. _api-transform-propertysetstate-validate-table:

pss.validate_table()
--------------------------------------------------
Check whether a particular datatable has the correct column names to be appropriate for this state object.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   validate_table(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...




.. _api-transform-rollingwindowtransform:

RollingWindowTransform
==================================================
Class for transforms that add data from previous events in a time sequence to the current row. Note that if an instance of this class is saved for deployment it will store the most recently calculated value for each key in the transform. When it is reloaded it will use that map (and default value provided) to perform transforms on new rows. Current reloading a deployed transform requires that the keys be strings, ints, longs, None or a tuple of any combination of these types. It also does not support re-saving a previously saved transform.

Usage
--------------------------------------------------
 
* Class: ``cr.transform.RollingWindowTransform``
* Bases: ``cr.transform_base.TableTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``RollingWindowTransform`` class. For example:

.. code-block:: python

   rw = cr.transform.RollingWindowTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   rw.apply()

Arguments
--------------------------------------------------
The following arguments are available to this function:

**timestamp_column** (str, required)
   Name of the column that contains timestamps.

**time_periods** (list, required)
   List of aggregation interval pairs of datetime.timedelta type. Within each pair, the first timedelta specifies when to start relative to the current time, and the second timedelta specifies when to stop relative to the current time. For example, to get the previous day the interval would be (datetime.timedelta(days=-1), datetime.timedelta(0)). By default, the left end of the interval is closed and the right end is open.

**aggregation_columns** (list, required)
   Names of the columns that contain data to aggregate.

**new_columns** (list, required)
   Names of the new columns to generate. This should be a multiple (usually 1) of the number of ``time_periods``.

**aggregation_function** (lambda, required)
   Function from old columns to new columns. The input to this function will be a list of tuples that are in the appropriate time range containing only the data from the aggregation columns. The output should be a tuple of the appropriate length.

**group_by_columns** (list, required)
   Segment events into groups and compute rolling window statistics for each group separately. Each unique set of values across all the columns forms a group.

**include_start_timestamp** (bool, optional)
   If True, include events exactly matching the start time of the interval. Default is True.

**include_finish_timestamp** (bool, optional)
   If True, include events exactly matching the finish time of the interval. Default is False.

**name** (str, optional)
   Name of the transform.

**default_data** (list, optional)
   A list of values to use as a default when the deployed transform lookup doesn't contain the proper key (the lookup table is used when this transform is deployed).


Methods
--------------------------------------------------
The ``RollingWindowTransform`` class has the following methods:

* :ref:`api-transform-rollingwindowtransform-apply`
* :ref:`api-transform-rollingwindowtransform-load-state`
* :ref:`api-transform-rollingwindowtransform-save-state`


.. _api-transform-rollingwindowtransform-apply:

rw.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-rollingwindowtransform-load-state:

rw.load_state()
--------------------------------------------------
Load the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...



.. _api-transform-rollingwindowtransform-save-state:

rw.save_state()
--------------------------------------------------
Save the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self, props, for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

**for_deployment**
   ...




.. _api-transform-rollingwindowbycounttransform:

RollingWindowByCount
==================================================
Transform for doing a rolling window where instead of collecting data from a span of time you collect a particular number of rows before and/or after the current row.

Usage
--------------------------------------------------

* Class: ``cr.transform.RollingWindowByCountTransform``
* Bases: ``cr.transform_timeseries.RollingWindowTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``RollingWindowByCountTransform`` class. For example:

.. code-block:: python

   rwbc = cr.transform.RollingWindowByCountTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   rwbc.apply()

Arguments
--------------------------------------------------
The following arguments are available to this function:

**timestamp_column** (str, required)
   Name of the column that contains timestamps.

**row_counts** (list, required)
   List of pairs of rows to include in the interval. Negative numbers count backward and positive numbers count forward. The interval includes the first number, but not the second. For example, (-2, 3) means include two rows backward, the current row, and two rows forward. (-2, 0) would include two rows backward and not the current row. When there aren't enough rows to fill the range, the available rows are used. Note that if there are multiple rows with the same timestamp, it is arbitrary what order they will be put in.

**aggregation_columns** (list, required)
   Names of the columns that contain data to aggregate.

**new_columns** (list, required)
   Names of the new columns to generate. This should be a multiple (usually 1) of the length of row_counts.

**aggregation_function** (lambda, required)
   Function from old columns to new columns. The input to this function will be a list of tuples that are in the appropriate range containing only the data from the aggregation columns. The output should be a tuple of the appropriate length.

**group_by_columns** (list, required)
   Segment events into groups and compute statistics for each group separately. Each unique set of values across all the columns forms a group.

**name** (str, optional)
   Name of the transform.

Methods
--------------------------------------------------
The ``RollingWindowByCountTransform`` class has the following methods:

* :ref:`api-transform-rollingwindowbycounttransform-apply`


.. _api-transform-rollingwindowbycounttransform-apply:

rwbc.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...



.. _api-transform-sessionizationtransform:

SessionizationTransform
==================================================
Transform for breaking a timestream into sessions. For each event in time sequence, a new session is started if it has been sufficient time since the last event. Otherwise, it is added to the current session.

Usage
--------------------------------------------------

* Class: ``cr.transform.SessionizationTransform``
* Bases: ``cr.transform_timeseries.RollingWindowTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``SessionizationTransform`` class. For example:

.. code-block:: python

   st = cr.transform.SessionizationTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   st.apply()

Arguments
--------------------------------------------------
The following arguments are available to this function:

**timestamp_column** (str, required)
   Name of the column that contains timestamps.

**time_periods** (list, required)
   List of sessionization intervals (in seconds). A new session starts if more than this amount of time has passed since the last transaction.

**aggregation_columns** (list, required)
   Names of the columns that contain data to aggregate.

**new_columns** (list, required)
   Names of the new columns to generate. This should be a multiple (usually 1) of the number of ``time_periods``.

**aggregation_function** (lambda, required)
   Function from old columns to new columns. The input to this function will be a list of tuples that are in the appropriate time range containing only the data from the aggregation columns. The output should be a tuple of the appropriate length.

**group_by_columns** (list, required)
   Segment events into groups and compute sessionization statistics for each group separately. Each unique set of values across all the columns forms a group.

**name** (str, optional)
   Name of the transform.

Methods
--------------------------------------------------
The ``SessionizationTransform`` class has the following methods:

* :ref:`api-transform-sessionizationtransform-apply`

.. _api-transform-sessionizationtransform-apply:

st.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-fillmissingrowstransform:

FillMissingRowsTransform
==================================================
Transform for inserting rows corresponding to "nothing happened" at a particular time, when rows exist for all the times something did happen. The DataTable must contain a timestamp column with evenly spaced timestamps. For example, if data is "daily", all timestamps must have the same hour/minute/second values (which would usually all be zero). There also must not be duplicate entries for a given timestamp with the same group by keys.

Usage
--------------------------------------------------

* Class: ``cr.transform.FillMissingRowsTransform``
* Bases: ``cr.transform_base.TableTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``FillMissingRowsTransform`` class. For example:

.. code-block:: python

   fmr = cr.transform.FillMissingRowsTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   fmr.apply()

Arguments
--------------------------------------------------
The following arguments are available to this class:

**timestamp_column** (str, required)
   Name of the column that contains timestamps.

**resolution** (datetime.timedelta, required)
   Resolution of the data. For example, datetime.timedelta(days=1).

**group_by_columns** (list, required)
   List of columns to group by. For each set of values in these columns we generate the rows for all dates in range. By default no rows are generated for combinations not already in the data.

**start_timestamp** (datetime.datetime, optional)
   First timestamp that should appear in the output. If None, then the fill starts for each group from the first example for that group. Default is None.

**end_timestamp** (datetime.datetime, optional)
   Last timestamp that should appear in the output. If None, then the fill stops for each group from the last example for that group. Default is None.

**use_previous** (list, optional)
   List of columns which should be filled using the previous value. If both this and fill are set for a column, this takes precedence, but the fill value is still used for rows prior to the first observation.

**fill** (list of tuples, optional)
   Each item is a column name and default value to use. Any columns not mentioned in the fill (other than timestamp and group_by_columns) will be filled with None. Default is None.

**add_all_combinations** (bool, optional)
   If True, use all combinations of values in the group_by_columns, even those that don't appear in the data. If this is True, and start_timestamp and/or end_timestamp is None, then the earliest/latest timestamp that exists in the data is used for the start_timestamp/end_timestamp. Default is False.

Methods
--------------------------------------------------
The ``FillMissingRowsTransform`` class has the following methods:

* :ref:`api-transform-fillmissingrowstransform-apply`
* :ref:`api-transform-fillmissingrowstransform-load-state`
* :ref:`api-transform-fillmissingrowstransform-migrate-saved-object`
* :ref:`api-transform-fillmissingrowstransform-save-state`


.. _api-transform-fillmissingrowstransform-apply:

fmr.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...



.. _api-transform-fillmissingrowstransform-load-state:

fmr.load_state()
--------------------------------------------------
Load the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...


.. _api-transform-fillmissingrowstransform-migrate-saved-object:

fmr.migrate_saved_object()
--------------------------------------------------
See TableTransform.migrate_saved_object() for details.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   migrate_saved_object(cls,
                        saved_object,
                        curr_ver,
                        saved_ver)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**saved_object**
   ...

**curr_ver**
   ...

**saved_ver**
   ...


.. _api-transform-fillmissingrowstransform-save-state:

fmr.save_state()
--------------------------------------------------
Save the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self, props, for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

**for_deployment**
   ...




.. _api-transform-lookuptransform:

LookupTransform
==================================================
Class for transforms that look up data in an auxiliary table to associate with the data table, where the auxiliary table has one row for each instance of the key value in the data table.

Usage
--------------------------------------------------
 
* Class: ``cr.transform.LookupTransform``
* Bases: ``cr.transform_base.TableTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``LookupTransform`` class. For example:

.. code-block:: python

   lt = cr.transform.LookupTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   lt.apply()

Arguments
--------------------------------------------------
The following arguments are available to this function:

**lookup_keys** (list, required)
   List of column names that are the keys for auxiliary data.

**auxiliary_table** (cr.data.DataTable, required)
   Auxiliary table.

**auxiliary_columns** (list, required)
   Columns to get from auxiliary table

**auxiliary_keys** (list, optional)
   List of the key column names in the auxiliary data. Only needed if different than key column names in the data table.

**output_columns** (list, optional)
   Columns to use in the merged data. Must be the same length as auxiliary_columns. If not specified, the auxiliary_columns are used as the names of the output columns.

**default_data** (tuple, optional)
   Default values to use if there is not auxiliary entry for a row in the data table. If not set, an exception will be raised if the value isn't there. Must be the same length as auxiliary_columns.

**cleanup_function** (lambda, optional)
   Function to apply to added data. It accepts a tuple of the values looked up in the auxiliary table and returns a tuple to use to fill in the data table. Useful for turning None into a different value, for example. Must produce a tuple of the same length as auxiliary_columns.

**duplicates_are_errors** (bool, optional)
   If True, treat duplicates in the auxiliary tables as errors. If False, the transform will arbitrarily pick one lookup value to join against.

**force_join_mode** (str, optional)
   A string indicating the join mode to use. If not specified, the transform will decide whether to do the shuffle or ship the entire auxiliary table to each shard of the main table. The decisions of which join type to use requires calling num_rows on the auxiliary data table, so materialization is forced if mode isn't specified. The only currently supported modes are:

   ``SHUFFLE_JOIN``: Causes a shuffle on both the table to which the transform is applied and the auxiliary data table. This method is fully lazy.

   ``LOOKUP_JOIN``: Causes the auxiliary table to be treated as a dictionary, not even checking the size with num_rows. Specifying this option will materialize the auxiliary table in the class constructor. This is still better than having this option computed, which causes two materializations of this table, one just to count the rows.

Methods
--------------------------------------------------
The ``LookupTransform`` class has the following methods:

* :ref:`api-transform-lookuptransform-apply`
* :ref:`api-transform-lookuptransform-load-state`
* :ref:`api-transform-lookuptransform-save-state`


.. _api-transform-lookuptransform-apply:

lt.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...

.. _api-transform-lookuptransform-load-state:

lt.load_state()
--------------------------------------------------
Load the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

.. _api-transform-lookuptransform-save-state:

lt.save_state()
--------------------------------------------------
Save the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self, props, for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

**for_deployment**
   ...


.. _api-transform-collapserowstransform:

CollapseRowsTransform
==================================================
Transform to collapse and aggregate all rows sharing a common key or keys in their "group_by columns" to a single row per unique grouping.

All original columns except the group_by_columns are discarded, so the new DataTable contains only the group_by_columns and the outputs of the aggregations.

Each aggregation specifies which columns are used, what function to apply, and what new columns are generated. An original column can appear in zero, one, or more of the aggregations.

Usage
--------------------------------------------------

* Class: ``cr.transform.CollapseRowsTransform``
* Bases: ``cr.transform_base.TableTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``CollapseRowsTransform`` class. For example:

.. code-block:: python

   cr = cr.transform.CollapseRowsTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   cr.apply()

Arguments
--------------------------------------------------
The following arguments are available to this class:

**group_by_columns** (list, required)
   List of the columns that jointly specify the keys to segment the rows. Each unique set of values across all the columns in this list forms a group.

**aggregations** (list, required)
   A list of tuples of the form (column_list, aggregation_function, column_names). For each group, all of the entries from the column list are passed to the aggregation function which must return a number of output columns equal in length to the length of column_names.

**name** (str, optional)
   Name of the transform.


Methods
--------------------------------------------------
The ``CollapseRowsTransform`` class has the following methods:

* :ref:`api-transform-collapserowstransform-apply`
* :ref:`api-transform-collapserowstransform-load-state`
* :ref:`api-transform-collapserowstransform-save-state`

.. _api-transform-collapserowstransform-apply:

cr.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-collapserowstransform-load-state:

cr.load_state()
--------------------------------------------------
Load state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...


.. _api-transform-collapserowstransform-save-state:

cr.save_state()
--------------------------------------------------
Save the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self, props, for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

**for_deployment**
   ...


.. _api-transform-collapserowswithreducetransform:

CollapseRowsWithReduce
==================================================
Transform to collapse and aggregate all rows sharing a common key or keys in their group_by_columns to a single row per unique grouping.

This differs from CollapseRowsTransform because it summarizes smaller sets of data, merges the summarizations and then calculates the new columns from the merged summarizations. This approach will generally make more efficient use of cluster resources, but might require some approximations to gain the efficiencies (for example, if you want to calculate a quantile).

It has a lot of overlap with CollapseRowsTransforms and in many cases it can produce similar results if ListReducer is used, we keep them both (for now) to avoid incompatibilities in serialization/deserialization.

All original columns except the group_by_columns are discarded, so the new DataTable contains only the group_by_columns and the outputs of the aggregations.

Each aggregation specifies which columns are used, what function to apply, and what new columns are generated. An original column can appear in zero, one, or more of the aggregations.

Usage
--------------------------------------------------

* Class: ``cr.transform.CollapseRowsWithReduceTransform``
* Bases: ``cr.transform_base.CollapseRowsWithReduceTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``CollapseRowsWithReduceTransform`` class. For example:

.. code-block:: python

   crwr = cr.transform.CollapseRowsWithReduceTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   crwr.apply()

Methods
--------------------------------------------------
The ``CollapseRowsWithReduceTransform`` class has the following methods:

* :ref:`api-transform-collapserowswithreducetransform-apply`
* :ref:`api-transform-collapserowswithreducetransform-load-state`
* :ref:`api-transform-collapserowswithreducetransform-save-state`


.. _api-transform-collapserowswithreducetransform-apply:

crwr.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-collapserowswithreducetransform-load-state:

crwr.load_state()
--------------------------------------------------
Load state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...


.. _api-transform-collapserowswithreducetransform-save-state:

crwr.save_state()
--------------------------------------------------
Save state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self, props, for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

**for_deployment**
   ...



.. _api-transform-removeduplicaterowstransform:

RemoveDuplicateRows
==================================================
Transform for removing duplicate rows from a DataTable. The Transform also allows users to explicitly specify a subset of columns from a DataTable that should be used as keys to find and remove duplicates. See RemoveDuplicateRowsTransform.create() for usage.

.. note:: The correct usage for this Transform is:

   .. code-block:: python

      T = RemoveDuplicateRowsTransform.create(...)

   and NOT:

   .. code-block:: python

      T = RemoveDuplicateRowsTransform(...)

   That is to say, direct instantiation of the Transform class is discouraged. This is because we want to infer certain parameters from a DataTable provided to the Transform a priori.

Usage
--------------------------------------------------

* Class: ``cr.transform.RemoveDuplicateRowsTransform``
* Bases: ``cr.transform_base.TableTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``RemoveDuplicateRowsTransform`` class. For example:

.. code-block:: python

   rdr = cr.transform.RemoveDuplicateRowsTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   rdr.apply()

Methods
--------------------------------------------------
The ``RemoveDuplicateRowsTransform`` class has the following methods:

* :ref:`api-transform-removeduplicaterowstransform-apply`
* :ref:`api-transform-removeduplicaterowstransform-create`
* :ref:`api-transform-removeduplicaterowstransform-load-state`
* :ref:`api-transform-removeduplicaterowstransform-save-state`


.. _api-transform-removeduplicaterowstransform-apply:

rdr.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-removeduplicaterowstransform-create:

rdr.create()
--------------------------------------------------
Create a RemoveDuplicateRowsTransform, which concatenates column-values per row, and generates an MD5 hash that is used to determine duplicate rows and collapse them into a single row.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   create(data, key_columns=None, name=None)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**key_columns** (list of str, required)
   The list of columns to use to produce the column-values per row. If no value is provided, all columns are used.

**name** (str, required)
   Name of the transform.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A RemoveDuplicateRowsTransform.



.. _api-transform-removeduplicaterowstransform-load-state:

rdr.load_state()
--------------------------------------------------
Load the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...


.. _api-transform-removeduplicaterowstransform-save-state:

rdr.save_state()
--------------------------------------------------
Save the state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self, props, for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

**for_deployment**
   ...



.. _api-transform-reshapewidetransform:

ReshapeWideTransform
==================================================
Transform for flattening multiple rows that share a key into a single row with one or more columns from each of the old rows.

Usage
--------------------------------------------------

* Class: ``cr.transform.ReshapeWideTransform``
* Bases: ``cr.transform_base.TableTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``ReshapeWideTransform`` class. For example:

.. code-block:: python

   rw = cr.transform.ReshapeWideTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   rw.apply()

Arguments
--------------------------------------------------
The following arguments are available to this class:

**group_by_columns** (list, required)
   List of columns to group by. We flatten each set of rows that matches all values in these columns into a single row.

**category_columns** (list, required)
   Columns used to tag the data columns.

**data_columns** (list, required)
   Columns to associate with the category columns.

**category_values** (list, required)
   Values to look for in the category columns that will be used to make new columns in the output. The list elements are tuples with the same length as the number of category columns.

**default_values** (list of tuples, optional)
   Each item is a column name from the list of data_columns and a default value to use. Any columns not mentioned will be filled with None. Default is None.

**category_name_column** (boolean, optional)
   If True, make a column that includes the name of the category. Default is True.

**name** (str, optional)
   Name of the transform.


Methods
--------------------------------------------------
The ``ReshapeWideTransform`` class has the following methods:

* :ref:`api-transform-reshapewidetransform-apply`
* :ref:`api-transform-reshapewidetransform-create`
* :ref:`api-transform-reshapewidetransform-load-state`
* :ref:`api-transform-reshapewidetransform-save-state`


.. _api-transform-reshapewidetransform-apply:

rw.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-reshapewidetransform-create:

rw.create()
--------------------------------------------------
Create a new ReshapeWideTransform. See ReshapeWideTransform for argument descriptions.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   create(data,
          group_by_columns,
          category_columns,
          data_columns,
          max_distinct=100,
          min_frequency=1,
          default_values=None,
          category_name_column=True,
          name=None)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**group_by_columns**
   ...

**category_columns**
   ...

**data_columns**
   ...

**max_distinct**
   ...

**min_frequency**
   ...

**default_values**
   ...

**category_name_column**
   ...

**name**
   ...


.. _api-transform-reshapewidetransform-load-state:

rw.load_state()
--------------------------------------------------
Use to load the transform state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, props)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...


.. _api-transform-reshapewidetransform-save-state:

rw.save_state()
--------------------------------------------------
Use to save the transform state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self, props, for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**props**
   ...

**for_deployment**
   ...


Example
--------------------------------------------------

.. code-block:: python

   T = cr.transform.ReshapeWideTransform.create(
       data=data,
       group_by_columns=['date'],
       category_columns=['index'],
       data_columns=['open_value', 'close_value'],
       default_values=[('open_value', 1000)],
       name='reshape_test')

   new_data = T.apply(data)



.. _api-transform-statestrategy:

StateStrategy
==================================================
An interface for a strategy that works with StatefulRollingWindowTransform. The interface will call new_state if a state can't be found for a pre-existing key. Once it has a state object for a particular key, it guarantees that update_state_and_get_values() is called with data points that are in a non-decreasing chronological order. Keys are separated and sorted by time. When using a timedelta for a forward looking strategy, trim_state_and_get_values() is called after every update to the state object to ensure the data being considered is within the given time range.

Usage
--------------------------------------------------

* Class: ``cr.transform.StateStrategy``
* Bases: ``cr.version_base.Versionable``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``StateStrategy`` class. For example:

.. code-block:: python

   ss = cr.transform.StateStrategy()

and then use that variable to call the individual methods:

.. code-block:: python

   ss.apply()

Methods
--------------------------------------------------
The ``StateStrategy`` class has the following methods:

* :ref:`api-transform-statestrategy-collapse-state`
* :ref:`api-transform-statestrategy-explode-state`
* :ref:`api-transform-statestrategy-new-state`
* :ref:`api-transform-statestrategy-trim-state-and-get-values`
* :ref:`api-transform-statestrategy-update-state-and-get-values`

.. _api-transform-statestrategy-collapse-state:

ss.collapse_state()
--------------------------------------------------
Return a collapsed form (less memory required) of the state object, to keep it around when it isn't being actively used. The expected return type is a binary string. The default implementation is to cPickle the state object. Implementers should override this method if possible to use a format that is readable by native code if possible.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   collapse_state(self, state)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**state** (object, required)
   An object representing pre-existing state. Implementations may mutate this object for efficiency.


.. _api-transform-statestrategy-explode-state:

ss.explode_state()
--------------------------------------------------
The inverse of collapse state. Prepare the state to be ready for calculations. Could be renamed to deserialize.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   explode_state(self, state)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**state** (object, required)
   An object representing pre-existing state. Implementations may mutate this object for efficiency.



.. _api-transform-statestrategy-new-state:

ss.new_state()
--------------------------------------------------
A state factory method for when no prior state is available.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   new_state(self)



.. _api-transform-statestrategy-trim-state-and-get-values:

ss.trim_state_and_get_values()
--------------------------------------------------
Take a cutoff timestamp and a state object and provide updated values.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   trim_state_and_get_values(self,
                             timestamp,
                             state)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**timestamp** (datetime.timedelta, required)
   The timestamp obtained by adding the forward timedelta to the current row's timestamp. Used to determine which values to age out from state.

**state** (object, required)
   An object representing pre-existing state. Implementations may mutate this object for efficiency.


.. _api-transform-statestrategy-update-state-and-get-values:

ss.update_state_and_get_values()
--------------------------------------------------
Take a state object and a list of input values ``(updated state, (output values,))``.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   update_state_and_get_values(self,
                               state,
                               values)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**state** (object, required)
   An object representing pre-existing state. Implementations may mutate this object for efficiency.

**values** (list, required)
   A list of values to update the state with.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
For example, we might calculate the the sum over a rolling window of a state object containing two days. The current state might look like:

.. code-block:: javascript

   {
     current_sum: 20,
     granular_sum: [["2014/08/14", 19], ["2014/08/15", 1]]
   }

after calling this function with ``["2014/08/15", 2]``, it would return:

.. code-block:: javascript

   {
     current_sum: 22,
     granular_sum: [["2014/08/14", 19], ["2014/08/15", 3]]
   }

as the ``updated_state`` and (22,) as the new sum.







.. _api-transform-statefulrollingwindowtransform:

StatefulRollingWindow
==================================================
A transform that that keeps a state object that is updated with each row and passed forward to calculate the value for the next row. This is in contrast to a RollingWindowTransform that passes all rows to an aggregation function.

The state kept is user-defined and depends on the function that the user wants to apply. See StateStrategy for more details.

This transform can operate in two modes (controlled by the boolean attribute persistent_state). When the attribute is true, the transform will record internally (and provide the ability to permanently store) the updated state after applying this transform to a DataTable. This allows for processing a time-series incrementally by applying the transform to batches of new data as they become available.

When persistent_state is False, the state of of the transform will not persist beyond an application of this transform. This is useful in cases where you want to reprocess data for "What-If" scenarios.

ATTRIBUTES_TO_SAVE() = ['timestamp_column', 'new_columns', 'aggregation_columns', 'state_uuid', 'timestamp_column', 'group_by_columns', 'persistent_state', 'max_timedelta', 'key_type', 'num_shards', 'forward_strategy', 'forward_timedelta', 'forward_data_elements', 'new_column_types']

STRATEGY_STATE(object='') = 'strategy_state'

.. TODO: The ATTRIBUTES_TO_SAVE and STRATEGY_STATE were in the arguments list in the code, but doesn't seem very argument-y. Investigate.

Usage
--------------------------------------------------

* Class: ``cr.transform.StatefulRollingWindowTransform``
* Bases: ``cr.transform_base.TableTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``StatefulRollingWindowTransform`` class. For example:

.. code-block:: python

   srw = cr.transform.StatefulRollingWindowTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   srw.apply()

Arguments
--------------------------------------------------
The following arguments are available to this function:

**timestamp_column** (str, required)
   Name of the column that contains timestamps. This is used for ordering rows. Rows are passed in timestamp order to update the state of the transform.

**state_strategy** (StateStrategy, required)
   The state strategy to use for the transform.

**group_by_columns** (list, required)
   Segment events into groups and compute rolling window statistics for each group separately. Each unique set of values across all the columns forms a group.

**aggregation_columns** (list, required)
   Names of the columns that contain data to aggregate.

**new_columns** (list, required)
   This can either be a list of column names or a list of cr.datatable.ColumnMetadata if the types are known ahead of time. Currently, only name and type properties of column metadata are propagated to the new DataTable.

**max_timedelta** (datetime.timedelta, required)
   The maximum time a state is valid for. This value sets a retention policy for the how long states continue to be carried forward with no updates.

**persistent_state** (bool, optional)
   If True, persist updated state beyond the application of the transform (see class docstring for more details).

**name** (str, optional)
   Name of the transform.

**key_type** (type, optional)
   Type of key to use for hashing. Default is a tuple. This should not be used to reference a type in a user script.

**num_shards** (int, optional)
   Number of shards for state table. Most callers should leave this as None for automatic shard counts based on metrics.

**forward_strategy** (StateStrategy, optional)
   The state strategy to use for the forward looking transform.

**forward_timedelta** (datetime.timedelta, optional)
   The time delta that the forward looking strategy looks ahead for the transform.

**forward_data_elements** (int, optional)
   The number of elements forward the forward-looking strategy looks for the transform.


Methods
--------------------------------------------------
The ``StatefulRollingWindowTransform`` class has the following methods:

* :ref:`api-transform-statefulrollingwindowtransform-apply`
* :ref:`api-transform-statefulrollingwindowtransform-derive-table-from-state`
* :ref:`api-transform-statefulrollingwindowtransform-load-state`
* :ref:`api-transform-statefulrollingwindowtransform-save-state`


.. _api-transform-statefulrollingwindowtransform-apply:

srw.apply()
--------------------------------------------------
See TableTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, datatable)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**datatable**
   ...


.. _api-transform-statefulrollingwindowtransform-derive-table-from-state:

srw.derive_table_from_state()
--------------------------------------------------
Return a new DataTable that contains one column per simple key in the composite keys for state, the last update time for the entity, and the result of calling state_conversion_fn on the state for the entity.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   derive_table_from_state(self,
                           state_conversion_fn,
                           new_column_names)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**state_conversion_fn** (callable, required)
   A callable that takes the parameters (state_key, last_updated_time (in seconds from epoch), state) where state_key is the tuple of primary key for the the state object, last_updated_time is seconds since epoch and state is the arbitrary state object associated with the given key. It should return a N-tuple, where N is length of ``new_column_names``.

**new_column_names** (list of str, required)
   A list of additional columns to be added to the new table.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++
For example, if entity in question was a person whose composite key was (``last_name``, ``first_name``) and the state associated with the user was a 2-tuple, and we wanted to include both elements from the tuple in the data table, this function might be called as:

.. code-block:: python

   derive_table_from_state(lambda key, update_time, state: state,
                          ('element_1_of_tuple', 'element_2_of_tuple'))

and result in a table that has the columns: ``last_name``, ``first_name``, ``last_updated_time`` (in seconds from epoch), ``element_1_of_tuple`` and ``element_2_of_tuple``.


.. _api-transform-statefulrollingwindowtransform-load-state:

srw.load_state()
--------------------------------------------------
Load parameters from a dictionary to initialize this object.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_state(self, object_to_load)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**object_to_load**
   ...


.. _api-transform-statefulrollingwindowtransform-save-state:

srw.save_state()
--------------------------------------------------
Overridden method. Save parameters of this object excluding state data into a dictionary (state).

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_state(self,
              state,
              for_deployment=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**state**
   ...

**for_deployment** (bool)
   ...


.. _api-transform-compressedstatestrategy:

CompressedStateStrategy
==================================================
A StateStrategy that compresses/decompresses serialized state data for collapse_state/explode_state.

Usage
--------------------------------------------------

* Class: ``cr.transform.CompressedStateStrategy``
* Bases: ``cr.transform_stateful.StateStrategy``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``CompressedStateStrategy`` class. For example:

.. code-block:: python

   css = cr.transform.CompressedStateStrategy()

and then use that variable to call the individual methods:

.. code-block:: python

   css.apply()

Methods
--------------------------------------------------
The ``CompressedStateStrategy`` class has the following methods:

* :ref:`api-transform-compressedstatestrategy-collapse-state`
* :ref:`api-transform-compressedstatestrategy-collapse-state`


.. _api-transform-compressedstatestrategy-collapse-state:

css.collapse_state()
--------------------------------------------------
Return a gzipped picked version of state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   collapse_state(self, state)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**state**
   ...


.. _api-transform-compressedstatestrategy-explode-state:

css.explode_state()
--------------------------------------------------
Decompress and unpickle state.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   explode_state(self, state)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**state**
   ...






.. _api-transform-contextcolumnserror:

ContextColumnsError
==================================================
Exception for transform column errors.

Usage
--------------------------------------------------

* Class: ``cr.transform.ContextColumnsError``
* Bases: exceptions.Exception




.. _api-transform-contextcolumnstransform:

ContextColumnsTransform
==================================================
Use the ``ContextColumnsTransform`` class to create contextualized versions of numeric columns. The values in the new columns are adjusted according to how strongly the input correlates with the target column — in the context of the current row. The context is determined by string column(s) that define logical categories (groups or segments) in the data. Currently the value columns should be constant for any given time stamp value.

Usage
--------------------------------------------------

* Class: ``cr.transform.ContextColumnsTransform``
* Bases: ``cr.transform_base.RowTransform``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``ContextColumnsTransform`` class. For example:

.. code-block:: python

   cct = cr.transform.ContextColumnsTransform()

and then use that variable to call the individual methods:

.. code-block:: python

   cct.apply()


Arguments
--------------------------------------------------
The ``ContextColumnsTransform`` class has the following arguments:

**segment_columns** (list, required)
   String column(s) that define segments.

**value_columns** (list, required)
   Numeric columns to contextualize. For example, VIX.

**segment_ID** (dict, required)
   Mapping from segment to numeric ID.

**context_models** (list, required)
   List of helper models used to contextualize numeric inputs, one item per numeric input. Each item is a tuple of K models.

**similarity** (matrix, required)
   S by K matrix of similarity weights between the S segments and the K models.

**name** (str, optional)
   Name to associate with transform.

**keep_original** (boolean, optional)
   If True, keep original value columns in transform output. Default is False.

**accept_novel_segments** (boolean, optional)
   If True, mapper produces Nones when it sees a segment that was not seen in training. If False, raise an error in this event. Default is False.


Methods
--------------------------------------------------
The ``ContextColumnsTransform`` class has the following methods:

* :ref:`api-transform-contextcolumnstransform-apply`
* :ref:`api-transform-contextcolumnstransform-create`


.. _api-transform-contextcolumnstransform-apply:

cct.apply()
--------------------------------------------------
See RowTransform.apply().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   apply(self, data)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data**
   ...


.. _api-transform-contextcolumnstransform-create:

cct.create()
--------------------------------------------------
Create contextualized versions of numeric columns.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   create(cls,
          train_data=None,
          timestamp_column=None,
          target_columns=None,
          segment_columns=None,
          value_columns=None,
          name=None,
          keep_original=False,
          accept_novel_segments=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**train_data** (DataTable, required)
   Data with correct answers for model to predict.

**timestamp_column** (str, required)
   Name of column containing time stamps.

**target_columns** (list, required)
   Names of column(s) with modeling targets.

**segment_columns** (list, required)
   Names of column(s) that define segments.

**value_columns** (list, required)
   Names of numeric column(s) to contextualize.

**name** (str, required)
   Name to associate with transform.

**keep_original** (bool, required)
   If True, keep original value columns in transform output. Default is False.

**accept_novel_segments** (bool, required)
   If True, mapper produces Nones when it sees a segment that was not seen in training. If False, raise an error in this event. Default is False.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A ContextColumnsTransform.

Example
--------------------------------------------------
In the example below, it would be an error if the training data table had a different VIX value for any row with date 10/31/2014.

.. code-block:: python

   >>> cct = ContextColumnsTransform.create(train_data=train,
   ...                                      timestamp_column='date',
   ...                                      target_columns=['revenue'],
   ...                                      segment_columns=['customer'],
   ...                                      value_columns=['VIX'])
   >>> new_train = cct.apply(train)
   >>> new_test = cct.apply(test)


Given a training data table like:

.. code-block:: none
 
    date         revenue   customer   VIX
   ------------ --------- ---------- -------
    10/31/2014   $100.00   Alice      10.38
    10/31/2014   $25.00    Bob        10.38
    10/31/2014   $50.00    Charlene   10.38
   ------------ --------- ---------- -------

this transform produces a new table like:
   
.. code-block:: none
 
    date         revenue   customer   context_VIX
   ------------ --------- ---------- -------------
    10/31/2014   $100.00   Alice      0.7
    10/31/2014   $25.00    Bob        0.65
    10/31/2014   $50.00    Charlene   1.5
   ------------ --------- ---------- -------------
