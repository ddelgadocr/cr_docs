.. 
.. xxxxx
.. 

==================================================
Compute Cluster API
==================================================

The Compute Cluster API is built into and is part of every engine-specific API and enables the API to communicate with nodes in the compute cluster.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Compute Cluster API, it must first imported as part of the engine-specific platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.cluster as cluster

and then for each use of a function in this API within the customization package add ``cluster.`` as the first part of the function name. For example:

.. code-block:: python

   cluster.get_connections()

or:

.. code-block:: python

   cluster.get_versions()


The following functions may be used to interact with the platform, such as getting connections to data repositories, initializing the cluster, and so on.

* :ref:`api-cluster-get-connections`
* :ref:`api-cluster-get-session-property`
* :ref:`api-cluster-get-versions`
* :ref:`api-cluster-initialize-cluster`
* :ref:`api-cluster-set-session-property`
* :ref:`api-cluster-shutdown-cluster`

.. 
.. Data Types
.. ==================================================
.. 
.. The following data types are in this API:
.. 
.. * bool
.. * cr.datatable.DataTable
.. * cr.science.Model
.. * cr.science.PredictionTable
.. * DataTable (same as cr.datatable.DataTable?)
.. * dict
.. * float
.. * function
.. * hint
.. * int
.. * list of ColumnMetadata (see :ref:`api-cluster-add-columns`)
.. * list of cr.datatable.DataTable
.. * list of float
.. * list of hints
.. * list of str
.. * list of tuples
.. * LocalDataTable
.. * object
.. * Plot
.. * str
.. * tuple
.. * tuple of str
.. 
.. Some odd ones:
.. 
.. * ({key: [str]}, required) (see flexi_categorize in RISP)
.. * (fn(category cols) -> key, optional) (see flexi_categorize in RISP)
.. * {key: [str]} (flexi_categorize has this)
.. * tuple/custom op[/args] or list of tuples/custom ops[/args] (this is in aggregate)
.. 
.. 
.. TODO: integrate this (it's from a troubleshooting topic):
.. 
.. Lists contain multiple items where order is not important and are enclosed in square brackets. For example, "[item1, item2, item3]".
.. 
.. Tuples contain multiple items where order is important, such as when indicating the name of columns that correspond to one another in different tables. Tuples are enclosed in parentheses. For example, "(column1, column2)".
.. 
.. You may encounter problems if you forget to include the closing bracket or parentheses, forget commas separating items, or forget quotation marks around strings inside a list or tuple.
.. 




.. _api-cluster-get-connections:

get_connections()
==================================================
Get the available named connections to data repositories.

Usage
--------------------------------------------------

.. code-block:: python

   get_connections()

Arguments
--------------------------------------------------
None.

Returns
--------------------------------------------------
A LocalDataTable that lists the available connections, including name, description, storage type, and whether it is default.

Example
--------------------------------------------------

.. code-block:: python

   risp.get_connections()

will print information similar to:

.. code-block:: sql

    Name    Description             Type    Default
   ------- ----------------------- ------- ---------
    local   Storage on local disk   local   False
    hdfs    Storage on HDFS         hdfs    True
   ------- ----------------------- ------- ---------



.. _api-cluster-get-session-property:

get_session_property()
==================================================
Get the current value of a session property.

.. note:: Use the :ref:`api-cluster-set-session-property` to update a property.

Usage
--------------------------------------------------

.. code-block:: python

   get_session_property(key)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**key** (str, required)
   The key to identify the property. Possible values: ``risp.Session.DEBUG_EACH_STEP`` or ``risp.Session.DEBUG_TAG``.

   Use ``risp.Session.DEBUG_EACH_STEP`` to determine whether to immediately evaluate a table created by a function call and print example rows.

   Use ``risp.Session.DEBUG_TAG`` to get the description captured in the system for each call.

.. TODO: Are those actually the correct values? Is the ``risp.Session.`` needed? Or is that just because all the other examples seem to assume running Python in a shell? What would this actually look like in the customization package?

Returns
--------------------------------------------------
The value for the key.

Example
--------------------------------------------------

.. code-block:: python

   is_debug = risp.get_session_property(risp.Session.DEBUG_EACH_STEP)



.. _api-cluster-get-versions:

get_versions()
==================================================
Get the versions of the installed platform, API, and applications.

Usage
--------------------------------------------------

.. code-block:: python

   get_versions()

Arguments
--------------------------------------------------
None.

Returns
--------------------------------------------------
A LocalDataTable listing installed components that includes short name, long name, version number, and tags that provide additional information.

Example
--------------------------------------------------

.. code-block:: python

   risp.get_versions()

will print information similar to:

.. code-block:: sql

    Name     Long Name          Version   Tags
   -------- ------------------ --------- ------
    crplat   Versive Platform   2.7.8.0
    risp     Versive RISP       1.0
   -------- ------------------ --------- ------



.. _api-cluster-initialize-cluster:

initialize_cluster()
==================================================
Initialize the cluster to prepare compute nodes to process data during a session and to activate default logging. This function must called first. In general, distributed processing on a multi-node cluster is preferred over single core processing and local connections.

.. note:: Use :ref:`api-cluster-get-connections` to view all of the existing, available connections.

Usage
--------------------------------------------------

.. code-block:: python

   initialize_cluster(use_single_processor=False,
                      default_connection=None,
                      compute_version=None,
                      cluster_args=None)


Arguments
--------------------------------------------------
The following arguments are available to this function:

**APPLICATION_CODE_PATH** (list of str, optional)
   A comma-separated list of directory paths to serialize and send to every node in the cluster. These directories contain any Python code that is not directly imported by an application.

**cluster_args** (dict, optional)
   Additional arguments to be passed to the cluster.

**compute_version** (int, optional)
   The version of the compute infrastructure to use. This setting overrides the system-wide default setting. Possible values: ``COMPUTE_VERSION_SPARK``.

   Use ``COMPUTE_VERSION_SPARK`` for the Spark compute infrastructure. If ``use_single_processor`` is ``True``, this will use a local master. Otherwise, it will connect to a Spark master on the local machine at the default port of 7077.

**default_connection** (str, optional)
   The default connection to the storage cluster to use for load and save operations. If a value is not set here, a connection must be set for each load and save operation.

**use_single_processor** (bool, optional)
   If ``True``, process data on only one core of the master node. When a cluster is initialized, tables and models that are checkpointed to memory may not be available until the next session.

   If ``False``, distribute processing across all nodes in the compute cluster. When a cluster is initializedr, tables and models may not be loaded or saved using a local connection because it is not a distributed store. A CSV or Excel file may be saved to local storage instead.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   risp.initialize_cluster(use_single_processor=True,
                           default_connection='hdfs')



.. _api-cluster-set-session-property:

set_session_property()
==================================================
Set the value of a session property.

.. note:: Use :ref:`api-cluster-get-session-property` to get the current value of a property.

Usage
--------------------------------------------------

.. code-block:: python

   set_session_property(key, value)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**key** (str, required)
   The key to identify the property. Possible values: ``risp.Session.DEBUG_EACH_STEP`` or ``risp.Session.DEBUG_TAG``.

   Use ``risp.Session.DEBUG_EACH_STEP`` to determine whether to immediately evaluate a table created by a function call and print example rows.

   Use ``risp.Session.DEBUG_TAG`` to get the description captured in the system for each call.

**value** (object, optional)
   The value for the ``key``. For a ``risp.Session.DEBUG_EACH_STEP`` key, either ``True`` or ``False``. For a ``risp.Session.DEBUG_TAG`` key, a string.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   risp.set_session_property(risp.Session.DEBUG_EACH_STEP,
                             value=True)




.. _api-cluster-shutdown-cluster:

shutdown_cluster()
==================================================
Shuts down the connection to the compute cluster at the end of a session.

Usage
--------------------------------------------------

.. code-block:: python

   shutdown_cluster()

Arguments
--------------------------------------------------
None.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   initialize_cluster()
   data = risp.load_crtable(path='myproject/')
   risp.shutdown_cluster()
