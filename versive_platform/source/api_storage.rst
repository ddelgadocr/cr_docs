.. 
.. xxxxx
.. 

==================================================
Storage API
==================================================

The Storage API may be used to manage storage functionality which represent connections to data.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Storage API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.storage as storage

and then for each use of a function in this API within the customization package add ``security.`` as the first part of the function name. For example:

.. code-block:: python

   storage.create()

or:

.. code-block:: python

   storage.list_named()

The following functions may be used to build customized tables, models, and transaction behaviors:

:ref:`api-storage-create`
:ref:`api-storage-get-by-name`
:ref:`api-storage-list-named`
:ref:`api-storage-remove-by-name`
:ref:`api-storage-store-by-name`
:ref:`api-storage-storage`
:ref:`api-storage-storage-create`
:ref:`api-storage-storage-desc`
:ref:`api-storage-storage-enum-directories`
:ref:`api-storage-storage-enum-files`
:ref:`api-storage-storage-get-native`
:ref:`api-storage-storage-load-file`
:ref:`api-storage-storage-load-storage`
:ref:`api-storage-storage-open-file`
:ref:`api-storage-storage-open-rdd`
:ref:`api-storage-storage-read-schema`
:ref:`api-storage-storage-remove-file`
:ref:`api-storage-storage-save-file`
:ref:`api-storage-storage-save-rdd`
:ref:`api-storage-storage-save-storage`
:ref:`api-storage-storage-write-schema`
:ref:`api-storage-local`
:ref:`api-storage-s3`
:ref:`api-storage-hdfs`
:ref:`api-storage-hdfs-migrate-saved-objects`
:ref:`api-storage-memory`
:ref:`api-storage-memory-load-file`
:ref:`api-storage-odbc`





.. _api-storage-create:

create()
==================================================
Create a Storage object with optional arguments.

Usage
--------------------------------------------------

.. code-block:: python

   cr.storage.create(storage, **kwargs)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**storage** (str or Storage, required)
   If this is already a Storage object, it is simply returned unmodified. If this is a string that maps to a Storage type, it will return an instance of that Storage. This argument is not case-sensitive. Known storage types are:

   "local": Local

   "HDFS": HDFS

   "S3": S3

   "ODBC": ODBC

   "Memory": Memory

   .. note:: If this is a string that begins with "name:", then the remainder of the string is treated like a named connection and passed to the get_by_name() method.

.. note:: Optional arguments provided must be compatible with the type of Storage specified in the storage string

Returns
--------------------------------------------------
Storage.

Example
--------------------------------------------------

.. code-block:: python

   >>> storage_1 = cr.storage.create("name:saved_connection")
   >>> storage_2 = cr.storage.get_by_name("saved_connection")
   >>> storage_1 == storage_2



.. _api-storage-get-by-name:

get_by_name()
==================================================
Retrieve a named Storage object. It must have been previously saved via store_by_name() or via the command-line tool cr connect.

Usage
--------------------------------------------------

.. code-block:: python

   cr.storage.get_by_name(name)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**name** (str, required)
   Name of Storage object.

Returns
--------------------------------------------------
Storage object.


.. _api-storage-list-named:

list_named()
==================================================
Return a list of named Storage objects as tuples: (name, description, type).

Usage
--------------------------------------------------

.. code-block:: python

   cr.storage.list_named()



.. _api-storage-remove-by-name:

remove_by_name()
==================================================
Remove a named connection.

Usage
--------------------------------------------------

.. code-block:: python

   cr.storage.remove_by_name(name)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**name** (str, required)
   Name of connection to remove.



.. _api-storage-store-by-name:

store_by_name()
==================================================
Save a Storage object by the provided name.

Usage
--------------------------------------------------

.. code-block:: python

   cr.storage.store_by_name(name,
                            storage)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**name** (str, required)
   Name to save the Storage object as. It will be saved as part of cluster configuration, and available by name in future sessions.

**storage** (Storage, required)
   Storage object to save in cluster configuration.



.. _api-storage-storage:

Storage
==================================================
Base class for providers of access to storage systems.

Storage objects are simple objects which represent storage types, locations, and any required metadata for accessing the storage. They may be used across the |versive| API for describing where to save and load objects and data. Storage objects can also be saved and loaded like other objects, allowing easy redirection of storage specifiers from a well-known location.

Examples of Storage providers include:

* Local
* HDFS
* Memory
* S3
* ODBC

To use a Storage object, simply pass it as the storage argument of any function that accesses storage.

For example:

.. code-block:: python

   >>> storage=cr.storage.create('hdfs', host='127.0.0.1')

.. code-block:: python

   >>> cr.something.load(path='/foo', storage=storage)

.. code-block:: python

   >>> cr.something.load(path='/foo', storage=cr.storage.HDFS(host='127.0.0.1'))

.. code-block:: python

   >>> cr.something.load(path='/foo', storage="HDFS,host='127.0.0.1'")

Usage
--------------------------------------------------

* Class: ``cr.storage.Storage``
* Bases: ``cr.version_base.FieldVersionable``

Methods
--------------------------------------------------
The ``Storage`` class has the following methods:

:ref:`api-storage-storage-create`
:ref:`api-storage-storage-desc`
:ref:`api-storage-storage-enum-directories`
:ref:`api-storage-storage-enum-files`
:ref:`api-storage-storage-get-native`
:ref:`api-storage-storage-load-file`
:ref:`api-storage-storage-load-storage`
:ref:`api-storage-storage-open-file`
:ref:`api-storage-storage-open-rdd`
:ref:`api-storage-storage-read-schema`
:ref:`api-storage-storage-remove-file`
:ref:`api-storage-storage-save-file`
:ref:`api-storage-storage-save-rdd`
:ref:`api-storage-storage-save-storage`
:ref:`api-storage-storage-write-schema`


.. _api-storage-storage-create:

Storage.create()
--------------------------------------------------
See cr.storage.create() for more information.

.. TODO: is this the same as the api-storage-create function?

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   create(storage)



.. _api-storage-storage-desc:

desc()
--------------------------------------------------
Return a description for this Storage location.


.. _api-storage-storage-enum-directories:

enum_directories()
--------------------------------------------------
List directories under the path specified.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   enum_directories(self, path)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**path** (str, optional)
   Path within 'storage' to load this object from.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
List of dictionaries containing "name" and "size" keys.


.. _api-storage-storage-enum-files:

enum_files()
--------------------------------------------------
List files under the path specified.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   enum_files(self, path, recursive=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**path** (str, optional)
   Path within 'storage' to load this object from.

**recursive** (bool, required)
   ...

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
List of dictionaries containing "name" and "size" keys.



.. _api-storage-storage-get-native:

get_native()
--------------------------------------------------
Return a handle to a native filesystem object. The returned object is a Python object that may be used as a connector. This function is for internal use only.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_native(self)



.. _api-storage-storage-load-file:

load_file()
--------------------------------------------------
Read an entire file in and returns it as a string.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_file(self, path, **kwargs)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**path** (str, optional)
   Path within 'storage' to load this object from.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Contents of specified file.



.. _api-storage-storage-load-storage:

load_storage()
--------------------------------------------------
Load a saved Storage object.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_storage(storage, path)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**storage** (Storage, required)
   Describes the storage system to load this object from.

**path** (str, optional)
   Path within 'storage' to load this object from.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Returns the loaded Storage object.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> test_model_storage = cr.storage.load_storage(storage='local',
   ...                                              path='test_models.storage')



.. _api-storage-storage-open-file:

open_file()
--------------------------------------------------
Open a file and returns a stream to the file contents.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   open_file(self, path, **kwargs)

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A stream object with a read(nbytes) member.



.. _api-storage-storage-open-rdd:

open_rdd()
--------------------------------------------------
Open a file and returns a stream to the file contents.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   open_rdd(self, **kwargs)

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Open an RDD from the specified location. This is generally accessed via cr.data.DataTable.load().


.. _api-storage-storage-read-schema:

read_schema()
--------------------------------------------------
Read a schema file at the path indicated.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   read_schema(self, path)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**path** (str, required)
   Path to schema file to read.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A tuple containing (names, types), where names is a list of column names, and types is a matching list of Python types as described in the loaded schema.




.. _api-storage-storage-remove-file:

remove_file()
--------------------------------------------------
Remove a file from the storage system at the path indicated.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   remove_file(self, path, **kwargs)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**recursive** (bool, required)
   Remove files and directories recursively. Use with extreme caution, as there is no further confirmation of the operation. Default is False.



.. _api-storage-storage-save-file:

save_file()
--------------------------------------------------
Save data to a specified file path.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_file(self, path, data, **kwargs)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**path** (str, required)
   File path.

**data** (str, required)
   File data.



.. _api-storage-storage-save-rdd:

save_rdd()
--------------------------------------------------
Save an RDD to the specified location. This is generally accessed via cr.data.DataTable.save().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_rdd(self, rdd, path, **kwargs)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**rdd** ()
   ...

**path** (str, required)
   File path.



.. _api-storage-storage-save-storage:

save_storage()
--------------------------------------------------
Save the Storage object to storage for later use.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_storage(self, storage, path, overwrite=False)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**storage** (Storage, required)
   Describes the storage destination to save this object to.

**path** (str, required)
   Path within storage to save this object to.

**overwrite** (bool, optional)
   If True, overwrite the file if it already exists. Default is False.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> storage_to_save = cr.storage.HDFS(host='127.0.0.1',
   ...                                   base_path='/test_models/January/')
   >>> storage_to_save.save_storage(storage='local', path='test_models.storage')



.. _api-storage-storage-write-schema:

write_schema()
--------------------------------------------------
Write a schema file to the path indicated.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   write_schema(self, path, schema, **kwargs)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**path** (str, required)
   Path to schema file to read.

**schema** (tuple, required)
   A tuple containing (names, types), where names is a list of column names, and types is a matching list of Python types.

**overwrite** (bool, optional)
   Overwrite the file if it already exists. Default is True.



.. _api-storage-local:

Local
==================================================
Provide access to the local filesystem.

Usage
--------------------------------------------------

* Class: ``cr.storage.Local``
* Bases: ``cr.storage.Storage``

Arguments
--------------------------------------------------
The following arguments are available to this function:

**base_path** (str, required)
   Base path to use for all I/O to this Storage object. The base path will be combined with whatever additional paths are specified to determine the final path. Default is "./" (present working directory) if unspecified. Absolute paths may be used to override the base path.


.. _api-storage-s3:

S3
==================================================
Provide access to AWS S3 storage.

Usage
--------------------------------------------------

* Class: ``cr.storage.S3``
* Bases: ``cr.storage.Storage``

Arguments
--------------------------------------------------
The following arguments are available to this function:

**base_path** (str, required)
   Base path to use for all I/O to this Storage object. The base path will be combined with whatever additional paths are specified to determine the final path. Default is "/" if unspecified. Absolute paths may be used to override the base path.

**bucket** (str, required)
   S3 Bucket to use. If unspecified, it will use S3_DEFAULT_BUCKET from the environment.

**aws_access_key_id** (str, required)
   AWS credentials: access key. If unspecified, it will use AWS_ACCESS_KEY_ID from the environment.

**aws_secret_access_key** (str, required)
   AWS credentials: secret access key. If unspecified, it will use AWS_SECRET_ACCESS_KEY from the environment.


.. _api-storage-hdfs:

HDFS
==================================================
Provide access to an HDFS filestore via the WebHDFS protocol.

Usage
--------------------------------------------------

* Class: ``cr.storage.HDFS``
* Bases: ``cr.storage.Storage``

Arguments
--------------------------------------------------
The following arguments are available to this function:

**host** (str, required)
   Host on which the WebHDFS name server resides. This may be specified as an IP or host name.

**base_path** (str, optional)
   Base path to use for all I/O to this Storage object. The base path will be combined with whatever additional paths are specified to determine the final path. Default is "/" if unspecified. Absolute paths may be used to override the base path.

**port** (int, optional)
   Port number to access on host for WebHDFS protocol. If unspecified, will use the default 50070.

**native_port** (int, optional)
   Port number to access on host for native HDFS protocol. If unspecified, will use the default 8020. If 0 then native HDFS protocol will not be used. Only some compute engines and operations support native HDFS; this argument will be used only for operations supporting native HDFS.

**user** (str, optional)
   The user name for a non-security-enabled HDFS store.

**delegation_token** (str, optional)
   The authentication token for a security-enabled HDFS store.

**kerberos_keytab** (str, optional)
   The keytab file to use for Kerberos authentication. Either specify delegation_token or Kerberos configuration if HDFS is secured by Kerberos.

**kerberos_user** (str, optional)
   The user to use for Kerberos authentication.

**kerberos_realm** (str, optional)
   The realm to use for Kerberos authentication.

**timeout_sec** (int, optional)
   The socket timeout (default 30 seconds).

**timeout_retries** (int, optional)
   The number of times to retry on a connection error (such as timeout).

Methods
--------------------------------------------------
The ``HDFS`` class has the following methods:

* :ref:`api-storage-hdfs-migrate-saved-objects`

.. _api-storage-hdfs-migrate-saved-objects:

migrate_saved_object()
--------------------------------------------------
...

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   migrate_saved_object(cls,
                        saved_object,
                        curr_ver,
                        saved_ver)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**saved_object** ()
   ...

**curr_ver** ()
   ...

**saved_ver** ()
   ...




.. _api-storage-memory:

Memory
==================================================
Provide access to a temporary storage location in system memory. System memory consumed by the storage will be released when all references to this Memory instance are released. If this Storage object is saved or pickled, the entire contents of its storage will be saved or pickled along with it.

Usage
--------------------------------------------------

* Class: ``cr.storage.Memory``
* Bases: ``cr.storage.Storage``

Methods
--------------------------------------------------
The ``Memory`` class has the following methods:

* :ref:`api-storage-memory-load-file`


.. _api-storage-memory-load-file:

Memory.load_file()
--------------------------------------------------
Return the file contents instead of a stream over the file contents. For MemoryFileSystem, this is more efficient since it simply returns a reference to the existing file contents.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load_file(self, path)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**path** (str, required)



.. _api-storage-odbc:

ODBC
==================================================
Provide access to the Oracle/MSSQL/MySQL.

.. warning:: The ``ODBC`` class does not support the following methods:

   * ``load_file()``
   * ``open_file()``
   * ``remove_file()``
   * ``save_file()``

Usage
--------------------------------------------------

* Class: ``cr.storage.ODBC``
* Bases: cr.storage.Storage

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**connectionString** (str, required)
   ODBC connection string, for example "Driver=MySql; host=localhost; etc."

**defaultTable** (str, optional)
   DB table name used by default.

**columnFilter** (str, optional)
   Comma-separated column names.

**identityColumns** (str, optional)
   Comma-separated column names used in ORDER BY clause in MSSQL and Oracle queries.

**append** boolean
   If True, append to the DB when saving RDD; if False, overwrite the data.
