.. 
.. xxxxx
.. 

==================================================
Glossary Terms: Platform
==================================================

The following glossary terms apply to the |versive| platform:

.. glossary::

   **alias**
      The user-friendly text used to represent an input column name in anomaly details.

   **area under the curve (AUC)**
      .. include:: ../../includes_terms/term_metric_auc.rst

   **automated exploration**
      .. include:: ../../includes_terms/term_automated_exploration.rst

      .. include:: ../../includes_terms/term_automated_exploration_basic.rst

      .. include:: ../../includes_terms/term_automated_exploration_interactions.rst

   **backward report**
      .. include:: ../../includes_terms/term_report_backward.rst

   **baseline model**
      .. include:: ../../includes_terms/term_model_type_baseline.rst

   **batch mode**
      .. include:: ../../includes_terms/term_python_mode_batch.rst

   **basic access authentication**
      In the context of an HTTP transaction, basic access authentication is a method for an HTTP user agent to provide a username and password when making a request.

   **binary classification model**
      .. include:: ../../includes_terms/term_model_type_binary_classification.rst

   **bootstrap**
      The process of loading a set of instructions that is used to load a program onto a computer for the first time, often used in reference to loading operating systems.

   **canonical form**
      .. include:: ../../includes_terms/term_table_datatable_canonical_form.rst

   **classification accuracy (ACC)**
      .. include:: ../../includes_terms/term_metric_acc.rst

   **combo model**
      .. include:: ../../includes_terms/term_model_type_combo.rst

   **comma-separated values (CSV)**
      .. include:: ../../includes_terms/term_file_csv.rst

   **compute cluster**
      .. include:: ../../includes_terms/term_cluster_compute.rst

   **compute nodes**
      .. include:: ../../includes_terms/term_nodes_compute.rst

   **count regression model**
      .. include:: ../../includes_terms/term_model_type_count_regression.rst

   **cumulative distribution**
      .. include:: ../../includes_terms/term_cumulative_distribution.rst

   **DataFrame**
      .. include:: ../../includes_terms/term_table_dataframe.rst

   **DataTable**
      .. include:: ../../includes_terms/term_table_datatable.rst

   **distributed computing**
      .. include:: ../../includes_terms/term_distributed_computing.rst

   **enrichments**
      .. include:: ../../includes_terms/term_model_enrichment.rst

   **expected measurement**
      .. include:: ../../includes_terms/term_model_expected_measurement.rst

   **features**
      .. include:: ../../includes_terms/term_features.rst

   **F1 score (F1)**
      .. include:: ../../includes_terms/term_metric_f1.rst

   **forward report**
      .. include:: ../../includes_terms/term_report_forward.rst

   **information leakage**
      .. include:: ../../includes_terms/term_information_leakage.rst

   **inputs**
      .. include:: ../../includes_terms/term_inputs.rst

   **interactive mode**
      .. include:: ../../includes_terms/term_python_mode_interactive.rst

   **JavaScript Object Notation (JSON)**
      .. include:: ../../includes_terms/term_json.rst

   **learning curve**
      .. include:: ../../includes_terms/term_model_learning_curve.rst

   **linear regression model**
      .. include:: ../../includes_terms/term_model_type_linear_regression.rst

   **LocalDataTable**
      .. include:: ../../includes_terms/term_table_localdata.rst

   **logistic loss (LOGLOSS)**
      .. include:: ../../includes_terms/term_metric_logloss.rst

   **logistic loss baseline model (MULTILOGLOSS)**
      .. include:: ../../includes_terms/term_metric_multilogloss.rst

   **lookup table**
      .. include:: ../../includes_terms/term_table_lookup.rst

   **Lua**
      .. include:: ../../includes_terms/term_lua.rst

   **mean absolute error (MAE)**
      .. include:: ../../includes_terms/term_metric_mae.rst

   **mean absolute log-quotient (MALQ)**
      .. include:: ../../includes_terms/term_metric_malq.rst

   **mean absolute percentage error (MAPE)**
      .. include:: ../../includes_terms/term_metric_mape.rst

   **mean**
      .. include:: ../../includes_terms/term_mean.rst

   **measurement model**
      .. include:: ../../includes_terms/term_model_type_measurement.rst

      .. include:: ../../includes_terms/term_model_type_measurement_nested.rst

   **median**
      .. include:: ../../includes_terms/term_median.rst

   **mode**
      .. include:: ../../includes_terms/term_mode.rst

   **modeling table**
      .. include:: ../../includes_terms/term_table_modeling.rst

   **multiclass classification model**
      .. include:: ../../includes_terms/term_model_type_multiclass_classification.rst

   **nodes**
      .. include:: ../../includes_terms/term_nodes.rst

   **one-in report**
      .. include:: ../../includes_terms/term_report_one_in.rst

   **one-out report**
      .. include:: ../../includes_terms/term_report_one_out.rst

   **overfitting**
      .. include:: ../../includes_terms/term_overfitting.rst

   **precision (PREC)**
      .. include:: ../../includes_terms/term_metric_prec.rst

   **precision at K**
      .. include:: ../../includes_terms/term_precision_at_k.rst

   **PredictionTable**
      .. include:: ../../includes_terms/term_table_prediction.rst

   **predictive model**
      .. include:: ../../includes_terms/term_predictive_model.rst

   **primary node**
      .. include:: ../../includes_terms/term_nodes_primary.rst

   **quantile absolute percentage error (QAPE)**
      .. include:: ../../includes_terms/term_metric_qape.rst

   **recall (REC)**
      .. include:: ../../includes_terms/term_metric_rec.rst

   **recall at K**
      .. include:: ../../includes_terms/term_recall_at_k.rst

   **receiver operating characteristic (ROC)**
      .. include:: ../../includes_terms/term_plot_roc.rst

   **records**
      .. include:: ../../includes_terms/term_records.rst

   **regression model**
      .. include:: ../../includes_terms/term_model_type_regression.rst

   **replica nodes**
      .. include:: ../../includes_terms/term_nodes_replica.rst

   **Resilient Distributed Dataset (RDD)**
      .. include:: ../../includes_terms/term_rdd.rst

   **rolling window transform**
      .. include:: ../../includes_terms/term_rolling_window_transform.rst

   **root mean squared error (RMSE)**
      .. include:: ../../includes_terms/term_metric_rmse.rst

   **sampling**
      .. include:: ../../includes_terms/term_model_sampling.rst

   **schema file**
      .. include:: ../../includes_terms/term_file_schema.rst

   **sprocket**
      .. include:: ../../includes_terms/term_rdd_sprocket.rst

   **Secure Shell (SSH)**
       .. include:: ../../includes_terms/term_ssh.rst       

   **Secure Sockets Layer (SSL)**
      .. include:: ../../includes_terms/term_ssl.rst

   **storage cluster**
      .. include:: ../../includes_terms/term_cluster_storage.rst

   **subsample**
      .. include:: ../../includes_terms/term_subsample.rst

   **target**
      .. include:: ../../includes_terms/term_target.rst

   **tab-separated values (TSV)**
      .. include:: ../../includes_terms/term_file_tsv.rst

   **test table**
      .. include:: ../../includes_terms/term_table_test.rst

   **train table**
      .. include:: ../../includes_terms/term_table_train.rst

   **training data**
      .. include:: ../../includes_terms/term_data_training.rst

   **tune table**
      .. include:: ../../includes_terms/term_table_tune.rst

   **YAML**
      .. include:: ../../includes_terms/term_file_yaml.rst
