.. 
.. xxxxx
.. 

==================================================
Security API
==================================================

The Security API may be used to build out customized table and modeling behaviors.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Security API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.security as security

and then for each use of a function in this API within the customization package add ``security.`` as the first part of the function name. For example:

.. code-block:: python

   security.add_columns()

or:

.. code-block:: python

   security.classification_summary()


The following functions may be used to build customized tables, models, and transaction behaviors:

* :ref:`api-security-add-context`
* :ref:`api-security-add-context-from`
* :ref:`api-security-checkpoint`
* :ref:`api-security-classification-summary`
* :ref:`api-security-count-frequencies`
* :ref:`api-security-count-rows`
* :ref:`api-security-create-context-custom-op`
* :ref:`api-security-create-context-op`
* :ref:`api-security-create-custom-op`
* :ref:`api-security-create-parser-hint-date`
* :ref:`api-security-create-parser-hint-maxerr`
* :ref:`api-security-create-parser-hint-missing`
* :ref:`api-security-create-parser-hint-table-maxerr`
* :ref:`api-security-get-checkpoints`
* :ref:`api-security-get-udf-debug-logs`
* :ref:`api-security-load-checkpoint`
* :ref:`api-security-measure-confusion-matrix`
* :ref:`api-security-measure-input-importance`
* :ref:`api-security-measure-learning-curve`
* :ref:`api-security-measure-score-curve`
* :ref:`api-security-plot-bar`
* :ref:`api-security-plot-histogram`
* :ref:`api-security-plot-line`
* :ref:`api-security-plot-scatter`
* :ref:`api-security-relate`
* :ref:`api-security-score-predictions`
* :ref:`api-security-summarize-model`
* :ref:`api-security-udf-debug-log`

.. 
.. Data Types
.. ==================================================
.. 
.. The following data types are in this API:
.. 
.. * bool
.. * cr.datatable.DataTable
.. * cr.science.Model
.. * cr.science.PredictionTable
.. * DataTable (same as cr.datatable.DataTable?)
.. * dict
.. * float
.. * function
.. * hint
.. * int
.. * list of ColumnMetadata (see :ref:`api-security-add-columns`)
.. * list of cr.datatable.DataTable
.. * list of float
.. * list of hints
.. * list of str
.. * list of tuples
.. * LocalDataTable
.. * object
.. * Plot
.. * str
.. * tuple
.. * tuple of str
.. 
.. Some odd ones:
.. 
.. * ({key: [str]}, required) (see flexi_categorize in RISP)
.. * (fn(category cols) -> key, optional) (see flexi_categorize in RISP)
.. * {key: [str]} (flexi_categorize has this)
.. * tuple/custom op[/args] or list of tuples/custom ops[/args] (this is in aggregate)
.. 
.. 
.. TODO: integrate this (it's from a troubleshooting topic):
.. 
.. Lists contain multiple items where order is not important and are enclosed in square brackets. For example, "[item1, item2, item3]".
.. 
.. Tuples contain multiple items where order is important, such as when indicating the name of columns that correspond to one another in different tables. Tuples are enclosed in parentheses. For example, "(column1, column2)".
.. 
.. You may encounter problems if you forget to include the closing bracket or parentheses, forget commas separating items, or forget quotation marks around strings inside a list or tuple.
.. 





.. _api-security-add-context:

add_context()
==================================================
Add one (or more) columns to a table that aggregate data by time, according to a context operation.

.. note:: Use the :ref:`api-security-add-context-from` function to aggregate data from another table.

Usage
--------------------------------------------------

.. code-block:: python

   add_context(table,
               timestamp_column,
               context_ops,
               group_by_columns=None,
               description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**context_ops** (tuple or list of tuples, required)
   A tuple or a list of tuples that contain context operations that describe how to aggregate the data.

   Use the :ref:`api-security-create-context-op` function to create context operations using simple pre-defined aggregation functions.

   Use the :ref:`api-security-create-context-custom-op` function to create context operations that are defined by a custom function.

**description** (str, optional)
   A string to be added to log output.

**group_by_columns** (str or list of str, optional)
   A column or a list of columns that contain the categorical information by which the table data is grouped. Values are aggregated only for rows that share the same categories in these columns.

**table** (cr.datatable.DataTable, required)
   The table to which columns are added.

**timestamp_column** (str, required)
   The column in which timestamp data is located.

Returns
--------------------------------------------------
A cr.datatable.DataTable with the new columns added that contain data aggregated according to ``context_ops``.

Example
--------------------------------------------------
The following example will create a table that includes two new columns: ``bid_mean`` and ``ask_mean``, which contain the average for the 10-second interval that brackets the current row:

.. code-block:: python

   bid_op = risp.create_context_op(new_column='bid_mean',
                                   input_column='bid',
                                   aggregate_op='mean',
                                   start_offset=-5,
                                   end_offset=5,
                                   unit='seconds')
   ask_op = risp.create_context_op(new_column='ask_mean',
                                   input_column='ask',
                                   aggregate_op='mean',
                                   start_offset=-5,
                                   end_offset=5,
                                   unit='seconds')
   agg_table = risp.add_context(data,
                                timestamp_column='timestamp',
                                context_ops=[bid_op, ask_op],
                                group_by_columns=['gb_col1',
                                                  'gb_col2'])




.. _api-security-add-context-from:

add_context_from()
==================================================
Add columns that aggregate data from a secondary table based on timestamp column data according to a context operation.

.. note:: Use the :ref:`api-security-add-context` function to aggregate data from the same table. Use :ref:`api-security-create-context-op` to create context operations using predefined aggregation functions, such as sum or average. Use :ref:`api-security-create-custom-op` to create context operations from a custom function.

Usage
--------------------------------------------------

.. code-block:: python

   add_context_from(table,
                    from_table,
                    timestamp_columns,
                    context_ops,
                    group_by_columns=None,
                    description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**context_ops** (tuple or list of tuples, required)
   The context operation (or context operations) that describe how to aggregate data. Context operations are created using the :ref:`api-security-create-context-op` function.

   .. warning:: If context operations are created with ``unit='rows'``, the interval may contain a different number of rows, depending on if there is an exact match between the timestamps in the primary and secondary tables. For example, if ``start_offset=-2``, ``end_offset=2``, and the ``from_table`` does not contain a row with a timestamp that exactly matches a row in the primary table, the interval will contain four rows. If there is an exact match, the interval will contain five rows (the matching row and two from before and after).

**description** (str, optional)
   A string to be added to log output.

**from_table** (cr.datatable.DataTable, required)
   The table from which data used for aggregation is located.

**group_by_columns** (str, tuple of str, list of str, or list of tuples, optional)
   The columns that contain the categorical information by which the table data is grouped. If the column names are the same in each table, provide a string or list of strings. If the column names differ in each table, provide a two-item tuple or a list of two-item tuples that contain the column names in ``table`` and ``from_table``, respectively.

**table** (cr.datatable.DataTable, required)
   The table to which columns are added.

**timestamp_columns** (str or tuple of str, required)
   A column or a list of columns that contain the timestamp data. If column names are identical in multiple tables, use a string. If column names are not identical in multiple tables, use a two-item tuple that contains the column names in ``table`` and ``from_table`` respectively.

Returns
--------------------------------------------------
A cr.datatable.DataTable with new columns added that contain data aggregated according to ``context_ops``.

Example
--------------------------------------------------

.. TODO: the following example CLEARLY has nothing to do with CSE. need a new example!

.. code-block:: python

   bid_op = risp.create_context_op(new_column='bid_mean',
                                   input_column='bid',
                                   aggregate_op='mean',
                                   start_offset=-5,
                                   end_offset=5,
                                   unit='seconds')
   ask_op = risp.create_context_op(new_column='ask_mean',
                                   input_column='ask',
                                   aggregate_op='mean',
                                   start_offset=-5,
                                   end_offset=5,
                                   unit='seconds')
   agg_table = risp.add_context_from(data,
                                     from_table=satellite_data,
                                     timestamp_columns=('timestamp',
                                                        'sat_timestamp'),
                                     context_ops=[bid_op, ask_op],
                                     group_by_columns=['gb_col1',
                                                      ('gb_col2',
                                                       'gb_col2_in_sat')])

The resulting table includes two new columns---``bid_mean`` and ``ask_mean``---that contain the average values for the 10-second interval that brackets the current row.





.. _api-security-checkpoint:

checkpoint()
==================================================
Checkpoint a table or model in memory.

.. note::  Use the :ref:`api-security-get-checkpoints` function to view all checkpointed items. Use the :ref:`api-security-load-checkpoint` function to retrieve a checkpointed item.

Usage
--------------------------------------------------

.. code-block:: python

   checkpoint(item, key)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**item** (cr.datatable.DataTable, required)
   The table or model to checkpoint.

.. TODO: Also the ``cr.science.model`` data type.

**key** (str, optional)
   A key to identify the table or model in memory.

   .. note:: A checkpoint is overwritten if the key already exists for a table or model.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   risp.checkpoint(table, key='clean_primary')
   risp.get_checkpoints()

will print information similar to:

.. code-block:: sql

    Key             Item Type   Create Time
   --------------- ----------- ----------------------------
    clean_primary   table       2015-03-02 18:20:27.333698




.. _api-security-classification-summary:

classification_summary()
==================================================
Calculate per-class classification metrics from a table of predictions produced from a binary or multiclass classification model.

Usage
--------------------------------------------------

.. code-block:: python

   classification_summary(table,
                          threshold=0.5)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.science.PredictionTable, required)
   A table that contains inputs, a column of observed values of the target, and a column of predicted values of the target.

**threshold** (float, optional)
   A number between 0 and 1 that determines the threshold for predictions to be classified as either 0 or 1 (binary classification only). For example, if threshold=0.6 and the predicted value is 0.7, it will be classified as 1. Default value: ``0.5``.

Returns
--------------------------------------------------
A LocalDataTable containing columns corresponding to a particular classification metric and rows corresponding to each class.

Example
--------------------------------------------------

.. code-block:: python

   model = risp.learn_model(target='label',
                            model='MULTI_CLASSIFICATION',
                            train_table=train,
                            tune_table=tune)
   predictions = risp.predict(model,
                              test)
   risp.classification_summary(predictions,
                               threshold=0.5)
   

For example:

.. code-block:: sql

    table       Precision   Recall   F1      Accuracy   Support
   ----------- ----------- -------- ------- ---------- ---------
    class 0     0.644       0.887    0.746   0.860      772.0
    class 1     0.755       0.823    0.788   0.893      771.0
    class 2     0.741       0.617    0.673   0.856      806.0
    class 3     0.687       0.653    0.670   0.830      907.0
    class 4     0.711       0.554    0.623   0.839      824.0
    aggregate   0.708       0.707    0.707   0.702      4080.0
   ----------- ----------- -------- ------- ---------- ---------





.. _api-security-count-frequencies:

count_frequencies()
==================================================
Group data in a table according to categorical data in one (or more) columns, and then count how frequently a particular value appears in one (or more) columns for each defined category.

Usage
--------------------------------------------------

.. code-block:: python

   count_frequencies(table,
                     group_by_columns,
                     value_columns,
                     limit=None,
                     description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**group_by_columns** (str or list of str, required)
   A single column or a list of columns that define each category in the table. A single row is produced for each unique value. Columns must contain string values.

**limit** (int, optional)
   The maximum number of values to count in ``value_columns``. Only the most frequently occurring values will appear in the resulting table, capped at approximately this number.

**table** (cr.datatable.DataTable, required)
   The table in which the data to be analyzed is located.

**value_columns** (str or list of str, required)
   A single column or a list of columns that contain the values to be counted. A new column is created for each unique value.

Returns
--------------------------------------------------
A cr.datatable.DataTable with a row for each category in ``group_by_columns`` and a column for each unique value in ``value_columns``. This table shows how frequently each value for ``value_columns`` appears in each category defined by ``group_by_columns``.

Example
--------------------------------------------------

.. code-block:: python

   sales_data = risp.load_crtable(path='home/user/dir/')
   count = risp.count_frequencies(data,
                                  group_by_columns='foo',
                                  value_columns='bar')
   risp.get_example_rows(count)



.. _api-security-count-rows:

count_rows()
==================================================
Get the number of rows in a table.

Usage
--------------------------------------------------

.. code-block:: python

   count_rows(table)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table for which rows are counte.

Returns
--------------------------------------------------
The row count for a table.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.load_crtable(path='mytable/')
   risp.count_rows(data)



.. _api-security-create-context-custom-op:

create_context_custom_op()
==================================================
Create a context operation that aggregates data within a specified interval, as defined by a custom function.

.. note:: Simple predefined aggregate operations are available from the :ref:`api-security-create-context-op` function.

Usage
--------------------------------------------------

.. code-block:: python

   create_context_custom_op(new_columns,
                            input_columns,
                            udf,
                            start_offset,
                            end_offset,
                            unit,
                            include_start=True,
                            include_end=False,
                            max_window=None,
                            **kwargs)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**end_offset** (int, required)
   Relative to the current row's timestamp, the point at which the aggregation interval ends. Negative values move back in time; positive values move forward.

**include_end** (bool, optional)
   If ``True``, include the exact match of the end offset in the interval. If ``False``, do not include exact matches. Default value: ``False``.

**include_start** (bool, optional)
   If ``True``, include the exact match of the start offset in the interval. If ``False``, do not include exact matches. Default value: ``True``.

**input_columns** (str, tuple of str, or list of str, required)
   A list of column names to aggregate.

**max_window** (int, optional)
   If ``unit`` is set to ``rows``, the number of seconds in either direction of the current time that defines the window of time within which rows must occur to be included in the interval.

**new_columns** (str, tuple of str, or list of str, required)
   A list of column names to add to a table.

**start_offset** (int, required)
   Relative to the current row's timestamp, the point at which the aggregation interval begins. Negative values move back in time; positive values move forward.

**udf** (function, required)
   The name of a custom function that defines how to aggregate the data for the columns specified by ``input_columns``. If ``input_columns`` has one column, then the custom function should accept a list of values as input. If ``input_columns`` has more than one column, then the custom function should accept a list of lists where each list's length equals the number of columns in ``input_columns``. The number of items the custom function returns (either as a single item or multiple items in a tuple) must be equal to the number of columns in ``new_columns``.

**unit** (str, required)
   The unit by which an offset is measured. Possible values: ``seconds``, ``minutes``, ``hours``, ``days`` (measured in 24-hour increments from the current row), and ``rows``.

Returns
--------------------------------------------------
A tuple that defines the context operation to perform.

.. note:: Pass the results of this function to the :ref:`api-security-add-context` and :ref:`api-security-add-context-from` functions.

Example
--------------------------------------------------

.. code-block:: python

   spread_fn = lambda l : max(l) - min(l) if l else None
   bid_op = risp.create_context_custom_op(new_columns='bid_mean',
                                          input_columns='bid',
                                          udf=spread_fn,
                                          start_offset=-5,
                                          end_offset=5,
                                          unit='seconds')




.. _api-security-create-context-op:

create_context_op()
==================================================
Create a context operation that aggregates data within a specified interval.

.. note:: Use the :ref:`api-security-create-context-custom-op` function to define a custom aggregation.

Usage
--------------------------------------------------

.. code-block:: python

   create_context_op(new_column,
                     input_column,
                     aggregate_op,
                     start_offset,
                     end_offset,
                     unit,
                     include_start=True,
                     include_end=False,
                     max_window=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**aggregate_op** (str, required)
   The operation to carry out on the aggregated data in that column. The following operations are available:

   ``count``: The total number of values in the interval, not including missing values.

   ``diff``: The difference between the last value in the interval and the first value in the interval.

   ``div``: The last value in the interval divided by the first value in the interval.

   ``empty``: The number of missing values in the interval. ``None`` values are ignored, except when set to ``empty``.

   ``first``: The first value in the interval.

   ``last``: The last value in the interval.

   ``list``: A comma-separated list of unique values in the interval, as a string.

   ``max``: The maximum value in the interval.

   ``mean``: The arithmetic mean of the values in the interval.

   ``median``: The 50th percentile value in the interval. This may be an approximation if the number of values in the interval is large.

   ``min``: The minimum value in the interval.

   ``pct_chg``: The percent change from the first value in the interval to the last value in the interval.

   ``sem``: The standard error of the mean in the interval.

   ``stddev``: The standard deviation with n-1 degrees of freedom of the values in the interval.

   ``sum``: The sum of the values in the interval.

**end_offset** (int, required)
   Relative to the current row's timestamp, the point at which the aggregation interval ends. Negative values move back in time; positive values move forward.

**include_end** (bool, optional)
   If ``True``, include the exact match of the end offset in the interval. If ``False``, do not include exact matches. Default value: ``False``.

**include_start** (bool, optional)
   If ``True``, include the exact match of the start offset in the interval. If ``False``, do not include exact matches. Default value: ``True``.

**input_columns** (str, tuple of str, or list of str, required)
   A list of column names to aggregate.

**max_window** (int, optional)
   If ``unit`` is set to ``rows``, the number of seconds in either direction of the current time that defines the window of time within which rows must occur to be included in the interval.

**new_columns** (str, tuple of str, or list of str, required)
   A list of column names to add to a table.

**start_offset** (int, required)
   Relative to the current row's timestamp, the point at which the aggregation interval begins. Negative values move back in time; positive values move forward.

**unit** (str, required)
   The unit by which an offset is measured. Possible values: ``seconds``, ``minutes``, ``hours``, ``days`` (measured in 24-hour increments from the current row), and ``rows``.

Returns
--------------------------------------------------
A tuple that defines the context operation to perform. The results of this context operation may then be passed to the :ref:`api-security-add-context` or :ref:`api-security-add-context-from` functions.

Example
--------------------------------------------------

.. code-block:: python

   bid_op = risp.create_context_op(new_column='bid_mean',
                                   input_column='bid',
                                   aggregate_op='mean',
                                   start_offset=-5,
                                   end_offset=5,
                                   unit='seconds')
   agg_table = risp.add_context(data,
                                timestamp_column='timestamp',
                                context_ops=bid_op)



.. _api-security-create-custom-op:

create_custom_op()
==================================================
Create an operation that processes data according to a custom function. May be used with other aggregation functions, such as :ref:`api-common-aggregate`.

Usage
--------------------------------------------------

.. code-block:: python

   create_custom_op(new_columns,
                    input_columns,
                    udf)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**input_columns** (str, tuple of str, or list of str, required)
   A list of column names to aggregate.

**new_columns** (str, tuple of str, or list of str, required)
   A list of column names to add to a table.

**udf** (function, required)
   The name of a custom function that defines how to process the data. If ``input_columns`` has one column, then the custom function should accept a list of values as input. If ``input_columns`` has more than one column, then the custom function should accept a list of lists where each list's length equals the number of columns in ``input_columns``. The number of items the custom function returns (either as a single item or multiple items in a tuple) must be equal to the number of columns in ``new_columns``.

Returns
--------------------------------------------------
An object that defines the operation to perform. The results of this context operation may then be passed to the :ref:`api-common-aggregate` function.

Example
--------------------------------------------------

.. code-block:: python

   spread_fn = lambda b : max(b) - min(b) if b else None
   bid_op = risp.create_custom_op(new_columns='bid_spread',
                                  input_columns='bid',
                                  udf=spread_fn)

   per_cust = risp.aggregate(data,
                             group_by_columns=['customer_id'],
                             aggregate_ops=[('trx_amount', 'mean'),
                                            ('trx_amount', 'sum'),
                                             bid_op])






.. _api-security-create-parser-hint-date:

create_parser_hint_date()
==================================================
Create a hint for parsing datetime values in a column when loading or importing a table.

Usage
--------------------------------------------------

.. code-block:: python

   create_parser_hint_date(column,
                           date_format)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str, required)
   The column that contains the datetime values to which this hint applies. If this value is "*", the hint is applied to all columns.

**date_format** (str, required)
   A string, in :ref:`strptime syntax <python-strptime-syntax>`, that identifies the format to use for date and time. This string is composed of several identifiers that denote the various fields of the date and time, along with any separators used for delimiting date and time fields.

Returns
--------------------------------------------------
A hint that may be passed to :ref:`api-common-load-tabular`, :ref:`api-common-load-crtable`, or ``cr.data_ingest_v1.load_text()``.

.. TODO: figure out that load_text() function, deprecate or ADD it to the doc.

Example
--------------------------------------------------

.. code-block:: python

   hint1 = risp.create_parser_hint_date(column='first_date',
                                        date_format='%Y-%m-%d %H:%M:%S')
   hint2 = risp.create_parser_hint_date(column='second_date',
                                        date_format='%Y-%m-%d %H:%M:%S')
   table = risp.load_crtable(path='myproject/',
                             hints=[hint1, hint2])



.. _api-security-create-parser-hint-maxerr:

create_parser_hint_maxerr()
==================================================
Create a hint that sets the maximum number of cell parser errors that may occur in a column (as a table is loaded) before an exception is raised.

.. note:: To create a hint that sets the maximum percentage of line parser errors allowed for an entire table, use the :ref:`api-security-create-parser-hint-table-maxerr` function.

Usage
--------------------------------------------------

.. code-block:: python

   create_parser_hint_maxerr(column,
                             max_errors=int)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str, required)
   The column to which this hint applies.

**max_errors** (int, required)
   The maximum number of cell parser errors for a column. When this value is exceeded an exception is raised. If this value is ``0`` an exception is raised on the first cell parser error. Missing values do not count as errors, but malformed values do. For example, a string value in a numeric column. Malformed rows count as errors for all columns in that row.

Returns
--------------------------------------------------
A hint that may be passed to :ref:`api-common-load-tabular`, :ref:`api-common-load-crtable`, or ``cr.data_ingest_v1.load_text()``.

.. TODO: figure out that load_text() function, deprecate or ADD it to the doc.

Example
--------------------------------------------------

.. code-block:: python

   hint3 = risp.create_parser_hint_maxerr(column='my_date',
                                          max_errors=500)
   table = risp.load_crtable(path='myproject/',
                             hints=hint3)




.. _api-security-create-parser-hint-missing:

create_parser_hint_missing()
==================================================
Create a hint for parsing missing values in a column when loading a table.

Several values are identified automatically---such as ``NA`` or empty cells---as markers for missing values. However, in some data sets, a missing numeric value might be coded as ``99`` or ``-1`` or a missing label might be coded as ``MISSING``. This function allows these types of values to be treated as markers for missing values.

Usage
--------------------------------------------------

.. code-block:: python

   create_parser_hint_missing(column,
                              additional_markers)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str, required)
   The column to which this hint applies.

**additional_markers** (str or list of str, required)
   A string (or list of strings) to be treated as markers for missing values in the column. Values are specified as strings, even if the value to be replaced is numeric.

Returns
--------------------------------------------------
A hint that may be passed to :ref:`api-common-load-tabular`, :ref:`api-common-load-crtable`, or ``cr.data_ingest_v1.load_text()``.

.. TODO: figure out that load_text() function, deprecate or ADD it to the doc.

Example
--------------------------------------------------

.. code-block:: python

   hint4 = risp.create_parser_hint_missing(column='mynumber',
                                           additional_markers=['0'])
   hint5 = risp.create_parser_hint_missing(column='mystring',
                                           additional_markers=['UNKNOWN'])
   table = risp.load_crtable(path='myproject/',
                             hints=[hint4, hint5])




.. _api-security-create-parser-hint-table-maxerr:

create_parser_hint_table_maxerr()
==================================================
Create a hint that sets the maximum percentage of line parser errors that may occur in a table (as that table is loaded) before an exception is raised. This function validates only that each line contains the correct number of items. It does not validate that items are of the correct type.

.. note:: To create a hint to set the maximum number of cell parser errors allowed per column, use the :ref:`api-security-create-parser-hint-maxerr` function.

.. warning:: Only one hint of this type may be specified when loading a table.

Usage
--------------------------------------------------

.. code-block:: python

   create_parser_hint_table_maxerr(max_percent)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**max_percent** (float, required)
   The maximum percentage of line parser errors for a table. When this value is exceeded an exception is raised. If this value is ``0`` an exception is raised on the first line parser error. 

Returns
--------------------------------------------------
A hint that may be passed to :ref:`api-common-load-tabular`, :ref:`api-common-load-crtable`, or ``cr.data_ingest_v1.load_text()``.

Example
--------------------------------------------------

.. code-block:: python

   hint3 = risp.create_parser_hint_table_maxerr(5.0)
   table = risp.load_crtable(path='myproject/',
                             hints=hint3)





.. _api-security-get-checkpoints:

get_checkpoints()
==================================================
Get a list of all of the tables and/or models that are checkpointed in memory.

.. note:: Use :ref:`api-security-load-checkpoint` to retrieve a checkpointed item.

Usage
--------------------------------------------------

.. code-block:: python

   get_checkpoints(item_type=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**item_type** (str, required)
   The type of objects to list. Possible values: ``table``, ``model``, or ``None``. If ``None`` is specified, both tables and models are returned.

Returns
--------------------------------------------------
A LocalDataTable containing the key, type, and creation timestamp for each item.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.load_crtable(path='home/testdata02/')
   risp.checkpoint(data, key='data_raw')
   risp.get_checkpoints('table')

will print information similar to:

.. code-block:: sql

    Key        Item Type   Create Time
   ---------- ----------- ----------------------------
    data_raw   table       2015-02-25 18:43:16.758198
   ---------- ----------- ----------------------------






.. _api-security-get-udf-debug-logs:

get_udf_debug_logs()
==================================================
Return the log statements issued by custom functions that are used on a table.

.. note:: Use the :ref:`api-security-udf-debug-log` function to record a log statement from inside a custom function.

Usage
--------------------------------------------------

.. code-block:: python

   get_udf_debug_logs(table)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The name of the table for which log statements are returned.

Returns
--------------------------------------------------
A cr.datatable.DataTable that lists the logs including ``timestamp``, ``opaque_id``, and ``statement``. The ``opaque_id`` can be used to group messages sent from the same node.

Example
--------------------------------------------------

.. code-block:: python

   def cut_in_half(a):
     if not isinstance(a, int):
       risp.udf_debug_log('Got a non-integer %s' % a)
         return None
     else:
       return half * 0.5
   new_table = risp.add_columns(old_table,
                                input_columns='full',
                                new_columns='half',
                                udf=cut_in_half)
   print risp.get_udf_debug_logs(new_table)




.. _api-security-load-checkpoint:

load_checkpoint()
==================================================
Retrieve a table or model checkpointed in memory with the provided key.

.. note:: Use :ref:`api-security-checkpoint` to create a checkpoint. Use :ref:`api-security-get-checkpoints` to view all checkpointed items.

Usage
--------------------------------------------------

.. code-block:: python

   load_checkpoint(key)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**key** (str, required)
   A key to identify the table or model in memory.

Returns
--------------------------------------------------
A cr.datatable.DataTable.

.. TODO: originally said "A cr.datatable.DataTable or cr.science.model."

Example
--------------------------------------------------

.. code-block:: python

   risp.checkpoint(table, key='clean_primary')
   risp.get_checkpoints('table')

will print information similar to:

.. code-block:: sql

   Key             Item Type   Create Time
   -------------- ----------- ----------------------------
   clean_primary   table       2015-03-02 18:20:27.333698
   -------------- ----------- ----------------------------

and then clean the table:

.. code-block:: python

   table = risp.load_checkpoint('clean_primary')




.. _api-security-measure-confusion-matrix:

measure_confusion_matrix()
==================================================
Calculates a confusion matrix for a table of predictions produced from a binary or multiclass classification model.

Usage
--------------------------------------------------

.. code-block:: python

   measure_confusion_matrix(table,
                            threshold=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.science.PredictionTable, required)
   A table that contains inputs, a column of observed values of the target, and a column of predicted values of the target.

**threshold** (float, optional)
   A number between 0 and 1 that determines the threshold for predictions to be classified as either 0 or 1 (binary classification only). For example, if ``threshold=0.6`` and the predicted value is 0.7, it will be classified as 1. Default value: ``0.5``.

Returns
--------------------------------------------------
A LocalDataTable containing columns and rows equal to the number of classes. The columns show the predicted records in each class and the rows show the actual records in each class.

For example, predictions from a binary classification model would return a table with two columns and two rows. Clockwise from the upper left cell this would show the number of true positives, false positives, true negatives, and false negatives.

Example
--------------------------------------------------

.. code-block:: python

   risp.learn_model(target='appetency',
                    model_type='BINARY_CLASSIFICATION',
                    train_table=train, tune_table=tune)
   risp.predict(model, table=test, return_reasons=True)
   risp.measure_confusion_matrix(results, threshold=0.5)

will print information similar to:

.. code-block:: sql

   Predicted (down), Actual (across)   0        1
   ---------------------------------- -------- -------
   0                                   9821.0   173.0
   1                                   75.0     4.0
   ---------------------------------- -------- -------




.. _api-security-measure-input-importance:

measure_input_importance()
==================================================
Generates a report describing how model accuracy varies for different sets of input columns. Use to identify information leakage and understand which inputs are the most important for making predictions.

Usage
--------------------------------------------------

.. code-block:: python

   measure_input_importance(target,
                            model_type,
                            train_table,
                            tune_table,
                            test_table,
                            input_columns=None,
                            style='one-out',
                            steps=None,
                            metric=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**input_columns** (list of str, optional)
   A list of column names in the data to use as model inputs. If not specified, all columns that are not the target are used as inputs.

**metric** (str, optional)
   The scoring metric used to measure model quality. Available scoring metrics:

   .. list-table::
      :widths: 80 420
      :header-rows: 1

      * - Metric
        - Description
      * - **AUC**
        - .. include:: ../../includes_terms/term_metric_auc.rst
      * - **F1**
        - .. include:: ../../includes_terms/term_metric_f1.rst
      * - **LOGLOSS**
        - .. include:: ../../includes_terms/term_metric_logloss.rst
      * - **MAE**
        - .. include:: ../../includes_terms/term_metric_mae.rst
      * - **MAPE**
        - .. include:: ../../includes_terms/term_metric_mape.rst
      * - **PREC**
        - .. include:: ../../includes_terms/term_metric_prec.rst
      * - **REC**
        - .. include:: ../../includes_terms/term_metric_rec.rst
      * - **RMSE**
        - .. include:: ../../includes_terms/term_metric_rmse.rst

**model_type** (str, required)
   The output type for the model. Available values are:

   A ``BINARY_CLASSIFICATION`` model predicts one of two discrete outcomes or states, each of which has been encoded as a 1 or 0. For example, predicting a yes or no decision.

   A ``MULTI_CLASSIFICATION`` model predicts one of many discrete outcomes or states, each of which has been encoded as an integer numbered from 0. For example, predicting the make of a car.

   A ``LINEAR_REGRESSION`` model predicts a continuous value as its target. For example, predicting price.

**steps** (int, optional)
   The maximum number of iterations for ``backward`` or ``forward`` style reports. If you do not set a value, a full report that iterates through every input will be generated. This value is ignored for ``one-in`` and ``one-out`` reports.

**style** (str, optional)
   The report style used for analysis. Possible styles: ``backward``, ``forward``, ``one-in``, and ``one-out``.

   Use ``backward`` to build a series of models that starts with the full model and then with each iteration, removes the input with the least influence on model accuracy. Sometimes referred to as backward stepwise regression. Use the ``steps`` argument to specify the maximum number of model iterations.

   Use ``forward`` to build a series of models that starts with a single input and, with each iteration, adds the input with the greatest influence on model accuracy. This is sometimes referred to as forward stepwise regression. Use the ``steps`` argument to specify the maximum number of model iterations.

   Use ``one-in`` to learn a series of models, each of which uses a single input. This report is especially useful in identifying information leakage.

   Use ``one-out`` to learn a series of models, each of which removes a single input from the full model. This report can help identify inputs that have very little influence on the accuracy of predictions. Default value.

**target** (str, required)
   The column containing the value to be predicted.

**test_table** (cr.datatable.DataTable, required)
   .. include:: ../../includes_terms/term_table_test.rst

**train_table** (cr.datatable.DataTable, required)
   .. include:: ../../includes_terms/term_table_train.rst

**tune_table** (cr.datatable.DataTable, required)
   .. include:: ../../includes_terms/term_table_tune.rst

Returns
--------------------------------------------------
A LocalDataTable containing the metric scores for each model learned.

Example
--------------------------------------------------

First, split the table.

.. code-block:: python

   triple = risp.split_table(table, proportions=[60, 20, 20])
   train, tune, test = triple

Then generate a one-in report.

.. code-block:: python

   report = risp.measure_input_importance(target='Target',
                                          model_type='LINEAR_REGRESSION',
                                          train_table=train,
                                          tune_table=tune,
                                          test_table=test,
                                          style='one-in',
                                          metric='RMSE')

Then print the report:

.. code-block:: python

   print report
   Metric Name                                 RMSE
   Report Style                                one-in
   Best Score                                  159548.0
   Num. Models Tried                           147
   Variable(s): all input variables            159548.0
   Variable(s): include (BldgGrade)            180204.6875






.. _api-security-measure-learning-curve:

measure_learning_curve()
==================================================
Measure a learning curve, which reflects how the model accuracy varies as a function of the number of input rows. Use to identify at what point additional data stops improving the accuracy of the model.

.. warning:: When working on a large data set with many features, it is recommended to run this function on a multi-node cluster.

Usage
--------------------------------------------------

.. code-block:: python

   measure_learning_curve(target,
                          model_type,
                          train_table,
                          tune_table,
                          test_table,
                          input_columns=None,
                          metric=None,
                          max_train_size=150000,
                          key=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**input_columns** (list of str, optional)
   A list of column names in your data to use as model inputs. If you do not set a value, all columns that are not the target will be used as inputs.

**key** (str, optional)
   Key to use to checkpoint the model learned using the largest training set considered.

**max_train_size** (int, optional)
   The maximum number of rows to use when learning a model.

**metric** (str, optional)
   The scoring metric used to measure model quality. Available scoring metrics:

   .. list-table::
      :widths: 80 420
      :header-rows: 1

      * - Metric
        - Description
      * - **AUC**
        - .. include:: ../../includes_terms/term_metric_auc.rst
      * - **F1**
        - .. include:: ../../includes_terms/term_metric_f1.rst
      * - **LOGLOSS**
        - .. include:: ../../includes_terms/term_metric_logloss.rst
      * - **MAE**
        - .. include:: ../../includes_terms/term_metric_mae.rst
      * - **MAPE**
        - .. include:: ../../includes_terms/term_metric_mape.rst
      * - **PREC**
        - .. include:: ../../includes_terms/term_metric_prec.rst
      * - **REC**
        - .. include:: ../../includes_terms/term_metric_rec.rst
      * - **RMSE**
        - .. include:: ../../includes_terms/term_metric_rmse.rst

**model_type** (str, required)
   The output type for the model. Available values are:

   A ``BINARY_CLASSIFICATION`` model predicts one of two discrete outcomes or states, each of which has been encoded as a 1 or 0. For example, predicting a yes or no decision.

   A ``MULTI_CLASSIFICATION`` model predicts one of many discrete outcomes or states, each of which has been encoded as an integer numbered from 0. For example, predicting the make of a car.

   A ``LINEAR_REGRESSION`` model predicts a continuous value as its target. For example, predicting price.

**target** (str, required)
   The column containing the value to be predicted.

**test_table** (cr.datatable.DataTable, required)
   The table that contains historical data not used to learn the model that is used to evaluate a model.

**train_table** (cr.datatable.DataTable, required)
   The table that the software uses to learn thousands of possible models, selecting the ones that most accurately predict the known target values.

**tune_table** (cr.datatable.DataTable, required)
   The table that the software uses to check the accuracy of intermediate models to select the best tuning parameters to optimize automated model learning.

Returns
--------------------------------------------------
A LocalDataTable containing the training set size and metric score.

Example
--------------------------------------------------
First split the data:

.. code-block:: python

   train, tune, test = risp.split_table(table,
                                        proportions=[60, 20, 20])

Then measure the learning curve:

.. code-block:: python

   lc = risp.measure_learning_curve(target='a',
                                    model_type='LINEAR_REGRESSION',
                                    train_table=train,
                                    tune_table=tune,
                                    test_table=test,
                                    input_columns=['b', 'c'],
                                    key='lc_model')

Then view the learning curve:

.. code-block:: console

   $ print lc

.. code-block:: python

    Training set size   Score (RMSE)
   ------------------- ---------------
    64                  2.0253051927
    128                 1.41870292914
    256                 1.30678385209
    512                 1.28354649346
    1024                1.27024370338
    2048                1.25774672241
    4096                1.24958806604
    8192                1.24639283485
    11912               1.24612843818
   ------------------- ---------------

Use the model produced by this function.

.. code-block:: python

   model = risp.load_checkpoint('lc_model')
   risp.predict(model, table=new_data)




.. _api-security-measure-score-curve:

measure_score_curve()
==================================================
Calculate score curves for a table of predictions produced from a binary classification model to evaluate its performance as the threshold changes.

Usage
--------------------------------------------------

.. code-block:: python

   measure_score_curve(table,
                       curve_type,
                       top_k=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**curve_type** (str, required)
   A curve type used to measure model quality. Available values are:

   ``PREC@K``: Precision at K curve. Plots the proportion of predictions in the ``top_k`` that were true positives.

   ``REC@K``: Recall at K curve. Plots the proportion of true positives that were predicted in the ``top_k``.

   ``ROC``: Receiver operating characteristic curve. Plots the true positive rate against the false positive rate for different thresholds on the predicted probabilities.

**table** (cr.science.PredictionTable, required)
   A table that contains inputs, a column of observed values of the target, and a column of predicted values of the target.

**top_k** (int, optional)
   The number of top samples to use for ``PREC@K`` and ``REC@K`` calculations. Ignored for the ``ROC`` curve type. The samples are ordered by predicted probability in descending order.

Returns
--------------------------------------------------
A LocalDataTable containing two columns. The columns vary by curve type. Use :ref:`api-security-plot-line` to view the curves graphically.

Example
--------------------------------------------------

.. code-block:: python

   model = risp.learn_model(target='appetency',
                            model_type='BINARY_CLASSIFICATION',
                            train_table=train, tune_table=tune)
   results = risp.predict(model, table=test, return_reasons=True)
   roc = risp.measure_score_curve(results, curve_type='ROC')
   risp.plot_line(roc, x_column=0, y_columns=1)





.. _api-security-plot-bar:

plot_bar()
==================================================
Use tables produced from the :ref:`api-common-get-example-rows`, :ref:`api-security-measure-input-importance`, :ref:`api-security-score-predictions`, or :ref:`api-security-measure-learning-curve` functions to create a bar chart with the y-axis columns plotted against the x-axis.

Usage
--------------------------------------------------

.. code-block:: python

   plot_bar(table,
            x_column,
            y_columns,
            title=None,
            x_label=None,
            y_label=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (LocalDataTable, required)
   The table from which the data is to be plotted.

**title** (str, optional)
   The title for the bar chart. Default value: ``None``.

**x_column** (str or int, required)
   The name or index of the column to use on the x-axis.

**x_label** (str, optional)
   The label for the x-axis. If not specified, the value for ``x_column`` is used as the ``x_label``.

**y_columns** (str, int, tuple of str, tuple of int, list of str, or list of int, required)
   The names or indices of the columns to plot against the x-axis.

**y_label** (str or list of str, optional)
   The label for the y-axis. If not specified, the ``y_columns`` names are used as the ``y_label``.

Returns
--------------------------------------------------
An object that holds a reference to the plot. This object may be passed to the :ref:`api-common-save-plot` function.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.get_example_rows(table)
   bar_plot = risp.plot_bar(data,
                            x_column='Date',
                            y_columns='Name',
                            title='Title')





.. _api-security-plot-histogram:

plot_histogram()
==================================================
Create a histogram for the given column to represent the distribution of the data. Plot table data may be sourced from the :ref:`api-common-get-example-rows`, :ref:`api-security-measure-input-importance`, :ref:`api-security-score-predictions`, and :ref:`api-security-measure-learning-curve` functions.

Usage
--------------------------------------------------

.. code-block:: python

   plot_histogram(table,
                  column,
                  title=None,
                  x_label=None,
                  num_bins=25)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str or int, required)
   The name or index of the column to use.

**num_bins** (int, optional)
   The number of bins against which data is divided. Default value: ``25``.

**table** (LocalDataTable, required)
   The table that contains the data to be plotted.

**title** (str, optional)
   The title for the histogram. Default value: ``None``.

**x_label** (str, optional)
   The label for the x-axis. If not specified, the value of ``column`` is used to label the x-axis.

Returns
--------------------------------------------------
An object that holds a reference to the plot. This object may be passed to the :ref:`api-common-save-plot` function.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.get_example_rows(table, count=10000)
   histogram = risp.plot_histogram(data,
                                   column='Price',
                                   title='Price Distribution',
                                   num_bins=50)




.. _api-security-plot-line:

plot_line()
==================================================
Create a line chart with the y-axis columns plotted against the x-axis. Plot table data may be sourced from the :ref:`api-common-get-example-rows`, :ref:`api-security-measure-input-importance`, :ref:`api-security-score-predictions`, and :ref:`api-security-measure-learning-curve` functions.

Usage
--------------------------------------------------

.. code-block:: python

   plot_line(table,
             x_column,
             y_columns,
             title=None,
             x_label=None,
             y_label=None,
             x_range=None,
             y_range=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (LocalDataTable, required)
   The table that contains the data against which the line chart is plotted.

**title** (str, optional)
   The title for the chart. Default value: ``None``.

**x_column** (str or int, required)
   The name or index of the column to use on the x-axis.

**x_label** (str, optional)
   The label for the x-axis. If not specified, the value of ``x_column`` is used.

**x_range** (tuple, optional)
   A 2-tuple that defines the minimum and maximum values to display on the x-axis. Default value: ``None``.

**y_columns** (str, int, tuple of str, tuple of int, list of str, or list of int, required)
   The names or indices of the columns to plot against the x-axis.

**y_label** (str, optional)
   The label for the x-axis. If not specified, the values of ``y_columns`` are used.

**y_range** (tuple, optional)
   A 2-tuple that defines the minimum and maximum values to display on the y-axis. Default value: ``None``.

Returns
--------------------------------------------------
An object that holds a reference to the plot. This object may be passed to the :ref:`api-common-save-plot` function.

Example
--------------------------------------------------

.. code-block:: python

   model = risp.learn_model(target='appetency',
                            model_type='BINARY_CLASSIFICATION',
                            train_table=train,
                            tune_table=tune)
   results = risp.predict(model, table=test, return_reasons=True)
   roc = risp.measure_score_curve(results, curve_type='ROC')
   risp.plot_line(roc,
                  x_column='False positive rate',
                  y_columns='True positive rate')





.. _api-security-plot-scatter:

plot_scatter()
==================================================
Create a scatter plot with the y-axis columns plotted against the x-axis column. Plot table data may be sourced from the :ref:`api-common-get-example-rows`, :ref:`api-security-measure-input-importance`, :ref:`api-security-score-predictions`, and :ref:`api-security-measure-learning-curve` functions.

Usage
--------------------------------------------------

.. code-block:: python

   plot_scatter(table,
                x_column,
                y_columns,
                title=None,
                x_label=None,
                y_label=None,
                labels_column=None,
                x_range=None,
                y_range=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**labels_column** (str or int, optional)
   The name or index of the column in which labels for each point in the scatter plot are located.

**table** (LocalDataTable, required)
   The table that contains the data against which the scatter chart is plotted.

**title** (str, optional)
   The title for the chart. Default value: ``None``.

**x_column** (str or int, required)
   The name or index of the column to use on the x-axis.

**x_label** (str, optional)
   The label for the x-axis. If not specified, the value of ``x_column`` is used.

**x_range** (tuple, optional)
   A 2-tuple that defines the minimum and maximum values to display on the x-axis. Default value: ``None``.

**y_columns** (str, int, tuple of str, tuple of int, list of str, or list of int, required)
   The names or indices of the columns to plot against the x-axis.

**y_label** (str, optional)
   The label for the x-axis. If not specified, the values of ``y_columns`` are used.

**y_range** (tuple, optional)
   A 2-tuple that defines the minimum and maximum values to display on the y-axis. Default value: ``None``.

Returns
--------------------------------------------------
An object that holds a reference to the plot. This object may be passed to the :ref:`api-common-save-plot` function.

Example
--------------------------------------------------

.. code-block:: python

   data = risp.get_example_rows(table, count=10000)
   scatter_plot = risp.plot_scatter(data,
                                    x_column='SqFtTotLiving',
                                    y_columns='SalePrice')


.. _api-security-relate:

relate()
==================================================
Identify columns in two tables that contain overlapping data by measuring the Jaccard distance for each pair of columns. This can help identify the correct columns to use to join the two tables.

.. note:: Use the :ref:`api-common-join` function to join two tables according to the columns identified by this function.

Usage
--------------------------------------------------

.. code-block:: python

   relate(left_table,
          right_table,
          left_columns=None,
          right_columns=None,
          description='')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**description** (str, optional)
   A string to be added to log output.

**left_columns** (list of str, optional)
   A list of column names from ``left_table`` to analyze. If this value is not set, all columns are used.

**left_table** (cr.datatable.DataTable, required)
   The first table to be analyzed.

**right_columns** (list of str, optional)
   A list of column names from ``right_table`` to analyze. If this value is not set, all columns are used.

**right_table** (cr.datatable.DataTable, required)
   The second table to be analyzed.

Returns
--------------------------------------------------
A LocalDataTable describing the relationships between ``left_table`` and ``right_table``. The left coverage and right coverage values range from 0.0 to 1.0 and indicate the percentage of rows whose values have a counterpart in the other column. The coverage rate refers to the number of table cells that match, not the number of unique values that match. For example, if a column pair's left coverage rate is 0.5, then half of its values are matched in the right column.

Example
--------------------------------------------------

In this example, there are two potential join keys returned. Review the data to determine which is the best to use.

.. code-block:: python

   data_A = risp.load_crtable(path='home/user/data_A/')
   data_B = risp.load_crtable(path='home/user/data_B/')
   relate(data_A, data_B)

will print information similar to:

.. code-block:: sql

    Left_Column   Right_Column   Left_Coverage   Right_Coverage
   ------------- -------------- --------------- ----------------
    userid        id             0.95            0.23
    groupid       groupid        0.42            0.99
   ------------- -------------- --------------- ----------------



.. _api-security-score-predictions:

score_predictions()
==================================================
Scores the quality of predictions in a table that are obtained by making predictions with a model on a test table in which the target values are known.

Usage
--------------------------------------------------

.. code-block:: python

   score_predictions(table,
                     metrics=None,
                     group_by_columns=None,
                     threshold=None,
                     top_k=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**group_by_columns** (str or list of str, optional)
   A single column or list of columns by which to group results. Metrics are returned for each distinct category.

**metrics** (str or list of str, optional)
   A single scoring metric or a list of scoring metrics that are used to measure model quality. Available scoring metrics:

   .. list-table::
      :widths: 80 420
      :header-rows: 1

      * - Metric
        - Description
      * - **ACC**
        - .. include:: ../../includes_terms/term_metric_acc.rst
      * - **AUC**
        - .. include:: ../../includes_terms/term_metric_auc.rst
      * - **F1**
        - .. include:: ../../includes_terms/term_metric_f1.rst
      * - **LOGLOSS**
        - .. include:: ../../includes_terms/term_metric_logloss.rst
      * - **MAE**
        - .. include:: ../../includes_terms/term_metric_mae.rst
      * - **MAPE**
        - .. include:: ../../includes_terms/term_metric_mape.rst
      * - **PREC**
        - .. include:: ../../includes_terms/term_metric_prec.rst
      * - **PREC@K**
        - .. include:: ../../includes_terms/term_metric_prec_at_k.rst
      * - **REC**
        - .. include:: ../../includes_terms/term_metric_rec.rst
      * - **REC@K**
        - .. include:: ../../includes_terms/term_metric_rec_at_k.rst
      * - **RMSE**
        - .. include:: ../../includes_terms/term_metric_rmse.rst

**table** (cr.science.PredictionTable, required)
   A table that contains inputs, a column of observed values of the target, and a column of predicted values of the target.

**threshold** (float, optional)
   A number between 0 and 1 that determines the threshold for predictions to be classified as either 0 or 1 (binary classification only). For example, if threshold=0.6 and the predicted value is 0.7, it will be classified as 1. Default is 0.5. Used with PREC, REC, and F1 metrics and ignored for all others.

**top_k** (int, optional)
   The number of top samples to use for PREC@K and REC@K calculations (binary classification only). Ignored for all other metrics. The samples are ordered by predicted probability in descending order.

Returns
--------------------------------------------------
A LocalDataTable containing the model score and baseline model score measured using each of the metrics selected and that may be grouped by the values in ``group_by_columns``.

The baseline model predicts the mean of the target column for every row. The mean is computed from the full training data. For MULTI_CLASSIFICATION data, a vector of class frequency probabilities is used for the mean. For many metrics, this baseline shows the expected accuracy from random guessing.

Example
--------------------------------------------------

.. code-block:: python

   results = risp.predict(model, table=test_table)
   results_quality = risp.score_predictions(results,
                                            metrics=['RMSE',
                                                     'MAE',
                                                     'MAPE'])

will print information similar to:

.. code-block:: python

   print results_quality
   RMSE    MAE    MAPE    RMSE (base)    MAE (base)    MAPE (base)
   1.23    1.04   0.31    1.32           1.09          0.43




.. code-block:: python

   results_quality = risp.score_predictions(results,
                                            metrics='RMSE',
                                            group_by_columns=['color'])

will print information similar to:

.. code-block:: python

   print results_quality
   (color)     RMSE    RMSE (base)
   blue        1.33    1.49
   red         1.54    1.67






.. _api-security-summarize-model:

summarize_model()
==================================================
List summary information about a model, including which inputs are most important for making predictions.

A ModelSummary lists the relative importance of each input, expressed as a percentage. This function measures the accuracy of the model by using a test set of data as a baseline, iterating over all inputs, replacing data for each input in the test with noise, and then comparing the accuracy of the predictions to the baseline accuracy.

A large decrease in accuracy indicates a very important input (large percentage). A small decrease in accuracy indicates a less important input (small percentage). If replacing an input with noise actually increases the accuracy, this indicates that the input leads to overfitting and should be removed. The relative importance of such inputs is expressed as negative percentage.

.. note:: When the ``exploration`` argument for the :ref:`api-common-learn-model` function is set to ``INTERACTIONS`` the ModelSummary may also list the relative importance of the interaction between two inputs.

Usage
--------------------------------------------------

.. code-block:: python

   risp.summarize_model(model,
                        table,
                        display_limit=25)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**display_limit** (int, optional)
   The number of rows to show in the summary. Set to ``None`` to show all inputs. Default value: ``25``.

**model** (cr.science.Model, required)
   The model to summarize.

**table** (cr.datatable.DataTable, required)
   The table in which data for measuring input importance is located. In general, this is not the same data that is used to learn a model; however, in some cases comparing the results from separate runs of training and testing data is desired.

Returns
--------------------------------------------------
A ModelSummary that can be printed.

Example
--------------------------------------------------

.. code-block:: python

   model = risp.learn_model(target='SalePrice',
                            model_type='LINEAR_REGRESSION',
                            train_table=train, tune_table=tune,
                            exploration='INTERACTIONS')
   summary = risp.summarize_model(model, test_data, display_limit=3)

will print information similar to:

.. code-block:: python

   print summary
   Number of input columns: 20
   Score (RMSE): 151990.2498
   Baseline (RMSE): 257434.6353
   Relative Importance (%)   Inputs                    Score Without (RMSE)
   45.61                     BldgGrade                 202763.28
   12.83                     YrBuilt                   166269.86
   9.89                      Address                   163005.53
   5.55                      SqFtTotLiving             158164.77
   3.22                      SqFt2ndFloor              155578.81
   2.61                      SqFt1stFloor              154901.35
   2.58                      SqFtTotBasement           154866.38




.. _api-security-udf-debug-log:

udf_debug_log()
==================================================
Record a log statement from inside a custom function. Use this type of log statement to troubleshoot issues with custom functions.

.. note:: Use the :ref:`api-security-get-udf-debug-logs` function to gather all the log statements written by a custom function.

.. warning:: Each node in the compute cluster maintains only the last few log statements. It is recommended to use debug statements sparingly.

Usage
--------------------------------------------------

.. code-block:: python

   udf_debug_log(statement)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**statement** (str, required)
   The log statement to be recorded.

   .. note:: A log statement may not exceed 200 characters. Log statements that exceed that amount are truncated.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   def cut_in_half(a):
     if not isinstance(a, int):
       risp.udf_debug_log('Got a non-integer %s' % a)
         return None
     else:
       return half * 0.5
   new_table = risp.add_columns(old_table,
                                input_columns='full',
                                new_columns='half',
                                udf=cut_in_half)

