.. 
.. xxxxx
.. 

==================================================
Metadata API
==================================================

The Metadata API may be used to work with the metadata library for user and application development.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Metadata API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.metadata as metadata

and then for each use of a function in this API within the customization package add ``security.`` as the first part of the function name. For example:

.. code-block:: python

   metadata.get_namespaces()

or:

.. code-block:: python

   metadata.get_namespaces()


The following functions may be used to build customized tables, models, and transaction behaviors:

* :ref:`api-metadata-delete`
* :ref:`api-metadata-get`
* :ref:`api-metadata-get-all`
* :ref:`api-metadata-get-namespaces`
* :ref:`api-metadata-set`



.. _api-metadata-delete:

delete()
==================================================
Delete metadata from a table or a column in a table.

Usage
--------------------------------------------------

.. code-block:: python

   cr.metadata_v1.delete(table,
                         key,
                         namespace='user',
                         column=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table to delete metadata from.

**key** (str, required)
   The key associated with the metadata.

**namespace** (str, optional)
   The namespace for the metadata. Default is "user".

**column** (str, optional)
   The column to delete metadata from. Default is None, which indicates to delete the table-wide metadata instead of the column-specific metadata.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   >>> metadata.delete(table=mytable, key='outdated_key')



.. _api-metadata-get:

get()
==================================================
Get metadata associated with specific key for a table or a column in a table.

Usage
--------------------------------------------------

.. code-block:: python

   cr.metadata_v1.get(table,
                      key,
                      namespace='user',
                      column=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table to get metadata from.

**key** (str, required)
   The key associated with the metadata.

**namespace** (str, optional)
   The namespace for the metadata. Default is "user".

**column** (str, optional)
   The column to get metadata from. Default is None, which indicates to read the table-wide metadata instead of the column-specific metadata.

Returns
--------------------------------------------------
The value associated with the given key.

Example
--------------------------------------------------

.. code-block:: python

   >>> value = metadata.get(table=mytable, key='source')



.. _api-metadata-get-all:

get_all()
==================================================
Get all of the metadata from a table or a column in a table.

Usage
--------------------------------------------------

.. code-block:: python

   cr.metadata_v1.get_all(table,
                          namespace='user',
                          column=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table to get metadata from.

**namespace** (str, optional)
   The namespace for the metadata. Default is "user".

**column** (str, optional)
   The column to get metadata from. Default is None, which indicates to read the table-wide metadata instead of the column-specific metadata.

Returns
--------------------------------------------------
A dictionary of key-value pairs of all of the metadata in the namespace.

Example
--------------------------------------------------

.. code-block:: python

   >>> meta_dict = metadata.get_all(table=mytable, namespace='fame')
   >>> for k in meta_dict:
   ...      print '%s : %s' % (k, meta_dict[k])




.. _api-metadata-get-namespaces:

get_namespaces()
==================================================
Get metadata namespaces from a table or a column in a table.

.. note:: The list will always include "internal" and "user".

Usage
--------------------------------------------------

.. code-block:: python

   cr.metadata_v1.get_namespaces(table,
                                 column=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table to get metadata namespaces from.

**column** (str, optional)
   The column to get namespaces from. Default is None, which indicates to use the table-wide metadata instead of the column-specific metadata.

Returns
--------------------------------------------------
A list of strings which are the metadata namespace names.

Example
--------------------------------------------------

.. code-block:: python

   >>> metadata.get_namespaces(table=mytable)




.. _api-metadata-set:

set()
==================================================
Set metadata from a table or a column in a table.

.. note:: Valid object types for value include basic types; uuid.UUID, datetime.datetime, datetime.timedelta, numpy.ndarray, and cr.storage types; and any user type that implements to_json() and from_json().

Usage
--------------------------------------------------

.. code-block:: python

   cr.metadata_v1.set(table,
                      key,
                      value,
                      namespace='user',
                      column=None,
                      overwrite=False)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable required)
   The table for which metadata is set.

**key** (str, required)
   The key associated with the metadata.

**value** (object, required)
   The value for the metadata.

**namespace** (str, required)
   The namespace for the metadata. Default value: ``user``.

**column** (str, required)
   The column to set metadata for. Default is None, which indicates to set the table-wide metadata instead of the column-specific metadata.

**overwrite** (bool, optional)
   If True, overwrite the previous value associated with the key, assuming one exists. Default is False.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   >>> metadata.set(table=mytable, key='stage', value='finished_stage_3')

