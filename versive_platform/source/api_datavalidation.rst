.. 
.. xxxxx
.. 

==================================================
Data Validation API
==================================================

The Data Validation API may be used for data validation functionality..

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Data Validation API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.datavalidation as datavalidation

and then for each use of a function in this API within the customization package add ``security.`` as the first part of the function name. For example:

.. code-block:: python

   datavalidation.DataValidation.check_data()

or:

.. code-block:: python

   datavalidation.DataValidation.create_report()


The following functions may be used to build customized tables, models, and transaction behaviors:

* :ref:`api-datavalidation-datavalidation`
* :ref:`check_data() <api-datavalidation-datavalidation-check-data>`
* :ref:`create_report() <api-datavalidation-datavalidation-create-report>`
* :ref:`get_compute_methods() <api-datavalidation-datavalidation-get-compute-methods>`
* :ref:`is_data_available() <api-datavalidation-datavalidation-is-data-available>`
* :ref:`api-datavalidation-singlecolumndatavalidation`
* :ref:`check_data() <api-datavalidation-singlecolumndatavalidation-check-data>`
* :ref:`create_report() <api-datavalidation-singlecolumndatavalidation-create-report>`
* :ref:`get_compute_methods() <api-datavalidation-singlecolumndatavalidation-get-compute-methods>`
* :ref:`is_data_available() <api-datavalidation-singlecolumndatavalidation-is-data-available>`
* :ref:`api-datavalidation-simplesinglecolumndatavalidation`
* :ref:`api-data-validation-tablesizedatavalidation`
* :ref:`check_data() <api-data-validation-tablesizedatavalidation-check-data>`
* :ref:`create_report() <api-data-validation-tablesizedatavalidation-create-report>`
* :ref:`get_compute_methods() <api-data-validation-tablesizedatavalidation-get-compute-methods>`
* :ref:`is_data_available() <api-data-validation-tablesizedatavalidation-is-data-available>`

.. _api-datavalidation-datavalidation:

Data Validation
==================================================
Base class for specifying a validation of a cr.data.DataTable.
 
Usage
--------------------------------------------------

* Class: ``cr.datavalidation.DataValidation``
* Bases: object

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``DataValidation`` class. For example:

.. code-block:: python

   dv = cr.datavalidation.DataValidation()

and then use that variable to call the individual methods:

.. code-block:: python

   dv.check_data()
   
Methods
--------------------------------------------------
The ``DataValidation`` class has the following methods:

* :ref:`api-datavalidation-datavalidation-check-data`
* :ref:`api-datavalidation-datavalidation-create-report`
* :ref:`api-datavalidation-datavalidation-get-compute-methods`
* :ref:`api-datavalidation-datavalidation-is-data-available`


.. _api-datavalidation-datavalidation-check-data:

dv.check_data()
--------------------------------------------------
Check whether a cr.data.DataTable meets the validation criteria.

.. note:: This function should not do long computations. It should fail if the data needed has not already been computed.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   check_data(self,
              table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to check.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
True if the check succeeded; False if it did not.


.. _api-datavalidation-datavalidation-create-report:

dv.create_report()
--------------------------------------------------
Return a string describing the validation result.

.. note:: This function should not do long computations. It should fail if the data needed has not already been computed.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   create_report(self,
                 table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to check.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A string describing the validation result.


.. _api-datavalidation-datavalidation-get-compute-methods:

dv.get_compute_methods()
--------------------------------------------------
Returns the methods that be used to process data to calculate metrics for validating the table. The mapper udf is called on each row, the reducer to run on mapped output, and a callback udf to process the reduced output.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_compute_methods(self,
                       table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to compute on.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Tuple of three udfs (mapper, reducer, callback).


.. _api-datavalidation-datavalidation-is-data-available:

dv.is_data_available()
--------------------------------------------------
Return True if the data needed to check the validation has already been computed.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   is_data_available(self,
                     table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to check.




.. _api-datavalidation-singlecolumndatavalidation:

Single Columns
==================================================
A class used to validate a single column in a cr.data.DataTable.

Usage
--------------------------------------------------

* Class: ``cr.datavalidation.SingleColumnDataValidation``
* Bases: ``cr.datavalidation_impl.DataValidation``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``SingleColumnDataValidation`` class. For example:

.. code-block:: python

   sc = cr.datavalidation.DataValidation()

and then use that variable to call the individual methods:

.. code-block:: python

   sc.check_data()

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column_name** (str, required)
   The column to validate.

**mapper** (function, required)
   Mapping from the column values to a value appropriate for a call to reduce.

.. 
.. CRMR is removed, which means this argument is deprecated?
.. 
.. **reducer** (crmr.rddreducers.Reducer, required)
..    Reducer for the values produced by the mapper. Commonly the mapper will evaluate to True/False and the reducer will count the number of each.
.. 

**checker** (function, required)
   Mapping from what is produced by the reducer to a pass/no-pass answer.

**metadata_key** (str, required)
   Key to use in the metadata to store the computed data.

**reporter** (function, optional)
   Method to report results in a human-readable way.

**metadata_namespace** (str, optional)
   Namespace to use for the metadata saving the computed data.


Methods
--------------------------------------------------
The ``SingleColumnDataValidation`` class has the following methods:

* :ref:`api-datavalidation-singlecolumndatavalidation-check-data`
* :ref:`api-datavalidation-singlecolumndatavalidation-create-report`
* :ref:`api-datavalidation-singlecolumndatavalidation-get-compute-methods`
* :ref:`api-datavalidation-singlecolumndatavalidation-is-data-available`

.. _api-datavalidation-singlecolumndatavalidation-check-data:

sc.check_data()
--------------------------------------------------
Check whether a cr.data.DataTable meets the validation criteria.

.. note:: This is just checking the already computed validation value.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   check_data(self,
              table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to check.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
True if the check succeeded; False if it did not.


.. _api-datavalidation-singlecolumndatavalidation-create-report:

sc.create_report()
--------------------------------------------------
Return a string describing the validation result.

.. note:: This function will fail if the data needed has not already been computed.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   create_report(self,
                 table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to check.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A string describing the validation result.


.. _api-datavalidation-singlecolumndatavalidation-get-compute-methods:

sc.get_compute_methods()
--------------------------------------------------
Returns the methods that be used to process data to calculate metrics for validating the table. The mapper udf is called on each row, the reducer to run on mapped output, and a callback udf to process the reduced output.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_compute_methods(self, table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to compute on.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Tuple of three udfs (mapper, reducer, callback).


.. _api-datavalidation-singlecolumndatavalidation-is-data-available:

sc.is_data_available()
--------------------------------------------------
Return True if the data needed to check the validation has already been computed.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   is_data_available(self,
                     table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to check.



.. _api-datavalidation-simplesinglecolumndatavalidation:

Simple Single Columns
==================================================
Simplified version of SingleColumnValidation for when each item is processed to True/False.

.. note:: Only one of ``max_count`` and ``max_percent`` should be set.

Usage
--------------------------------------------------

* Class: ``cr.datavalidation.SimpleSingleColumnDataValidation``
* Bases: ``cr.datavalidation_impl.SingleColumnDataValidation``

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column_name** (str, required)
   Name of the column to validate.

**mapper** (function, required)
   Mapping function from value to True/False.

**metadata_key** (str, required)
   Key to use to store the metadata.

**max_count** (int, required)
   Maximum number of errors allowed to still pass validation.

**max_percent** (float, required)
   Maximum percentage of errors allowed to still pass validation.


.. _api-data-validation-tablesizedatavalidation:

Table Sizes
==================================================
A class to validate that a table has a minimum or maximum number of rows.

Usage
--------------------------------------------------

* Class: ``cr.datavalidation.TableSizeDataValidation``
* Bases: ``cr.datavalidation_impl.DataValidation``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``SingleColumnDataValidation`` class. For example:

.. code-block:: python

   ts = cr.datavalidation.DataValidation()

and then use that variable to call the individual methods:

.. code-block:: python

   ts.check_data()

Methods
--------------------------------------------------
The ``TableSizeDataValidation`` class has the following methods:

* :ref:`api-data-validation-tablesizedatavalidation-check-data`
* :ref:`api-data-validation-tablesizedatavalidation-create-report`
* :ref:`api-data-validation-tablesizedatavalidation-get-compute-methods`
* :ref:`api-data-validation-tablesizedatavalidation-is-data-available`

Arguments
--------------------------------------------------
The following arguments are available to this function:

**count** (cint, required)
   How many rows to compare to.

**check_maximum** (bool, required)
   Whether this is a maximum.


.. _api-data-validation-tablesizedatavalidation-check-data:

ts.check_data()
--------------------------------------------------
Check whether a cr.data.DataTable has a valid number of rows.

.. note:: This function will fail if the number of rows has not already been computed.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   check_data(self,
              table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to check.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
True if the check succeeded; False if it did not.


.. _api-data-validation-tablesizedatavalidation-create-report:

ts.create_report()
--------------------------------------------------
Return a string giving the number of rows in the table and the number being checked against.

.. note:: This function will fail if the number of rows has not already been computed.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   create_report(self,
                 table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to check.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A string describing the validation result.


.. _api-data-validation-tablesizedatavalidation-get-compute-methods:

ts.get_compute_methods()
--------------------------------------------------
Returns the methods that be used to process data to calculate metrics for validating the table. The mapper udf is called on each row, the reducer to run on mapped output, and a callback udf to process the reduced output.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_compute_methods(self,
                       table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to compute on.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Tuple of three udfs (mapper, reducer, callback).


.. _api-data-validation-tablesizedatavalidation-is-data-available:

ts.is_data_available()
--------------------------------------------------
Return True if the number of rows is already computed.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   is_data_available(self,
                     table)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (cr.data.DataTable, required)
   The cr.data.DataTable to check.
