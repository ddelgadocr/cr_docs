.. 
.. versive, primary, platform
.. 


==================================================
About the |versive| Platform
==================================================

.. include:: ../../includes_platform/platform_overview.rst

.. note:: The following sections descibe components within the |versive| platform.

.. _platform-data-tables:

Data Tables
==================================================
The |versive| platform uses the following types of data tables when processing data:

* :ref:`platform-data-tables-datatable`
* :ref:`platform-data-tables-localdatatable`

.. _platform-data-tables-datatable:

DataTable
--------------------------------------------------
.. include:: ../../includes_terms/term_table_datatable.rst

A DataTable has the following properties:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Property
     - Description
   * - **column_types**
     - A list of column data types.
   * - **column_names**
     - A list of column names.
   * - **num_rows**
     - An integer that specifies the number of rows in the table.
   * - **row_sample** (list)
     - A list of the first ten rows in the table.


.. _platform-data-tables-localdatatable:

LocalDataTable
--------------------------------------------------
.. include:: ../../includes_terms/term_table_localdata.rst

A LocalDataTable has the following properties:

.. list-table::
   :widths: 200 400
   :header-rows: 1

   * - Property
     - Description
   * - **column_types**
     - A list of column data types.
   * - **column_names**
     - A list of column names.
   * - **num_rows**
     - An integer that specifies the number of rows in the table.
   * - **row_sample** (list)
     - A list of the first ten rows in the table.

The data in a LocalDataTable may be accessed similar to data that is stored in a list.

**To print all rows**

.. code-block:: python

   >>> print table

**To print the first ten rows**

.. code-block:: python

   >>> print table[:10]

**To print the eighth row**

.. code-block:: python

   >>> print table[7]

**To print the third column in the second row**

.. code-block:: python

   >>> print table[1][2]



.. _platform-metrics:

Metrics
==================================================
The |versive| platform makes available the following metrics to use for processing data:

* :ref:`platform-metric-auc`
* :ref:`platform-metric-acc`
* :ref:`platform-metric-f1`
* :ref:`platform-metric-logloss`
* :ref:`platform-metric-multilogloss`
* :ref:`platform-metric-mae`
* :ref:`platform-metric-malq`
* :ref:`platform-metric-mape`
* :ref:`platform-metric-precision`
* :ref:`platform-metric-precision-at-k`
* :ref:`platform-metric-qape`
* :ref:`platform-metric-recall`
* :ref:`platform-metric-recall-at-k`
* :ref:`platform-metric-rmse`


.. _platform-metric-auc:

Area Under the Curve (AUC)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_auc.rst

.. _platform-metric-acc:

Classification Accuracy (ACC)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_acc.rst

.. _platform-metric-f1:

F1 score (F1)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_f1.rst

.. _platform-metric-logloss:

Logistic Loss (LOGLOSS)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_logloss.rst

.. 
..     """Normalized logistic loss scoring metric. This a base class that is
..     not intended to be used directly. It encapsulates the shared functionality
..     between LogLoss and MultiClassLogLoss.
..     """
.. 
..     """Normalized logistic loss scoring metric. This scores
..     prediction records::
.. 
..         (y_1, o_1, w_1)
..         (y_2, o_2, w_2)
..         ...
..         (y_N, o_N, w_N)
.. 
..     where the y-values are the correct output, o-values are the model
..     output, and w-values are weights for each record. The contribution
..     of row (y, o, w) to the metric is::
.. 
..         ll_i = w * ( y*log(o/y) + (1-y)*log((1-o)/(1-y)) )
.. 
..     Pass this metric to a :py:class:`cr.data.PredictionTable` object to compute::
.. 
..         LogLoss = - 1/sum(w) * sum(ll_i)
.. 
..     Note that the o-values are smoothed by a small epsilon factor when
..     they are equal to 1 or 0.
..     """
.. 

.. _platform-metric-multilogloss:

Logistic Loss Baseline Model (MULTILOGLOSS)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_multilogloss.rst

.. 
..     """Normalized multiclass logistic loss scoring metric. This scores
..     prediction records::
.. 
..         (y_1, o_1, w_1)
..         (y_2, o_2, w_2)
..         ...
..         (y_N, o_N, w_N)
.. 
..     where the y-values are the correct output (class id), o-values are the
..     model output, and w-values are weights for each record. Note that each
..     o_i is a numpy vector of length num_classes. Now let t_i be a vector
..     of the same length with a 1 in position y_i and 0s elsewhere. So if
..     the correct output (y_i) is 4, then t_i = [0, 0, 0, 0, 1, 0, ... 0].
..     Then the contribution of row (y, o, w) to the metric is::
.. 
..         ll_i = sum( y*log(t/y) )
.. 
..     where the sum runs over the elements of t and y (i.e. one element for
..     each class)
.. 
..     Pass this metric to a :py:class:`cr.data.PredictionTable` object to compute::
.. 
..         LogLoss = - 1/sum(w) * sum(ll_i)
.. 
..     Note that the o-values are smoothed by a small epsilon factor when
..     they are equal to 1 or 0.
..     """
.. 

.. _platform-metric-mae:

Mean Absolute Error (MAE)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_mae.rst

.. 
..     """Mean absolute error (MAE) scoring metric. This scores
..     prediction records::
.. 
..         (y_1, o_1, w_1)
..         (y_2, o_2, w_2)
..         ...
..         (y_N, o_N, w_N)
.. 
..     where the y-values are the correct output, o-values are the model
..     output, and w-values are weights for each record.  Pass this to a
..     PredictionRDD object to compute::
.. 
..         MAE = 1/sum(w[1:N]) sum_i w_i * |y_i - o_i|
.. 
..     """
.. 

.. _platform-metric-malq:

Mean Absolute Log-quotient (MALQ)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_malq.rst

.. 
..     """Mean absolute log-quotient (MALQ) scoring metric.  It is a
..     relative error metric that measures the average order-of-magnitude
..     difference between predicted and true values.  Unlike mean
..     absolute percent error (MAPE), MALQ error is symmetric for over-
..     and under-predictions.  Works for regression predictions.
.. 
..     Optional Arguments:
..         threshold (float):
..             Values where |v| < threshold are treated as zero for MALQ
..             scoring.  Default threshold is 1e-4.  Set this based on
..             measurement precision for the true values and based on
..             subjective judgement: what is the smallest value that
..             domain experts treat as distinct from zero?
.. 
..     TECHNICAL DETAILS
.. 
..     This scores prediction records as:
.. 
..         (y_1, o_1, w_1)
..         (y_2, o_2, w_2)
..         ...
..         (y_N, o_N, w_N)
.. 
..     where the y-values are the target value, o-values are model
..     predictions, and w-values are weights for each record.  Pass this
..     metric to PredictionTable.score() to compute:
.. 
..         MALQ = 1/sum(w[1:N]) sum_i w_i * abs(log(Q_i))
.. 
..     where Q_i = o_i / y_i .
.. 
..     This is only defined when both o_i and y_i are positive.  To
..     address this, this implementation extends Tofallis' definition to
..     handle both zero and negative values.  Because:
.. 
..         log(Q_i) = log(o_i) - log(y_i)
.. 
..     we can think of log(Q_i) as the distance between o_i and y_i on a
..     logarithmic number line.  Suppose any values < 1e-4 should be
..     treated as essentially zero (see threshold argument above).  Then
..     the logarithmic number line for -10^-1, -10^-2, ..., 10^-2, 10^-1
..     looks like:
.. 
..         <-------------------------------|----------------------------->
..                     -1   -2   -3   -4   0   -4   -3   -2   -1
.. 
..     To compute MALQ for zero and negative values, we want to assign
..     them to new values that let us keep counting downwards in
..     logarithmic increments:
.. 
..         <-------------------------------|----------------------------->
..                     -9   -8   -7   -6  -5   -4   -3   -2   -1
.. 
..     Mathematically, we define a log-ordinal function logo(x) as:
.. 
..         logo(x) = log(threshold) - 1              if |x| < threshold
..         logo(x) = log(x)                          if x >= threshold
..         logo(x) = logo(0) + (logo(0) - log(|x|))
..                 = 2*logo(0) - log(|x|)            if x <= -threshold
.. 
..     References:
.. 
..         Tofallis (2015).  A better measure of relative prediction
..         accuracy for model selection and model estimation.  Journal of
..         the Operational Research Society 66:1352-1362.
..     """
.. 

.. _platform-metric-mape:

Mean Absolute Percentage Error (MAPE)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_mape.rst

.. 
..     """Mean absolute percent error (MAPE) scoring metric.  It is used
..     to measure the average percentage difference between a prediction
..     and the true value.  It is appropriate when data are far from zero
..     and share a common scale.
.. 
..     This scores prediction records:
.. 
..         (y_1, o_1, w_1)
..         (y_2, o_2, w_2)
..         ...
..         (y_N, o_N, w_N)
.. 
..     where the y-values are the correct output, o-values are the model
..     output, and w-values are weights for each record.  Pass this to a
..     PredictionRDD object to compute:
.. 
..                                                \|y_i - o_i\|
..         MAPE = 100 * 1/sum(w[1:N]) sum_i w_i * -----------
..                                                   \|y_i\|
.. 
..     When \|y_i\| is very small or equal to 0, MAPE can be very large (or
..     infinite) due to just a few records.  To make the metric better
..     behaved, this implementation applies a minimum threshold for the
..     denominator.  If \|y_i\| < threshold, threshold is used for the
..     denominator instead.  The default threshold is 1, but can be
..     specified in the constructor.
.. 
..     Optional Arguments:
..         threshold (float):
..             Smallest allowed value for the denominator.  Default is 1.
..     """
.. 

.. _platform-metric-precision:

Precision (PREC)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_prec.rst

.. _platform-metric-precision-at-k:

Precision at K
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_prec_at_k.rst

.. _platform-metric-qape:

Quantile absolute percentage error (QAPE)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_qape.rst

.. 
..     '''
..     Quantile absolute percent error (QAPE) scoring metric. It is used
..     to measure the absolute percentage difference between a prediction
..     and the true value. It is appropriate when data are far from zero,
..     share a common scale, and may contain outliers.
.. 
..     This scores prediction records:
.. 
..         (y_1, o_1, w_1)
..         (y_2, o_2, w_2)
..         ...
..         (y_N, o_N, w_N)
.. 
..     where the y-values are the correct output, o-values are the model
..     output, and w-values are weights for each record. Currently, quantiles
..     are computed without taking into account weight. Pass this to a
..     PredictionRDD object to compute:
.. 
..                                    \|y_i - o_i\|
..         QAPE = 100 * q'th_quantile(-----------)
..                                       \|y_i\|
.. 
..     When \|y_i\| is very small or equal to 0, QAPE can be very large (or
..     infinite) due to just a few records. To make the metric better
..     behaved, this implementation applies a minimum threshold for the
..     denominator. If \|y_i\| < threshold, threshold is used for the
..     denominator instead. The default threshold is 1, but can be
..     specified in the constructor.
.. 
..     Optional Arguments:
..         threshold (float):
..             Smallest allowed value for the denominator. Default is 1.
..         q (float):
..             Desired quantile summary. Default is 0.5 (median).
..     '''
.. 

.. _platform-metric-recall:

Recall (REC)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_rec.rst

.. _platform-metric-recall-at-k:

Recall at K
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_rec_at_k.rst

.. _platform-metric-rmse:

Root Mean Squared Error (RMSE)
--------------------------------------------------
.. include:: ../../includes_terms/term_metric_rmse.rst

.. 
.. Root mean squared error (RMSE) scoring metric. This scores prediction records:
.. 
..    (y_1, o_1, w_1)
..    (y_2, o_2, w_2)
..    ...
..    (y_N, o_N, w_N)
.. 
.. where the y-values are the target value, o-values are model predictions, and w-values are weights for each record. Works for regression and binary classification predictions, where y and o are scalars, and for multi-class predictions where o is a length k numpy vector.
.. 
.. In the case of multi-class, let t_i also be a vector of length k with a 1 in position y_i and 0s elsewhere. So if the correct output (y_i) is 4, then t_i = [0, 0, 0, 0, 1, 0, ... 0].
.. 
.. Pass this to a ``cr.data.PredictionTable`` object to compute:
.. 
..   RMSE = sqrt(MSE)
..   MSE = 1/V * 1/sum(w[1:N]) * sum_i sum_k w_i * (t_ik - o_ik)^2
.. 
.. For regression and binary classification, V=1, and for multi-class, V=2.
.. 
