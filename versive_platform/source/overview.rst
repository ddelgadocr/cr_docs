.. 
.. versive, primary, platform
.. 


==================================================
About the |versive| Platform
==================================================

.. include:: ../../includes_platform/platform_overview.rst

Platform Compute Cluster
==================================================
.. include:: ../../includes_platform/platform_compute_cluster.rst

Because the |versive| platform can deploy predictive models quickly, their development cost is greatly reduced. The speed of the |versive| platform enables machine learning to be a viable tool for competing in rapidly changing markets. Additionally, the |versive| platform can integrate data from a variety of disparate source types, which allows all data types to be leveraged while using the same workflow.

Platform API Modules
==================================================
.. include:: ../../includes_platform/platform_api_modules.rst
