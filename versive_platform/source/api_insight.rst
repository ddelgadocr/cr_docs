.. 
.. xxxxx
.. 

==================================================
Insight API
==================================================

The Insight API may be used to gain insights from models and data.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Insight API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.insight as insight

and then for each use of a function in this API within the customization package add ``security.`` as the first part of the function name. For example:

.. code-block:: python

   insight.create_input_report()

or:

.. code-block:: python

   insight.InputReportType()

The following functions may be used to build customized tables, models, and transaction behaviors:

* :ref:`api-insight-create-input-report`
* :ref:`api-insight-create-partial-dependence-plot`
* :ref:`api-insight-inputreporttype`
* :ref:`api-insight-inputvariablereport-best-blueprint`
* :ref:`api-insight-inputvariablereport-best-score`
* :ref:`api-insight-inputvariablesensitivityreport`
* :ref:`api-insight-cvstyle`
* :ref:`api-insight-cross-validate`
* :ref:`api-insight-crossvalidationreport`
* :ref:`api-insight-test-for-autocorrelation`
* :ref:`api-insight-test-for-heteroskedasticity`
* :ref:`api-insight-test-for-normality`


.. _api-insight-create-input-report:

create_input_report()
==================================================
Generate a report describing how model accuracy varies for different sets of inputs. Use to identify information leakage and understand which inputs are the most important for making decisions. Interactions are not generated. Input data must be dense tabular data.

.. note:: This method is not compatible with exploration, so an error will be raised if the explorations instruction is set.


Usage
--------------------------------------------------

.. code-block:: python

   cr.insight.create_input_report(plan,
                                  model_name=None,
                                  style=InputReportType.ONE_OUT,
                                  compare_vs_all=True,
                                  metric=cr.scoring.SCORE_RMSE,
                                  score=None,
                                  steps=None,
                                  groups=None,
                                  parallel_execution=False,
                                  approx_scoring=False)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**plan** (cr.ml.Blueprint, required)
   The base Blueprint for building the models. The variant models used to generate the input report will follow these instructions, with only the inputs changing. If the plan does not include test data, part of the training data will be automatically used for test data.

**model_name** (str, optional)
   The model in the plan that was used when calling cr.ml.Blueprint.add_model(). If there is only one model in the plan, it will be used by default.


**style** (InputReportType string, optional)
   The report style used for analysis. Available values are:

   ``cr.insight.InputReportType.ONE_IN``: Learn a series of models, each of which uses a single input. This report is especially useful in identifying information leakage.

   ``cr.insight.InputReportType.ONE_OUT``: Learn a series of models, each of which removes a single input from the full model. This report can help identify inputs that have very little influence on the accuracy of predictions. Default value.

   ``cr.insight.InputReportType.FORWARD``: A generalized version of the one-in report. Build a series of models that starts with a single input and, with each iteration, adds the input with the greatest influence on model accuracy. This is sometimes referred to as forward stepwise regression.

   ``cr.insight.InputReportType.BACKWARD``: A generalized version of the one-out report. Build a series of models that starts with the full model and, with each iteration, removes the input with the least influence on model accuracy. This is sometimes referred to as backward stepwise regression.

**compare_vs_all** (bool, optional)
   If True, include the accuracy score of a model learned using all inputs for comparison. Default is True.

**metric** (special, optional)
   The scoring metric used to measure model quality. Can be the name of built-in scoring metric or an instance of cr.scoring.Metric. The built-in metrics are described in cr.scoring.

**steps** (int, optional)
   The maximum number of steps for backward or forward style reports. If you do not set a value, a full report that iterates through every input will be generated. This value is ignored for one-in and one-out reports.

   If a value is set in groups, each step will add or remove a group instead of a single input.

   This argument specifies the maximum number of steps. Fewer steps may run in some cases; for example, when overlapping input groups are set.

**groups** (dict of string->tuple key-value pairs, optional)
   A dictionary mapping group names to tuples of inputs. The inputs within a tuple will be grouped for the input report instead of treated individually. The tuples need not be disjoint or cover all inputs. Default is None, to treat each input individually. See the example below.

   If there are inputs specified in the Blueprint but not specified in any group, the backward and one-out reports will still start by including all inputs before removing a group.

   If there are inputs specified in multiple groups, it’s possible that a forward or backward report will never add a particular group. This can occur when all inputs within a group have already been added or removed in previous steps.

**parallel_execution** (bool, optional)
   If True, distribute generation of the input report across the cluster. In particular, each model is built on one node of the cluster using a subset of the data. This dramatically speeds up processing, but results in more approximate results. For time-series data, this may produce misleading results. Default is False.

**approx_scoring** (bool, optional)
   If True, use a fast approximate scoring method. Only valid if used with parallel_execution. Default is False.

Returns
--------------------------------------------------
A InputVariableReport.

Example
--------------------------------------------------
The following example will generate a report that consists of four models: one built using all inputs, one on x1 and x3, another on x2, and the last on x4.

.. code-block:: python

   >>> report = cr.insight.create_input_report(plan=bp,
   ...                             model_name='my_model',
   ...                             style=cr.insight.InputReportType.ONE_IN,
   ...                             groups = {'name1':("x1","x3"),
   ...                                       'name2':("x2",),
   ...                                       'name3':("x4",)})





.. _api-insight-create-partial-dependence-plot:

create_partial_dependence_plot()
==================================================
Calculate the partial dependence of a model’s prediction on a given input that can be used to understand the relationship between input data and prediction as well as diagnose model performance issues. This function computes a model’s average prediction as a specified input varies across the support of the data for each example.

For best results, data should be a representative sample of your data. For example, it might make sense to use your test set, train set, or entire dataset, depending on where you want to evaluate your model’s performance.

For details, see 10.13.2 in Hastie, Tibshirani, and Friedman (2009).

Usage
--------------------------------------------------

.. code-block:: python

   cr.insight.create_partial_dependence_plot(model,
                                             data,
                                             columns,
                                             return_ci=False,
                                             ci_size=0.95,
                                             max_num_points=10,
                                             sample=False,
                                             v_x=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:


**model** (cr.ml.Model, required)
   The model to be analyzed. This model should have been learned, for example, via cr.ml.learn_model().

**data** (cr.data.DataTable, required)
   The data over which partial dependence will be calculated.

**columns** (str, list, required)
   The name of the column to vary. All other columns are averaged over a range of values for the given column. Numeric column types will be treated as such, string column types will be treated as categorical.

**max_num_points** (int, optional)
   The maximum number of given column slices over which to evaluate partial dependence. If the number of distinct representations in the model of the given column is less than max_num_points the function returns all possible values.

**return_ci** (bool, optional)
   If True, return the 95 percent credible interval for each partial dependence. Default is False.

**ci_size** (float, optional)
   Size of the credible interval. Default is 0.95. Must be in the range (0, 1].

**sample** (bool, optional)
   If True, sample the given data set while maintaining a 1 percent margin of error in estimation with high probability. Default is False.

**v_x** (list, optional)
   Explicit list of unique x-values to be used in partial dependence plot. Default is deciles for numeric data and most frequent values for categorical.

Returns
--------------------------------------------------
A of tuple of lists for the input column and the corresponding average predictions or None if all the weights on the columns are zero.

Example
--------------------------------------------------

.. code-block:: python

   >>> x, y, ci = create_partial_dependence_plot(model, data, 'weight', return_ci=True)
   >>> lower_b, upper_b = zip(*ci)
   >>> import matplotlib.pyplot as plt
   >>> plt.errorbar(x, y, yerr=[lower_b, upper_b])
   >>> plt.show()






.. _api-insight-inputreporttype:

InputReportType
==================================================
The results from exploring models built using different sets of inputs. The type of report produced by create_input_report(). Typical usage:

.. code-block:: python

   >>> report = create_input_report(plan, style=cr.insight.InputReportType.ONE_OUT)

Usage
--------------------------------------------------

Class: ``cr.insight.InputReportType``
Bases: object

Properties
--------------------------------------------------
The following properties are available to this function:

**metric_name** (str)
   Name of the scoring metric used to measure model accuracy.

**results** (list)
   List of (variation-name, cr.ml.TrainingDigest, score) tuples. The list is sorted with the best score first.

**style** (str)
   InputReportType. The four types currently supported are: ``ONE_IN``, ``ONE_OUT``, ``FORWARD``, and ``BACKWARD``.

   BACKWARD(object='') = 'backward'

   FORWARD(object='') = 'forward'

   ONE_IN(object='') = 'one-in'

   ONE_OUT(object='') = 'one-out'

Example
--------------------------------------------------

.. code-block:: python

   >>> report = cr.insight.create_input_report(plan, test_data)
   >>> print report


.. _api-insight-inputvariablereport-best-blueprint:

best_blueprint()
--------------------------------------------------
The completed cr.ml.Blueprint for the best scoring model variant.

.. _api-insight-inputvariablereport-best-score:

best_score()
--------------------------------------------------
The score of the best model variant.




.. _api-insight-inputvariablesensitivityreport:

InputVariableSensitivityReport
==================================================
Summary of a model’s sensitivity to each input.

Usage
--------------------------------------------------

* Class: ``class cr.insight.InputVariableSensitivityReport``
* Bases: object

Arguments
--------------------------------------------------
The following arguments are available to this function:

**results** (list)
   List of tuples with inputs and corresponding importance.

**scores** (dict)
   Dictionary with keys for each column and values corresponding to model score without the given column.


.. _api-insight-cvstyle:

CVStyle
==================================================
Definition of the cross-validation method and parameters for use in cross_validate().

Usage
--------------------------------------------------

* Class: ``cr.insight.CVStyle``
* Bases: object

Properties
--------------------------------------------------
The following properties are available to this function:

**cv_style** (str)
   Description of the cross-validation method to use. Available values are:

   ``k-fold``: Randomly divide data into num_folds non-overlapping subsets of nearly equal length. In each fold, retain a single subset as a test set and use the remaining num_folds - 1 subsets for training and validation. Repeat modeling num_folds times, using each subset for testing exactly once.

   ``rolling``: Sort data according to the column set in order_column and divide into num_folds + 1 non-overlapping subsets of nearly equal length. Subsets are indexed (0, 1, ..., num_folds). In the i-th cross-validation fold, retain the i + 1 subset as a test set and use the remaining (0, ..., i) subsets for training and validation.

   ``shifting``: Sort data according to the column set in order_column and divide into num_folds + window_size non-overlapping subsets of nearly equal length. Subsets are indexed (0, 1, ..., num_folds + window_size). In the i-th cross-validation fold, retain the i + window_size subset as a test set and use the remaining (i, ..., i + window_size) subsets for training and validation.

**num_folds** (int)
   Number of cross-validation repetitions to perform. A single repetition is one combination of data partitions that includes a training set and holdout data to use as a test set. The values set in num_folds, cv_style, and window_size determine the number of partitions in the given data. Default is 5.

**order_column** (str)
   The column that contains the information by which the training data will be ordered.

**window_size** (int)
   Number of data partitions to use for the training set when cv_style="shifting".

Example
--------------------------------------------------

.. code-block:: python

   >>> cv = cr.insight.CVStyle(cv_style='k-fold', num_folds=10)
   >>> cv_results = cr.insight.cross_validate(bp, cv)


.. _api-insight-cross-validate:

cross_validate()
==================================================
Run cross-validation over given model building instructions.

Usage
--------------------------------------------------

.. code-block:: python

   cr.insight.cross_validate(bp,
                             cv_plan=None,
                             metric='RMSE',
                             seed=12345)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**bp** (cr.ml.Blueprint, required)
   Instructions for training a model.

**cv_plan** (CVStyle, optional)
   Instructions for how to carry out cross-validation. Default is unordered, five-fold cross-validation.

**metric** (str, optional)
   The scoring metric used to measure model quality. The built-in metrics are described in cr.scoring.

**seed** (int, optional)
   Random seed for splitting the dataset.

Returns
--------------------------------------------------
A ``cv_result`` dictionary of cross-validation statistics and parameters.


.. _api-insight-crossvalidationreport:

CrossValidationReport
==================================================
Summary of model performance and parameters across the folds of a cross-validation run.

Usage
--------------------------------------------------

* Class: ``cr.insight.CrossValidationReport``
* Bases: object

Arguments
--------------------------------------------------
The following arguments are available to this function:

**mean** (float)
   Arithmetic mean of given metric across cross-validation folds.

**stddev** float
   Standard deviation with n degrees of freedom of given metric across cross-validation folds.

**raw_scores** (list)
   Scoring metric result from each cross-validation fold.

**summary** (str)
   A summary of cross-validation performance.

**metric** (str)
   One or more scoring metrics used during cross-validation.

Example
--------------------------------------------------

.. code-block:: python

   >>> cv_results = cross_validate(bp, cv)
   >>> print cv_results





.. _api-insight-test-for-autocorrelation:

test_for_autocorrelation()
==================================================
Check for autocorrelation in residuals from data ordered by a given column (usually time).

Usage
--------------------------------------------------

.. code-block:: python

   cr.insight.test_for_autocorrelation(model,
                                       data,
                                       order_by_column,
                                       test='breusch-godfrey')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**model** (Model, required)
   Original model. Required to generate residuals.

**data** (DataTable, required)
   Data that will be tested for heteroskedasticity.

**order_by_column** (str, required)
   Name of the column that contains ordered values to sort the given data.

**test** (str, optional)
   String abbreviation for the desired statistical test. The default (and only allowed value) is ``breusch-godfrey``.


Returns
--------------------------------------------------
None.

Example
--------------------------------------------------
None.



.. _api-insight-test-for-heteroskedasticity:

test_for_heteroskedasticity()
==================================================
Test for heteroskedasticity (lack of constant variance) for a given model and data.

Usage
--------------------------------------------------

.. code-block:: python

   cr.insight.test_for_heteroskedasticity(model,
                                          data,
                                          test='levene')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**model** (Model, required)
   Original model. Required to generate residuals.

**data** (DataTable, required)
   Data that will be tested for heteroskedasticity.

**test** (str, optional)
   String abbreviation for the desired statistical test. Allowed values: ``levene`` and ``breusch-pagan``. Default value: ``levene``.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------
None.


.. _api-insight-test-for-normality:

test_for_normality()
==================================================
Test for normality in regression model residuals.


Usage
--------------------------------------------------

.. code-block:: python

   cr.insight.test_for_normality(model,
                                 table,
                                 test='omnibus')

Arguments
--------------------------------------------------
The following arguments are available to this function:

**model** (Model, required)
   Original model. Required to generate residuals.

**table** (DataTable, required)
   Datatable that will be tested for normality.

**test** (str, optional)
   String abbreviation for the desired statistical test. Allowed values: ``omnibus``. Default value: ``omnibus``. Use ``omnibus`` to test both skewness and kurtosis of residual distribution against null hypothesis of normal.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------
None.


