.. 
.. xxxxx
.. 

==================================================
Data Validation V1 API
==================================================

The Data Validation V1 API may be used to work with the data validation library.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Data Validation V1 API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.datavalidation_v1 as datavalidation_v1

and then for each use of a function in this API within the customization package add ``security.`` as the first part of the function name. For example:

.. code-block:: python

   cr.datavalidation_v1.add_columns()

or:

.. code-block:: python

   cr.datavalidation_v1.classification_summary()

The following functions may be used to build customized tables, models, and transaction behaviors:

* :ref:`api-datavalidation-v1-compute-statistics`
* :ref:`api-datavalidation-v1-create-custom-validation`
* :ref:`api-datavalidation_v1-create-missing-validation`
* :ref:`api-datavalidation_v1-create-regex-validation`
* :ref:`api-datavalidation_v1-create-type-validation`
* :ref:`api-datavalidation-v1-get-available-statistics`
* :ref:`api-datavalidation-v1-get-statistic`
* :ref:`api-datavalidation-v1-get-statistic-by-group`
* :ref:`api-datavalidation-v1-validate-table`


.. _api-datavalidation-v1-compute-statistics:

compute_statistics()
==================================================
Compute statistics for columns in a DataTable.

.. note:: Statistics that are already present in the metadata are not recomputed.

.. note:: Use :ref:`api-datavalidation-v1-get-statistic` or :ref:`api-datavalidation-v1-get-statistic-by-group` to access statistics information.

Usage
--------------------------------------------------

.. code-block:: python

   compute_statistics(table,
                      statistics_ops,
                      group_by_columns=None,
                      max_groups=100)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table to compute statistics for.

**statistics_ops** (list of ops, required)
   A list of the statistics operations to apply. Each operation can be one of the following:

   * A column name. All default statistics are calculated for that column.
   * A tuple of column name and statistic name.
   * A tuple of column name, statistic name, and statistic parameters.

   To calculate statistics for all columns, pass in "*". The following statistics are available:

   **count**: The total number of values in the column or group, not including missing values.

   **empty**: The number of missing values in the column or group.

   **sum**: The sum of the values in the column or group.

   **min**: The minimum value in the column or group.

   **max**: The maximum value in the column or group.

   **stddev**: The standard deviation with n-1 degrees of freedom of the values in the column or group.

   **mean**: The arithmetic mean of the values in the column or group.

   **median**: The 50th percentile value in the column or group. This may be an approximation if the number of values in the group is large.

   **histogram**: A histogram that represents the frequencies of values in the column or group. Provide an integer as a parameter to indicate how many bins to compute.

   **quantiles**: The quantiles of values for the group. This may be an approximation if the number of values in the group is large. The quantiles are in the form of a list of tuples where the first element of the tuple is the quantile, and the second is the value at that quantile. Provide an integer as a parameter to indicate how many quantiles to generate, e.g., 4 to produce quartiles.

   **top_x**: The most common values in the column or group. Provide an integer as a parameter to indicate how many values to compute.

**group_by_columns** (str or list of str, optional)
   A single column or list of columns that are used to group rows to compute statistics by group. By default rows are not grouped.

**max_groups** (int, optional)
   The maximum number of groups to make. If this number is lower than the number of distinct elements in the group_by_columns, an error is raised instead of computing statistics.

Returns
--------------------------------------------------
None. The statistics are added to the metadata of the table.

Example
--------------------------------------------------

.. code-block:: python

   >>> datavalidation.compute_statistics(table=mytable,
   ...                                   statistics_ops=['hostname',
   ...                                                   ('username', 'count'),
   ...                                                   ('bytes_in', 'max')])




.. _api-datavalidation_v1-create-count-validation:

create_count_validation()
==================================================
Create a validation based on the number of rows in the table. By default this checks that the table is not empty.

Usage
--------------------------------------------------

.. code-block:: python

   create_count_validation(count=1,
                           check_maximum=False)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**count** (int, optional)
   The number of rows to check for, either minimum or maximum.

**check_maximum** (bool, optional)
   True if count specifies the maximum number of rows; False if is the minimum. Default is False.

Returns
--------------------------------------------------
A DataValidation object suitable for passing to validate_table().

Example
--------------------------------------------------

.. code-block:: python

   >>> count = datavalidation.create_count_validation(count=5)                                                   max_count=3)


.. _api-datavalidation-v1-create-custom-validation:

create_custom_validation()
==================================================
Create a validation that a column does not have too many values that don’t meet custom criteria defined by a UDF.

.. note:: Do not set both ``max_count`` and ``max_percent`` to a non-None value. If neither is set, by default all rows must contain the correct type for the validation to succeed.

Usage
--------------------------------------------------

.. code-block:: python

   create_custom_validation(column,
                            udf,
                            metadata_key,
                            max_count=None,
                            max_percent=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str, required)
   The column to validate.

**udf** (function, required)
   The function to apply to each entry. It should return True for a valid entry and False for an invalid entry. If the function raises an exception, the entry is considered invalid, but this does not stop data validation.

   .. note:: If there are None values in the data they are passed to the function.

**metadata_key** (str, required)
   Name to use to store metadata associated with the validation.

**max_count** (int, optional)
   A non-negative number that specifies the maximum number of rows that can contain invalid entries.

**max_percent** (float, optional)
   A number between 0.0 and 100.0 that specifies the maxiumum percentage of rows that can contain invalid entries.

Returns
--------------------------------------------------
A DataValidation object suitable for passing to validate_table().

Example
--------------------------------------------------

.. code-block:: python

   >>> custom = datavalidation.create_custom_validation(column='timestamp',
   ...                                                  udf=udf,
   ...                                                  metadata_key='test',
   ...                                                  max_percent=1)



.. _api-datavalidation_v1-create-missing-validation:

create_missing_validation()
==================================================
Create a validation to check that a column does not have too many None values.

.. note:: Do not set both max_count and max_percent to a non-None value. If neither is set, by default all rows must contain values for the validation to succeed.

Usage
--------------------------------------------------

.. code-block:: python

   create_missing_validation(column,
                             max_count=None,
                             max_percent=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str, optional)
   The column to validate.

**max_count** (int, optional)
   A non-negative number that specifies the maximum number of rows that can contain missing values.

**max_percent** (float, optional)
   A number between 0.0 and 100.0 that specifies the maximum percentage of rows that can contain missing values.

Returns
--------------------------------------------------
A DataValidation object suitable for passing to validate_table().

Example
--------------------------------------------------

.. code-block:: python

   >>> missing = datavalidation.create_missing_validation(column='hostname',
   ...                                                    max_count=3)



.. _api-datavalidation_v1-create-regex-validation:

create_regex_validation()
==================================================
Create a validation to check that a column does not have too many values that cannot be parsed by a regular expression.

.. note:: Do not set both max_count and max_percent to a non-None value. If neither is set, by default all rows must contain the correct type for the validation to succeed.

Usage
--------------------------------------------------

.. code-block:: python

   create_regex_validation(column,
                           regex,
                           nones_are_valid=True,
                           max_count=None,
                           max_percent=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str, required)
   The column to validate.

**regex** (str, required)
   The regular expression to check values against.

**nones_are_valid** (bool, optional)
   True if None values are considered valid; False if they are not. Default is True.

**max_count** (int, optional)
   A non-negative number that specifies the maximum number of rows that can contain values that fail regex parsing.

**max_percent** (float, optional)
   A number between 0.0 and 100.0 that specifies the maxiumum percentage of rows that can contain values that fail regex parsing.

Returns
--------------------------------------------------
A DataValidation object suitable for passing to validate_table().

Example
--------------------------------------------------

.. code-block:: python

   >>> regex = datavalidation.create_regex_validation(column='username'
   ...                                                regex='[a-z]',
   ...                                                nones_are_valid=False)




.. _api-datavalidation_v1-create-type-validation:

create_type_validation()
==================================================
Create a validation to check that a column does not have too many values of the wrong data type.

.. note:: Do not set both max_count and max_percent to a non-None value. If neither is set, by default all rows must contain the correct type for the validation to succeed.

Usage
--------------------------------------------------

.. code-block:: python

   create_type_validation(column,
                          types,
                          nones_are_valid=True,
                          max_count=None,
                          max_percent=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**column** (str, required)
   The column to validate.

**types** (type or list of types, required)
   A single data type or list of types expected.

**nones_are_valid** (bool, optional)
   True if None values are considered valid; False if they are not. Default is True.

**max_count** (int, optional)
   A non-negative number that specifies the maximum number of rows that can contain the wrong type.

**max_percent** (float, optional)
   A number between 0.0 and 100.0 that specifies the maxiumum percentage of rows that can contain the wrong type.

Returns
--------------------------------------------------
A DataValidation object suitable for passing to validate_table().

Example
--------------------------------------------------

.. code-block:: python

   >>> type = datavalidation.create_type_validation(column='username',
   ...                                              types=str)


.. _api-datavalidation-v1-get-available-statistics:

get_available_statistics()
==================================================
Get a list of available statistics for a column in a DataTable.

.. note:: Use :ref:`api-datavalidation-v1-compute-statistics` to compute the statistics.

Usage
--------------------------------------------------

.. code-block:: python

   get_available_statistics(table,
                            column,
                            group_by_columns=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table to check for statistics.

**column** (str, required)
   The column to check for statistics.

**group_by_columns** (str or list of str, optional)
   A single column or list of columns that was used to group rows to compute statistics by group.

Returns
--------------------------------------------------
A list of available statistics that can be computed.

Example
--------------------------------------------------
None.



.. _api-datavalidation-v1-get-statistic:

get_statistic()
==================================================
Get a previously computed statistic for a column in a DataTable.

.. note:: This function does not do computation, but only retrieves the results from a previous call to :ref:`api-datavalidation-v1-compute-statistics`.

.. note:: To retrieve a statistic that was computed with ``group_by_columns`` set, use :ref:`api-datavalidation-v1-get-statistic-by-group` instead.

Usage
--------------------------------------------------

.. code-block:: python

   get_statistic(table,
                 column,
                 statistic_name,
                 statistic_params=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table to get statistics from.

**column** (str, required)
   The column to get statistics from.

**statistic_name** (str, required)
   Name of the statistic to retrieve.

**statistic_params** (object, optional)
   Parameters used to compute the statistic.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   >>> stat = datavalidation.get_statistic(table=mytable,
   ...                                     column='hostname',
   ...                                     statistic_name='count')



.. _api-datavalidation-v1-get-statistic-by-group:

get_statistic_by_group()
==================================================
Get a previously computed statistic for a column in a DataTable when the computation included ``group_by_columns``.

.. note:: This function does not do computation, but only retrieves the results from a previous call to :ref:`api-datavalidation-v1-compute-statistics`.

.. note:: Use :ref:`api-datavalidation-v1-get-statistic` to retrieve a statistic that was computed without a ``group_by_columns``.

Usage
--------------------------------------------------

.. code-block:: python

   get_statistic_by_group(table,
                          column,
                          statistic_name,
                          group_by_columns,
                          statistic_params=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table to get statistics from.

**column** (str, required)
   The column to get statistics from.

**statistic_name** (str, required)
   Name of the statistic to retrieve.

**group_by_columns** (str or list of str, required)
   A single column or list of columns that was used to group rows to compute statistics by group.

**statistic_params** (object, optional)
   Parameters used to compute the statistic.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   >>> group_stat = datavalidation.get_statistic_by_group(
   ...   table=mytable,
   ...   column='username',
   ...   statistic_name='count'
   ...   group_by_columns='department')






.. _api-datavalidation-v1-validate-table:

validate_table()
==================================================
Validate a DataTable using the provided validation objects.

Usage
--------------------------------------------------

.. code-block:: python

   validate_table(table,
                  validations)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (cr.datatable.DataTable, required)
   The table to validate.

**validations** (list of DataValidation, required)
   A list of the previously-created data validations to apply.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------

.. code-block:: python

   >>> datavalidation.validate_table(table=mytable,
   ...                               validations=[missing,
                                                  regex,
                                                  custom])

