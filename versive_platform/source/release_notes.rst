.. 
.. versive platform
.. 

=====================================================
Release Notes: Versive Platform
=====================================================

Release notes for the |versive| platform.

What's New
=====================================================
The following items are new and/or are changes from previous versions. The short version:

* **Mean absolute log-quotient (MALQ)** measures the average order-of-magnitude difference between predicted and actual values.

System Requirements
-----------------------------------------------------
There are no changes to the system requirements for this release.

.. 
.. CentOS
.. Spark
.. etc.
.. 
.. but also for each feature, like Netflow, Proxy data, etc.
.. 
.. for example: do the standard columns needed for proxy data exist?
.. what area of the environment does each proxy source cover?
.. etc.
.. 


New Metric: MALQ
-----------------------------------------------------
.. include:: ../../includes_terms/term_metric_malq.rst

