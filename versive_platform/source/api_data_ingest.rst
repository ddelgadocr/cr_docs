.. 
.. xxxxx
.. 

==================================================
Data Ingest API
==================================================

The Data Ingest API may be used to work with the Data Ingest Library.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Data Ingest API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.data_ingest as data_ingest

and then for each use of a function in this API within the customization package add ``security.`` as the first part of the function name. For example:

.. code-block:: python

   data_ingest.load_text()

or:

.. code-block:: python

   data_ingest.load_text()


The following functions may be used to build customized tables, models, and transaction behaviors:

* :ref:`api-data-ingest-load-text`
* :ref:`api-data-ingest-get-table-errors`



.. _api-data-ingest-load-text:

load_text()
==================================================
The :ref:`api-data-ingest-load-text` function loads a semi-structured text file, such as a log file, into the |versive| platform for data processing and modeling.

.. note:: When loading data for the first time, it’s recommended that you validate and subsample the table. Validation helps you quickly identify problems with the data and subsampling speeds up model development by requiring less memory to perform many operations. If use_single_processor=False when you initialize the cluster, you cannot load or save tables or models using a "local" connection because it is not a distributed store. You can, however, save a CSV or Excel file to local storage.

.. note:: To load a simple character-delimited file, use cr.risp_v1.load_tabular(). To load a table previously saved in RISP, use cr.risp_v1.load_crtable(). To see skipped rows or rows that failed to parse, use cr.data_ingest_v1.get_table_errors(). To create parsing hints, use cr.risp_v1.create_parser_hint_date() for date and time, cr.risp_v1.create_parser_hint_missing() for missing values, or cr.risp_v1.create_parser_hint_maxerr() for maximum number of errors.

YAML Patterns File
--------------------------------------------------
.. TODO: Sync this up with the primary docs for YAML patterns files.

The patterns file is a YAML file that contains Python regular expressions (regexes) for each pattern to search for in the semi-structured text file. The patterns file must meet the following criteria:

* Each regex must use a named capture group, formatted as (?P<name>), that matches the name of the field it applies to. Regexes should not use backreferences such as /1.
* The name and a type for each field must be included in a "fields" section. These will be the name and type of the column in the returned DataTable.
* The regexes can be organized in one of two ways. You must follow one style of organization or the other; you cannot mix styles in the same file.
* List each regex individually under the field it applies to in the "fields" section. This style is easier to read and makes additions from Splunk simpler.
* List all regex patterns together in a separate "file" section. This style runs faster and allows you to add regex patterns for lines to skip entirely, such as skipping comment lines.

The two examples below illustrate the two different styles of organization you can use in the patterns file.

.. code-block:: yaml

   fields:
     - name: epoch
       pattern: (?i) IN (?P<epoch>[^ ]+)
       type: str
     - name: source
       pattern: (?i) IN (?P<source>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})
       type: str

or:

.. code-block:: yaml

   file:
     pattern: (?P<epoch>[^ ]+),(?P<source>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})
     skip_patterns:
       - ^#.*
   fields:
     - name: epoch
       type: str
     - name: source
       type: str


Usage
--------------------------------------------------

.. code-block:: python

   cr.data_ingest_v1.load_text(path,
                               patterns_file,
                               connection=None,
                               validate=True,
                               subsample=False,
                               columns=None,
                               hints=None,
                               path_filter=None,
                               generate_cast_errors=False,
                               cache=False)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**path** (str, required)
   The path to the file or directory that contains the data. If this points to a directory, all files in that directory will be loaded and merged into a single table.

**patterns_file** (str, required)
   The path to the YAML file that contains the regular expressions used to search for patterns in the text file and parse it into a DataTable that you can work with in the Versive platform. The format of this file is described in more detail below.

**connection** (str, optional)
   The named connection to the storage cluster. If not specified, will use the default connection set with cr.risp_v1.initialize_cluster().

**validatebool** (, optional)
   If True, validate the table against its schema at load time; if False, postpone schema validation until the table is materialized for subsequent operations.

**subsample** (bool, optional)
   If True, load only a portion of the table; if False, load the entire table. Data is subsampled randomly, which could lead to misleading results in time-series data.

**columns** (str or list of str, optional)
   A single column name or list of column names to load. If you do not set a value, all columns are loaded.

**hints** (hint or list of hints, optional)
   A single hint or list of column hints that aid in parsing the data.

**path_filter** function
   A user-defined function (UDF) that takes the path to a list of files and returns the filtered list of files to load.

**generate_cast_errors** (bool, optional)
   If True, includes error in casting columns to the specified types in with the other rows returned by get_table_errors().

**cache** (bool, optional)
   Cache data that will make retrieving results faster if you plan to access both the successful table and the error table


Returns
--------------------------------------------------
A cr.datatable.DataTable that you can work with in the Versive platform.

Example
--------------------------------------------------

.. code-block:: yaml

   $ cat parse_patterns.yaml
   file:
       pattern: (?P<my_str>[^,]),(?P<my_int>.*)
       skip_patterns:
           - ^#.*
   fields:
       - name: my_str
         type: str
       - name: my_int
         type: int

.. code-block:: python

   >>> import cr.data_ingest_v1 as ingest
   >>> import cr.risp_v1 as risp
   >>> risp.initialize_cluster(default_connection='hdfs')
   >>> table = ingest.load_text(path='logs/my_logs.txt',
   ...                          patterns_file='regex/parse_patterns.yaml',
   ...                          validate=True, subsample=False)



.. _api-data-ingest-get-table-errors:

get_table_errors()
==================================================
The :ref:`api-data-ingest-get-table-errors` function returns the lines in a semi-structured text file that regular expressions were failed to parse correctly or were skipped when loading data with the :ref:`api-data-ingest-load-text` function.

.. note:: The output of this function can help troubleshoot problems with regular expressions used in the YAML patterns file required by the :ref:`api-data-ingest-load-text` function.

Usage
--------------------------------------------------

.. code-block:: python

   cr.data_ingest_v1.get_table_errors(table,
                                      statuses=[],
                                      reasons=[])

Arguments
--------------------------------------------------
The following arguments are available to this function:

**table** (DataTable, required)
   The name of a table that was created by the :ref:`api-data-ingest-load-text` function.

**statuses** (str or list of str, optional)
   The parse status for lines to be included in the returned table. One (or more) of ``success``, ``skip``, and ``fail``. Use ``success`` to include lines that were successfully parsed. Use ``skip`` to include lines that were skipped when loading data. Use ``fail`` to include lines that failed to parse correctly. Default value: ``[skip, fail]``.

**reasons** (str or list of str, optional)
   For lines that failed to parse correctly or that were skipped, the regular expressions to include in the returned table. By default, all regular expressions are included.

Returns
--------------------------------------------------
A cr.datatable.DataTable that contains columns for the status, reason, and line.

Example
--------------------------------------------------

.. code-block:: python

   >>> import cr.data_ingest_v1 as ingest
   >>> import cr.risp_v1 as risp
   >>> table = ingest.load_text(path='logs/my_log.txt',
                                patterns_file='regex/patterns.yaml',
                                validate=True, subsample=True)
   >>> error_table = ingest.get_table_errors(table)
   >>> risp.get_example_rows(error_table)

returns a table similar to:

.. code-block:: none

    status   reason            line
   -------- ----------------- ------------------
    fail     (?P<my_int>/d+)   this is a string
    skip      ^#               #comment
   -------- ----------------- ------------------

