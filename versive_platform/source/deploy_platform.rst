.. 
.. This is no longer a straight copy from the versive_master directory. This needs to be updated for the installer, eventually.
.. 

.. 
.. versive, primary, security engine
.. 

==================================================
|versive| Platform
==================================================

.. include:: ../../includes_platform/platform_overview.rst

Platform Compute Cluster
==================================================

.. include:: ../../includes_platform/platform_compute_cluster.rst

.. image:: ../../images/arch_data_sources.svg
   :width: 600 px
   :align: center

This topic describes the steps necessary to prepare, install, and then configure the |versive| platform.

.. note:: A command-line interface to the compute cluster is available from the :doc:`cr group of commands </cmd_cr>`.


.. _deploy-platform-requirements:

Requirements
==================================================
The following sections detail the hardware (both per-node and per-cluster), software, and network requirements for the |versive| platform.

.. note:: The requirements for the engine that runs on top of the |versive| platform are generally the same as the requirements for the platform itself, especially with regard to cluster sizing, performance, and the size of data to be analyzed. However, in some cases the requirements for the engine will increase the requirements for the platform.

Hardware
--------------------------------------------------
Physical hardware for the compute cluster is preferred, but virtual machines (VMs) are fine for the initial configuration. Use one of the following hardware setups for each node in the compute cluster:

* 64-bit Intel x86-compatible computer
* 64-bit virtual machine running under any hypervisor (such as Windows HyperV or VMWare) that can run the supported operating systems

Per-cluster
++++++++++++++++++++++++++++++++++++++++++++++++++
Across the entire compute cluster, the total resource requirements depend on the total size of the data. 

* Actions that look for patterns over long periods of time and/or actions that retrain a model very quickly (in seconds or milliseconds instead of minutes) often require additional computing resources.
* Actions that look for patterns over short periods of time or retraining a model slowly (in minutes instead of seconds or milliseconds) require less computing resources.

For large data sets (around 200 GB):

* An initial cluster of 100-200 compute cores
* 8-core CPU, 8-16 GB RAM per compute core
* HDFS 50-100 TB, depending on the sie of log data; adjust this number downward depending on the desired data rention policy

Per-node
++++++++++++++++++++++++++++++++++++++++++++++++++
Each node in the compute cluster, whether running as physical computers or as virtual machines, must have the following minimum resources per node in the compute cluster:

* A 4-core CPU
* 8 GB RAM per core or enough RAM to process a single day of data, whichever is greater
* 100 GB of free hard disk space or twice the disk space required for data ingestion and at least enough disk space for intermediate checkpoints (relative to the size of the raw data), whichever is greater

The |versive| platform is a scale-out, in-memory solution. The automation used for model building requires a large number of CPU cycles. Therefore, more cores is preferred and additional RAM is always better, even if the CPU core count isn't increased.

Software
--------------------------------------------------
The |versive| platform requires one of the following operating systems to be running on all nodes in the compute cluster:

* Red Hat Enterprise Linux (RHEL) 6.5 (or higher)
* CentOS 6.5 (or higher)

The following applications and libraries are required, but are typically included in the operating system distribution:

* Apache Spark 1.6.1, 1.6.2, or 1.6.3
* Hadoop 2.6 (or higher)
* YARN scheduler; set up YARN on all compute nodes in the cluster with `the Fair Scheduler <https://hadoop.apache.org/docs/r2.4.1/hadoop-yarn/hadoop-yarn-site/FairScheduler.html>`__.

  On the primary node in the cluster---the resource manager---the following services must be started: ``hadoop-yarn-resourcemanager`` and ``hadoop-yarn-proxyserver``.

  On all replica nodes in the cluster---the node managers---the following services must be started: ``hadoop-yarn-nodemanager``.

* JVM version 1.7.0_67
* GNU C Library (GLIBC) 2.11 (or higher)
* Python 2.7 with UCS-4 (4-byte) Unicode libraries

All other third-party packages required by the compute cluster are included with the installer for the |versive| platform.

.. note:: The window manager `GNU Screen <http://www.gnu.org/software/screen/>`_ is recommended, though not required.

Network
--------------------------------------------------
Network communication for the |versive| platform requires:

* 1.0 GB per second network connection (or better) between nodes in the compute cluster.
* 1.0 GB per second network connection (or better) for communication with the storage cluster from each node; 10.0 GB per second network connection is recommended.
* A static IP address for all nodes in the compute cluster.
* Nodes in the compute cluster can communicate over all required ports.

.. note:: Network connections will time out after 60 seconds. Use the ``ping`` endpoint to keep connections active during low-traffic times.

TCP/IP ports used for communication across the cluster will differ based on the compute infrastructure chosen when initializing the cluster. All nodes must be able to communicate with each other over the following ports:

.. list-table::
   :widths: 250 250 100
   :header-rows: 1

   * - Component
     - Runs on
     - Port
   * - Execution server predictions (old)
     - Primary, compute
     - 8000
   * - Cluster status portal
     - Primary
     - 9100
   * - Cluster cache checker
     - Primary
     - 9101
   * - Monitor REST entry point
     - Primary, compute
     - 9103
   * - Watch service
     - Primary, compute
     - 9002
   * - Cluster progress service
     - Primary
     - 9104
   * - Execution server administration portal
     - Primary, compute
     - 9105
   * - Execution server, REST predict requests
     - Primary, compute
     - 9106
   * - Execution server, internal RPC
     - Primary, compute
     - 9107

.. note:: For Spark compute, see the following links to Cloudera documentation for:

   * `Networking requirements <https://www.cloudera.com/documentation/enterprise/5-4-x/topics/cm_ig_cm_requirements.html>`__
   * `Ports used by CDH 5 <http://www.cloudera.com/documentation/enterprise/5-2-x/topics/cdh_ig_ports_cdh5.html>`__


Data Sources
--------------------------------------------------
Data types must be provided to the engine as data sources pulled from an :ref:`HDFS <deploy-connect-data-sources-hdfs-keytab-file>` or :ref:`Amazon S3 <deploy-connect-data-sources-amazon-s3>` storage cluster. Multiple data sources per data type is recommended.

.. list-table::
   :widths: 75 375 150
   :header-rows: 1

   * - Data Type
     - Possible Data Sources
     - Required?
   * - DNS
     - 
     - Yes
   * - Flow
     - Raw flow, Gigamon, Tanium, Illumio, VArmour
     - No, but preferred
   * - Proxy
     - BlueCoat, ZScalar, Cato, ScanSafe
     - Yes


.. _deploy-platform-directory-structure:

Directory Structure
==================================================
The directory structure for the |versive| platform is created by default during installation.


.. _deploy-platform-opt-cr:

/opt/cr
--------------------------------------------------
The installation directory is ``/opt/cr``, under which the ``$CR_INSTALL``, ``$CR_REPO``, and ``$CR_REPO`` directories are created during the installation of the |versive| platform.

.. note:: If the |versive| platform is installed to a different location, substitute that path for ``/opt/cr``.


.. _deploy-platform-cr-install:

$CR_INSTALL
--------------------------------------------------
The ``$CR_INSTALL`` directory contains subdirectories, each with an identical structure as the ``$CR_ROOT`` directory. It also contains a directory called ``active/`` that contains the current in-use platform and application versions, plus snapshot directories that begin with ``snapshot-``.

Snapshots
++++++++++++++++++++++++++++++++++++++++++++++++++
A snapshot is a copy of the ``$CR_ROOT`` directory that is created automatically when certain installation operations occur. For example, when the platform is installed or updated. Snapshots allow the version of the platform to be easily rolled back to a previous version or switched between versions, as desired.

The table below describes the installation operation and corresponding name format for which snapshots are created:

.. list-table::
   :widths: 350 250
   :header-rows: 1

   * - Snapshot Type
     - Description
   * - ``snapshot-new_install``
     - Platform is installed.
   * - ``snapshot-<platform_version>``
     - Active platform version has changed manually or via upgrade.
   * - ``snapshot-<app_and_version>``
     - Application installed or upgraded.
   * - ``snapshot-<app_and_version>_removed``
     - Application deactivated.
   * - ``snapshot-rollback-<date_and_time>``
     - Roll back to previous configuration.

Directory Structure
++++++++++++++++++++++++++++++++++++++++++++++++++
The structure of the ``$CR_INSTALL`` directory is:

.. code-block:: none

   active/
   snapshot-<tag>/
     ...

where ``<tag>`` is a value that uniquely identifies a particular configuration.


.. _deploy-platform-cr-repo:

$CR_REPO
--------------------------------------------------
The ``$CR_REPO`` directory contains all immutable elements of the |versive| platform and applications, such as ``.crpkg`` files and the directories with the contents of these files. A ``.crpkg`` file is an xz-compressed tar file that contains some metadata files.

Nothing is written to the ``$CR_REPO`` directory when using the |versive| platform. During an installation or upgrade, files that exist in this directory are not removed. When installing using the automated installer, the ``$CR_REPO`` directory will contain the same files and subdirectories as the ``$CR_INSTALL`` directory.

Directory Structure
++++++++++++++++++++++++++++++++++++++++++++++++++
The structure of the ``$CR_REPO`` directory is:

.. code-block:: none

   cr-<version>/
   cr-<version>.crpkg
   crepe-<application>-<version>/
   crepe-<application>-<version>.crpkg

where ``<version>`` is a version string and ``<application>`` is the short name of the application. For example: ``cse-1.2.3.4.cc.abcd123``.


.. _deploy-platform-cr-root:

$CR_ROOT
--------------------------------------------------
The ``$CR_ROOT`` directory contains symbolic links to paths under ``*$CR_REPO/plat-<version>*`` for the |versive| platform and, under a ``crepes/`` subdirectory, contains symbolic links to paths under the ``$CR_REPO/crepe-<app>-<version>`` directories for applications that run on the |versive| platform, such as the ``connect.json`` file.

.. TODO: connect.json is analogous the cr connect group of commands; investigate and update because the new installer probably deprecates the cr connect group of commands in favor of just modifying connect.json directly.

Prior to |versive| platform version 2.11.1, this was the top-level directory and contained ``bin/``, ``etc/``, ``lib/``, ``libexec/``, ``sbin/``, ``var/``, ``web/``, and ``crepes/`` subdirectories. These directories were replaced with symbolic links, but the behavior remains the same.

Directory Structure
++++++++++++++++++++++++++++++++++++++++++++++++++
The structure of the ``$CR_ROOT`` directory is:

.. code-block:: none

   $CR_REPO/plat-<version>/bin
   $CR_REPO/plat-<version>/etc
   $CR_REPO/plat-<version>/lib
   ...
   crepes/
     $CR_REPO/crepe-<app>-<version>
       ...


.. _deploy-platform-preparation:

Preparation
==================================================
Prior to installing the |versive| platform, gather the following information about the nodes in the compute cluster:

#. Identify the node that will be the primary node, and then make note of its IP address.
#. Make a note of the IP addresses for all nodes that will be compute nodes.
#. Choose a name for the compute cluster.
#. Identify the user account that will install, administer, and build models with the platform. This account must be able to connect to all nodes using SSH without requiring a password, which may require root access.
#. Determine the installation directory, which must end in ``/cr``. The account that owns the installation must have write access to this directory for all nodes in the cluster. The default installation directory is ``/opt/cr``. All of the default directories will be created inside ``/opt/cr``.
#. Determine the data directory, which must end in ``/cr``. This directory will store log files and result files on each of the nodes in the cluster.

   .. note:: Management of the cluster is easier if all nodes have the same data directory, but this is not required.

#. Identify a location on each node that has at least 100 GB of space.
#. Consider using non-root file systems. For example, if the hosts contain a large volume of free space on ``/mnt``, then ``/mnt/cr`` may be a good choice to be the application data directory.
#. Verify that the required TCP ports are available. If they are not, then specify another low TCP port to use as the base service port, after which all services will be assigned to ports based on that number.


.. _deploy-platform-install-the-platform:

Install the Platform
==================================================
The following section describes how to install the |versive| platform on the host machines that will act as nodes in a distributed compute cluster. The installer will install the |versive| platform on all nodes in the cluster, and then configure the compute cluster. There are two ways to install the |versive| platform:

* With a :ref:`configuration file <deploy-platform-config-file>`
* With :ref:`the installer <deploy-platform-installer>`


.. _deploy-platform-installer:

Use Installer
--------------------------------------------------
To install the platform using the installer, do the following:

#. Copy the |versive| platform installer to the primary node. Contact your |versive| representative for details about how to obtain the |versive| platform installer.

#. Connect to the host that will act as the primary node via SSH.

   From a Mac or Linux computer, use the ``ssh`` command in a terminal window.

   From a Windows computer, use `PuTTY <http://www.chiark.greenend.org.uk>`__.

#. Log in to the host with the user account identified while preparing to install the |versive| platform.

#. Copy the |versive| platform installer to the host on which you are installing using ``scp`` at the command line:

   .. code-block:: console

      $ scp source_host:directory/file destination_host:directory/file

   .. note:: For hosts running RHEL or CentOS, use ``cr-<version>.cc.<revision>.run``.

      For hosts running RHEL or CentOS with Anaconda Python, use ``cr-<version>.c7c.<revision>.run``, and then verify that the Anaconda executable is in the system ``PATH`` variable.

#. Install the |versive| platform and configure the cluster.

   Cluster configuration information may be provided to the installer in two ways. The first is to wait for the installer to prompt, and then provide configuration details as they are requested. The second is to create a configuration file that contains all these details, and then provide that configuration file to the installer.

#. At the command line, run one of the following commands:

   For RHEL or CentOS:

   .. code-block:: console

      $ sh ./cr-<version>.cc.<revision>.run

   For RHEL or CentOS with Anaconda Python:

   .. code-block:: console

      $ sh ./cr-<version>.c7c.<revision>.run

   At each of the installer prompts, provide the configuration information gathered earlier.

   If the installation fails, the installer will provide an error message that indicates where the log files can be found to help diagnose the problem.

#. Verify the |versive| platform installed correctly:

   .. code-block:: console

      $ cr version

#. If an account without root access was used to install the |versive| platform, the ``$CR_ROOT`` and ``PATH`` variables must be exported or else a "Command not found" error will be returned when running ``cr`` commands. At the command line run:

   .. code-block:: console

      $ export CR_ROOT=/opt/cr/active

   and then:

   .. code-block:: console

      $ export PATH=$CR_ROOT/bin:$CR_ROOT/sbin:$PATH

#. Verify the cluster configuration:

   .. include:: ../../includes/cmd_cr_cluster_status.rst


.. _deploy-platform-config-file:

Use Configuration File
--------------------------------------------------
To install the platform using a configuration file, do the following:

#. Copy the |versive| platform installer to the primary node. Contact your |versive| representative for details about how to obtain the |versive| platform installer.

#. Connect to the host that will act as the primary node via SSH.

   From a Mac or Linux computer, use the ``ssh`` command in a terminal window.

   From a Windows computer, use `PuTTY <http://www.chiark.greenend.org.uk>`_.

#. Log in to the host with the user account identified while preparing to install the |versive| platform.

#. Copy the |versive| platform installer to the host on which you are installing using ``scp`` at the command line:

   .. code-block:: console

      $ scp source_host:directory/file destination_host:directory/file

   .. note:: For hosts running RHEL or CentOS, use ``cr-<version>.cc.<revision>.run``.

      For hosts running RHEL or CentOS with Anaconda Python, use ``cr-<version>.c7c.<revision>.run``, and then verify that the Anaconda executable is in the system ``PATH`` variable.

#. Install the |versive| platform and configure the cluster.

   Cluster configuration information may be provided to the installer in two ways. The first is to wait for the installer to prompt, and then provide configuration details as they are requested. The second is to create a configuration file that contains all these details, and then provide that configuration file to the installer.

   .. note:: Multiple configuration files may be maintained, one for each version of the |versive| platform, if desired.
	
#. Create a configuration file that specifies the configuration options in the following format:

   .. code-block:: yaml

      user: <account_with_SSH_access>
      master_ip: <primary_ip>
      compute_ips:
        - <compute_ip>
        - <compute_ip>
      service_user: <account>
      cluster_name: <name>
      data_dir: <path>
      install_dir: <path>
      base_service_port: <port>

#. Save the file with the extension ``.yaml``.

#. At the command line, specify the configuration file when installing.

   For RHEL or CentOS:

   .. code-block:: console

      $ sh ./cr-<version>.cc.<revision>.run -- --config_file=<FILE>

   For RHEL or CentOS with Anaconda Python:

   .. code-block:: console

      $ sh ./cr-<version>.c7c.<revision>.run -- --config_file=<FILE>

   .. note:: The second set of dashes is required.

#. Verify the |versive| platform installed correctly:

   .. code-block:: console

      $ cr version

#. If an account without root access was used to install the |versive| platform, the ``$CR_ROOT`` and ``PATH`` variables must be exported or else a "Command not found" error will be returned when running ``cr`` commands. At the command line run:

   .. code-block:: console

      $ export CR_ROOT=/opt/cr/active

   and then:

   .. code-block:: console

      $ export PATH=$CR_ROOT/bin:$CR_ROOT/sbin:$PATH

#. Verify the cluster configuration:

   .. include:: ../../includes/cmd_cr_cluster_status.rst



.. 
.. .. _deploy-platform-installer2:
.. 
.. Use Installer2
.. --------------------------------------------------
.. To install the |versive| platform using the package installer, do the following:
.. 
.. #. Copy the |versive| platform software package installer to the primary node.
.. #. At the command line, unzip the software package:
.. 
..    .. code-block:: console
.. 
..       $ unzip cr-<version>.c7c.<revision>.zip
.. 
.. #. Setup environment variables:
.. 
..    .. code-block:: console
.. 
..       $ CR_ROOT=<path_to_extracted_folder>
.. 
..    and then:
.. 
..    .. code-block:: console
.. 
..       $ CRPYTHON=${CR_ROOT}/bin/crpython
.. 
.. #. Copy the contents of the Spark configuration directory to a local directory:
.. 
..    .. code-block:: console 
.. 
..       $ cp â€“r <SparkConfDir:/etc/spark/conf> $CR_ROOT/versive_spark_conf
.. 
.. #. Edit the ``spark-defaults.conf`` file located in the ``/versive_spark_conf`` directory to add settings required by the |versive| platform:
.. 
..    .. code-block:: spark
.. 
..       spark.executorEnv.CR_ROOT=./cr-<version>.c7c.<revision>.zip/cr
..       spark.executorEnv.CRPYTHON=./cr-<version>.c7c.<revision>.zip/cr/bin/patched_python
..       spark.executorEnv.MPLCONFIGDIR=/tmp
..       spark.yarn.dist.archives=cr-<version>.c7c.<revision>.zip
..       spark.driver.extraClassPath=./cr-<version>.c7c.<revision>.zip/cr/lib/cr-hadoop-backports.jar:./cr-<version>.c7c.<revision>.zip/cr/lib/context.jar
..       spark.executor.extraClassPath=./cr-<version>.c7c.<revision>.zip/cr/lib/cr-hadoop-backports.jar:./cr-<version>.c7c.<revision>.zip/cr/lib/context.jar
.. 
.. #. Edit the ``custom-env.sh`` file located in the ``$CR_ROOT/etc/`` directory to configure the environment for ``crpython``. Add the following:
.. 
..    .. code-block:: console
.. 
..       export HADOOP_CONF_DIR=<Hadoop configs location> # often /etc/hadoop/conf 
..       export SPARK_CONF_DIR=$CR_ROOT/versive_spark_conf
..       PYTHONPATH='' # ensure empty spark executor PYTHONPATH
.. 
.. remove the x prefix when uncommenting!      x_PYSPARK_PYTHON=./cr-<version>.c7c.<revision>.zip/cr/bin/patched_python
.. remove the x prefix when uncommenting!       x_SITE_PACKAGES=$CR_ROOT/lib/python2.7/site-packages
..       PYTHONPATH="$_SITE_PACKAGES:$PYTHONPATH"
.. 
..       #Optional Variables
..       #export SPARK_HOME=<if custom spark location>
..       #export KRB5_CONF=<if nonstandard krb5 conf file>
.. 
.. #. Edit the ``connect.json`` file located in the ``$CR_ROOT/etc/`` directory to configure HDFS keytab. Add the following code block: 
.. 
..    .. code-block:: javascript
.. 
..       {
..         "hdfs_keytab": [
..           "HDFS connection using keytab file",
..           false,
..           "hdfs",
..           {
..             "port": "50070",
..             "kerberos_realm": "<EXAMPLE>.COM",
..             "host": "<hostname>.<example>.com",
..             "kerberos_user": "<user>/thishost.example.com",
..             "kerberos_keytab": "<path/to/key>.keytab"
..           }
..         ],
..       }
.. 
.. #. Run an example Spark job to validate successful installation. Start the |versive| platform in interactive mode:
.. 
..    .. code-block:: console
.. 
..       $ CRPYTHON
.. 
..    and then run the following commands to run the example Spark job:
.. 
..    .. code-block:: python
.. 
..       >>> import cr.risp_v1 as risp
..       >>> risp.initialize_cluster()
..       >>> rows = [(1,2),] * 100
..       >>> columns = ['first','second']
..       >>> table = cr.data.DataTable.create(rows,columns)
..       >>> model = risp.learn_model('first', 'LINEAR_REGRESSION', table, table)
..       >>> risp.summarize_model(model,table) 
.. 
..    .. warning:: WHAT DOES THIS RETURN?
.. 
.. #. Load a file from HDFS to validate file connections. Start the |versive| platform in interactive mode:
.. 
..    .. code-block:: console
.. 
..       $ CRPYTHON
.. 
..    and then run the following commands:
.. 
..    .. code-block:: python
.. 
..       >>> import cr.risp_v1 as risp
..       >>> risp.initialize_cluster()
..       >>> data = risp.load_csv(path='<data_dir>',
..                                schema_file='<schema_file>',
..                                connection='hdfs_keytab')
..       >>> risp.get_example_rows(data)
.. 
..    .. warning:: WHAT DOES THIS RETURN?
.. 


.. _deploy-platform-connect-to-data-sources:

Connect to Data Sources
==================================================
After the |versive| platform is installed and the engine is installed on top of it, :doc:`connect to data source </deploy_connect_data_sources>`. Data may be stored in a Hadoop Distributed File System (HDFS), Amazon S3, or any other file system with a unified view.



.. _deploy-platform-appendix:

Appendix
==================================================
The following sections describe optional configurations and installation methods:

* :ref:`deploy-platform-connect-json`
* :ref:`deploy-platform-ipython`
* :ref:`deploy-platform-ssh`
* :ref:`deploy-platform-command-line`
* :ref:`deploy-platform-rollback`
* :ref:`deploy-platform-upgrade`
* :ref:`deploy-platform-uninstall`



.. _deploy-platform-connect-json:

connect.json
--------------------------------------------------
The ``connect.json`` file specifies the named connections to a storage cluster. For each named connection, the syntax is:

.. code-block:: javascript

   "connection_name": [
     "optional_description",
     false,
     "storage_type",
     {
       "host": "location-of-host"
     }
   ],


A ``connect.json`` file typically contains more than one named connection. For example:

.. code-block:: javascript

   {
     "hdfs_local": [
       "",
       false,
       "hdfs",
       {
         "host": "172.18.75.230"
       }
     ],
     "local": [
       "Storage on local disk.",
       false,
       "local",
       {
         "host": "localhost"
       }
     ],
     "hdfs_shared": [
       "",
       false,
       "hdfs",
       {
         "host": "hdfs-shared.host-name"
       }
     ],
   }


.. _deploy-platform-ipython:

IPython
--------------------------------------------------
`IPython <http://ipython.org/>`_ is an interactive shell for computing that provides a browser-based notebook with support for data visualization, such as inline plots. IPython and supporting modules are included with the |versive| platform beginning with version 2.8.4. Training demos and data using IPython are distributed as a separate .tgz file.

Set up IPython
++++++++++++++++++++++++++++++++++++++++++++++++++
IPython must first be set up and configured:

#. Connect to the primary node via SSH.
#. At the command line, enter the following to launch IPython and create the profile directory:

   .. code-block:: console

      $ ipython notebook --no-browser

   The ``--no-browser`` option prevents IPython from starting the browser on the primary node.

#. Enter ``Ctrl-C`` to exit IPython.
#. By default, IPython captures all logs and displays them in pink. To disable logging, navigate to ``~/.ipython/profile_default/startup`` on the primary node and create a file called ``disable-warnings.py`` that contains the following:

   .. code-block:: none

      import logging
      logging.disable(50)

#. Optional. On the client computer, create an alias for tunneling into the primary node. Open the ``.profile`` file and add the following:

   .. code-block:: console

      alias inotebook="ssh -f -N -L 8888:localhost:8888 \
      <primary_IP_address> && open http://localhost:8888"

   This command creates an alias that uses only "inotebook" to connect to the primary node running IPython, and then open it in the browser on the client computer.

#. Reload the ``.profile`` file:

   .. code-block:: console

      $ source ~/.profile

#. IPython is now set up.

Run IPython
++++++++++++++++++++++++++++++++++++++++++++++++++
To run IPython:

#. On the primary node, start IPython:

   .. code-block:: console

      $ ipython notebook --no-browser

#. From the client computer, tunnel into the primary node. If an alias was created, enter:

   .. code-block:: console

      $ inotebook

   If an alias was not created, enter:

   .. code-block:: console

      $ ssh -f -N -L 8888:localhost:8888 <primary_IP_address> && open http://localhost:8888

   IPython notebook will open in the browser on your computer.

#. When finished, quit IPython by entering ``Ctrl-C`` on the primary node.




.. _deploy-platform-ssh:

SSH
--------------------------------------------------
SSH keys are a way to identify trusted computers, but without involving passwords. This topic describes how to create and distribute SSH keys, and then enable passwordless login to connect to the nodes in the compute cluster.

* The instructions below are for a Mac or Linux computer.
* The examples use "account" for the account name; replace this with the name of the account that will be using the |versive| platform to build predictive models.
* For a Windows computer, use the instructions for `PuTTY <http://www.chiark.greenend.org.uk/%7Esgtatham/putty/>`__.

When an SSH key-pair is generated, a public key and a private key are created that are associated to each other. The private key resides on the machine from which the user who requires passwordless access will work. The public key resides on the primary node in the compute cluster. The two keys are associated with each other and are used to verify the user before granting access.

Generate SSH Key-Pair
++++++++++++++++++++++++++++++++++++++++++++++++++
To generate an SSH key-pair, do the following:

#. At the command line, navigate to the home directory:

   .. code-block:: console

      $ cd ~/

#. Create the SSH key pair:
	
   .. code-block:: console

      $ ssh-keygen -t rsa
      Generating public/private rsa key pair.

   The command will prompt for a file name and a passphrase for the key pairs:

   .. code-block:: console

      Enter file in which to save the key (/Users/account/.ssh/id_rsa): /Users/account/.ssh/id_rsa
      Enter passphrase (empty for no passphrase):
      Enter same passphrase again:
      Your identification has been saved in id_rsa.
      Your public key has been saved in id_rsa.pub.
      The key fingerprint is: 81:e4:03:8f:f9:19:82:49:1e:c0:b7:58:7b:12:99:c5 account@computer.local

   .. note:: If a file name is not specified, the default file ``id_rsa`` is created.

#. Verify that the keys are created. At the command line, navigate to the ``.ssh`` directory:

   .. code-block:: console

      $ cd ~/.ssh

   List all keys:
	
   .. code-block:: console

      $ ls -all 

#. Create the ``.ssh`` directory on the primary node. This will require the IP address of the primary node.

   .. code-block:: none

      $ ssh account@10.250.57.149 mkdir -p .ssh
        The authenticity of host '10.250.57.149' can't be established.
        RSA key fingerprint is d6:53:94:43:b3:cf:d7:e2:b0:0d:50:7b:17:32:29:2a.
        Are you sure you want to continue connecting (yes/no)? yes
        Warning: Permanently added '10.250.57.149' (RSA) to the list of known hosts.

#. Upload the public key (``id_rsa.pub``) to the primary node, and then add it the list of authorized keys:

   .. code-block:: none

      $ cat .ssh/id_rsa.pub | ssh account@10.250.57.149 'cat >> .ssh/authorized_keys' account@10.250.57.149's password:

#. Verify the connection:

   .. code-block:: console

      $ ssh account@10.250.57.149

   .. note:: If the connection does not work, the permissions for the ``.ssh` directory and ``authorized_keys`` file on the primary node may need to be set:

      .. code-block:: console

         $ ssh account@10.250.57.149 "chmod 700 .ssh; chmod 640 .ssh/authorized_keys"


.. _deploy-platform-command-line:

Command-line
--------------------------------------------------
The recommended way to install and configure the |versive| platform compute cluster is to use installer prompts or to pass the installer a configuration file. However, it is also possible to configure nodes in the compute cluster by specifying individual command-line options.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ sh ./cr-<version>.cc.<revision>.run -- --option=OPTION ...

.. note:: The second set of dashes is required.

Options
++++++++++++++++++++++++++++++++++++++++++++++++++
The following options are available:

``--base_service_port=PORT``
   A low TCP port that is used to begin assigning ports from which services listen. If not specified, will use the default ports created by the default installation of the |versive| platform.

``--clean=NODE``
   Uninstall the |versive| platform on the specified node in the cluster.

``--cluster_name=NAME``
   The name of the cluster.

``--data_dir=PATH``
   The absolute path to the location in which data is stored on each node.

``--install_dir=PATH``
   The absolute path to the location in which the |versive| platform is installed or upgraded.

``--ips=IP_ADDRESS IP_ADDRESS ...``
   A whitespace separated list of the IP address for each node in the cluster. The first IP address is the primary node.

``--service_user=USER_NAME``
   The name of the user account that owns all installation files and all running service processes.

``--user=USER_NAME``
   The name of the user account that has passwordless SSH access to all of the nodes in the cluster.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

   $ sh ./cr-<version>.amd64.run -- --user=user --cluster_name=name \
     ips=10.250.57.149 10.250.9.95 --service_user=admin \
     --install_dir=opt/cr --data_dir=data/cr



.. _deploy-platform-rollback:

Rollback Versions
--------------------------------------------------
.. include:: ../../includes/cmd_cr_install_rollback.rst

For example:

.. code-block:: console

   cr install rollback [SNAPSHOT]



.. _deploy-platform-upgrade:

Upgrade the Platform
--------------------------------------------------
.. TODO: This is not the right spot for this section. But this section is short and sweet so for now, here it is.

To upgrade from an older version of the |versive| platform, simply install the new version. The installer will automatically set the new version to be the active version of the |versive| platform. Use the :ref:`cmd-cr-install` group of commands to manage the active version that is running on the |versive| platform.


.. _deploy-platform-uninstall:

Uninstall the Platform
--------------------------------------------------
.. TODO: This is not the right spot for this section. But this section is short and sweet so for now, here it is.

To uninstall the |versive| platform from a cluster, use the ``--clean`` option when running the installer:

.. code-block:: console

   $ sh ./cr-<version>.cc.<revision>.run -- --clean

A configuration file may also be specified:

.. code-block:: console

   $ sh ./cr-<version>.cc.<revision>.run -- --clean --config_file=<FILE>

