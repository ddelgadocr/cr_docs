.. 
.. xxxxx
.. 

==================================================
Finance API
==================================================

The Finance API may be used to build customized tables, models, and transaction behaviors.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Finance API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.finance as finance

and then for each use of a function in this API within the customization package add ``transform.`` as the first part of the function name. For example:

.. code-block:: python

   finance.add_columns()

or:

.. code-block:: python

   finance.add_lag()

The following functions may be used to build customized tables, models, and transaction behaviors:

* :ref:`api-finance-add-lag`
* :ref:`api-finance-add-lag-from`
* :ref:`api-finance-add-lead`
* :ref:`api-finance-add-lead-from`
* :ref:`api-finance-add-returns`
* :ref:`api-finance-add-returns-from`
* :ref:`api-finance-add-inverse-returns`
* :ref:`api-finance-add-inverse-returns-from`
* :ref:`api-finance-add-exp-avg`




.. _api-finance-add-lag:

add_lag()
==================================================
Add columns to a table that contain time-lagged values.

Because time offsets are likely to land on values that are close to, but not exact matches for, the offset endpoint, the arguments fill_mode and fill_buffer are used to define the amount of leeway to use when filling in values to use for the lag calculation.

For example, say you set 30-second offset where there is no value at exactly 30 seconds before the current row, but there are values at 28 and 34 seconds. If fill_buffer is not set, it will output None. If fill_mode='next' and fill_buffer=3, it will output the value at 28 seconds. If fill_mode='previous' and fill_buffer=3, it will output None.

.. note:: Use :ref:`api-finance-add-lag-from` to add time-lagged values from another table.

Usage
--------------------------------------------------

.. code-block:: python

   add_lag(table,
           timestamp_column,
           input_columns,
           offsets,
           unit='rows',
           fill_mode=None,
           fill_buffer=None,
           group_by_columns=None,
           max_window=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**fill_buffer** (str, optional)
   The time buffer, specified in the same unit as the interval, that defines the maximum distance from the specified interval endpoint to use with ``fill_mode``.

**fill_mode** (str, optional)
   When the specified endpoint of the offset does not land on a value, the method used to provide a value to use for calculations. If no value is found, output ``None``. Available values are:

   **next** The value that occurs at a later point in time than the offset endpoint.

   **previous** The value that occurs at a prior point in time than the offset endpoint.

   **closest** The closest value to the offset endpoint, regardless of whether it occurs before or after.

   **interpolated** The linearly interpolated value between the two points of the previous and next value within a symmetrical ``fill_buffer`` around the offset endpoint.

**group_by_columns** (str, optional)
   The single column or list of columns that contain the categorical information by which the table data will be grouped. For example, if the data contains information about multiple tickers you would want to group by ticker.

**input_columns** (str, list of str, required)
   The column or list of columns that contain the data to offset.

**max_window** (str, optional)
   When ``unit='rows'``, the number of seconds in either direction of the current time that defines the time window within which rows must occur to be included in the interval.

**offsets** (list of int, required)
   Relative to the current row, how far to look back in the past. Accepts only positive integers.

**table** (cr.datatable.DataTable, required)
   The table that contains the data to offset.

**timestamp_column** (str, required)
   The column that contains the timestamp data.

**unit** (str, optional)
   The unit the offsets are measured in. Available values are ``seconds``, ``minutes``, ``hours``, ``days``, and ``rows``. Default is ``rows``.

Returns
--------------------------------------------------
A cr.datatable.Datatable that contains new columns with time-lagged values. The new columns are named using the format [input column]_lag_[offset length].


Example
--------------------------------------------------
This example adds a reference to instrument beta (the volatility of the security with respect to the market) from 30 days back. If a value is not available at exactly 30 days from the current row’s date, the function will look for the most recent price before the 30-day mark that is no older than 33 days. If the value is still not available, it will output None. The output will be in a new 'beta_lag_30days' column.

.. code-block:: python

   finance.add_lag(portfolio_table, timestamp_column='trade_date',
                   input_columns='beta', offsets=[30], unit='days',
                   fill_mode='previous', fill_buffer=3,
                   group_by_columns='ticker')

This example adds a reference to historical daily volume metrics. It will add eight columns: four different offsets for both 'share_volume' and 'dollar_volume'. Missing values will output None.

.. code-block:: python

   finance.add_lag(daily_trade_table, timestamp_column='trade_day',
                   input_columns=['share_volume', 'dollar_volume'],
                   offsets=[3, 7, 14, 30], unit='days')


.. _api-finance-add-lag-from:

add_lag_from()
==================================================
Add columns to a table that contain time-lagged values using data from another table.

Because time offsets are likely to land on values that are close to, but not exact matches for, the offset endpoint, the arguments fill_mode and fill_buffer are used to define the amount of leeway to use when filling in values to use for the lag calculation.

For example, say you set 30-second offset where there is no value at exactly 30 seconds before the current row, but there are values at 28 and 34 seconds. If fill_buffer is not set, it will output None. If fill_mode='next' and fill_buffer=3, it will output the value at 28 seconds. If fill_mode='previous' and fill_buffer=3, it will output None.

.. note:: Use :ref:`api-finance-add-lag` to add time-lagged values from the same table.

Usage
--------------------------------------------------

.. code-block:: python

   add_lag_from(table,
                from_table,
                timestamp_columns,
                input_columns,
                offsets,
                unit='rows',
                fill_mode=None,
                fill_buffer=None,
                group_by_columns=None,
                max_window=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**fill_buffer** (str, optional)
   The time buffer, specified in the same unit as the interval, that defines the maximum distance from the specified interval endpoint to use with ``fill_mode``.

**fill_mode** (str, optional)
   When the specified endpoint of the offset does not land on a value, the method used to provide a value to use for calculations. If no value is found, output ``None``. Available values are:

   **next** The value that occurs at a later point in time than the offset endpoint.

   **previous** The value that occurs at a prior point in time than the offset endpoint.

   **closest** The closest value to the offset endpoint, regardless of whether it occurs before or after.

   **interpolated** The linearly interpolated value between the two points of the previous and next value within a symmetrical ``fill_buffer`` around the offset endpoint.

**from_table** (cr.datatable.DataTable, required)
   The table that contains the inputs to offset.

**group_by_columns** (str, optional)
   The single column or list of columns that contain the categorical information by which the table data will be grouped. For example, if the data contains information about multiple tickers you would want to group by ticker.

**input_columns** (str, list of str, required)
   The column or list of columns in the ``from_table`` that contain the data to offset.

**max_window** (str, optional)
   When ``unit='rows'``, the number of seconds in either direction of the current time that defines the time window within which rows must occur to be included in the interval.

**offsets** (list of int, required)
   Relative to the current row, how far to look back in the past. Accepts only positive integers.

**table** (cr.datatable.DataTable, required)
   The table to which the new columns will be added.

**timestamp_columns** (str, tuple of str required)
   The columns in each table that contain timestamp data. Provide a string if the names are the same in each table. Provide a tuple with the column name in the table and ``from_table`` if they differ.

**unit** (str, optional)
   The unit the offsets are measured in. Available values are ``seconds``, ``minutes``, ``hours``, ``days``, and ``rows``. Default is ``rows``.

Returns
--------------------------------------------------
A cr.datatable.DataTable that contains new columns with time-lagged values. The new columns are named using the format ``<input column>_lag_<offset length>``.

Example
--------------------------------------------------

.. code-block:: python

   add_lag_from(trade_table,
                from_table=market_data_table,
                timestamp_columns='trade_date',
                input_columns=['volume', 'close_price'],
                offsets=[1, 3, 5],
                unit='days',
                fill_mode='previous',
                fill_buffer=1)


.. _api-finance-add-lead:

add_lead()
==================================================
Add columns to a table that contain lead-time values.

Because time offsets are likely to land on values that are close to, but not exact matches for, the offset endpoint, the arguments fill_mode and fill_buffer are used to define the amount of leeway to use when filling in values to use for the lag calculation.

For example, say you set 30-second offset where there is no value at exactly 30 seconds before the current row, but there are values at 28 and 34 seconds. If fill_buffer is not set, it will output None. If fill_mode='next' and fill_buffer=3, it will output the value at 28 seconds. If fill_mode='previous' and fill_buffer=3, it will output None.

.. note:: Use :ref:`api-finance-add-lead-from` to add lead-time values from another table.

Usage
--------------------------------------------------

.. code-block:: python

   add_lead(table,
            timestamp_column,
            input_columns,
            offsets,
            unit='rows',
            fill_mode=None,
            fill_buffer=None,
            group_by_columns=None,
            max_window=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**fill_buffer** (str, optional)
   The time buffer, specified in the same unit as the interval, that defines the maximum distance from the specified interval endpoint to use with ``fill_mode``.

**fill_mode** (str, optional)
   When the specified endpoint of the offset does not land on a value, the method used to provide a value to use for calculations. If no value is found, output ``None``. Available values are:

   **next** The value that occurs at a later point in time than the offset endpoint.

   **previous** The value that occurs at a prior point in time than the offset endpoint.

   **closest** The closest value to the offset endpoint, regardless of whether it occurs before or after.

   **interpolated** The linearly interpolated value between the two points of the previous and next value within a symmetrical ``fill_buffer`` around the offset endpoint.

**group_by_columns** (str, optional)
   The single column or list of columns that contain the categorical information by which the table data will be grouped. For example, if the data contains information about multiple tickers you would want to group by ticker.

**input_columns** (str, list of str, required)
   The column or list of columns that contain the data to offset.

**max_window** (str, optional)
   When ``unit='rows'``, the number of seconds in either direction of the current time that defines the time window within which rows must occur to be included in the interval.

**offsets** (list of int, required)
   Relative to the current row, how far to look back in the past. Accepts only positive integers.

**table** (cr.datatable.DataTable, required)
   The table that contains the data to offset.

**timestamp_column** (str, required)
   The column that contains the timestamp data.

**unit** (str, optional)
   The unit the offsets are measured in. Available values are ``seconds``, ``minutes``, ``hours``, ``days``, and ``rows``. Default is ``rows``.

Returns
--------------------------------------------------
A cr.datatable.DataTable that contains new columns with lead-time values. The new columns are named using the format ``<input column>_lead_<offset length>``.

Example
--------------------------------------------------
This example adds a reference to price at one, five, and ten minutes after the current row. The new columns are named price_lead_1minute, price_lead_5minute, and price_lead_10minute.

.. code-block:: python

   table = finance.add_lead(trade_table,
                            timestamp_column='trade_time',
                            input_columns='price',
                            offsets=[1, 5, 10],
                            unit='minutes',
                            fill_mode='closest',
                            fill_buffer=1,
                            group_by_columns='ticker')



.. _api-finance-add-lead-from:

add_lead_from()
==================================================
Add columns to a table that contain time-lead values using data from another table.

Because time offsets are likely to land on values that are close to, but not exact matches for, the offset endpoint, the arguments fill_mode and fill_buffer are used to define the amount of leeway to use when filling in values to use for the lag calculation.

For example, say you set 30-second offset where there is no value at exactly 30 seconds before the current row, but there are values at 28 and 34 seconds. If fill_buffer is not set, it will output None. If fill_mode='next' and fill_buffer=3, it will output the value at 28 seconds. If fill_mode='previous' and fill_buffer=3, it will output None.

.. note:: Use :ref:`api-finance-add-lead` to add lead-time values from the same table.

Usage
--------------------------------------------------

.. code-block:: python

   add_lead_from(table,
                 from_table,
                 timestamp_columns,
                 input_columns,
                 offsets,
                 unit='rows',
                 fill_mode=None,
                 fill_buffer=None,
                 group_by_columns=None,
                 max_window=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**fill_buffer** (str, optional)
   The time buffer, specified in the same unit as the interval, that defines the maximum distance from the specified interval endpoint to use with ``fill_mode``.

**fill_mode** (str, optional)
   When the specified endpoint of the offset does not land on a value, the method used to provide a value to use for calculations. If no value is found, output ``None``. Available values are:

   **next** The value that occurs at a later point in time than the offset endpoint.

   **previous** The value that occurs at a prior point in time than the offset endpoint.

   **closest** The closest value to the offset endpoint, regardless of whether it occurs before or after.

   **interpolated** The linearly interpolated value between the two points of the previous and next value within a symmetrical ``fill_buffer`` around the offset endpoint.

**from_table** (cr.datatable.DataTable, required)
   The table that contains the inputs to offset.

**group_by_columns** (str, optional)
   The single column or list of columns that contain the categorical information by which the table data will be grouped. For example, if the data contains information about multiple tickers you would want to group by ticker.

**input_columns** (str, list of str, required)
   The column or list of columns in the ``from_table`` that contain the data to offset.

**max_window** (str, optional)
   When ``unit='rows'``, the number of seconds in either direction of the current time that defines the time window within which rows must occur to be included in the interval.

**offsets** (list of int, required)
   Relative to the current row, how far to look back in the past. Accepts only positive integers.

**table** (cr.datatable.DataTable, required)
   The table to which the new columns will be added.

**timestamp_column** (str, required)
   The columns in each table that contain timestamp data. Provide a string if the names are the same in each table. Provide a tuple with the column name in the table and ``from_table`` if they differ.

**unit** (str, optional)
   The unit the offsets are measured in. Available values are ``seconds``, ``minutes``, ``hours``, ``days``, and ``rows``. Default is ``rows``.

Returns
--------------------------------------------------
A cr.datatable.DataTable that contains new columns with lead-time values. The new columns are named using the format ``<input column>_lead_<offset length>``.

Example
--------------------------------------------------
This example adds a reference to price at one, five, and ten minutes after the current row using data from a customer table. The new columns are named price_lead_1minute, price_lead_5minute, and price_lead_10minute.

.. code-block:: python

   table = finance.add_lead_from(trade_table,
                                 from_table='cust_table',
                                 timestamp_columns=('trade_time',
                                                    'timestamp'),
                                 input_columns='level',
                                 offsets=[1, 5, 10],
                                 unit='minutes',
                                 fill_mode='previous',
                                 fill_buffer=1,
                                 group_by_columns='ticker')




.. _api-finance-add-returns:

add_returns()
==================================================
Add columns to a table that calculate the rate of return for price values.

The data in value_columns must be a series that contains all information relevant to calculating the rate of return. For example, there should be no additional columns for stock splits, dividends, or spinoffs that need to be passed to the function.

Because time intervals are likely to land on values that are close to, but not exact matches for, the interval endpoint, the arguments fill_mode and fill_buffer are used to define the amount of leeway to use when filling in values to use for the return calculation.

For example, say you set 30-second backward interval where there is no value at exactly 30 seconds before the current row, but there are values at 28 and 34 seconds. If fill_buffer is not set, it will output None. If fill_mode='next' and fill_buffer=3, it will output the value at 28 seconds. If fill_mode='previous' and fill_buffer=3, it will output None.

.. note:: Use :ref:`api-finance-add-returns-from` to add rate of return from another table.

Usage
--------------------------------------------------

.. code-block:: python

   add_returns(table,
               timestamp_column,
               value_columns,
               return_intervals,
               unit='rows',
               return_type='simple',
               fill_mode=None,
               fill_buffer=None,
               group_by_columns=None,
               direction='backward',
               max_window=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**direction** (str, optional)
   The direction in time the interval looks. Available values are ``forward`` and ``backward``. Default is ``backward``.

**fill_buffer** (str, optional)
   The time buffer, specified in the same unit as the interval, that defines the maximum distance from the specified interval endpoint to use with ``fill_mode``.

**fill_mode** (str, optional)
   When the specified endpoint of the offset does not land on a value, the method used to provide a value to use for calculations. If no value is found, output ``None``. Available values are:

   **next** The value that occurs at a later point in time than the offset endpoint.

   **previous** The value that occurs at a prior point in time than the offset endpoint.

   **closest** The closest value to the offset endpoint, regardless of whether it occurs before or after.

   **interpolated** The linearly interpolated value between the two points of the previous and next value within a symmetrical ``fill_buffer`` around the offset endpoint.

**group_by_columns** (str, optional)
   The single column or list of columns that contain the categorical information by which the table data will be grouped. For example, if the data contains information about multiple tickers you would want to group by ticker.

**max_window** (str, optional)
   When ``unit='rows'``, the number of seconds in either direction of the current time that defines the time window within which rows must occur to be included in the interval.

**return_intervals** (int, tuple of int, or list of int, required)
   The time interval or list of intervals over which the returns are calculated. Accepts only positive integers.

**return_type** (str)
   The type of calculation used for the inverse return. Default is 'simple'. Available values are:

   **simple** The simple inverse rate of return calculated by p1*(1 + r2) for backward intervals, where p1 is the value at the earlier row and r2 is the return at the later row. Calculated by p2/(1 + r1) for forward intervals.

   **log** The logarithmic inverse return calculated by p1*e^r2 for backward intervals and p2/e^r1 for forward intervals.

**table** (cr.datatable.DataTable, required)
   The table to which the returns column will be added.

**timestamp_column** (str, required)
   The column that contains the timestamp data.

**unit** (str, optional)
   The unit the offsets are measured in. Available values are ``seconds``, ``minutes``, ``hours``, ``days``, and ``rows``. Default is ``rows``.

**value_columns** (str, tuple of str, or list of str, required)
   A single column or list of columns that contain the price values.

Returns
--------------------------------------------------
A cr.datatable.DataTable with new columns containing calculated returns. The new columns are named using the format ``<value column>_<return interval length>return``.

Example
--------------------------------------------------
The resulting table contains the returns in the new columns 'close_price_1daysreturn' and 'close_price_7daysreturn'.

.. code-block:: python

   table = finance.add_returns(SPX_constituents_table,
                               timestamp_column='trade_date',
                               value_columns='close_price',
                               return_intervals=[1, 7],
                               unit='days',
                               fill_mode='previous',
                               fill_buffer=1,
                               group_by_columns='ticker')



.. _api-finance-add-returns-from:

add_returns_from()
==================================================
Add columns to a table that calculate the rate of return for price values from another table.

The data in value_columns must be a series that contains all information relevant to calculating the rate of return. For example, there should be no additional columns for stock splits, dividends, or spinoffs that need to be passed to the function.

Because time intervals are likely to land on values that are close to, but not exact matches for, the interval endpoint, the arguments fill_mode and fill_buffer are used to define the amount of leeway to use when filling in values to use for the return calculation.

For example, say you set 30-second forward interval where there is no value at exactly 30 seconds after the current row, but there are values at 28 and 34 seconds. If fill_buffer is not set, it will output None. If fill_mode='next' and fill_buffer=3, it will output None. If fill_mode='previous' and fill_buffer=3, it will output the value at 28 seconds.

.. note:: Use :ref:`api-finance-add-returns` to add rate of return from the same table.

Usage
--------------------------------------------------

.. code-block:: python

   add_returns_from(table,
                    from_table,
                    timestamp_columns,
                    value_columns,
                    return_intervals,
                    unit='rows',
                    return_type='simple',
                    fill_mode=None,
                    fill_buffer=None,
                    group_by_columns=None,
                    direction='backward',
                    max_window=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**direction** (str)
   The direction in time the interval looks. Available values are 'forward' and 'backward'. Default is 'backward'.

**fill_buffer** (str, optional)
   The time buffer, specified in the same unit as the interval, that defines the maximum distance from the specified interval endpoint to use with ``fill_mode``.

**fill_mode** (str, optional)
   When the specified endpoint of the offset does not land on a value, the method used to provide a value to use for calculations. If no value is found, output ``None``. Available values are:

   **next** The value that occurs at a later point in time than the offset endpoint.

   **previous** The value that occurs at a prior point in time than the offset endpoint.

   **closest** The closest value to the offset endpoint, regardless of whether it occurs before or after.

   **interpolated** The linearly interpolated value between the two points of the previous and next value within a symmetrical ``fill_buffer`` around the offset endpoint.

**from_table** (cr.datatable.DataTable, required)
   The table that contains the price values used to calculate the returns.

**group_by_columns** (str, optional)
   The single column or list of columns that contain the categorical information by which the table data will be grouped. For example, if the data contains information about multiple tickers you would want to group by ticker.

**max_window** (str, optional)
   When ``unit='rows'``, the number of seconds in either direction of the current time that defines the time window within which rows must occur to be included in the interval.

**return_intervals** (int, tuple of int, or list of int, required)
   The time interval or list of intervals over which the returns are calculated. Accepts only positive integers.

**return_type** (str)
   The type of calculation used for the inverse return. Default is 'simple'. Available values are:

   **simple** The simple inverse rate of return calculated by p1*(1 + r2) for backward intervals, where p1 is the value at the earlier row and r2 is the return at the later row. Calculated by p2/(1 + r1) for forward intervals.

   **log** The logarithmic inverse return calculated by p1*e^r2 for backward intervals and p2/e^r1 for forward intervals.

**table** (cr.datatable.DataTable, required)
   The table to which the returns column will be added.

**timestamp_column** (str, required)
   The columns in each table that contain timestamp data. Provide a string if the names are the same in each table. Provide a tuple with the column name in the table and ``from_table`` if they differ.

**unit** (str, optional)
   The unit the offsets are measured in. Available values are ``seconds``, ``minutes``, ``hours``, ``days``, and ``rows``. Default is ``rows``.

**value_columns** (str, tuple of str, or list of str, required)
   A single column or list of columns that contain the price values.

Returns
--------------------------------------------------
A cr.datatable.DataTable with new columns containing calculated returns. The new columns are named using the format ``<value column>_<return interval length>return``.

Example
--------------------------------------------------
The resulting table contains the returns in the new columns 'close_price_1daysreturn' and 'close_price_7daysreturn'.

.. code-block:: python

   add_returns_from(portfolio_table,
                    from_table=SPX_table,
                    timestamp_column='trade_date',
                    value_columns='close_price',
                    return_intervals=[1, 7],
                    unit='days',
                    fill_mode='previous',
                    fill_buffer=1)



.. _api-finance-add-inverse-returns:

add_inverse_returns()
==================================================
Add a column to a table that converts the rate of return back to price values.

The data in value_columns must be a series that contains all information relevant to calculating the rate of return. For example, there should be no additional columns for stock splits, dividends, or spinoffs that need to be passed to the function.

Because time intervals are likely to land on values that are close to, but not exact matches for, the interval endpoint, the arguments fill_mode and fill_buffer are used to define the amount of leeway to use when filling in values to use for the inverse return calculation.

For example, say you set 30-second backward interval where there is no value at exactly 30 seconds before the current row, but there are values at 28 and 34 seconds. If fill_buffer is not set, it will output None. If fill_mode='next' and fill_buffer=3, it will output the value at 28 seconds. If fill_mode='previous' and fill_buffer=3, it will output None.

.. note:: Use :ref:`api-finance-add-inverse-returns` to add inverse returns using data from another table. :ref:`api-finance-add-returns` or :ref:`api-finance-add-returns-from` to calculate the rate of return.

Usage
--------------------------------------------------

.. code-block:: python

   add_inverse_returns(table,
                       timestamp_column,
                       returns_column,
                       value_column,
                       return_interval,
                       unit='rows',
                       return_type='simple',
                       fill_mode=None,
                       fill_buffer=None,
                       group_by_columns=None,
                       direction='backward',
                       max_window=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**direction** (str, optional)
   The direction in time the interval looks. Available values are 'forward' and 'backward'. Default is 'backward'.

**fill_buffer** (str, optional)
   The time buffer, specified in the same unit as the interval, that defines the maximum distance from the specified interval endpoint to use with ``fill_mode``.

**fill_mode** (str, optional)
   When the specified endpoint of the offset does not land on a value, the method used to provide a value to use for calculations. If no value is found, output ``None``. Available values are:

   **next** The value that occurs at a later point in time than the offset endpoint.

   **previous** The value that occurs at a prior point in time than the offset endpoint.

   **closest** The closest value to the offset endpoint, regardless of whether it occurs before or after.

   **interpolated** The linearly interpolated value between the two points of the previous and next value within a symmetrical ``fill_buffer`` around the offset endpoint.

**group_by_columns** (str, optional)
   The single column or list of columns that contain the categorical information by which the table data will be grouped. For example, if the data contains information about multiple tickers you would want to group by ticker.

**max_window** (str, optional)
   When ``unit='rows'``, the number of seconds in either direction of the current time that defines the time window within which rows must occur to be included in the interval.

**return_interval** (int, required)
   The time interval over which the returns were calculated. Accepts only positive integers.

**return_type** (str)
   The type of calculation used for the inverse return. Default is 'simple'. Available values are:

   **simple** The simple inverse rate of return calculated by p1*(1 + r2) for backward intervals, where p1 is the value at the earlier row and r2 is the return at the later row. Calculated by p2/(1 + r1) for forward intervals.

   **log** The logarithmic inverse return calculated by p1*e^r2 for backward intervals and p2/e^r1 for forward intervals.

**returns_column** (str, required)
   The column that contains the returns to be converted.

**table** (cr.datatable.DataTable, required)
   The table to which the new value column will be added.

**timestamp_column** (str, required)
   The column that contains the timestamp data.

**unit** (str, optional)
   The unit the offsets are measured in. Available values are ``seconds``, ``minutes``, ``hours``, ``days``, and ``rows``. Default is ``rows``.

**value_column** (str, required)
   The column that contains the price values from which the return was calculated.

Returns
--------------------------------------------------
A cr.datatable.DataTable with a new inverse return column. The new columns are named using the format ``<returns column>_invertedback_<return interval length>``.

Example
--------------------------------------------------
The resulting table will contain a new column 'pct_change_invertedback_1days'.

.. code-block:: python

   table = finance.add_inverse_returns(SPX_table,
                                       timestamp_column=trade_date,
                                       returns_column='pct_change',
                                       value_column='close_price',
                                       return_interval=-1,
                                       unit='days')



.. _api-finance-add-inverse-returns-from:

add_inverse_returns_from()
==================================================
Add a column to a table that converts the rate of return back to price values, using data from another table.

The data in value_columns must be a series that contains all information relevant to calculating the rate of return. For example, there should be no additional columns for stock splits, dividends, or spinoffs that need to be passed to the function.

Because time intervals are likely to land on values that are close to, but not exact matches for, the interval endpoint, the arguments fill_mode and fill_buffer are used to define the amount of leeway to use when filling in values to use for the inverse return calculation.

For example, say you set 30-second forward interval where there is no value at exactly 30 seconds after the current row, but there are values at 28 and 34 seconds. If fill_buffer is not set, it will output None. If fill_mode='next' and fill_buffer=3, it will output None. If fill_mode='previous' and fill_buffer=3, it will output the value at 28 seconds.

.. note:: Use :ref:`api-finance-add-inverse-returns` to calculate inverse returns from data in the same table. Use :ref:`api-finance-add-returns` or :ref:`api-finance-add-returns-from` to calculate the rate of return.

Usage
--------------------------------------------------

.. code-block:: python

   add_inverse_returns_from(table,
                            from_table,
                            timestamp_columns,
                            returns_column,
                            value_column,
                            return_interval,
                            unit='rows',
                            return_type='simple',
                            fill_mode=None,
                            fill_buffer=None,
                            group_by_columns=None,
                            direction='backward',
                            max_window=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**direction** (str, optional)
   The direction in time the interval looks. Available values are ``forward`` and ``backward``. Default is ``backward``.

**fill_buffer** (str, optional)
   The time buffer, specified in the same unit as the interval, that defines the maximum distance from the specified interval endpoint to use with ``fill_mode``.

**fill_mode** (str, optional)
   When the specified endpoint of the offset does not land on a value, the method used to provide a value to use for calculations. If no value is found, output ``None``. Available values are:

   **next** The value that occurs at a later point in time than the offset endpoint.

   **previous** The value that occurs at a prior point in time than the offset endpoint.

   **closest** The closest value to the offset endpoint, regardless of whether it occurs before or after.

   **interpolated** The linearly interpolated value between the two points of the previous and next value within a symmetrical ``fill_buffer`` around the offset endpoint.

**from_table** (cr.datatable.DataTable, required)
   The table that contains the value and returns columns used to calculate the inverse returns.

**group_by_columns** (str, optional)
   The single column or list of columns that contain the categorical information by which the table data will be grouped. For example, if the data contains information about multiple tickers you would want to group by ticker.

**max_window** (str, optional)
   When ``unit='rows'``, the number of seconds in either direction of the current time that defines the time window within which rows must occur to be included in the interval.

**return_interval** (int, required)
   The time interval over which the returns were calculated. Accepts only positive integers.

**return_type** (str)
   The type of calculation used for the inverse return. Default is 'simple'. Available values are:

   **simple** The simple inverse rate of return calculated by p1*(1 + r2) for backward intervals, where p1 is the value at the earlier row and r2 is the return at the later row. Calculated by p2/(1 + r1) for forward intervals.

   **log** The logarithmic inverse return calculated by p1*e^r2 for backward intervals and p2/e^r1 for forward intervals.

**returns_columns** (str, required)
   The column in the from_table that contains the returns to be converted.

**table** (cr.datatable.DataTable, required)
   The table to which the new value column will be added.

**timestamp_column** (str, required)
   The columns in each table that contain timestamp data. Provide a string if the names are the same in each table. Provide a tuple with the column name in the table and from_table if they differ.

**unit** (str, optional)
   The unit the offsets are measured in. Available values are ``seconds``, ``minutes``, ``hours``, ``days``, and ``rows``. Default is ``rows``.

**value_columns** (str, required)
   The column in the from_table that contains the price values from which the return was calculated.

Returns
--------------------------------------------------
A cr.datatable.DataTable with a new inverse return column. The new columns are named using the format ``<returns column>_invertedback_<return interval length>``.

Example
--------------------------------------------------

.. code-block:: python

   finance.add_inverse_returns_from(portfolio_table,
                                    from_table=SPX_table,
                                    timestamp_columns=('trade_date',
                                                        'from_trade_date'),
                                    returns_column='pct_change',
                                    value_column='close_price',
                                    return_interval=1, unit='days',
                                    return_type='log')





.. _api-finance-add-exp-avg:

add_exp_avg()
==================================================
Add columns to a table that compute the exponentially-weighted mean in a rolling window. This is also known as an exponentially-weighted moving average.

This function can be used to smooth or introduce a recency bias into the model. In time-series data that is unevenly spaced, it will normalize for irregular time gaps between data points.

Usage
--------------------------------------------------

.. code-block:: python

   add_exp_avg(table,
               timestamp_column,
               input_columns,
               windows,
               half_life,
               unit='rows',
               temporal=False,
               include_current=False,
               group_by_columns=None,
               max_window=None)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**group_by_columns** (str, optional)
   The single column or list of columns that contain the categorical information by which the table data will be grouped. For example, if the data contains information about multiple tickers you would want to group by ticker.

**half_life** (float, required)
   The amount of time, in the units specified in unit, required for the property measured in ``input_columns`` to fall to half its initial value.

**include_current** (bool, optional)
   If ``True``, include the current data point in the exponentially-weighted mean calculation.

**input_columns** (str, list of str, required)
   The column or list of columns that contain the data on which to calculate the exponentially-weighted mean.

**max_window** (str, optional)
   When ``unit='rows'``, the number of seconds in either direction of the current time that defines the time window within which rows must occur to be included in the interval.

**table** (cr.datatable.DataTable, required)
   The table to which the new exponentially-weighted mean columns will be added.

**temporal** (bool, optional)
   If ``True``, take into account the amount of time that has passed when calculating decay. If ``False``, decay each data point without consideration for the amount of time that has passed. Default is ``False``. The formulas used to calculate decay are described more below.

**timestamp_column** (str, required)
   The column that contains the timestamp data.

**unit** (str, optional)
   The unit the offsets are measured in. Available values are ``seconds``, ``minutes``, ``hours``, ``days``, and ``rows``. Default is ``rows``.

**windows** (list of int, required)
  The length, in the units specified in unit, of the windows over which to calculate the exponentially-weighted mean.

Returns
--------------------------------------------------
A cr.datatable.DataTable with new exponentially-weighted mean columns. The new columns are named using the format ``<input column>_exp_avg_<window size>_<unit>``.

Example
--------------------------------------------------

.. code-block:: python

   table = finance.add_exp_avg(table,
                               timestamp_column='SaleDate',
                               input_columns='Price',
                               windows=[1, 3, 5],
                               half_life=2.57,
                               unit='days',
                               temporal=True)
