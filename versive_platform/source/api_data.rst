.. 
.. xxxxx
.. 

==================================================
Data API
==================================================

The Data API may be used to access and manipulate data sets.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Data API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.data as data

and then for each use of a function in this API within the customization package add ``security.`` as the first part of the function name. For example:

.. code-block:: python

   data.add_columns()

or:

.. code-block:: python

   data.classification_summary()


The following functions may be used to build customized tables, models, and transaction behaviors:

* :ref:`api-data-create-broadcast-closure`
* :ref:`api-data-columnmetadata`
* :ref:`create() <api-data-columnmetadata-create>`
* :ref:`api-data-columnmetadata-name`
* :ref:`api-data-columnmetadata-parents`
* :ref:`api-data-columnmetadata-type`
* :ref:`api-data-columnmetadata-uuid`
* :ref:`api-data-datatable`
* :ref:`api-data-datatable-as-abstract`
* :ref:`api-data-datatable-as-non-abstract`
* :ref:`api-data-datatable-column-names`
* :ref:`api-data-datatable-column-types`
* :ref:`create() <api-data-datatable-create>`
* :ref:`api-data-datatable-frequency-distribution`
* :ref:`api-data-datatable-generate-quantiles`
* :ref:`api-data-datatable-get-column-types-if-available`
* :ref:`api-data-datatable-get-delimiter`
* :ref:`api-data-datatable-get-descriptive-statistics`
* :ref:`api-data-datatable-is-abstract`
* :ref:`api-data-datatable-load`
* :ref:`api-data-datatable-num-rows`
* :ref:`api-data-datatable-ordered-split`
* :ref:`api-data-datatable-random-split`
* :ref:`api-data-datatable-rename-columns`
* :ref:`api-data-datatable-row-sample`
* :ref:`save ()<api-data-datatable-save>`
* :ref:`api-data-datatable-save-schema`
* :ref:`api-data-datatable-set-column-types`
* :ref:`api-data-predictiontable`
* :ref:`api-data-predictiontable-get-confusion-matrix`
* :ref:`api-data-predictiontable-group-by`
* :ref:`api-data-predictiontable-is-prediction-table`
* :ref:`api-data-predictiontable-reason-columns`
* :ref:`save() <api-data-predictiontable-save>`
* :ref:`api-data-predictiontable-score`
* :ref:`api-data-predictiontable-score-by-group`
* :ref:`api-data-copy-prediction-info`
* :ref:`api-data-sparsedataset`
* :ref:`create() <api-data-sparsedataset-create>`
* :ref:`api-data-sparsedataset-num-examples`
* :ref:`api-data-sparsedataset-num-examples-unweighted`
* :ref:`api-data-sparsedataset-num-features`
* :ref:`api-data-sparsedataset-num-outputs`
* :ref:`api-data-sparsedataset-num-targets`
* :ref:`api-data-sparsedataset-parse-list`
* :ref:`api-data-sparsedataset-target-info`
* :ref:`api-data-sparsedataset-target-width`
* :ref:`api-data-sparsetargetinfo`
* :ref:`api-data-predictionresult`



.. _api-data-create-broadcast-closure:

create_broadcast_closure()
==================================================
Create a new closure that will call fn with broadcast variables.

.. note:: Broadcast variables will be accessed immediately on calling this object.

.. note:: The object must stay in scope on the master until all use is finished otherwise the broadcast variable will get cleaned up (see cr.cluster.broadcast).

Usage
--------------------------------------------------

.. code-block:: python

   create_broadcast_closure(fn,
                            *broadcast_args)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**fn** (callable, required)
   A function or callable to close over. The function arguments should be in the following form:

   .. code-block:: python

      (broadcast_arg1, ..., broadcast_argN, unbound1, ..., unboundN)

   where unbound parameters are meant to be supplied by callers of this object and ``broadcast_args`` are supplied by the ``broadcast_args`` passed in this method.

**broadcast_args** (list, required)
   The list of variables to broadcast. Broadcast variables are expected to be read-only/immutable.

Returns
--------------------------------------------------
A callable object that closes over object that are meant to be distributed across the cluster. This is similar to the traditional method of shipping data with a closure but on some compute platforms might be more efficient if the variables that need to shipped around are relatively large. For instance, on Spark, variables closed over will be only be shipped once to each node, and use a torrent-like protocol for distribution. They are also cached in their unpickled state.

Example
--------------------------------------------------
Suppose we wanted to this class to create an augmentation type function. We could write the following code:

.. code-block:: python

   >>> lookup_dict = {'foo': ('bar',)}  # return a tuple, because that is
   ...                                  # risp.add_columns expects as an output
   >>> def _augment(reference_values, input):
   ...     return reference_values.get(input, 'Not Present')
   >>> udf = BroadcastClosure(_augment, lookup_dict)
   >>> risp.add_columns(datatable, ['input'], ['augment'], udf)

This code will add a column by doing the dictionary lookup in augment.





.. _api-data-columnmetadata:

ColumnMetadata
==================================================
A class to hold the metadata for a single column. Note that the DataTable object is responsible for associating the metadata with the correct column in the table.

Usage
--------------------------------------------------

* Class: ``cr.data.ColumnMetadata``
* Bases: object

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``ColumnMetadata`` class. For example:

.. code-block:: python

   cm = cr.data.ColumnMetadata()

and then use that variable to call the individual methods:

.. code-block:: python

   cm.create()

Properties
--------------------------------------------------
The following arguments are available to this class:

**name**
   Column name.

**type**
   Column data type.

**parents**
   Column parents.

**uuid**
   Column ID.

**provenance**
   Origin of the column.

Methods
--------------------------------------------------
The ``ColumnMetadata`` class has the following methods:

* :ref:`api-data-columnmetadata-create`
* :ref:`api-data-columnmetadata-name`
* :ref:`api-data-columnmetadata-parents`
* :ref:`api-data-columnmetadata-type`
* :ref:`api-data-columnmetadata-uuid`


.. _api-data-columnmetadata-create:

cm.create()
--------------------------------------------------
Create a new column metadata object with the given name and type.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   create(cls,
          column_name,
          column_type)


Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**column_name** (str, required)
   The name of a column.

**column_type** (type, required)
   The type of a column. The data model for DataTables only supports int, list, str, float and datetime.


Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A column metadata object.


Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> cm = datatable.ColumnMetadata.create(column_name='url',
   ...                                      column_type=str)


.. _api-data-columnmetadata-name:

cm.name()
--------------------------------------------------
The name of the column.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   name


.. _api-data-columnmetadata-parents:

cm.parents()
--------------------------------------------------
The columns that this column was derived from.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   parents


.. _api-data-columnmetadata-type:

cm.type()
--------------------------------------------------
The type of data contained in the column.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   type



.. _api-data-columnmetadata-uuid:

cm.uuid()
--------------------------------------------------
A unique identifier for this particular column.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   uuid





.. _api-data-datatable:

DataTable
==================================================
A data table with named columns.

Usage
--------------------------------------------------

* Class: ``cr.data.DataTable``
* Bases: object

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``DataTable`` class. For example:

.. code-block:: python

   dt = cr.data.DataTable()

and then use that variable to call the individual methods:

.. code-block:: python

   dt.create()

Arguments
--------------------------------------------------
The following arguments are available to this class:

**column_types** (dict)
   Mapping from column names to their type.

**column_names** (list)
   Names of the columns.

**num_rows** (int)
   Number of rows in the table.

**row_sample** (list)
   First ten rows of the table.

**_metadata_provider** (MetadataProviderPtr)
   Metadata provider.

   COLUMN_NAMES_PER_ROW(x=0) = 3

   COLUMN_NAMES_TO_DISPLAY_LIMIT = None

   MAX_DISTINCT_CATEGORY_VALUES(x=0) = 250


Methods
--------------------------------------------------
The ``DataTable`` class has the following methods:

* :ref:`api-data-datatable-as-abstract`
* :ref:`api-data-datatable-as-non-abstract`
* :ref:`api-data-datatable-column-names`
* :ref:`api-data-datatable-column-types`
* :ref:`api-data-datatable-create`
* :ref:`api-data-datatable-frequency-distribution`
* :ref:`api-data-datatable-generate-quantiles`
* :ref:`api-data-datatable-get-column-types-if-available`
* :ref:`api-data-datatable-get-delimiter`
* :ref:`api-data-datatable-get-descriptive-statistics`
* :ref:`api-data-datatable-is-abstract`
* :ref:`api-data-datatable-load`
* :ref:`api-data-datatable-num-rows`
* :ref:`api-data-datatable-ordered-split`
* :ref:`api-data-datatable-random-split`
* :ref:`api-data-datatable-rename-columns`
* :ref:`api-data-datatable-row-sample`
* :ref:`api-data-datatable-save`
* :ref:`api-data-datatable-save-schema`
* :ref:`api-data-datatable-set-column-types`







.. _api-data-datatable-as-abstract:

dt.as_abstract()
--------------------------------------------------
Return an abstract DataTable from a DataTable. If this object is already abstract, then it returns itself. Otherwise, it returns a new object which is an abstract DataTable.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   as_abstract(self)

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Existing or new DataTable.



.. _api-data-datatable-as-non-abstract:

dt.as_non_abstract()
--------------------------------------------------
Return a non-abstract DataTable from an abstract DataTable. If this object is already non-abstract, then it returns itself. Otherwise, it returns a new object which is a DataTable.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   as_non_abstract(self)

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Existing or new DataTable.




.. _api-data-datatable-column-names:

dt.column_names
--------------------------------------------------
Return a list of strings that represents each column in a table.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   column_names

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> datatable.column_names
   ['predicted_a', 'a', 'b', 'c', 'd']



.. _api-data-datatable-column-types:

dt.column_types
--------------------------------------------------
Return a dictionary of names and data types that are mapped to each column.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   column_types

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> datatable.column_types
   {'a': <type 'int'>, 'b': <type 'float'>}




.. _api-data-datatable-create:

dt.create()
--------------------------------------------------
Create a data table with the given rows and column names.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   create(cls,
          rows,
          column_names,
          column_types=None)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**rows** (list, required)
   A list of tuples. All tuples should have length equal to the number of column names.

**column_names** (list, required)
   Names of the columns in the table.

**column_types** (dict, optional)
   Mapping from column names to column types.


Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
The data table.



.. _api-data-datatable-frequency-distribution:

dt.frequency_distribution()
--------------------------------------------------
Count the frequency of each value in named column. The frequency counts are approximate unless max_unique is set to 0.

This function is faster than using get_descriptive_statistics() for computing frequency counts by themselves. If you need to compute multiple statistics, use get_descriptive_statistics().

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   frequency_distribution(self,
                          column,
                          max_unique=1000)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**column** (str, required)
   Name of the column to inspect.

**max_unique** (int, optional)
   Maximum number of unique values to track when counting. Set to 0 to count with no limit on number of unique values. Default is 1000.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Dictionary mapping values to their frequency counts. If the column contains more than max_unique values, the most frequently occurring values are returned.




.. _api-data-datatable-generate-quantiles:

dt.generate_quantiles()
--------------------------------------------------
Return quantiles for specified column of a data table.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   generate_quantiles(self,
                      column,
                      numbins)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**table** (datatable, required)
   The source data table to be analyzed.

**column** (str, required)
   The name of the column to be analyzed.

**numbins** (int, required)
   The number of fenceposts between the bins. Common settings:

   4 (quartiles)

   10 (deciles)

   100 (percentiles)

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A list of the (numbins+1) quantile values. The first entry (0th quantile) will be the minimum value, and the last entry will be the maximum value. For example, for quartiles (numbins = 4), the five values returned will be the min, first quartile, median, third quartile, and max.




.. _api-data-datatable-get-column-types-if-available:

dt.get_column_types_if_available()
--------------------------------------------------
Return a dictionary of column names to column types if such is available, or None if the types are currently not known.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_column_types_if_available(self)


.. _api-data-datatable-get-delimiter:

dt.get_delimiter()
--------------------------------------------------
Retrieve the delimiter for a table.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_delimiter(self)

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> datatable.get_delimiter()
   ','



.. _api-data-datatable-get-descriptive-statistics:

dt.get_descriptive_statistics()
--------------------------------------------------
Collect descriptive statistics for each column in the table.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_descriptive_statistics(self,
                              column_list=None,
                              max_unique=None,
                              num_quantiles=20, desc=None)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**column_list** (list of str, optional)
   If ``column_list`` is None, all column statistics are generated. Otherwise the statistics are generated for the columns in the list.

**max_unique** (int, optional)
   If ``max_unique`` is 0, then there is no limit on the number of strings kept. Otherwise, an error string appears in the stats for a str column if it has more than ``max_unique`` unique strings.

**num_quantiles** (int, optional)
   Default is 20, which means every 5%. Note that the size of the quantiles array returned will be num_quantiles + 1, since it is inclusive at both ends.

**desc** (str, optional)
   String to append to progress tracking and logged strings. The statistics computed depend on the type of column:

   float column -- 'count', 'empty', 'min', 'max', 'mean', 'median', 'quantiles', 'sum', and 'stddev'.

   int column -- 'appx_unique', 'count', 'empty', 'min', 'max', 'mean', 'median', 'quantiles', 'sum', 'stddev', and 'values'

   string column -- 'appx_unique', 'values', 'count', and 'empty'

   datetime column -- 'count', 'empty', 'min', 'max', and 'values'

   Where each statistic is defined as:

   appx_unique -- Approximate number of unique values in column.

   count -- Number of values (excluding missing values).

   empty -- Number of missing values.

   min -- Smallest value.

   max -- Largest value.

   mean -- Average of the column values.

   median -- Median of the column values (may be approximate).

   quantiles -- An array with the approximate values of the column at num_quantiles + 1 evenly spaced quantiles. For example, if num_quantiles were 3, and the values of the column were uniformly distributed between 1 and 2, this would be [1.0, 1.33, 1.66, 2.0].

   sum -- Sum of all the values in the column.

   stddev -- The standard deviation with n-1 degrees of freedom.

   values -- The unique values and their frequency in the column.


Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A dictionary mapping from column names (the keys) to their statistics. The stats for each column are stored in turn in a sub-dictionary. The available stats depend on the type of column (see above).

Examples
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> data = cr.science.DataTable.load(...)
   >>> print data.column_names
   ['customer', 'date', 'amount_sold']
   >>> stats = data.get_descriptive_statistics(column_list=['customer'])
   >>> customer_stats = stats['customer']
   >>> for name, freq in customer_stats['values']:
   ...     print name, freq
   ACME 42
   Elbonia 101
   Initech 37
   Chotchkie's 5
   Dunder Mifflin 12




.. _api-data-datatable-is-abstract:

dt.is_abstract
--------------------------------------------------
Return True if this is an abstract DataTable.

An abstract DataTable acts as a source of potential data, but the underlying data may not be directly inspected or evaluated in any way; only delay-evaluated transforms may be applied to the DataTable to generate a new DataTable. Any attempt to immediately inspect the data inside the abstract DataTable will result in an error. Transforms may be applied to an abstract DataTable in order to generate a new abstract DataTable. The purpose of an abstract DataTable is to allow construction of delay-evaluated transform pipelines with the assurance that the data will not be implicitly evaluated under the covers. See the DataTable.as_non_abstract() and DataTable.as_abstract() functions for conversion to and from an abstract DataTable.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   is_abstract



.. _api-data-datatable-load:

dt.load()
--------------------------------------------------
Load a data table. The table to load is either specified by the path to where it was saved:

.. code-block:: python

   >>> table = DataTable.load(path='/abc/def/my_table')

or by the name of the schema file and data file:

.. code-block:: python

   >>> table = DataTable.load(file_name='/abc/my_data', file_schema='/def/my_schema.txt')

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   load(cls,
        file_name=None,
        file_schema=None,
        path=None,
        storage='local',
        as_abstract=False,
        sample=False,
        strict=False,
        fmt=None,
        delimiter='t',
        skip_first=False,
        generate_schema=False,
        column_filter=None,
        hints=None,
        mem_to_use=None,
        mem_to_use_for_detect=None,
        path_filter=None,
        generate_parse_errors=False,
        **storargs)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**file_name** (str, required)
   Name of the data directory or file to load.

**file_schema** (str, required)
   Name of the schema file for the table.

**path** (str, required)
   Name of the table directory to load.

**fmt** (str, optional)
   The format of the stored data. Allowed values are:

   ``delimited`` -- For the delimited format, the data is stored as text with rows separated by line returns, and field values separated by a delimiter string (see the delimiter argument). If not specified, the system may attempt to auto-detect formats based upon file names and contents.

   When running on Spark, additional allowed values are: ``parquet``, ``json``, or ``orc``.

**delimiter** (str, optional)
   The delimiter separating column values. Default is tab. But None is a flag value that says to automatically detect the delimiter.

**as_abstract** (bool, optional)
   Causes the data to be returned as an abstract cr.data.DataTable This ensures that the data is not actually materialized until it is converted to a non-abstract DataTable. See DataTable.is_abstract(). Default is True.

**sample** (bool, optional)
   Shows number of rows imported and sample of one row. Not allowed if as_abstract is True. Default is False.

**generate_schema** (bool, optional)
   If False, no automatic schema generation and a schema must have been specified by path or schema_file. If True, the types of the columns are automatically generated, and so are the column names. The argument can also be set to a SchemaGenerator object with custom arguments. Setting this to True causes immediate ingest of samples of the data, which has performance implications. Default is False.

**skip_first** (bool, optional)
   If True, skip the first row. Set to True if first row contains column names. Default is False.

**strict** (bool, optional)
   If True, raise an exception on column or type cast errors during load. Default is False.

**storage** (cr.storage.Storage, optional)
   The data store to load from. See cr.storage.create() for more details. Defaults to cr.storage.Local.

**mem_to_use** (int, optional)
   A suggestion on total memory to use when loading the table. If the table size exceeds the memory size an attempt to downsample the data will be made. If mem_to_use is None no limit will be imposed.

**mem_to_use_for_detect** (int, optional)
   A suggestion on the total memory to use for running schema and delimiter detection algorithms. If None, will use the use mem_to_use if defined; otherwise, it will use the entire file.

**path_filter** (function, optional)
   A UDF that takes the path to a list of files as input and returns the filtered list of files to load.

**generate_parse_errors** (bool, optional)
   If True, generate CSV parsing errors as part of the DataTable. Default is False.


Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A DataTable.


Example:
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> table = DataTable.load(
   ...     file_schema='/def/schema.txt',
   ...     file_name='/abc/my_data.csv',
   ...     fmt='delimited',
   ...     delimiter=',')



.. _api-data-datatable-num-rows:

dt.num_rows
--------------------------------------------------
Return the row count of the number of rows in the table.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   num_rows

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> datatable.num_rows
   92



.. _api-data-datatable-ordered-split:

dt.ordered_split()
--------------------------------------------------
Order the rows by given column and then split them into two tables. May not be applied to an abstract DataTable, since it calculates quantiles in order to generate the split function.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   ordered_split(self,
                 order_by_column,
                 ratio=0.8)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**order_by_column** (string)
   The column name of the column to sort by.

**ratio** (float)
   A number between 0 and 1, specifying the percent of rows that should be placed in the first DataTable, the remaining rows will be placed in the second DataTable. Defaults to 0.8.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A pair of DataTable objects.


.. _api-data-datatable-random-split:

dt.random_split()
--------------------------------------------------
Randomly partitions the table rows into two tables.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   random_split(self,
                ratio=0.8,
                seed=12345)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**ratio** (float, required)
   A number between 0 and 1, specifying the percent of rows that should be placed in the first DataTable, the remaining rows will be placed in the second DataTable. Defaults to 0.8.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A pair of DataTable objects.





.. _api-data-datatable-rename-columns:

dt.rename_columns()
--------------------------------------------------
Rename one or more columns in this table.

.. note:: The new column names may exist in the original column names, but each table must have unique column names.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   rename_columns(self,
                  name_changes,
                  description='')

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**name_changes** (tuple or list of tuples, required)
   One or more tuples that contain the column's existing name and its intended new name.

**description** (str, optional)
   A string to include in the log output to clarify the context.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A cr.datatable.DataTable that contains the same data with new column names.



.. _api-data-datatable-row-sample:

dt.row_sample
--------------------------------------------------
Return a list of the first 10 rows from the table.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   row_sample


.. _api-data-datatable-save:

dt.save()
--------------------------------------------------
Save the table. The simplest way to save is to provide the name of the directory:

.. code-block:: python

   >>> my_table.save(path='/abc/def/my_table')

This creates a folder named "my_table" in the /abc/def directory with the contents:

* /abc/def/my_table/schema.txt
* /abc/def/my_table/metadata.txt
* /abc/def/my_table/data/

The schema.txt file contains the names of the columns along with the type of data held in each column (for example, int, float). The data/ subdirectory holds the table contents, divided up into multiple chunks (e.g., 00.gz, 01.gz). The metadata.txt file contains metadata about the columns in the table. Optional arguments can be given to store the table somewhere other than the local file system.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save(self,
        path,
        storage='local',
        overwrite=True,
        **storargs)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**path** (str, required)
   Name of the output table directory.

**overwrite** (bool, optional)
   If True, overwrite if files already exist. Default is True.

**storage** (cr.storage.Storage, optional)
   The data store to save to. See cr.storage.create() for more details. Defaults to cr.storage.Local.




.. _api-data-datatable-save-schema:

dt.save_schema()
--------------------------------------------------
Save the schema only. This is useful when the schema is auto-generated. The ``schema.txt`` file contains the names of the columns along with the type of data held in each column (for example, int, float). Optional arguments can be given to store the table somewhere other than the local file system.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save_schema(self,
               path,
               storage='local',
               overwrite=True,
               **storargs)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**path** (str, required)
   Folder to write schema.txt in. For example, the path is a folder and "schema.txt" is appended to the end.:

   .. code-block:: python

      >>> my_table.save_schema(path='/abc/def/my_table/')

**overwrite** (bool, optional)
   If True, overwrite if files already exist. Default is True.

**storage** (cr.storage.Storage, optional)
   The data store to save to. See cr.storage.create() for more details. Defaults to cr.storage.Local.


.. _api-data-datatable-set-column-types:

dt.set_column_types()
--------------------------------------------------
Set the types of all the columns.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   set_column_types(self,
                    column_types)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**column_types** (dictionary of string,type, required)
   The name and types of all the columns.



.. 
.. CRMR is removed, which means this function is deprecated?
.. 
.. .. _api-data-override-types-requirement:
.. 
.. override_types_requirement()
.. ==================================================
.. Temporarily set the CRMR knob THROW_ON_NO_TYPES to require_types.
.. 
.. Usage
.. --------------------------------------------------
.. 
.. .. code-block:: python
.. 
..    override_types_requirement(require_types)
.. 
.. Arguments
.. --------------------------------------------------
.. The following arguments are available to this function:
.. 
.. **require_types** (bool, required)
..    If True, temporarily set THROW_ON_NO_TYPES to require_types.
.. 


.. _api-data-predictiontable:

PredictionTable
==================================================
Result table from dense model prediction.

Usage
--------------------------------------------------

* Class: ``cr.data.PredictionTable``
* Bases: ``cr.datatable.DataTable``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``PredictionTable`` class. For example:

.. code-block:: python

   pt = cr.data.PredictionTable()

and then use that variable to call the individual methods:

.. code-block:: python

   pt.save()

Arguments
--------------------------------------------------
The following arguments are available to this class:

**reason_columns** (list)
   Names of columns containing reasons explaining the predictions. May be an empty list if there are no reason columns in the table.

**prediction_column** (str)
   Name of column containing the model prediction.

Methods
--------------------------------------------------
The ``PredictionTable`` class has the following methods:

* :ref:`api-data-predictiontable-get-confusion-matrix`
* :ref:`api-data-predictiontable-group-by`
* :ref:`api-data-predictiontable-is-prediction-table`
* :ref:`api-data-predictiontable-reason-columns`
* :ref:`api-data-predictiontable-save`
* :ref:`api-data-predictiontable-score`
* :ref:`api-data-predictiontable-score-by-group`


.. _api-data-predictiontable-get-confusion-matrix:

pt.get_confusion_matrix()
--------------------------------------------------
Calculate a confusion matrix for binary and multi-class prediction tables. The API is beta quality and subject to change.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_confusion_matrix(self,
                        threshold=None)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**threshold** (float, optional)
   Minimum probability to classify a guess as True (class 1). Default is 0.5. For multiclass this is ignored and the class with highest probability is always chosen as predicted class.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
An instance of cr.scoring.ConfusionMatrix.




.. _api-data-predictiontable-group-by:

pt.group_by()
--------------------------------------------------
Name of the column used to create scoring groups.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   group_by(str,
            list,
            tuple)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**min_group_size** (int, optional)
   Return groups which have greater than min_group_size examples. Default is 0.

**score_baseline** (bool, optional)
   Compute a score for the baseline model as well if True and a baseline model is available. Default is True.

**max_groups** (int, optional)
   Maximum number of groups to score per column. If the ``group_by`` column is categorical, the most frequently occurring values are scored while infrequent values are collected into an ``_other_values_ group``. If the group_by column is numeric, groups are defined by evenly spaced quantiles. By default, ``max_groups`` is None and the number of groups equals the number of unique values in the ``group_by`` column.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A list of (group_value, ScoreSummary) pairs.


.. _api-data-predictiontable-is-prediction-table:

pt.is_prediction_table()
--------------------------------------------------
Returns True if this is a valid PredictionTable.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   is_prediction_table(self)


.. _api-data-predictiontable-reason-columns:

pt.reason_columns()
--------------------------------------------------
Return the names of columns containing reasons explaining the predictions. May be an empty list if there are no reason columns in the table.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   reason_columns


.. _api-data-predictiontable-save:

pt.save()
--------------------------------------------------
Save the table. See cr.data.DataTable.save() for details.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   save(self,
        path,
        storage='local',
        overwrite=True,
        **storargs)



.. _api-data-predictiontable-score:

pt.score()
--------------------------------------------------
Score prediction with a given metric, after verifying compatibility with prediction type.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   score(self,
         metric=cr.scoring.SCORE_RMSE,
         score_baseline=True)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**metric** (special, optional)
   The scoring metric to use. Can either be the name of built-in scoring metric or an instance of cr.scoring.Metric. Available scoring metrics:

.. list-table::
   :widths: 80 420
   :header-rows: 1

   * - Metric
     - Description
   * - **ACC**
     - .. include:: ../../includes_terms/term_metric_acc.rst
   * - **AUC**
     - .. include:: ../../includes_terms/term_metric_auc.rst
   * - **F1**
     - .. include:: ../../includes_terms/term_metric_f1.rst
   * - **LOGLOSS**
     - .. include:: ../../includes_terms/term_metric_logloss.rst
   * - **MAE**
     - .. include:: ../../includes_terms/term_metric_mae.rst
   * - **MALQ**
     - .. include:: ../../includes_terms/term_metric_malq.rst
   * - **MAPE**
     - .. include:: ../../includes_terms/term_metric_mape.rst
   * - **MULTILOGLOSS**
     - .. include:: ../../includes_terms/term_metric_multilogloss.rst
   * - **PRECISION**
     - .. include:: ../../includes_terms/term_metric_prec.rst
   * - **PREC@K**
     - .. include:: ../../includes_terms/term_metric_prec_at_k.rst
   * - **QAPE**
     - .. include:: ../../includes_terms/term_metric_qape.rst
   * - **REC**
     - .. include:: ../../includes_terms/term_metric_rec.rst
   * - **REC@K**
     - .. include:: ../../includes_terms/term_metric_rec_at_k.rst
   * - **RMSE**
     - .. include:: ../../includes_terms/term_metric_rmse.rst

**score_baseline** (bool, optional)
   If True, compute a score for the baseline model if a baseline model is available.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A cr.scoring.ScoreSummary.


.. _api-data-predictiontable-score-by-group:

pt.score_by_group()
--------------------------------------------------
Score prediction groups with a given metric, one group score will be returned for each distinct value in the column name provided to the ``group_by`` argument.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   score_by_group(self,
                  metric,
                  group_by,
                  min_group_size=0,
                  score_baseline=True,
                  max_groups=None)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**metric** (special, required)
   The scoring metric to use. Can either be the name of builtin scoring metric or an instance of cr.scoring.Metric. The built-in metrics are described in cr.scoring.



.. _api-data-copy-prediction-info:

copy_prediction_info()
==================================================
Turn a DataTable into a PredictionTable by copying properties from an existing PredictionTable. This is useful after applying a transform to a source PredictionTable, to turn the resulting table back into a PredictionTable.

Optionally, specific properties can be selectively overridden as keyword arguments to this function.

Usage
--------------------------------------------------

.. code-block:: python

   copy_prediction_info(src_preds,
                        new_table,
                        **overrides)

Arguments
--------------------------------------------------
The following arguments are available to this function:

**src_preds** (PredictionTable, required)
   Copy properties from this table.

**new_table** (DataTable, required)
   Cast this table to a PredictionTable.

**overrides** (``*``, optional)
   Key word replacments for any of the parameters to PredictionTable() (excluding rdd).

Returns
--------------------------------------------------
A new PredictionTable with the rows and columns from ``new_table``.

Example
--------------------------------------------------
Suppose we want to rename the names of the reason columns and ensure the result can still be scored:

.. code-block:: python

   >>> src_preds = model.predict(some_data, return_reasons=True)
   >>> old_cols = src_preds.reason_columns
   >>> new_cols = ['R1', 'R2', 'R3']
   >>> new_table = risp.rename_columns(src_preds, zip(old_cols, new_cols))
   >>> new_preds = copy_prediction_info(src_preds,
   ...                                  new_table,
   ...                                  reason_cols=new_cols)
   >>> summary = new_preds.score(metric='RMSE')




.. _api-data-sparsedataset:

SparseDataset
==================================================
Sparse dataset plus some meta data. A collection of sparse feature vectors and associated target labels. A dataset has read-only properties: the number of output columns, the data dimensionality (number of feature columns, including bias term), the number of examples, the unweighted number of examples, and the actual data. The unweighted number of examples is the number of rows regardless of whether the data is weighted or not. In contrast, the (weighted) number of examples will be equal to the sum of the row weights when row weights are used.

Usage
--------------------------------------------------

* Class: ``cr.data.SparseDataset``
* Bases: ``cr.SparseDataset``

Set a Variable
++++++++++++++++++++++++++++++++++++++++++++++++++
Use a variable to call the ``SparseDataset`` class. For example:

.. code-block:: python

   sd = cr.data.SparseDataset()

and then use that variable to call the individual methods:

.. code-block:: python

   sd.create()


Methods
--------------------------------------------------
The ``SparseDataset`` class has the following methods:

* :ref:`api-data-sparsedataset-create`
* :ref:`api-data-sparsedataset-num-examples`
* :ref:`api-data-sparsedataset-num-features`
* :ref:`api-data-sparsedataset-num-outputs`
* :ref:`api-data-sparsedataset-num-targets`
* :ref:`api-data-sparsedataset-parse-list`
* :ref:`api-data-sparsedataset-target-info`
* :ref:`api-data-sparsedataset-target-width`


.. _api-data-sparsedataset-create:

sd.create()
--------------------------------------------------
Return SparseDataset built from the input data matrices. Return the distributed dataset object.

N: Number of examples.

D: Input dimensionality.

O: Number of targets within the dataset.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   create(cls,
          T,
          X,
          rw=None,
          num_batches=1,
          multinom_target=False,
          add_bias=True)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**T** (matrix, required)
   Dense N x O numpy matrix containing the ground truth labels. Values can be numeric for regression problems or binary valued (0 or 1) for classification.

**X** (matrix, required)
   Sparse N x D scipy csr matrix, where each row is a feature vector, and feature indices in the range [1, D].

**rw** (matrix, optional)
   Dense N x O numpy matrix containing row weights.

**num_batches** (int, optional)
   Number of batches of roughly equal size to break the data into. Default is one.

**multinom_target** (bool, optional)
   If True, interpret a multicolumn T as a single multinomial target, rather than a multi-target matrix. Default is False.

**add_bias** (bool, optional)
   If False, do not add the bias column to the dataset. Default is True.



.. _api-data-sparsedataset-num-examples:

sd.num_examples()
--------------------------------------------------
Return the number of examples. If the examples are weighted, return the sum of the row weights. Use num_examples_unweighted for the count of the examples, weighted or not.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   num_examples


Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> dataset.num_examples
   4352




.. _api-data-sparsedataset-num-examples-unweighted:

sd.num_examples_unweighted()
--------------------------------------------------
Return the number of unweighted examples.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   num_examples_unweighted


Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> dataset.num_examples_unweighted
   4352



.. _api-data-sparsedataset-num-features:

sd.num_features()
--------------------------------------------------
Return the number of features in the dataset, including bias term.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   num_features


Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> data.num_features
   1962


Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> dataset.num_features
   100



.. _api-data-sparsedataset-num-outputs:

sd.num_outputs()
--------------------------------------------------
Deprecated. Use :ref:`api-data-sparsedataset-target-width`.


.. _api-data-sparsedataset-num-targets:

sd.num_targets()
--------------------------------------------------
Return the number of targets in the SparseDataset.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   num_targets

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> data.num_targets
   1


.. _api-data-sparsedataset-parse-list:

sd.parse_list()
--------------------------------------------------
Parse strings representing sparse feature vector data and return a SparseDataset.

The expected format for each string is ``target features``, where target is a single token from the set {-1, 1} or {0, 1}, and features is a white space separated list of feature indices and values. Two example strings are:

.. code-block:: none

   0 4:0.2 42:-0.5 110:2.2 1893:0.7
   1 1:1 2:1 3:1 5:1 7:1 11:1 13:1 17:1

In the first example, the ground truth target is 0, feature 4 has value 0.2, feature 42 has value -0.5, and so on. Feature indices start at 1. (Index 0 is reserved for bias weights.)

The parser also supports a more compact encoding for cases when all feature values are 0 or 1. In this case the values can be omitted. Using the compact encoding, the second example can be shortened to:

.. code-block:: none

   1 1 2 3 5 7 11 13 17

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   parse_list(cls,
              data,
              batch_size=None,
              max_feature_index=None)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**data** (list, required)
   List of strings, with each string containing a (target, feature vector) pair.

**batch_size** (int, required)
   Set the maximum number of examples that occur in a batch. Automatically determined if set to None (the default).

**max_feature_index** (int, optional)
   Set to the maximum feature index that occurs in the dataset. Automatically determined if set to None (the default).

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A SparseDataset containing the target and feature data.


.. _api-data-sparsedataset-target-info:

sd.target_info()
--------------------------------------------------
Retrieve information on one of the targets in the SparseDataset.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   target_info(self,
               target_id)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**target_id** (int, required)
   Index of target in range [0..num_targets).

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
A SparseTargetInfo for specified target.




.. _api-data-sparsedataset-target-width:

sd.target_width()
--------------------------------------------------
Retrieve information on one of the targets in the SparseDataset.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   target_width

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Return the number of columns in the target for a single-target dataset. For a multinomial target, this value will be greater than one.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> data.target_width
   1



.. _api-data-sparsetargetinfo:

SparseTargetInfo
--------------------------------------------------
Information about one of the sparse targets within a SparseDataset.

Usage
--------------------------------------------------

* Class: ``cr.data.SparseTargetInfo``
* Bases: object

Arguments
--------------------------------------------------
The following arguments are available to this function:

**target_width** (int)
   The number of output columns.

**num_examples** (float)
   The number of examples, or sum of row weights for the target.

**weighted** (bool)
   True if target is weighted.


.. _api-data-predictionresult:

PredictionResult
==================================================
Result from sparse model prediction.

Usage
--------------------------------------------------

* Class: ``cr.data.PredictionResult``
* Bases: object

Arguments
--------------------------------------------------
The following arguments are available to this function:

**target** (matrix, required)
   True target (N x O); may be None.

**features** (matrix, required)
   Input features (sparse).

**prediction** (matrix, required)
   Output prediction for each row (N x O).

**comments** (list, required)
   Comment for each row (N).

**row_weights** (matrix, required)
   Importance weights for each row (N x 1); may be None.

**nlls** (matrix)
   Negative log-likelihood for each row.

   N: Number of prediction examples.

   D: Dimensionality of the feature space.

   O: Number of target columns.

Returns
--------------------------------------------------
None.

Example
--------------------------------------------------
None.
