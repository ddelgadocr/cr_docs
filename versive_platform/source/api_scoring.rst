.. 
.. xxxxx
.. 

==================================================
Scoring API
==================================================

The Scoring API may be used to score model predictions.

.. note:: Dates and times should follow :ref:`strptime syntax <python-strptime-syntax>`.

Requirements
==================================================
To use the Scoring API, it must first be imported from the platform library. Put the following statement at the top of the customization package:

.. code-block:: python

   import cr.scoring as scoring

and then for each use of a function in this API within the customization package add ``security.`` as the first part of the function name. For example:

.. code-block:: python

   cr.scoring.ConfusionMatrix.get_accuracy()

or:

.. code-block:: python

   cr.scoring.RankingMetric.score_ranking()

The following functions may be used to score models:

Score summaries:

* :ref:`api-scoring-scoresummary`

Confusion Matrix:

* :ref:`api-scoring-confusionmatrix`
* :ref:`api-scoring-confusionmatrix-classification-summary`
* :ref:`api-scoring-confusionmatrix-get-accuracy`
* :ref:`api-scoring-confusionmatrix-get-aggregate-accuracy`
* :ref:`api-scoring-confusionmatrix-get-aggregate-fmeasure`
* :ref:`api-scoring-confusionmatrix-get-aggregate-precision`
* :ref:`api-scoring-confusionmatrix-get-aggregate-recall`
* :ref:`api-scoring-confusionmatrix-get-fmeasure`
* :ref:`api-scoring-confusionmatrix-get-precision`
* :ref:`api-scoring-confusionmatrix-get-recall`
* :ref:`api-scoring-confusionmatrix-num-classes`
* :ref:`api-scoring-confusionmatrix-rows`
* :ref:`api-scoring-confusionmatrix-sample-count`

Itemwise Matrix:

* :ref:`api-scoring-itemwisemetric`
* :ref:`api-scoring-itemwisemetric-finalize-score`
* :ref:`api-scoring-itemwisemetric-merge-partial-scores`
* :ref:`api-scoring-itemwisemetric-score-item`
* :ref:`api-scoring-itemwise-metric`

Metric:

* :ref:`api-scoring-metric-abbrv`
* :ref:`api-scoring-metric-compatible`
* :ref:`api-scoring-metric-lower-is-better`
* :ref:`api-scoring-metric-name`

Ranking Metric:

* :ref:`api-scoring-rankingmetric`
* :ref:`api-scoring-rankingmetric-calculate-item-stats`
* :ref:`api-scoring-rankingmetric-merge-partial-stats`
* :ref:`api-scoring-rankingmetric-score-ranking`
* :ref:`api-scoring-rankingmetric-top-element-count`

Prediction Types:

* :ref:`api-scoring-predictiontype`


.. _api-scoring-metrics:

Scoring Metrics
==================================================
The following built-in metrics are identified by the following strings:

.. list-table::
   :widths: 80 420
   :header-rows: 1

   * - Metric
     - Description
   * - **ACC**
     - .. include:: ../../includes_terms/term_metric_acc.rst
   * - **AUC**
     - .. include:: ../../includes_terms/term_metric_auc.rst
   * - **F1**
     - .. include:: ../../includes_terms/term_metric_f1.rst
   * - **LOGLOSS**
     - .. include:: ../../includes_terms/term_metric_logloss.rst
   * - **MAE**
     - .. include:: ../../includes_terms/term_metric_mae.rst
   * - **MALQ**
     - .. include:: ../../includes_terms/term_metric_malq.rst
   * - **MAPE**
     - .. include:: ../../includes_terms/term_metric_mape.rst
   * - **MULTILOGLOSS**
     - .. include:: ../../includes_terms/term_metric_multilogloss.rst
   * - **PRECISION**
     - .. include:: ../../includes_terms/term_metric_prec.rst
   * - **PREC@K**
     - .. include:: ../../includes_terms/term_metric_prec_at_k.rst
   * - **QAPE**
     - .. include:: ../../includes_terms/term_metric_qape.rst
   * - **REC**
     - .. include:: ../../includes_terms/term_metric_rec.rst
   * - **REC@K**
     - .. include:: ../../includes_terms/term_metric_rec_at_k.rst
   * - **RMSE**
     - .. include:: ../../includes_terms/term_metric_rmse.rst


.. _api-scoring-scoresummary:

ScoreSummary
==================================================
The :ref:`api-scoring-scoresummary` class outputs details about scoring model predictions, including:

* The metric that was used by the model
* A score that measures prediction quality
* The baseline score
* The number of data points that were evaulated

Usage
--------------------------------------------------

* Class: ``cr.scoring.ScoreSummary``
* Bases: object

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**score** (float, required)
   Number summarizing how good or bad the predictions were.

**wt_count** (float, required)
   Count of how many test data points were evaluated during scoring. This count includes the weights associated with the test points (if weights were provided).

**plotvals** ((list), required)
   List of (x,y) pairs, sorted by increasing x, that can be used to plot model quality (the y-axis). Use None for this parameter to indicate no plot values are available.

Properties
++++++++++++++++++++++++++++++++++++++++++++++++++
The following properties are available to this function:

**score** (float, required)
   Number summarizing the prediction quality.

**weighted_count** (float, required)
   Count of how many test data points were evaluated during scoring. This count includes the weights associated with the test points (if weights were provided).

**plotvals** (list, required)
   List of (x,y) pairs, sorted by increasing x, that can be used to plot model quality (the y-axis). If no plot values are available, set to None.

**metric** (str, required)
   The metric used to compute the score, or None if not available.

**baseline** (float, required)
   The score from predicting the mean (also known as the "baseline model") everywhere or None if not available.







.. _api-scoring-confusionmatrix:

ConfusionMatrix
==================================================
Stores confusion matrix for classification model predictions.

.. https://en.wikipedia.org/wiki/Confusion_matrix

.. 
.. "true positive" (TP) for correctly predicted event values
.. "false positive" (FP) for incorrectly predicted event values
.. "true negative" (TN) for correctly predicted no-event values
.. "false negative" (FN) for incorrectly predicted no-event values
.. 
.. for example: accuracy
.. 
.. (TP + TN) / (TP + TN + FP + FN)
.. 


Usage
--------------------------------------------------

* Class: ``cr.scoring.ConfusionMatrix``
* Bases: object

Methods
--------------------------------------------------
The ``ConfusionMatrix`` class has the following methods:

* :ref:`api-scoring-confusionmatrix-classification-summary`
* :ref:`api-scoring-confusionmatrix-get-accuracy`
* :ref:`api-scoring-confusionmatrix-get-aggregate-accuracy`
* :ref:`api-scoring-confusionmatrix-get-aggregate-fmeasure`
* :ref:`api-scoring-confusionmatrix-get-aggregate-precision`
* :ref:`api-scoring-confusionmatrix-get-aggregate-recall`
* :ref:`api-scoring-confusionmatrix-get-fmeasure`
* :ref:`api-scoring-confusionmatrix-get-precision`
* :ref:`api-scoring-confusionmatrix-get-recall`
* :ref:`api-scoring-confusionmatrix-num-classes`
* :ref:`api-scoring-confusionmatrix-sample-count`


.. _api-scoring-confusionmatrix-classification-summary:

classification_summary()
--------------------------------------------------
Create a per-class summary of available metrics.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   classification_summary(self)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
``metric_summary (LocalDataTable)``.

.. _api-scoring-confusionmatrix-get-accuracy:

get_accuracy()
--------------------------------------------------
Calculate per-class accuracy as (TP + TN) / (TP + TN + FP + FN). TP + TN is the same for all classes (sum of confusion matrix diagonal) FP is the sum of the klass column not including the klass row FN is the sum of the klass row not including the klass column.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_accuracy(self,
                klass)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
None.


.. _api-scoring-confusionmatrix-get-aggregate-accuracy:

get_aggregate_accuracy()
--------------------------------------------------
Calculate the overall classification accuracy for the confusion matrix.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_aggregate_accuracy(self)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Accuracy as a float.


.. _api-scoring-confusionmatrix-get-aggregate-fmeasure:

get_aggregate_fmeasure()
--------------------------------------------------
Aggregate F-measure for multi-classification.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_aggregate_fmeasure(self,
                          aggregate_type='micro',
                          beta=1)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**aggregate_type** (str, optional)
   String label of the overall averaging method. Allowed values: ``micro`` or ``macro``.

**beta** (float, optional)
   beta to use for F-measure. Must be positive. Defaults to 1.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Aggregate F-measure as a float.


.. _api-scoring-confusionmatrix-get-aggregate-precision:

get_aggregate_precision()
--------------------------------------------------
Aggregate precision for multi-classification.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_aggregate_precision(self,
                           aggregate_type='micro')

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**aggregate_type** (str, required)
   String label of the overall averaging method. Allowed values: ``micro`` or ``macro``.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Aggregate precision as a float.


.. _api-scoring-confusionmatrix-get-aggregate-recall:

get_aggregate_recall()
--------------------------------------------------
Aggregate recall for multi-classification.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_aggregate_recall(self,
                        aggregate_type='micro')

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**aggregate_type** (str, required)
   String label of the overall averaging method. Allowed values: ``micro`` or ``macro``.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Aggregate recall as a float.


.. _api-scoring-confusionmatrix-get-fmeasure:

get_fmeasure()
--------------------------------------------------
Calculate the F-measure. Returns NaN for classes with 0 support.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_fmeasure(self,
                klass,
                beta=1)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**klass** (int, required)
   Class number.

**beta** (float, required)
   beta to use for F-measure. Must be positive. Default is 1.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
F-measure as a float.


.. _api-scoring-confusionmatrix-get-precision:

get_precision()
--------------------------------------------------
Calculate the proportion of the predictions that are accurate when guess was klass. Returns NaN if a class was never predicted.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_precision(self,
                 klass)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**klass** (int, required)
   Predicted class.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Precision as a float.


.. _api-scoring-confusionmatrix-get-recall:

get_recall()
--------------------------------------------------
Calculate the proportion of samples of klass that were predicted accurately. Returns NaN if provided data set did not include any examples of given class.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   get_recall(self,
              klass)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**klass** (int, required)
   Actual class.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Recall as a float.


.. _api-scoring-confusionmatrix-num-classes:

num_classes
--------------------------------------------------
Return an integer that contains the number of classes in the classification model.

.. _api-scoring-confusionmatrix-rows:

rows
--------------------------------------------------
Return a numpy array where cell i, j represents the count of samples with predictions as class i when the true class is j.

.. _api-scoring-confusionmatrix-sample-count:

sample_count
--------------------------------------------------
Return a float containing the sum of weights of all the samples used in construction of the matrix.



.. _api-scoring-itemwisemetric:

ItemwiseMetric
==================================================
Interface for a score metric that can be computed by looking at one data point at a time.

.. note:: Even though :ref:`api-scoring-itemwisemetric-score-item`, :ref:`api-scoring-itemwisemetric-merge-partial-scores`, and :ref:`api-scoring-itemwisemetric-finalize-score` are instance methods, these functions should not modify object state. In other words, it is okay to read from self, but do not write to it.

Usage
--------------------------------------------------

* Class: ``cr.scoring.ItemwiseMetric``
* Bases: object

Methods
--------------------------------------------------
The ``Metric`` class has the following methods:

* :ref:`api-scoring-itemwisemetric-merge-partial-scores`
* :ref:`api-scoring-itemwisemetric-finalize-score`
* :ref:`api-scoring-itemwisemetric-score-item`

Code uses these methods as follows:

.. code-block:: none

   |   --> SCORING_IN_PROGRESS --> score_item() --
   |       |                   ^                 |
   |       |                   |                 |
   |       |                   -------------------
   |       |
   |       | merge_partial_scores()
   |       |
   |       --> MERGING  -- merge_partial_scores() --
   |           |        ^                          |
   |           |        |                          |
   |           |        ----------------------------
   |           |
   |           | finalize_score()
   |           |
   |   <--  ScoreSummary



.. 


.. _api-scoring-itemwisemetric-finalize-score:

finalize_score()
--------------------------------------------------
Perform any final computations necessary and return the final result.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   finalize_score(self,
                  merged_scores)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

The computed ScoreSummary.

.. code-block:: python

   >>> return ScoreSummary(
     merged_scores[0] / (merged_scores[1] + epsilon),
     merged_scores[1],
     None)


.. _api-scoring-itemwisemetric-merge-partial-scores:

merge_partial_scores()
--------------------------------------------------
Return result from merging partials A and B.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   merge_partial_scores(self,
                        partial_a,
                        partial_b)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
None.


.. _api-scoring-itemwisemetric-score-item:

score_item()
--------------------------------------------------
Return the partial score of the single row item.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   score_item(self,
              row)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**row** (tuple, required)
   row[0] is the true goal value

   row[1] is the predicted value

   row[2] is the weight for the item

Example
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   >>> return (abs(truth - guess)*weight, weight)




.. _api-scoring-itemwise-metric:

Metric
==================================================
Use to get details about metrics.

Usage
--------------------------------------------------

* Class: ``cr.scoring.Metric``
* Bases: object

Methods
--------------------------------------------------
The ``Metric`` class has the following methods:

* :ref:`api-scoring-metric-abbrv`
* :ref:`api-scoring-metric-compatible`
* :ref:`api-scoring-metric-lower-is-better`
* :ref:`api-scoring-metric-name`


.. _api-scoring-metric-abbrv:

abbrv()
--------------------------------------------------
Return the abbreviation of the metric’s name. For example, for root mean squared error, this returns "RMSE".

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   abbrv(self)

.. _api-scoring-metric-compatible:

compatible()
--------------------------------------------------
Check if this metric is compatible with the given modeling output type.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   compatible(self,
              outtype,
              extra_compat_args=None)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**outtype** (PredictionType, required)
   The kind of output produced by the model.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
True if the metric can be computed for outtype; otherwise, returns False.


.. _api-scoring-metric-lower-is-better:

lower_is_better()
--------------------------------------------------
Return True if lower scores are better; otherwise, return False.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   lower_is_better(self)


.. _api-scoring-metric-name:

name()
--------------------------------------------------
Return a short readable description of the metric. For example, the RMSE metric may return "Root mean squared error (RMSE)".

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   name(self)



.. _api-scoring-rankingmetric:

RankingMetric
==================================================
Interface for a scoring metric computed from ordering data points by predicted value.

If a ranking metric needs to derive statistics from the entire set of scores, it should override :ref:`api-scoring-rankingmetric-calculate-item-stats` and :ref:`api-scoring-rankingmetric-merge-partial-stats`. The resulting statistics, if any, will be passed into :ref:`api-scoring-rankingmetric-score-ranking`. For example, the RecallAtKMetric counts the total number of positive class examples using these functions. During the ranking phase, the data points with the top K highest predicted values are passed to the :ref:`api-scoring-rankingmetric-score-ranking` method.

Usage
--------------------------------------------------

* Class: ``cr.scoring.RankingMetric``
* Bases: ``cr.scoring.Metric``

Methods
--------------------------------------------------
The ``RankingMetric`` class has the following methods:

* :ref:`api-scoring-rankingmetric-calculate-item-stats`
* :ref:`api-scoring-rankingmetric-merge-partial-stats`
* :ref:`api-scoring-rankingmetric-score-ranking`
* :ref:`api-scoring-rankingmetric-top-element-count`

Code uses these methods as follows:

.. code-block:: none

   |   --> PRE_RANKING_IN_PROGRESS --> calculate_item_stats()
   |       |                   ^                 |
   |       |                   |                 |
   |       |                   -------------------
   |       |
   |       --> MERGING  -- merge_partial_stats()
   |           |        ^                  |
   |           |        |                  |
   |           |        --------------------
   |   <-- stats
   |
   |   --> RANKING_IN_PROGRESS --> top_element_count()
   |                           |
   |                           K
   |                           |
   |                           --> score_ranking(top K items, stats)
   |                               |
   |   <-------------------------- ScoreSummary


Example
--------------------------------------------------

.. 
.. from somewhere in platform code:
.. 
.. .. code-block:: python
.. 
..         elif isinstance(metric, cr.scoring.RankingMetric):
..             if not metric.top_element_count():
..                 raise NotImplementedError("Only top k RankingMetric supported.")
.. 
..             # For RankingMetric we first group by key. This has scalability issues but this
..             # group by is unavoidable given how the ranking metrics need things like number of
..             # rows, number of positive class rows, etc on the whole set of scores. After the
..             # grouping the scores are trimmed to top k scores.
..             projected = projected.group_by_key()
..             projected = projected.filter(_RowSelectorMinimumCountOfValues(min_group_size))
..             projected = projected.map_value(_ProcessGroupForRankedMetric(metric))
.. 
..             for (grp, agg) in projected.collect():
..                 perf_by_grp[grp] = metric.score_ranking(*agg)
.. 

.. _api-scoring-rankingmetric-calculate-item-stats:

calculate_item_stats()
--------------------------------------------------
Calculate desired partial statistics of the single row item.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   calculate_item_stats(self,
                        row)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**row** (tuple, required)
   row[0] is the true goal value

   row[1] is the predicted value

   row[2] is the weight for the item

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Implementation dependent. The returned values will be merged using the :ref:`api-scoring-rankingmetric-merge-partial-stats` method.


.. _api-scoring-rankingmetric-merge-partial-stats:

merge_partial_stats()
--------------------------------------------------
Merge partial statistics from :ref:`api-scoring-rankingmetric-calculate-item-stats`. The final merged value, after merging all partial statistics, is passed into the :ref:`api-scoring-rankingmetric-score-ranking` method.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   merge_partial_stats(self,
                       left,
                       right)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**left** (object, required)
   Value returned by either :ref:`api-scoring-rankingmetric-calculate-item-stats` or :ref:`api-scoring-rankingmetric-merge-partial-stats`.

**right** (object, required)
   Same as requirements as left argument.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
Implementation dependent. Must be same kind of object as returned by calculate_item_stats().


.. _api-scoring-rankingmetric-score-ranking:

score_ranking()
--------------------------------------------------
Score the provided ranking and return the result.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   score_ranking(self,
                 ranked_scores,
                 optional_stats)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
The following arguments are available to this function:

**ranked_scores** (list, required)
   The top K data points by predicted value. Each list item is a tuple of (truth, guess, weight).

**optional_stats** (object, required)
   The value returned by :ref:`api-scoring-rankingmetric-merge-partial-stats`.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
The computed ScoreSummary.


.. _api-scoring-rankingmetric-top-element-count:

top_element_count()
--------------------------------------------------
Return the maximum number of elements that should be in the ranked list passed to :ref:`api-scoring-rankingmetric-score-ranking`.

Usage
++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: python

   top_element_count(self)

Arguments
++++++++++++++++++++++++++++++++++++++++++++++++++
None.

Returns
++++++++++++++++++++++++++++++++++++++++++++++++++
None.




.. _api-scoring-predictiontype:

PredictionType
==================================================
Enumeration of types of outputs from prediction models.

* ALL_TYPES() = [0, 1, 2, 3]
* BINARY_PROBABILITY(x=0) = 1
* CLASS_LABEL(x=0) = 0
* NUMERIC(x=0) = 3
* POSTERIOR_CLASS_DISTRIBUTION(x=0) = 2

Usage
--------------------------------------------------

* Class: ``cr.scoring.PredictionType``
* Bases: object
