# Readme

This is the readme for cr_docs.

## Sphinx

http://www.sphinx-doc.org/en/1.4.8/install.html

Run it locally with a command similar to:

    $ sphinx-build -b html cr_docs/versive_master/source/ cr_docs/output/cr_master


## PDFs

We build PDFs using Sphinx. This requires the rst2pdf package:

rst2pdf http://rst2pdf.ralsina.me/stories/index.html

Run it locally with a command similar to:

    $ sphinx-build -b pdf cr_docs/versive_pdf_stages/source/ cr_docs/output/cr_master/downloads


## Contributing

Please create a branch.

Make your changes.

Run the Sphinx HTML build locally.

Please fix any errors/warnings with one exception:

    WARNING: search index couldn't be loaded, but not all
    documents will be built: the index will be incomplete.

That error is related to the search functionality, which is currently disabled.

Once the build is otherwise good, commit the branch to the master branch.