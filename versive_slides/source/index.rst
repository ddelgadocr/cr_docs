=====================================================
Versive Documentation Slide Decks
=====================================================

.. revealjs::

 .. revealjs:: Versive Documentation Slide Decks

  .. image:: ../../images/slides_splash.svg

.. Hide the TOC from this file. Just keep these alphabetized please.

.. toctree::
   :hidden:

   blocks
   demo
   stages
   vse
