================================================
Blocks Tutorial
================================================


.. revealjs::

 .. revealjs:: Blocks Tutorial: Ingest Daily Proxy Logs
    :noheading:
    :data-transition: none

    .. image:: ../../images/slides_blocks_splash.svg


 .. revealjs:: About Blocks
    :data-transition: none

    .. include:: ../../includes_terms/term_blocks.rst


 .. revealjs:: Blocks and Columns
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_columns.rst


 .. revealjs:: Block Graphs
    :data-transition: none

    .. include:: ../../includes_terms/term_block_graph.rst

    .. image:: ../../images/slides_blocks_order.svg

    In this tutorial, data is ingested into the engine (1), is parsed into tabular data (2), is cleaned up for IP address data (3) and for user data (4), is transformed (5), and then built into two tables (6).


 .. revealjs:: Block Graph Configuration
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_configuration.rst


 .. revealjs:: Steps in this Tutorial
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_steps.rst


 .. revealjs:: Step One: Verify Data Source
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_steps_00.rst


 .. revealjs:: Step Two: Parse Proxy Data
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_steps_01.rst


 .. revealjs:: Step Three: Split Parsed, Unparsed
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_steps_02.rst


 .. revealjs:: Step Four: Clean Up IP Address Data
    :data-transition: none


    .. include:: ../../includes/blocks_tutorial_steps_03.rst


 .. revealjs:: Step Five: Clean Up User Data
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_steps_04.rst


 .. revealjs:: Step Six: Add Columns
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_steps_05.rst


 .. revealjs:: Step Seven: Group by Source IP
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_steps_06.rst


 .. revealjs:: Step Eight: Sum for Bytes In / Out
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_steps_07.rst


 .. revealjs:: Step Nine: Create Lists
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_steps_08.rst


 .. revealjs:: Step Ten: Verify Dependencies
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_steps_09.rst


 .. revealjs:: Step Eleven: Verify Output
    :data-transition: none

    .. include:: ../../includes/blocks_tutorial_steps_10.rst


 .. revealjs:: More Information
    :data-transition: none

    For more information about this tutorial, blocks, and using blocks to build the engine pipeline, see the following links:

    * `Full YAML configuration example <../start_here.html#full-yaml-example>`_
    * `Complete Blocks reference <../blocks.html>`_


 .. revealjs:: Blocks Tutorial: Parse Data
    :noheading:
    :data-transition: none

    .. image:: ../../images/slides_blocks_parse.svg


 .. revealjs:: Blocks Tutorial: Clean up IP Address Data
    :noheading:
    :data-transition: none

    .. image:: ../../images/slides_blocks_ips.svg


 .. revealjs:: Blocks Tutorial: Clean up User Data
    :noheading:
    :data-transition: none

    .. image:: ../../images/slides_blocks_users.svg


 .. revealjs:: Blocks Tutorial: Aggregate Data
    :noheading:
    :data-transition: none

    .. image:: ../../images/slides_blocks_aggregate.svg

 .. revealjs:: Blocks Tutorial: Build Final Output
    :noheading:
    :data-transition: none

    .. image:: ../../images/slides_blocks_all.svg


 .. revealjs:: Blocks Tutorial: Order Matters
    :noheading:
    :data-transition: none

    .. image:: ../../images/slides_blocks_order.svg

