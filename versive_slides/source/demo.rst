================================================
Docs Revolution 2017
================================================


.. revealjs::

 .. revealjs:: Docs Revolution 2017: Part II
    :data-transition: none

    .. image:: ../../images/busycorp.svg

 .. revealjs:: The Golden Rules of Technical Writing (updated!)
    :data-transition: none

    The golden rules of technical writing (abridged):

    * topics are not snowflakes, they are cattle. make them moo!
    * templates and patterns over process and rules
    * reference material is the easiest thing to document, so start there
    * build doc sets asynchronously (from that which the docs are documenting); publish those builds to a website as often as possible
    * your "audience" is way less important in the age of search (plus it was always less important than people thought it was)
    * it's impossible to be your own editor 
    * reuse content as often as possible by single-sourcing what you can, where you can (new!)


 .. revealjs:: Costs
    :data-transition: none

    True facts:

    * Content is expensive.
    * Most companies don't factor in hidden costs, such as automation, engineering, support.
    * Many companies hide costs behind teams that have customer-facing roles, such as support, training, and solutions teams.

    We are spending at least $500,000 per year on content. Right now.

    (Probably a lot more if we sat down and figured it all out.)


 .. revealjs:: Content Silos
    :data-transition: none

    Most companies (and by that I mean "every company everywhere") writes documentation in silos:

    * Engineers document their tools and features, sometimes automated from code, typically constrained by whatever Agile sprint they happen to be working in this week.
    * Technical writers support engineers and build out complete reference collections, but often in a way that isn't totally aligned with engineering effort. 
    * Marketing folks crank out whitepapers whenever they want.
    * Support teams shove reactive troubleshooting topics into proprietary ticketing systems like Zendesk as customers raise issues.
    * Training teams copypaste from reference documenation to build tutorials in PowerPoint decks.


 .. revealjs:: Problems with Silos
    :data-transition: none

    Across the entire company, the cadence of documentation is asynchronous to any individual silos:

    * People only focus on the content in their silo.
    * Every silo chooses its own authoring format and output.
    * Engineers automate a REST API via Swagger.
    * Support engineers toss content into Zendesk.
    * Marketing writers blog in WordPress, crank out PDFs from Word docs, and fight with WordPress plugins in their "spare" time.
    * Training folks (slowly) build out PowerPoint decks and stick them on a share, but also maintain copies of these decks on their own laptops and don't update the one on the share and they're never in sync. So they update them on the plane as they fly from customer to customer.
    * Technical writers tend to prefer XML tooling that every non-technical writer hates as much as a marketing writer hates WordPress.

    Nobody wins.

 .. revealjs:: Not Fine.
    :data-transition: none

    .. image:: ../../images/tableflip.svg


 .. revealjs:: Kill Silos by Reusing Content across Silos
    :data-transition: none

    We should build our content using the same

    * System
    * Tools
    * Processes
    * Words
    * Effort
    * Review standards
    * ... whatever.

    Put all the content into the same library. From there, every word that is written can be reused. Every word that is reused lowers costs, increases accuracy, and makes it easier for us to communicate with our customers.


 .. revealjs:: Live Demo
    :data-transition: none

    <insert live demo here>