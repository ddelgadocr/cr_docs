BUILDDIR = build
S3BUCKET = cr-docs
S3OPTIONS = --acl-public --exclude='.doctrees/*' --exclude='cr_docs/.doctrees/*' --config ~/.s3cfg-cr-docs  --add-header "Cache-Control: max-age=0"
BUILD_COMMAND = sphinx-build -a -W
PARALLEL_BUILD:=
BUILD_COMMAND_AND_ARGS = $(BUILD_COMMAND) $(PARALLEL_BUILD)

clean:
	@rm -rf $(BUILDDIR)

cr_master:
	mkdir -p $(BUILDDIR)
#	cp -r misc/robots.txt build/
#	cp -r misc/sitemap.xml build/
	$(BUILD_COMMAND_AND_ARGS) cr_master/source $(BUILDDIR)

cr_cse:
	mkdir -p $(BUILDDIR)
	$(BUILD_COMMAND_AND_ARGS) cr_master/source $(BUILDDIR)

# cr-subsite-name:
#	mkdir -p $(BUILDDIR)/release/cr-subsite-name/
#	$(BUILD_COMMAND_AND_ARGS) cr-subsite-name/source $(BUILDDIR)/release/cr-subsite-name/

# decks:
# 	mkdir -p $(BUILDDIR)/decks/
# 	$(BUILD_COMMAND_AND_ARGS) slide_decks/source $(BUILDDIR)/decks/

upload:	release
	s3cmd sync $(S3OPTIONS) $(BUILDDIR)/ s3://$(S3BUCKET)/
