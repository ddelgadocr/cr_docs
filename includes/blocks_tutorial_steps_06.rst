.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

The ``group_by`` column tells the engine to apply aggregations from the context of this column. Set ``group_by`` to ``source_ip``:

.. code-block:: yaml

   aggregate_proxy:
     class_name:
       symbol: 'AggregateBlock'
     group_by: source_ip
     columns:
       - name: source_ip
         type: str
       ...

For each unique source IP address, the results of each declared operation will be added to the row output.
