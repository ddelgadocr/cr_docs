.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

Update the ``outputs`` configuration to specify the final block in the block graph from which the block graph output is generated:

.. code-block:: yaml

   block_graphs:
     tutorial:
       blocks:
         parse_proxy:
           ...
       outputs:
         - block_name: aggregate_proxy
           frame_key: {'stage': 'parsed'}
         - block_name: parse_proxy
           frame_key: {'stage': 'parsed_error'}

Parsed data takes two paths:

* Unsuccessfully parsed data is sent to ``parsed_error`` output
* Successfully parsed data is sent to the ``parsed`` output, from which processing---additional parse, standardize, transform, and aggregate steps---can be run
