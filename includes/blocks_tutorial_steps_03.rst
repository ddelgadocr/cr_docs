.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

Raw IP address data should be cleaned up so that all of the source IP address and all of the destination IP address are separated into unique columms. This step uses the ``StandardizeIPAddressBlock`` and the configuration is similar to:

.. code-block:: yaml

   clean_ips:
     class_name:
       symbol: 'StandardizeIPAddressBlock'
     columns:
       - name: source_ip
         type: str
       - name: dest_ip
         type: str
