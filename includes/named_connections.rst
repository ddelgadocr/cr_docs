.. 
.. The contents of this file are included in the following topics:
.. 
..    cmd_cr
..    deploy_platform
..    tutorials/named_connections
.. 

All credentials that are needed to connect to the storage cluster may be stored in a named connection that is assigned a friendly name, such as ``aws`` or ``hdfs``. All named connections require a name and type. Certain named connection types require additional options to be specified as name-value pairs.

A named connection simplifies access to data that will be retrieved and processed by the |versive| platform.  Once created, users of the |versive| platform may refer to the simple name from Python scripts without having to supply the full set of credentials. If credentials expire periodically or if the location of the storage cluster changes, simply update the configuration for that named connection. Any script that uses that simple name will automatically use the updated configuration.
