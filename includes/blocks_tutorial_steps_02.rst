.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

Not all proxy data is parsable. This can be for many reasons and can be mitigated and improved over time. But for each day's processing, parsable data must be separated from un-parsable data. Use the ``outputs`` section to declare the output tables for these two groups of data:

.. code-block:: yaml

   block_graphs:
     tutorial:
       blocks:
         parse_proxy:
           ...
       outputs:
         - block_name: parse_proxy
           frame_key: {'stage': 'parsed'}
         - block_name: parse_proxy
           frame_key: {'stage': 'parsed_error'}
