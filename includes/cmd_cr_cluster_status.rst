.. 
.. The contents of this file are included in the following topics:
.. 
..    cmd_cr
..    deploy_platform
.. 


.. code-block:: console

    $ cr cluster status --loop=600

The master node and all compute nodes should be returned, listed with heartbeats shorter than 60 seconds. For example:

.. code-block:: console

   ==== 2014/09/17 05:36:05
   ==== Cluster test (27583dfa206c48a0a3c20beb59876c62)
   Type Heartbeat CPU MB-RSS MBTotal CPU% Host
   Master 52 sec 1 365 1652 0.2 10.250.57.149
   Compute 39 sec 4 209 14980 0.0 10.250.9.95
