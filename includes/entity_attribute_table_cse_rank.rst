.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    entity_attribute_tables
.. 

A :ref:`ranking table <model-data-ranks-ranking-table>` contains the results of the calculations performed against the intermediate table, including the total number of rows for the key column and the total number of distinct entries for any additional columns that were specified for the intermediate table on which the ranking table depends.
