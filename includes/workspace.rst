.. The contents of this file are included in multiple topics:
.. 
..    xxxxx
.. 

To control the location used for reading inputs and writing outputs of this phase, configure the **workspaces** section of the configuration file.
