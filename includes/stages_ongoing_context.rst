.. 
.. The contents of this file are included in the following topics:
.. 
..    describe_behaviors
..    stages
..    slide_decks/stages
.. 

Traditional perimeter defense and endpoint protection are important even though most are defeated once the attacker is inside the network.

Even more advanced perimeter defense solutions solutions fall short. They may find important tells that indicate the presence of an adversary within the network, but often have a high false positive rate.

Once inside the network, the patterns of the adversary closely follow those described for the |stage_recon|, |stage_collect|, and |stage_exfil| stages. This predictable behavior enables the use of machine learning to surface potential adversary activity as it relates to any invidual stages and across all stages.

Using the engine to discover adversary activity inside the network is a monumental shift. All of the pieces are pulled together into a map that shows potential adversary activity inside the network with the right context, the right level of accuracy, and a ranking of activity for prioritization and follow-up. This map is also referred to as a "threat case".

It is possible for the engine to discover retroactively the evidence for previously-unnoticed |stage_recon|, |stage_collect|, and |stage_exfil| efforts in much the same way the engine can discover ongoining |stage_recon|, |stage_collect|, and |stage_exfil| efforts.
