.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 

A malicious actor typically spends considerable time in the |stage_plan| stage. Most of this activity is either undetectable or not of value to an SOC team. Initial targeting occurs outside the network and can come from any number of public or private sources. Many of which never need to touch the target network itself.
