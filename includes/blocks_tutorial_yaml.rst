.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in configure.rst (for "block_graph" config group) and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

The following code shows the complete configuration for a block graph:

.. code-block:: yaml

   block_graphs:
     tutorial:
       blocks:
         parse_proxy:
           class_name:
             symbol: 'ParseBlock'
           schema: '/path/to/schema.yml'
           date_format: '%Y%m%d'
           path: '/path/to/output/location'
         clean_ips:
           class_name:
             symbol: 'StandardizeIPAddressBlock'
           columns:
             - name: source_ip
               type: str
             - name: dest_ip
               type: str
         clean_user:
           class_name:
             symbol: 'StandardizeUsernameBlock'
           columns:
             - name: user
               type: str
           username_contains_domain: True
           domain_suffix: '_domain'
         aggregate_proxy:
           class_name:
             symbol: 'AggregateBlock'
           group_by: source_ip
           columns:
             - name: source_ip
               type: str
             - name: bytes_in
               type: str
             - name: bytes_out
               type: str
             - name: dest_ip
               type: str
             - name: user
               type: str
           ops:
             - name: sum
               columns: bytes_in, bytes_out
             - name: list
               columns: dest_ip, user
               limit: 255
         depends:
           clean_ips: ['parse_proxy']
           clean_user: ['clean_ips']
           aggregate_proxy: ['clean_user']
         outputs:
           - block_name: aggregate_proxy
             frame_key: {'stage': 'parsed'}
           - block_name: parse_proxy
             frame_key: {'stage': 'parsed_error'}

