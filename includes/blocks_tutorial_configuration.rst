.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

A block graph, and all of the individual blocks in that block graph, are defined in a dedicated location in the configuration file for the engine grouped under ``block_graphs``:

.. code-block:: yaml

   block_graphs:
     tutorial:
       blocks:
         <block_name>:
           class_name:
             symbol: 'BlockName'
           columns:
             - name: column_name
               type: str
             ...

The configuration for the individual blocks in the block graph are covered later on in this tutorial.
