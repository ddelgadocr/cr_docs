.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    entity_attribute_tables
.. 

A :ref:`sharing table <model-data-sharing-table>` computes computed publication and consumption ratios for user and registered domain data.
