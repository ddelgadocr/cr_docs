.. 
.. The contents of this file are included in the following topics:
.. 
..    cmd_cr
..    deploy_platform
.. 

The |versive| platform may connect to data that is stored on an Amazon S3 storage cluster. To access data stored on an Amazon S3 storage cluster, first make note of the name of the Amazon S3 bucket, the AWS access key ID, the AWS secret access key, and the AWS region.

**To connect to data on an Amazon S3 storage cluster**

#. Use the ``cr connect add`` command to add a named connection for the storage cluster:

   .. code-block:: console

      $ cr connect add --name='NAME' --type='TYPE' bucket='BUCKET' \
        aws_access_key_id='KEY_ID' aws_secret_access_key='SECRET_KEY' \
        default_host='REGION'

   For example:

   .. code-block:: console

      $ cr connect add --name='aws' --type='s3' bucket='vse-cluster' \
        aws_access_key_id='ASsdt3drF9CA20' \
        aws_secret_access_key='xzlk9adk123d4tgf' \
        default_host='s3-us-west-2.amazonaws.com'

   .. note:: Future connections to this storage cluster only require ``--name`` and ``--type`` to be specified. For example: ``cr connect add --name='aws' --type='s3'``.

#. Verify the named connection is set up correctly by using the ``cr connect list`` command:

   .. include:: ../../includes/cmd_cr_connect_list_amazon_s3.rst
