.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 


At this point, a defender rarely gains significant knowledge about an adversary, other than perhaps a general understanding of their skill level. Unfortunately, when initial access fails, that adversary can simply try again. And when initial access is successful, that adversary often removes evidence of entry.

Traditional permiter monitoring solutions have difficulty finding |stage_access| stage activity and must also find it quickly. The reality is that a determined adversary will gain access to the network, regardless of the intrusion detection systems and/or endpoint protections that are deployed on the network.
