.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

Each block defines its own input and output behaviors. Within this tutorial, the block for parsing data into the engine is unique; the other blocks are the same:

* Each block inputs a Spark DataFrame object. Input columns are assumed to be identical to the columns that were output from the previous block.
* Each block outputs a Spark DataFrame object. Output columns are assumed to be identical to the columns that were input to this block, unless otherwise declared.

The expectation that a block will input and output consistent sets of tabular data is sometimes referred to as a "column contract" and is visible in the configuration file.
