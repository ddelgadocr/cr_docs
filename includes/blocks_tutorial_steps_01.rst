.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

Proxy log file data must be converted to tabular data:

* A schema file tells the engine how to parse the ingested log file
* A path declares the location to which output is saved
* A date format to use when applying timestamps in the parsed data
* Output tables that split successfully parsed data from unsuccessfully parsed data

The configuration for a ``ParseBlock`` is shown here:

.. code-block:: yaml

   block_graphs:
     tutorial:
       blocks:
         parse_proxy:
           class_name:
             symbol: 'ParseBlock'
           schema: '/path/to/schema.yml'
           date_format: '%Y%m%d'
           path: '/path/to/output/location'
       ...
