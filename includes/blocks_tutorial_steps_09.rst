.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
..  

The desired order of processing for successfully parsed data is 1) parse_proxy, 2) clean_ips, 3) clean_users, 4) aggregate_proxy, after which this data is added to the ``parsed`` output table. After verifying the correct order, ensure the configuration matches:

.. code-block:: yaml

   block_graphs:
     tutorial:
       blocks:
         ...
       depends:
         clean_ips: ['parse_proxy']
         clean_user: ['clean_ips']
         aggregate_proxy: ['clean_user']

where each item under ``depends`` contains a block name followed by the block name for which that block has a dependency. For example: the ``aggregate_proxy`` block depends on the ``clean_user`` block, which depends on the ``clean_ips`` block, which depends on the ``parse_proxy`` block.
