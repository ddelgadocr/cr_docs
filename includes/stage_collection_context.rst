.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 

The |stage_collect| stage involves the collection of data from identified data stores, resources, and network locations to a location from which |stage_exfil| can occur. Indicators and measurements of |stage_collect| stage activity are similar to those found with |stage_recon| stage activity:

* Clients and servers have normal and expected behaviors and patterns. 
* Users browse the network already having an understanding of that network and what they expect find and will generally take a direct path through the network.

The |stage_collect| stage expands these patterns to include a better understanding of network social circles:

* Direction of data movement and the amount of data moved
* Session times, with a focus on longer-lived sessions that are present only in this stage
* Duration of activity over time and how that activity relates to other stages
* Numbers, locations, and types of potential targets
* Collection points (one or many machines from which data is collected); disparate data sources may indicate a lack of domain-specific interest that is unusual when compared to normal network activity
* Staging points (one or many machines to which data is moved)
* Proximity to locations in the network from which exfiltration may occur; each hop away from the target machine gets closer to the point at which data can be exfiltrated out of the network to an adversary-controlled (external) machine

Analysis of DNS, proxy, and flow log files will detect these types of collection activities. Comparing these patterns to analysis of the |stage_recon| and |stage_exfil| stages can result in greater levels of certainty.

Comparing multi-dimensional |stage_collect| stage data to |stage_recon| and |stage_exfil| stage data greatly increases the ability to discover adversary activity. Especially when this data can be correlated with strong and/or weak indicators of adversary activity that span all three stages. For example, |stage_collect| stage measurements that correlate strongly with |stage_recon| measurements can show adversary behavior even if |stage_exfil| measurements are not available. This absence can mean many things, ranging from:

* Activity that, once investigated and followed-up, is determined to be benign
* An out-of-band exfiltration where an individual has moved that data to a hard disk drive and then walked out of the building
* Evidence of actual |stage_exfil| activity that has yet to occur from one of the identified staging machines
