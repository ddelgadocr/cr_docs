.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 

.. TODO: single-source these with the sections in the stages.rst in the main docs.

An adversary campaign is broken into five distinct stages:

* |stage_plan|
* |stage_access|
* |stage_recon|
* |stage_collect|
* |stage_exfil|

These stages are all necessary for an adversary campaign to be successful and represent patterns that are discoverable from within an organization's network during an active campaign.

The |stage_recon|, |stage_collect|, and |stage_exfil| stages represent the most interesting stages because they contain a large amount of visible evidence that is discoverable with the right data inputs and the right amount of processing. These are the stages against which the engine is focused.