.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

The ``sources`` section of the configuration file declares the location from which data will be ingested, and then the name of the block graph responsible for processing that data:

.. code-block:: yaml

   sources:
     proxy:
       table_type: ProxyTable
       path: '/path/to/proxy/data/'
       parse:
         default_validation: True
         block_name: 'tutorial'

where the ``block_name`` value tells the engine to run the ``tutorial`` block graph while processing Proxy data. The engine will look under ``block_graphs`` to discover the ``tutorial`` block graph, identify the blocks that are defined in that block graph, and then run the blocks in the defined order.
