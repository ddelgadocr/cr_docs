.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    entity_attribute_tables
.. 

An :ref:`intermediate table <model-data-ranks-intermediate-table>` is aggregated over multiple days for the purpose of collecting enough data for generating ranked metrics and percentiles.
