.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    entity_attribute_tables
.. 

A :ref:`hostname table <model-data-hostname-table>` uses the ``hostname`` column as input, and then determines if a hostname is an IP address. For each hostname that is an IP address, adds columns for the IP address, the fully qualified domain name (FQDN), the number of hyphens it contains, and then returns a table with the following additional columns: ``HOSTNAME_IS_IP``, ``HOSTNAME_IS_IP_VALUE``, ``HOSTNAME_INCLUDES_FQDN``, ``HOSTNAME_NUM_HYPHENS``.
