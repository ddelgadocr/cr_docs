.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 

An adversary must move collected and staged data outside of the network.

* An external adversary often exfiltrates data soon after discovering data stores of interest and the path to use for exfiltration.
* An internal adversary has more options, such as using some type of physical medium like a hard disk drive, and may not move as quickly to exfiltrate data.
* Both external and internal adversaries will consider their risk tolerance for discovery prior to exfiltration.

Exfiltration is typically done from one (or more) staging systems from within the network. Evidence of preparing for exfiltration, and then for the exfiltration of data itself, is discoverable from flow and flow-like data sources, proxy logs, and so on. This type of analysis compares:

* The expected state of the network's configuration and security posture
* The actual state of the network's configuration and security posture over time

Analyzing changes to the expected configuration and security posture of the network along with understanding the protocols and paths that are associated with data movement combine to become an indicator of anomalous behavior.

For example, clandestinely enabling specific services (such as RDP) on a staging machine and/or modifying proxy or firewall settings to allow for previously-prohibited protocols (such as FTP), and then discovering unusual byte movement from those machines using certain protocols can be a strong indicator of anomalous behavior.

* When a protocol behaves in unexpected ways, this can be an indicator of command-and-control activity.
* Non-interactive sessions for protocols that are expected to be interactive, but take on non-interactive behaviors can be an indicator of data exfiltration.
* Data that moves at volume from inside the network to outside the network is measurable and can be categorized and when associated with anomalous protocol and/or session activity can be a strong indicator of actual or future |stage_exfil| stage activity.
