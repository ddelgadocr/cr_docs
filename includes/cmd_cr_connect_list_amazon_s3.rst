.. 
.. The contents of this file are included in the following topics:
.. 
..    cmd_cr
..    deploy_platform
.. 


.. code-block:: console

   $ cr connect list

will print information similar to:

.. code-block:: sql

   ------- ----------------------- -------
    Name    Description             Type
   ------- ----------------------- -------
    local   Storage on local disk   local
    s3      Storage on S3           s3
   ------- ----------------------- -------

.. note:: If a connection is not set up correctly, a table that shows local storage as the only named connection is printed:

   .. code-block:: sql

      ------- ----------------------- -------
       Name    Description             Type
      ------- ----------------------- -------
       local   Storage on local disk   local
      ------- ----------------------- -------
