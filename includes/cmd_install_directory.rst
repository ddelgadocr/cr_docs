.. 
.. The contents of this file are included in the following topics:
.. 
..    cmd_aggregate
..    cmd_check_quality
..    cmd_drive
..    cmd_generate_lookup_table
..    cmd_generate_modeling_table
..    cmd_generate_transition_table
..    cmd_generate
..    cmd_import_results_feedback
..    cmd_learn_models
..    cmd_list
..    cmd_parse
..    cmd_postprocess
..    cmd_predict
..    cmd_prepare
..    cmd_process
..    cmd_standardize_keys
..    cmd_transform
.. 


The default installation directory is ``/opt/cr``. If you installed somewhere else, substitute that path wherever you see ``/opt/cr``.