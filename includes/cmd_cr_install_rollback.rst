.. 
.. The contents of this file are included in the following topics:
.. 
..    cmd_cr
..    deploy_platform
.. 


A rollback replaces the contents of ``$CR_ROOT`` with the named snapshot for all nodes in the cluster. A rollback is useful for switching between application versions or rolling back to a previous version after an upgrade.

.. note:: Use the :ref:`cmd-cr-install-list` command to get a list of snapshots.
