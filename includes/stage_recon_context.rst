.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 


The |stage_recon| stage often sees more aggressive patterns of exploration by the adversary, depending on their resource and time constraints. They are looking to achieve their goal of quickly finding high-value targets, while staying within their risk tolerance for being discovered.

These patterns can be identified in a number of ways:

* Clients and servers have normal and expected behaviors and patterns. Client-server schizophrenia occurs when the activity of the client or server differs from the expected behaviors and patterns. An adversary who has discovered a server, and then continues their reconnaissance effort from that server, is more likely to use that server as if it were a client, which results in discoverable patterns of that server behaving as if it were a client.

* Users browse the network already having an understanding of that network and what they expect find and will generally take a direct path through the network. An adversary that does not understand the network will often show lateral movement (instead of direct movement), which results in discoverable patterns of that user not being familiar with the network.

After the adversary's understanding of the network has reached a sufficient level, they are more likely to use a combination of captured credentials, exploits, and privilege escalations to move laterally across the network.

Analysis of DNS, proxy, and flow log files will still detect these more advanced movement patterns. Looking at these patterns in isolation can result in false-positive discovery, but comparing these patterns to analysis of the |stage_collect| and |stage_exfil| stages can result in greater levels of certainty.
