.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

This tutorial walks through the following steps:

#. Verify data source configuration
#. Parse proxy data into a tabular format
#. Separate parsed from un-parsed
#. Clean up IP address data
#. Clean up user data
#. Add columns
#. Group by source IP address data
#. For each unique source IP address, sum traffic to and from the source IP address
#. For each unique source IP address, create lists for destination IP addresses and users
#. Verify block dependencies within the block graph
#. Verify output for parsed and un-parsed data
