.. 
.. This is in the blocks.rst and process_data.rst.
.. 

The following blocks are built into the engine and may be used in any block graph. Each individual block in the library represents a reusable feature that performs a specific task, such as converting column data to lowercase or pivoting data.

* :ref:`blocks-library-addcolumnbycategory`
* :ref:`blocks-library-addconstantcolumn`
* :ref:`blocks-library-addsubnetlabels`
* :ref:`blocks-library-addurlfeatures`
* :ref:`blocks-library-aggregate`
* :ref:`blocks-library-backup`
* :ref:`blocks-library-buildurl`
* :ref:`blocks-library-calculatebytesratiosblock`
* :ref:`blocks-library-calculatecolumnlength`
* :ref:`blocks-library-categorize`
* :ref:`blocks-library-categorizeport`
* :ref:`blocks-library-categorizeprotocol`
* :ref:`blocks-library-categorizetcpnotsnmp`
* :ref:`blocks-library-cleanflowdata`
* :ref:`blocks-library-collectflowstats`
* :ref:`blocks-library-concatenate`
* :ref:`blocks-library-convertstringtodatetime`
* :ref:`blocks-library-copycolumn`
* :ref:`blocks-library-deletecolumns`
* :ref:`blocks-library-fillcolumn`
* :ref:`blocks-library-filteremptyrows`
* :ref:`blocks-library-join`
* :ref:`blocks-library-lowercase`
* :ref:`blocks-library-mergecategories`
* :ref:`blocks-library-mergeentityscores`
* :ref:`blocks-library-mergetables`
* :ref:`blocks-library-parse`
* :ref:`blocks-library-parseurl`
* :ref:`blocks-library-partition`
* :ref:`blocks-library-preaggregateflow`
* :ref:`blocks-library-replacenonedates`
* :ref:`blocks-library-sample`
* :ref:`blocks-library-standardizeblock`
* :ref:`blocks-library-standardizedates`
* :ref:`blocks-library-standardizedomain`
* :ref:`blocks-library-standardizehostname`
* :ref:`blocks-library-standardizeipaddress`
* :ref:`blocks-library-standardizereverseip`
* :ref:`blocks-library-standardizeusername`
* :ref:`blocks-library-summarizetable`
* :ref:`blocks-library-validatecolumn`
* :ref:`blocks-library-verifyemptycolumns`
