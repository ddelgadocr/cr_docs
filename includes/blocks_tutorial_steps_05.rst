.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

Data that is transformed in certain ways is more useful. Add the following columns to the ``columns`` list: ``source_ip``, ``dest_ip``, ``bytes_in``, ``bytes_out``, and ``user`` using the ``name``/``type`` pair for each column:

.. code-block:: yaml

   aggregate_proxy:
     class_name:
       symbol: 'AggregateBlock'
     group_by: source_ip
     columns:
       - name: source_ip
         type: str
       - name: dest_ip
         type: str
       ...

and so on. (All of the column types should be ``str``.)
