.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 

Regardless of how much an adversary learned during the initial |stage_plan| stage, they are now faced with a network for which they lack visibility and understanding. The |stage_recon| stage starts with the adversary establishing a foothold in the network, after which they begin mapping the network and moving laterally across it while gathering information.

* Initially, these types of activities show a lack of knowledge about the types, locations, and contents of the various systems within the network.
* Over time, these activities become more precise and may involve placing command and control (C&C) channels at certain spots within the network.

Network reconnaissance is required to build and improve the adversary's understanding of what targets are available, such as:

* What is the hardware profile for the host?
* What software is installed on that host?
* What are the locations of key data stores?
* What types of endpoint protection are deployed?
* To which other hosts does this host have access?

The adversary will carefully and thoroughly look for high-value targets by scanning hosts and servers, scraping memory, analyzing log files, sniffing network traffic, and more.
