.. 
.. The contents of this file are included in the following topics:
.. 
..    define_anomalies
..    stages
..    slide_decks/stages
.. 

.. TODO: single-source these with the sections in the stages.rst in the main docs.

The traditional security hunting applications focus on the |stage_plan| and |stage_access| stages, whereas |vse| focus on the |stage_recon|, |stage_collect|, and |stage_exfil| stages because they contain a large amount of visible evidence that is discoverable with the right data inputs and the right amount of processing. These are the stages against which the engine is focused.
