.. 
.. The contents of this file are included in the following topics:
.. 
..    cmd_cr
..    deploy_platform
..    tutorials/named_connections
.. 


The |versive| platform may connect to data that is stored on an an HDFS storage cluster using a keytab file and Kerberos authentication.

.. note:: Before connecting the |versive| platform to the storage cluster:

   #. Install Kerberos tools on the HDFS storage cluster.
   #. Configure Kerberos authentication to use a keytab file that stores the Kerberos user and encrypted password for authentication.
   #. Configure two tokens to renew on a schedule that maintains access to the HDFS host. Using two tokens with overlapping expiration windows will help prevent authentication issues. The |versive| platform will generate the initial ticket-granting ticket, so there is no need to separately run ``kinit``.

   Additional information about setting up Kerberos authentication is available:

   * If using Hadoop, use `HTTP authentication <http://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/HttpAuthentication.html>`__.
   * If using Cloudera, the manual tasks for implementing Kerberos authentication can be automated with the `Kerberos wizard <http://www.cloudera.com/content/cloudera/en/documentation/core/latest/topics/cm_sg_intro_kerb.html>`__.

After Kerberos is configured on the HDFS storage cluster, make note of HDFS hostname, Kerberos settings, and user account to use for Kerberos authentication.

**To connect to data on an HDFS storage cluster using a keytab file**

#. Create a keytab file.

   Keytab files store the Kerberos user (principal) and encrypted password pair that any process capable of accessing the file can use to authenticate to a Kerberos realm without requiring the user to enter a password.

   For the encryption type, enter either ``rc4-hmac`` or ``aes256-cts``:

   .. code-block:: console

      $ ktutil
        ktutil: add_entry -password -p <USER>@<KERBEROS_REALM> -k 1 -e <ENCRYPTION_TYPE>
        Password for <USER>@<KERBEROS_REALM>: [enter password]
        ktutil: write_kt <USER>.keytab
        ktutil: quit

#. Use ``scp`` (or a similar command) to distribute the keytab file to the same location on all cluster nodes:

   .. code-block:: console

      $ scp <USER>.keytab username@hostname:/directory

#. For each node in the cluster, update the ``/etc/krb5.conf`` configuration file with an entry for the Kerberos real in the ``[realms]`` section:

   .. code-block:: text

      <KERBEROS_REALM> = {
          kdc = <IP_ADDRESS>
          admin_server = <IP_ADDRESS>
      }

#. Verify the settings are correct. On the client computer, authenticate as the local user:

   .. code-block:: console

      $ kinit -kt /path/to/<USER>.keytab <USER>

   Connect to the WebHDFS server that is Kerberos-enabled, and use the cURL ``negotiate`` argument to discuss authentication with the web server:

   .. code-block:: console

      $ curl -i --negotiate -u : "http://<IP_ADDRESS>:50070/webhdfs/v1/?op=LISTSTATUS" \
        egrep -v '^<br/>\s*$'

#. Use the ``cr connect add`` command to add a named connection for the storage cluster:

   .. code-block:: console

      $ cr connect add --name=NAME --type=TYPE host="IP_ADDRESS" \
        port=PORT kerberos_user=USER kerberos_realm=REALM \
        kerberos_keytab=PATH/TO/USER.keytab

   For example:

   .. code-block:: console

      $ cr connect add --name="hdfs" --type="hdfs" host="10.213.168.179" \
        port=88 kerberos_user="user" kerberos_realm="realm" \
        kerberos_keytab="path/to/user.keytab"

#. Verify the named connection is set up correctly by using the ``cr connect list`` command:

   .. include:: ../../includes/cmd_cr_connect_list_hdfs.rst
