.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

Raw user data should be cleaned up so that only user data is in the ``user`` column. If the user data contains a domain, that information is stored in a new column named ``user_domain``:

.. code-block:: yaml

   clean_user:
     class_name:
       symbol: 'StandardizeUsernameBlock'
     columns:
       - name: user
         type: str
     username_contains_domain: True
     domain_suffix: '_domain'
