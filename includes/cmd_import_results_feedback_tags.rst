.. 
.. The contents of this file are included in the following topics:
.. 
..    commands
..    models
.. 


Multiple different tag values per ``(entity id, <reason_code>_tags)`` exist across all days combined cannot be specified. The **import_results_feedback** command will fail if such rows are present.
