.. 
.. The contents of this file are included in the following topics:
.. 
..    configure
..    entity_attribute_tables
.. 

A :ref:`sessionize table <model-data-sessionize-table>` computes session statistics per hostname, where a session is each interaction by a particular domain over a consecutive 30-minute time window, and then returns a table with columns and values added for ``session_length`` and ``time_until_next_session``.
