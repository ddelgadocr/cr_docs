.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

Next, aggregate the ``bytes_in`` and ``bytes_out`` columns to sum their values by using the ``ops`` grouping to declare the calculation (``sum``), and then list the columns for which this calculation is performed:

.. code-block:: yaml

   aggregate_proxy:
     class_name:
       symbol: 'AggregateBlock'
     group_by: source_ip
     columns:
       ...
       - name: bytes_in
         type: str
       - name: bytes_out
         type: str
       ...
     ops:
       - name: sum
         columns: bytes_in, bytes_out
