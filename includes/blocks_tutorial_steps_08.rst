.. 
.. NOTE: Cannot link to any files using the :ref: or :doc: directives because this content is in a slide deck.
.. This is in the blocks.rst slide deck and (currently) in the start_here.rst doc for the "Blocks Tutorial".
.. 

Next, aggregate the ``dest_ip`` and ``user`` columns to create a list that does not exceed 255 items:

.. code-block:: yaml

   aggregate_proxy:
     class_name:
       symbol: 'AggregateBlock'
     group_by: source_ip
     columns:
       ...
       - name: dest_ip
         type: str
       - name: user
         type: str
       ...
     ops:
       ...
       - name: list
         columns: dest_ip, user
         limit: 255
