.. 
.. The contents of this file are included in the following topics:
.. 
..    describe_behaviors
..    stages
..    slide_decks/stages
.. 

A successful adversary will continue to look for more data, returning to the |stage_recon| and |stage_collect| stages as often as their risk tolerance allows. It is not uncommon for an adversary to perform additional steps in the |stage_recon| stage based on what they have learned during the |stage_collect| and |stage_exfil| stages. In some cases, the adversary may even return to the initial planning and access stages for new attempts at additional areas within the target network.
