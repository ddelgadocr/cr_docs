.. 
.. xxxxx
.. 

The following example shows how use ``0`` as a marker to find missing values, and then load the table:

.. code-block:: python

   hint0 = risp.create_parser_hint_missing(column='mynumber',
                                           additional_markers=['0'])
   table = risp.load_crtable(path='myproject/',
                             hints=[hint0])
