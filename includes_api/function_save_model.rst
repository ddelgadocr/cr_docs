.. 
.. xxxxx
.. 

The :ref:`api-save-model` function saves a model to persistent storage. Saved models do not contain checkpointed computations that are held in-memory.
