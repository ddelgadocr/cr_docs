.. 
.. xxxxx
.. 

The :ref:`api-load-tabular` function loads data stored in a tabular format---such as CSV, Excel, TSV, Parquet, RC, Gzip-compressed---for transformation and modeling. A table should be:

* Loaded to ensure it is in the correct format
* Validated to help identify problems with the data
* Subsampled to improve the speed at which models are developed, as well as requiring less memory to perform operations. 

  .. note:: Data is subsampled randomly, which may cause misleading results in time-series data. In some cases, it may be necessary to reduce the size of a data file manually, and then load it using validation.
