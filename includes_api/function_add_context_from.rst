.. 
.. xxxxx
.. 

The :ref:`api-add-context-from` function adds columns that aggregate data from a secondary table based on timestamp column data according to a context operation.
