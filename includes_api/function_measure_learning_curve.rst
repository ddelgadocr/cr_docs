.. 
.. xxxxx
.. 

The :ref:`api-measure-learning-curve` function measures a learning curve, which reflects how model accuracy varies as a function of the number of input rows. Use this function to help identify the point at which adding data to a model stops improving the accuracy of that model.
