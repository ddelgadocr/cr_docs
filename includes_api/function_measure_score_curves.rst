.. 
.. xxxxx
.. 

The :ref:`api-measure-score-curve` function calculates a table of predictions produced from a binary classification model to evaluate its performance as its threshold changes and to identify the point at which the classification threshold should be defined.
