.. 
.. in release_notes and api
.. 

**repartition** (bool, optional)
   Specifies how the output table is partitioned. Default value: ``True``.

   If ``True``, the input data are shuffled and rebalanced into new partitions of roughly equal size. The number of new partitions equals the largest number of partitions among the input tables.

   If ``False``, the input data are *not* shuffled, and the returned table contains all the partitions from the input tables. The amount of data per partition is unchanged.

   For example: Three input tables (X, Y, and Z). Table X has three partitions with 100 rows each. Table Y has one partition with 3 million rows. Table Z has one partition with one row. When ``False`` the output table will have five partitions with row counts of 100, 100, 100, 3 million, and 1. When ``True``, the output table will have three partitions with roughly 1,000,100 rows.
