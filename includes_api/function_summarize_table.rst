.. 
.. xxxxx
.. 

The :ref:`api-summarize-table` function returns summary statistics for each column in a table, except for columns with ``None`` values. Different statistics may be returned depending on the type of data in each column.
