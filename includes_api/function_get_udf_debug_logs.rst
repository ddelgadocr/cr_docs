.. 
.. xxxxx
.. 

The :ref:`api-get-udf-debug-logs` function returns a list of log statements that were issued by user-defined functions for the named table.
