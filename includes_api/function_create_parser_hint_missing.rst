.. 
.. xxxxx
.. 

The :ref:`api-create-parser-hint-missing` function creates a hint for parsing missing values in a column when loading a table. The engine automatically identifies several values---such as ``NA`` or empty cells---as markers for missing values. However, in some data sets, a missing numeric value might be coded as ``99`` or ``-1`` or a missing label might be coded as ``MISSING``. This function allows these types of values to be treated as markers for missing values.
