.. 
.. xxxxx
.. 

The :ref:`api-save-csv` function saves a table from a working session to a comma-separated values (CSV) file.
