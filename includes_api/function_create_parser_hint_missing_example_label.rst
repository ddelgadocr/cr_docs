.. 
.. xxxxx
.. 

The following example shows how to use ``UNKNOWN`` as a marker to find missing labels, and then load the table:

.. code-block:: python

   hintunknown = risp.create_parser_hint_missing(column='mystring',
                                                 additional_markers=['UNKNOWN'])
   table = risp.load_crtable(path='myproject/',
                             hints=[hintunknown])
