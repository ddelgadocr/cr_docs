.. 
.. xxxxx
.. 

The :ref:`api-measure-confusion-matrix` function `calculates a confusion matrix <https://en.wikipedia.org/wiki/Confusion_matrix>`__ for a table of predictions produced from a binary or multiclass classification model. A confusion matrix compares sets of data for actual and predicted values. The values for each identical data point are compared to each other. The results of that comparison show where the model is more and less successful at making predictions.
