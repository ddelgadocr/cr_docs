.. 
.. xxxxx
.. 

The :ref:`api-score-predictions` function measures prediction accuracy by making predictions, applying one (or more) scoring metrics against a test table in which the target values are known.
