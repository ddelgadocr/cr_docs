.. 
.. xxxxx
.. 

The :ref:`api-measure-input-importance` function generates a report that describes how model accuracy varies for different sets of input columns. Use this function to:

* Identify information leakage, i.e. "data that is derived from the target" or "data from the future that is used as an input for model building"
* Inderstand the inputs that have the greatest influence on model predictions
* Identify inputs that can be removed without losing prediction accuracy
