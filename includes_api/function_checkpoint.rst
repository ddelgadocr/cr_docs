.. 
.. xxxxx
.. 

The :ref:`api-checkpoint` function holds tables and models in-memory so they may be quickly retrieved later. Checkpointed items are retained across working sessions, as long as the compute cluster is not reset. Using checkpoints frequently allows a model to be easily returned to a previous state, if necessary. Because the contents of a checkpoint are held in-memory, this speeds up development by preventing the need to re-run expensive computations.
