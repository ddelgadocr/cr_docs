.. 
.. xxxxx
.. 

The :ref:`api-add-context` function adds one (or more) columns to a table that aggregate data by time, according to a context operation.
