.. 
.. xxxxx
.. 

The :ref:`api-udf-debug-log` function records a log statement from inside a user-defined function. Use this log statement to help troubleshoot issues with user-defined functions.

.. note:: This function should be used sparingly because each node in the compute cluster maintains a short list of only the most recent log statements.
