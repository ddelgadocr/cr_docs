.. 
.. xxxxx
.. 

The :ref:`api-predict` function uses the data table from a previously-learned model to predict the target, such as from a test table that contains the known values for the target or from a table that contains new data in which the target is unknown. In all cases, a table must contain the same columns that were used as inputs when learning the model.
